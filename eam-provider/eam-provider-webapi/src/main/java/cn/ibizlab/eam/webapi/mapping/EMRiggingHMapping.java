package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMRiggingH;
import cn.ibizlab.eam.webapi.dto.EMRiggingHDTO;
import cn.ibizlab.eam.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="WebApiEMRiggingHMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface EMRiggingHMapping extends MappingBase<EMRiggingHDTO, EMRiggingH> {


}

