package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSTop;
import cn.ibizlab.eam.core.eam_core.service.IEMEQSTopService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQSTopSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"设备停机监控表" })
@RestController("WebApi-emeqstop")
@RequestMapping("")
public class EMEQSTopResource {

    @Autowired
    public IEMEQSTopService emeqstopService;

    @Autowired
    @Lazy
    public EMEQSTopMapping emeqstopMapping;

    @PreAuthorize("hasPermission(this.emeqstopMapping.toDomain(#emeqstopdto),'eam-EMEQSTop-Create')")
    @ApiOperation(value = "新建设备停机监控表", tags = {"设备停机监控表" },  notes = "新建设备停机监控表")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqstops")
    public ResponseEntity<EMEQSTopDTO> create(@Validated @RequestBody EMEQSTopDTO emeqstopdto) {
        EMEQSTop domain = emeqstopMapping.toDomain(emeqstopdto);
		emeqstopService.create(domain);
        EMEQSTopDTO dto = emeqstopMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqstopMapping.toDomain(#emeqstopdtos),'eam-EMEQSTop-Create')")
    @ApiOperation(value = "批量新建设备停机监控表", tags = {"设备停机监控表" },  notes = "批量新建设备停机监控表")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqstops/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQSTopDTO> emeqstopdtos) {
        emeqstopService.createBatch(emeqstopMapping.toDomain(emeqstopdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqstop" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqstopService.get(#emeqstop_id),'eam-EMEQSTop-Update')")
    @ApiOperation(value = "更新设备停机监控表", tags = {"设备停机监控表" },  notes = "更新设备停机监控表")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqstops/{emeqstop_id}")
    public ResponseEntity<EMEQSTopDTO> update(@PathVariable("emeqstop_id") String emeqstop_id, @RequestBody EMEQSTopDTO emeqstopdto) {
		EMEQSTop domain  = emeqstopMapping.toDomain(emeqstopdto);
        domain .setEmeqstopid(emeqstop_id);
		emeqstopService.update(domain );
		EMEQSTopDTO dto = emeqstopMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqstopService.getEmeqstopByEntities(this.emeqstopMapping.toDomain(#emeqstopdtos)),'eam-EMEQSTop-Update')")
    @ApiOperation(value = "批量更新设备停机监控表", tags = {"设备停机监控表" },  notes = "批量更新设备停机监控表")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqstops/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQSTopDTO> emeqstopdtos) {
        emeqstopService.updateBatch(emeqstopMapping.toDomain(emeqstopdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqstopService.get(#emeqstop_id),'eam-EMEQSTop-Remove')")
    @ApiOperation(value = "删除设备停机监控表", tags = {"设备停机监控表" },  notes = "删除设备停机监控表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqstops/{emeqstop_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqstop_id") String emeqstop_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqstopService.remove(emeqstop_id));
    }

    @PreAuthorize("hasPermission(this.emeqstopService.getEmeqstopByIds(#ids),'eam-EMEQSTop-Remove')")
    @ApiOperation(value = "批量删除设备停机监控表", tags = {"设备停机监控表" },  notes = "批量删除设备停机监控表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqstops/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqstopService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqstopMapping.toDomain(returnObject.body),'eam-EMEQSTop-Get')")
    @ApiOperation(value = "获取设备停机监控表", tags = {"设备停机监控表" },  notes = "获取设备停机监控表")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqstops/{emeqstop_id}")
    public ResponseEntity<EMEQSTopDTO> get(@PathVariable("emeqstop_id") String emeqstop_id) {
        EMEQSTop domain = emeqstopService.get(emeqstop_id);
        EMEQSTopDTO dto = emeqstopMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取设备停机监控表草稿", tags = {"设备停机监控表" },  notes = "获取设备停机监控表草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqstops/getdraft")
    public ResponseEntity<EMEQSTopDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emeqstopMapping.toDto(emeqstopService.getDraft(new EMEQSTop())));
    }

    @ApiOperation(value = "检查设备停机监控表", tags = {"设备停机监控表" },  notes = "检查设备停机监控表")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqstops/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQSTopDTO emeqstopdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqstopService.checkKey(emeqstopMapping.toDomain(emeqstopdto)));
    }

    @PreAuthorize("hasPermission(this.emeqstopMapping.toDomain(#emeqstopdto),'eam-EMEQSTop-Save')")
    @ApiOperation(value = "保存设备停机监控表", tags = {"设备停机监控表" },  notes = "保存设备停机监控表")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqstops/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEQSTopDTO emeqstopdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emeqstopService.save(emeqstopMapping.toDomain(emeqstopdto)));
    }

    @PreAuthorize("hasPermission(this.emeqstopMapping.toDomain(#emeqstopdtos),'eam-EMEQSTop-Save')")
    @ApiOperation(value = "批量保存设备停机监控表", tags = {"设备停机监控表" },  notes = "批量保存设备停机监控表")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqstops/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQSTopDTO> emeqstopdtos) {
        emeqstopService.saveBatch(emeqstopMapping.toDomain(emeqstopdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSTop-searchDefault-all') and hasPermission(#context,'eam-EMEQSTop-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"设备停机监控表" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqstops/fetchdefault")
	public ResponseEntity<List<EMEQSTopDTO>> fetchDefault(EMEQSTopSearchContext context) {
        Page<EMEQSTop> domains = emeqstopService.searchDefault(context) ;
        List<EMEQSTopDTO> list = emeqstopMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSTop-searchDefault-all') and hasPermission(#context,'eam-EMEQSTop-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"设备停机监控表" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqstops/searchdefault")
	public ResponseEntity<Page<EMEQSTopDTO>> searchDefault(@RequestBody EMEQSTopSearchContext context) {
        Page<EMEQSTop> domains = emeqstopService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqstopMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

