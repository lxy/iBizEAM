package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQMonitor;
import cn.ibizlab.eam.core.eam_core.service.IEMEQMonitorService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQMonitorSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"设备状态监控" })
@RestController("WebApi-emeqmonitor")
@RequestMapping("")
public class EMEQMonitorResource {

    @Autowired
    public IEMEQMonitorService emeqmonitorService;

    @Autowired
    @Lazy
    public EMEQMonitorMapping emeqmonitorMapping;

    @PreAuthorize("hasPermission(this.emeqmonitorMapping.toDomain(#emeqmonitordto),'eam-EMEQMonitor-Create')")
    @ApiOperation(value = "新建设备状态监控", tags = {"设备状态监控" },  notes = "新建设备状态监控")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmonitors")
    public ResponseEntity<EMEQMonitorDTO> create(@Validated @RequestBody EMEQMonitorDTO emeqmonitordto) {
        EMEQMonitor domain = emeqmonitorMapping.toDomain(emeqmonitordto);
		emeqmonitorService.create(domain);
        EMEQMonitorDTO dto = emeqmonitorMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqmonitorMapping.toDomain(#emeqmonitordtos),'eam-EMEQMonitor-Create')")
    @ApiOperation(value = "批量新建设备状态监控", tags = {"设备状态监控" },  notes = "批量新建设备状态监控")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmonitors/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQMonitorDTO> emeqmonitordtos) {
        emeqmonitorService.createBatch(emeqmonitorMapping.toDomain(emeqmonitordtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqmonitor" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqmonitorService.get(#emeqmonitor_id),'eam-EMEQMonitor-Update')")
    @ApiOperation(value = "更新设备状态监控", tags = {"设备状态监控" },  notes = "更新设备状态监控")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqmonitors/{emeqmonitor_id}")
    public ResponseEntity<EMEQMonitorDTO> update(@PathVariable("emeqmonitor_id") String emeqmonitor_id, @RequestBody EMEQMonitorDTO emeqmonitordto) {
		EMEQMonitor domain  = emeqmonitorMapping.toDomain(emeqmonitordto);
        domain .setEmeqmonitorid(emeqmonitor_id);
		emeqmonitorService.update(domain );
		EMEQMonitorDTO dto = emeqmonitorMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqmonitorService.getEmeqmonitorByEntities(this.emeqmonitorMapping.toDomain(#emeqmonitordtos)),'eam-EMEQMonitor-Update')")
    @ApiOperation(value = "批量更新设备状态监控", tags = {"设备状态监控" },  notes = "批量更新设备状态监控")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqmonitors/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQMonitorDTO> emeqmonitordtos) {
        emeqmonitorService.updateBatch(emeqmonitorMapping.toDomain(emeqmonitordtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqmonitorService.get(#emeqmonitor_id),'eam-EMEQMonitor-Remove')")
    @ApiOperation(value = "删除设备状态监控", tags = {"设备状态监控" },  notes = "删除设备状态监控")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqmonitors/{emeqmonitor_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqmonitor_id") String emeqmonitor_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqmonitorService.remove(emeqmonitor_id));
    }

    @PreAuthorize("hasPermission(this.emeqmonitorService.getEmeqmonitorByIds(#ids),'eam-EMEQMonitor-Remove')")
    @ApiOperation(value = "批量删除设备状态监控", tags = {"设备状态监控" },  notes = "批量删除设备状态监控")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqmonitors/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqmonitorService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqmonitorMapping.toDomain(returnObject.body),'eam-EMEQMonitor-Get')")
    @ApiOperation(value = "获取设备状态监控", tags = {"设备状态监控" },  notes = "获取设备状态监控")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqmonitors/{emeqmonitor_id}")
    public ResponseEntity<EMEQMonitorDTO> get(@PathVariable("emeqmonitor_id") String emeqmonitor_id) {
        EMEQMonitor domain = emeqmonitorService.get(emeqmonitor_id);
        EMEQMonitorDTO dto = emeqmonitorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取设备状态监控草稿", tags = {"设备状态监控" },  notes = "获取设备状态监控草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqmonitors/getdraft")
    public ResponseEntity<EMEQMonitorDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emeqmonitorMapping.toDto(emeqmonitorService.getDraft(new EMEQMonitor())));
    }

    @ApiOperation(value = "检查设备状态监控", tags = {"设备状态监控" },  notes = "检查设备状态监控")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmonitors/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQMonitorDTO emeqmonitordto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqmonitorService.checkKey(emeqmonitorMapping.toDomain(emeqmonitordto)));
    }

    @PreAuthorize("hasPermission(this.emeqmonitorMapping.toDomain(#emeqmonitordto),'eam-EMEQMonitor-Save')")
    @ApiOperation(value = "保存设备状态监控", tags = {"设备状态监控" },  notes = "保存设备状态监控")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmonitors/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEQMonitorDTO emeqmonitordto) {
        return ResponseEntity.status(HttpStatus.OK).body(emeqmonitorService.save(emeqmonitorMapping.toDomain(emeqmonitordto)));
    }

    @PreAuthorize("hasPermission(this.emeqmonitorMapping.toDomain(#emeqmonitordtos),'eam-EMEQMonitor-Save')")
    @ApiOperation(value = "批量保存设备状态监控", tags = {"设备状态监控" },  notes = "批量保存设备状态监控")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmonitors/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQMonitorDTO> emeqmonitordtos) {
        emeqmonitorService.saveBatch(emeqmonitorMapping.toDomain(emeqmonitordtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMonitor-searchDefault-all') and hasPermission(#context,'eam-EMEQMonitor-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"设备状态监控" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqmonitors/fetchdefault")
	public ResponseEntity<List<EMEQMonitorDTO>> fetchDefault(EMEQMonitorSearchContext context) {
        Page<EMEQMonitor> domains = emeqmonitorService.searchDefault(context) ;
        List<EMEQMonitorDTO> list = emeqmonitorMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMonitor-searchDefault-all') and hasPermission(#context,'eam-EMEQMonitor-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"设备状态监控" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqmonitors/searchdefault")
	public ResponseEntity<Page<EMEQMonitorDTO>> searchDefault(@RequestBody EMEQMonitorSearchContext context) {
        Page<EMEQMonitor> domains = emeqmonitorService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqmonitorMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

