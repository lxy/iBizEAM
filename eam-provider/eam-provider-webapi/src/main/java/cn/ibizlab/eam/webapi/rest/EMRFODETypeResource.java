package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMRFODEType;
import cn.ibizlab.eam.core.eam_core.service.IEMRFODETypeService;
import cn.ibizlab.eam.core.eam_core.filter.EMRFODETypeSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"现象分类" })
@RestController("WebApi-emrfodetype")
@RequestMapping("")
public class EMRFODETypeResource {

    @Autowired
    public IEMRFODETypeService emrfodetypeService;

    @Autowired
    @Lazy
    public EMRFODETypeMapping emrfodetypeMapping;

    @PreAuthorize("hasPermission(this.emrfodetypeMapping.toDomain(#emrfodetypedto),'eam-EMRFODEType-Create')")
    @ApiOperation(value = "新建现象分类", tags = {"现象分类" },  notes = "新建现象分类")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodetypes")
    public ResponseEntity<EMRFODETypeDTO> create(@Validated @RequestBody EMRFODETypeDTO emrfodetypedto) {
        EMRFODEType domain = emrfodetypeMapping.toDomain(emrfodetypedto);
		emrfodetypeService.create(domain);
        EMRFODETypeDTO dto = emrfodetypeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfodetypeMapping.toDomain(#emrfodetypedtos),'eam-EMRFODEType-Create')")
    @ApiOperation(value = "批量新建现象分类", tags = {"现象分类" },  notes = "批量新建现象分类")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodetypes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMRFODETypeDTO> emrfodetypedtos) {
        emrfodetypeService.createBatch(emrfodetypeMapping.toDomain(emrfodetypedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emrfodetype" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emrfodetypeService.get(#emrfodetype_id),'eam-EMRFODEType-Update')")
    @ApiOperation(value = "更新现象分类", tags = {"现象分类" },  notes = "更新现象分类")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodetypes/{emrfodetype_id}")
    public ResponseEntity<EMRFODETypeDTO> update(@PathVariable("emrfodetype_id") String emrfodetype_id, @RequestBody EMRFODETypeDTO emrfodetypedto) {
		EMRFODEType domain  = emrfodetypeMapping.toDomain(emrfodetypedto);
        domain .setEmrfodetypeid(emrfodetype_id);
		emrfodetypeService.update(domain );
		EMRFODETypeDTO dto = emrfodetypeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfodetypeService.getEmrfodetypeByEntities(this.emrfodetypeMapping.toDomain(#emrfodetypedtos)),'eam-EMRFODEType-Update')")
    @ApiOperation(value = "批量更新现象分类", tags = {"现象分类" },  notes = "批量更新现象分类")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodetypes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMRFODETypeDTO> emrfodetypedtos) {
        emrfodetypeService.updateBatch(emrfodetypeMapping.toDomain(emrfodetypedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emrfodetypeService.get(#emrfodetype_id),'eam-EMRFODEType-Remove')")
    @ApiOperation(value = "删除现象分类", tags = {"现象分类" },  notes = "删除现象分类")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodetypes/{emrfodetype_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emrfodetype_id") String emrfodetype_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emrfodetypeService.remove(emrfodetype_id));
    }

    @PreAuthorize("hasPermission(this.emrfodetypeService.getEmrfodetypeByIds(#ids),'eam-EMRFODEType-Remove')")
    @ApiOperation(value = "批量删除现象分类", tags = {"现象分类" },  notes = "批量删除现象分类")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodetypes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emrfodetypeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emrfodetypeMapping.toDomain(returnObject.body),'eam-EMRFODEType-Get')")
    @ApiOperation(value = "获取现象分类", tags = {"现象分类" },  notes = "获取现象分类")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfodetypes/{emrfodetype_id}")
    public ResponseEntity<EMRFODETypeDTO> get(@PathVariable("emrfodetype_id") String emrfodetype_id) {
        EMRFODEType domain = emrfodetypeService.get(emrfodetype_id);
        EMRFODETypeDTO dto = emrfodetypeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取现象分类草稿", tags = {"现象分类" },  notes = "获取现象分类草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfodetypes/getdraft")
    public ResponseEntity<EMRFODETypeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emrfodetypeMapping.toDto(emrfodetypeService.getDraft(new EMRFODEType())));
    }

    @ApiOperation(value = "检查现象分类", tags = {"现象分类" },  notes = "检查现象分类")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodetypes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMRFODETypeDTO emrfodetypedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emrfodetypeService.checkKey(emrfodetypeMapping.toDomain(emrfodetypedto)));
    }

    @PreAuthorize("hasPermission(this.emrfodetypeMapping.toDomain(#emrfodetypedto),'eam-EMRFODEType-Save')")
    @ApiOperation(value = "保存现象分类", tags = {"现象分类" },  notes = "保存现象分类")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodetypes/save")
    public ResponseEntity<Boolean> save(@RequestBody EMRFODETypeDTO emrfodetypedto) {
        return ResponseEntity.status(HttpStatus.OK).body(emrfodetypeService.save(emrfodetypeMapping.toDomain(emrfodetypedto)));
    }

    @PreAuthorize("hasPermission(this.emrfodetypeMapping.toDomain(#emrfodetypedtos),'eam-EMRFODEType-Save')")
    @ApiOperation(value = "批量保存现象分类", tags = {"现象分类" },  notes = "批量保存现象分类")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodetypes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMRFODETypeDTO> emrfodetypedtos) {
        emrfodetypeService.saveBatch(emrfodetypeMapping.toDomain(emrfodetypedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFODEType-searchDefault-all') and hasPermission(#context,'eam-EMRFODEType-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"现象分类" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emrfodetypes/fetchdefault")
	public ResponseEntity<List<EMRFODETypeDTO>> fetchDefault(EMRFODETypeSearchContext context) {
        Page<EMRFODEType> domains = emrfodetypeService.searchDefault(context) ;
        List<EMRFODETypeDTO> list = emrfodetypeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFODEType-searchDefault-all') and hasPermission(#context,'eam-EMRFODEType-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"现象分类" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emrfodetypes/searchdefault")
	public ResponseEntity<Page<EMRFODETypeDTO>> searchDefault(@RequestBody EMRFODETypeSearchContext context) {
        Page<EMRFODEType> domains = emrfodetypeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emrfodetypeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

