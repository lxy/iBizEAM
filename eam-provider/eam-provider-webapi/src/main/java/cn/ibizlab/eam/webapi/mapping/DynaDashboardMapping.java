package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.r7rt_dyna.domain.DynaDashboard;
import cn.ibizlab.eam.webapi.dto.DynaDashboardDTO;
import cn.ibizlab.eam.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="WebApiDynaDashboardMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface DynaDashboardMapping extends MappingBase<DynaDashboardDTO, DynaDashboard> {


}

