package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWPListCost;
import cn.ibizlab.eam.core.eam_core.service.IEMWPListCostService;
import cn.ibizlab.eam.core.eam_core.filter.EMWPListCostSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"询价单" })
@RestController("WebApi-emwplistcost")
@RequestMapping("")
public class EMWPListCostResource {

    @Autowired
    public IEMWPListCostService emwplistcostService;

    @Autowired
    @Lazy
    public EMWPListCostMapping emwplistcostMapping;

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "新建询价单", tags = {"询价单" },  notes = "新建询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplistcosts")
    public ResponseEntity<EMWPListCostDTO> create(@Validated @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
		emwplistcostService.create(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "批量新建询价单", tags = {"询价单" },  notes = "批量新建询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplistcosts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        emwplistcostService.createBatch(emwplistcostMapping.toDomain(emwplistcostdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplistcost" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "更新询价单", tags = {"询价单" },  notes = "更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> update(@PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
		EMWPListCost domain  = emwplistcostMapping.toDomain(emwplistcostdto);
        domain .setEmwplistcostid(emwplistcost_id);
		emwplistcostService.update(domain );
		EMWPListCostDTO dto = emwplistcostMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByEntities(this.emwplistcostMapping.toDomain(#emwplistcostdtos)),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "批量更新询价单", tags = {"询价单" },  notes = "批量更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwplistcosts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        emwplistcostService.updateBatch(emwplistcostMapping.toDomain(emwplistcostdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "删除询价单", tags = {"询价单" },  notes = "删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emwplistcost_id") String emwplistcost_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.remove(emwplistcost_id));
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByIds(#ids),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "批量删除询价单", tags = {"询价单" },  notes = "批量删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwplistcosts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emwplistcostService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistcostMapping.toDomain(returnObject.body),'eam-EMWPListCost-Get')")
    @ApiOperation(value = "获取询价单", tags = {"询价单" },  notes = "获取询价单")
	@RequestMapping(method = RequestMethod.GET, value = "/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> get(@PathVariable("emwplistcost_id") String emwplistcost_id) {
        EMWPListCost domain = emwplistcostService.get(emwplistcost_id);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取询价单草稿", tags = {"询价单" },  notes = "获取询价单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emwplistcosts/getdraft")
    public ResponseEntity<EMWPListCostDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostMapping.toDto(emwplistcostService.getDraft(new EMWPListCost())));
    }

    @ApiOperation(value = "检查询价单", tags = {"询价单" },  notes = "检查询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplistcosts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMWPListCostDTO emwplistcostdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.checkKey(emwplistcostMapping.toDomain(emwplistcostdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-Confirm-all')")
    @ApiOperation(value = "确认询价", tags = {"询价单" },  notes = "确认询价")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplistcosts/{emwplistcost_id}/confirm")
    public ResponseEntity<EMWPListCostDTO> confirm(@PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setEmwplistcostid(emwplistcost_id);
        domain = emwplistcostService.confirm(domain);
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-FillItem-all')")
    @ApiOperation(value = "填充物品信息", tags = {"询价单" },  notes = "填充物品信息")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplistcosts/{emwplistcost_id}/fillitem")
    public ResponseEntity<EMWPListCostDTO> fillItem(@PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setEmwplistcostid(emwplistcost_id);
        domain = emwplistcostService.fillItem(domain);
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "保存询价单", tags = {"询价单" },  notes = "保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplistcosts/save")
    public ResponseEntity<Boolean> save(@RequestBody EMWPListCostDTO emwplistcostdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.save(emwplistcostMapping.toDomain(emwplistcostdto)));
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "批量保存询价单", tags = {"询价单" },  notes = "批量保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplistcosts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        emwplistcostService.saveBatch(emwplistcostMapping.toDomain(emwplistcostdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "获取CostByItem", tags = {"询价单" } ,notes = "获取CostByItem")
    @RequestMapping(method= RequestMethod.GET , value="/emwplistcosts/fetchcostbyitem")
	public ResponseEntity<List<EMWPListCostDTO>> fetchCostByItem(EMWPListCostSearchContext context) {
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "查询CostByItem", tags = {"询价单" } ,notes = "查询CostByItem")
    @RequestMapping(method= RequestMethod.POST , value="/emwplistcosts/searchcostbyitem")
	public ResponseEntity<Page<EMWPListCostDTO>> searchCostByItem(@RequestBody EMWPListCostSearchContext context) {
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"询价单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emwplistcosts/fetchdefault")
	public ResponseEntity<List<EMWPListCostDTO>> fetchDefault(EMWPListCostSearchContext context) {
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"询价单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emwplistcosts/searchdefault")
	public ResponseEntity<Page<EMWPListCostDTO>> searchDefault(@RequestBody EMWPListCostSearchContext context) {
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据物品建立询价单", tags = {"询价单" },  notes = "根据物品建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplistcosts")
    public ResponseEntity<EMWPListCostDTO> createByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
		emwplistcostService.create(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据物品批量建立询价单", tags = {"询价单" },  notes = "根据物品批量建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> createBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistcostService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplistcost" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据物品更新询价单", tags = {"询价单" },  notes = "根据物品更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> updateByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        domain.setEmwplistcostid(emwplistcost_id);
		emwplistcostService.update(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByEntities(this.emwplistcostMapping.toDomain(#emwplistcostdtos)),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据物品批量更新询价单", tags = {"询价单" },  notes = "根据物品批量更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> updateBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistcostService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据物品删除询价单", tags = {"询价单" },  notes = "根据物品删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<Boolean> removeByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.remove(emwplistcost_id));
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByIds(#ids),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据物品批量删除询价单", tags = {"询价单" },  notes = "根据物品批量删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> removeBatchByEMItem(@RequestBody List<String> ids) {
        emwplistcostService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistcostMapping.toDomain(returnObject.body),'eam-EMWPListCost-Get')")
    @ApiOperation(value = "根据物品获取询价单", tags = {"询价单" },  notes = "根据物品获取询价单")
	@RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> getByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
        EMWPListCost domain = emwplistcostService.get(emwplistcost_id);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据物品获取询价单草稿", tags = {"询价单" },  notes = "根据物品获取询价单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emwplistcosts/getdraft")
    public ResponseEntity<EMWPListCostDTO> getDraftByEMItem(@PathVariable("emitem_id") String emitem_id) {
        EMWPListCost domain = new EMWPListCost();
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostMapping.toDto(emwplistcostService.getDraft(domain)));
    }

    @ApiOperation(value = "根据物品检查询价单", tags = {"询价单" },  notes = "根据物品检查询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplistcosts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.checkKey(emwplistcostMapping.toDomain(emwplistcostdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-Confirm-all')")
    @ApiOperation(value = "根据物品询价单", tags = {"询价单" },  notes = "根据物品询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}/confirm")
    public ResponseEntity<EMWPListCostDTO> confirmByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        domain = emwplistcostService.confirm(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-FillItem-all')")
    @ApiOperation(value = "根据物品询价单", tags = {"询价单" },  notes = "根据物品询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}/fillitem")
    public ResponseEntity<EMWPListCostDTO> fillItemByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        domain = emwplistcostService.fillItem(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据物品保存询价单", tags = {"询价单" },  notes = "根据物品保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplistcosts/save")
    public ResponseEntity<Boolean> saveByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据物品批量保存询价单", tags = {"询价单" },  notes = "根据物品批量保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplistcosts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emwplistcostService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据物品获取CostByItem", tags = {"询价单" } ,notes = "根据物品获取CostByItem")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplistcosts/fetchcostbyitem")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostCostByItemByEMItem(@PathVariable("emitem_id") String emitem_id,EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据物品查询CostByItem", tags = {"询价单" } ,notes = "根据物品查询CostByItem")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplistcosts/searchcostbyitem")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostCostByItemByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据物品获取DEFAULT", tags = {"询价单" } ,notes = "根据物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplistcosts/fetchdefault")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostDefaultByEMItem(@PathVariable("emitem_id") String emitem_id,EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据物品查询DEFAULT", tags = {"询价单" } ,notes = "根据物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplistcosts/searchdefault")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostDefaultByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据服务商建立询价单", tags = {"询价单" },  notes = "根据服务商建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emwplistcosts")
    public ResponseEntity<EMWPListCostDTO> createByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setLabserviceid(emservice_id);
		emwplistcostService.create(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据服务商批量建立询价单", tags = {"询价单" },  notes = "根据服务商批量建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> createBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setLabserviceid(emservice_id);
        }
        emwplistcostService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplistcost" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据服务商更新询价单", tags = {"询价单" },  notes = "根据服务商更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> updateByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setLabserviceid(emservice_id);
        domain.setEmwplistcostid(emwplistcost_id);
		emwplistcostService.update(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByEntities(this.emwplistcostMapping.toDomain(#emwplistcostdtos)),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据服务商批量更新询价单", tags = {"询价单" },  notes = "根据服务商批量更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> updateBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setLabserviceid(emservice_id);
        }
        emwplistcostService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据服务商删除询价单", tags = {"询价单" },  notes = "根据服务商删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<Boolean> removeByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.remove(emwplistcost_id));
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByIds(#ids),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据服务商批量删除询价单", tags = {"询价单" },  notes = "根据服务商批量删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> removeBatchByEMService(@RequestBody List<String> ids) {
        emwplistcostService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistcostMapping.toDomain(returnObject.body),'eam-EMWPListCost-Get')")
    @ApiOperation(value = "根据服务商获取询价单", tags = {"询价单" },  notes = "根据服务商获取询价单")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> getByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
        EMWPListCost domain = emwplistcostService.get(emwplistcost_id);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商获取询价单草稿", tags = {"询价单" },  notes = "根据服务商获取询价单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emwplistcosts/getdraft")
    public ResponseEntity<EMWPListCostDTO> getDraftByEMService(@PathVariable("emservice_id") String emservice_id) {
        EMWPListCost domain = new EMWPListCost();
        domain.setLabserviceid(emservice_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostMapping.toDto(emwplistcostService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商检查询价单", tags = {"询价单" },  notes = "根据服务商检查询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emwplistcosts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.checkKey(emwplistcostMapping.toDomain(emwplistcostdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-Confirm-all')")
    @ApiOperation(value = "根据服务商询价单", tags = {"询价单" },  notes = "根据服务商询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emwplistcosts/{emwplistcost_id}/confirm")
    public ResponseEntity<EMWPListCostDTO> confirmByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setLabserviceid(emservice_id);
        domain = emwplistcostService.confirm(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-FillItem-all')")
    @ApiOperation(value = "根据服务商询价单", tags = {"询价单" },  notes = "根据服务商询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emwplistcosts/{emwplistcost_id}/fillitem")
    public ResponseEntity<EMWPListCostDTO> fillItemByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setLabserviceid(emservice_id);
        domain = emwplistcostService.fillItem(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据服务商保存询价单", tags = {"询价单" },  notes = "根据服务商保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emwplistcosts/save")
    public ResponseEntity<Boolean> saveByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setLabserviceid(emservice_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据服务商批量保存询价单", tags = {"询价单" },  notes = "根据服务商批量保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emwplistcosts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
             domain.setLabserviceid(emservice_id);
        }
        emwplistcostService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据服务商获取CostByItem", tags = {"询价单" } ,notes = "根据服务商获取CostByItem")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emwplistcosts/fetchcostbyitem")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostCostByItemByEMService(@PathVariable("emservice_id") String emservice_id,EMWPListCostSearchContext context) {
        context.setN_labserviceid_eq(emservice_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据服务商查询CostByItem", tags = {"询价单" } ,notes = "根据服务商查询CostByItem")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emwplistcosts/searchcostbyitem")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostCostByItemByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_labserviceid_eq(emservice_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据服务商获取DEFAULT", tags = {"询价单" } ,notes = "根据服务商获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emwplistcosts/fetchdefault")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostDefaultByEMService(@PathVariable("emservice_id") String emservice_id,EMWPListCostSearchContext context) {
        context.setN_labserviceid_eq(emservice_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据服务商查询DEFAULT", tags = {"询价单" } ,notes = "根据服务商查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emwplistcosts/searchdefault")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostDefaultByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_labserviceid_eq(emservice_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据采购申请建立询价单", tags = {"询价单" },  notes = "根据采购申请建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/emwplistcosts")
    public ResponseEntity<EMWPListCostDTO> createByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
		emwplistcostService.create(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据采购申请批量建立询价单", tags = {"询价单" },  notes = "根据采购申请批量建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> createBatchByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        emwplistcostService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplistcost" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据采购申请更新询价单", tags = {"询价单" },  notes = "根据采购申请更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> updateByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain.setEmwplistcostid(emwplistcost_id);
		emwplistcostService.update(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByEntities(this.emwplistcostMapping.toDomain(#emwplistcostdtos)),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据采购申请批量更新询价单", tags = {"询价单" },  notes = "根据采购申请批量更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> updateBatchByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        emwplistcostService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据采购申请删除询价单", tags = {"询价单" },  notes = "根据采购申请删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<Boolean> removeByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.remove(emwplistcost_id));
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByIds(#ids),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据采购申请批量删除询价单", tags = {"询价单" },  notes = "根据采购申请批量删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> removeBatchByEMWPList(@RequestBody List<String> ids) {
        emwplistcostService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistcostMapping.toDomain(returnObject.body),'eam-EMWPListCost-Get')")
    @ApiOperation(value = "根据采购申请获取询价单", tags = {"询价单" },  notes = "根据采购申请获取询价单")
	@RequestMapping(method = RequestMethod.GET, value = "/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> getByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
        EMWPListCost domain = emwplistcostService.get(emwplistcost_id);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据采购申请获取询价单草稿", tags = {"询价单" },  notes = "根据采购申请获取询价单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emwplists/{emwplist_id}/emwplistcosts/getdraft")
    public ResponseEntity<EMWPListCostDTO> getDraftByEMWPList(@PathVariable("emwplist_id") String emwplist_id) {
        EMWPListCost domain = new EMWPListCost();
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostMapping.toDto(emwplistcostService.getDraft(domain)));
    }

    @ApiOperation(value = "根据采购申请检查询价单", tags = {"询价单" },  notes = "根据采购申请检查询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/emwplistcosts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.checkKey(emwplistcostMapping.toDomain(emwplistcostdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-Confirm-all')")
    @ApiOperation(value = "根据采购申请询价单", tags = {"询价单" },  notes = "根据采购申请询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}/confirm")
    public ResponseEntity<EMWPListCostDTO> confirmByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain = emwplistcostService.confirm(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-FillItem-all')")
    @ApiOperation(value = "根据采购申请询价单", tags = {"询价单" },  notes = "根据采购申请询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}/fillitem")
    public ResponseEntity<EMWPListCostDTO> fillItemByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain = emwplistcostService.fillItem(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据采购申请保存询价单", tags = {"询价单" },  notes = "根据采购申请保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/emwplistcosts/save")
    public ResponseEntity<Boolean> saveByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据采购申请批量保存询价单", tags = {"询价单" },  notes = "根据采购申请批量保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/emwplistcosts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
             domain.setWplistid(emwplist_id);
        }
        emwplistcostService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据采购申请获取CostByItem", tags = {"询价单" } ,notes = "根据采购申请获取CostByItem")
    @RequestMapping(method= RequestMethod.GET , value="/emwplists/{emwplist_id}/emwplistcosts/fetchcostbyitem")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostCostByItemByEMWPList(@PathVariable("emwplist_id") String emwplist_id,EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据采购申请查询CostByItem", tags = {"询价单" } ,notes = "根据采购申请查询CostByItem")
    @RequestMapping(method= RequestMethod.POST , value="/emwplists/{emwplist_id}/emwplistcosts/searchcostbyitem")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostCostByItemByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据采购申请获取DEFAULT", tags = {"询价单" } ,notes = "根据采购申请获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emwplists/{emwplist_id}/emwplistcosts/fetchdefault")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostDefaultByEMWPList(@PathVariable("emwplist_id") String emwplist_id,EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据采购申请查询DEFAULT", tags = {"询价单" } ,notes = "根据采购申请查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emwplists/{emwplist_id}/emwplistcosts/searchdefault")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostDefaultByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据服务商物品建立询价单", tags = {"询价单" },  notes = "根据服务商物品建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplistcosts")
    public ResponseEntity<EMWPListCostDTO> createByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
		emwplistcostService.create(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据服务商物品批量建立询价单", tags = {"询价单" },  notes = "根据服务商物品批量建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistcostService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplistcost" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据服务商物品更新询价单", tags = {"询价单" },  notes = "根据服务商物品更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> updateByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        domain.setEmwplistcostid(emwplistcost_id);
		emwplistcostService.update(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByEntities(this.emwplistcostMapping.toDomain(#emwplistcostdtos)),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据服务商物品批量更新询价单", tags = {"询价单" },  notes = "根据服务商物品批量更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistcostService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据服务商物品删除询价单", tags = {"询价单" },  notes = "根据服务商物品删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.remove(emwplistcost_id));
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByIds(#ids),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据服务商物品批量删除询价单", tags = {"询价单" },  notes = "根据服务商物品批量删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMItem(@RequestBody List<String> ids) {
        emwplistcostService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistcostMapping.toDomain(returnObject.body),'eam-EMWPListCost-Get')")
    @ApiOperation(value = "根据服务商物品获取询价单", tags = {"询价单" },  notes = "根据服务商物品获取询价单")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> getByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
        EMWPListCost domain = emwplistcostService.get(emwplistcost_id);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商物品获取询价单草稿", tags = {"询价单" },  notes = "根据服务商物品获取询价单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplistcosts/getdraft")
    public ResponseEntity<EMWPListCostDTO> getDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id) {
        EMWPListCost domain = new EMWPListCost();
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostMapping.toDto(emwplistcostService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商物品检查询价单", tags = {"询价单" },  notes = "根据服务商物品检查询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplistcosts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.checkKey(emwplistcostMapping.toDomain(emwplistcostdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-Confirm-all')")
    @ApiOperation(value = "根据服务商物品询价单", tags = {"询价单" },  notes = "根据服务商物品询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}/confirm")
    public ResponseEntity<EMWPListCostDTO> confirmByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        domain = emwplistcostService.confirm(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-FillItem-all')")
    @ApiOperation(value = "根据服务商物品询价单", tags = {"询价单" },  notes = "根据服务商物品询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}/fillitem")
    public ResponseEntity<EMWPListCostDTO> fillItemByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        domain = emwplistcostService.fillItem(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据服务商物品保存询价单", tags = {"询价单" },  notes = "根据服务商物品保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplistcosts/save")
    public ResponseEntity<Boolean> saveByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据服务商物品批量保存询价单", tags = {"询价单" },  notes = "根据服务商物品批量保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplistcosts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emwplistcostService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据服务商物品获取CostByItem", tags = {"询价单" } ,notes = "根据服务商物品获取CostByItem")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplistcosts/fetchcostbyitem")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostCostByItemByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据服务商物品查询CostByItem", tags = {"询价单" } ,notes = "根据服务商物品查询CostByItem")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplistcosts/searchcostbyitem")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostCostByItemByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据服务商物品获取DEFAULT", tags = {"询价单" } ,notes = "根据服务商物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplistcosts/fetchdefault")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据服务商物品查询DEFAULT", tags = {"询价单" } ,notes = "根据服务商物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplistcosts/searchdefault")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据仓库库位物品建立询价单", tags = {"询价单" },  notes = "根据仓库库位物品建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts")
    public ResponseEntity<EMWPListCostDTO> createByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
		emwplistcostService.create(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据仓库库位物品批量建立询价单", tags = {"询价单" },  notes = "根据仓库库位物品批量建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> createBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistcostService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplistcost" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据仓库库位物品更新询价单", tags = {"询价单" },  notes = "根据仓库库位物品更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> updateByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        domain.setEmwplistcostid(emwplistcost_id);
		emwplistcostService.update(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByEntities(this.emwplistcostMapping.toDomain(#emwplistcostdtos)),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据仓库库位物品批量更新询价单", tags = {"询价单" },  notes = "根据仓库库位物品批量更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> updateBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistcostService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据仓库库位物品删除询价单", tags = {"询价单" },  notes = "根据仓库库位物品删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<Boolean> removeByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.remove(emwplistcost_id));
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByIds(#ids),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据仓库库位物品批量删除询价单", tags = {"询价单" },  notes = "根据仓库库位物品批量删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> removeBatchByEMStorePartEMItem(@RequestBody List<String> ids) {
        emwplistcostService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistcostMapping.toDomain(returnObject.body),'eam-EMWPListCost-Get')")
    @ApiOperation(value = "根据仓库库位物品获取询价单", tags = {"询价单" },  notes = "根据仓库库位物品获取询价单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> getByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
        EMWPListCost domain = emwplistcostService.get(emwplistcost_id);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库库位物品获取询价单草稿", tags = {"询价单" },  notes = "根据仓库库位物品获取询价单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/getdraft")
    public ResponseEntity<EMWPListCostDTO> getDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id) {
        EMWPListCost domain = new EMWPListCost();
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostMapping.toDto(emwplistcostService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库库位物品检查询价单", tags = {"询价单" },  notes = "根据仓库库位物品检查询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.checkKey(emwplistcostMapping.toDomain(emwplistcostdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-Confirm-all')")
    @ApiOperation(value = "根据仓库库位物品询价单", tags = {"询价单" },  notes = "根据仓库库位物品询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}/confirm")
    public ResponseEntity<EMWPListCostDTO> confirmByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        domain = emwplistcostService.confirm(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-FillItem-all')")
    @ApiOperation(value = "根据仓库库位物品询价单", tags = {"询价单" },  notes = "根据仓库库位物品询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}/fillitem")
    public ResponseEntity<EMWPListCostDTO> fillItemByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        domain = emwplistcostService.fillItem(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据仓库库位物品保存询价单", tags = {"询价单" },  notes = "根据仓库库位物品保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/save")
    public ResponseEntity<Boolean> saveByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据仓库库位物品批量保存询价单", tags = {"询价单" },  notes = "根据仓库库位物品批量保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emwplistcostService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库库位物品获取CostByItem", tags = {"询价单" } ,notes = "根据仓库库位物品获取CostByItem")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/fetchcostbyitem")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostCostByItemByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库库位物品查询CostByItem", tags = {"询价单" } ,notes = "根据仓库库位物品查询CostByItem")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/searchcostbyitem")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostCostByItemByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库库位物品获取DEFAULT", tags = {"询价单" } ,notes = "根据仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/fetchdefault")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库库位物品查询DEFAULT", tags = {"询价单" } ,notes = "根据仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/searchdefault")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据仓库物品建立询价单", tags = {"询价单" },  notes = "根据仓库物品建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplistcosts")
    public ResponseEntity<EMWPListCostDTO> createByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
		emwplistcostService.create(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据仓库物品批量建立询价单", tags = {"询价单" },  notes = "根据仓库物品批量建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistcostService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplistcost" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据仓库物品更新询价单", tags = {"询价单" },  notes = "根据仓库物品更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> updateByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        domain.setEmwplistcostid(emwplistcost_id);
		emwplistcostService.update(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByEntities(this.emwplistcostMapping.toDomain(#emwplistcostdtos)),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据仓库物品批量更新询价单", tags = {"询价单" },  notes = "根据仓库物品批量更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistcostService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据仓库物品删除询价单", tags = {"询价单" },  notes = "根据仓库物品删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.remove(emwplistcost_id));
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByIds(#ids),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据仓库物品批量删除询价单", tags = {"询价单" },  notes = "根据仓库物品批量删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMItem(@RequestBody List<String> ids) {
        emwplistcostService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistcostMapping.toDomain(returnObject.body),'eam-EMWPListCost-Get')")
    @ApiOperation(value = "根据仓库物品获取询价单", tags = {"询价单" },  notes = "根据仓库物品获取询价单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> getByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
        EMWPListCost domain = emwplistcostService.get(emwplistcost_id);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库物品获取询价单草稿", tags = {"询价单" },  notes = "根据仓库物品获取询价单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplistcosts/getdraft")
    public ResponseEntity<EMWPListCostDTO> getDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id) {
        EMWPListCost domain = new EMWPListCost();
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostMapping.toDto(emwplistcostService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库物品检查询价单", tags = {"询价单" },  notes = "根据仓库物品检查询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplistcosts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.checkKey(emwplistcostMapping.toDomain(emwplistcostdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-Confirm-all')")
    @ApiOperation(value = "根据仓库物品询价单", tags = {"询价单" },  notes = "根据仓库物品询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}/confirm")
    public ResponseEntity<EMWPListCostDTO> confirmByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        domain = emwplistcostService.confirm(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-FillItem-all')")
    @ApiOperation(value = "根据仓库物品询价单", tags = {"询价单" },  notes = "根据仓库物品询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}/fillitem")
    public ResponseEntity<EMWPListCostDTO> fillItemByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        domain = emwplistcostService.fillItem(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据仓库物品保存询价单", tags = {"询价单" },  notes = "根据仓库物品保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplistcosts/save")
    public ResponseEntity<Boolean> saveByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据仓库物品批量保存询价单", tags = {"询价单" },  notes = "根据仓库物品批量保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplistcosts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emwplistcostService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库物品获取CostByItem", tags = {"询价单" } ,notes = "根据仓库物品获取CostByItem")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplistcosts/fetchcostbyitem")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostCostByItemByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库物品查询CostByItem", tags = {"询价单" } ,notes = "根据仓库物品查询CostByItem")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplistcosts/searchcostbyitem")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostCostByItemByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库物品获取DEFAULT", tags = {"询价单" } ,notes = "根据仓库物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplistcosts/fetchdefault")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库物品查询DEFAULT", tags = {"询价单" } ,notes = "根据仓库物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplistcosts/searchdefault")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据物品采购申请建立询价单", tags = {"询价单" },  notes = "根据物品采购申请建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts")
    public ResponseEntity<EMWPListCostDTO> createByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
		emwplistcostService.create(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据物品采购申请批量建立询价单", tags = {"询价单" },  notes = "根据物品采购申请批量建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> createBatchByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        emwplistcostService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplistcost" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据物品采购申请更新询价单", tags = {"询价单" },  notes = "根据物品采购申请更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> updateByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain.setEmwplistcostid(emwplistcost_id);
		emwplistcostService.update(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByEntities(this.emwplistcostMapping.toDomain(#emwplistcostdtos)),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据物品采购申请批量更新询价单", tags = {"询价单" },  notes = "根据物品采购申请批量更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> updateBatchByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        emwplistcostService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据物品采购申请删除询价单", tags = {"询价单" },  notes = "根据物品采购申请删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<Boolean> removeByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.remove(emwplistcost_id));
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByIds(#ids),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据物品采购申请批量删除询价单", tags = {"询价单" },  notes = "根据物品采购申请批量删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> removeBatchByEMItemEMWPList(@RequestBody List<String> ids) {
        emwplistcostService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistcostMapping.toDomain(returnObject.body),'eam-EMWPListCost-Get')")
    @ApiOperation(value = "根据物品采购申请获取询价单", tags = {"询价单" },  notes = "根据物品采购申请获取询价单")
	@RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> getByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
        EMWPListCost domain = emwplistcostService.get(emwplistcost_id);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据物品采购申请获取询价单草稿", tags = {"询价单" },  notes = "根据物品采购申请获取询价单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/getdraft")
    public ResponseEntity<EMWPListCostDTO> getDraftByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
        EMWPListCost domain = new EMWPListCost();
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostMapping.toDto(emwplistcostService.getDraft(domain)));
    }

    @ApiOperation(value = "根据物品采购申请检查询价单", tags = {"询价单" },  notes = "根据物品采购申请检查询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.checkKey(emwplistcostMapping.toDomain(emwplistcostdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-Confirm-all')")
    @ApiOperation(value = "根据物品采购申请询价单", tags = {"询价单" },  notes = "根据物品采购申请询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}/confirm")
    public ResponseEntity<EMWPListCostDTO> confirmByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain = emwplistcostService.confirm(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-FillItem-all')")
    @ApiOperation(value = "根据物品采购申请询价单", tags = {"询价单" },  notes = "根据物品采购申请询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}/fillitem")
    public ResponseEntity<EMWPListCostDTO> fillItemByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain = emwplistcostService.fillItem(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据物品采购申请保存询价单", tags = {"询价单" },  notes = "根据物品采购申请保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/save")
    public ResponseEntity<Boolean> saveByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据物品采购申请批量保存询价单", tags = {"询价单" },  notes = "根据物品采购申请批量保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
             domain.setWplistid(emwplist_id);
        }
        emwplistcostService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据物品采购申请获取CostByItem", tags = {"询价单" } ,notes = "根据物品采购申请获取CostByItem")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/fetchcostbyitem")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostCostByItemByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据物品采购申请查询CostByItem", tags = {"询价单" } ,notes = "根据物品采购申请查询CostByItem")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/searchcostbyitem")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostCostByItemByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据物品采购申请获取DEFAULT", tags = {"询价单" } ,notes = "根据物品采购申请获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/fetchdefault")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostDefaultByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据物品采购申请查询DEFAULT", tags = {"询价单" } ,notes = "根据物品采购申请查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/searchdefault")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostDefaultByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据服务商物品采购申请建立询价单", tags = {"询价单" },  notes = "根据服务商物品采购申请建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts")
    public ResponseEntity<EMWPListCostDTO> createByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
		emwplistcostService.create(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据服务商物品采购申请批量建立询价单", tags = {"询价单" },  notes = "根据服务商物品采购申请批量建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        emwplistcostService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplistcost" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据服务商物品采购申请更新询价单", tags = {"询价单" },  notes = "根据服务商物品采购申请更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> updateByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain.setEmwplistcostid(emwplistcost_id);
		emwplistcostService.update(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByEntities(this.emwplistcostMapping.toDomain(#emwplistcostdtos)),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据服务商物品采购申请批量更新询价单", tags = {"询价单" },  notes = "根据服务商物品采购申请批量更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        emwplistcostService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据服务商物品采购申请删除询价单", tags = {"询价单" },  notes = "根据服务商物品采购申请删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.remove(emwplistcost_id));
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByIds(#ids),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据服务商物品采购申请批量删除询价单", tags = {"询价单" },  notes = "根据服务商物品采购申请批量删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMItemEMWPList(@RequestBody List<String> ids) {
        emwplistcostService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistcostMapping.toDomain(returnObject.body),'eam-EMWPListCost-Get')")
    @ApiOperation(value = "根据服务商物品采购申请获取询价单", tags = {"询价单" },  notes = "根据服务商物品采购申请获取询价单")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> getByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
        EMWPListCost domain = emwplistcostService.get(emwplistcost_id);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商物品采购申请获取询价单草稿", tags = {"询价单" },  notes = "根据服务商物品采购申请获取询价单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/getdraft")
    public ResponseEntity<EMWPListCostDTO> getDraftByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
        EMWPListCost domain = new EMWPListCost();
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostMapping.toDto(emwplistcostService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商物品采购申请检查询价单", tags = {"询价单" },  notes = "根据服务商物品采购申请检查询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.checkKey(emwplistcostMapping.toDomain(emwplistcostdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-Confirm-all')")
    @ApiOperation(value = "根据服务商物品采购申请询价单", tags = {"询价单" },  notes = "根据服务商物品采购申请询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}/confirm")
    public ResponseEntity<EMWPListCostDTO> confirmByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain = emwplistcostService.confirm(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-FillItem-all')")
    @ApiOperation(value = "根据服务商物品采购申请询价单", tags = {"询价单" },  notes = "根据服务商物品采购申请询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}/fillitem")
    public ResponseEntity<EMWPListCostDTO> fillItemByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain = emwplistcostService.fillItem(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据服务商物品采购申请保存询价单", tags = {"询价单" },  notes = "根据服务商物品采购申请保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/save")
    public ResponseEntity<Boolean> saveByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据服务商物品采购申请批量保存询价单", tags = {"询价单" },  notes = "根据服务商物品采购申请批量保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
             domain.setWplistid(emwplist_id);
        }
        emwplistcostService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据服务商物品采购申请获取CostByItem", tags = {"询价单" } ,notes = "根据服务商物品采购申请获取CostByItem")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/fetchcostbyitem")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostCostByItemByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据服务商物品采购申请查询CostByItem", tags = {"询价单" } ,notes = "根据服务商物品采购申请查询CostByItem")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/searchcostbyitem")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostCostByItemByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据服务商物品采购申请获取DEFAULT", tags = {"询价单" } ,notes = "根据服务商物品采购申请获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/fetchdefault")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostDefaultByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据服务商物品采购申请查询DEFAULT", tags = {"询价单" } ,notes = "根据服务商物品采购申请查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/searchdefault")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostDefaultByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据仓库库位物品采购申请建立询价单", tags = {"询价单" },  notes = "根据仓库库位物品采购申请建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts")
    public ResponseEntity<EMWPListCostDTO> createByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
		emwplistcostService.create(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据仓库库位物品采购申请批量建立询价单", tags = {"询价单" },  notes = "根据仓库库位物品采购申请批量建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> createBatchByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        emwplistcostService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplistcost" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据仓库库位物品采购申请更新询价单", tags = {"询价单" },  notes = "根据仓库库位物品采购申请更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> updateByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain.setEmwplistcostid(emwplistcost_id);
		emwplistcostService.update(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByEntities(this.emwplistcostMapping.toDomain(#emwplistcostdtos)),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据仓库库位物品采购申请批量更新询价单", tags = {"询价单" },  notes = "根据仓库库位物品采购申请批量更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> updateBatchByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        emwplistcostService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据仓库库位物品采购申请删除询价单", tags = {"询价单" },  notes = "根据仓库库位物品采购申请删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<Boolean> removeByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.remove(emwplistcost_id));
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByIds(#ids),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据仓库库位物品采购申请批量删除询价单", tags = {"询价单" },  notes = "根据仓库库位物品采购申请批量删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> removeBatchByEMStorePartEMItemEMWPList(@RequestBody List<String> ids) {
        emwplistcostService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistcostMapping.toDomain(returnObject.body),'eam-EMWPListCost-Get')")
    @ApiOperation(value = "根据仓库库位物品采购申请获取询价单", tags = {"询价单" },  notes = "根据仓库库位物品采购申请获取询价单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> getByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
        EMWPListCost domain = emwplistcostService.get(emwplistcost_id);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库库位物品采购申请获取询价单草稿", tags = {"询价单" },  notes = "根据仓库库位物品采购申请获取询价单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/getdraft")
    public ResponseEntity<EMWPListCostDTO> getDraftByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
        EMWPListCost domain = new EMWPListCost();
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostMapping.toDto(emwplistcostService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库库位物品采购申请检查询价单", tags = {"询价单" },  notes = "根据仓库库位物品采购申请检查询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.checkKey(emwplistcostMapping.toDomain(emwplistcostdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-Confirm-all')")
    @ApiOperation(value = "根据仓库库位物品采购申请询价单", tags = {"询价单" },  notes = "根据仓库库位物品采购申请询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}/confirm")
    public ResponseEntity<EMWPListCostDTO> confirmByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain = emwplistcostService.confirm(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-FillItem-all')")
    @ApiOperation(value = "根据仓库库位物品采购申请询价单", tags = {"询价单" },  notes = "根据仓库库位物品采购申请询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}/fillitem")
    public ResponseEntity<EMWPListCostDTO> fillItemByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain = emwplistcostService.fillItem(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据仓库库位物品采购申请保存询价单", tags = {"询价单" },  notes = "根据仓库库位物品采购申请保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/save")
    public ResponseEntity<Boolean> saveByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据仓库库位物品采购申请批量保存询价单", tags = {"询价单" },  notes = "根据仓库库位物品采购申请批量保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
             domain.setWplistid(emwplist_id);
        }
        emwplistcostService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库库位物品采购申请获取CostByItem", tags = {"询价单" } ,notes = "根据仓库库位物品采购申请获取CostByItem")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/fetchcostbyitem")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostCostByItemByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库库位物品采购申请查询CostByItem", tags = {"询价单" } ,notes = "根据仓库库位物品采购申请查询CostByItem")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/searchcostbyitem")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostCostByItemByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库库位物品采购申请获取DEFAULT", tags = {"询价单" } ,notes = "根据仓库库位物品采购申请获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/fetchdefault")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostDefaultByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库库位物品采购申请查询DEFAULT", tags = {"询价单" } ,notes = "根据仓库库位物品采购申请查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/searchdefault")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostDefaultByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据仓库物品采购申请建立询价单", tags = {"询价单" },  notes = "根据仓库物品采购申请建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts")
    public ResponseEntity<EMWPListCostDTO> createByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
		emwplistcostService.create(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据仓库物品采购申请批量建立询价单", tags = {"询价单" },  notes = "根据仓库物品采购申请批量建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        emwplistcostService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplistcost" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据仓库物品采购申请更新询价单", tags = {"询价单" },  notes = "根据仓库物品采购申请更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> updateByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain.setEmwplistcostid(emwplistcost_id);
		emwplistcostService.update(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByEntities(this.emwplistcostMapping.toDomain(#emwplistcostdtos)),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据仓库物品采购申请批量更新询价单", tags = {"询价单" },  notes = "根据仓库物品采购申请批量更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        emwplistcostService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据仓库物品采购申请删除询价单", tags = {"询价单" },  notes = "根据仓库物品采购申请删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.remove(emwplistcost_id));
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByIds(#ids),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据仓库物品采购申请批量删除询价单", tags = {"询价单" },  notes = "根据仓库物品采购申请批量删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMItemEMWPList(@RequestBody List<String> ids) {
        emwplistcostService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistcostMapping.toDomain(returnObject.body),'eam-EMWPListCost-Get')")
    @ApiOperation(value = "根据仓库物品采购申请获取询价单", tags = {"询价单" },  notes = "根据仓库物品采购申请获取询价单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> getByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
        EMWPListCost domain = emwplistcostService.get(emwplistcost_id);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库物品采购申请获取询价单草稿", tags = {"询价单" },  notes = "根据仓库物品采购申请获取询价单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/getdraft")
    public ResponseEntity<EMWPListCostDTO> getDraftByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
        EMWPListCost domain = new EMWPListCost();
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostMapping.toDto(emwplistcostService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库物品采购申请检查询价单", tags = {"询价单" },  notes = "根据仓库物品采购申请检查询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.checkKey(emwplistcostMapping.toDomain(emwplistcostdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-Confirm-all')")
    @ApiOperation(value = "根据仓库物品采购申请询价单", tags = {"询价单" },  notes = "根据仓库物品采购申请询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}/confirm")
    public ResponseEntity<EMWPListCostDTO> confirmByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain = emwplistcostService.confirm(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-FillItem-all')")
    @ApiOperation(value = "根据仓库物品采购申请询价单", tags = {"询价单" },  notes = "根据仓库物品采购申请询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}/fillitem")
    public ResponseEntity<EMWPListCostDTO> fillItemByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain = emwplistcostService.fillItem(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据仓库物品采购申请保存询价单", tags = {"询价单" },  notes = "根据仓库物品采购申请保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/save")
    public ResponseEntity<Boolean> saveByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据仓库物品采购申请批量保存询价单", tags = {"询价单" },  notes = "根据仓库物品采购申请批量保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
             domain.setWplistid(emwplist_id);
        }
        emwplistcostService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库物品采购申请获取CostByItem", tags = {"询价单" } ,notes = "根据仓库物品采购申请获取CostByItem")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/fetchcostbyitem")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostCostByItemByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库物品采购申请查询CostByItem", tags = {"询价单" } ,notes = "根据仓库物品采购申请查询CostByItem")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/searchcostbyitem")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostCostByItemByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库物品采购申请获取DEFAULT", tags = {"询价单" } ,notes = "根据仓库物品采购申请获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/fetchdefault")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostDefaultByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库物品采购申请查询DEFAULT", tags = {"询价单" } ,notes = "根据仓库物品采购申请查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/searchdefault")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostDefaultByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品建立询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts")
    public ResponseEntity<EMWPListCostDTO> createByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
		emwplistcostService.create(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品批量建立询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品批量建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistcostService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplistcost" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品更新询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> updateByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        domain.setEmwplistcostid(emwplistcost_id);
		emwplistcostService.update(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByEntities(this.emwplistcostMapping.toDomain(#emwplistcostdtos)),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品批量更新询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品批量更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistcostService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品删除询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.remove(emwplistcost_id));
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByIds(#ids),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品批量删除询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品批量删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMStorePartEMItem(@RequestBody List<String> ids) {
        emwplistcostService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistcostMapping.toDomain(returnObject.body),'eam-EMWPListCost-Get')")
    @ApiOperation(value = "根据仓库仓库库位物品获取询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品获取询价单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> getByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
        EMWPListCost domain = emwplistcostService.get(emwplistcost_id);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库仓库库位物品获取询价单草稿", tags = {"询价单" },  notes = "根据仓库仓库库位物品获取询价单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/getdraft")
    public ResponseEntity<EMWPListCostDTO> getDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id) {
        EMWPListCost domain = new EMWPListCost();
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostMapping.toDto(emwplistcostService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库仓库库位物品检查询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品检查询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.checkKey(emwplistcostMapping.toDomain(emwplistcostdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-Confirm-all')")
    @ApiOperation(value = "根据仓库仓库库位物品询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}/confirm")
    public ResponseEntity<EMWPListCostDTO> confirmByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        domain = emwplistcostService.confirm(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-FillItem-all')")
    @ApiOperation(value = "根据仓库仓库库位物品询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/{emwplistcost_id}/fillitem")
    public ResponseEntity<EMWPListCostDTO> fillItemByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        domain = emwplistcostService.fillItem(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品保存询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/save")
    public ResponseEntity<Boolean> saveByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品批量保存询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品批量保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emwplistcostService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取CostByItem", tags = {"询价单" } ,notes = "根据仓库仓库库位物品获取CostByItem")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/fetchcostbyitem")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostCostByItemByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询CostByItem", tags = {"询价单" } ,notes = "根据仓库仓库库位物品查询CostByItem")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/searchcostbyitem")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostCostByItemByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取DEFAULT", tags = {"询价单" } ,notes = "根据仓库仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/fetchdefault")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询DEFAULT", tags = {"询价单" } ,notes = "根据仓库仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplistcosts/searchdefault")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请建立询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品采购申请建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts")
    public ResponseEntity<EMWPListCostDTO> createByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
		emwplistcostService.create(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请批量建立询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品采购申请批量建立询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        emwplistcostService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplistcost" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请更新询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品采购申请更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> updateByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain.setEmwplistcostid(emwplistcost_id);
		emwplistcostService.update(domain);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByEntities(this.emwplistcostMapping.toDomain(#emwplistcostdtos)),'eam-EMWPListCost-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请批量更新询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品采购申请批量更新询价单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        emwplistcostService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.get(#emwplistcost_id),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请删除询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品采购申请删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.remove(emwplistcost_id));
    }

    @PreAuthorize("hasPermission(this.emwplistcostService.getEmwplistcostByIds(#ids),'eam-EMWPListCost-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请批量删除询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品采购申请批量删除询价单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMStorePartEMItemEMWPList(@RequestBody List<String> ids) {
        emwplistcostService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistcostMapping.toDomain(returnObject.body),'eam-EMWPListCost-Get')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请获取询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品采购申请获取询价单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}")
    public ResponseEntity<EMWPListCostDTO> getByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id) {
        EMWPListCost domain = emwplistcostService.get(emwplistcost_id);
        EMWPListCostDTO dto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库仓库库位物品采购申请获取询价单草稿", tags = {"询价单" },  notes = "根据仓库仓库库位物品采购申请获取询价单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/getdraft")
    public ResponseEntity<EMWPListCostDTO> getDraftByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
        EMWPListCost domain = new EMWPListCost();
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostMapping.toDto(emwplistcostService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库仓库库位物品采购申请检查询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品采购申请检查询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.checkKey(emwplistcostMapping.toDomain(emwplistcostdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-Confirm-all')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品采购申请询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}/confirm")
    public ResponseEntity<EMWPListCostDTO> confirmByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain = emwplistcostService.confirm(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-FillItem-all')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品采购申请询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/{emwplistcost_id}/fillitem")
    public ResponseEntity<EMWPListCostDTO> fillItemByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("emwplistcost_id") String emwplistcost_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        domain = emwplistcostService.fillItem(domain) ;
        emwplistcostdto = emwplistcostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostdto);
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdto),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请保存询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品采购申请保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/save")
    public ResponseEntity<Boolean> saveByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostDTO emwplistcostdto) {
        EMWPListCost domain = emwplistcostMapping.toDomain(emwplistcostdto);
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistcostService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistcostMapping.toDomain(#emwplistcostdtos),'eam-EMWPListCost-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请批量保存询价单", tags = {"询价单" },  notes = "根据仓库仓库库位物品采购申请批量保存询价单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMWPListCostDTO> emwplistcostdtos) {
        List<EMWPListCost> domainlist=emwplistcostMapping.toDomain(emwplistcostdtos);
        for(EMWPListCost domain:domainlist){
             domain.setWplistid(emwplist_id);
        }
        emwplistcostService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品采购申请获取CostByItem", tags = {"询价单" } ,notes = "根据仓库仓库库位物品采购申请获取CostByItem")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/fetchcostbyitem")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostCostByItemByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchCostByItem-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品采购申请查询CostByItem", tags = {"询价单" } ,notes = "根据仓库仓库库位物品采购申请查询CostByItem")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/searchcostbyitem")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostCostByItemByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchCostByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品采购申请获取DEFAULT", tags = {"询价单" } ,notes = "根据仓库仓库库位物品采购申请获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/fetchdefault")
	public ResponseEntity<List<EMWPListCostDTO>> fetchEMWPListCostDefaultByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
        List<EMWPListCostDTO> list = emwplistcostMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPListCost-searchDefault-all') and hasPermission(#context,'eam-EMWPListCost-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品采购申请查询DEFAULT", tags = {"询价单" } ,notes = "根据仓库仓库库位物品采购申请查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/emwplistcosts/searchdefault")
	public ResponseEntity<Page<EMWPListCostDTO>> searchEMWPListCostDefaultByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListCostSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMWPListCost> domains = emwplistcostService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistcostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

