package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMCab;
import cn.ibizlab.eam.core.eam_core.service.IEMCabService;
import cn.ibizlab.eam.core.eam_core.filter.EMCabSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"货架" })
@RestController("WebApi-emcab")
@RequestMapping("")
public class EMCabResource {

    @Autowired
    public IEMCabService emcabService;

    @Autowired
    @Lazy
    public EMCabMapping emcabMapping;

    @PreAuthorize("hasPermission(this.emcabMapping.toDomain(#emcabdto),'eam-EMCab-Create')")
    @ApiOperation(value = "新建货架", tags = {"货架" },  notes = "新建货架")
	@RequestMapping(method = RequestMethod.POST, value = "/emcabs")
    public ResponseEntity<EMCabDTO> create(@Validated @RequestBody EMCabDTO emcabdto) {
        EMCab domain = emcabMapping.toDomain(emcabdto);
		emcabService.create(domain);
        EMCabDTO dto = emcabMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emcabMapping.toDomain(#emcabdtos),'eam-EMCab-Create')")
    @ApiOperation(value = "批量新建货架", tags = {"货架" },  notes = "批量新建货架")
	@RequestMapping(method = RequestMethod.POST, value = "/emcabs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMCabDTO> emcabdtos) {
        emcabService.createBatch(emcabMapping.toDomain(emcabdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emcab" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emcabService.get(#emcab_id),'eam-EMCab-Update')")
    @ApiOperation(value = "更新货架", tags = {"货架" },  notes = "更新货架")
	@RequestMapping(method = RequestMethod.PUT, value = "/emcabs/{emcab_id}")
    public ResponseEntity<EMCabDTO> update(@PathVariable("emcab_id") String emcab_id, @RequestBody EMCabDTO emcabdto) {
		EMCab domain  = emcabMapping.toDomain(emcabdto);
        domain .setEmcabid(emcab_id);
		emcabService.update(domain );
		EMCabDTO dto = emcabMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emcabService.getEmcabByEntities(this.emcabMapping.toDomain(#emcabdtos)),'eam-EMCab-Update')")
    @ApiOperation(value = "批量更新货架", tags = {"货架" },  notes = "批量更新货架")
	@RequestMapping(method = RequestMethod.PUT, value = "/emcabs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMCabDTO> emcabdtos) {
        emcabService.updateBatch(emcabMapping.toDomain(emcabdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emcabService.get(#emcab_id),'eam-EMCab-Remove')")
    @ApiOperation(value = "删除货架", tags = {"货架" },  notes = "删除货架")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emcabs/{emcab_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emcab_id") String emcab_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emcabService.remove(emcab_id));
    }

    @PreAuthorize("hasPermission(this.emcabService.getEmcabByIds(#ids),'eam-EMCab-Remove')")
    @ApiOperation(value = "批量删除货架", tags = {"货架" },  notes = "批量删除货架")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emcabs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emcabService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emcabMapping.toDomain(returnObject.body),'eam-EMCab-Get')")
    @ApiOperation(value = "获取货架", tags = {"货架" },  notes = "获取货架")
	@RequestMapping(method = RequestMethod.GET, value = "/emcabs/{emcab_id}")
    public ResponseEntity<EMCabDTO> get(@PathVariable("emcab_id") String emcab_id) {
        EMCab domain = emcabService.get(emcab_id);
        EMCabDTO dto = emcabMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取货架草稿", tags = {"货架" },  notes = "获取货架草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emcabs/getdraft")
    public ResponseEntity<EMCabDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emcabMapping.toDto(emcabService.getDraft(new EMCab())));
    }

    @ApiOperation(value = "检查货架", tags = {"货架" },  notes = "检查货架")
	@RequestMapping(method = RequestMethod.POST, value = "/emcabs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMCabDTO emcabdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emcabService.checkKey(emcabMapping.toDomain(emcabdto)));
    }

    @PreAuthorize("hasPermission(this.emcabMapping.toDomain(#emcabdto),'eam-EMCab-Save')")
    @ApiOperation(value = "保存货架", tags = {"货架" },  notes = "保存货架")
	@RequestMapping(method = RequestMethod.POST, value = "/emcabs/save")
    public ResponseEntity<Boolean> save(@RequestBody EMCabDTO emcabdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emcabService.save(emcabMapping.toDomain(emcabdto)));
    }

    @PreAuthorize("hasPermission(this.emcabMapping.toDomain(#emcabdtos),'eam-EMCab-Save')")
    @ApiOperation(value = "批量保存货架", tags = {"货架" },  notes = "批量保存货架")
	@RequestMapping(method = RequestMethod.POST, value = "/emcabs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMCabDTO> emcabdtos) {
        emcabService.saveBatch(emcabMapping.toDomain(emcabdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMCab-searchDefault-all') and hasPermission(#context,'eam-EMCab-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"货架" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emcabs/fetchdefault")
	public ResponseEntity<List<EMCabDTO>> fetchDefault(EMCabSearchContext context) {
        Page<EMCab> domains = emcabService.searchDefault(context) ;
        List<EMCabDTO> list = emcabMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMCab-searchDefault-all') and hasPermission(#context,'eam-EMCab-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"货架" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emcabs/searchdefault")
	public ResponseEntity<Page<EMCabDTO>> searchDefault(@RequestBody EMCabSearchContext context) {
        Page<EMCab> domains = emcabService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emcabMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

