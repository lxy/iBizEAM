package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMSubList;
import cn.ibizlab.eam.core.eam_core.service.IEMSubListService;
import cn.ibizlab.eam.core.eam_core.filter.EMSubListSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"材料申购清单" })
@RestController("WebApi-emsublist")
@RequestMapping("")
public class EMSubListResource {

    @Autowired
    public IEMSubListService emsublistService;

    @Autowired
    @Lazy
    public EMSubListMapping emsublistMapping;

    @PreAuthorize("hasPermission(this.emsublistMapping.toDomain(#emsublistdto),'eam-EMSubList-Create')")
    @ApiOperation(value = "新建材料申购清单", tags = {"材料申购清单" },  notes = "新建材料申购清单")
	@RequestMapping(method = RequestMethod.POST, value = "/emsublists")
    public ResponseEntity<EMSubListDTO> create(@Validated @RequestBody EMSubListDTO emsublistdto) {
        EMSubList domain = emsublistMapping.toDomain(emsublistdto);
		emsublistService.create(domain);
        EMSubListDTO dto = emsublistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emsublistMapping.toDomain(#emsublistdtos),'eam-EMSubList-Create')")
    @ApiOperation(value = "批量新建材料申购清单", tags = {"材料申购清单" },  notes = "批量新建材料申购清单")
	@RequestMapping(method = RequestMethod.POST, value = "/emsublists/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMSubListDTO> emsublistdtos) {
        emsublistService.createBatch(emsublistMapping.toDomain(emsublistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emsublist" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emsublistService.get(#emsublist_id),'eam-EMSubList-Update')")
    @ApiOperation(value = "更新材料申购清单", tags = {"材料申购清单" },  notes = "更新材料申购清单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emsublists/{emsublist_id}")
    public ResponseEntity<EMSubListDTO> update(@PathVariable("emsublist_id") String emsublist_id, @RequestBody EMSubListDTO emsublistdto) {
		EMSubList domain  = emsublistMapping.toDomain(emsublistdto);
        domain .setEmsublistid(emsublist_id);
		emsublistService.update(domain );
		EMSubListDTO dto = emsublistMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emsublistService.getEmsublistByEntities(this.emsublistMapping.toDomain(#emsublistdtos)),'eam-EMSubList-Update')")
    @ApiOperation(value = "批量更新材料申购清单", tags = {"材料申购清单" },  notes = "批量更新材料申购清单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emsublists/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMSubListDTO> emsublistdtos) {
        emsublistService.updateBatch(emsublistMapping.toDomain(emsublistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emsublistService.get(#emsublist_id),'eam-EMSubList-Remove')")
    @ApiOperation(value = "删除材料申购清单", tags = {"材料申购清单" },  notes = "删除材料申购清单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emsublists/{emsublist_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emsublist_id") String emsublist_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emsublistService.remove(emsublist_id));
    }

    @PreAuthorize("hasPermission(this.emsublistService.getEmsublistByIds(#ids),'eam-EMSubList-Remove')")
    @ApiOperation(value = "批量删除材料申购清单", tags = {"材料申购清单" },  notes = "批量删除材料申购清单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emsublists/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emsublistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emsublistMapping.toDomain(returnObject.body),'eam-EMSubList-Get')")
    @ApiOperation(value = "获取材料申购清单", tags = {"材料申购清单" },  notes = "获取材料申购清单")
	@RequestMapping(method = RequestMethod.GET, value = "/emsublists/{emsublist_id}")
    public ResponseEntity<EMSubListDTO> get(@PathVariable("emsublist_id") String emsublist_id) {
        EMSubList domain = emsublistService.get(emsublist_id);
        EMSubListDTO dto = emsublistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取材料申购清单草稿", tags = {"材料申购清单" },  notes = "获取材料申购清单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emsublists/getdraft")
    public ResponseEntity<EMSubListDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emsublistMapping.toDto(emsublistService.getDraft(new EMSubList())));
    }

    @ApiOperation(value = "检查材料申购清单", tags = {"材料申购清单" },  notes = "检查材料申购清单")
	@RequestMapping(method = RequestMethod.POST, value = "/emsublists/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMSubListDTO emsublistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emsublistService.checkKey(emsublistMapping.toDomain(emsublistdto)));
    }

    @PreAuthorize("hasPermission(this.emsublistMapping.toDomain(#emsublistdto),'eam-EMSubList-Save')")
    @ApiOperation(value = "保存材料申购清单", tags = {"材料申购清单" },  notes = "保存材料申购清单")
	@RequestMapping(method = RequestMethod.POST, value = "/emsublists/save")
    public ResponseEntity<Boolean> save(@RequestBody EMSubListDTO emsublistdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emsublistService.save(emsublistMapping.toDomain(emsublistdto)));
    }

    @PreAuthorize("hasPermission(this.emsublistMapping.toDomain(#emsublistdtos),'eam-EMSubList-Save')")
    @ApiOperation(value = "批量保存材料申购清单", tags = {"材料申购清单" },  notes = "批量保存材料申购清单")
	@RequestMapping(method = RequestMethod.POST, value = "/emsublists/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMSubListDTO> emsublistdtos) {
        emsublistService.saveBatch(emsublistMapping.toDomain(emsublistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMSubList-searchDefault-all') and hasPermission(#context,'eam-EMSubList-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"材料申购清单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emsublists/fetchdefault")
	public ResponseEntity<List<EMSubListDTO>> fetchDefault(EMSubListSearchContext context) {
        Page<EMSubList> domains = emsublistService.searchDefault(context) ;
        List<EMSubListDTO> list = emsublistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMSubList-searchDefault-all') and hasPermission(#context,'eam-EMSubList-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"材料申购清单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emsublists/searchdefault")
	public ResponseEntity<Page<EMSubListDTO>> searchDefault(@RequestBody EMSubListSearchContext context) {
        Page<EMSubList> domains = emsublistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emsublistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

