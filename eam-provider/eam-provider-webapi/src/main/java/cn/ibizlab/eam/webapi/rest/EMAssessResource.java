package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMAssess;
import cn.ibizlab.eam.core.eam_core.service.IEMAssessService;
import cn.ibizlab.eam.core.eam_core.filter.EMAssessSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"设备故障考核" })
@RestController("WebApi-emassess")
@RequestMapping("")
public class EMAssessResource {

    @Autowired
    public IEMAssessService emassessService;

    @Autowired
    @Lazy
    public EMAssessMapping emassessMapping;

    @PreAuthorize("hasPermission(this.emassessMapping.toDomain(#emassessdto),'eam-EMAssess-Create')")
    @ApiOperation(value = "新建设备故障考核", tags = {"设备故障考核" },  notes = "新建设备故障考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emassesses")
    public ResponseEntity<EMAssessDTO> create(@Validated @RequestBody EMAssessDTO emassessdto) {
        EMAssess domain = emassessMapping.toDomain(emassessdto);
		emassessService.create(domain);
        EMAssessDTO dto = emassessMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassessMapping.toDomain(#emassessdtos),'eam-EMAssess-Create')")
    @ApiOperation(value = "批量新建设备故障考核", tags = {"设备故障考核" },  notes = "批量新建设备故障考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emassesses/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMAssessDTO> emassessdtos) {
        emassessService.createBatch(emassessMapping.toDomain(emassessdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emassess" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emassessService.get(#emassess_id),'eam-EMAssess-Update')")
    @ApiOperation(value = "更新设备故障考核", tags = {"设备故障考核" },  notes = "更新设备故障考核")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassesses/{emassess_id}")
    public ResponseEntity<EMAssessDTO> update(@PathVariable("emassess_id") String emassess_id, @RequestBody EMAssessDTO emassessdto) {
		EMAssess domain  = emassessMapping.toDomain(emassessdto);
        domain .setEmassessid(emassess_id);
		emassessService.update(domain );
		EMAssessDTO dto = emassessMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassessService.getEmassessByEntities(this.emassessMapping.toDomain(#emassessdtos)),'eam-EMAssess-Update')")
    @ApiOperation(value = "批量更新设备故障考核", tags = {"设备故障考核" },  notes = "批量更新设备故障考核")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassesses/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMAssessDTO> emassessdtos) {
        emassessService.updateBatch(emassessMapping.toDomain(emassessdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emassessService.get(#emassess_id),'eam-EMAssess-Remove')")
    @ApiOperation(value = "删除设备故障考核", tags = {"设备故障考核" },  notes = "删除设备故障考核")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassesses/{emassess_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emassess_id") String emassess_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emassessService.remove(emassess_id));
    }

    @PreAuthorize("hasPermission(this.emassessService.getEmassessByIds(#ids),'eam-EMAssess-Remove')")
    @ApiOperation(value = "批量删除设备故障考核", tags = {"设备故障考核" },  notes = "批量删除设备故障考核")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassesses/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emassessService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emassessMapping.toDomain(returnObject.body),'eam-EMAssess-Get')")
    @ApiOperation(value = "获取设备故障考核", tags = {"设备故障考核" },  notes = "获取设备故障考核")
	@RequestMapping(method = RequestMethod.GET, value = "/emassesses/{emassess_id}")
    public ResponseEntity<EMAssessDTO> get(@PathVariable("emassess_id") String emassess_id) {
        EMAssess domain = emassessService.get(emassess_id);
        EMAssessDTO dto = emassessMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取设备故障考核草稿", tags = {"设备故障考核" },  notes = "获取设备故障考核草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emassesses/getdraft")
    public ResponseEntity<EMAssessDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emassessMapping.toDto(emassessService.getDraft(new EMAssess())));
    }

    @ApiOperation(value = "检查设备故障考核", tags = {"设备故障考核" },  notes = "检查设备故障考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emassesses/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMAssessDTO emassessdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emassessService.checkKey(emassessMapping.toDomain(emassessdto)));
    }

    @PreAuthorize("hasPermission(this.emassessMapping.toDomain(#emassessdto),'eam-EMAssess-Save')")
    @ApiOperation(value = "保存设备故障考核", tags = {"设备故障考核" },  notes = "保存设备故障考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emassesses/save")
    public ResponseEntity<Boolean> save(@RequestBody EMAssessDTO emassessdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emassessService.save(emassessMapping.toDomain(emassessdto)));
    }

    @PreAuthorize("hasPermission(this.emassessMapping.toDomain(#emassessdtos),'eam-EMAssess-Save')")
    @ApiOperation(value = "批量保存设备故障考核", tags = {"设备故障考核" },  notes = "批量保存设备故障考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emassesses/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMAssessDTO> emassessdtos) {
        emassessService.saveBatch(emassessMapping.toDomain(emassessdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssess-searchDefault-all') and hasPermission(#context,'eam-EMAssess-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"设备故障考核" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emassesses/fetchdefault")
	public ResponseEntity<List<EMAssessDTO>> fetchDefault(EMAssessSearchContext context) {
        Page<EMAssess> domains = emassessService.searchDefault(context) ;
        List<EMAssessDTO> list = emassessMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssess-searchDefault-all') and hasPermission(#context,'eam-EMAssess-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"设备故障考核" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emassesses/searchdefault")
	public ResponseEntity<Page<EMAssessDTO>> searchDefault(@RequestBody EMAssessSearchContext context) {
        Page<EMAssess> domains = emassessService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emassessMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

