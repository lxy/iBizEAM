package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMPO;
import cn.ibizlab.eam.core.eam_core.service.IEMPOService;
import cn.ibizlab.eam.core.eam_core.filter.EMPOSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"订单" })
@RestController("WebApi-empo")
@RequestMapping("")
public class EMPOResource {

    @Autowired
    public IEMPOService empoService;

    @Autowired
    @Lazy
    public EMPOMapping empoMapping;

    @PreAuthorize("hasPermission(this.empoMapping.toDomain(#empodto),'eam-EMPO-Create')")
    @ApiOperation(value = "新建订单", tags = {"订单" },  notes = "新建订单")
	@RequestMapping(method = RequestMethod.POST, value = "/empos")
    public ResponseEntity<EMPODTO> create(@Validated @RequestBody EMPODTO empodto) {
        EMPO domain = empoMapping.toDomain(empodto);
		empoService.create(domain);
        EMPODTO dto = empoMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empoMapping.toDomain(#empodtos),'eam-EMPO-Create')")
    @ApiOperation(value = "批量新建订单", tags = {"订单" },  notes = "批量新建订单")
	@RequestMapping(method = RequestMethod.POST, value = "/empos/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMPODTO> empodtos) {
        empoService.createBatch(empoMapping.toDomain(empodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "empo" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.empoService.get(#empo_id),'eam-EMPO-Update')")
    @ApiOperation(value = "更新订单", tags = {"订单" },  notes = "更新订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/empos/{empo_id}")
    public ResponseEntity<EMPODTO> update(@PathVariable("empo_id") String empo_id, @RequestBody EMPODTO empodto) {
		EMPO domain  = empoMapping.toDomain(empodto);
        domain .setEmpoid(empo_id);
		empoService.update(domain );
		EMPODTO dto = empoMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empoService.getEmpoByEntities(this.empoMapping.toDomain(#empodtos)),'eam-EMPO-Update')")
    @ApiOperation(value = "批量更新订单", tags = {"订单" },  notes = "批量更新订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/empos/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMPODTO> empodtos) {
        empoService.updateBatch(empoMapping.toDomain(empodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.empoService.get(#empo_id),'eam-EMPO-Remove')")
    @ApiOperation(value = "删除订单", tags = {"订单" },  notes = "删除订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/empos/{empo_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("empo_id") String empo_id) {
         return ResponseEntity.status(HttpStatus.OK).body(empoService.remove(empo_id));
    }

    @PreAuthorize("hasPermission(this.empoService.getEmpoByIds(#ids),'eam-EMPO-Remove')")
    @ApiOperation(value = "批量删除订单", tags = {"订单" },  notes = "批量删除订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/empos/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        empoService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.empoMapping.toDomain(returnObject.body),'eam-EMPO-Get')")
    @ApiOperation(value = "获取订单", tags = {"订单" },  notes = "获取订单")
	@RequestMapping(method = RequestMethod.GET, value = "/empos/{empo_id}")
    public ResponseEntity<EMPODTO> get(@PathVariable("empo_id") String empo_id) {
        EMPO domain = empoService.get(empo_id);
        EMPODTO dto = empoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取订单草稿", tags = {"订单" },  notes = "获取订单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/empos/getdraft")
    public ResponseEntity<EMPODTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(empoMapping.toDto(empoService.getDraft(new EMPO())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPO-Arrival-all')")
    @ApiOperation(value = "到货", tags = {"订单" },  notes = "到货")
	@RequestMapping(method = RequestMethod.POST, value = "/empos/{empo_id}/arrival")
    public ResponseEntity<EMPODTO> arrival(@PathVariable("empo_id") String empo_id, @RequestBody EMPODTO empodto) {
        EMPO domain = empoMapping.toDomain(empodto);
        domain.setEmpoid(empo_id);
        domain = empoService.arrival(domain);
        empodto = empoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodto);
    }

    @ApiOperation(value = "检查订单", tags = {"订单" },  notes = "检查订单")
	@RequestMapping(method = RequestMethod.POST, value = "/empos/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMPODTO empodto) {
        return  ResponseEntity.status(HttpStatus.OK).body(empoService.checkKey(empoMapping.toDomain(empodto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPO-GenId-all')")
    @ApiOperation(value = "生成订单编号", tags = {"订单" },  notes = "生成订单编号")
	@RequestMapping(method = RequestMethod.POST, value = "/empos/{empo_id}/genid")
    public ResponseEntity<EMPODTO> genId(@PathVariable("empo_id") String empo_id, @RequestBody EMPODTO empodto) {
        EMPO domain = empoMapping.toDomain(empodto);
        domain.setEmpoid(empo_id);
        domain = empoService.genId(domain);
        empodto = empoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPO-PlaceOrder-all')")
    @ApiOperation(value = "下订单", tags = {"订单" },  notes = "下订单")
	@RequestMapping(method = RequestMethod.POST, value = "/empos/{empo_id}/placeorder")
    public ResponseEntity<EMPODTO> placeOrder(@PathVariable("empo_id") String empo_id, @RequestBody EMPODTO empodto) {
        EMPO domain = empoMapping.toDomain(empodto);
        domain.setEmpoid(empo_id);
        domain = empoService.placeOrder(domain);
        empodto = empoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodto);
    }

    @PreAuthorize("hasPermission(this.empoMapping.toDomain(#empodto),'eam-EMPO-Save')")
    @ApiOperation(value = "保存订单", tags = {"订单" },  notes = "保存订单")
	@RequestMapping(method = RequestMethod.POST, value = "/empos/save")
    public ResponseEntity<Boolean> save(@RequestBody EMPODTO empodto) {
        return ResponseEntity.status(HttpStatus.OK).body(empoService.save(empoMapping.toDomain(empodto)));
    }

    @PreAuthorize("hasPermission(this.empoMapping.toDomain(#empodtos),'eam-EMPO-Save')")
    @ApiOperation(value = "批量保存订单", tags = {"订单" },  notes = "批量保存订单")
	@RequestMapping(method = RequestMethod.POST, value = "/empos/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMPODTO> empodtos) {
        empoService.saveBatch(empoMapping.toDomain(empodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPO-searchClosedOrder-all') and hasPermission(#context,'eam-EMPO-Get')")
	@ApiOperation(value = "获取已关闭订单", tags = {"订单" } ,notes = "获取已关闭订单")
    @RequestMapping(method= RequestMethod.GET , value="/empos/fetchclosedorder")
	public ResponseEntity<List<EMPODTO>> fetchClosedOrder(EMPOSearchContext context) {
        Page<EMPO> domains = empoService.searchClosedOrder(context) ;
        List<EMPODTO> list = empoMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPO-searchClosedOrder-all') and hasPermission(#context,'eam-EMPO-Get')")
	@ApiOperation(value = "查询已关闭订单", tags = {"订单" } ,notes = "查询已关闭订单")
    @RequestMapping(method= RequestMethod.POST , value="/empos/searchclosedorder")
	public ResponseEntity<Page<EMPODTO>> searchClosedOrder(@RequestBody EMPOSearchContext context) {
        Page<EMPO> domains = empoService.searchClosedOrder(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empoMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPO-searchDefault-all') and hasPermission(#context,'eam-EMPO-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"订单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/empos/fetchdefault")
	public ResponseEntity<List<EMPODTO>> fetchDefault(EMPOSearchContext context) {
        Page<EMPO> domains = empoService.searchDefault(context) ;
        List<EMPODTO> list = empoMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPO-searchDefault-all') and hasPermission(#context,'eam-EMPO-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"订单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/empos/searchdefault")
	public ResponseEntity<Page<EMPODTO>> searchDefault(@RequestBody EMPOSearchContext context) {
        Page<EMPO> domains = empoService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empoMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPO-searchOnOrder-all') and hasPermission(#context,'eam-EMPO-Get')")
	@ApiOperation(value = "获取在途", tags = {"订单" } ,notes = "获取在途")
    @RequestMapping(method= RequestMethod.GET , value="/empos/fetchonorder")
	public ResponseEntity<List<EMPODTO>> fetchOnOrder(EMPOSearchContext context) {
        Page<EMPO> domains = empoService.searchOnOrder(context) ;
        List<EMPODTO> list = empoMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPO-searchOnOrder-all') and hasPermission(#context,'eam-EMPO-Get')")
	@ApiOperation(value = "查询在途", tags = {"订单" } ,notes = "查询在途")
    @RequestMapping(method= RequestMethod.POST , value="/empos/searchonorder")
	public ResponseEntity<Page<EMPODTO>> searchOnOrder(@RequestBody EMPOSearchContext context) {
        Page<EMPO> domains = empoService.searchOnOrder(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empoMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPO-searchPlaceOrder-all') and hasPermission(#context,'eam-EMPO-Get')")
	@ApiOperation(value = "获取等待下单", tags = {"订单" } ,notes = "获取等待下单")
    @RequestMapping(method= RequestMethod.GET , value="/empos/fetchplaceorder")
	public ResponseEntity<List<EMPODTO>> fetchPlaceOrder(EMPOSearchContext context) {
        Page<EMPO> domains = empoService.searchPlaceOrder(context) ;
        List<EMPODTO> list = empoMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPO-searchPlaceOrder-all') and hasPermission(#context,'eam-EMPO-Get')")
	@ApiOperation(value = "查询等待下单", tags = {"订单" } ,notes = "查询等待下单")
    @RequestMapping(method= RequestMethod.POST , value="/empos/searchplaceorder")
	public ResponseEntity<Page<EMPODTO>> searchPlaceOrder(@RequestBody EMPOSearchContext context) {
        Page<EMPO> domains = empoService.searchPlaceOrder(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empoMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

