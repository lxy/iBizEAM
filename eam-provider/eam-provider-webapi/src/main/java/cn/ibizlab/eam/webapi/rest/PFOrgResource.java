package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_pf.domain.PFOrg;
import cn.ibizlab.eam.core.eam_pf.service.IPFOrgService;
import cn.ibizlab.eam.core.eam_pf.filter.PFOrgSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"组织" })
@RestController("WebApi-pforg")
@RequestMapping("")
public class PFOrgResource {

    @Autowired
    public IPFOrgService pforgService;

    @Autowired
    @Lazy
    public PFOrgMapping pforgMapping;

    @PreAuthorize("hasPermission(this.pforgMapping.toDomain(#pforgdto),'eam-PFOrg-Create')")
    @ApiOperation(value = "新建组织", tags = {"组织" },  notes = "新建组织")
	@RequestMapping(method = RequestMethod.POST, value = "/pforgs")
    public ResponseEntity<PFOrgDTO> create(@Validated @RequestBody PFOrgDTO pforgdto) {
        PFOrg domain = pforgMapping.toDomain(pforgdto);
		pforgService.create(domain);
        PFOrgDTO dto = pforgMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pforgMapping.toDomain(#pforgdtos),'eam-PFOrg-Create')")
    @ApiOperation(value = "批量新建组织", tags = {"组织" },  notes = "批量新建组织")
	@RequestMapping(method = RequestMethod.POST, value = "/pforgs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<PFOrgDTO> pforgdtos) {
        pforgService.createBatch(pforgMapping.toDomain(pforgdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "pforg" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.pforgService.get(#pforg_id),'eam-PFOrg-Update')")
    @ApiOperation(value = "更新组织", tags = {"组织" },  notes = "更新组织")
	@RequestMapping(method = RequestMethod.PUT, value = "/pforgs/{pforg_id}")
    public ResponseEntity<PFOrgDTO> update(@PathVariable("pforg_id") String pforg_id, @RequestBody PFOrgDTO pforgdto) {
		PFOrg domain  = pforgMapping.toDomain(pforgdto);
        domain .setPforgid(pforg_id);
		pforgService.update(domain );
		PFOrgDTO dto = pforgMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pforgService.getPforgByEntities(this.pforgMapping.toDomain(#pforgdtos)),'eam-PFOrg-Update')")
    @ApiOperation(value = "批量更新组织", tags = {"组织" },  notes = "批量更新组织")
	@RequestMapping(method = RequestMethod.PUT, value = "/pforgs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<PFOrgDTO> pforgdtos) {
        pforgService.updateBatch(pforgMapping.toDomain(pforgdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.pforgService.get(#pforg_id),'eam-PFOrg-Remove')")
    @ApiOperation(value = "删除组织", tags = {"组织" },  notes = "删除组织")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pforgs/{pforg_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("pforg_id") String pforg_id) {
         return ResponseEntity.status(HttpStatus.OK).body(pforgService.remove(pforg_id));
    }

    @PreAuthorize("hasPermission(this.pforgService.getPforgByIds(#ids),'eam-PFOrg-Remove')")
    @ApiOperation(value = "批量删除组织", tags = {"组织" },  notes = "批量删除组织")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pforgs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        pforgService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.pforgMapping.toDomain(returnObject.body),'eam-PFOrg-Get')")
    @ApiOperation(value = "获取组织", tags = {"组织" },  notes = "获取组织")
	@RequestMapping(method = RequestMethod.GET, value = "/pforgs/{pforg_id}")
    public ResponseEntity<PFOrgDTO> get(@PathVariable("pforg_id") String pforg_id) {
        PFOrg domain = pforgService.get(pforg_id);
        PFOrgDTO dto = pforgMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取组织草稿", tags = {"组织" },  notes = "获取组织草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/pforgs/getdraft")
    public ResponseEntity<PFOrgDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(pforgMapping.toDto(pforgService.getDraft(new PFOrg())));
    }

    @ApiOperation(value = "检查组织", tags = {"组织" },  notes = "检查组织")
	@RequestMapping(method = RequestMethod.POST, value = "/pforgs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody PFOrgDTO pforgdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(pforgService.checkKey(pforgMapping.toDomain(pforgdto)));
    }

    @PreAuthorize("hasPermission(this.pforgMapping.toDomain(#pforgdto),'eam-PFOrg-Save')")
    @ApiOperation(value = "保存组织", tags = {"组织" },  notes = "保存组织")
	@RequestMapping(method = RequestMethod.POST, value = "/pforgs/save")
    public ResponseEntity<Boolean> save(@RequestBody PFOrgDTO pforgdto) {
        return ResponseEntity.status(HttpStatus.OK).body(pforgService.save(pforgMapping.toDomain(pforgdto)));
    }

    @PreAuthorize("hasPermission(this.pforgMapping.toDomain(#pforgdtos),'eam-PFOrg-Save')")
    @ApiOperation(value = "批量保存组织", tags = {"组织" },  notes = "批量保存组织")
	@RequestMapping(method = RequestMethod.POST, value = "/pforgs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<PFOrgDTO> pforgdtos) {
        pforgService.saveBatch(pforgMapping.toDomain(pforgdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFOrg-searchDefault-all') and hasPermission(#context,'eam-PFOrg-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"组织" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pforgs/fetchdefault")
	public ResponseEntity<List<PFOrgDTO>> fetchDefault(PFOrgSearchContext context) {
        Page<PFOrg> domains = pforgService.searchDefault(context) ;
        List<PFOrgDTO> list = pforgMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFOrg-searchDefault-all') and hasPermission(#context,'eam-PFOrg-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"组织" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pforgs/searchdefault")
	public ResponseEntity<Page<PFOrgDTO>> searchDefault(@RequestBody PFOrgSearchContext context) {
        Page<PFOrg> domains = pforgService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(pforgMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

