package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMStorePart;
import cn.ibizlab.eam.core.eam_core.service.IEMStorePartService;
import cn.ibizlab.eam.core.eam_core.filter.EMStorePartSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"仓库库位" })
@RestController("WebApi-emstorepart")
@RequestMapping("")
public class EMStorePartResource {

    @Autowired
    public IEMStorePartService emstorepartService;

    @Autowired
    @Lazy
    public EMStorePartMapping emstorepartMapping;

    @PreAuthorize("hasPermission(this.emstorepartMapping.toDomain(#emstorepartdto),'eam-EMStorePart-Create')")
    @ApiOperation(value = "新建仓库库位", tags = {"仓库库位" },  notes = "新建仓库库位")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts")
    public ResponseEntity<EMStorePartDTO> create(@Validated @RequestBody EMStorePartDTO emstorepartdto) {
        EMStorePart domain = emstorepartMapping.toDomain(emstorepartdto);
		emstorepartService.create(domain);
        EMStorePartDTO dto = emstorepartMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstorepartMapping.toDomain(#emstorepartdtos),'eam-EMStorePart-Create')")
    @ApiOperation(value = "批量新建仓库库位", tags = {"仓库库位" },  notes = "批量新建仓库库位")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMStorePartDTO> emstorepartdtos) {
        emstorepartService.createBatch(emstorepartMapping.toDomain(emstorepartdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emstorepart" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emstorepartService.get(#emstorepart_id),'eam-EMStorePart-Update')")
    @ApiOperation(value = "更新仓库库位", tags = {"仓库库位" },  notes = "更新仓库库位")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}")
    public ResponseEntity<EMStorePartDTO> update(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMStorePartDTO emstorepartdto) {
		EMStorePart domain  = emstorepartMapping.toDomain(emstorepartdto);
        domain .setEmstorepartid(emstorepart_id);
		emstorepartService.update(domain );
		EMStorePartDTO dto = emstorepartMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstorepartService.getEmstorepartByEntities(this.emstorepartMapping.toDomain(#emstorepartdtos)),'eam-EMStorePart-Update')")
    @ApiOperation(value = "批量更新仓库库位", tags = {"仓库库位" },  notes = "批量更新仓库库位")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMStorePartDTO> emstorepartdtos) {
        emstorepartService.updateBatch(emstorepartMapping.toDomain(emstorepartdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emstorepartService.get(#emstorepart_id),'eam-EMStorePart-Remove')")
    @ApiOperation(value = "删除仓库库位", tags = {"仓库库位" },  notes = "删除仓库库位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emstorepart_id") String emstorepart_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emstorepartService.remove(emstorepart_id));
    }

    @PreAuthorize("hasPermission(this.emstorepartService.getEmstorepartByIds(#ids),'eam-EMStorePart-Remove')")
    @ApiOperation(value = "批量删除仓库库位", tags = {"仓库库位" },  notes = "批量删除仓库库位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emstorepartService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emstorepartMapping.toDomain(returnObject.body),'eam-EMStorePart-Get')")
    @ApiOperation(value = "获取仓库库位", tags = {"仓库库位" },  notes = "获取仓库库位")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}")
    public ResponseEntity<EMStorePartDTO> get(@PathVariable("emstorepart_id") String emstorepart_id) {
        EMStorePart domain = emstorepartService.get(emstorepart_id);
        EMStorePartDTO dto = emstorepartMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取仓库库位草稿", tags = {"仓库库位" },  notes = "获取仓库库位草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/getdraft")
    public ResponseEntity<EMStorePartDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emstorepartMapping.toDto(emstorepartService.getDraft(new EMStorePart())));
    }

    @ApiOperation(value = "检查仓库库位", tags = {"仓库库位" },  notes = "检查仓库库位")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMStorePartDTO emstorepartdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emstorepartService.checkKey(emstorepartMapping.toDomain(emstorepartdto)));
    }

    @PreAuthorize("hasPermission(this.emstorepartMapping.toDomain(#emstorepartdto),'eam-EMStorePart-Save')")
    @ApiOperation(value = "保存仓库库位", tags = {"仓库库位" },  notes = "保存仓库库位")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/save")
    public ResponseEntity<Boolean> save(@RequestBody EMStorePartDTO emstorepartdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emstorepartService.save(emstorepartMapping.toDomain(emstorepartdto)));
    }

    @PreAuthorize("hasPermission(this.emstorepartMapping.toDomain(#emstorepartdtos),'eam-EMStorePart-Save')")
    @ApiOperation(value = "批量保存仓库库位", tags = {"仓库库位" },  notes = "批量保存仓库库位")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMStorePartDTO> emstorepartdtos) {
        emstorepartService.saveBatch(emstorepartMapping.toDomain(emstorepartdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStorePart-searchDefault-all') and hasPermission(#context,'eam-EMStorePart-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"仓库库位" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/fetchdefault")
	public ResponseEntity<List<EMStorePartDTO>> fetchDefault(EMStorePartSearchContext context) {
        Page<EMStorePart> domains = emstorepartService.searchDefault(context) ;
        List<EMStorePartDTO> list = emstorepartMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStorePart-searchDefault-all') and hasPermission(#context,'eam-EMStorePart-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"仓库库位" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/searchdefault")
	public ResponseEntity<Page<EMStorePartDTO>> searchDefault(@RequestBody EMStorePartSearchContext context) {
        Page<EMStorePart> domains = emstorepartService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emstorepartMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emstorepartMapping.toDomain(#emstorepartdto),'eam-EMStorePart-Create')")
    @ApiOperation(value = "根据仓库建立仓库库位", tags = {"仓库库位" },  notes = "根据仓库建立仓库库位")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts")
    public ResponseEntity<EMStorePartDTO> createByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMStorePartDTO emstorepartdto) {
        EMStorePart domain = emstorepartMapping.toDomain(emstorepartdto);
        domain.setStoreid(emstore_id);
		emstorepartService.create(domain);
        EMStorePartDTO dto = emstorepartMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstorepartMapping.toDomain(#emstorepartdtos),'eam-EMStorePart-Create')")
    @ApiOperation(value = "根据仓库批量建立仓库库位", tags = {"仓库库位" },  notes = "根据仓库批量建立仓库库位")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/batch")
    public ResponseEntity<Boolean> createBatchByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody List<EMStorePartDTO> emstorepartdtos) {
        List<EMStorePart> domainlist=emstorepartMapping.toDomain(emstorepartdtos);
        for(EMStorePart domain:domainlist){
            domain.setStoreid(emstore_id);
        }
        emstorepartService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emstorepart" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emstorepartService.get(#emstorepart_id),'eam-EMStorePart-Update')")
    @ApiOperation(value = "根据仓库更新仓库库位", tags = {"仓库库位" },  notes = "根据仓库更新仓库库位")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}")
    public ResponseEntity<EMStorePartDTO> updateByEMStore(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMStorePartDTO emstorepartdto) {
        EMStorePart domain = emstorepartMapping.toDomain(emstorepartdto);
        domain.setStoreid(emstore_id);
        domain.setEmstorepartid(emstorepart_id);
		emstorepartService.update(domain);
        EMStorePartDTO dto = emstorepartMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstorepartService.getEmstorepartByEntities(this.emstorepartMapping.toDomain(#emstorepartdtos)),'eam-EMStorePart-Update')")
    @ApiOperation(value = "根据仓库批量更新仓库库位", tags = {"仓库库位" },  notes = "根据仓库批量更新仓库库位")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/batch")
    public ResponseEntity<Boolean> updateBatchByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody List<EMStorePartDTO> emstorepartdtos) {
        List<EMStorePart> domainlist=emstorepartMapping.toDomain(emstorepartdtos);
        for(EMStorePart domain:domainlist){
            domain.setStoreid(emstore_id);
        }
        emstorepartService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emstorepartService.get(#emstorepart_id),'eam-EMStorePart-Remove')")
    @ApiOperation(value = "根据仓库删除仓库库位", tags = {"仓库库位" },  notes = "根据仓库删除仓库库位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}")
    public ResponseEntity<Boolean> removeByEMStore(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emstorepartService.remove(emstorepart_id));
    }

    @PreAuthorize("hasPermission(this.emstorepartService.getEmstorepartByIds(#ids),'eam-EMStorePart-Remove')")
    @ApiOperation(value = "根据仓库批量删除仓库库位", tags = {"仓库库位" },  notes = "根据仓库批量删除仓库库位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/batch")
    public ResponseEntity<Boolean> removeBatchByEMStore(@RequestBody List<String> ids) {
        emstorepartService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emstorepartMapping.toDomain(returnObject.body),'eam-EMStorePart-Get')")
    @ApiOperation(value = "根据仓库获取仓库库位", tags = {"仓库库位" },  notes = "根据仓库获取仓库库位")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}")
    public ResponseEntity<EMStorePartDTO> getByEMStore(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id) {
        EMStorePart domain = emstorepartService.get(emstorepart_id);
        EMStorePartDTO dto = emstorepartMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库获取仓库库位草稿", tags = {"仓库库位" },  notes = "根据仓库获取仓库库位草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/getdraft")
    public ResponseEntity<EMStorePartDTO> getDraftByEMStore(@PathVariable("emstore_id") String emstore_id) {
        EMStorePart domain = new EMStorePart();
        domain.setStoreid(emstore_id);
        return ResponseEntity.status(HttpStatus.OK).body(emstorepartMapping.toDto(emstorepartService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库检查仓库库位", tags = {"仓库库位" },  notes = "根据仓库检查仓库库位")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMStorePartDTO emstorepartdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emstorepartService.checkKey(emstorepartMapping.toDomain(emstorepartdto)));
    }

    @PreAuthorize("hasPermission(this.emstorepartMapping.toDomain(#emstorepartdto),'eam-EMStorePart-Save')")
    @ApiOperation(value = "根据仓库保存仓库库位", tags = {"仓库库位" },  notes = "根据仓库保存仓库库位")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/save")
    public ResponseEntity<Boolean> saveByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMStorePartDTO emstorepartdto) {
        EMStorePart domain = emstorepartMapping.toDomain(emstorepartdto);
        domain.setStoreid(emstore_id);
        return ResponseEntity.status(HttpStatus.OK).body(emstorepartService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emstorepartMapping.toDomain(#emstorepartdtos),'eam-EMStorePart-Save')")
    @ApiOperation(value = "根据仓库批量保存仓库库位", tags = {"仓库库位" },  notes = "根据仓库批量保存仓库库位")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody List<EMStorePartDTO> emstorepartdtos) {
        List<EMStorePart> domainlist=emstorepartMapping.toDomain(emstorepartdtos);
        for(EMStorePart domain:domainlist){
             domain.setStoreid(emstore_id);
        }
        emstorepartService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStorePart-searchDefault-all') and hasPermission(#context,'eam-EMStorePart-Get')")
	@ApiOperation(value = "根据仓库获取DEFAULT", tags = {"仓库库位" } ,notes = "根据仓库获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/fetchdefault")
	public ResponseEntity<List<EMStorePartDTO>> fetchEMStorePartDefaultByEMStore(@PathVariable("emstore_id") String emstore_id,EMStorePartSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<EMStorePart> domains = emstorepartService.searchDefault(context) ;
        List<EMStorePartDTO> list = emstorepartMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStorePart-searchDefault-all') and hasPermission(#context,'eam-EMStorePart-Get')")
	@ApiOperation(value = "根据仓库查询DEFAULT", tags = {"仓库库位" } ,notes = "根据仓库查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/searchdefault")
	public ResponseEntity<Page<EMStorePartDTO>> searchEMStorePartDefaultByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMStorePartSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<EMStorePart> domains = emstorepartService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emstorepartMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

