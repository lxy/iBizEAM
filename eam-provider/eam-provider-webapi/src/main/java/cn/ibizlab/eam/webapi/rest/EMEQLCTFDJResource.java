package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTFDJ;
import cn.ibizlab.eam.core.eam_core.service.IEMEQLCTFDJService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLCTFDJSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"发动机位置" })
@RestController("WebApi-emeqlctfdj")
@RequestMapping("")
public class EMEQLCTFDJResource {

    @Autowired
    public IEMEQLCTFDJService emeqlctfdjService;

    @Autowired
    @Lazy
    public EMEQLCTFDJMapping emeqlctfdjMapping;

    @PreAuthorize("hasPermission(this.emeqlctfdjMapping.toDomain(#emeqlctfdjdto),'eam-EMEQLCTFDJ-Create')")
    @ApiOperation(value = "新建发动机位置", tags = {"发动机位置" },  notes = "新建发动机位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctfdjs")
    public ResponseEntity<EMEQLCTFDJDTO> create(@Validated @RequestBody EMEQLCTFDJDTO emeqlctfdjdto) {
        EMEQLCTFDJ domain = emeqlctfdjMapping.toDomain(emeqlctfdjdto);
		emeqlctfdjService.create(domain);
        EMEQLCTFDJDTO dto = emeqlctfdjMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqlctfdjMapping.toDomain(#emeqlctfdjdtos),'eam-EMEQLCTFDJ-Create')")
    @ApiOperation(value = "批量新建发动机位置", tags = {"发动机位置" },  notes = "批量新建发动机位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctfdjs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQLCTFDJDTO> emeqlctfdjdtos) {
        emeqlctfdjService.createBatch(emeqlctfdjMapping.toDomain(emeqlctfdjdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqlctfdj" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqlctfdjService.get(#emeqlctfdj_id),'eam-EMEQLCTFDJ-Update')")
    @ApiOperation(value = "更新发动机位置", tags = {"发动机位置" },  notes = "更新发动机位置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqlctfdjs/{emeqlctfdj_id}")
    public ResponseEntity<EMEQLCTFDJDTO> update(@PathVariable("emeqlctfdj_id") String emeqlctfdj_id, @RequestBody EMEQLCTFDJDTO emeqlctfdjdto) {
		EMEQLCTFDJ domain  = emeqlctfdjMapping.toDomain(emeqlctfdjdto);
        domain .setEmeqlocationid(emeqlctfdj_id);
		emeqlctfdjService.update(domain );
		EMEQLCTFDJDTO dto = emeqlctfdjMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqlctfdjService.getEmeqlctfdjByEntities(this.emeqlctfdjMapping.toDomain(#emeqlctfdjdtos)),'eam-EMEQLCTFDJ-Update')")
    @ApiOperation(value = "批量更新发动机位置", tags = {"发动机位置" },  notes = "批量更新发动机位置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqlctfdjs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQLCTFDJDTO> emeqlctfdjdtos) {
        emeqlctfdjService.updateBatch(emeqlctfdjMapping.toDomain(emeqlctfdjdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqlctfdjService.get(#emeqlctfdj_id),'eam-EMEQLCTFDJ-Remove')")
    @ApiOperation(value = "删除发动机位置", tags = {"发动机位置" },  notes = "删除发动机位置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqlctfdjs/{emeqlctfdj_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqlctfdj_id") String emeqlctfdj_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqlctfdjService.remove(emeqlctfdj_id));
    }

    @PreAuthorize("hasPermission(this.emeqlctfdjService.getEmeqlctfdjByIds(#ids),'eam-EMEQLCTFDJ-Remove')")
    @ApiOperation(value = "批量删除发动机位置", tags = {"发动机位置" },  notes = "批量删除发动机位置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqlctfdjs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqlctfdjService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqlctfdjMapping.toDomain(returnObject.body),'eam-EMEQLCTFDJ-Get')")
    @ApiOperation(value = "获取发动机位置", tags = {"发动机位置" },  notes = "获取发动机位置")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqlctfdjs/{emeqlctfdj_id}")
    public ResponseEntity<EMEQLCTFDJDTO> get(@PathVariable("emeqlctfdj_id") String emeqlctfdj_id) {
        EMEQLCTFDJ domain = emeqlctfdjService.get(emeqlctfdj_id);
        EMEQLCTFDJDTO dto = emeqlctfdjMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取发动机位置草稿", tags = {"发动机位置" },  notes = "获取发动机位置草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqlctfdjs/getdraft")
    public ResponseEntity<EMEQLCTFDJDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emeqlctfdjMapping.toDto(emeqlctfdjService.getDraft(new EMEQLCTFDJ())));
    }

    @ApiOperation(value = "检查发动机位置", tags = {"发动机位置" },  notes = "检查发动机位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctfdjs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQLCTFDJDTO emeqlctfdjdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqlctfdjService.checkKey(emeqlctfdjMapping.toDomain(emeqlctfdjdto)));
    }

    @PreAuthorize("hasPermission(this.emeqlctfdjMapping.toDomain(#emeqlctfdjdto),'eam-EMEQLCTFDJ-Save')")
    @ApiOperation(value = "保存发动机位置", tags = {"发动机位置" },  notes = "保存发动机位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctfdjs/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEQLCTFDJDTO emeqlctfdjdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emeqlctfdjService.save(emeqlctfdjMapping.toDomain(emeqlctfdjdto)));
    }

    @PreAuthorize("hasPermission(this.emeqlctfdjMapping.toDomain(#emeqlctfdjdtos),'eam-EMEQLCTFDJ-Save')")
    @ApiOperation(value = "批量保存发动机位置", tags = {"发动机位置" },  notes = "批量保存发动机位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctfdjs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQLCTFDJDTO> emeqlctfdjdtos) {
        emeqlctfdjService.saveBatch(emeqlctfdjMapping.toDomain(emeqlctfdjdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTFDJ-searchDefault-all') and hasPermission(#context,'eam-EMEQLCTFDJ-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"发动机位置" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqlctfdjs/fetchdefault")
	public ResponseEntity<List<EMEQLCTFDJDTO>> fetchDefault(EMEQLCTFDJSearchContext context) {
        Page<EMEQLCTFDJ> domains = emeqlctfdjService.searchDefault(context) ;
        List<EMEQLCTFDJDTO> list = emeqlctfdjMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTFDJ-searchDefault-all') and hasPermission(#context,'eam-EMEQLCTFDJ-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"发动机位置" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqlctfdjs/searchdefault")
	public ResponseEntity<Page<EMEQLCTFDJDTO>> searchDefault(@RequestBody EMEQLCTFDJSearchContext context) {
        Page<EMEQLCTFDJ> domains = emeqlctfdjService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqlctfdjMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

