package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMResEmp;
import cn.ibizlab.eam.webapi.dto.EMResEmpDTO;
import cn.ibizlab.eam.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="WebApiEMResEmpMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface EMResEmpMapping extends MappingBase<EMResEmpDTO, EMResEmp> {


}

