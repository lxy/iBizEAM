package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEICamHist;
import cn.ibizlab.eam.core.eam_core.service.IEMEICamHistService;
import cn.ibizlab.eam.core.eam_core.filter.EMEICamHistSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"探头检修记录" })
@RestController("WebApi-emeicamhist")
@RequestMapping("")
public class EMEICamHistResource {

    @Autowired
    public IEMEICamHistService emeicamhistService;

    @Autowired
    @Lazy
    public EMEICamHistMapping emeicamhistMapping;

    @PreAuthorize("hasPermission(this.emeicamhistMapping.toDomain(#emeicamhistdto),'eam-EMEICamHist-Create')")
    @ApiOperation(value = "新建探头检修记录", tags = {"探头检修记录" },  notes = "新建探头检修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicamhists")
    public ResponseEntity<EMEICamHistDTO> create(@Validated @RequestBody EMEICamHistDTO emeicamhistdto) {
        EMEICamHist domain = emeicamhistMapping.toDomain(emeicamhistdto);
		emeicamhistService.create(domain);
        EMEICamHistDTO dto = emeicamhistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeicamhistMapping.toDomain(#emeicamhistdtos),'eam-EMEICamHist-Create')")
    @ApiOperation(value = "批量新建探头检修记录", tags = {"探头检修记录" },  notes = "批量新建探头检修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicamhists/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEICamHistDTO> emeicamhistdtos) {
        emeicamhistService.createBatch(emeicamhistMapping.toDomain(emeicamhistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeicamhist" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeicamhistService.get(#emeicamhist_id),'eam-EMEICamHist-Update')")
    @ApiOperation(value = "更新探头检修记录", tags = {"探头检修记录" },  notes = "更新探头检修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeicamhists/{emeicamhist_id}")
    public ResponseEntity<EMEICamHistDTO> update(@PathVariable("emeicamhist_id") String emeicamhist_id, @RequestBody EMEICamHistDTO emeicamhistdto) {
		EMEICamHist domain  = emeicamhistMapping.toDomain(emeicamhistdto);
        domain .setEmeicamhistid(emeicamhist_id);
		emeicamhistService.update(domain );
		EMEICamHistDTO dto = emeicamhistMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeicamhistService.getEmeicamhistByEntities(this.emeicamhistMapping.toDomain(#emeicamhistdtos)),'eam-EMEICamHist-Update')")
    @ApiOperation(value = "批量更新探头检修记录", tags = {"探头检修记录" },  notes = "批量更新探头检修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeicamhists/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEICamHistDTO> emeicamhistdtos) {
        emeicamhistService.updateBatch(emeicamhistMapping.toDomain(emeicamhistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeicamhistService.get(#emeicamhist_id),'eam-EMEICamHist-Remove')")
    @ApiOperation(value = "删除探头检修记录", tags = {"探头检修记录" },  notes = "删除探头检修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeicamhists/{emeicamhist_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeicamhist_id") String emeicamhist_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeicamhistService.remove(emeicamhist_id));
    }

    @PreAuthorize("hasPermission(this.emeicamhistService.getEmeicamhistByIds(#ids),'eam-EMEICamHist-Remove')")
    @ApiOperation(value = "批量删除探头检修记录", tags = {"探头检修记录" },  notes = "批量删除探头检修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeicamhists/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeicamhistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeicamhistMapping.toDomain(returnObject.body),'eam-EMEICamHist-Get')")
    @ApiOperation(value = "获取探头检修记录", tags = {"探头检修记录" },  notes = "获取探头检修记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emeicamhists/{emeicamhist_id}")
    public ResponseEntity<EMEICamHistDTO> get(@PathVariable("emeicamhist_id") String emeicamhist_id) {
        EMEICamHist domain = emeicamhistService.get(emeicamhist_id);
        EMEICamHistDTO dto = emeicamhistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取探头检修记录草稿", tags = {"探头检修记录" },  notes = "获取探头检修记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeicamhists/getdraft")
    public ResponseEntity<EMEICamHistDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emeicamhistMapping.toDto(emeicamhistService.getDraft(new EMEICamHist())));
    }

    @ApiOperation(value = "检查探头检修记录", tags = {"探头检修记录" },  notes = "检查探头检修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicamhists/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEICamHistDTO emeicamhistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeicamhistService.checkKey(emeicamhistMapping.toDomain(emeicamhistdto)));
    }

    @PreAuthorize("hasPermission(this.emeicamhistMapping.toDomain(#emeicamhistdto),'eam-EMEICamHist-Save')")
    @ApiOperation(value = "保存探头检修记录", tags = {"探头检修记录" },  notes = "保存探头检修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicamhists/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEICamHistDTO emeicamhistdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emeicamhistService.save(emeicamhistMapping.toDomain(emeicamhistdto)));
    }

    @PreAuthorize("hasPermission(this.emeicamhistMapping.toDomain(#emeicamhistdtos),'eam-EMEICamHist-Save')")
    @ApiOperation(value = "批量保存探头检修记录", tags = {"探头检修记录" },  notes = "批量保存探头检修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicamhists/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEICamHistDTO> emeicamhistdtos) {
        emeicamhistService.saveBatch(emeicamhistMapping.toDomain(emeicamhistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEICamHist-searchDefault-all') and hasPermission(#context,'eam-EMEICamHist-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"探头检修记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeicamhists/fetchdefault")
	public ResponseEntity<List<EMEICamHistDTO>> fetchDefault(EMEICamHistSearchContext context) {
        Page<EMEICamHist> domains = emeicamhistService.searchDefault(context) ;
        List<EMEICamHistDTO> list = emeicamhistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEICamHist-searchDefault-all') and hasPermission(#context,'eam-EMEICamHist-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"探头检修记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeicamhists/searchdefault")
	public ResponseEntity<Page<EMEICamHistDTO>> searchDefault(@RequestBody EMEICamHistSearchContext context) {
        Page<EMEICamHist> domains = emeicamhistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeicamhistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

