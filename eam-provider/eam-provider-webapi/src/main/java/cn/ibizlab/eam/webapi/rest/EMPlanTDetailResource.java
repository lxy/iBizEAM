package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMPlanTDetail;
import cn.ibizlab.eam.core.eam_core.service.IEMPlanTDetailService;
import cn.ibizlab.eam.core.eam_core.filter.EMPlanTDetailSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"计划模板步骤" })
@RestController("WebApi-emplantdetail")
@RequestMapping("")
public class EMPlanTDetailResource {

    @Autowired
    public IEMPlanTDetailService emplantdetailService;

    @Autowired
    @Lazy
    public EMPlanTDetailMapping emplantdetailMapping;

    @PreAuthorize("hasPermission(this.emplantdetailMapping.toDomain(#emplantdetaildto),'eam-EMPlanTDetail-Create')")
    @ApiOperation(value = "新建计划模板步骤", tags = {"计划模板步骤" },  notes = "新建计划模板步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantdetails")
    public ResponseEntity<EMPlanTDetailDTO> create(@Validated @RequestBody EMPlanTDetailDTO emplantdetaildto) {
        EMPlanTDetail domain = emplantdetailMapping.toDomain(emplantdetaildto);
		emplantdetailService.create(domain);
        EMPlanTDetailDTO dto = emplantdetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplantdetailMapping.toDomain(#emplantdetaildtos),'eam-EMPlanTDetail-Create')")
    @ApiOperation(value = "批量新建计划模板步骤", tags = {"计划模板步骤" },  notes = "批量新建计划模板步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantdetails/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMPlanTDetailDTO> emplantdetaildtos) {
        emplantdetailService.createBatch(emplantdetailMapping.toDomain(emplantdetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplantdetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplantdetailService.get(#emplantdetail_id),'eam-EMPlanTDetail-Update')")
    @ApiOperation(value = "更新计划模板步骤", tags = {"计划模板步骤" },  notes = "更新计划模板步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplantdetails/{emplantdetail_id}")
    public ResponseEntity<EMPlanTDetailDTO> update(@PathVariable("emplantdetail_id") String emplantdetail_id, @RequestBody EMPlanTDetailDTO emplantdetaildto) {
		EMPlanTDetail domain  = emplantdetailMapping.toDomain(emplantdetaildto);
        domain .setEmplantdetailid(emplantdetail_id);
		emplantdetailService.update(domain );
		EMPlanTDetailDTO dto = emplantdetailMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplantdetailService.getEmplantdetailByEntities(this.emplantdetailMapping.toDomain(#emplantdetaildtos)),'eam-EMPlanTDetail-Update')")
    @ApiOperation(value = "批量更新计划模板步骤", tags = {"计划模板步骤" },  notes = "批量更新计划模板步骤")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplantdetails/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMPlanTDetailDTO> emplantdetaildtos) {
        emplantdetailService.updateBatch(emplantdetailMapping.toDomain(emplantdetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplantdetailService.get(#emplantdetail_id),'eam-EMPlanTDetail-Remove')")
    @ApiOperation(value = "删除计划模板步骤", tags = {"计划模板步骤" },  notes = "删除计划模板步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplantdetails/{emplantdetail_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emplantdetail_id") String emplantdetail_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emplantdetailService.remove(emplantdetail_id));
    }

    @PreAuthorize("hasPermission(this.emplantdetailService.getEmplantdetailByIds(#ids),'eam-EMPlanTDetail-Remove')")
    @ApiOperation(value = "批量删除计划模板步骤", tags = {"计划模板步骤" },  notes = "批量删除计划模板步骤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplantdetails/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emplantdetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplantdetailMapping.toDomain(returnObject.body),'eam-EMPlanTDetail-Get')")
    @ApiOperation(value = "获取计划模板步骤", tags = {"计划模板步骤" },  notes = "获取计划模板步骤")
	@RequestMapping(method = RequestMethod.GET, value = "/emplantdetails/{emplantdetail_id}")
    public ResponseEntity<EMPlanTDetailDTO> get(@PathVariable("emplantdetail_id") String emplantdetail_id) {
        EMPlanTDetail domain = emplantdetailService.get(emplantdetail_id);
        EMPlanTDetailDTO dto = emplantdetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取计划模板步骤草稿", tags = {"计划模板步骤" },  notes = "获取计划模板步骤草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emplantdetails/getdraft")
    public ResponseEntity<EMPlanTDetailDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emplantdetailMapping.toDto(emplantdetailService.getDraft(new EMPlanTDetail())));
    }

    @ApiOperation(value = "检查计划模板步骤", tags = {"计划模板步骤" },  notes = "检查计划模板步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantdetails/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMPlanTDetailDTO emplantdetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplantdetailService.checkKey(emplantdetailMapping.toDomain(emplantdetaildto)));
    }

    @PreAuthorize("hasPermission(this.emplantdetailMapping.toDomain(#emplantdetaildto),'eam-EMPlanTDetail-Save')")
    @ApiOperation(value = "保存计划模板步骤", tags = {"计划模板步骤" },  notes = "保存计划模板步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantdetails/save")
    public ResponseEntity<Boolean> save(@RequestBody EMPlanTDetailDTO emplantdetaildto) {
        return ResponseEntity.status(HttpStatus.OK).body(emplantdetailService.save(emplantdetailMapping.toDomain(emplantdetaildto)));
    }

    @PreAuthorize("hasPermission(this.emplantdetailMapping.toDomain(#emplantdetaildtos),'eam-EMPlanTDetail-Save')")
    @ApiOperation(value = "批量保存计划模板步骤", tags = {"计划模板步骤" },  notes = "批量保存计划模板步骤")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantdetails/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMPlanTDetailDTO> emplantdetaildtos) {
        emplantdetailService.saveBatch(emplantdetailMapping.toDomain(emplantdetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanTDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanTDetail-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"计划模板步骤" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emplantdetails/fetchdefault")
	public ResponseEntity<List<EMPlanTDetailDTO>> fetchDefault(EMPlanTDetailSearchContext context) {
        Page<EMPlanTDetail> domains = emplantdetailService.searchDefault(context) ;
        List<EMPlanTDetailDTO> list = emplantdetailMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanTDetail-searchDefault-all') and hasPermission(#context,'eam-EMPlanTDetail-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"计划模板步骤" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emplantdetails/searchdefault")
	public ResponseEntity<Page<EMPlanTDetailDTO>> searchDefault(@RequestBody EMPlanTDetailSearchContext context) {
        Page<EMPlanTDetail> domains = emplantdetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplantdetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

