package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMRFOMO;
import cn.ibizlab.eam.core.eam_core.service.IEMRFOMOService;
import cn.ibizlab.eam.core.eam_core.filter.EMRFOMOSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"模式" })
@RestController("WebApi-emrfomo")
@RequestMapping("")
public class EMRFOMOResource {

    @Autowired
    public IEMRFOMOService emrfomoService;

    @Autowired
    @Lazy
    public EMRFOMOMapping emrfomoMapping;

    @PreAuthorize("hasPermission(this.emrfomoMapping.toDomain(#emrfomodto),'eam-EMRFOMO-Create')")
    @ApiOperation(value = "新建模式", tags = {"模式" },  notes = "新建模式")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfomos")
    public ResponseEntity<EMRFOMODTO> create(@Validated @RequestBody EMRFOMODTO emrfomodto) {
        EMRFOMO domain = emrfomoMapping.toDomain(emrfomodto);
		emrfomoService.create(domain);
        EMRFOMODTO dto = emrfomoMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfomoMapping.toDomain(#emrfomodtos),'eam-EMRFOMO-Create')")
    @ApiOperation(value = "批量新建模式", tags = {"模式" },  notes = "批量新建模式")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfomos/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMRFOMODTO> emrfomodtos) {
        emrfomoService.createBatch(emrfomoMapping.toDomain(emrfomodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emrfomo" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emrfomoService.get(#emrfomo_id),'eam-EMRFOMO-Update')")
    @ApiOperation(value = "更新模式", tags = {"模式" },  notes = "更新模式")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfomos/{emrfomo_id}")
    public ResponseEntity<EMRFOMODTO> update(@PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOMODTO emrfomodto) {
		EMRFOMO domain  = emrfomoMapping.toDomain(emrfomodto);
        domain .setEmrfomoid(emrfomo_id);
		emrfomoService.update(domain );
		EMRFOMODTO dto = emrfomoMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfomoService.getEmrfomoByEntities(this.emrfomoMapping.toDomain(#emrfomodtos)),'eam-EMRFOMO-Update')")
    @ApiOperation(value = "批量更新模式", tags = {"模式" },  notes = "批量更新模式")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfomos/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMRFOMODTO> emrfomodtos) {
        emrfomoService.updateBatch(emrfomoMapping.toDomain(emrfomodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emrfomoService.get(#emrfomo_id),'eam-EMRFOMO-Remove')")
    @ApiOperation(value = "删除模式", tags = {"模式" },  notes = "删除模式")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfomos/{emrfomo_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emrfomo_id") String emrfomo_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emrfomoService.remove(emrfomo_id));
    }

    @PreAuthorize("hasPermission(this.emrfomoService.getEmrfomoByIds(#ids),'eam-EMRFOMO-Remove')")
    @ApiOperation(value = "批量删除模式", tags = {"模式" },  notes = "批量删除模式")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfomos/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emrfomoService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emrfomoMapping.toDomain(returnObject.body),'eam-EMRFOMO-Get')")
    @ApiOperation(value = "获取模式", tags = {"模式" },  notes = "获取模式")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfomos/{emrfomo_id}")
    public ResponseEntity<EMRFOMODTO> get(@PathVariable("emrfomo_id") String emrfomo_id) {
        EMRFOMO domain = emrfomoService.get(emrfomo_id);
        EMRFOMODTO dto = emrfomoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取模式草稿", tags = {"模式" },  notes = "获取模式草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfomos/getdraft")
    public ResponseEntity<EMRFOMODTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emrfomoMapping.toDto(emrfomoService.getDraft(new EMRFOMO())));
    }

    @ApiOperation(value = "检查模式", tags = {"模式" },  notes = "检查模式")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfomos/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMRFOMODTO emrfomodto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emrfomoService.checkKey(emrfomoMapping.toDomain(emrfomodto)));
    }

    @PreAuthorize("hasPermission(this.emrfomoMapping.toDomain(#emrfomodto),'eam-EMRFOMO-Save')")
    @ApiOperation(value = "保存模式", tags = {"模式" },  notes = "保存模式")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfomos/save")
    public ResponseEntity<Boolean> save(@RequestBody EMRFOMODTO emrfomodto) {
        return ResponseEntity.status(HttpStatus.OK).body(emrfomoService.save(emrfomoMapping.toDomain(emrfomodto)));
    }

    @PreAuthorize("hasPermission(this.emrfomoMapping.toDomain(#emrfomodtos),'eam-EMRFOMO-Save')")
    @ApiOperation(value = "批量保存模式", tags = {"模式" },  notes = "批量保存模式")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfomos/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMRFOMODTO> emrfomodtos) {
        emrfomoService.saveBatch(emrfomoMapping.toDomain(emrfomodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOMO-searchDefault-all') and hasPermission(#context,'eam-EMRFOMO-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"模式" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emrfomos/fetchdefault")
	public ResponseEntity<List<EMRFOMODTO>> fetchDefault(EMRFOMOSearchContext context) {
        Page<EMRFOMO> domains = emrfomoService.searchDefault(context) ;
        List<EMRFOMODTO> list = emrfomoMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOMO-searchDefault-all') and hasPermission(#context,'eam-EMRFOMO-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"模式" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emrfomos/searchdefault")
	public ResponseEntity<Page<EMRFOMODTO>> searchDefault(@RequestBody EMRFOMOSearchContext context) {
        Page<EMRFOMO> domains = emrfomoService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emrfomoMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emrfomoMapping.toDomain(#emrfomodto),'eam-EMRFOMO-Create')")
    @ApiOperation(value = "根据现象建立模式", tags = {"模式" },  notes = "根据现象建立模式")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfomos")
    public ResponseEntity<EMRFOMODTO> createByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody EMRFOMODTO emrfomodto) {
        EMRFOMO domain = emrfomoMapping.toDomain(emrfomodto);
        domain.setRfodeid(emrfode_id);
		emrfomoService.create(domain);
        EMRFOMODTO dto = emrfomoMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfomoMapping.toDomain(#emrfomodtos),'eam-EMRFOMO-Create')")
    @ApiOperation(value = "根据现象批量建立模式", tags = {"模式" },  notes = "根据现象批量建立模式")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfomos/batch")
    public ResponseEntity<Boolean> createBatchByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody List<EMRFOMODTO> emrfomodtos) {
        List<EMRFOMO> domainlist=emrfomoMapping.toDomain(emrfomodtos);
        for(EMRFOMO domain:domainlist){
            domain.setRfodeid(emrfode_id);
        }
        emrfomoService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emrfomo" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emrfomoService.get(#emrfomo_id),'eam-EMRFOMO-Update')")
    @ApiOperation(value = "根据现象更新模式", tags = {"模式" },  notes = "根据现象更新模式")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}")
    public ResponseEntity<EMRFOMODTO> updateByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOMODTO emrfomodto) {
        EMRFOMO domain = emrfomoMapping.toDomain(emrfomodto);
        domain.setRfodeid(emrfode_id);
        domain.setEmrfomoid(emrfomo_id);
		emrfomoService.update(domain);
        EMRFOMODTO dto = emrfomoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfomoService.getEmrfomoByEntities(this.emrfomoMapping.toDomain(#emrfomodtos)),'eam-EMRFOMO-Update')")
    @ApiOperation(value = "根据现象批量更新模式", tags = {"模式" },  notes = "根据现象批量更新模式")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodes/{emrfode_id}/emrfomos/batch")
    public ResponseEntity<Boolean> updateBatchByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody List<EMRFOMODTO> emrfomodtos) {
        List<EMRFOMO> domainlist=emrfomoMapping.toDomain(emrfomodtos);
        for(EMRFOMO domain:domainlist){
            domain.setRfodeid(emrfode_id);
        }
        emrfomoService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emrfomoService.get(#emrfomo_id),'eam-EMRFOMO-Remove')")
    @ApiOperation(value = "根据现象删除模式", tags = {"模式" },  notes = "根据现象删除模式")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}")
    public ResponseEntity<Boolean> removeByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emrfomoService.remove(emrfomo_id));
    }

    @PreAuthorize("hasPermission(this.emrfomoService.getEmrfomoByIds(#ids),'eam-EMRFOMO-Remove')")
    @ApiOperation(value = "根据现象批量删除模式", tags = {"模式" },  notes = "根据现象批量删除模式")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodes/{emrfode_id}/emrfomos/batch")
    public ResponseEntity<Boolean> removeBatchByEMRFODE(@RequestBody List<String> ids) {
        emrfomoService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emrfomoMapping.toDomain(returnObject.body),'eam-EMRFOMO-Get')")
    @ApiOperation(value = "根据现象获取模式", tags = {"模式" },  notes = "根据现象获取模式")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}")
    public ResponseEntity<EMRFOMODTO> getByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id) {
        EMRFOMO domain = emrfomoService.get(emrfomo_id);
        EMRFOMODTO dto = emrfomoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据现象获取模式草稿", tags = {"模式" },  notes = "根据现象获取模式草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emrfodes/{emrfode_id}/emrfomos/getdraft")
    public ResponseEntity<EMRFOMODTO> getDraftByEMRFODE(@PathVariable("emrfode_id") String emrfode_id) {
        EMRFOMO domain = new EMRFOMO();
        domain.setRfodeid(emrfode_id);
        return ResponseEntity.status(HttpStatus.OK).body(emrfomoMapping.toDto(emrfomoService.getDraft(domain)));
    }

    @ApiOperation(value = "根据现象检查模式", tags = {"模式" },  notes = "根据现象检查模式")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfomos/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody EMRFOMODTO emrfomodto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emrfomoService.checkKey(emrfomoMapping.toDomain(emrfomodto)));
    }

    @PreAuthorize("hasPermission(this.emrfomoMapping.toDomain(#emrfomodto),'eam-EMRFOMO-Save')")
    @ApiOperation(value = "根据现象保存模式", tags = {"模式" },  notes = "根据现象保存模式")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfomos/save")
    public ResponseEntity<Boolean> saveByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody EMRFOMODTO emrfomodto) {
        EMRFOMO domain = emrfomoMapping.toDomain(emrfomodto);
        domain.setRfodeid(emrfode_id);
        return ResponseEntity.status(HttpStatus.OK).body(emrfomoService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emrfomoMapping.toDomain(#emrfomodtos),'eam-EMRFOMO-Save')")
    @ApiOperation(value = "根据现象批量保存模式", tags = {"模式" },  notes = "根据现象批量保存模式")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfomos/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody List<EMRFOMODTO> emrfomodtos) {
        List<EMRFOMO> domainlist=emrfomoMapping.toDomain(emrfomodtos);
        for(EMRFOMO domain:domainlist){
             domain.setRfodeid(emrfode_id);
        }
        emrfomoService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOMO-searchDefault-all') and hasPermission(#context,'eam-EMRFOMO-Get')")
	@ApiOperation(value = "根据现象获取DEFAULT", tags = {"模式" } ,notes = "根据现象获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emrfodes/{emrfode_id}/emrfomos/fetchdefault")
	public ResponseEntity<List<EMRFOMODTO>> fetchEMRFOMODefaultByEMRFODE(@PathVariable("emrfode_id") String emrfode_id,EMRFOMOSearchContext context) {
        context.setN_rfodeid_eq(emrfode_id);
        Page<EMRFOMO> domains = emrfomoService.searchDefault(context) ;
        List<EMRFOMODTO> list = emrfomoMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOMO-searchDefault-all') and hasPermission(#context,'eam-EMRFOMO-Get')")
	@ApiOperation(value = "根据现象查询DEFAULT", tags = {"模式" } ,notes = "根据现象查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emrfodes/{emrfode_id}/emrfomos/searchdefault")
	public ResponseEntity<Page<EMRFOMODTO>> searchEMRFOMODefaultByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody EMRFOMOSearchContext context) {
        context.setN_rfodeid_eq(emrfode_id);
        Page<EMRFOMO> domains = emrfomoService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emrfomoMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

