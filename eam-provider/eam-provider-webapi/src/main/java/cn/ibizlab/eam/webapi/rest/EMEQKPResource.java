package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQKP;
import cn.ibizlab.eam.core.eam_core.service.IEMEQKPService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQKPSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"设备关键点" })
@RestController("WebApi-emeqkp")
@RequestMapping("")
public class EMEQKPResource {

    @Autowired
    public IEMEQKPService emeqkpService;

    @Autowired
    @Lazy
    public EMEQKPMapping emeqkpMapping;

    @PreAuthorize("hasPermission(this.emeqkpMapping.toDomain(#emeqkpdto),'eam-EMEQKP-Create')")
    @ApiOperation(value = "新建设备关键点", tags = {"设备关键点" },  notes = "新建设备关键点")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkps")
    public ResponseEntity<EMEQKPDTO> create(@Validated @RequestBody EMEQKPDTO emeqkpdto) {
        EMEQKP domain = emeqkpMapping.toDomain(emeqkpdto);
		emeqkpService.create(domain);
        EMEQKPDTO dto = emeqkpMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqkpMapping.toDomain(#emeqkpdtos),'eam-EMEQKP-Create')")
    @ApiOperation(value = "批量新建设备关键点", tags = {"设备关键点" },  notes = "批量新建设备关键点")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQKPDTO> emeqkpdtos) {
        emeqkpService.createBatch(emeqkpMapping.toDomain(emeqkpdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqkp" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqkpService.get(#emeqkp_id),'eam-EMEQKP-Update')")
    @ApiOperation(value = "更新设备关键点", tags = {"设备关键点" },  notes = "更新设备关键点")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqkps/{emeqkp_id}")
    public ResponseEntity<EMEQKPDTO> update(@PathVariable("emeqkp_id") String emeqkp_id, @RequestBody EMEQKPDTO emeqkpdto) {
		EMEQKP domain  = emeqkpMapping.toDomain(emeqkpdto);
        domain .setEmeqkpid(emeqkp_id);
		emeqkpService.update(domain );
		EMEQKPDTO dto = emeqkpMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqkpService.getEmeqkpByEntities(this.emeqkpMapping.toDomain(#emeqkpdtos)),'eam-EMEQKP-Update')")
    @ApiOperation(value = "批量更新设备关键点", tags = {"设备关键点" },  notes = "批量更新设备关键点")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqkps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQKPDTO> emeqkpdtos) {
        emeqkpService.updateBatch(emeqkpMapping.toDomain(emeqkpdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqkpService.get(#emeqkp_id),'eam-EMEQKP-Remove')")
    @ApiOperation(value = "删除设备关键点", tags = {"设备关键点" },  notes = "删除设备关键点")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqkps/{emeqkp_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqkp_id") String emeqkp_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqkpService.remove(emeqkp_id));
    }

    @PreAuthorize("hasPermission(this.emeqkpService.getEmeqkpByIds(#ids),'eam-EMEQKP-Remove')")
    @ApiOperation(value = "批量删除设备关键点", tags = {"设备关键点" },  notes = "批量删除设备关键点")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqkps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqkpService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqkpMapping.toDomain(returnObject.body),'eam-EMEQKP-Get')")
    @ApiOperation(value = "获取设备关键点", tags = {"设备关键点" },  notes = "获取设备关键点")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqkps/{emeqkp_id}")
    public ResponseEntity<EMEQKPDTO> get(@PathVariable("emeqkp_id") String emeqkp_id) {
        EMEQKP domain = emeqkpService.get(emeqkp_id);
        EMEQKPDTO dto = emeqkpMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取设备关键点草稿", tags = {"设备关键点" },  notes = "获取设备关键点草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqkps/getdraft")
    public ResponseEntity<EMEQKPDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emeqkpMapping.toDto(emeqkpService.getDraft(new EMEQKP())));
    }

    @ApiOperation(value = "检查设备关键点", tags = {"设备关键点" },  notes = "检查设备关键点")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQKPDTO emeqkpdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqkpService.checkKey(emeqkpMapping.toDomain(emeqkpdto)));
    }

    @PreAuthorize("hasPermission(this.emeqkpMapping.toDomain(#emeqkpdto),'eam-EMEQKP-Save')")
    @ApiOperation(value = "保存设备关键点", tags = {"设备关键点" },  notes = "保存设备关键点")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkps/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEQKPDTO emeqkpdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emeqkpService.save(emeqkpMapping.toDomain(emeqkpdto)));
    }

    @PreAuthorize("hasPermission(this.emeqkpMapping.toDomain(#emeqkpdtos),'eam-EMEQKP-Save')")
    @ApiOperation(value = "批量保存设备关键点", tags = {"设备关键点" },  notes = "批量保存设备关键点")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkps/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQKPDTO> emeqkpdtos) {
        emeqkpService.saveBatch(emeqkpMapping.toDomain(emeqkpdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKP-searchDefault-all') and hasPermission(#context,'eam-EMEQKP-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"设备关键点" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqkps/fetchdefault")
	public ResponseEntity<List<EMEQKPDTO>> fetchDefault(EMEQKPSearchContext context) {
        Page<EMEQKP> domains = emeqkpService.searchDefault(context) ;
        List<EMEQKPDTO> list = emeqkpMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKP-searchDefault-all') and hasPermission(#context,'eam-EMEQKP-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"设备关键点" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqkps/searchdefault")
	public ResponseEntity<Page<EMEQKPDTO>> searchDefault(@RequestBody EMEQKPSearchContext context) {
        Page<EMEQKP> domains = emeqkpService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqkpMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

