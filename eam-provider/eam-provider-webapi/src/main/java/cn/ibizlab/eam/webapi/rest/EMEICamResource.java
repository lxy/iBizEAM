package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEICam;
import cn.ibizlab.eam.core.eam_core.service.IEMEICamService;
import cn.ibizlab.eam.core.eam_core.filter.EMEICamSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"探头" })
@RestController("WebApi-emeicam")
@RequestMapping("")
public class EMEICamResource {

    @Autowired
    public IEMEICamService emeicamService;

    @Autowired
    @Lazy
    public EMEICamMapping emeicamMapping;

    @PreAuthorize("hasPermission(this.emeicamMapping.toDomain(#emeicamdto),'eam-EMEICam-Create')")
    @ApiOperation(value = "新建探头", tags = {"探头" },  notes = "新建探头")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicams")
    public ResponseEntity<EMEICamDTO> create(@Validated @RequestBody EMEICamDTO emeicamdto) {
        EMEICam domain = emeicamMapping.toDomain(emeicamdto);
		emeicamService.create(domain);
        EMEICamDTO dto = emeicamMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeicamMapping.toDomain(#emeicamdtos),'eam-EMEICam-Create')")
    @ApiOperation(value = "批量新建探头", tags = {"探头" },  notes = "批量新建探头")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicams/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEICamDTO> emeicamdtos) {
        emeicamService.createBatch(emeicamMapping.toDomain(emeicamdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeicam" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeicamService.get(#emeicam_id),'eam-EMEICam-Update')")
    @ApiOperation(value = "更新探头", tags = {"探头" },  notes = "更新探头")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeicams/{emeicam_id}")
    public ResponseEntity<EMEICamDTO> update(@PathVariable("emeicam_id") String emeicam_id, @RequestBody EMEICamDTO emeicamdto) {
		EMEICam domain  = emeicamMapping.toDomain(emeicamdto);
        domain .setEmeicamid(emeicam_id);
		emeicamService.update(domain );
		EMEICamDTO dto = emeicamMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeicamService.getEmeicamByEntities(this.emeicamMapping.toDomain(#emeicamdtos)),'eam-EMEICam-Update')")
    @ApiOperation(value = "批量更新探头", tags = {"探头" },  notes = "批量更新探头")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeicams/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEICamDTO> emeicamdtos) {
        emeicamService.updateBatch(emeicamMapping.toDomain(emeicamdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeicamService.get(#emeicam_id),'eam-EMEICam-Remove')")
    @ApiOperation(value = "删除探头", tags = {"探头" },  notes = "删除探头")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeicams/{emeicam_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeicam_id") String emeicam_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeicamService.remove(emeicam_id));
    }

    @PreAuthorize("hasPermission(this.emeicamService.getEmeicamByIds(#ids),'eam-EMEICam-Remove')")
    @ApiOperation(value = "批量删除探头", tags = {"探头" },  notes = "批量删除探头")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeicams/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeicamService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeicamMapping.toDomain(returnObject.body),'eam-EMEICam-Get')")
    @ApiOperation(value = "获取探头", tags = {"探头" },  notes = "获取探头")
	@RequestMapping(method = RequestMethod.GET, value = "/emeicams/{emeicam_id}")
    public ResponseEntity<EMEICamDTO> get(@PathVariable("emeicam_id") String emeicam_id) {
        EMEICam domain = emeicamService.get(emeicam_id);
        EMEICamDTO dto = emeicamMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取探头草稿", tags = {"探头" },  notes = "获取探头草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeicams/getdraft")
    public ResponseEntity<EMEICamDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emeicamMapping.toDto(emeicamService.getDraft(new EMEICam())));
    }

    @ApiOperation(value = "检查探头", tags = {"探头" },  notes = "检查探头")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicams/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEICamDTO emeicamdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeicamService.checkKey(emeicamMapping.toDomain(emeicamdto)));
    }

    @PreAuthorize("hasPermission(this.emeicamMapping.toDomain(#emeicamdto),'eam-EMEICam-Save')")
    @ApiOperation(value = "保存探头", tags = {"探头" },  notes = "保存探头")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicams/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEICamDTO emeicamdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emeicamService.save(emeicamMapping.toDomain(emeicamdto)));
    }

    @PreAuthorize("hasPermission(this.emeicamMapping.toDomain(#emeicamdtos),'eam-EMEICam-Save')")
    @ApiOperation(value = "批量保存探头", tags = {"探头" },  notes = "批量保存探头")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicams/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEICamDTO> emeicamdtos) {
        emeicamService.saveBatch(emeicamMapping.toDomain(emeicamdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEICam-searchDefault-all') and hasPermission(#context,'eam-EMEICam-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"探头" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeicams/fetchdefault")
	public ResponseEntity<List<EMEICamDTO>> fetchDefault(EMEICamSearchContext context) {
        Page<EMEICam> domains = emeicamService.searchDefault(context) ;
        List<EMEICamDTO> list = emeicamMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEICam-searchDefault-all') and hasPermission(#context,'eam-EMEICam-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"探头" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeicams/searchdefault")
	public ResponseEntity<Page<EMEICamDTO>> searchDefault(@RequestBody EMEICamSearchContext context) {
        Page<EMEICam> domains = emeicamService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeicamMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

