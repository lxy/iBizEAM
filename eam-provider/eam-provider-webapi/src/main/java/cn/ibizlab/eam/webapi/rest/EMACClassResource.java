package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMACClass;
import cn.ibizlab.eam.core.eam_core.service.IEMACClassService;
import cn.ibizlab.eam.core.eam_core.filter.EMACClassSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"总帐科目" })
@RestController("WebApi-emacclass")
@RequestMapping("")
public class EMACClassResource {

    @Autowired
    public IEMACClassService emacclassService;

    @Autowired
    @Lazy
    public EMACClassMapping emacclassMapping;

    @PreAuthorize("hasPermission(this.emacclassMapping.toDomain(#emacclassdto),'eam-EMACClass-Create')")
    @ApiOperation(value = "新建总帐科目", tags = {"总帐科目" },  notes = "新建总帐科目")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses")
    public ResponseEntity<EMACClassDTO> create(@Validated @RequestBody EMACClassDTO emacclassdto) {
        EMACClass domain = emacclassMapping.toDomain(emacclassdto);
		emacclassService.create(domain);
        EMACClassDTO dto = emacclassMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emacclassMapping.toDomain(#emacclassdtos),'eam-EMACClass-Create')")
    @ApiOperation(value = "批量新建总帐科目", tags = {"总帐科目" },  notes = "批量新建总帐科目")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMACClassDTO> emacclassdtos) {
        emacclassService.createBatch(emacclassMapping.toDomain(emacclassdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emacclass" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emacclassService.get(#emacclass_id),'eam-EMACClass-Update')")
    @ApiOperation(value = "更新总帐科目", tags = {"总帐科目" },  notes = "更新总帐科目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}")
    public ResponseEntity<EMACClassDTO> update(@PathVariable("emacclass_id") String emacclass_id, @RequestBody EMACClassDTO emacclassdto) {
		EMACClass domain  = emacclassMapping.toDomain(emacclassdto);
        domain .setEmacclassid(emacclass_id);
		emacclassService.update(domain );
		EMACClassDTO dto = emacclassMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emacclassService.getEmacclassByEntities(this.emacclassMapping.toDomain(#emacclassdtos)),'eam-EMACClass-Update')")
    @ApiOperation(value = "批量更新总帐科目", tags = {"总帐科目" },  notes = "批量更新总帐科目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMACClassDTO> emacclassdtos) {
        emacclassService.updateBatch(emacclassMapping.toDomain(emacclassdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emacclassService.get(#emacclass_id),'eam-EMACClass-Remove')")
    @ApiOperation(value = "删除总帐科目", tags = {"总帐科目" },  notes = "删除总帐科目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emacclass_id") String emacclass_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emacclassService.remove(emacclass_id));
    }

    @PreAuthorize("hasPermission(this.emacclassService.getEmacclassByIds(#ids),'eam-EMACClass-Remove')")
    @ApiOperation(value = "批量删除总帐科目", tags = {"总帐科目" },  notes = "批量删除总帐科目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emacclassService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emacclassMapping.toDomain(returnObject.body),'eam-EMACClass-Get')")
    @ApiOperation(value = "获取总帐科目", tags = {"总帐科目" },  notes = "获取总帐科目")
	@RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}")
    public ResponseEntity<EMACClassDTO> get(@PathVariable("emacclass_id") String emacclass_id) {
        EMACClass domain = emacclassService.get(emacclass_id);
        EMACClassDTO dto = emacclassMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取总帐科目草稿", tags = {"总帐科目" },  notes = "获取总帐科目草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emacclasses/getdraft")
    public ResponseEntity<EMACClassDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emacclassMapping.toDto(emacclassService.getDraft(new EMACClass())));
    }

    @ApiOperation(value = "检查总帐科目", tags = {"总帐科目" },  notes = "检查总帐科目")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMACClassDTO emacclassdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emacclassService.checkKey(emacclassMapping.toDomain(emacclassdto)));
    }

    @PreAuthorize("hasPermission(this.emacclassMapping.toDomain(#emacclassdto),'eam-EMACClass-Save')")
    @ApiOperation(value = "保存总帐科目", tags = {"总帐科目" },  notes = "保存总帐科目")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/save")
    public ResponseEntity<Boolean> save(@RequestBody EMACClassDTO emacclassdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emacclassService.save(emacclassMapping.toDomain(emacclassdto)));
    }

    @PreAuthorize("hasPermission(this.emacclassMapping.toDomain(#emacclassdtos),'eam-EMACClass-Save')")
    @ApiOperation(value = "批量保存总帐科目", tags = {"总帐科目" },  notes = "批量保存总帐科目")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMACClassDTO> emacclassdtos) {
        emacclassService.saveBatch(emacclassMapping.toDomain(emacclassdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMACClass-searchDefault-all') and hasPermission(#context,'eam-EMACClass-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"总帐科目" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emacclasses/fetchdefault")
	public ResponseEntity<List<EMACClassDTO>> fetchDefault(EMACClassSearchContext context) {
        Page<EMACClass> domains = emacclassService.searchDefault(context) ;
        List<EMACClassDTO> list = emacclassMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMACClass-searchDefault-all') and hasPermission(#context,'eam-EMACClass-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"总帐科目" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emacclasses/searchdefault")
	public ResponseEntity<Page<EMACClassDTO>> searchDefault(@RequestBody EMACClassSearchContext context) {
        Page<EMACClass> domains = emacclassService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emacclassMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

