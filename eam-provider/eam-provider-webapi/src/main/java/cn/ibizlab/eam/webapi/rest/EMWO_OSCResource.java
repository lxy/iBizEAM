package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_OSC;
import cn.ibizlab.eam.core.eam_core.service.IEMWO_OSCService;
import cn.ibizlab.eam.core.eam_core.filter.EMWO_OSCSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"外委保养工单" })
@RestController("WebApi-emwo_osc")
@RequestMapping("")
public class EMWO_OSCResource {

    @Autowired
    public IEMWO_OSCService emwo_oscService;

    @Autowired
    @Lazy
    public EMWO_OSCMapping emwo_oscMapping;

    @PreAuthorize("hasPermission(this.emwo_oscMapping.toDomain(#emwo_oscdto),'eam-EMWO_OSC-Create')")
    @ApiOperation(value = "新建外委保养工单", tags = {"外委保养工单" },  notes = "新建外委保养工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_oscs")
    public ResponseEntity<EMWO_OSCDTO> create(@Validated @RequestBody EMWO_OSCDTO emwo_oscdto) {
        EMWO_OSC domain = emwo_oscMapping.toDomain(emwo_oscdto);
		emwo_oscService.create(domain);
        EMWO_OSCDTO dto = emwo_oscMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_oscMapping.toDomain(#emwo_oscdtos),'eam-EMWO_OSC-Create')")
    @ApiOperation(value = "批量新建外委保养工单", tags = {"外委保养工单" },  notes = "批量新建外委保养工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_oscs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMWO_OSCDTO> emwo_oscdtos) {
        emwo_oscService.createBatch(emwo_oscMapping.toDomain(emwo_oscdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo_osc" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwo_oscService.get(#emwo_osc_id),'eam-EMWO_OSC-Update')")
    @ApiOperation(value = "更新外委保养工单", tags = {"外委保养工单" },  notes = "更新外委保养工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwo_oscs/{emwo_osc_id}")
    public ResponseEntity<EMWO_OSCDTO> update(@PathVariable("emwo_osc_id") String emwo_osc_id, @RequestBody EMWO_OSCDTO emwo_oscdto) {
		EMWO_OSC domain  = emwo_oscMapping.toDomain(emwo_oscdto);
        domain .setEmwoOscid(emwo_osc_id);
		emwo_oscService.update(domain );
		EMWO_OSCDTO dto = emwo_oscMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_oscService.getEmwoOscByEntities(this.emwo_oscMapping.toDomain(#emwo_oscdtos)),'eam-EMWO_OSC-Update')")
    @ApiOperation(value = "批量更新外委保养工单", tags = {"外委保养工单" },  notes = "批量更新外委保养工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwo_oscs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMWO_OSCDTO> emwo_oscdtos) {
        emwo_oscService.updateBatch(emwo_oscMapping.toDomain(emwo_oscdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwo_oscService.get(#emwo_osc_id),'eam-EMWO_OSC-Remove')")
    @ApiOperation(value = "删除外委保养工单", tags = {"外委保养工单" },  notes = "删除外委保养工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwo_oscs/{emwo_osc_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emwo_osc_id") String emwo_osc_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emwo_oscService.remove(emwo_osc_id));
    }

    @PreAuthorize("hasPermission(this.emwo_oscService.getEmwoOscByIds(#ids),'eam-EMWO_OSC-Remove')")
    @ApiOperation(value = "批量删除外委保养工单", tags = {"外委保养工单" },  notes = "批量删除外委保养工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwo_oscs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emwo_oscService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwo_oscMapping.toDomain(returnObject.body),'eam-EMWO_OSC-Get')")
    @ApiOperation(value = "获取外委保养工单", tags = {"外委保养工单" },  notes = "获取外委保养工单")
	@RequestMapping(method = RequestMethod.GET, value = "/emwo_oscs/{emwo_osc_id}")
    public ResponseEntity<EMWO_OSCDTO> get(@PathVariable("emwo_osc_id") String emwo_osc_id) {
        EMWO_OSC domain = emwo_oscService.get(emwo_osc_id);
        EMWO_OSCDTO dto = emwo_oscMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取外委保养工单草稿", tags = {"外委保养工单" },  notes = "获取外委保养工单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emwo_oscs/getdraft")
    public ResponseEntity<EMWO_OSCDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emwo_oscMapping.toDto(emwo_oscService.getDraft(new EMWO_OSC())));
    }

    @ApiOperation(value = "检查外委保养工单", tags = {"外委保养工单" },  notes = "检查外委保养工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_oscs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMWO_OSCDTO emwo_oscdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwo_oscService.checkKey(emwo_oscMapping.toDomain(emwo_oscdto)));
    }

    @PreAuthorize("hasPermission(this.emwo_oscMapping.toDomain(#emwo_oscdto),'eam-EMWO_OSC-Save')")
    @ApiOperation(value = "保存外委保养工单", tags = {"外委保养工单" },  notes = "保存外委保养工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_oscs/save")
    public ResponseEntity<Boolean> save(@RequestBody EMWO_OSCDTO emwo_oscdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emwo_oscService.save(emwo_oscMapping.toDomain(emwo_oscdto)));
    }

    @PreAuthorize("hasPermission(this.emwo_oscMapping.toDomain(#emwo_oscdtos),'eam-EMWO_OSC-Save')")
    @ApiOperation(value = "批量保存外委保养工单", tags = {"外委保养工单" },  notes = "批量保存外委保养工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_oscs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMWO_OSCDTO> emwo_oscdtos) {
        emwo_oscService.saveBatch(emwo_oscMapping.toDomain(emwo_oscdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_OSC-searchCalendar-all') and hasPermission(#context,'eam-EMWO_OSC-Get')")
	@ApiOperation(value = "获取日历", tags = {"外委保养工单" } ,notes = "获取日历")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_oscs/fetchcalendar")
	public ResponseEntity<List<EMWO_OSCDTO>> fetchCalendar(EMWO_OSCSearchContext context) {
        Page<EMWO_OSC> domains = emwo_oscService.searchCalendar(context) ;
        List<EMWO_OSCDTO> list = emwo_oscMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_OSC-searchCalendar-all') and hasPermission(#context,'eam-EMWO_OSC-Get')")
	@ApiOperation(value = "查询日历", tags = {"外委保养工单" } ,notes = "查询日历")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_oscs/searchcalendar")
	public ResponseEntity<Page<EMWO_OSCDTO>> searchCalendar(@RequestBody EMWO_OSCSearchContext context) {
        Page<EMWO_OSC> domains = emwo_oscService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_oscMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_OSC-searchDefault-all') and hasPermission(#context,'eam-EMWO_OSC-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"外委保养工单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_oscs/fetchdefault")
	public ResponseEntity<List<EMWO_OSCDTO>> fetchDefault(EMWO_OSCSearchContext context) {
        Page<EMWO_OSC> domains = emwo_oscService.searchDefault(context) ;
        List<EMWO_OSCDTO> list = emwo_oscMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_OSC-searchDefault-all') and hasPermission(#context,'eam-EMWO_OSC-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"外委保养工单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_oscs/searchdefault")
	public ResponseEntity<Page<EMWO_OSCDTO>> searchDefault(@RequestBody EMWO_OSCSearchContext context) {
        Page<EMWO_OSC> domains = emwo_oscService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_oscMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwo_oscMapping.toDomain(#emwo_oscdto),'eam-EMWO_OSC-Create')")
    @ApiOperation(value = "根据设备档案建立外委保养工单", tags = {"外委保养工单" },  notes = "根据设备档案建立外委保养工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_oscs")
    public ResponseEntity<EMWO_OSCDTO> createByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_OSCDTO emwo_oscdto) {
        EMWO_OSC domain = emwo_oscMapping.toDomain(emwo_oscdto);
        domain.setEquipid(emequip_id);
		emwo_oscService.create(domain);
        EMWO_OSCDTO dto = emwo_oscMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_oscMapping.toDomain(#emwo_oscdtos),'eam-EMWO_OSC-Create')")
    @ApiOperation(value = "根据设备档案批量建立外委保养工单", tags = {"外委保养工单" },  notes = "根据设备档案批量建立外委保养工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_oscs/batch")
    public ResponseEntity<Boolean> createBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_OSCDTO> emwo_oscdtos) {
        List<EMWO_OSC> domainlist=emwo_oscMapping.toDomain(emwo_oscdtos);
        for(EMWO_OSC domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_oscService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo_osc" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwo_oscService.get(#emwo_osc_id),'eam-EMWO_OSC-Update')")
    @ApiOperation(value = "根据设备档案更新外委保养工单", tags = {"外委保养工单" },  notes = "根据设备档案更新外委保养工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emwo_oscs/{emwo_osc_id}")
    public ResponseEntity<EMWO_OSCDTO> updateByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_osc_id") String emwo_osc_id, @RequestBody EMWO_OSCDTO emwo_oscdto) {
        EMWO_OSC domain = emwo_oscMapping.toDomain(emwo_oscdto);
        domain.setEquipid(emequip_id);
        domain.setEmwoOscid(emwo_osc_id);
		emwo_oscService.update(domain);
        EMWO_OSCDTO dto = emwo_oscMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_oscService.getEmwoOscByEntities(this.emwo_oscMapping.toDomain(#emwo_oscdtos)),'eam-EMWO_OSC-Update')")
    @ApiOperation(value = "根据设备档案批量更新外委保养工单", tags = {"外委保养工单" },  notes = "根据设备档案批量更新外委保养工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emwo_oscs/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_OSCDTO> emwo_oscdtos) {
        List<EMWO_OSC> domainlist=emwo_oscMapping.toDomain(emwo_oscdtos);
        for(EMWO_OSC domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_oscService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwo_oscService.get(#emwo_osc_id),'eam-EMWO_OSC-Remove')")
    @ApiOperation(value = "根据设备档案删除外委保养工单", tags = {"外委保养工单" },  notes = "根据设备档案删除外委保养工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emwo_oscs/{emwo_osc_id}")
    public ResponseEntity<Boolean> removeByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_osc_id") String emwo_osc_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwo_oscService.remove(emwo_osc_id));
    }

    @PreAuthorize("hasPermission(this.emwo_oscService.getEmwoOscByIds(#ids),'eam-EMWO_OSC-Remove')")
    @ApiOperation(value = "根据设备档案批量删除外委保养工单", tags = {"外委保养工单" },  notes = "根据设备档案批量删除外委保养工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emwo_oscs/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquip(@RequestBody List<String> ids) {
        emwo_oscService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwo_oscMapping.toDomain(returnObject.body),'eam-EMWO_OSC-Get')")
    @ApiOperation(value = "根据设备档案获取外委保养工单", tags = {"外委保养工单" },  notes = "根据设备档案获取外委保养工单")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emwo_oscs/{emwo_osc_id}")
    public ResponseEntity<EMWO_OSCDTO> getByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_osc_id") String emwo_osc_id) {
        EMWO_OSC domain = emwo_oscService.get(emwo_osc_id);
        EMWO_OSCDTO dto = emwo_oscMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案获取外委保养工单草稿", tags = {"外委保养工单" },  notes = "根据设备档案获取外委保养工单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emwo_oscs/getdraft")
    public ResponseEntity<EMWO_OSCDTO> getDraftByEMEquip(@PathVariable("emequip_id") String emequip_id) {
        EMWO_OSC domain = new EMWO_OSC();
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_oscMapping.toDto(emwo_oscService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案检查外委保养工单", tags = {"外委保养工单" },  notes = "根据设备档案检查外委保养工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_oscs/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_OSCDTO emwo_oscdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwo_oscService.checkKey(emwo_oscMapping.toDomain(emwo_oscdto)));
    }

    @PreAuthorize("hasPermission(this.emwo_oscMapping.toDomain(#emwo_oscdto),'eam-EMWO_OSC-Save')")
    @ApiOperation(value = "根据设备档案保存外委保养工单", tags = {"外委保养工单" },  notes = "根据设备档案保存外委保养工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_oscs/save")
    public ResponseEntity<Boolean> saveByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_OSCDTO emwo_oscdto) {
        EMWO_OSC domain = emwo_oscMapping.toDomain(emwo_oscdto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_oscService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emwo_oscMapping.toDomain(#emwo_oscdtos),'eam-EMWO_OSC-Save')")
    @ApiOperation(value = "根据设备档案批量保存外委保养工单", tags = {"外委保养工单" },  notes = "根据设备档案批量保存外委保养工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_oscs/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_OSCDTO> emwo_oscdtos) {
        List<EMWO_OSC> domainlist=emwo_oscMapping.toDomain(emwo_oscdtos);
        for(EMWO_OSC domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emwo_oscService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_OSC-searchCalendar-all') and hasPermission(#context,'eam-EMWO_OSC-Get')")
	@ApiOperation(value = "根据设备档案获取日历", tags = {"外委保养工单" } ,notes = "根据设备档案获取日历")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_oscs/fetchcalendar")
	public ResponseEntity<List<EMWO_OSCDTO>> fetchEMWO_OSCCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_OSCSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_OSC> domains = emwo_oscService.searchCalendar(context) ;
        List<EMWO_OSCDTO> list = emwo_oscMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_OSC-searchCalendar-all') and hasPermission(#context,'eam-EMWO_OSC-Get')")
	@ApiOperation(value = "根据设备档案查询日历", tags = {"外委保养工单" } ,notes = "根据设备档案查询日历")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_oscs/searchcalendar")
	public ResponseEntity<Page<EMWO_OSCDTO>> searchEMWO_OSCCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_OSCSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_OSC> domains = emwo_oscService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_oscMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_OSC-searchDefault-all') and hasPermission(#context,'eam-EMWO_OSC-Get')")
	@ApiOperation(value = "根据设备档案获取DEFAULT", tags = {"外委保养工单" } ,notes = "根据设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_oscs/fetchdefault")
	public ResponseEntity<List<EMWO_OSCDTO>> fetchEMWO_OSCDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_OSCSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_OSC> domains = emwo_oscService.searchDefault(context) ;
        List<EMWO_OSCDTO> list = emwo_oscMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_OSC-searchDefault-all') and hasPermission(#context,'eam-EMWO_OSC-Get')")
	@ApiOperation(value = "根据设备档案查询DEFAULT", tags = {"外委保养工单" } ,notes = "根据设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_oscs/searchdefault")
	public ResponseEntity<Page<EMWO_OSCDTO>> searchEMWO_OSCDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_OSCSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_OSC> domains = emwo_oscService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_oscMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwo_oscMapping.toDomain(#emwo_oscdto),'eam-EMWO_OSC-Create')")
    @ApiOperation(value = "根据班组设备档案建立外委保养工单", tags = {"外委保养工单" },  notes = "根据班组设备档案建立外委保养工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_oscs")
    public ResponseEntity<EMWO_OSCDTO> createByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_OSCDTO emwo_oscdto) {
        EMWO_OSC domain = emwo_oscMapping.toDomain(emwo_oscdto);
        domain.setEquipid(emequip_id);
		emwo_oscService.create(domain);
        EMWO_OSCDTO dto = emwo_oscMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_oscMapping.toDomain(#emwo_oscdtos),'eam-EMWO_OSC-Create')")
    @ApiOperation(value = "根据班组设备档案批量建立外委保养工单", tags = {"外委保养工单" },  notes = "根据班组设备档案批量建立外委保养工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_oscs/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_OSCDTO> emwo_oscdtos) {
        List<EMWO_OSC> domainlist=emwo_oscMapping.toDomain(emwo_oscdtos);
        for(EMWO_OSC domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_oscService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo_osc" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwo_oscService.get(#emwo_osc_id),'eam-EMWO_OSC-Update')")
    @ApiOperation(value = "根据班组设备档案更新外委保养工单", tags = {"外委保养工单" },  notes = "根据班组设备档案更新外委保养工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_oscs/{emwo_osc_id}")
    public ResponseEntity<EMWO_OSCDTO> updateByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_osc_id") String emwo_osc_id, @RequestBody EMWO_OSCDTO emwo_oscdto) {
        EMWO_OSC domain = emwo_oscMapping.toDomain(emwo_oscdto);
        domain.setEquipid(emequip_id);
        domain.setEmwoOscid(emwo_osc_id);
		emwo_oscService.update(domain);
        EMWO_OSCDTO dto = emwo_oscMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_oscService.getEmwoOscByEntities(this.emwo_oscMapping.toDomain(#emwo_oscdtos)),'eam-EMWO_OSC-Update')")
    @ApiOperation(value = "根据班组设备档案批量更新外委保养工单", tags = {"外委保养工单" },  notes = "根据班组设备档案批量更新外委保养工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_oscs/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_OSCDTO> emwo_oscdtos) {
        List<EMWO_OSC> domainlist=emwo_oscMapping.toDomain(emwo_oscdtos);
        for(EMWO_OSC domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_oscService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwo_oscService.get(#emwo_osc_id),'eam-EMWO_OSC-Remove')")
    @ApiOperation(value = "根据班组设备档案删除外委保养工单", tags = {"外委保养工单" },  notes = "根据班组设备档案删除外委保养工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_oscs/{emwo_osc_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_osc_id") String emwo_osc_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwo_oscService.remove(emwo_osc_id));
    }

    @PreAuthorize("hasPermission(this.emwo_oscService.getEmwoOscByIds(#ids),'eam-EMWO_OSC-Remove')")
    @ApiOperation(value = "根据班组设备档案批量删除外委保养工单", tags = {"外委保养工单" },  notes = "根据班组设备档案批量删除外委保养工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_oscs/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquip(@RequestBody List<String> ids) {
        emwo_oscService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwo_oscMapping.toDomain(returnObject.body),'eam-EMWO_OSC-Get')")
    @ApiOperation(value = "根据班组设备档案获取外委保养工单", tags = {"外委保养工单" },  notes = "根据班组设备档案获取外委保养工单")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_oscs/{emwo_osc_id}")
    public ResponseEntity<EMWO_OSCDTO> getByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_osc_id") String emwo_osc_id) {
        EMWO_OSC domain = emwo_oscService.get(emwo_osc_id);
        EMWO_OSCDTO dto = emwo_oscMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案获取外委保养工单草稿", tags = {"外委保养工单" },  notes = "根据班组设备档案获取外委保养工单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_oscs/getdraft")
    public ResponseEntity<EMWO_OSCDTO> getDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id) {
        EMWO_OSC domain = new EMWO_OSC();
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_oscMapping.toDto(emwo_oscService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案检查外委保养工单", tags = {"外委保养工单" },  notes = "根据班组设备档案检查外委保养工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_oscs/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_OSCDTO emwo_oscdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwo_oscService.checkKey(emwo_oscMapping.toDomain(emwo_oscdto)));
    }

    @PreAuthorize("hasPermission(this.emwo_oscMapping.toDomain(#emwo_oscdto),'eam-EMWO_OSC-Save')")
    @ApiOperation(value = "根据班组设备档案保存外委保养工单", tags = {"外委保养工单" },  notes = "根据班组设备档案保存外委保养工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_oscs/save")
    public ResponseEntity<Boolean> saveByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_OSCDTO emwo_oscdto) {
        EMWO_OSC domain = emwo_oscMapping.toDomain(emwo_oscdto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_oscService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emwo_oscMapping.toDomain(#emwo_oscdtos),'eam-EMWO_OSC-Save')")
    @ApiOperation(value = "根据班组设备档案批量保存外委保养工单", tags = {"外委保养工单" },  notes = "根据班组设备档案批量保存外委保养工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_oscs/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_OSCDTO> emwo_oscdtos) {
        List<EMWO_OSC> domainlist=emwo_oscMapping.toDomain(emwo_oscdtos);
        for(EMWO_OSC domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emwo_oscService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_OSC-searchCalendar-all') and hasPermission(#context,'eam-EMWO_OSC-Get')")
	@ApiOperation(value = "根据班组设备档案获取日历", tags = {"外委保养工单" } ,notes = "根据班组设备档案获取日历")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_oscs/fetchcalendar")
	public ResponseEntity<List<EMWO_OSCDTO>> fetchEMWO_OSCCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_OSCSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_OSC> domains = emwo_oscService.searchCalendar(context) ;
        List<EMWO_OSCDTO> list = emwo_oscMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_OSC-searchCalendar-all') and hasPermission(#context,'eam-EMWO_OSC-Get')")
	@ApiOperation(value = "根据班组设备档案查询日历", tags = {"外委保养工单" } ,notes = "根据班组设备档案查询日历")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_oscs/searchcalendar")
	public ResponseEntity<Page<EMWO_OSCDTO>> searchEMWO_OSCCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_OSCSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_OSC> domains = emwo_oscService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_oscMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_OSC-searchDefault-all') and hasPermission(#context,'eam-EMWO_OSC-Get')")
	@ApiOperation(value = "根据班组设备档案获取DEFAULT", tags = {"外委保养工单" } ,notes = "根据班组设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_oscs/fetchdefault")
	public ResponseEntity<List<EMWO_OSCDTO>> fetchEMWO_OSCDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_OSCSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_OSC> domains = emwo_oscService.searchDefault(context) ;
        List<EMWO_OSCDTO> list = emwo_oscMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_OSC-searchDefault-all') and hasPermission(#context,'eam-EMWO_OSC-Get')")
	@ApiOperation(value = "根据班组设备档案查询DEFAULT", tags = {"外委保养工单" } ,notes = "根据班组设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_oscs/searchdefault")
	public ResponseEntity<Page<EMWO_OSCDTO>> searchEMWO_OSCDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_OSCSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_OSC> domains = emwo_oscService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_oscMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

