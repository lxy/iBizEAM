package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMPODetail;
import cn.ibizlab.eam.core.eam_core.service.IEMPODetailService;
import cn.ibizlab.eam.core.eam_core.filter.EMPODetailSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"订单条目" })
@RestController("WebApi-empodetail")
@RequestMapping("")
public class EMPODetailResource {

    @Autowired
    public IEMPODetailService empodetailService;

    @Autowired
    @Lazy
    public EMPODetailMapping empodetailMapping;

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Create')")
    @ApiOperation(value = "新建订单条目", tags = {"订单条目" },  notes = "新建订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/empodetails")
    public ResponseEntity<EMPODetailDTO> create(@Validated @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
		empodetailService.create(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Create')")
    @ApiOperation(value = "批量新建订单条目", tags = {"订单条目" },  notes = "批量新建订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/empodetails/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMPODetailDTO> empodetaildtos) {
        empodetailService.createBatch(empodetailMapping.toDomain(empodetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "empodetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Update')")
    @ApiOperation(value = "更新订单条目", tags = {"订单条目" },  notes = "更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> update(@PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
		EMPODetail domain  = empodetailMapping.toDomain(empodetaildto);
        domain .setEmpodetailid(empodetail_id);
		empodetailService.update(domain );
		EMPODetailDTO dto = empodetailMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByEntities(this.empodetailMapping.toDomain(#empodetaildtos)),'eam-EMPODetail-Update')")
    @ApiOperation(value = "批量更新订单条目", tags = {"订单条目" },  notes = "批量更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/empodetails/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMPODetailDTO> empodetaildtos) {
        empodetailService.updateBatch(empodetailMapping.toDomain(empodetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "删除订单条目", tags = {"订单条目" },  notes = "删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/empodetails/{empodetail_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("empodetail_id") String empodetail_id) {
         return ResponseEntity.status(HttpStatus.OK).body(empodetailService.remove(empodetail_id));
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByIds(#ids),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "批量删除订单条目", tags = {"订单条目" },  notes = "批量删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/empodetails/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        empodetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.empodetailMapping.toDomain(returnObject.body),'eam-EMPODetail-Get')")
    @ApiOperation(value = "获取订单条目", tags = {"订单条目" },  notes = "获取订单条目")
	@RequestMapping(method = RequestMethod.GET, value = "/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> get(@PathVariable("empodetail_id") String empodetail_id) {
        EMPODetail domain = empodetailService.get(empodetail_id);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取订单条目草稿", tags = {"订单条目" },  notes = "获取订单条目草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/empodetails/getdraft")
    public ResponseEntity<EMPODetailDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(empodetailMapping.toDto(empodetailService.getDraft(new EMPODetail())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-Check-all')")
    @ApiOperation(value = "验货完成", tags = {"订单条目" },  notes = "验货完成")
	@RequestMapping(method = RequestMethod.POST, value = "/empodetails/{empodetail_id}/check")
    public ResponseEntity<EMPODetailDTO> check(@PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setEmpodetailid(empodetail_id);
        domain = empodetailService.check(domain);
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @ApiOperation(value = "检查订单条目", tags = {"订单条目" },  notes = "检查订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/empodetails/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMPODetailDTO empodetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(empodetailService.checkKey(empodetailMapping.toDomain(empodetaildto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-CreateRin-all')")
    @ApiOperation(value = "生成入库单", tags = {"订单条目" },  notes = "生成入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/empodetails/{empodetail_id}/createrin")
    public ResponseEntity<EMPODetailDTO> createRin(@PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setEmpodetailid(empodetail_id);
        domain = empodetailService.createRin(domain);
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-GenId-all')")
    @ApiOperation(value = "生成采购订单条目号", tags = {"订单条目" },  notes = "生成采购订单条目号")
	@RequestMapping(method = RequestMethod.POST, value = "/empodetails/{empodetail_id}/genid")
    public ResponseEntity<EMPODetailDTO> genId(@PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setEmpodetailid(empodetail_id);
        domain = empodetailService.genId(domain);
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Save')")
    @ApiOperation(value = "保存订单条目", tags = {"订单条目" },  notes = "保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/empodetails/save")
    public ResponseEntity<Boolean> save(@RequestBody EMPODetailDTO empodetaildto) {
        return ResponseEntity.status(HttpStatus.OK).body(empodetailService.save(empodetailMapping.toDomain(empodetaildto)));
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Save')")
    @ApiOperation(value = "批量保存订单条目", tags = {"订单条目" },  notes = "批量保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/empodetails/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMPODetailDTO> empodetaildtos) {
        empodetailService.saveBatch(empodetailMapping.toDomain(empodetaildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "获取已关闭", tags = {"订单条目" } ,notes = "获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/empodetails/fetchclosed")
	public ResponseEntity<List<EMPODetailDTO>> fetchClosed(EMPODetailSearchContext context) {
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "查询已关闭", tags = {"订单条目" } ,notes = "查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/empodetails/searchclosed")
	public ResponseEntity<Page<EMPODetailDTO>> searchClosed(@RequestBody EMPODetailSearchContext context) {
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"订单条目" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/empodetails/fetchdefault")
	public ResponseEntity<List<EMPODetailDTO>> fetchDefault(EMPODetailSearchContext context) {
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"订单条目" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/empodetails/searchdefault")
	public ResponseEntity<Page<EMPODetailDTO>> searchDefault(@RequestBody EMPODetailSearchContext context) {
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "获取近五年采购", tags = {"订单条目" } ,notes = "获取近五年采购")
    @RequestMapping(method= RequestMethod.GET , value="/empodetails/fetchlateryear")
	public ResponseEntity<List<HashMap>> fetchLaterYear(EMPODetailSearchContext context) {
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "查询近五年采购", tags = {"订单条目" } ,notes = "查询近五年采购")
    @RequestMapping(method= RequestMethod.POST , value="/empodetails/searchlateryear")
	public ResponseEntity<Page<HashMap>> searchLaterYear(@RequestBody EMPODetailSearchContext context) {
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "获取待记账", tags = {"订单条目" } ,notes = "获取待记账")
    @RequestMapping(method= RequestMethod.GET , value="/empodetails/fetchwaitbook")
	public ResponseEntity<List<EMPODetailDTO>> fetchWaitBook(EMPODetailSearchContext context) {
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "查询待记账", tags = {"订单条目" } ,notes = "查询待记账")
    @RequestMapping(method= RequestMethod.POST , value="/empodetails/searchwaitbook")
	public ResponseEntity<Page<EMPODetailDTO>> searchWaitBook(@RequestBody EMPODetailSearchContext context) {
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "获取待验收", tags = {"订单条目" } ,notes = "获取待验收")
    @RequestMapping(method= RequestMethod.GET , value="/empodetails/fetchwaitcheck")
	public ResponseEntity<List<EMPODetailDTO>> fetchWaitCheck(EMPODetailSearchContext context) {
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "查询待验收", tags = {"订单条目" } ,notes = "查询待验收")
    @RequestMapping(method= RequestMethod.POST , value="/empodetails/searchwaitcheck")
	public ResponseEntity<Page<EMPODetailDTO>> searchWaitCheck(@RequestBody EMPODetailSearchContext context) {
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据物品建立订单条目", tags = {"订单条目" },  notes = "根据物品建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/empodetails")
    public ResponseEntity<EMPODetailDTO> createByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
		empodetailService.create(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据物品批量建立订单条目", tags = {"订单条目" },  notes = "根据物品批量建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/empodetails/batch")
    public ResponseEntity<Boolean> createBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        empodetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "empodetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据物品更新订单条目", tags = {"订单条目" },  notes = "根据物品更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> updateByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain.setEmpodetailid(empodetail_id);
		empodetailService.update(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByEntities(this.empodetailMapping.toDomain(#empodetaildtos)),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据物品批量更新订单条目", tags = {"订单条目" },  notes = "根据物品批量更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/empodetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        empodetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据物品删除订单条目", tags = {"订单条目" },  notes = "根据物品删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/empodetails/{empodetail_id}")
    public ResponseEntity<Boolean> removeByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(empodetailService.remove(empodetail_id));
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByIds(#ids),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据物品批量删除订单条目", tags = {"订单条目" },  notes = "根据物品批量删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/empodetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMItem(@RequestBody List<String> ids) {
        empodetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.empodetailMapping.toDomain(returnObject.body),'eam-EMPODetail-Get')")
    @ApiOperation(value = "根据物品获取订单条目", tags = {"订单条目" },  notes = "根据物品获取订单条目")
	@RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> getByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id) {
        EMPODetail domain = empodetailService.get(empodetail_id);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据物品获取订单条目草稿", tags = {"订单条目" },  notes = "根据物品获取订单条目草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/empodetails/getdraft")
    public ResponseEntity<EMPODetailDTO> getDraftByEMItem(@PathVariable("emitem_id") String emitem_id) {
        EMPODetail domain = new EMPODetail();
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailMapping.toDto(empodetailService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-Check-all')")
    @ApiOperation(value = "根据物品订单条目", tags = {"订单条目" },  notes = "根据物品订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/empodetails/{empodetail_id}/check")
    public ResponseEntity<EMPODetailDTO> checkByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain = empodetailService.check(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @ApiOperation(value = "根据物品检查订单条目", tags = {"订单条目" },  notes = "根据物品检查订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/empodetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailDTO empodetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(empodetailService.checkKey(empodetailMapping.toDomain(empodetaildto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-CreateRin-all')")
    @ApiOperation(value = "根据物品订单条目", tags = {"订单条目" },  notes = "根据物品订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/empodetails/{empodetail_id}/createrin")
    public ResponseEntity<EMPODetailDTO> createRinByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain = empodetailService.createRin(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-GenId-all')")
    @ApiOperation(value = "根据物品订单条目", tags = {"订单条目" },  notes = "根据物品订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/empodetails/{empodetail_id}/genid")
    public ResponseEntity<EMPODetailDTO> genIdByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain = empodetailService.genId(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据物品保存订单条目", tags = {"订单条目" },  notes = "根据物品保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/empodetails/save")
    public ResponseEntity<Boolean> saveByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据物品批量保存订单条目", tags = {"订单条目" },  notes = "根据物品批量保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/empodetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
             domain.setItemid(emitem_id);
        }
        empodetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据物品获取已关闭", tags = {"订单条目" } ,notes = "根据物品获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/empodetails/fetchclosed")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailClosedByEMItem(@PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据物品查询已关闭", tags = {"订单条目" } ,notes = "根据物品查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/empodetails/searchclosed")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailClosedByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据物品获取DEFAULT", tags = {"订单条目" } ,notes = "根据物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/empodetails/fetchdefault")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailDefaultByEMItem(@PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据物品查询DEFAULT", tags = {"订单条目" } ,notes = "根据物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/empodetails/searchdefault")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailDefaultByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据物品获取近五年采购", tags = {"订单条目" } ,notes = "根据物品获取近五年采购")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/empodetails/fetchlateryear")
	public ResponseEntity<List<HashMap>> fetchEMPODetailLaterYearByEMItem(@PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据物品查询近五年采购", tags = {"订单条目" } ,notes = "根据物品查询近五年采购")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/empodetails/searchlateryear")
	public ResponseEntity<Page<HashMap>> searchEMPODetailLaterYearByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据物品获取待记账", tags = {"订单条目" } ,notes = "根据物品获取待记账")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/empodetails/fetchwaitbook")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitBookByEMItem(@PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据物品查询待记账", tags = {"订单条目" } ,notes = "根据物品查询待记账")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/empodetails/searchwaitbook")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitBookByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据物品获取待验收", tags = {"订单条目" } ,notes = "根据物品获取待验收")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/empodetails/fetchwaitcheck")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitCheckByEMItem(@PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据物品查询待验收", tags = {"订单条目" } ,notes = "根据物品查询待验收")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/empodetails/searchwaitcheck")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitCheckByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据订单建立订单条目", tags = {"订单条目" },  notes = "根据订单建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/empos/{empo_id}/empodetails")
    public ResponseEntity<EMPODetailDTO> createByEMPO(@PathVariable("empo_id") String empo_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setPoid(empo_id);
		empodetailService.create(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据订单批量建立订单条目", tags = {"订单条目" },  notes = "根据订单批量建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/empos/{empo_id}/empodetails/batch")
    public ResponseEntity<Boolean> createBatchByEMPO(@PathVariable("empo_id") String empo_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setPoid(empo_id);
        }
        empodetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "empodetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据订单更新订单条目", tags = {"订单条目" },  notes = "根据订单更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/empos/{empo_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> updateByEMPO(@PathVariable("empo_id") String empo_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setPoid(empo_id);
        domain.setEmpodetailid(empodetail_id);
		empodetailService.update(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByEntities(this.empodetailMapping.toDomain(#empodetaildtos)),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据订单批量更新订单条目", tags = {"订单条目" },  notes = "根据订单批量更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/empos/{empo_id}/empodetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMPO(@PathVariable("empo_id") String empo_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setPoid(empo_id);
        }
        empodetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据订单删除订单条目", tags = {"订单条目" },  notes = "根据订单删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/empos/{empo_id}/empodetails/{empodetail_id}")
    public ResponseEntity<Boolean> removeByEMPO(@PathVariable("empo_id") String empo_id, @PathVariable("empodetail_id") String empodetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(empodetailService.remove(empodetail_id));
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByIds(#ids),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据订单批量删除订单条目", tags = {"订单条目" },  notes = "根据订单批量删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/empos/{empo_id}/empodetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMPO(@RequestBody List<String> ids) {
        empodetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.empodetailMapping.toDomain(returnObject.body),'eam-EMPODetail-Get')")
    @ApiOperation(value = "根据订单获取订单条目", tags = {"订单条目" },  notes = "根据订单获取订单条目")
	@RequestMapping(method = RequestMethod.GET, value = "/empos/{empo_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> getByEMPO(@PathVariable("empo_id") String empo_id, @PathVariable("empodetail_id") String empodetail_id) {
        EMPODetail domain = empodetailService.get(empodetail_id);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据订单获取订单条目草稿", tags = {"订单条目" },  notes = "根据订单获取订单条目草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/empos/{empo_id}/empodetails/getdraft")
    public ResponseEntity<EMPODetailDTO> getDraftByEMPO(@PathVariable("empo_id") String empo_id) {
        EMPODetail domain = new EMPODetail();
        domain.setPoid(empo_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailMapping.toDto(empodetailService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-Check-all')")
    @ApiOperation(value = "根据订单订单条目", tags = {"订单条目" },  notes = "根据订单订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/empos/{empo_id}/empodetails/{empodetail_id}/check")
    public ResponseEntity<EMPODetailDTO> checkByEMPO(@PathVariable("empo_id") String empo_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setPoid(empo_id);
        domain = empodetailService.check(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @ApiOperation(value = "根据订单检查订单条目", tags = {"订单条目" },  notes = "根据订单检查订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/empos/{empo_id}/empodetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMPO(@PathVariable("empo_id") String empo_id, @RequestBody EMPODetailDTO empodetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(empodetailService.checkKey(empodetailMapping.toDomain(empodetaildto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-CreateRin-all')")
    @ApiOperation(value = "根据订单订单条目", tags = {"订单条目" },  notes = "根据订单订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/empos/{empo_id}/empodetails/{empodetail_id}/createrin")
    public ResponseEntity<EMPODetailDTO> createRinByEMPO(@PathVariable("empo_id") String empo_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setPoid(empo_id);
        domain = empodetailService.createRin(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-GenId-all')")
    @ApiOperation(value = "根据订单订单条目", tags = {"订单条目" },  notes = "根据订单订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/empos/{empo_id}/empodetails/{empodetail_id}/genid")
    public ResponseEntity<EMPODetailDTO> genIdByEMPO(@PathVariable("empo_id") String empo_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setPoid(empo_id);
        domain = empodetailService.genId(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据订单保存订单条目", tags = {"订单条目" },  notes = "根据订单保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/empos/{empo_id}/empodetails/save")
    public ResponseEntity<Boolean> saveByEMPO(@PathVariable("empo_id") String empo_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setPoid(empo_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据订单批量保存订单条目", tags = {"订单条目" },  notes = "根据订单批量保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/empos/{empo_id}/empodetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMPO(@PathVariable("empo_id") String empo_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
             domain.setPoid(empo_id);
        }
        empodetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据订单获取已关闭", tags = {"订单条目" } ,notes = "根据订单获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/empos/{empo_id}/empodetails/fetchclosed")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailClosedByEMPO(@PathVariable("empo_id") String empo_id,EMPODetailSearchContext context) {
        context.setN_poid_eq(empo_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据订单查询已关闭", tags = {"订单条目" } ,notes = "根据订单查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/empos/{empo_id}/empodetails/searchclosed")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailClosedByEMPO(@PathVariable("empo_id") String empo_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_poid_eq(empo_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据订单获取DEFAULT", tags = {"订单条目" } ,notes = "根据订单获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/empos/{empo_id}/empodetails/fetchdefault")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailDefaultByEMPO(@PathVariable("empo_id") String empo_id,EMPODetailSearchContext context) {
        context.setN_poid_eq(empo_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据订单查询DEFAULT", tags = {"订单条目" } ,notes = "根据订单查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/empos/{empo_id}/empodetails/searchdefault")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailDefaultByEMPO(@PathVariable("empo_id") String empo_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_poid_eq(empo_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据订单获取近五年采购", tags = {"订单条目" } ,notes = "根据订单获取近五年采购")
    @RequestMapping(method= RequestMethod.GET , value="/empos/{empo_id}/empodetails/fetchlateryear")
	public ResponseEntity<List<HashMap>> fetchEMPODetailLaterYearByEMPO(@PathVariable("empo_id") String empo_id,EMPODetailSearchContext context) {
        context.setN_poid_eq(empo_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据订单查询近五年采购", tags = {"订单条目" } ,notes = "根据订单查询近五年采购")
    @RequestMapping(method= RequestMethod.POST , value="/empos/{empo_id}/empodetails/searchlateryear")
	public ResponseEntity<Page<HashMap>> searchEMPODetailLaterYearByEMPO(@PathVariable("empo_id") String empo_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_poid_eq(empo_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据订单获取待记账", tags = {"订单条目" } ,notes = "根据订单获取待记账")
    @RequestMapping(method= RequestMethod.GET , value="/empos/{empo_id}/empodetails/fetchwaitbook")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitBookByEMPO(@PathVariable("empo_id") String empo_id,EMPODetailSearchContext context) {
        context.setN_poid_eq(empo_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据订单查询待记账", tags = {"订单条目" } ,notes = "根据订单查询待记账")
    @RequestMapping(method= RequestMethod.POST , value="/empos/{empo_id}/empodetails/searchwaitbook")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitBookByEMPO(@PathVariable("empo_id") String empo_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_poid_eq(empo_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据订单获取待验收", tags = {"订单条目" } ,notes = "根据订单获取待验收")
    @RequestMapping(method= RequestMethod.GET , value="/empos/{empo_id}/empodetails/fetchwaitcheck")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitCheckByEMPO(@PathVariable("empo_id") String empo_id,EMPODetailSearchContext context) {
        context.setN_poid_eq(empo_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据订单查询待验收", tags = {"订单条目" } ,notes = "根据订单查询待验收")
    @RequestMapping(method= RequestMethod.POST , value="/empos/{empo_id}/empodetails/searchwaitcheck")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitCheckByEMPO(@PathVariable("empo_id") String empo_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_poid_eq(empo_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据采购申请建立订单条目", tags = {"订单条目" },  notes = "根据采购申请建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/empodetails")
    public ResponseEntity<EMPODetailDTO> createByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
		empodetailService.create(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据采购申请批量建立订单条目", tags = {"订单条目" },  notes = "根据采购申请批量建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> createBatchByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        empodetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "empodetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据采购申请更新订单条目", tags = {"订单条目" },  notes = "根据采购申请更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> updateByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain.setEmpodetailid(empodetail_id);
		empodetailService.update(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByEntities(this.empodetailMapping.toDomain(#empodetaildtos)),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据采购申请批量更新订单条目", tags = {"订单条目" },  notes = "根据采购申请批量更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        empodetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据采购申请删除订单条目", tags = {"订单条目" },  notes = "根据采购申请删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<Boolean> removeByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(empodetailService.remove(empodetail_id));
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByIds(#ids),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据采购申请批量删除订单条目", tags = {"订单条目" },  notes = "根据采购申请批量删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMWPList(@RequestBody List<String> ids) {
        empodetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.empodetailMapping.toDomain(returnObject.body),'eam-EMPODetail-Get')")
    @ApiOperation(value = "根据采购申请获取订单条目", tags = {"订单条目" },  notes = "根据采购申请获取订单条目")
	@RequestMapping(method = RequestMethod.GET, value = "/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> getByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id) {
        EMPODetail domain = empodetailService.get(empodetail_id);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据采购申请获取订单条目草稿", tags = {"订单条目" },  notes = "根据采购申请获取订单条目草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emwplists/{emwplist_id}/empodetails/getdraft")
    public ResponseEntity<EMPODetailDTO> getDraftByEMWPList(@PathVariable("emwplist_id") String emwplist_id) {
        EMPODetail domain = new EMPODetail();
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailMapping.toDto(empodetailService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-Check-all')")
    @ApiOperation(value = "根据采购申请订单条目", tags = {"订单条目" },  notes = "根据采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/empodetails/{empodetail_id}/check")
    public ResponseEntity<EMPODetailDTO> checkByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.check(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @ApiOperation(value = "根据采购申请检查订单条目", tags = {"订单条目" },  notes = "根据采购申请检查订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/empodetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(empodetailService.checkKey(empodetailMapping.toDomain(empodetaildto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-CreateRin-all')")
    @ApiOperation(value = "根据采购申请订单条目", tags = {"订单条目" },  notes = "根据采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/empodetails/{empodetail_id}/createrin")
    public ResponseEntity<EMPODetailDTO> createRinByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.createRin(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-GenId-all')")
    @ApiOperation(value = "根据采购申请订单条目", tags = {"订单条目" },  notes = "根据采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/empodetails/{empodetail_id}/genid")
    public ResponseEntity<EMPODetailDTO> genIdByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.genId(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据采购申请保存订单条目", tags = {"订单条目" },  notes = "根据采购申请保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/empodetails/save")
    public ResponseEntity<Boolean> saveByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据采购申请批量保存订单条目", tags = {"订单条目" },  notes = "根据采购申请批量保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/empodetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
             domain.setWplistid(emwplist_id);
        }
        empodetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据采购申请获取已关闭", tags = {"订单条目" } ,notes = "根据采购申请获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/emwplists/{emwplist_id}/empodetails/fetchclosed")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailClosedByEMWPList(@PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据采购申请查询已关闭", tags = {"订单条目" } ,notes = "根据采购申请查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/emwplists/{emwplist_id}/empodetails/searchclosed")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailClosedByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据采购申请获取DEFAULT", tags = {"订单条目" } ,notes = "根据采购申请获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emwplists/{emwplist_id}/empodetails/fetchdefault")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailDefaultByEMWPList(@PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据采购申请查询DEFAULT", tags = {"订单条目" } ,notes = "根据采购申请查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emwplists/{emwplist_id}/empodetails/searchdefault")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailDefaultByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据采购申请获取近五年采购", tags = {"订单条目" } ,notes = "根据采购申请获取近五年采购")
    @RequestMapping(method= RequestMethod.GET , value="/emwplists/{emwplist_id}/empodetails/fetchlateryear")
	public ResponseEntity<List<HashMap>> fetchEMPODetailLaterYearByEMWPList(@PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据采购申请查询近五年采购", tags = {"订单条目" } ,notes = "根据采购申请查询近五年采购")
    @RequestMapping(method= RequestMethod.POST , value="/emwplists/{emwplist_id}/empodetails/searchlateryear")
	public ResponseEntity<Page<HashMap>> searchEMPODetailLaterYearByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据采购申请获取待记账", tags = {"订单条目" } ,notes = "根据采购申请获取待记账")
    @RequestMapping(method= RequestMethod.GET , value="/emwplists/{emwplist_id}/empodetails/fetchwaitbook")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitBookByEMWPList(@PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据采购申请查询待记账", tags = {"订单条目" } ,notes = "根据采购申请查询待记账")
    @RequestMapping(method= RequestMethod.POST , value="/emwplists/{emwplist_id}/empodetails/searchwaitbook")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitBookByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据采购申请获取待验收", tags = {"订单条目" } ,notes = "根据采购申请获取待验收")
    @RequestMapping(method= RequestMethod.GET , value="/emwplists/{emwplist_id}/empodetails/fetchwaitcheck")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitCheckByEMWPList(@PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据采购申请查询待验收", tags = {"订单条目" } ,notes = "根据采购申请查询待验收")
    @RequestMapping(method= RequestMethod.POST , value="/emwplists/{emwplist_id}/empodetails/searchwaitcheck")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitCheckByEMWPList(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据服务商物品建立订单条目", tags = {"订单条目" },  notes = "根据服务商物品建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/empodetails")
    public ResponseEntity<EMPODetailDTO> createByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
		empodetailService.create(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据服务商物品批量建立订单条目", tags = {"订单条目" },  notes = "根据服务商物品批量建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        empodetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "empodetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据服务商物品更新订单条目", tags = {"订单条目" },  notes = "根据服务商物品更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> updateByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain.setEmpodetailid(empodetail_id);
		empodetailService.update(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByEntities(this.empodetailMapping.toDomain(#empodetaildtos)),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据服务商物品批量更新订单条目", tags = {"订单条目" },  notes = "根据服务商物品批量更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        empodetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据服务商物品删除订单条目", tags = {"订单条目" },  notes = "根据服务商物品删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/{empodetail_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(empodetailService.remove(empodetail_id));
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByIds(#ids),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据服务商物品批量删除订单条目", tags = {"订单条目" },  notes = "根据服务商物品批量删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMItem(@RequestBody List<String> ids) {
        empodetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.empodetailMapping.toDomain(returnObject.body),'eam-EMPODetail-Get')")
    @ApiOperation(value = "根据服务商物品获取订单条目", tags = {"订单条目" },  notes = "根据服务商物品获取订单条目")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> getByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id) {
        EMPODetail domain = empodetailService.get(empodetail_id);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商物品获取订单条目草稿", tags = {"订单条目" },  notes = "根据服务商物品获取订单条目草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/getdraft")
    public ResponseEntity<EMPODetailDTO> getDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id) {
        EMPODetail domain = new EMPODetail();
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailMapping.toDto(empodetailService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-Check-all')")
    @ApiOperation(value = "根据服务商物品订单条目", tags = {"订单条目" },  notes = "根据服务商物品订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/{empodetail_id}/check")
    public ResponseEntity<EMPODetailDTO> checkByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain = empodetailService.check(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @ApiOperation(value = "根据服务商物品检查订单条目", tags = {"订单条目" },  notes = "根据服务商物品检查订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailDTO empodetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(empodetailService.checkKey(empodetailMapping.toDomain(empodetaildto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-CreateRin-all')")
    @ApiOperation(value = "根据服务商物品订单条目", tags = {"订单条目" },  notes = "根据服务商物品订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/{empodetail_id}/createrin")
    public ResponseEntity<EMPODetailDTO> createRinByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain = empodetailService.createRin(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-GenId-all')")
    @ApiOperation(value = "根据服务商物品订单条目", tags = {"订单条目" },  notes = "根据服务商物品订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/{empodetail_id}/genid")
    public ResponseEntity<EMPODetailDTO> genIdByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain = empodetailService.genId(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据服务商物品保存订单条目", tags = {"订单条目" },  notes = "根据服务商物品保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/save")
    public ResponseEntity<Boolean> saveByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据服务商物品批量保存订单条目", tags = {"订单条目" },  notes = "根据服务商物品批量保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
             domain.setItemid(emitem_id);
        }
        empodetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据服务商物品获取已关闭", tags = {"订单条目" } ,notes = "根据服务商物品获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/fetchclosed")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailClosedByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据服务商物品查询已关闭", tags = {"订单条目" } ,notes = "根据服务商物品查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/searchclosed")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailClosedByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据服务商物品获取DEFAULT", tags = {"订单条目" } ,notes = "根据服务商物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/fetchdefault")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据服务商物品查询DEFAULT", tags = {"订单条目" } ,notes = "根据服务商物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/searchdefault")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据服务商物品获取近五年采购", tags = {"订单条目" } ,notes = "根据服务商物品获取近五年采购")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/fetchlateryear")
	public ResponseEntity<List<HashMap>> fetchEMPODetailLaterYearByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据服务商物品查询近五年采购", tags = {"订单条目" } ,notes = "根据服务商物品查询近五年采购")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/searchlateryear")
	public ResponseEntity<Page<HashMap>> searchEMPODetailLaterYearByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据服务商物品获取待记账", tags = {"订单条目" } ,notes = "根据服务商物品获取待记账")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/fetchwaitbook")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitBookByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据服务商物品查询待记账", tags = {"订单条目" } ,notes = "根据服务商物品查询待记账")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/searchwaitbook")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitBookByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据服务商物品获取待验收", tags = {"订单条目" } ,notes = "根据服务商物品获取待验收")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/fetchwaitcheck")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitCheckByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据服务商物品查询待验收", tags = {"订单条目" } ,notes = "根据服务商物品查询待验收")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/empodetails/searchwaitcheck")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitCheckByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据仓库库位物品建立订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails")
    public ResponseEntity<EMPODetailDTO> createByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
		empodetailService.create(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据仓库库位物品批量建立订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品批量建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/batch")
    public ResponseEntity<Boolean> createBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        empodetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "empodetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据仓库库位物品更新订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> updateByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain.setEmpodetailid(empodetail_id);
		empodetailService.update(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByEntities(this.empodetailMapping.toDomain(#empodetaildtos)),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据仓库库位物品批量更新订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品批量更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        empodetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据仓库库位物品删除订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/{empodetail_id}")
    public ResponseEntity<Boolean> removeByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(empodetailService.remove(empodetail_id));
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByIds(#ids),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据仓库库位物品批量删除订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品批量删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMStorePartEMItem(@RequestBody List<String> ids) {
        empodetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.empodetailMapping.toDomain(returnObject.body),'eam-EMPODetail-Get')")
    @ApiOperation(value = "根据仓库库位物品获取订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品获取订单条目")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> getByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id) {
        EMPODetail domain = empodetailService.get(empodetail_id);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库库位物品获取订单条目草稿", tags = {"订单条目" },  notes = "根据仓库库位物品获取订单条目草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/getdraft")
    public ResponseEntity<EMPODetailDTO> getDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id) {
        EMPODetail domain = new EMPODetail();
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailMapping.toDto(empodetailService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-Check-all')")
    @ApiOperation(value = "根据仓库库位物品订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/{empodetail_id}/check")
    public ResponseEntity<EMPODetailDTO> checkByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain = empodetailService.check(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @ApiOperation(value = "根据仓库库位物品检查订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品检查订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailDTO empodetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(empodetailService.checkKey(empodetailMapping.toDomain(empodetaildto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-CreateRin-all')")
    @ApiOperation(value = "根据仓库库位物品订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/{empodetail_id}/createrin")
    public ResponseEntity<EMPODetailDTO> createRinByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain = empodetailService.createRin(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-GenId-all')")
    @ApiOperation(value = "根据仓库库位物品订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/{empodetail_id}/genid")
    public ResponseEntity<EMPODetailDTO> genIdByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain = empodetailService.genId(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据仓库库位物品保存订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/save")
    public ResponseEntity<Boolean> saveByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据仓库库位物品批量保存订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品批量保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
             domain.setItemid(emitem_id);
        }
        empodetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库库位物品获取已关闭", tags = {"订单条目" } ,notes = "根据仓库库位物品获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/fetchclosed")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailClosedByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库库位物品查询已关闭", tags = {"订单条目" } ,notes = "根据仓库库位物品查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/searchclosed")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailClosedByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库库位物品获取DEFAULT", tags = {"订单条目" } ,notes = "根据仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/fetchdefault")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库库位物品查询DEFAULT", tags = {"订单条目" } ,notes = "根据仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/searchdefault")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库库位物品获取近五年采购", tags = {"订单条目" } ,notes = "根据仓库库位物品获取近五年采购")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/fetchlateryear")
	public ResponseEntity<List<HashMap>> fetchEMPODetailLaterYearByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库库位物品查询近五年采购", tags = {"订单条目" } ,notes = "根据仓库库位物品查询近五年采购")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/searchlateryear")
	public ResponseEntity<Page<HashMap>> searchEMPODetailLaterYearByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库库位物品获取待记账", tags = {"订单条目" } ,notes = "根据仓库库位物品获取待记账")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/fetchwaitbook")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitBookByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库库位物品查询待记账", tags = {"订单条目" } ,notes = "根据仓库库位物品查询待记账")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/searchwaitbook")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitBookByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库库位物品获取待验收", tags = {"订单条目" } ,notes = "根据仓库库位物品获取待验收")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/fetchwaitcheck")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitCheckByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库库位物品查询待验收", tags = {"订单条目" } ,notes = "根据仓库库位物品查询待验收")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/searchwaitcheck")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitCheckByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据仓库物品建立订单条目", tags = {"订单条目" },  notes = "根据仓库物品建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/empodetails")
    public ResponseEntity<EMPODetailDTO> createByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
		empodetailService.create(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据仓库物品批量建立订单条目", tags = {"订单条目" },  notes = "根据仓库物品批量建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        empodetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "empodetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据仓库物品更新订单条目", tags = {"订单条目" },  notes = "根据仓库物品更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> updateByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain.setEmpodetailid(empodetail_id);
		empodetailService.update(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByEntities(this.empodetailMapping.toDomain(#empodetaildtos)),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据仓库物品批量更新订单条目", tags = {"订单条目" },  notes = "根据仓库物品批量更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        empodetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据仓库物品删除订单条目", tags = {"订单条目" },  notes = "根据仓库物品删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/{empodetail_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(empodetailService.remove(empodetail_id));
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByIds(#ids),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据仓库物品批量删除订单条目", tags = {"订单条目" },  notes = "根据仓库物品批量删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMItem(@RequestBody List<String> ids) {
        empodetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.empodetailMapping.toDomain(returnObject.body),'eam-EMPODetail-Get')")
    @ApiOperation(value = "根据仓库物品获取订单条目", tags = {"订单条目" },  notes = "根据仓库物品获取订单条目")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> getByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id) {
        EMPODetail domain = empodetailService.get(empodetail_id);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库物品获取订单条目草稿", tags = {"订单条目" },  notes = "根据仓库物品获取订单条目草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/getdraft")
    public ResponseEntity<EMPODetailDTO> getDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id) {
        EMPODetail domain = new EMPODetail();
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailMapping.toDto(empodetailService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-Check-all')")
    @ApiOperation(value = "根据仓库物品订单条目", tags = {"订单条目" },  notes = "根据仓库物品订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/{empodetail_id}/check")
    public ResponseEntity<EMPODetailDTO> checkByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain = empodetailService.check(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @ApiOperation(value = "根据仓库物品检查订单条目", tags = {"订单条目" },  notes = "根据仓库物品检查订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailDTO empodetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(empodetailService.checkKey(empodetailMapping.toDomain(empodetaildto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-CreateRin-all')")
    @ApiOperation(value = "根据仓库物品订单条目", tags = {"订单条目" },  notes = "根据仓库物品订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/{empodetail_id}/createrin")
    public ResponseEntity<EMPODetailDTO> createRinByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain = empodetailService.createRin(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-GenId-all')")
    @ApiOperation(value = "根据仓库物品订单条目", tags = {"订单条目" },  notes = "根据仓库物品订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/{empodetail_id}/genid")
    public ResponseEntity<EMPODetailDTO> genIdByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain = empodetailService.genId(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据仓库物品保存订单条目", tags = {"订单条目" },  notes = "根据仓库物品保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/save")
    public ResponseEntity<Boolean> saveByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据仓库物品批量保存订单条目", tags = {"订单条目" },  notes = "根据仓库物品批量保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
             domain.setItemid(emitem_id);
        }
        empodetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库物品获取已关闭", tags = {"订单条目" } ,notes = "根据仓库物品获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/fetchclosed")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailClosedByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库物品查询已关闭", tags = {"订单条目" } ,notes = "根据仓库物品查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/searchclosed")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailClosedByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库物品获取DEFAULT", tags = {"订单条目" } ,notes = "根据仓库物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/fetchdefault")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库物品查询DEFAULT", tags = {"订单条目" } ,notes = "根据仓库物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/searchdefault")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库物品获取近五年采购", tags = {"订单条目" } ,notes = "根据仓库物品获取近五年采购")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/fetchlateryear")
	public ResponseEntity<List<HashMap>> fetchEMPODetailLaterYearByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库物品查询近五年采购", tags = {"订单条目" } ,notes = "根据仓库物品查询近五年采购")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/searchlateryear")
	public ResponseEntity<Page<HashMap>> searchEMPODetailLaterYearByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库物品获取待记账", tags = {"订单条目" } ,notes = "根据仓库物品获取待记账")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/fetchwaitbook")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitBookByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库物品查询待记账", tags = {"订单条目" } ,notes = "根据仓库物品查询待记账")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/searchwaitbook")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitBookByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库物品获取待验收", tags = {"订单条目" } ,notes = "根据仓库物品获取待验收")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/fetchwaitcheck")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitCheckByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库物品查询待验收", tags = {"订单条目" } ,notes = "根据仓库物品查询待验收")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/empodetails/searchwaitcheck")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitCheckByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据物品采购申请建立订单条目", tags = {"订单条目" },  notes = "根据物品采购申请建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails")
    public ResponseEntity<EMPODetailDTO> createByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
		empodetailService.create(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据物品采购申请批量建立订单条目", tags = {"订单条目" },  notes = "根据物品采购申请批量建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> createBatchByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        empodetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "empodetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据物品采购申请更新订单条目", tags = {"订单条目" },  notes = "根据物品采购申请更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> updateByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain.setEmpodetailid(empodetail_id);
		empodetailService.update(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByEntities(this.empodetailMapping.toDomain(#empodetaildtos)),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据物品采购申请批量更新订单条目", tags = {"订单条目" },  notes = "根据物品采购申请批量更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        empodetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据物品采购申请删除订单条目", tags = {"订单条目" },  notes = "根据物品采购申请删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<Boolean> removeByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(empodetailService.remove(empodetail_id));
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByIds(#ids),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据物品采购申请批量删除订单条目", tags = {"订单条目" },  notes = "根据物品采购申请批量删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMItemEMWPList(@RequestBody List<String> ids) {
        empodetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.empodetailMapping.toDomain(returnObject.body),'eam-EMPODetail-Get')")
    @ApiOperation(value = "根据物品采购申请获取订单条目", tags = {"订单条目" },  notes = "根据物品采购申请获取订单条目")
	@RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> getByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id) {
        EMPODetail domain = empodetailService.get(empodetail_id);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据物品采购申请获取订单条目草稿", tags = {"订单条目" },  notes = "根据物品采购申请获取订单条目草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/getdraft")
    public ResponseEntity<EMPODetailDTO> getDraftByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
        EMPODetail domain = new EMPODetail();
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailMapping.toDto(empodetailService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-Check-all')")
    @ApiOperation(value = "根据物品采购申请订单条目", tags = {"订单条目" },  notes = "根据物品采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}/check")
    public ResponseEntity<EMPODetailDTO> checkByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.check(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @ApiOperation(value = "根据物品采购申请检查订单条目", tags = {"订单条目" },  notes = "根据物品采购申请检查订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(empodetailService.checkKey(empodetailMapping.toDomain(empodetaildto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-CreateRin-all')")
    @ApiOperation(value = "根据物品采购申请订单条目", tags = {"订单条目" },  notes = "根据物品采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}/createrin")
    public ResponseEntity<EMPODetailDTO> createRinByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.createRin(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-GenId-all')")
    @ApiOperation(value = "根据物品采购申请订单条目", tags = {"订单条目" },  notes = "根据物品采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}/genid")
    public ResponseEntity<EMPODetailDTO> genIdByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.genId(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据物品采购申请保存订单条目", tags = {"订单条目" },  notes = "根据物品采购申请保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/save")
    public ResponseEntity<Boolean> saveByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据物品采购申请批量保存订单条目", tags = {"订单条目" },  notes = "根据物品采购申请批量保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
             domain.setWplistid(emwplist_id);
        }
        empodetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据物品采购申请获取已关闭", tags = {"订单条目" } ,notes = "根据物品采购申请获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchclosed")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailClosedByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据物品采购申请查询已关闭", tags = {"订单条目" } ,notes = "根据物品采购申请查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchclosed")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailClosedByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据物品采购申请获取DEFAULT", tags = {"订单条目" } ,notes = "根据物品采购申请获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchdefault")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailDefaultByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据物品采购申请查询DEFAULT", tags = {"订单条目" } ,notes = "根据物品采购申请查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchdefault")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailDefaultByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据物品采购申请获取近五年采购", tags = {"订单条目" } ,notes = "根据物品采购申请获取近五年采购")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchlateryear")
	public ResponseEntity<List<HashMap>> fetchEMPODetailLaterYearByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据物品采购申请查询近五年采购", tags = {"订单条目" } ,notes = "根据物品采购申请查询近五年采购")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchlateryear")
	public ResponseEntity<Page<HashMap>> searchEMPODetailLaterYearByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据物品采购申请获取待记账", tags = {"订单条目" } ,notes = "根据物品采购申请获取待记账")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchwaitbook")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitBookByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据物品采购申请查询待记账", tags = {"订单条目" } ,notes = "根据物品采购申请查询待记账")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchwaitbook")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitBookByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据物品采购申请获取待验收", tags = {"订单条目" } ,notes = "根据物品采购申请获取待验收")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchwaitcheck")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitCheckByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据物品采购申请查询待验收", tags = {"订单条目" } ,notes = "根据物品采购申请查询待验收")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchwaitcheck")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitCheckByEMItemEMWPList(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据服务商物品采购申请建立订单条目", tags = {"订单条目" },  notes = "根据服务商物品采购申请建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails")
    public ResponseEntity<EMPODetailDTO> createByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
		empodetailService.create(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据服务商物品采购申请批量建立订单条目", tags = {"订单条目" },  notes = "根据服务商物品采购申请批量建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        empodetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "empodetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据服务商物品采购申请更新订单条目", tags = {"订单条目" },  notes = "根据服务商物品采购申请更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> updateByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain.setEmpodetailid(empodetail_id);
		empodetailService.update(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByEntities(this.empodetailMapping.toDomain(#empodetaildtos)),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据服务商物品采购申请批量更新订单条目", tags = {"订单条目" },  notes = "根据服务商物品采购申请批量更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        empodetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据服务商物品采购申请删除订单条目", tags = {"订单条目" },  notes = "根据服务商物品采购申请删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(empodetailService.remove(empodetail_id));
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByIds(#ids),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据服务商物品采购申请批量删除订单条目", tags = {"订单条目" },  notes = "根据服务商物品采购申请批量删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMItemEMWPList(@RequestBody List<String> ids) {
        empodetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.empodetailMapping.toDomain(returnObject.body),'eam-EMPODetail-Get')")
    @ApiOperation(value = "根据服务商物品采购申请获取订单条目", tags = {"订单条目" },  notes = "根据服务商物品采购申请获取订单条目")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> getByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id) {
        EMPODetail domain = empodetailService.get(empodetail_id);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商物品采购申请获取订单条目草稿", tags = {"订单条目" },  notes = "根据服务商物品采购申请获取订单条目草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/getdraft")
    public ResponseEntity<EMPODetailDTO> getDraftByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
        EMPODetail domain = new EMPODetail();
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailMapping.toDto(empodetailService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-Check-all')")
    @ApiOperation(value = "根据服务商物品采购申请订单条目", tags = {"订单条目" },  notes = "根据服务商物品采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}/check")
    public ResponseEntity<EMPODetailDTO> checkByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.check(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @ApiOperation(value = "根据服务商物品采购申请检查订单条目", tags = {"订单条目" },  notes = "根据服务商物品采购申请检查订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(empodetailService.checkKey(empodetailMapping.toDomain(empodetaildto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-CreateRin-all')")
    @ApiOperation(value = "根据服务商物品采购申请订单条目", tags = {"订单条目" },  notes = "根据服务商物品采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}/createrin")
    public ResponseEntity<EMPODetailDTO> createRinByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.createRin(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-GenId-all')")
    @ApiOperation(value = "根据服务商物品采购申请订单条目", tags = {"订单条目" },  notes = "根据服务商物品采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}/genid")
    public ResponseEntity<EMPODetailDTO> genIdByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.genId(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据服务商物品采购申请保存订单条目", tags = {"订单条目" },  notes = "根据服务商物品采购申请保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/save")
    public ResponseEntity<Boolean> saveByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据服务商物品采购申请批量保存订单条目", tags = {"订单条目" },  notes = "根据服务商物品采购申请批量保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
             domain.setWplistid(emwplist_id);
        }
        empodetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据服务商物品采购申请获取已关闭", tags = {"订单条目" } ,notes = "根据服务商物品采购申请获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchclosed")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailClosedByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据服务商物品采购申请查询已关闭", tags = {"订单条目" } ,notes = "根据服务商物品采购申请查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchclosed")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailClosedByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据服务商物品采购申请获取DEFAULT", tags = {"订单条目" } ,notes = "根据服务商物品采购申请获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchdefault")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailDefaultByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据服务商物品采购申请查询DEFAULT", tags = {"订单条目" } ,notes = "根据服务商物品采购申请查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchdefault")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailDefaultByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据服务商物品采购申请获取近五年采购", tags = {"订单条目" } ,notes = "根据服务商物品采购申请获取近五年采购")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchlateryear")
	public ResponseEntity<List<HashMap>> fetchEMPODetailLaterYearByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据服务商物品采购申请查询近五年采购", tags = {"订单条目" } ,notes = "根据服务商物品采购申请查询近五年采购")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchlateryear")
	public ResponseEntity<Page<HashMap>> searchEMPODetailLaterYearByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据服务商物品采购申请获取待记账", tags = {"订单条目" } ,notes = "根据服务商物品采购申请获取待记账")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchwaitbook")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitBookByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据服务商物品采购申请查询待记账", tags = {"订单条目" } ,notes = "根据服务商物品采购申请查询待记账")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchwaitbook")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitBookByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据服务商物品采购申请获取待验收", tags = {"订单条目" } ,notes = "根据服务商物品采购申请获取待验收")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchwaitcheck")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitCheckByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据服务商物品采购申请查询待验收", tags = {"订单条目" } ,notes = "根据服务商物品采购申请查询待验收")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchwaitcheck")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitCheckByEMServiceEMItemEMWPList(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据仓库库位物品采购申请建立订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品采购申请建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails")
    public ResponseEntity<EMPODetailDTO> createByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
		empodetailService.create(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据仓库库位物品采购申请批量建立订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品采购申请批量建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> createBatchByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        empodetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "empodetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据仓库库位物品采购申请更新订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品采购申请更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> updateByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain.setEmpodetailid(empodetail_id);
		empodetailService.update(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByEntities(this.empodetailMapping.toDomain(#empodetaildtos)),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据仓库库位物品采购申请批量更新订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品采购申请批量更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        empodetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据仓库库位物品采购申请删除订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品采购申请删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<Boolean> removeByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(empodetailService.remove(empodetail_id));
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByIds(#ids),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据仓库库位物品采购申请批量删除订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品采购申请批量删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMStorePartEMItemEMWPList(@RequestBody List<String> ids) {
        empodetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.empodetailMapping.toDomain(returnObject.body),'eam-EMPODetail-Get')")
    @ApiOperation(value = "根据仓库库位物品采购申请获取订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品采购申请获取订单条目")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> getByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id) {
        EMPODetail domain = empodetailService.get(empodetail_id);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库库位物品采购申请获取订单条目草稿", tags = {"订单条目" },  notes = "根据仓库库位物品采购申请获取订单条目草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/getdraft")
    public ResponseEntity<EMPODetailDTO> getDraftByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
        EMPODetail domain = new EMPODetail();
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailMapping.toDto(empodetailService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-Check-all')")
    @ApiOperation(value = "根据仓库库位物品采购申请订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}/check")
    public ResponseEntity<EMPODetailDTO> checkByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.check(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @ApiOperation(value = "根据仓库库位物品采购申请检查订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品采购申请检查订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(empodetailService.checkKey(empodetailMapping.toDomain(empodetaildto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-CreateRin-all')")
    @ApiOperation(value = "根据仓库库位物品采购申请订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}/createrin")
    public ResponseEntity<EMPODetailDTO> createRinByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.createRin(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-GenId-all')")
    @ApiOperation(value = "根据仓库库位物品采购申请订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}/genid")
    public ResponseEntity<EMPODetailDTO> genIdByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.genId(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据仓库库位物品采购申请保存订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品采购申请保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/save")
    public ResponseEntity<Boolean> saveByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据仓库库位物品采购申请批量保存订单条目", tags = {"订单条目" },  notes = "根据仓库库位物品采购申请批量保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
             domain.setWplistid(emwplist_id);
        }
        empodetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库库位物品采购申请获取已关闭", tags = {"订单条目" } ,notes = "根据仓库库位物品采购申请获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchclosed")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailClosedByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库库位物品采购申请查询已关闭", tags = {"订单条目" } ,notes = "根据仓库库位物品采购申请查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchclosed")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailClosedByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库库位物品采购申请获取DEFAULT", tags = {"订单条目" } ,notes = "根据仓库库位物品采购申请获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchdefault")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailDefaultByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库库位物品采购申请查询DEFAULT", tags = {"订单条目" } ,notes = "根据仓库库位物品采购申请查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchdefault")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailDefaultByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库库位物品采购申请获取近五年采购", tags = {"订单条目" } ,notes = "根据仓库库位物品采购申请获取近五年采购")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchlateryear")
	public ResponseEntity<List<HashMap>> fetchEMPODetailLaterYearByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库库位物品采购申请查询近五年采购", tags = {"订单条目" } ,notes = "根据仓库库位物品采购申请查询近五年采购")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchlateryear")
	public ResponseEntity<Page<HashMap>> searchEMPODetailLaterYearByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库库位物品采购申请获取待记账", tags = {"订单条目" } ,notes = "根据仓库库位物品采购申请获取待记账")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchwaitbook")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitBookByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库库位物品采购申请查询待记账", tags = {"订单条目" } ,notes = "根据仓库库位物品采购申请查询待记账")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchwaitbook")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitBookByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库库位物品采购申请获取待验收", tags = {"订单条目" } ,notes = "根据仓库库位物品采购申请获取待验收")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchwaitcheck")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitCheckByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库库位物品采购申请查询待验收", tags = {"订单条目" } ,notes = "根据仓库库位物品采购申请查询待验收")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchwaitcheck")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitCheckByEMStorePartEMItemEMWPList(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据仓库物品采购申请建立订单条目", tags = {"订单条目" },  notes = "根据仓库物品采购申请建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails")
    public ResponseEntity<EMPODetailDTO> createByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
		empodetailService.create(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据仓库物品采购申请批量建立订单条目", tags = {"订单条目" },  notes = "根据仓库物品采购申请批量建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        empodetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "empodetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据仓库物品采购申请更新订单条目", tags = {"订单条目" },  notes = "根据仓库物品采购申请更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> updateByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain.setEmpodetailid(empodetail_id);
		empodetailService.update(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByEntities(this.empodetailMapping.toDomain(#empodetaildtos)),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据仓库物品采购申请批量更新订单条目", tags = {"订单条目" },  notes = "根据仓库物品采购申请批量更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        empodetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据仓库物品采购申请删除订单条目", tags = {"订单条目" },  notes = "根据仓库物品采购申请删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(empodetailService.remove(empodetail_id));
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByIds(#ids),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据仓库物品采购申请批量删除订单条目", tags = {"订单条目" },  notes = "根据仓库物品采购申请批量删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMItemEMWPList(@RequestBody List<String> ids) {
        empodetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.empodetailMapping.toDomain(returnObject.body),'eam-EMPODetail-Get')")
    @ApiOperation(value = "根据仓库物品采购申请获取订单条目", tags = {"订单条目" },  notes = "根据仓库物品采购申请获取订单条目")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> getByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id) {
        EMPODetail domain = empodetailService.get(empodetail_id);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库物品采购申请获取订单条目草稿", tags = {"订单条目" },  notes = "根据仓库物品采购申请获取订单条目草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/getdraft")
    public ResponseEntity<EMPODetailDTO> getDraftByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
        EMPODetail domain = new EMPODetail();
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailMapping.toDto(empodetailService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-Check-all')")
    @ApiOperation(value = "根据仓库物品采购申请订单条目", tags = {"订单条目" },  notes = "根据仓库物品采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}/check")
    public ResponseEntity<EMPODetailDTO> checkByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.check(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @ApiOperation(value = "根据仓库物品采购申请检查订单条目", tags = {"订单条目" },  notes = "根据仓库物品采购申请检查订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(empodetailService.checkKey(empodetailMapping.toDomain(empodetaildto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-CreateRin-all')")
    @ApiOperation(value = "根据仓库物品采购申请订单条目", tags = {"订单条目" },  notes = "根据仓库物品采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}/createrin")
    public ResponseEntity<EMPODetailDTO> createRinByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.createRin(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-GenId-all')")
    @ApiOperation(value = "根据仓库物品采购申请订单条目", tags = {"订单条目" },  notes = "根据仓库物品采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}/genid")
    public ResponseEntity<EMPODetailDTO> genIdByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.genId(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据仓库物品采购申请保存订单条目", tags = {"订单条目" },  notes = "根据仓库物品采购申请保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/save")
    public ResponseEntity<Boolean> saveByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据仓库物品采购申请批量保存订单条目", tags = {"订单条目" },  notes = "根据仓库物品采购申请批量保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
             domain.setWplistid(emwplist_id);
        }
        empodetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库物品采购申请获取已关闭", tags = {"订单条目" } ,notes = "根据仓库物品采购申请获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchclosed")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailClosedByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库物品采购申请查询已关闭", tags = {"订单条目" } ,notes = "根据仓库物品采购申请查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchclosed")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailClosedByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库物品采购申请获取DEFAULT", tags = {"订单条目" } ,notes = "根据仓库物品采购申请获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchdefault")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailDefaultByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库物品采购申请查询DEFAULT", tags = {"订单条目" } ,notes = "根据仓库物品采购申请查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchdefault")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailDefaultByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库物品采购申请获取近五年采购", tags = {"订单条目" } ,notes = "根据仓库物品采购申请获取近五年采购")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchlateryear")
	public ResponseEntity<List<HashMap>> fetchEMPODetailLaterYearByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库物品采购申请查询近五年采购", tags = {"订单条目" } ,notes = "根据仓库物品采购申请查询近五年采购")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchlateryear")
	public ResponseEntity<Page<HashMap>> searchEMPODetailLaterYearByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库物品采购申请获取待记账", tags = {"订单条目" } ,notes = "根据仓库物品采购申请获取待记账")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchwaitbook")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitBookByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库物品采购申请查询待记账", tags = {"订单条目" } ,notes = "根据仓库物品采购申请查询待记账")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchwaitbook")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitBookByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库物品采购申请获取待验收", tags = {"订单条目" } ,notes = "根据仓库物品采购申请获取待验收")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchwaitcheck")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitCheckByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库物品采购申请查询待验收", tags = {"订单条目" } ,notes = "根据仓库物品采购申请查询待验收")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchwaitcheck")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitCheckByEMStoreEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品建立订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails")
    public ResponseEntity<EMPODetailDTO> createByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
		empodetailService.create(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品批量建立订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品批量建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        empodetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "empodetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品更新订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> updateByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain.setEmpodetailid(empodetail_id);
		empodetailService.update(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByEntities(this.empodetailMapping.toDomain(#empodetaildtos)),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品批量更新订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品批量更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setItemid(emitem_id);
        }
        empodetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品删除订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/{empodetail_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(empodetailService.remove(empodetail_id));
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByIds(#ids),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品批量删除订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品批量删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMStorePartEMItem(@RequestBody List<String> ids) {
        empodetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.empodetailMapping.toDomain(returnObject.body),'eam-EMPODetail-Get')")
    @ApiOperation(value = "根据仓库仓库库位物品获取订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品获取订单条目")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> getByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id) {
        EMPODetail domain = empodetailService.get(empodetail_id);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库仓库库位物品获取订单条目草稿", tags = {"订单条目" },  notes = "根据仓库仓库库位物品获取订单条目草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/getdraft")
    public ResponseEntity<EMPODetailDTO> getDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id) {
        EMPODetail domain = new EMPODetail();
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailMapping.toDto(empodetailService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-Check-all')")
    @ApiOperation(value = "根据仓库仓库库位物品订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/{empodetail_id}/check")
    public ResponseEntity<EMPODetailDTO> checkByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain = empodetailService.check(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @ApiOperation(value = "根据仓库仓库库位物品检查订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品检查订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailDTO empodetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(empodetailService.checkKey(empodetailMapping.toDomain(empodetaildto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-CreateRin-all')")
    @ApiOperation(value = "根据仓库仓库库位物品订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/{empodetail_id}/createrin")
    public ResponseEntity<EMPODetailDTO> createRinByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain = empodetailService.createRin(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-GenId-all')")
    @ApiOperation(value = "根据仓库仓库库位物品订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/{empodetail_id}/genid")
    public ResponseEntity<EMPODetailDTO> genIdByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        domain = empodetailService.genId(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品保存订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/save")
    public ResponseEntity<Boolean> saveByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品批量保存订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品批量保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
             domain.setItemid(emitem_id);
        }
        empodetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取已关闭", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/fetchclosed")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailClosedByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询已关闭", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/searchclosed")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailClosedByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取DEFAULT", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/fetchdefault")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询DEFAULT", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/searchdefault")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库仓库库位物品获取近五年采购", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品获取近五年采购")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/fetchlateryear")
	public ResponseEntity<List<HashMap>> fetchEMPODetailLaterYearByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库仓库库位物品查询近五年采购", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品查询近五年采购")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/searchlateryear")
	public ResponseEntity<Page<HashMap>> searchEMPODetailLaterYearByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取待记账", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品获取待记账")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/fetchwaitbook")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitBookByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询待记账", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品查询待记账")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/searchwaitbook")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitBookByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取待验收", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品获取待验收")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/fetchwaitcheck")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitCheckByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询待验收", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品查询待验收")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/empodetails/searchwaitcheck")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitCheckByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请建立订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品采购申请建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails")
    public ResponseEntity<EMPODetailDTO> createByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
		empodetailService.create(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请批量建立订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品采购申请批量建立订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        empodetailService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "empodetail" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请更新订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品采购申请更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> updateByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain.setEmpodetailid(empodetail_id);
		empodetailService.update(domain);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByEntities(this.empodetailMapping.toDomain(#empodetaildtos)),'eam-EMPODetail-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请批量更新订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品采购申请批量更新订单条目")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
            domain.setWplistid(emwplist_id);
        }
        empodetailService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.empodetailService.get(#empodetail_id),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请删除订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品采购申请删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id) {
		return ResponseEntity.status(HttpStatus.OK).body(empodetailService.remove(empodetail_id));
    }

    @PreAuthorize("hasPermission(this.empodetailService.getEmpodetailByIds(#ids),'eam-EMPODetail-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请批量删除订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品采购申请批量删除订单条目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMStorePartEMItemEMWPList(@RequestBody List<String> ids) {
        empodetailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.empodetailMapping.toDomain(returnObject.body),'eam-EMPODetail-Get')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请获取订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品采购申请获取订单条目")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}")
    public ResponseEntity<EMPODetailDTO> getByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id) {
        EMPODetail domain = empodetailService.get(empodetail_id);
        EMPODetailDTO dto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库仓库库位物品采购申请获取订单条目草稿", tags = {"订单条目" },  notes = "根据仓库仓库库位物品采购申请获取订单条目草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/getdraft")
    public ResponseEntity<EMPODetailDTO> getDraftByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
        EMPODetail domain = new EMPODetail();
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailMapping.toDto(empodetailService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-Check-all')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}/check")
    public ResponseEntity<EMPODetailDTO> checkByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.check(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @ApiOperation(value = "根据仓库仓库库位物品采购申请检查订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品采购申请检查订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(empodetailService.checkKey(empodetailMapping.toDomain(empodetaildto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-CreateRin-all')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}/createrin")
    public ResponseEntity<EMPODetailDTO> createRinByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.createRin(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-GenId-all')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品采购申请订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/{empodetail_id}/genid")
    public ResponseEntity<EMPODetailDTO> genIdByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @PathVariable("empodetail_id") String empodetail_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        domain = empodetailService.genId(domain) ;
        empodetaildto = empodetailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(empodetaildto);
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildto),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请保存订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品采购申请保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/save")
    public ResponseEntity<Boolean> saveByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailDTO empodetaildto) {
        EMPODetail domain = empodetailMapping.toDomain(empodetaildto);
        domain.setWplistid(emwplist_id);
        return ResponseEntity.status(HttpStatus.OK).body(empodetailService.save(domain));
    }

    @PreAuthorize("hasPermission(this.empodetailMapping.toDomain(#empodetaildtos),'eam-EMPODetail-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请批量保存订单条目", tags = {"订单条目" },  notes = "根据仓库仓库库位物品采购申请批量保存订单条目")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody List<EMPODetailDTO> empodetaildtos) {
        List<EMPODetail> domainlist=empodetailMapping.toDomain(empodetaildtos);
        for(EMPODetail domain:domainlist){
             domain.setWplistid(emwplist_id);
        }
        empodetailService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品采购申请获取已关闭", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品采购申请获取已关闭")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchclosed")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailClosedByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchClosed-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品采购申请查询已关闭", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品采购申请查询已关闭")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchclosed")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailClosedByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchClosed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品采购申请获取DEFAULT", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品采购申请获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchdefault")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailDefaultByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchDefault-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品采购申请查询DEFAULT", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品采购申请查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchdefault")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailDefaultByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库仓库库位物品采购申请获取近五年采购", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品采购申请获取近五年采购")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchlateryear")
	public ResponseEntity<List<HashMap>> fetchEMPODetailLaterYearByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库仓库库位物品采购申请查询近五年采购", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品采购申请查询近五年采购")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchlateryear")
	public ResponseEntity<Page<HashMap>> searchEMPODetailLaterYearByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<HashMap> domains = empodetailService.searchLaterYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品采购申请获取待记账", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品采购申请获取待记账")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchwaitbook")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitBookByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitBook-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品采购申请查询待记账", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品采购申请查询待记账")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchwaitbook")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitBookByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitBook(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品采购申请获取待验收", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品采购申请获取待验收")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/fetchwaitcheck")
	public ResponseEntity<List<EMPODetailDTO>> fetchEMPODetailWaitCheckByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id,EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
        List<EMPODetailDTO> list = empodetailMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPODetail-searchWaitCheck-all') and hasPermission(#context,'eam-EMPODetail-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品采购申请查询待验收", tags = {"订单条目" } ,notes = "根据仓库仓库库位物品采购申请查询待验收")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/empodetails/searchwaitcheck")
	public ResponseEntity<Page<EMPODetailDTO>> searchEMPODetailWaitCheckByEMStoreEMStorePartEMItemEMWPList(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMPODetailSearchContext context) {
        context.setN_wplistid_eq(emwplist_id);
        Page<EMPODetail> domains = empodetailService.searchWaitCheck(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(empodetailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

