package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMResService;
import cn.ibizlab.eam.core.eam_core.service.IEMResServiceService;
import cn.ibizlab.eam.core.eam_core.filter.EMResServiceSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"服务资源" })
@RestController("WebApi-emresservice")
@RequestMapping("")
public class EMResServiceResource {

    @Autowired
    public IEMResServiceService emresserviceService;

    @Autowired
    @Lazy
    public EMResServiceMapping emresserviceMapping;

    @PreAuthorize("hasPermission(this.emresserviceMapping.toDomain(#emresservicedto),'eam-EMResService-Create')")
    @ApiOperation(value = "新建服务资源", tags = {"服务资源" },  notes = "新建服务资源")
	@RequestMapping(method = RequestMethod.POST, value = "/emresservices")
    public ResponseEntity<EMResServiceDTO> create(@Validated @RequestBody EMResServiceDTO emresservicedto) {
        EMResService domain = emresserviceMapping.toDomain(emresservicedto);
		emresserviceService.create(domain);
        EMResServiceDTO dto = emresserviceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emresserviceMapping.toDomain(#emresservicedtos),'eam-EMResService-Create')")
    @ApiOperation(value = "批量新建服务资源", tags = {"服务资源" },  notes = "批量新建服务资源")
	@RequestMapping(method = RequestMethod.POST, value = "/emresservices/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMResServiceDTO> emresservicedtos) {
        emresserviceService.createBatch(emresserviceMapping.toDomain(emresservicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emresservice" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emresserviceService.get(#emresservice_id),'eam-EMResService-Update')")
    @ApiOperation(value = "更新服务资源", tags = {"服务资源" },  notes = "更新服务资源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emresservices/{emresservice_id}")
    public ResponseEntity<EMResServiceDTO> update(@PathVariable("emresservice_id") String emresservice_id, @RequestBody EMResServiceDTO emresservicedto) {
		EMResService domain  = emresserviceMapping.toDomain(emresservicedto);
        domain .setEmresserviceid(emresservice_id);
		emresserviceService.update(domain );
		EMResServiceDTO dto = emresserviceMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emresserviceService.getEmresserviceByEntities(this.emresserviceMapping.toDomain(#emresservicedtos)),'eam-EMResService-Update')")
    @ApiOperation(value = "批量更新服务资源", tags = {"服务资源" },  notes = "批量更新服务资源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emresservices/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMResServiceDTO> emresservicedtos) {
        emresserviceService.updateBatch(emresserviceMapping.toDomain(emresservicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emresserviceService.get(#emresservice_id),'eam-EMResService-Remove')")
    @ApiOperation(value = "删除服务资源", tags = {"服务资源" },  notes = "删除服务资源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emresservices/{emresservice_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emresservice_id") String emresservice_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emresserviceService.remove(emresservice_id));
    }

    @PreAuthorize("hasPermission(this.emresserviceService.getEmresserviceByIds(#ids),'eam-EMResService-Remove')")
    @ApiOperation(value = "批量删除服务资源", tags = {"服务资源" },  notes = "批量删除服务资源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emresservices/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emresserviceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emresserviceMapping.toDomain(returnObject.body),'eam-EMResService-Get')")
    @ApiOperation(value = "获取服务资源", tags = {"服务资源" },  notes = "获取服务资源")
	@RequestMapping(method = RequestMethod.GET, value = "/emresservices/{emresservice_id}")
    public ResponseEntity<EMResServiceDTO> get(@PathVariable("emresservice_id") String emresservice_id) {
        EMResService domain = emresserviceService.get(emresservice_id);
        EMResServiceDTO dto = emresserviceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取服务资源草稿", tags = {"服务资源" },  notes = "获取服务资源草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emresservices/getdraft")
    public ResponseEntity<EMResServiceDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emresserviceMapping.toDto(emresserviceService.getDraft(new EMResService())));
    }

    @ApiOperation(value = "检查服务资源", tags = {"服务资源" },  notes = "检查服务资源")
	@RequestMapping(method = RequestMethod.POST, value = "/emresservices/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMResServiceDTO emresservicedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emresserviceService.checkKey(emresserviceMapping.toDomain(emresservicedto)));
    }

    @PreAuthorize("hasPermission(this.emresserviceMapping.toDomain(#emresservicedto),'eam-EMResService-Save')")
    @ApiOperation(value = "保存服务资源", tags = {"服务资源" },  notes = "保存服务资源")
	@RequestMapping(method = RequestMethod.POST, value = "/emresservices/save")
    public ResponseEntity<Boolean> save(@RequestBody EMResServiceDTO emresservicedto) {
        return ResponseEntity.status(HttpStatus.OK).body(emresserviceService.save(emresserviceMapping.toDomain(emresservicedto)));
    }

    @PreAuthorize("hasPermission(this.emresserviceMapping.toDomain(#emresservicedtos),'eam-EMResService-Save')")
    @ApiOperation(value = "批量保存服务资源", tags = {"服务资源" },  notes = "批量保存服务资源")
	@RequestMapping(method = RequestMethod.POST, value = "/emresservices/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMResServiceDTO> emresservicedtos) {
        emresserviceService.saveBatch(emresserviceMapping.toDomain(emresservicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMResService-searchDefault-all') and hasPermission(#context,'eam-EMResService-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"服务资源" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emresservices/fetchdefault")
	public ResponseEntity<List<EMResServiceDTO>> fetchDefault(EMResServiceSearchContext context) {
        Page<EMResService> domains = emresserviceService.searchDefault(context) ;
        List<EMResServiceDTO> list = emresserviceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMResService-searchDefault-all') and hasPermission(#context,'eam-EMResService-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"服务资源" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emresservices/searchdefault")
	public ResponseEntity<Page<EMResServiceDTO>> searchDefault(@RequestBody EMResServiceSearchContext context) {
        Page<EMResService> domains = emresserviceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emresserviceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

