package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMRigging;
import cn.ibizlab.eam.core.eam_core.service.IEMRiggingService;
import cn.ibizlab.eam.core.eam_core.filter.EMRiggingSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工具库管理" })
@RestController("WebApi-emrigging")
@RequestMapping("")
public class EMRiggingResource {

    @Autowired
    public IEMRiggingService emriggingService;

    @Autowired
    @Lazy
    public EMRiggingMapping emriggingMapping;

    @PreAuthorize("hasPermission(this.emriggingMapping.toDomain(#emriggingdto),'eam-EMRigging-Create')")
    @ApiOperation(value = "新建工具库管理", tags = {"工具库管理" },  notes = "新建工具库管理")
	@RequestMapping(method = RequestMethod.POST, value = "/emriggings")
    public ResponseEntity<EMRiggingDTO> create(@Validated @RequestBody EMRiggingDTO emriggingdto) {
        EMRigging domain = emriggingMapping.toDomain(emriggingdto);
		emriggingService.create(domain);
        EMRiggingDTO dto = emriggingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emriggingMapping.toDomain(#emriggingdtos),'eam-EMRigging-Create')")
    @ApiOperation(value = "批量新建工具库管理", tags = {"工具库管理" },  notes = "批量新建工具库管理")
	@RequestMapping(method = RequestMethod.POST, value = "/emriggings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMRiggingDTO> emriggingdtos) {
        emriggingService.createBatch(emriggingMapping.toDomain(emriggingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emrigging" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emriggingService.get(#emrigging_id),'eam-EMRigging-Update')")
    @ApiOperation(value = "更新工具库管理", tags = {"工具库管理" },  notes = "更新工具库管理")
	@RequestMapping(method = RequestMethod.PUT, value = "/emriggings/{emrigging_id}")
    public ResponseEntity<EMRiggingDTO> update(@PathVariable("emrigging_id") String emrigging_id, @RequestBody EMRiggingDTO emriggingdto) {
		EMRigging domain  = emriggingMapping.toDomain(emriggingdto);
        domain .setEmriggingid(emrigging_id);
		emriggingService.update(domain );
		EMRiggingDTO dto = emriggingMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emriggingService.getEmriggingByEntities(this.emriggingMapping.toDomain(#emriggingdtos)),'eam-EMRigging-Update')")
    @ApiOperation(value = "批量更新工具库管理", tags = {"工具库管理" },  notes = "批量更新工具库管理")
	@RequestMapping(method = RequestMethod.PUT, value = "/emriggings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMRiggingDTO> emriggingdtos) {
        emriggingService.updateBatch(emriggingMapping.toDomain(emriggingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emriggingService.get(#emrigging_id),'eam-EMRigging-Remove')")
    @ApiOperation(value = "删除工具库管理", tags = {"工具库管理" },  notes = "删除工具库管理")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emriggings/{emrigging_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emrigging_id") String emrigging_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emriggingService.remove(emrigging_id));
    }

    @PreAuthorize("hasPermission(this.emriggingService.getEmriggingByIds(#ids),'eam-EMRigging-Remove')")
    @ApiOperation(value = "批量删除工具库管理", tags = {"工具库管理" },  notes = "批量删除工具库管理")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emriggings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emriggingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emriggingMapping.toDomain(returnObject.body),'eam-EMRigging-Get')")
    @ApiOperation(value = "获取工具库管理", tags = {"工具库管理" },  notes = "获取工具库管理")
	@RequestMapping(method = RequestMethod.GET, value = "/emriggings/{emrigging_id}")
    public ResponseEntity<EMRiggingDTO> get(@PathVariable("emrigging_id") String emrigging_id) {
        EMRigging domain = emriggingService.get(emrigging_id);
        EMRiggingDTO dto = emriggingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工具库管理草稿", tags = {"工具库管理" },  notes = "获取工具库管理草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emriggings/getdraft")
    public ResponseEntity<EMRiggingDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emriggingMapping.toDto(emriggingService.getDraft(new EMRigging())));
    }

    @ApiOperation(value = "检查工具库管理", tags = {"工具库管理" },  notes = "检查工具库管理")
	@RequestMapping(method = RequestMethod.POST, value = "/emriggings/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMRiggingDTO emriggingdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emriggingService.checkKey(emriggingMapping.toDomain(emriggingdto)));
    }

    @PreAuthorize("hasPermission(this.emriggingMapping.toDomain(#emriggingdto),'eam-EMRigging-Save')")
    @ApiOperation(value = "保存工具库管理", tags = {"工具库管理" },  notes = "保存工具库管理")
	@RequestMapping(method = RequestMethod.POST, value = "/emriggings/save")
    public ResponseEntity<Boolean> save(@RequestBody EMRiggingDTO emriggingdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emriggingService.save(emriggingMapping.toDomain(emriggingdto)));
    }

    @PreAuthorize("hasPermission(this.emriggingMapping.toDomain(#emriggingdtos),'eam-EMRigging-Save')")
    @ApiOperation(value = "批量保存工具库管理", tags = {"工具库管理" },  notes = "批量保存工具库管理")
	@RequestMapping(method = RequestMethod.POST, value = "/emriggings/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMRiggingDTO> emriggingdtos) {
        emriggingService.saveBatch(emriggingMapping.toDomain(emriggingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRigging-searchDefault-all') and hasPermission(#context,'eam-EMRigging-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"工具库管理" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emriggings/fetchdefault")
	public ResponseEntity<List<EMRiggingDTO>> fetchDefault(EMRiggingSearchContext context) {
        Page<EMRigging> domains = emriggingService.searchDefault(context) ;
        List<EMRiggingDTO> list = emriggingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRigging-searchDefault-all') and hasPermission(#context,'eam-EMRigging-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"工具库管理" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emriggings/searchdefault")
	public ResponseEntity<Page<EMRiggingDTO>> searchDefault(@RequestBody EMRiggingSearchContext context) {
        Page<EMRigging> domains = emriggingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emriggingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

