package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEICell;
import cn.ibizlab.eam.core.eam_core.service.IEMEICellService;
import cn.ibizlab.eam.core.eam_core.filter.EMEICellSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"对讲机" })
@RestController("WebApi-emeicell")
@RequestMapping("")
public class EMEICellResource {

    @Autowired
    public IEMEICellService emeicellService;

    @Autowired
    @Lazy
    public EMEICellMapping emeicellMapping;

    @PreAuthorize("hasPermission(this.emeicellMapping.toDomain(#emeicelldto),'eam-EMEICell-Create')")
    @ApiOperation(value = "新建对讲机", tags = {"对讲机" },  notes = "新建对讲机")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicells")
    public ResponseEntity<EMEICellDTO> create(@Validated @RequestBody EMEICellDTO emeicelldto) {
        EMEICell domain = emeicellMapping.toDomain(emeicelldto);
		emeicellService.create(domain);
        EMEICellDTO dto = emeicellMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeicellMapping.toDomain(#emeicelldtos),'eam-EMEICell-Create')")
    @ApiOperation(value = "批量新建对讲机", tags = {"对讲机" },  notes = "批量新建对讲机")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicells/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEICellDTO> emeicelldtos) {
        emeicellService.createBatch(emeicellMapping.toDomain(emeicelldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeicell" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeicellService.get(#emeicell_id),'eam-EMEICell-Update')")
    @ApiOperation(value = "更新对讲机", tags = {"对讲机" },  notes = "更新对讲机")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeicells/{emeicell_id}")
    public ResponseEntity<EMEICellDTO> update(@PathVariable("emeicell_id") String emeicell_id, @RequestBody EMEICellDTO emeicelldto) {
		EMEICell domain  = emeicellMapping.toDomain(emeicelldto);
        domain .setEmeicellid(emeicell_id);
		emeicellService.update(domain );
		EMEICellDTO dto = emeicellMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeicellService.getEmeicellByEntities(this.emeicellMapping.toDomain(#emeicelldtos)),'eam-EMEICell-Update')")
    @ApiOperation(value = "批量更新对讲机", tags = {"对讲机" },  notes = "批量更新对讲机")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeicells/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEICellDTO> emeicelldtos) {
        emeicellService.updateBatch(emeicellMapping.toDomain(emeicelldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeicellService.get(#emeicell_id),'eam-EMEICell-Remove')")
    @ApiOperation(value = "删除对讲机", tags = {"对讲机" },  notes = "删除对讲机")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeicells/{emeicell_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeicell_id") String emeicell_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeicellService.remove(emeicell_id));
    }

    @PreAuthorize("hasPermission(this.emeicellService.getEmeicellByIds(#ids),'eam-EMEICell-Remove')")
    @ApiOperation(value = "批量删除对讲机", tags = {"对讲机" },  notes = "批量删除对讲机")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeicells/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeicellService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeicellMapping.toDomain(returnObject.body),'eam-EMEICell-Get')")
    @ApiOperation(value = "获取对讲机", tags = {"对讲机" },  notes = "获取对讲机")
	@RequestMapping(method = RequestMethod.GET, value = "/emeicells/{emeicell_id}")
    public ResponseEntity<EMEICellDTO> get(@PathVariable("emeicell_id") String emeicell_id) {
        EMEICell domain = emeicellService.get(emeicell_id);
        EMEICellDTO dto = emeicellMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取对讲机草稿", tags = {"对讲机" },  notes = "获取对讲机草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeicells/getdraft")
    public ResponseEntity<EMEICellDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emeicellMapping.toDto(emeicellService.getDraft(new EMEICell())));
    }

    @ApiOperation(value = "检查对讲机", tags = {"对讲机" },  notes = "检查对讲机")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicells/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEICellDTO emeicelldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeicellService.checkKey(emeicellMapping.toDomain(emeicelldto)));
    }

    @PreAuthorize("hasPermission(this.emeicellMapping.toDomain(#emeicelldto),'eam-EMEICell-Save')")
    @ApiOperation(value = "保存对讲机", tags = {"对讲机" },  notes = "保存对讲机")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicells/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEICellDTO emeicelldto) {
        return ResponseEntity.status(HttpStatus.OK).body(emeicellService.save(emeicellMapping.toDomain(emeicelldto)));
    }

    @PreAuthorize("hasPermission(this.emeicellMapping.toDomain(#emeicelldtos),'eam-EMEICell-Save')")
    @ApiOperation(value = "批量保存对讲机", tags = {"对讲机" },  notes = "批量保存对讲机")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicells/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEICellDTO> emeicelldtos) {
        emeicellService.saveBatch(emeicellMapping.toDomain(emeicelldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEICell-searchDefault-all') and hasPermission(#context,'eam-EMEICell-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"对讲机" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeicells/fetchdefault")
	public ResponseEntity<List<EMEICellDTO>> fetchDefault(EMEICellSearchContext context) {
        Page<EMEICell> domains = emeicellService.searchDefault(context) ;
        List<EMEICellDTO> list = emeicellMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEICell-searchDefault-all') and hasPermission(#context,'eam-EMEICell-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"对讲机" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeicells/searchdefault")
	public ResponseEntity<Page<EMEICellDTO>> searchDefault(@RequestBody EMEICellSearchContext context) {
        Page<EMEICell> domains = emeicellService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeicellMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

