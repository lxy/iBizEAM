package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_PT;
import cn.ibizlab.eam.webapi.dto.EMWO_PTDTO;
import cn.ibizlab.eam.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="WebApiEMWO_PTMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface EMWO_PTMapping extends MappingBase<EMWO_PTDTO, EMWO_PT> {


}

