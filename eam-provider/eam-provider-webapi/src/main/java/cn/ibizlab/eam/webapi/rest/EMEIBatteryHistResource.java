package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEIBatteryHist;
import cn.ibizlab.eam.core.eam_core.service.IEMEIBatteryHistService;
import cn.ibizlab.eam.core.eam_core.filter.EMEIBatteryHistSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"电瓶检修记录" })
@RestController("WebApi-emeibatteryhist")
@RequestMapping("")
public class EMEIBatteryHistResource {

    @Autowired
    public IEMEIBatteryHistService emeibatteryhistService;

    @Autowired
    @Lazy
    public EMEIBatteryHistMapping emeibatteryhistMapping;

    @PreAuthorize("hasPermission(this.emeibatteryhistMapping.toDomain(#emeibatteryhistdto),'eam-EMEIBatteryHist-Create')")
    @ApiOperation(value = "新建电瓶检修记录", tags = {"电瓶检修记录" },  notes = "新建电瓶检修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatteryhists")
    public ResponseEntity<EMEIBatteryHistDTO> create(@Validated @RequestBody EMEIBatteryHistDTO emeibatteryhistdto) {
        EMEIBatteryHist domain = emeibatteryhistMapping.toDomain(emeibatteryhistdto);
		emeibatteryhistService.create(domain);
        EMEIBatteryHistDTO dto = emeibatteryhistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeibatteryhistMapping.toDomain(#emeibatteryhistdtos),'eam-EMEIBatteryHist-Create')")
    @ApiOperation(value = "批量新建电瓶检修记录", tags = {"电瓶检修记录" },  notes = "批量新建电瓶检修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatteryhists/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEIBatteryHistDTO> emeibatteryhistdtos) {
        emeibatteryhistService.createBatch(emeibatteryhistMapping.toDomain(emeibatteryhistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeibatteryhist" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeibatteryhistService.get(#emeibatteryhist_id),'eam-EMEIBatteryHist-Update')")
    @ApiOperation(value = "更新电瓶检修记录", tags = {"电瓶检修记录" },  notes = "更新电瓶检修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeibatteryhists/{emeibatteryhist_id}")
    public ResponseEntity<EMEIBatteryHistDTO> update(@PathVariable("emeibatteryhist_id") String emeibatteryhist_id, @RequestBody EMEIBatteryHistDTO emeibatteryhistdto) {
		EMEIBatteryHist domain  = emeibatteryhistMapping.toDomain(emeibatteryhistdto);
        domain .setEmeibatteryhistid(emeibatteryhist_id);
		emeibatteryhistService.update(domain );
		EMEIBatteryHistDTO dto = emeibatteryhistMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeibatteryhistService.getEmeibatteryhistByEntities(this.emeibatteryhistMapping.toDomain(#emeibatteryhistdtos)),'eam-EMEIBatteryHist-Update')")
    @ApiOperation(value = "批量更新电瓶检修记录", tags = {"电瓶检修记录" },  notes = "批量更新电瓶检修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeibatteryhists/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEIBatteryHistDTO> emeibatteryhistdtos) {
        emeibatteryhistService.updateBatch(emeibatteryhistMapping.toDomain(emeibatteryhistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeibatteryhistService.get(#emeibatteryhist_id),'eam-EMEIBatteryHist-Remove')")
    @ApiOperation(value = "删除电瓶检修记录", tags = {"电瓶检修记录" },  notes = "删除电瓶检修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeibatteryhists/{emeibatteryhist_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeibatteryhist_id") String emeibatteryhist_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeibatteryhistService.remove(emeibatteryhist_id));
    }

    @PreAuthorize("hasPermission(this.emeibatteryhistService.getEmeibatteryhistByIds(#ids),'eam-EMEIBatteryHist-Remove')")
    @ApiOperation(value = "批量删除电瓶检修记录", tags = {"电瓶检修记录" },  notes = "批量删除电瓶检修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeibatteryhists/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeibatteryhistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeibatteryhistMapping.toDomain(returnObject.body),'eam-EMEIBatteryHist-Get')")
    @ApiOperation(value = "获取电瓶检修记录", tags = {"电瓶检修记录" },  notes = "获取电瓶检修记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emeibatteryhists/{emeibatteryhist_id}")
    public ResponseEntity<EMEIBatteryHistDTO> get(@PathVariable("emeibatteryhist_id") String emeibatteryhist_id) {
        EMEIBatteryHist domain = emeibatteryhistService.get(emeibatteryhist_id);
        EMEIBatteryHistDTO dto = emeibatteryhistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取电瓶检修记录草稿", tags = {"电瓶检修记录" },  notes = "获取电瓶检修记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeibatteryhists/getdraft")
    public ResponseEntity<EMEIBatteryHistDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emeibatteryhistMapping.toDto(emeibatteryhistService.getDraft(new EMEIBatteryHist())));
    }

    @ApiOperation(value = "检查电瓶检修记录", tags = {"电瓶检修记录" },  notes = "检查电瓶检修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatteryhists/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEIBatteryHistDTO emeibatteryhistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeibatteryhistService.checkKey(emeibatteryhistMapping.toDomain(emeibatteryhistdto)));
    }

    @PreAuthorize("hasPermission(this.emeibatteryhistMapping.toDomain(#emeibatteryhistdto),'eam-EMEIBatteryHist-Save')")
    @ApiOperation(value = "保存电瓶检修记录", tags = {"电瓶检修记录" },  notes = "保存电瓶检修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatteryhists/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEIBatteryHistDTO emeibatteryhistdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emeibatteryhistService.save(emeibatteryhistMapping.toDomain(emeibatteryhistdto)));
    }

    @PreAuthorize("hasPermission(this.emeibatteryhistMapping.toDomain(#emeibatteryhistdtos),'eam-EMEIBatteryHist-Save')")
    @ApiOperation(value = "批量保存电瓶检修记录", tags = {"电瓶检修记录" },  notes = "批量保存电瓶检修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatteryhists/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEIBatteryHistDTO> emeibatteryhistdtos) {
        emeibatteryhistService.saveBatch(emeibatteryhistMapping.toDomain(emeibatteryhistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEIBatteryHist-searchDefault-all') and hasPermission(#context,'eam-EMEIBatteryHist-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"电瓶检修记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeibatteryhists/fetchdefault")
	public ResponseEntity<List<EMEIBatteryHistDTO>> fetchDefault(EMEIBatteryHistSearchContext context) {
        Page<EMEIBatteryHist> domains = emeibatteryhistService.searchDefault(context) ;
        List<EMEIBatteryHistDTO> list = emeibatteryhistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEIBatteryHist-searchDefault-all') and hasPermission(#context,'eam-EMEIBatteryHist-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"电瓶检修记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeibatteryhists/searchdefault")
	public ResponseEntity<Page<EMEIBatteryHistDTO>> searchDefault(@RequestBody EMEIBatteryHistSearchContext context) {
        Page<EMEIBatteryHist> domains = emeibatteryhistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeibatteryhistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

