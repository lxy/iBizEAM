package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEICamDrop;
import cn.ibizlab.eam.core.eam_core.service.IEMEICamDropService;
import cn.ibizlab.eam.core.eam_core.filter.EMEICamDropSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"探头摘除记录" })
@RestController("WebApi-emeicamdrop")
@RequestMapping("")
public class EMEICamDropResource {

    @Autowired
    public IEMEICamDropService emeicamdropService;

    @Autowired
    @Lazy
    public EMEICamDropMapping emeicamdropMapping;

    @PreAuthorize("hasPermission(this.emeicamdropMapping.toDomain(#emeicamdropdto),'eam-EMEICamDrop-Create')")
    @ApiOperation(value = "新建探头摘除记录", tags = {"探头摘除记录" },  notes = "新建探头摘除记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicamdrops")
    public ResponseEntity<EMEICamDropDTO> create(@Validated @RequestBody EMEICamDropDTO emeicamdropdto) {
        EMEICamDrop domain = emeicamdropMapping.toDomain(emeicamdropdto);
		emeicamdropService.create(domain);
        EMEICamDropDTO dto = emeicamdropMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeicamdropMapping.toDomain(#emeicamdropdtos),'eam-EMEICamDrop-Create')")
    @ApiOperation(value = "批量新建探头摘除记录", tags = {"探头摘除记录" },  notes = "批量新建探头摘除记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicamdrops/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEICamDropDTO> emeicamdropdtos) {
        emeicamdropService.createBatch(emeicamdropMapping.toDomain(emeicamdropdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeicamdrop" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeicamdropService.get(#emeicamdrop_id),'eam-EMEICamDrop-Update')")
    @ApiOperation(value = "更新探头摘除记录", tags = {"探头摘除记录" },  notes = "更新探头摘除记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeicamdrops/{emeicamdrop_id}")
    public ResponseEntity<EMEICamDropDTO> update(@PathVariable("emeicamdrop_id") String emeicamdrop_id, @RequestBody EMEICamDropDTO emeicamdropdto) {
		EMEICamDrop domain  = emeicamdropMapping.toDomain(emeicamdropdto);
        domain .setEmeicamdropid(emeicamdrop_id);
		emeicamdropService.update(domain );
		EMEICamDropDTO dto = emeicamdropMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeicamdropService.getEmeicamdropByEntities(this.emeicamdropMapping.toDomain(#emeicamdropdtos)),'eam-EMEICamDrop-Update')")
    @ApiOperation(value = "批量更新探头摘除记录", tags = {"探头摘除记录" },  notes = "批量更新探头摘除记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeicamdrops/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEICamDropDTO> emeicamdropdtos) {
        emeicamdropService.updateBatch(emeicamdropMapping.toDomain(emeicamdropdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeicamdropService.get(#emeicamdrop_id),'eam-EMEICamDrop-Remove')")
    @ApiOperation(value = "删除探头摘除记录", tags = {"探头摘除记录" },  notes = "删除探头摘除记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeicamdrops/{emeicamdrop_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeicamdrop_id") String emeicamdrop_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeicamdropService.remove(emeicamdrop_id));
    }

    @PreAuthorize("hasPermission(this.emeicamdropService.getEmeicamdropByIds(#ids),'eam-EMEICamDrop-Remove')")
    @ApiOperation(value = "批量删除探头摘除记录", tags = {"探头摘除记录" },  notes = "批量删除探头摘除记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeicamdrops/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeicamdropService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeicamdropMapping.toDomain(returnObject.body),'eam-EMEICamDrop-Get')")
    @ApiOperation(value = "获取探头摘除记录", tags = {"探头摘除记录" },  notes = "获取探头摘除记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emeicamdrops/{emeicamdrop_id}")
    public ResponseEntity<EMEICamDropDTO> get(@PathVariable("emeicamdrop_id") String emeicamdrop_id) {
        EMEICamDrop domain = emeicamdropService.get(emeicamdrop_id);
        EMEICamDropDTO dto = emeicamdropMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取探头摘除记录草稿", tags = {"探头摘除记录" },  notes = "获取探头摘除记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeicamdrops/getdraft")
    public ResponseEntity<EMEICamDropDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emeicamdropMapping.toDto(emeicamdropService.getDraft(new EMEICamDrop())));
    }

    @ApiOperation(value = "检查探头摘除记录", tags = {"探头摘除记录" },  notes = "检查探头摘除记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicamdrops/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEICamDropDTO emeicamdropdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeicamdropService.checkKey(emeicamdropMapping.toDomain(emeicamdropdto)));
    }

    @PreAuthorize("hasPermission(this.emeicamdropMapping.toDomain(#emeicamdropdto),'eam-EMEICamDrop-Save')")
    @ApiOperation(value = "保存探头摘除记录", tags = {"探头摘除记录" },  notes = "保存探头摘除记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicamdrops/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEICamDropDTO emeicamdropdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emeicamdropService.save(emeicamdropMapping.toDomain(emeicamdropdto)));
    }

    @PreAuthorize("hasPermission(this.emeicamdropMapping.toDomain(#emeicamdropdtos),'eam-EMEICamDrop-Save')")
    @ApiOperation(value = "批量保存探头摘除记录", tags = {"探头摘除记录" },  notes = "批量保存探头摘除记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicamdrops/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEICamDropDTO> emeicamdropdtos) {
        emeicamdropService.saveBatch(emeicamdropMapping.toDomain(emeicamdropdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEICamDrop-searchDefault-all') and hasPermission(#context,'eam-EMEICamDrop-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"探头摘除记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeicamdrops/fetchdefault")
	public ResponseEntity<List<EMEICamDropDTO>> fetchDefault(EMEICamDropSearchContext context) {
        Page<EMEICamDrop> domains = emeicamdropService.searchDefault(context) ;
        List<EMEICamDropDTO> list = emeicamdropMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEICamDrop-searchDefault-all') and hasPermission(#context,'eam-EMEICamDrop-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"探头摘除记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeicamdrops/searchdefault")
	public ResponseEntity<Page<EMEICamDropDTO>> searchDefault(@RequestBody EMEICamDropSearchContext context) {
        Page<EMEICamDrop> domains = emeicamdropService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeicamdropMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

