package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMServiceEvl;
import cn.ibizlab.eam.core.eam_core.service.IEMServiceEvlService;
import cn.ibizlab.eam.core.eam_core.filter.EMServiceEvlSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"服务商评估" })
@RestController("WebApi-emserviceevl")
@RequestMapping("")
public class EMServiceEvlResource {

    @Autowired
    public IEMServiceEvlService emserviceevlService;

    @Autowired
    @Lazy
    public EMServiceEvlMapping emserviceevlMapping;

    @PreAuthorize("hasPermission(this.emserviceevlMapping.toDomain(#emserviceevldto),'eam-EMServiceEvl-Create')")
    @ApiOperation(value = "新建服务商评估", tags = {"服务商评估" },  notes = "新建服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emserviceevls")
    public ResponseEntity<EMServiceEvlDTO> create(@Validated @RequestBody EMServiceEvlDTO emserviceevldto) {
        EMServiceEvl domain = emserviceevlMapping.toDomain(emserviceevldto);
		emserviceevlService.create(domain);
        EMServiceEvlDTO dto = emserviceevlMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emserviceevlMapping.toDomain(#emserviceevldtos),'eam-EMServiceEvl-Create')")
    @ApiOperation(value = "批量新建服务商评估", tags = {"服务商评估" },  notes = "批量新建服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emserviceevls/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMServiceEvlDTO> emserviceevldtos) {
        emserviceevlService.createBatch(emserviceevlMapping.toDomain(emserviceevldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emserviceevl" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emserviceevlService.get(#emserviceevl_id),'eam-EMServiceEvl-Update')")
    @ApiOperation(value = "更新服务商评估", tags = {"服务商评估" },  notes = "更新服务商评估")
	@RequestMapping(method = RequestMethod.PUT, value = "/emserviceevls/{emserviceevl_id}")
    public ResponseEntity<EMServiceEvlDTO> update(@PathVariable("emserviceevl_id") String emserviceevl_id, @RequestBody EMServiceEvlDTO emserviceevldto) {
		EMServiceEvl domain  = emserviceevlMapping.toDomain(emserviceevldto);
        domain .setEmserviceevlid(emserviceevl_id);
		emserviceevlService.update(domain );
		EMServiceEvlDTO dto = emserviceevlMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emserviceevlService.getEmserviceevlByEntities(this.emserviceevlMapping.toDomain(#emserviceevldtos)),'eam-EMServiceEvl-Update')")
    @ApiOperation(value = "批量更新服务商评估", tags = {"服务商评估" },  notes = "批量更新服务商评估")
	@RequestMapping(method = RequestMethod.PUT, value = "/emserviceevls/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMServiceEvlDTO> emserviceevldtos) {
        emserviceevlService.updateBatch(emserviceevlMapping.toDomain(emserviceevldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emserviceevlService.get(#emserviceevl_id),'eam-EMServiceEvl-Remove')")
    @ApiOperation(value = "删除服务商评估", tags = {"服务商评估" },  notes = "删除服务商评估")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emserviceevls/{emserviceevl_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emserviceevl_id") String emserviceevl_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emserviceevlService.remove(emserviceevl_id));
    }

    @PreAuthorize("hasPermission(this.emserviceevlService.getEmserviceevlByIds(#ids),'eam-EMServiceEvl-Remove')")
    @ApiOperation(value = "批量删除服务商评估", tags = {"服务商评估" },  notes = "批量删除服务商评估")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emserviceevls/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emserviceevlService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emserviceevlMapping.toDomain(returnObject.body),'eam-EMServiceEvl-Get')")
    @ApiOperation(value = "获取服务商评估", tags = {"服务商评估" },  notes = "获取服务商评估")
	@RequestMapping(method = RequestMethod.GET, value = "/emserviceevls/{emserviceevl_id}")
    public ResponseEntity<EMServiceEvlDTO> get(@PathVariable("emserviceevl_id") String emserviceevl_id) {
        EMServiceEvl domain = emserviceevlService.get(emserviceevl_id);
        EMServiceEvlDTO dto = emserviceevlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取服务商评估草稿", tags = {"服务商评估" },  notes = "获取服务商评估草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emserviceevls/getdraft")
    public ResponseEntity<EMServiceEvlDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emserviceevlMapping.toDto(emserviceevlService.getDraft(new EMServiceEvl())));
    }

    @ApiOperation(value = "检查服务商评估", tags = {"服务商评估" },  notes = "检查服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emserviceevls/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMServiceEvlDTO emserviceevldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emserviceevlService.checkKey(emserviceevlMapping.toDomain(emserviceevldto)));
    }

    @PreAuthorize("hasPermission(this.emserviceevlMapping.toDomain(#emserviceevldto),'eam-EMServiceEvl-Save')")
    @ApiOperation(value = "保存服务商评估", tags = {"服务商评估" },  notes = "保存服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emserviceevls/save")
    public ResponseEntity<Boolean> save(@RequestBody EMServiceEvlDTO emserviceevldto) {
        return ResponseEntity.status(HttpStatus.OK).body(emserviceevlService.save(emserviceevlMapping.toDomain(emserviceevldto)));
    }

    @PreAuthorize("hasPermission(this.emserviceevlMapping.toDomain(#emserviceevldtos),'eam-EMServiceEvl-Save')")
    @ApiOperation(value = "批量保存服务商评估", tags = {"服务商评估" },  notes = "批量保存服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emserviceevls/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMServiceEvlDTO> emserviceevldtos) {
        emserviceevlService.saveBatch(emserviceevlMapping.toDomain(emserviceevldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchDefault-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"服务商评估" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emserviceevls/fetchdefault")
	public ResponseEntity<List<EMServiceEvlDTO>> fetchDefault(EMServiceEvlSearchContext context) {
        Page<EMServiceEvl> domains = emserviceevlService.searchDefault(context) ;
        List<EMServiceEvlDTO> list = emserviceevlMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchDefault-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"服务商评估" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emserviceevls/searchdefault")
	public ResponseEntity<Page<EMServiceEvlDTO>> searchDefault(@RequestBody EMServiceEvlSearchContext context) {
        Page<EMServiceEvl> domains = emserviceevlService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emserviceevlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "获取评估前五", tags = {"服务商评估" } ,notes = "获取评估前五")
    @RequestMapping(method= RequestMethod.GET , value="/emserviceevls/fetchevaluatetop5")
	public ResponseEntity<List<HashMap>> fetchEvaluateTop5(EMServiceEvlSearchContext context) {
        Page<HashMap> domains = emserviceevlService.searchEvaluateTop5(context) ;
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "查询评估前五", tags = {"服务商评估" } ,notes = "查询评估前五")
    @RequestMapping(method= RequestMethod.POST , value="/emserviceevls/searchevaluatetop5")
	public ResponseEntity<Page<HashMap>> searchEvaluateTop5(@RequestBody EMServiceEvlSearchContext context) {
        Page<HashMap> domains = emserviceevlService.searchEvaluateTop5(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "获取OverallEVL", tags = {"服务商评估" } ,notes = "获取OverallEVL")
    @RequestMapping(method= RequestMethod.GET , value="/emserviceevls/fetchoverallevl")
	public ResponseEntity<List<HashMap>> fetchOverallEVL(EMServiceEvlSearchContext context) {
        Page<HashMap> domains = emserviceevlService.searchOverallEVL(context) ;
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "查询OverallEVL", tags = {"服务商评估" } ,notes = "查询OverallEVL")
    @RequestMapping(method= RequestMethod.POST , value="/emserviceevls/searchoverallevl")
	public ResponseEntity<Page<HashMap>> searchOverallEVL(@RequestBody EMServiceEvlSearchContext context) {
        Page<HashMap> domains = emserviceevlService.searchOverallEVL(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emserviceevlMapping.toDomain(#emserviceevldto),'eam-EMServiceEvl-Create')")
    @ApiOperation(value = "根据服务商建立服务商评估", tags = {"服务商评估" },  notes = "根据服务商建立服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emserviceevls")
    public ResponseEntity<EMServiceEvlDTO> createByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceEvlDTO emserviceevldto) {
        EMServiceEvl domain = emserviceevlMapping.toDomain(emserviceevldto);
        domain.setServiceid(emservice_id);
		emserviceevlService.create(domain);
        EMServiceEvlDTO dto = emserviceevlMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emserviceevlMapping.toDomain(#emserviceevldtos),'eam-EMServiceEvl-Create')")
    @ApiOperation(value = "根据服务商批量建立服务商评估", tags = {"服务商评估" },  notes = "根据服务商批量建立服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emserviceevls/batch")
    public ResponseEntity<Boolean> createBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMServiceEvlDTO> emserviceevldtos) {
        List<EMServiceEvl> domainlist=emserviceevlMapping.toDomain(emserviceevldtos);
        for(EMServiceEvl domain:domainlist){
            domain.setServiceid(emservice_id);
        }
        emserviceevlService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emserviceevl" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emserviceevlService.get(#emserviceevl_id),'eam-EMServiceEvl-Update')")
    @ApiOperation(value = "根据服务商更新服务商评估", tags = {"服务商评估" },  notes = "根据服务商更新服务商评估")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emserviceevls/{emserviceevl_id}")
    public ResponseEntity<EMServiceEvlDTO> updateByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emserviceevl_id") String emserviceevl_id, @RequestBody EMServiceEvlDTO emserviceevldto) {
        EMServiceEvl domain = emserviceevlMapping.toDomain(emserviceevldto);
        domain.setServiceid(emservice_id);
        domain.setEmserviceevlid(emserviceevl_id);
		emserviceevlService.update(domain);
        EMServiceEvlDTO dto = emserviceevlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emserviceevlService.getEmserviceevlByEntities(this.emserviceevlMapping.toDomain(#emserviceevldtos)),'eam-EMServiceEvl-Update')")
    @ApiOperation(value = "根据服务商批量更新服务商评估", tags = {"服务商评估" },  notes = "根据服务商批量更新服务商评估")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emserviceevls/batch")
    public ResponseEntity<Boolean> updateBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMServiceEvlDTO> emserviceevldtos) {
        List<EMServiceEvl> domainlist=emserviceevlMapping.toDomain(emserviceevldtos);
        for(EMServiceEvl domain:domainlist){
            domain.setServiceid(emservice_id);
        }
        emserviceevlService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emserviceevlService.get(#emserviceevl_id),'eam-EMServiceEvl-Remove')")
    @ApiOperation(value = "根据服务商删除服务商评估", tags = {"服务商评估" },  notes = "根据服务商删除服务商评估")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emserviceevls/{emserviceevl_id}")
    public ResponseEntity<Boolean> removeByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emserviceevl_id") String emserviceevl_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emserviceevlService.remove(emserviceevl_id));
    }

    @PreAuthorize("hasPermission(this.emserviceevlService.getEmserviceevlByIds(#ids),'eam-EMServiceEvl-Remove')")
    @ApiOperation(value = "根据服务商批量删除服务商评估", tags = {"服务商评估" },  notes = "根据服务商批量删除服务商评估")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emserviceevls/batch")
    public ResponseEntity<Boolean> removeBatchByEMService(@RequestBody List<String> ids) {
        emserviceevlService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emserviceevlMapping.toDomain(returnObject.body),'eam-EMServiceEvl-Get')")
    @ApiOperation(value = "根据服务商获取服务商评估", tags = {"服务商评估" },  notes = "根据服务商获取服务商评估")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emserviceevls/{emserviceevl_id}")
    public ResponseEntity<EMServiceEvlDTO> getByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emserviceevl_id") String emserviceevl_id) {
        EMServiceEvl domain = emserviceevlService.get(emserviceevl_id);
        EMServiceEvlDTO dto = emserviceevlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商获取服务商评估草稿", tags = {"服务商评估" },  notes = "根据服务商获取服务商评估草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emserviceevls/getdraft")
    public ResponseEntity<EMServiceEvlDTO> getDraftByEMService(@PathVariable("emservice_id") String emservice_id) {
        EMServiceEvl domain = new EMServiceEvl();
        domain.setServiceid(emservice_id);
        return ResponseEntity.status(HttpStatus.OK).body(emserviceevlMapping.toDto(emserviceevlService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商检查服务商评估", tags = {"服务商评估" },  notes = "根据服务商检查服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emserviceevls/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceEvlDTO emserviceevldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emserviceevlService.checkKey(emserviceevlMapping.toDomain(emserviceevldto)));
    }

    @PreAuthorize("hasPermission(this.emserviceevlMapping.toDomain(#emserviceevldto),'eam-EMServiceEvl-Save')")
    @ApiOperation(value = "根据服务商保存服务商评估", tags = {"服务商评估" },  notes = "根据服务商保存服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emserviceevls/save")
    public ResponseEntity<Boolean> saveByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceEvlDTO emserviceevldto) {
        EMServiceEvl domain = emserviceevlMapping.toDomain(emserviceevldto);
        domain.setServiceid(emservice_id);
        return ResponseEntity.status(HttpStatus.OK).body(emserviceevlService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emserviceevlMapping.toDomain(#emserviceevldtos),'eam-EMServiceEvl-Save')")
    @ApiOperation(value = "根据服务商批量保存服务商评估", tags = {"服务商评估" },  notes = "根据服务商批量保存服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emserviceevls/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMServiceEvlDTO> emserviceevldtos) {
        List<EMServiceEvl> domainlist=emserviceevlMapping.toDomain(emserviceevldtos);
        for(EMServiceEvl domain:domainlist){
             domain.setServiceid(emservice_id);
        }
        emserviceevlService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchDefault-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "根据服务商获取DEFAULT", tags = {"服务商评估" } ,notes = "根据服务商获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emserviceevls/fetchdefault")
	public ResponseEntity<List<EMServiceEvlDTO>> fetchEMServiceEvlDefaultByEMService(@PathVariable("emservice_id") String emservice_id,EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<EMServiceEvl> domains = emserviceevlService.searchDefault(context) ;
        List<EMServiceEvlDTO> list = emserviceevlMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchDefault-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "根据服务商查询DEFAULT", tags = {"服务商评估" } ,notes = "根据服务商查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emserviceevls/searchdefault")
	public ResponseEntity<Page<EMServiceEvlDTO>> searchEMServiceEvlDefaultByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<EMServiceEvl> domains = emserviceevlService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emserviceevlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据服务商获取评估前五", tags = {"服务商评估" } ,notes = "根据服务商获取评估前五")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emserviceevls/fetchevaluatetop5")
	public ResponseEntity<List<HashMap>> fetchEMServiceEvlEvaluateTop5ByEMService(@PathVariable("emservice_id") String emservice_id,EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<HashMap> domains = emserviceevlService.searchEvaluateTop5(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据服务商查询评估前五", tags = {"服务商评估" } ,notes = "根据服务商查询评估前五")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emserviceevls/searchevaluatetop5")
	public ResponseEntity<Page<HashMap>> searchEMServiceEvlEvaluateTop5ByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<HashMap> domains = emserviceevlService.searchEvaluateTop5(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据服务商获取OverallEVL", tags = {"服务商评估" } ,notes = "根据服务商获取OverallEVL")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emserviceevls/fetchoverallevl")
	public ResponseEntity<List<HashMap>> fetchEMServiceEvlOverallEVLByEMService(@PathVariable("emservice_id") String emservice_id,EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<HashMap> domains = emserviceevlService.searchOverallEVL(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据服务商查询OverallEVL", tags = {"服务商评估" } ,notes = "根据服务商查询OverallEVL")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emserviceevls/searchoverallevl")
	public ResponseEntity<Page<HashMap>> searchEMServiceEvlOverallEVLByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<HashMap> domains = emserviceevlService.searchOverallEVL(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
}

