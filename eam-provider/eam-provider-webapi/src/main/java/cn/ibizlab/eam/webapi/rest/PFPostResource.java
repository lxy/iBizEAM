package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_pf.domain.PFPost;
import cn.ibizlab.eam.core.eam_pf.service.IPFPostService;
import cn.ibizlab.eam.core.eam_pf.filter.PFPostSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"岗位" })
@RestController("WebApi-pfpost")
@RequestMapping("")
public class PFPostResource {

    @Autowired
    public IPFPostService pfpostService;

    @Autowired
    @Lazy
    public PFPostMapping pfpostMapping;

    @PreAuthorize("hasPermission(this.pfpostMapping.toDomain(#pfpostdto),'eam-PFPost-Create')")
    @ApiOperation(value = "新建岗位", tags = {"岗位" },  notes = "新建岗位")
	@RequestMapping(method = RequestMethod.POST, value = "/pfposts")
    public ResponseEntity<PFPostDTO> create(@Validated @RequestBody PFPostDTO pfpostdto) {
        PFPost domain = pfpostMapping.toDomain(pfpostdto);
		pfpostService.create(domain);
        PFPostDTO dto = pfpostMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pfpostMapping.toDomain(#pfpostdtos),'eam-PFPost-Create')")
    @ApiOperation(value = "批量新建岗位", tags = {"岗位" },  notes = "批量新建岗位")
	@RequestMapping(method = RequestMethod.POST, value = "/pfposts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<PFPostDTO> pfpostdtos) {
        pfpostService.createBatch(pfpostMapping.toDomain(pfpostdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "pfpost" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.pfpostService.get(#pfpost_id),'eam-PFPost-Update')")
    @ApiOperation(value = "更新岗位", tags = {"岗位" },  notes = "更新岗位")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfposts/{pfpost_id}")
    public ResponseEntity<PFPostDTO> update(@PathVariable("pfpost_id") String pfpost_id, @RequestBody PFPostDTO pfpostdto) {
		PFPost domain  = pfpostMapping.toDomain(pfpostdto);
        domain .setPfpostid(pfpost_id);
		pfpostService.update(domain );
		PFPostDTO dto = pfpostMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pfpostService.getPfpostByEntities(this.pfpostMapping.toDomain(#pfpostdtos)),'eam-PFPost-Update')")
    @ApiOperation(value = "批量更新岗位", tags = {"岗位" },  notes = "批量更新岗位")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfposts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<PFPostDTO> pfpostdtos) {
        pfpostService.updateBatch(pfpostMapping.toDomain(pfpostdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.pfpostService.get(#pfpost_id),'eam-PFPost-Remove')")
    @ApiOperation(value = "删除岗位", tags = {"岗位" },  notes = "删除岗位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfposts/{pfpost_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("pfpost_id") String pfpost_id) {
         return ResponseEntity.status(HttpStatus.OK).body(pfpostService.remove(pfpost_id));
    }

    @PreAuthorize("hasPermission(this.pfpostService.getPfpostByIds(#ids),'eam-PFPost-Remove')")
    @ApiOperation(value = "批量删除岗位", tags = {"岗位" },  notes = "批量删除岗位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfposts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        pfpostService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.pfpostMapping.toDomain(returnObject.body),'eam-PFPost-Get')")
    @ApiOperation(value = "获取岗位", tags = {"岗位" },  notes = "获取岗位")
	@RequestMapping(method = RequestMethod.GET, value = "/pfposts/{pfpost_id}")
    public ResponseEntity<PFPostDTO> get(@PathVariable("pfpost_id") String pfpost_id) {
        PFPost domain = pfpostService.get(pfpost_id);
        PFPostDTO dto = pfpostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取岗位草稿", tags = {"岗位" },  notes = "获取岗位草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/pfposts/getdraft")
    public ResponseEntity<PFPostDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(pfpostMapping.toDto(pfpostService.getDraft(new PFPost())));
    }

    @ApiOperation(value = "检查岗位", tags = {"岗位" },  notes = "检查岗位")
	@RequestMapping(method = RequestMethod.POST, value = "/pfposts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody PFPostDTO pfpostdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(pfpostService.checkKey(pfpostMapping.toDomain(pfpostdto)));
    }

    @PreAuthorize("hasPermission(this.pfpostMapping.toDomain(#pfpostdto),'eam-PFPost-Save')")
    @ApiOperation(value = "保存岗位", tags = {"岗位" },  notes = "保存岗位")
	@RequestMapping(method = RequestMethod.POST, value = "/pfposts/save")
    public ResponseEntity<Boolean> save(@RequestBody PFPostDTO pfpostdto) {
        return ResponseEntity.status(HttpStatus.OK).body(pfpostService.save(pfpostMapping.toDomain(pfpostdto)));
    }

    @PreAuthorize("hasPermission(this.pfpostMapping.toDomain(#pfpostdtos),'eam-PFPost-Save')")
    @ApiOperation(value = "批量保存岗位", tags = {"岗位" },  notes = "批量保存岗位")
	@RequestMapping(method = RequestMethod.POST, value = "/pfposts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<PFPostDTO> pfpostdtos) {
        pfpostService.saveBatch(pfpostMapping.toDomain(pfpostdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFPost-searchDefault-all') and hasPermission(#context,'eam-PFPost-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"岗位" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfposts/fetchdefault")
	public ResponseEntity<List<PFPostDTO>> fetchDefault(PFPostSearchContext context) {
        Page<PFPost> domains = pfpostService.searchDefault(context) ;
        List<PFPostDTO> list = pfpostMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFPost-searchDefault-all') and hasPermission(#context,'eam-PFPost-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"岗位" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfposts/searchdefault")
	public ResponseEntity<Page<PFPostDTO>> searchDefault(@RequestBody PFPostSearchContext context) {
        Page<PFPost> domains = pfpostService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(pfpostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

