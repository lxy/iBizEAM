package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSpareMap;
import cn.ibizlab.eam.core.eam_core.service.IEMEQSpareMapService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQSpareMapSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"备件包引用" })
@RestController("WebApi-emeqsparemap")
@RequestMapping("")
public class EMEQSpareMapResource {

    @Autowired
    public IEMEQSpareMapService emeqsparemapService;

    @Autowired
    @Lazy
    public EMEQSpareMapMapping emeqsparemapMapping;

    @PreAuthorize("hasPermission(this.emeqsparemapMapping.toDomain(#emeqsparemapdto),'eam-EMEQSpareMap-Create')")
    @ApiOperation(value = "新建备件包引用", tags = {"备件包引用" },  notes = "新建备件包引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqsparemaps")
    public ResponseEntity<EMEQSpareMapDTO> create(@Validated @RequestBody EMEQSpareMapDTO emeqsparemapdto) {
        EMEQSpareMap domain = emeqsparemapMapping.toDomain(emeqsparemapdto);
		emeqsparemapService.create(domain);
        EMEQSpareMapDTO dto = emeqsparemapMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparemapMapping.toDomain(#emeqsparemapdtos),'eam-EMEQSpareMap-Create')")
    @ApiOperation(value = "批量新建备件包引用", tags = {"备件包引用" },  notes = "批量新建备件包引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqsparemaps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQSpareMapDTO> emeqsparemapdtos) {
        emeqsparemapService.createBatch(emeqsparemapMapping.toDomain(emeqsparemapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqsparemap" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqsparemapService.get(#emeqsparemap_id),'eam-EMEQSpareMap-Update')")
    @ApiOperation(value = "更新备件包引用", tags = {"备件包引用" },  notes = "更新备件包引用")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqsparemaps/{emeqsparemap_id}")
    public ResponseEntity<EMEQSpareMapDTO> update(@PathVariable("emeqsparemap_id") String emeqsparemap_id, @RequestBody EMEQSpareMapDTO emeqsparemapdto) {
		EMEQSpareMap domain  = emeqsparemapMapping.toDomain(emeqsparemapdto);
        domain .setEmeqsparemapid(emeqsparemap_id);
		emeqsparemapService.update(domain );
		EMEQSpareMapDTO dto = emeqsparemapMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparemapService.getEmeqsparemapByEntities(this.emeqsparemapMapping.toDomain(#emeqsparemapdtos)),'eam-EMEQSpareMap-Update')")
    @ApiOperation(value = "批量更新备件包引用", tags = {"备件包引用" },  notes = "批量更新备件包引用")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqsparemaps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQSpareMapDTO> emeqsparemapdtos) {
        emeqsparemapService.updateBatch(emeqsparemapMapping.toDomain(emeqsparemapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqsparemapService.get(#emeqsparemap_id),'eam-EMEQSpareMap-Remove')")
    @ApiOperation(value = "删除备件包引用", tags = {"备件包引用" },  notes = "删除备件包引用")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqsparemaps/{emeqsparemap_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqsparemap_id") String emeqsparemap_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqsparemapService.remove(emeqsparemap_id));
    }

    @PreAuthorize("hasPermission(this.emeqsparemapService.getEmeqsparemapByIds(#ids),'eam-EMEQSpareMap-Remove')")
    @ApiOperation(value = "批量删除备件包引用", tags = {"备件包引用" },  notes = "批量删除备件包引用")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqsparemaps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqsparemapService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqsparemapMapping.toDomain(returnObject.body),'eam-EMEQSpareMap-Get')")
    @ApiOperation(value = "获取备件包引用", tags = {"备件包引用" },  notes = "获取备件包引用")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqsparemaps/{emeqsparemap_id}")
    public ResponseEntity<EMEQSpareMapDTO> get(@PathVariable("emeqsparemap_id") String emeqsparemap_id) {
        EMEQSpareMap domain = emeqsparemapService.get(emeqsparemap_id);
        EMEQSpareMapDTO dto = emeqsparemapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取备件包引用草稿", tags = {"备件包引用" },  notes = "获取备件包引用草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqsparemaps/getdraft")
    public ResponseEntity<EMEQSpareMapDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparemapMapping.toDto(emeqsparemapService.getDraft(new EMEQSpareMap())));
    }

    @ApiOperation(value = "检查备件包引用", tags = {"备件包引用" },  notes = "检查备件包引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqsparemaps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQSpareMapDTO emeqsparemapdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqsparemapService.checkKey(emeqsparemapMapping.toDomain(emeqsparemapdto)));
    }

    @PreAuthorize("hasPermission(this.emeqsparemapMapping.toDomain(#emeqsparemapdto),'eam-EMEQSpareMap-Save')")
    @ApiOperation(value = "保存备件包引用", tags = {"备件包引用" },  notes = "保存备件包引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqsparemaps/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEQSpareMapDTO emeqsparemapdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparemapService.save(emeqsparemapMapping.toDomain(emeqsparemapdto)));
    }

    @PreAuthorize("hasPermission(this.emeqsparemapMapping.toDomain(#emeqsparemapdtos),'eam-EMEQSpareMap-Save')")
    @ApiOperation(value = "批量保存备件包引用", tags = {"备件包引用" },  notes = "批量保存备件包引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqsparemaps/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQSpareMapDTO> emeqsparemapdtos) {
        emeqsparemapService.saveBatch(emeqsparemapMapping.toDomain(emeqsparemapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareMap-searchByEQ-all') and hasPermission(#context,'eam-EMEQSpareMap-Get')")
	@ApiOperation(value = "获取ByEQ", tags = {"备件包引用" } ,notes = "获取ByEQ")
    @RequestMapping(method= RequestMethod.GET , value="/emeqsparemaps/fetchbyeq")
	public ResponseEntity<List<EMEQSpareMapDTO>> fetchByEQ(EMEQSpareMapSearchContext context) {
        Page<EMEQSpareMap> domains = emeqsparemapService.searchByEQ(context) ;
        List<EMEQSpareMapDTO> list = emeqsparemapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareMap-searchByEQ-all') and hasPermission(#context,'eam-EMEQSpareMap-Get')")
	@ApiOperation(value = "查询ByEQ", tags = {"备件包引用" } ,notes = "查询ByEQ")
    @RequestMapping(method= RequestMethod.POST , value="/emeqsparemaps/searchbyeq")
	public ResponseEntity<Page<EMEQSpareMapDTO>> searchByEQ(@RequestBody EMEQSpareMapSearchContext context) {
        Page<EMEQSpareMap> domains = emeqsparemapService.searchByEQ(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqsparemapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareMap-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareMap-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"备件包引用" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqsparemaps/fetchdefault")
	public ResponseEntity<List<EMEQSpareMapDTO>> fetchDefault(EMEQSpareMapSearchContext context) {
        Page<EMEQSpareMap> domains = emeqsparemapService.searchDefault(context) ;
        List<EMEQSpareMapDTO> list = emeqsparemapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareMap-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareMap-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"备件包引用" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqsparemaps/searchdefault")
	public ResponseEntity<Page<EMEQSpareMapDTO>> searchDefault(@RequestBody EMEQSpareMapSearchContext context) {
        Page<EMEQSpareMap> domains = emeqsparemapService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqsparemapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emeqsparemapMapping.toDomain(#emeqsparemapdto),'eam-EMEQSpareMap-Create')")
    @ApiOperation(value = "根据备件包建立备件包引用", tags = {"备件包引用" },  notes = "根据备件包建立备件包引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqspares/{emeqspare_id}/emeqsparemaps")
    public ResponseEntity<EMEQSpareMapDTO> createByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @RequestBody EMEQSpareMapDTO emeqsparemapdto) {
        EMEQSpareMap domain = emeqsparemapMapping.toDomain(emeqsparemapdto);
        domain.setEqspareid(emeqspare_id);
		emeqsparemapService.create(domain);
        EMEQSpareMapDTO dto = emeqsparemapMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparemapMapping.toDomain(#emeqsparemapdtos),'eam-EMEQSpareMap-Create')")
    @ApiOperation(value = "根据备件包批量建立备件包引用", tags = {"备件包引用" },  notes = "根据备件包批量建立备件包引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqspares/{emeqspare_id}/emeqsparemaps/batch")
    public ResponseEntity<Boolean> createBatchByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @RequestBody List<EMEQSpareMapDTO> emeqsparemapdtos) {
        List<EMEQSpareMap> domainlist=emeqsparemapMapping.toDomain(emeqsparemapdtos);
        for(EMEQSpareMap domain:domainlist){
            domain.setEqspareid(emeqspare_id);
        }
        emeqsparemapService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqsparemap" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqsparemapService.get(#emeqsparemap_id),'eam-EMEQSpareMap-Update')")
    @ApiOperation(value = "根据备件包更新备件包引用", tags = {"备件包引用" },  notes = "根据备件包更新备件包引用")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqspares/{emeqspare_id}/emeqsparemaps/{emeqsparemap_id}")
    public ResponseEntity<EMEQSpareMapDTO> updateByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @PathVariable("emeqsparemap_id") String emeqsparemap_id, @RequestBody EMEQSpareMapDTO emeqsparemapdto) {
        EMEQSpareMap domain = emeqsparemapMapping.toDomain(emeqsparemapdto);
        domain.setEqspareid(emeqspare_id);
        domain.setEmeqsparemapid(emeqsparemap_id);
		emeqsparemapService.update(domain);
        EMEQSpareMapDTO dto = emeqsparemapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsparemapService.getEmeqsparemapByEntities(this.emeqsparemapMapping.toDomain(#emeqsparemapdtos)),'eam-EMEQSpareMap-Update')")
    @ApiOperation(value = "根据备件包批量更新备件包引用", tags = {"备件包引用" },  notes = "根据备件包批量更新备件包引用")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqspares/{emeqspare_id}/emeqsparemaps/batch")
    public ResponseEntity<Boolean> updateBatchByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @RequestBody List<EMEQSpareMapDTO> emeqsparemapdtos) {
        List<EMEQSpareMap> domainlist=emeqsparemapMapping.toDomain(emeqsparemapdtos);
        for(EMEQSpareMap domain:domainlist){
            domain.setEqspareid(emeqspare_id);
        }
        emeqsparemapService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqsparemapService.get(#emeqsparemap_id),'eam-EMEQSpareMap-Remove')")
    @ApiOperation(value = "根据备件包删除备件包引用", tags = {"备件包引用" },  notes = "根据备件包删除备件包引用")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqspares/{emeqspare_id}/emeqsparemaps/{emeqsparemap_id}")
    public ResponseEntity<Boolean> removeByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @PathVariable("emeqsparemap_id") String emeqsparemap_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqsparemapService.remove(emeqsparemap_id));
    }

    @PreAuthorize("hasPermission(this.emeqsparemapService.getEmeqsparemapByIds(#ids),'eam-EMEQSpareMap-Remove')")
    @ApiOperation(value = "根据备件包批量删除备件包引用", tags = {"备件包引用" },  notes = "根据备件包批量删除备件包引用")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqspares/{emeqspare_id}/emeqsparemaps/batch")
    public ResponseEntity<Boolean> removeBatchByEMEQSpare(@RequestBody List<String> ids) {
        emeqsparemapService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqsparemapMapping.toDomain(returnObject.body),'eam-EMEQSpareMap-Get')")
    @ApiOperation(value = "根据备件包获取备件包引用", tags = {"备件包引用" },  notes = "根据备件包获取备件包引用")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqspares/{emeqspare_id}/emeqsparemaps/{emeqsparemap_id}")
    public ResponseEntity<EMEQSpareMapDTO> getByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @PathVariable("emeqsparemap_id") String emeqsparemap_id) {
        EMEQSpareMap domain = emeqsparemapService.get(emeqsparemap_id);
        EMEQSpareMapDTO dto = emeqsparemapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据备件包获取备件包引用草稿", tags = {"备件包引用" },  notes = "根据备件包获取备件包引用草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emeqspares/{emeqspare_id}/emeqsparemaps/getdraft")
    public ResponseEntity<EMEQSpareMapDTO> getDraftByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id) {
        EMEQSpareMap domain = new EMEQSpareMap();
        domain.setEqspareid(emeqspare_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparemapMapping.toDto(emeqsparemapService.getDraft(domain)));
    }

    @ApiOperation(value = "根据备件包检查备件包引用", tags = {"备件包引用" },  notes = "根据备件包检查备件包引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqspares/{emeqspare_id}/emeqsparemaps/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @RequestBody EMEQSpareMapDTO emeqsparemapdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqsparemapService.checkKey(emeqsparemapMapping.toDomain(emeqsparemapdto)));
    }

    @PreAuthorize("hasPermission(this.emeqsparemapMapping.toDomain(#emeqsparemapdto),'eam-EMEQSpareMap-Save')")
    @ApiOperation(value = "根据备件包保存备件包引用", tags = {"备件包引用" },  notes = "根据备件包保存备件包引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqspares/{emeqspare_id}/emeqsparemaps/save")
    public ResponseEntity<Boolean> saveByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @RequestBody EMEQSpareMapDTO emeqsparemapdto) {
        EMEQSpareMap domain = emeqsparemapMapping.toDomain(emeqsparemapdto);
        domain.setEqspareid(emeqspare_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsparemapService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emeqsparemapMapping.toDomain(#emeqsparemapdtos),'eam-EMEQSpareMap-Save')")
    @ApiOperation(value = "根据备件包批量保存备件包引用", tags = {"备件包引用" },  notes = "根据备件包批量保存备件包引用")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqspares/{emeqspare_id}/emeqsparemaps/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @RequestBody List<EMEQSpareMapDTO> emeqsparemapdtos) {
        List<EMEQSpareMap> domainlist=emeqsparemapMapping.toDomain(emeqsparemapdtos);
        for(EMEQSpareMap domain:domainlist){
             domain.setEqspareid(emeqspare_id);
        }
        emeqsparemapService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareMap-searchByEQ-all') and hasPermission(#context,'eam-EMEQSpareMap-Get')")
	@ApiOperation(value = "根据备件包获取ByEQ", tags = {"备件包引用" } ,notes = "根据备件包获取ByEQ")
    @RequestMapping(method= RequestMethod.GET , value="/emeqspares/{emeqspare_id}/emeqsparemaps/fetchbyeq")
	public ResponseEntity<List<EMEQSpareMapDTO>> fetchEMEQSpareMapByEQByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id,EMEQSpareMapSearchContext context) {
        context.setN_eqspareid_eq(emeqspare_id);
        Page<EMEQSpareMap> domains = emeqsparemapService.searchByEQ(context) ;
        List<EMEQSpareMapDTO> list = emeqsparemapMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareMap-searchByEQ-all') and hasPermission(#context,'eam-EMEQSpareMap-Get')")
	@ApiOperation(value = "根据备件包查询ByEQ", tags = {"备件包引用" } ,notes = "根据备件包查询ByEQ")
    @RequestMapping(method= RequestMethod.POST , value="/emeqspares/{emeqspare_id}/emeqsparemaps/searchbyeq")
	public ResponseEntity<Page<EMEQSpareMapDTO>> searchEMEQSpareMapByEQByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @RequestBody EMEQSpareMapSearchContext context) {
        context.setN_eqspareid_eq(emeqspare_id);
        Page<EMEQSpareMap> domains = emeqsparemapService.searchByEQ(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqsparemapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareMap-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareMap-Get')")
	@ApiOperation(value = "根据备件包获取DEFAULT", tags = {"备件包引用" } ,notes = "根据备件包获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqspares/{emeqspare_id}/emeqsparemaps/fetchdefault")
	public ResponseEntity<List<EMEQSpareMapDTO>> fetchEMEQSpareMapDefaultByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id,EMEQSpareMapSearchContext context) {
        context.setN_eqspareid_eq(emeqspare_id);
        Page<EMEQSpareMap> domains = emeqsparemapService.searchDefault(context) ;
        List<EMEQSpareMapDTO> list = emeqsparemapMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpareMap-searchDefault-all') and hasPermission(#context,'eam-EMEQSpareMap-Get')")
	@ApiOperation(value = "根据备件包查询DEFAULT", tags = {"备件包引用" } ,notes = "根据备件包查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqspares/{emeqspare_id}/emeqsparemaps/searchdefault")
	public ResponseEntity<Page<EMEQSpareMapDTO>> searchEMEQSpareMapDefaultByEMEQSpare(@PathVariable("emeqspare_id") String emeqspare_id, @RequestBody EMEQSpareMapSearchContext context) {
        context.setN_eqspareid_eq(emeqspare_id);
        Page<EMEQSpareMap> domains = emeqsparemapService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqsparemapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

