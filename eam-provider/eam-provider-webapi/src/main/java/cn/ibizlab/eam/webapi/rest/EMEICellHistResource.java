package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEICellHist;
import cn.ibizlab.eam.core.eam_core.service.IEMEICellHistService;
import cn.ibizlab.eam.core.eam_core.filter.EMEICellHistSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"对讲机检修记录" })
@RestController("WebApi-emeicellhist")
@RequestMapping("")
public class EMEICellHistResource {

    @Autowired
    public IEMEICellHistService emeicellhistService;

    @Autowired
    @Lazy
    public EMEICellHistMapping emeicellhistMapping;

    @PreAuthorize("hasPermission(this.emeicellhistMapping.toDomain(#emeicellhistdto),'eam-EMEICellHist-Create')")
    @ApiOperation(value = "新建对讲机检修记录", tags = {"对讲机检修记录" },  notes = "新建对讲机检修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicellhists")
    public ResponseEntity<EMEICellHistDTO> create(@Validated @RequestBody EMEICellHistDTO emeicellhistdto) {
        EMEICellHist domain = emeicellhistMapping.toDomain(emeicellhistdto);
		emeicellhistService.create(domain);
        EMEICellHistDTO dto = emeicellhistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeicellhistMapping.toDomain(#emeicellhistdtos),'eam-EMEICellHist-Create')")
    @ApiOperation(value = "批量新建对讲机检修记录", tags = {"对讲机检修记录" },  notes = "批量新建对讲机检修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicellhists/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEICellHistDTO> emeicellhistdtos) {
        emeicellhistService.createBatch(emeicellhistMapping.toDomain(emeicellhistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeicellhist" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeicellhistService.get(#emeicellhist_id),'eam-EMEICellHist-Update')")
    @ApiOperation(value = "更新对讲机检修记录", tags = {"对讲机检修记录" },  notes = "更新对讲机检修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeicellhists/{emeicellhist_id}")
    public ResponseEntity<EMEICellHistDTO> update(@PathVariable("emeicellhist_id") String emeicellhist_id, @RequestBody EMEICellHistDTO emeicellhistdto) {
		EMEICellHist domain  = emeicellhistMapping.toDomain(emeicellhistdto);
        domain .setEmeicellhistid(emeicellhist_id);
		emeicellhistService.update(domain );
		EMEICellHistDTO dto = emeicellhistMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeicellhistService.getEmeicellhistByEntities(this.emeicellhistMapping.toDomain(#emeicellhistdtos)),'eam-EMEICellHist-Update')")
    @ApiOperation(value = "批量更新对讲机检修记录", tags = {"对讲机检修记录" },  notes = "批量更新对讲机检修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeicellhists/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEICellHistDTO> emeicellhistdtos) {
        emeicellhistService.updateBatch(emeicellhistMapping.toDomain(emeicellhistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeicellhistService.get(#emeicellhist_id),'eam-EMEICellHist-Remove')")
    @ApiOperation(value = "删除对讲机检修记录", tags = {"对讲机检修记录" },  notes = "删除对讲机检修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeicellhists/{emeicellhist_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeicellhist_id") String emeicellhist_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeicellhistService.remove(emeicellhist_id));
    }

    @PreAuthorize("hasPermission(this.emeicellhistService.getEmeicellhistByIds(#ids),'eam-EMEICellHist-Remove')")
    @ApiOperation(value = "批量删除对讲机检修记录", tags = {"对讲机检修记录" },  notes = "批量删除对讲机检修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeicellhists/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeicellhistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeicellhistMapping.toDomain(returnObject.body),'eam-EMEICellHist-Get')")
    @ApiOperation(value = "获取对讲机检修记录", tags = {"对讲机检修记录" },  notes = "获取对讲机检修记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emeicellhists/{emeicellhist_id}")
    public ResponseEntity<EMEICellHistDTO> get(@PathVariable("emeicellhist_id") String emeicellhist_id) {
        EMEICellHist domain = emeicellhistService.get(emeicellhist_id);
        EMEICellHistDTO dto = emeicellhistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取对讲机检修记录草稿", tags = {"对讲机检修记录" },  notes = "获取对讲机检修记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeicellhists/getdraft")
    public ResponseEntity<EMEICellHistDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emeicellhistMapping.toDto(emeicellhistService.getDraft(new EMEICellHist())));
    }

    @ApiOperation(value = "检查对讲机检修记录", tags = {"对讲机检修记录" },  notes = "检查对讲机检修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicellhists/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEICellHistDTO emeicellhistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeicellhistService.checkKey(emeicellhistMapping.toDomain(emeicellhistdto)));
    }

    @PreAuthorize("hasPermission(this.emeicellhistMapping.toDomain(#emeicellhistdto),'eam-EMEICellHist-Save')")
    @ApiOperation(value = "保存对讲机检修记录", tags = {"对讲机检修记录" },  notes = "保存对讲机检修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicellhists/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEICellHistDTO emeicellhistdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emeicellhistService.save(emeicellhistMapping.toDomain(emeicellhistdto)));
    }

    @PreAuthorize("hasPermission(this.emeicellhistMapping.toDomain(#emeicellhistdtos),'eam-EMEICellHist-Save')")
    @ApiOperation(value = "批量保存对讲机检修记录", tags = {"对讲机检修记录" },  notes = "批量保存对讲机检修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicellhists/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEICellHistDTO> emeicellhistdtos) {
        emeicellhistService.saveBatch(emeicellhistMapping.toDomain(emeicellhistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEICellHist-searchDefault-all') and hasPermission(#context,'eam-EMEICellHist-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"对讲机检修记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeicellhists/fetchdefault")
	public ResponseEntity<List<EMEICellHistDTO>> fetchDefault(EMEICellHistSearchContext context) {
        Page<EMEICellHist> domains = emeicellhistService.searchDefault(context) ;
        List<EMEICellHistDTO> list = emeicellhistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEICellHist-searchDefault-all') and hasPermission(#context,'eam-EMEICellHist-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"对讲机检修记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeicellhists/searchdefault")
	public ResponseEntity<Page<EMEICellHistDTO>> searchDefault(@RequestBody EMEICellHistSearchContext context) {
        Page<EMEICellHist> domains = emeicellhistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeicellhistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

