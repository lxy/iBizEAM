package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMPlanTempl;
import cn.ibizlab.eam.core.eam_core.service.IEMPlanTemplService;
import cn.ibizlab.eam.core.eam_core.filter.EMPlanTemplSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"计划模板" })
@RestController("WebApi-emplantempl")
@RequestMapping("")
public class EMPlanTemplResource {

    @Autowired
    public IEMPlanTemplService emplantemplService;

    @Autowired
    @Lazy
    public EMPlanTemplMapping emplantemplMapping;

    @PreAuthorize("hasPermission(this.emplantemplMapping.toDomain(#emplantempldto),'eam-EMPlanTempl-Create')")
    @ApiOperation(value = "新建计划模板", tags = {"计划模板" },  notes = "新建计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls")
    public ResponseEntity<EMPlanTemplDTO> create(@Validated @RequestBody EMPlanTemplDTO emplantempldto) {
        EMPlanTempl domain = emplantemplMapping.toDomain(emplantempldto);
		emplantemplService.create(domain);
        EMPlanTemplDTO dto = emplantemplMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplantemplMapping.toDomain(#emplantempldtos),'eam-EMPlanTempl-Create')")
    @ApiOperation(value = "批量新建计划模板", tags = {"计划模板" },  notes = "批量新建计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMPlanTemplDTO> emplantempldtos) {
        emplantemplService.createBatch(emplantemplMapping.toDomain(emplantempldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplantempl" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplantemplService.get(#emplantempl_id),'eam-EMPlanTempl-Update')")
    @ApiOperation(value = "更新计划模板", tags = {"计划模板" },  notes = "更新计划模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplantempls/{emplantempl_id}")
    public ResponseEntity<EMPlanTemplDTO> update(@PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanTemplDTO emplantempldto) {
		EMPlanTempl domain  = emplantemplMapping.toDomain(emplantempldto);
        domain .setEmplantemplid(emplantempl_id);
		emplantemplService.update(domain );
		EMPlanTemplDTO dto = emplantemplMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplantemplService.getEmplantemplByEntities(this.emplantemplMapping.toDomain(#emplantempldtos)),'eam-EMPlanTempl-Update')")
    @ApiOperation(value = "批量更新计划模板", tags = {"计划模板" },  notes = "批量更新计划模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/emplantempls/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMPlanTemplDTO> emplantempldtos) {
        emplantemplService.updateBatch(emplantemplMapping.toDomain(emplantempldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplantemplService.get(#emplantempl_id),'eam-EMPlanTempl-Remove')")
    @ApiOperation(value = "删除计划模板", tags = {"计划模板" },  notes = "删除计划模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplantempls/{emplantempl_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emplantempl_id") String emplantempl_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emplantemplService.remove(emplantempl_id));
    }

    @PreAuthorize("hasPermission(this.emplantemplService.getEmplantemplByIds(#ids),'eam-EMPlanTempl-Remove')")
    @ApiOperation(value = "批量删除计划模板", tags = {"计划模板" },  notes = "批量删除计划模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emplantempls/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emplantemplService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplantemplMapping.toDomain(returnObject.body),'eam-EMPlanTempl-Get')")
    @ApiOperation(value = "获取计划模板", tags = {"计划模板" },  notes = "获取计划模板")
	@RequestMapping(method = RequestMethod.GET, value = "/emplantempls/{emplantempl_id}")
    public ResponseEntity<EMPlanTemplDTO> get(@PathVariable("emplantempl_id") String emplantempl_id) {
        EMPlanTempl domain = emplantemplService.get(emplantempl_id);
        EMPlanTemplDTO dto = emplantemplMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取计划模板草稿", tags = {"计划模板" },  notes = "获取计划模板草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emplantempls/getdraft")
    public ResponseEntity<EMPlanTemplDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emplantemplMapping.toDto(emplantemplService.getDraft(new EMPlanTempl())));
    }

    @ApiOperation(value = "检查计划模板", tags = {"计划模板" },  notes = "检查计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMPlanTemplDTO emplantempldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplantemplService.checkKey(emplantemplMapping.toDomain(emplantempldto)));
    }

    @PreAuthorize("hasPermission(this.emplantemplMapping.toDomain(#emplantempldto),'eam-EMPlanTempl-Save')")
    @ApiOperation(value = "保存计划模板", tags = {"计划模板" },  notes = "保存计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/save")
    public ResponseEntity<Boolean> save(@RequestBody EMPlanTemplDTO emplantempldto) {
        return ResponseEntity.status(HttpStatus.OK).body(emplantemplService.save(emplantemplMapping.toDomain(emplantempldto)));
    }

    @PreAuthorize("hasPermission(this.emplantemplMapping.toDomain(#emplantempldtos),'eam-EMPlanTempl-Save')")
    @ApiOperation(value = "批量保存计划模板", tags = {"计划模板" },  notes = "批量保存计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/emplantempls/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMPlanTemplDTO> emplantempldtos) {
        emplantemplService.saveBatch(emplantemplMapping.toDomain(emplantempldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanTempl-searchDefault-all') and hasPermission(#context,'eam-EMPlanTempl-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"计划模板" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emplantempls/fetchdefault")
	public ResponseEntity<List<EMPlanTemplDTO>> fetchDefault(EMPlanTemplSearchContext context) {
        Page<EMPlanTempl> domains = emplantemplService.searchDefault(context) ;
        List<EMPlanTemplDTO> list = emplantemplMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanTempl-searchDefault-all') and hasPermission(#context,'eam-EMPlanTempl-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"计划模板" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emplantempls/searchdefault")
	public ResponseEntity<Page<EMPlanTemplDTO>> searchDefault(@RequestBody EMPlanTemplSearchContext context) {
        Page<EMPlanTempl> domains = emplantemplService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplantemplMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplantemplMapping.toDomain(#emplantempldto),'eam-EMPlanTempl-Create')")
    @ApiOperation(value = "根据总帐科目建立计划模板", tags = {"计划模板" },  notes = "根据总帐科目建立计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls")
    public ResponseEntity<EMPlanTemplDTO> createByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @RequestBody EMPlanTemplDTO emplantempldto) {
        EMPlanTempl domain = emplantemplMapping.toDomain(emplantempldto);
        domain.setAcclassid(emacclass_id);
		emplantemplService.create(domain);
        EMPlanTemplDTO dto = emplantemplMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplantemplMapping.toDomain(#emplantempldtos),'eam-EMPlanTempl-Create')")
    @ApiOperation(value = "根据总帐科目批量建立计划模板", tags = {"计划模板" },  notes = "根据总帐科目批量建立计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/batch")
    public ResponseEntity<Boolean> createBatchByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @RequestBody List<EMPlanTemplDTO> emplantempldtos) {
        List<EMPlanTempl> domainlist=emplantemplMapping.toDomain(emplantempldtos);
        for(EMPlanTempl domain:domainlist){
            domain.setAcclassid(emacclass_id);
        }
        emplantemplService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplantempl" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplantemplService.get(#emplantempl_id),'eam-EMPlanTempl-Update')")
    @ApiOperation(value = "根据总帐科目更新计划模板", tags = {"计划模板" },  notes = "根据总帐科目更新计划模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}")
    public ResponseEntity<EMPlanTemplDTO> updateByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanTemplDTO emplantempldto) {
        EMPlanTempl domain = emplantemplMapping.toDomain(emplantempldto);
        domain.setAcclassid(emacclass_id);
        domain.setEmplantemplid(emplantempl_id);
		emplantemplService.update(domain);
        EMPlanTemplDTO dto = emplantemplMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplantemplService.getEmplantemplByEntities(this.emplantemplMapping.toDomain(#emplantempldtos)),'eam-EMPlanTempl-Update')")
    @ApiOperation(value = "根据总帐科目批量更新计划模板", tags = {"计划模板" },  notes = "根据总帐科目批量更新计划模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/emacclasses/{emacclass_id}/emplantempls/batch")
    public ResponseEntity<Boolean> updateBatchByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @RequestBody List<EMPlanTemplDTO> emplantempldtos) {
        List<EMPlanTempl> domainlist=emplantemplMapping.toDomain(emplantempldtos);
        for(EMPlanTempl domain:domainlist){
            domain.setAcclassid(emacclass_id);
        }
        emplantemplService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplantemplService.get(#emplantempl_id),'eam-EMPlanTempl-Remove')")
    @ApiOperation(value = "根据总帐科目删除计划模板", tags = {"计划模板" },  notes = "根据总帐科目删除计划模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}")
    public ResponseEntity<Boolean> removeByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplantemplService.remove(emplantempl_id));
    }

    @PreAuthorize("hasPermission(this.emplantemplService.getEmplantemplByIds(#ids),'eam-EMPlanTempl-Remove')")
    @ApiOperation(value = "根据总帐科目批量删除计划模板", tags = {"计划模板" },  notes = "根据总帐科目批量删除计划模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emacclasses/{emacclass_id}/emplantempls/batch")
    public ResponseEntity<Boolean> removeBatchByEMACClass(@RequestBody List<String> ids) {
        emplantemplService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplantemplMapping.toDomain(returnObject.body),'eam-EMPlanTempl-Get')")
    @ApiOperation(value = "根据总帐科目获取计划模板", tags = {"计划模板" },  notes = "根据总帐科目获取计划模板")
	@RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplantempls/{emplantempl_id}")
    public ResponseEntity<EMPlanTemplDTO> getByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @PathVariable("emplantempl_id") String emplantempl_id) {
        EMPlanTempl domain = emplantemplService.get(emplantempl_id);
        EMPlanTemplDTO dto = emplantemplMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据总帐科目获取计划模板草稿", tags = {"计划模板" },  notes = "根据总帐科目获取计划模板草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emacclasses/{emacclass_id}/emplantempls/getdraft")
    public ResponseEntity<EMPlanTemplDTO> getDraftByEMACClass(@PathVariable("emacclass_id") String emacclass_id) {
        EMPlanTempl domain = new EMPlanTempl();
        domain.setAcclassid(emacclass_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplantemplMapping.toDto(emplantemplService.getDraft(domain)));
    }

    @ApiOperation(value = "根据总帐科目检查计划模板", tags = {"计划模板" },  notes = "根据总帐科目检查计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @RequestBody EMPlanTemplDTO emplantempldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplantemplService.checkKey(emplantemplMapping.toDomain(emplantempldto)));
    }

    @PreAuthorize("hasPermission(this.emplantemplMapping.toDomain(#emplantempldto),'eam-EMPlanTempl-Save')")
    @ApiOperation(value = "根据总帐科目保存计划模板", tags = {"计划模板" },  notes = "根据总帐科目保存计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/save")
    public ResponseEntity<Boolean> saveByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @RequestBody EMPlanTemplDTO emplantempldto) {
        EMPlanTempl domain = emplantemplMapping.toDomain(emplantempldto);
        domain.setAcclassid(emacclass_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplantemplService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emplantemplMapping.toDomain(#emplantempldtos),'eam-EMPlanTempl-Save')")
    @ApiOperation(value = "根据总帐科目批量保存计划模板", tags = {"计划模板" },  notes = "根据总帐科目批量保存计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/emacclasses/{emacclass_id}/emplantempls/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @RequestBody List<EMPlanTemplDTO> emplantempldtos) {
        List<EMPlanTempl> domainlist=emplantemplMapping.toDomain(emplantempldtos);
        for(EMPlanTempl domain:domainlist){
             domain.setAcclassid(emacclass_id);
        }
        emplantemplService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanTempl-searchDefault-all') and hasPermission(#context,'eam-EMPlanTempl-Get')")
	@ApiOperation(value = "根据总帐科目获取DEFAULT", tags = {"计划模板" } ,notes = "根据总帐科目获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emacclasses/{emacclass_id}/emplantempls/fetchdefault")
	public ResponseEntity<List<EMPlanTemplDTO>> fetchEMPlanTemplDefaultByEMACClass(@PathVariable("emacclass_id") String emacclass_id,EMPlanTemplSearchContext context) {
        context.setN_acclassid_eq(emacclass_id);
        Page<EMPlanTempl> domains = emplantemplService.searchDefault(context) ;
        List<EMPlanTemplDTO> list = emplantemplMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanTempl-searchDefault-all') and hasPermission(#context,'eam-EMPlanTempl-Get')")
	@ApiOperation(value = "根据总帐科目查询DEFAULT", tags = {"计划模板" } ,notes = "根据总帐科目查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emacclasses/{emacclass_id}/emplantempls/searchdefault")
	public ResponseEntity<Page<EMPlanTemplDTO>> searchEMPlanTemplDefaultByEMACClass(@PathVariable("emacclass_id") String emacclass_id, @RequestBody EMPlanTemplSearchContext context) {
        context.setN_acclassid_eq(emacclass_id);
        Page<EMPlanTempl> domains = emplantemplService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplantemplMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplantemplMapping.toDomain(#emplantempldto),'eam-EMPlanTempl-Create')")
    @ApiOperation(value = "根据服务商建立计划模板", tags = {"计划模板" },  notes = "根据服务商建立计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls")
    public ResponseEntity<EMPlanTemplDTO> createByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMPlanTemplDTO emplantempldto) {
        EMPlanTempl domain = emplantemplMapping.toDomain(emplantempldto);
        domain.setRserviceid(emservice_id);
		emplantemplService.create(domain);
        EMPlanTemplDTO dto = emplantemplMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplantemplMapping.toDomain(#emplantempldtos),'eam-EMPlanTempl-Create')")
    @ApiOperation(value = "根据服务商批量建立计划模板", tags = {"计划模板" },  notes = "根据服务商批量建立计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/batch")
    public ResponseEntity<Boolean> createBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMPlanTemplDTO> emplantempldtos) {
        List<EMPlanTempl> domainlist=emplantemplMapping.toDomain(emplantempldtos);
        for(EMPlanTempl domain:domainlist){
            domain.setRserviceid(emservice_id);
        }
        emplantemplService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplantempl" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplantemplService.get(#emplantempl_id),'eam-EMPlanTempl-Update')")
    @ApiOperation(value = "根据服务商更新计划模板", tags = {"计划模板" },  notes = "根据服务商更新计划模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}")
    public ResponseEntity<EMPlanTemplDTO> updateByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanTemplDTO emplantempldto) {
        EMPlanTempl domain = emplantemplMapping.toDomain(emplantempldto);
        domain.setRserviceid(emservice_id);
        domain.setEmplantemplid(emplantempl_id);
		emplantemplService.update(domain);
        EMPlanTemplDTO dto = emplantemplMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplantemplService.getEmplantemplByEntities(this.emplantemplMapping.toDomain(#emplantempldtos)),'eam-EMPlanTempl-Update')")
    @ApiOperation(value = "根据服务商批量更新计划模板", tags = {"计划模板" },  notes = "根据服务商批量更新计划模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emplantempls/batch")
    public ResponseEntity<Boolean> updateBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMPlanTemplDTO> emplantempldtos) {
        List<EMPlanTempl> domainlist=emplantemplMapping.toDomain(emplantempldtos);
        for(EMPlanTempl domain:domainlist){
            domain.setRserviceid(emservice_id);
        }
        emplantemplService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplantemplService.get(#emplantempl_id),'eam-EMPlanTempl-Remove')")
    @ApiOperation(value = "根据服务商删除计划模板", tags = {"计划模板" },  notes = "根据服务商删除计划模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}")
    public ResponseEntity<Boolean> removeByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplantemplService.remove(emplantempl_id));
    }

    @PreAuthorize("hasPermission(this.emplantemplService.getEmplantemplByIds(#ids),'eam-EMPlanTempl-Remove')")
    @ApiOperation(value = "根据服务商批量删除计划模板", tags = {"计划模板" },  notes = "根据服务商批量删除计划模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emplantempls/batch")
    public ResponseEntity<Boolean> removeBatchByEMService(@RequestBody List<String> ids) {
        emplantemplService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplantemplMapping.toDomain(returnObject.body),'eam-EMPlanTempl-Get')")
    @ApiOperation(value = "根据服务商获取计划模板", tags = {"计划模板" },  notes = "根据服务商获取计划模板")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplantempls/{emplantempl_id}")
    public ResponseEntity<EMPlanTemplDTO> getByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emplantempl_id") String emplantempl_id) {
        EMPlanTempl domain = emplantemplService.get(emplantempl_id);
        EMPlanTemplDTO dto = emplantemplMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商获取计划模板草稿", tags = {"计划模板" },  notes = "根据服务商获取计划模板草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emplantempls/getdraft")
    public ResponseEntity<EMPlanTemplDTO> getDraftByEMService(@PathVariable("emservice_id") String emservice_id) {
        EMPlanTempl domain = new EMPlanTempl();
        domain.setRserviceid(emservice_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplantemplMapping.toDto(emplantemplService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商检查计划模板", tags = {"计划模板" },  notes = "根据服务商检查计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMPlanTemplDTO emplantempldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplantemplService.checkKey(emplantemplMapping.toDomain(emplantempldto)));
    }

    @PreAuthorize("hasPermission(this.emplantemplMapping.toDomain(#emplantempldto),'eam-EMPlanTempl-Save')")
    @ApiOperation(value = "根据服务商保存计划模板", tags = {"计划模板" },  notes = "根据服务商保存计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/save")
    public ResponseEntity<Boolean> saveByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMPlanTemplDTO emplantempldto) {
        EMPlanTempl domain = emplantemplMapping.toDomain(emplantempldto);
        domain.setRserviceid(emservice_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplantemplService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emplantemplMapping.toDomain(#emplantempldtos),'eam-EMPlanTempl-Save')")
    @ApiOperation(value = "根据服务商批量保存计划模板", tags = {"计划模板" },  notes = "根据服务商批量保存计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emplantempls/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMPlanTemplDTO> emplantempldtos) {
        List<EMPlanTempl> domainlist=emplantemplMapping.toDomain(emplantempldtos);
        for(EMPlanTempl domain:domainlist){
             domain.setRserviceid(emservice_id);
        }
        emplantemplService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanTempl-searchDefault-all') and hasPermission(#context,'eam-EMPlanTempl-Get')")
	@ApiOperation(value = "根据服务商获取DEFAULT", tags = {"计划模板" } ,notes = "根据服务商获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emplantempls/fetchdefault")
	public ResponseEntity<List<EMPlanTemplDTO>> fetchEMPlanTemplDefaultByEMService(@PathVariable("emservice_id") String emservice_id,EMPlanTemplSearchContext context) {
        context.setN_rserviceid_eq(emservice_id);
        Page<EMPlanTempl> domains = emplantemplService.searchDefault(context) ;
        List<EMPlanTemplDTO> list = emplantemplMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanTempl-searchDefault-all') and hasPermission(#context,'eam-EMPlanTempl-Get')")
	@ApiOperation(value = "根据服务商查询DEFAULT", tags = {"计划模板" } ,notes = "根据服务商查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emplantempls/searchdefault")
	public ResponseEntity<Page<EMPlanTemplDTO>> searchEMPlanTemplDefaultByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMPlanTemplSearchContext context) {
        context.setN_rserviceid_eq(emservice_id);
        Page<EMPlanTempl> domains = emplantemplService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplantemplMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emplantemplMapping.toDomain(#emplantempldto),'eam-EMPlanTempl-Create')")
    @ApiOperation(value = "根据班组建立计划模板", tags = {"计划模板" },  notes = "根据班组建立计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls")
    public ResponseEntity<EMPlanTemplDTO> createByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMPlanTemplDTO emplantempldto) {
        EMPlanTempl domain = emplantemplMapping.toDomain(emplantempldto);
        domain.setRteamid(pfteam_id);
		emplantemplService.create(domain);
        EMPlanTemplDTO dto = emplantemplMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplantemplMapping.toDomain(#emplantempldtos),'eam-EMPlanTempl-Create')")
    @ApiOperation(value = "根据班组批量建立计划模板", tags = {"计划模板" },  notes = "根据班组批量建立计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/batch")
    public ResponseEntity<Boolean> createBatchByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMPlanTemplDTO> emplantempldtos) {
        List<EMPlanTempl> domainlist=emplantemplMapping.toDomain(emplantempldtos);
        for(EMPlanTempl domain:domainlist){
            domain.setRteamid(pfteam_id);
        }
        emplantemplService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emplantempl" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emplantemplService.get(#emplantempl_id),'eam-EMPlanTempl-Update')")
    @ApiOperation(value = "根据班组更新计划模板", tags = {"计划模板" },  notes = "根据班组更新计划模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}")
    public ResponseEntity<EMPlanTemplDTO> updateByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id, @RequestBody EMPlanTemplDTO emplantempldto) {
        EMPlanTempl domain = emplantemplMapping.toDomain(emplantempldto);
        domain.setRteamid(pfteam_id);
        domain.setEmplantemplid(emplantempl_id);
		emplantemplService.update(domain);
        EMPlanTemplDTO dto = emplantemplMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emplantemplService.getEmplantemplByEntities(this.emplantemplMapping.toDomain(#emplantempldtos)),'eam-EMPlanTempl-Update')")
    @ApiOperation(value = "根据班组批量更新计划模板", tags = {"计划模板" },  notes = "根据班组批量更新计划模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emplantempls/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMPlanTemplDTO> emplantempldtos) {
        List<EMPlanTempl> domainlist=emplantemplMapping.toDomain(emplantempldtos);
        for(EMPlanTempl domain:domainlist){
            domain.setRteamid(pfteam_id);
        }
        emplantemplService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emplantemplService.get(#emplantempl_id),'eam-EMPlanTempl-Remove')")
    @ApiOperation(value = "根据班组删除计划模板", tags = {"计划模板" },  notes = "根据班组删除计划模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}")
    public ResponseEntity<Boolean> removeByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emplantemplService.remove(emplantempl_id));
    }

    @PreAuthorize("hasPermission(this.emplantemplService.getEmplantemplByIds(#ids),'eam-EMPlanTempl-Remove')")
    @ApiOperation(value = "根据班组批量删除计划模板", tags = {"计划模板" },  notes = "根据班组批量删除计划模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emplantempls/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeam(@RequestBody List<String> ids) {
        emplantemplService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emplantemplMapping.toDomain(returnObject.body),'eam-EMPlanTempl-Get')")
    @ApiOperation(value = "根据班组获取计划模板", tags = {"计划模板" },  notes = "根据班组获取计划模板")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplantempls/{emplantempl_id}")
    public ResponseEntity<EMPlanTemplDTO> getByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emplantempl_id") String emplantempl_id) {
        EMPlanTempl domain = emplantemplService.get(emplantempl_id);
        EMPlanTemplDTO dto = emplantemplMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组获取计划模板草稿", tags = {"计划模板" },  notes = "根据班组获取计划模板草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emplantempls/getdraft")
    public ResponseEntity<EMPlanTemplDTO> getDraftByPFTeam(@PathVariable("pfteam_id") String pfteam_id) {
        EMPlanTempl domain = new EMPlanTempl();
        domain.setRteamid(pfteam_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplantemplMapping.toDto(emplantemplService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组检查计划模板", tags = {"计划模板" },  notes = "根据班组检查计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMPlanTemplDTO emplantempldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emplantemplService.checkKey(emplantemplMapping.toDomain(emplantempldto)));
    }

    @PreAuthorize("hasPermission(this.emplantemplMapping.toDomain(#emplantempldto),'eam-EMPlanTempl-Save')")
    @ApiOperation(value = "根据班组保存计划模板", tags = {"计划模板" },  notes = "根据班组保存计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/save")
    public ResponseEntity<Boolean> saveByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMPlanTemplDTO emplantempldto) {
        EMPlanTempl domain = emplantemplMapping.toDomain(emplantempldto);
        domain.setRteamid(pfteam_id);
        return ResponseEntity.status(HttpStatus.OK).body(emplantemplService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emplantemplMapping.toDomain(#emplantempldtos),'eam-EMPlanTempl-Save')")
    @ApiOperation(value = "根据班组批量保存计划模板", tags = {"计划模板" },  notes = "根据班组批量保存计划模板")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emplantempls/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMPlanTemplDTO> emplantempldtos) {
        List<EMPlanTempl> domainlist=emplantemplMapping.toDomain(emplantempldtos);
        for(EMPlanTempl domain:domainlist){
             domain.setRteamid(pfteam_id);
        }
        emplantemplService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanTempl-searchDefault-all') and hasPermission(#context,'eam-EMPlanTempl-Get')")
	@ApiOperation(value = "根据班组获取DEFAULT", tags = {"计划模板" } ,notes = "根据班组获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emplantempls/fetchdefault")
	public ResponseEntity<List<EMPlanTemplDTO>> fetchEMPlanTemplDefaultByPFTeam(@PathVariable("pfteam_id") String pfteam_id,EMPlanTemplSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMPlanTempl> domains = emplantemplService.searchDefault(context) ;
        List<EMPlanTemplDTO> list = emplantemplMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMPlanTempl-searchDefault-all') and hasPermission(#context,'eam-EMPlanTempl-Get')")
	@ApiOperation(value = "根据班组查询DEFAULT", tags = {"计划模板" } ,notes = "根据班组查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emplantempls/searchdefault")
	public ResponseEntity<Page<EMPlanTemplDTO>> searchEMPlanTemplDefaultByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMPlanTemplSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMPlanTempl> domains = emplantemplService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emplantemplMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

