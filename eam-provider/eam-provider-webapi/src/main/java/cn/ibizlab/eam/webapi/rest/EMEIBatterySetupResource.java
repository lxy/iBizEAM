package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEIBatterySetup;
import cn.ibizlab.eam.core.eam_core.service.IEMEIBatterySetupService;
import cn.ibizlab.eam.core.eam_core.filter.EMEIBatterySetupSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"电瓶换装记录" })
@RestController("WebApi-emeibatterysetup")
@RequestMapping("")
public class EMEIBatterySetupResource {

    @Autowired
    public IEMEIBatterySetupService emeibatterysetupService;

    @Autowired
    @Lazy
    public EMEIBatterySetupMapping emeibatterysetupMapping;

    @PreAuthorize("hasPermission(this.emeibatterysetupMapping.toDomain(#emeibatterysetupdto),'eam-EMEIBatterySetup-Create')")
    @ApiOperation(value = "新建电瓶换装记录", tags = {"电瓶换装记录" },  notes = "新建电瓶换装记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatterysetups")
    public ResponseEntity<EMEIBatterySetupDTO> create(@Validated @RequestBody EMEIBatterySetupDTO emeibatterysetupdto) {
        EMEIBatterySetup domain = emeibatterysetupMapping.toDomain(emeibatterysetupdto);
		emeibatterysetupService.create(domain);
        EMEIBatterySetupDTO dto = emeibatterysetupMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeibatterysetupMapping.toDomain(#emeibatterysetupdtos),'eam-EMEIBatterySetup-Create')")
    @ApiOperation(value = "批量新建电瓶换装记录", tags = {"电瓶换装记录" },  notes = "批量新建电瓶换装记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatterysetups/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEIBatterySetupDTO> emeibatterysetupdtos) {
        emeibatterysetupService.createBatch(emeibatterysetupMapping.toDomain(emeibatterysetupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeibatterysetup" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeibatterysetupService.get(#emeibatterysetup_id),'eam-EMEIBatterySetup-Update')")
    @ApiOperation(value = "更新电瓶换装记录", tags = {"电瓶换装记录" },  notes = "更新电瓶换装记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeibatterysetups/{emeibatterysetup_id}")
    public ResponseEntity<EMEIBatterySetupDTO> update(@PathVariable("emeibatterysetup_id") String emeibatterysetup_id, @RequestBody EMEIBatterySetupDTO emeibatterysetupdto) {
		EMEIBatterySetup domain  = emeibatterysetupMapping.toDomain(emeibatterysetupdto);
        domain .setEmeibatterysetupid(emeibatterysetup_id);
		emeibatterysetupService.update(domain );
		EMEIBatterySetupDTO dto = emeibatterysetupMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeibatterysetupService.getEmeibatterysetupByEntities(this.emeibatterysetupMapping.toDomain(#emeibatterysetupdtos)),'eam-EMEIBatterySetup-Update')")
    @ApiOperation(value = "批量更新电瓶换装记录", tags = {"电瓶换装记录" },  notes = "批量更新电瓶换装记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeibatterysetups/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEIBatterySetupDTO> emeibatterysetupdtos) {
        emeibatterysetupService.updateBatch(emeibatterysetupMapping.toDomain(emeibatterysetupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeibatterysetupService.get(#emeibatterysetup_id),'eam-EMEIBatterySetup-Remove')")
    @ApiOperation(value = "删除电瓶换装记录", tags = {"电瓶换装记录" },  notes = "删除电瓶换装记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeibatterysetups/{emeibatterysetup_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeibatterysetup_id") String emeibatterysetup_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeibatterysetupService.remove(emeibatterysetup_id));
    }

    @PreAuthorize("hasPermission(this.emeibatterysetupService.getEmeibatterysetupByIds(#ids),'eam-EMEIBatterySetup-Remove')")
    @ApiOperation(value = "批量删除电瓶换装记录", tags = {"电瓶换装记录" },  notes = "批量删除电瓶换装记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeibatterysetups/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeibatterysetupService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeibatterysetupMapping.toDomain(returnObject.body),'eam-EMEIBatterySetup-Get')")
    @ApiOperation(value = "获取电瓶换装记录", tags = {"电瓶换装记录" },  notes = "获取电瓶换装记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emeibatterysetups/{emeibatterysetup_id}")
    public ResponseEntity<EMEIBatterySetupDTO> get(@PathVariable("emeibatterysetup_id") String emeibatterysetup_id) {
        EMEIBatterySetup domain = emeibatterysetupService.get(emeibatterysetup_id);
        EMEIBatterySetupDTO dto = emeibatterysetupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取电瓶换装记录草稿", tags = {"电瓶换装记录" },  notes = "获取电瓶换装记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeibatterysetups/getdraft")
    public ResponseEntity<EMEIBatterySetupDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emeibatterysetupMapping.toDto(emeibatterysetupService.getDraft(new EMEIBatterySetup())));
    }

    @ApiOperation(value = "检查电瓶换装记录", tags = {"电瓶换装记录" },  notes = "检查电瓶换装记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatterysetups/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEIBatterySetupDTO emeibatterysetupdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeibatterysetupService.checkKey(emeibatterysetupMapping.toDomain(emeibatterysetupdto)));
    }

    @PreAuthorize("hasPermission(this.emeibatterysetupMapping.toDomain(#emeibatterysetupdto),'eam-EMEIBatterySetup-Save')")
    @ApiOperation(value = "保存电瓶换装记录", tags = {"电瓶换装记录" },  notes = "保存电瓶换装记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatterysetups/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEIBatterySetupDTO emeibatterysetupdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emeibatterysetupService.save(emeibatterysetupMapping.toDomain(emeibatterysetupdto)));
    }

    @PreAuthorize("hasPermission(this.emeibatterysetupMapping.toDomain(#emeibatterysetupdtos),'eam-EMEIBatterySetup-Save')")
    @ApiOperation(value = "批量保存电瓶换装记录", tags = {"电瓶换装记录" },  notes = "批量保存电瓶换装记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeibatterysetups/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEIBatterySetupDTO> emeibatterysetupdtos) {
        emeibatterysetupService.saveBatch(emeibatterysetupMapping.toDomain(emeibatterysetupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEIBatterySetup-searchDefault-all') and hasPermission(#context,'eam-EMEIBatterySetup-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"电瓶换装记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeibatterysetups/fetchdefault")
	public ResponseEntity<List<EMEIBatterySetupDTO>> fetchDefault(EMEIBatterySetupSearchContext context) {
        Page<EMEIBatterySetup> domains = emeibatterysetupService.searchDefault(context) ;
        List<EMEIBatterySetupDTO> list = emeibatterysetupMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEIBatterySetup-searchDefault-all') and hasPermission(#context,'eam-EMEIBatterySetup-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"电瓶换装记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeibatterysetups/searchdefault")
	public ResponseEntity<Page<EMEIBatterySetupDTO>> searchDefault(@RequestBody EMEIBatterySetupSearchContext context) {
        Page<EMEIBatterySetup> domains = emeibatterysetupService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeibatterysetupMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

