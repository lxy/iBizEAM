package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEN;
import cn.ibizlab.eam.core.eam_core.service.IEMENService;
import cn.ibizlab.eam.core.eam_core.filter.EMENSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"能源" })
@RestController("WebApi-emen")
@RequestMapping("")
public class EMENResource {

    @Autowired
    public IEMENService emenService;

    @Autowired
    @Lazy
    public EMENMapping emenMapping;

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendto),'eam-EMEN-Create')")
    @ApiOperation(value = "新建能源", tags = {"能源" },  notes = "新建能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emen")
    public ResponseEntity<EMENDTO> create(@Validated @RequestBody EMENDTO emendto) {
        EMEN domain = emenMapping.toDomain(emendto);
		emenService.create(domain);
        EMENDTO dto = emenMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendtos),'eam-EMEN-Create')")
    @ApiOperation(value = "批量新建能源", tags = {"能源" },  notes = "批量新建能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emen/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMENDTO> emendtos) {
        emenService.createBatch(emenMapping.toDomain(emendtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emen" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emenService.get(#emen_id),'eam-EMEN-Update')")
    @ApiOperation(value = "更新能源", tags = {"能源" },  notes = "更新能源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emen/{emen_id}")
    public ResponseEntity<EMENDTO> update(@PathVariable("emen_id") String emen_id, @RequestBody EMENDTO emendto) {
		EMEN domain  = emenMapping.toDomain(emendto);
        domain .setEmenid(emen_id);
		emenService.update(domain );
		EMENDTO dto = emenMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenService.getEmenByEntities(this.emenMapping.toDomain(#emendtos)),'eam-EMEN-Update')")
    @ApiOperation(value = "批量更新能源", tags = {"能源" },  notes = "批量更新能源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emen/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMENDTO> emendtos) {
        emenService.updateBatch(emenMapping.toDomain(emendtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emenService.get(#emen_id),'eam-EMEN-Remove')")
    @ApiOperation(value = "删除能源", tags = {"能源" },  notes = "删除能源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emen/{emen_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emen_id") String emen_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emenService.remove(emen_id));
    }

    @PreAuthorize("hasPermission(this.emenService.getEmenByIds(#ids),'eam-EMEN-Remove')")
    @ApiOperation(value = "批量删除能源", tags = {"能源" },  notes = "批量删除能源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emen/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emenService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emenMapping.toDomain(returnObject.body),'eam-EMEN-Get')")
    @ApiOperation(value = "获取能源", tags = {"能源" },  notes = "获取能源")
	@RequestMapping(method = RequestMethod.GET, value = "/emen/{emen_id}")
    public ResponseEntity<EMENDTO> get(@PathVariable("emen_id") String emen_id) {
        EMEN domain = emenService.get(emen_id);
        EMENDTO dto = emenMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取能源草稿", tags = {"能源" },  notes = "获取能源草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emen/getdraft")
    public ResponseEntity<EMENDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emenMapping.toDto(emenService.getDraft(new EMEN())));
    }

    @ApiOperation(value = "检查能源", tags = {"能源" },  notes = "检查能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emen/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMENDTO emendto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emenService.checkKey(emenMapping.toDomain(emendto)));
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendto),'eam-EMEN-Save')")
    @ApiOperation(value = "保存能源", tags = {"能源" },  notes = "保存能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emen/save")
    public ResponseEntity<Boolean> save(@RequestBody EMENDTO emendto) {
        return ResponseEntity.status(HttpStatus.OK).body(emenService.save(emenMapping.toDomain(emendto)));
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendtos),'eam-EMEN-Save')")
    @ApiOperation(value = "批量保存能源", tags = {"能源" },  notes = "批量保存能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emen/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMENDTO> emendtos) {
        emenService.saveBatch(emenMapping.toDomain(emendtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEN-searchDefault-all') and hasPermission(#context,'eam-EMEN-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"能源" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emen/fetchdefault")
	public ResponseEntity<List<EMENDTO>> fetchDefault(EMENSearchContext context) {
        Page<EMEN> domains = emenService.searchDefault(context) ;
        List<EMENDTO> list = emenMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEN-searchDefault-all') and hasPermission(#context,'eam-EMEN-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"能源" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emen/searchdefault")
	public ResponseEntity<Page<EMENDTO>> searchDefault(@RequestBody EMENSearchContext context) {
        Page<EMEN> domains = emenService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emenMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendto),'eam-EMEN-Create')")
    @ApiOperation(value = "根据物品建立能源", tags = {"能源" },  notes = "根据物品建立能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emen")
    public ResponseEntity<EMENDTO> createByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMENDTO emendto) {
        EMEN domain = emenMapping.toDomain(emendto);
        domain.setItemid(emitem_id);
		emenService.create(domain);
        EMENDTO dto = emenMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendtos),'eam-EMEN-Create')")
    @ApiOperation(value = "根据物品批量建立能源", tags = {"能源" },  notes = "根据物品批量建立能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emen/batch")
    public ResponseEntity<Boolean> createBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMENDTO> emendtos) {
        List<EMEN> domainlist=emenMapping.toDomain(emendtos);
        for(EMEN domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emenService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emen" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emenService.get(#emen_id),'eam-EMEN-Update')")
    @ApiOperation(value = "根据物品更新能源", tags = {"能源" },  notes = "根据物品更新能源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emen/{emen_id}")
    public ResponseEntity<EMENDTO> updateByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENDTO emendto) {
        EMEN domain = emenMapping.toDomain(emendto);
        domain.setItemid(emitem_id);
        domain.setEmenid(emen_id);
		emenService.update(domain);
        EMENDTO dto = emenMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenService.getEmenByEntities(this.emenMapping.toDomain(#emendtos)),'eam-EMEN-Update')")
    @ApiOperation(value = "根据物品批量更新能源", tags = {"能源" },  notes = "根据物品批量更新能源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emen/batch")
    public ResponseEntity<Boolean> updateBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMENDTO> emendtos) {
        List<EMEN> domainlist=emenMapping.toDomain(emendtos);
        for(EMEN domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emenService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emenService.get(#emen_id),'eam-EMEN-Remove')")
    @ApiOperation(value = "根据物品删除能源", tags = {"能源" },  notes = "根据物品删除能源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emen/{emen_id}")
    public ResponseEntity<Boolean> removeByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emenService.remove(emen_id));
    }

    @PreAuthorize("hasPermission(this.emenService.getEmenByIds(#ids),'eam-EMEN-Remove')")
    @ApiOperation(value = "根据物品批量删除能源", tags = {"能源" },  notes = "根据物品批量删除能源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emen/batch")
    public ResponseEntity<Boolean> removeBatchByEMItem(@RequestBody List<String> ids) {
        emenService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emenMapping.toDomain(returnObject.body),'eam-EMEN-Get')")
    @ApiOperation(value = "根据物品获取能源", tags = {"能源" },  notes = "根据物品获取能源")
	@RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emen/{emen_id}")
    public ResponseEntity<EMENDTO> getByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id) {
        EMEN domain = emenService.get(emen_id);
        EMENDTO dto = emenMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据物品获取能源草稿", tags = {"能源" },  notes = "根据物品获取能源草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emen/getdraft")
    public ResponseEntity<EMENDTO> getDraftByEMItem(@PathVariable("emitem_id") String emitem_id) {
        EMEN domain = new EMEN();
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenMapping.toDto(emenService.getDraft(domain)));
    }

    @ApiOperation(value = "根据物品检查能源", tags = {"能源" },  notes = "根据物品检查能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emen/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMENDTO emendto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emenService.checkKey(emenMapping.toDomain(emendto)));
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendto),'eam-EMEN-Save')")
    @ApiOperation(value = "根据物品保存能源", tags = {"能源" },  notes = "根据物品保存能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emen/save")
    public ResponseEntity<Boolean> saveByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMENDTO emendto) {
        EMEN domain = emenMapping.toDomain(emendto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendtos),'eam-EMEN-Save')")
    @ApiOperation(value = "根据物品批量保存能源", tags = {"能源" },  notes = "根据物品批量保存能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emen/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMENDTO> emendtos) {
        List<EMEN> domainlist=emenMapping.toDomain(emendtos);
        for(EMEN domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emenService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEN-searchDefault-all') and hasPermission(#context,'eam-EMEN-Get')")
	@ApiOperation(value = "根据物品获取DEFAULT", tags = {"能源" } ,notes = "根据物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emen/fetchdefault")
	public ResponseEntity<List<EMENDTO>> fetchEMENDefaultByEMItem(@PathVariable("emitem_id") String emitem_id,EMENSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEN> domains = emenService.searchDefault(context) ;
        List<EMENDTO> list = emenMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEN-searchDefault-all') and hasPermission(#context,'eam-EMEN-Get')")
	@ApiOperation(value = "根据物品查询DEFAULT", tags = {"能源" } ,notes = "根据物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emen/searchdefault")
	public ResponseEntity<Page<EMENDTO>> searchEMENDefaultByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMENSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEN> domains = emenService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emenMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendto),'eam-EMEN-Create')")
    @ApiOperation(value = "根据服务商物品建立能源", tags = {"能源" },  notes = "根据服务商物品建立能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen")
    public ResponseEntity<EMENDTO> createByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMENDTO emendto) {
        EMEN domain = emenMapping.toDomain(emendto);
        domain.setItemid(emitem_id);
		emenService.create(domain);
        EMENDTO dto = emenMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendtos),'eam-EMEN-Create')")
    @ApiOperation(value = "根据服务商物品批量建立能源", tags = {"能源" },  notes = "根据服务商物品批量建立能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMENDTO> emendtos) {
        List<EMEN> domainlist=emenMapping.toDomain(emendtos);
        for(EMEN domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emenService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emen" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emenService.get(#emen_id),'eam-EMEN-Update')")
    @ApiOperation(value = "根据服务商物品更新能源", tags = {"能源" },  notes = "根据服务商物品更新能源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}")
    public ResponseEntity<EMENDTO> updateByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENDTO emendto) {
        EMEN domain = emenMapping.toDomain(emendto);
        domain.setItemid(emitem_id);
        domain.setEmenid(emen_id);
		emenService.update(domain);
        EMENDTO dto = emenMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenService.getEmenByEntities(this.emenMapping.toDomain(#emendtos)),'eam-EMEN-Update')")
    @ApiOperation(value = "根据服务商物品批量更新能源", tags = {"能源" },  notes = "根据服务商物品批量更新能源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMENDTO> emendtos) {
        List<EMEN> domainlist=emenMapping.toDomain(emendtos);
        for(EMEN domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emenService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emenService.get(#emen_id),'eam-EMEN-Remove')")
    @ApiOperation(value = "根据服务商物品删除能源", tags = {"能源" },  notes = "根据服务商物品删除能源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emenService.remove(emen_id));
    }

    @PreAuthorize("hasPermission(this.emenService.getEmenByIds(#ids),'eam-EMEN-Remove')")
    @ApiOperation(value = "根据服务商物品批量删除能源", tags = {"能源" },  notes = "根据服务商物品批量删除能源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMItem(@RequestBody List<String> ids) {
        emenService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emenMapping.toDomain(returnObject.body),'eam-EMEN-Get')")
    @ApiOperation(value = "根据服务商物品获取能源", tags = {"能源" },  notes = "根据服务商物品获取能源")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}")
    public ResponseEntity<EMENDTO> getByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id) {
        EMEN domain = emenService.get(emen_id);
        EMENDTO dto = emenMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商物品获取能源草稿", tags = {"能源" },  notes = "根据服务商物品获取能源草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/getdraft")
    public ResponseEntity<EMENDTO> getDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id) {
        EMEN domain = new EMEN();
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenMapping.toDto(emenService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商物品检查能源", tags = {"能源" },  notes = "根据服务商物品检查能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMENDTO emendto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emenService.checkKey(emenMapping.toDomain(emendto)));
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendto),'eam-EMEN-Save')")
    @ApiOperation(value = "根据服务商物品保存能源", tags = {"能源" },  notes = "根据服务商物品保存能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/save")
    public ResponseEntity<Boolean> saveByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMENDTO emendto) {
        EMEN domain = emenMapping.toDomain(emendto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendtos),'eam-EMEN-Save')")
    @ApiOperation(value = "根据服务商物品批量保存能源", tags = {"能源" },  notes = "根据服务商物品批量保存能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMENDTO> emendtos) {
        List<EMEN> domainlist=emenMapping.toDomain(emendtos);
        for(EMEN domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emenService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEN-searchDefault-all') and hasPermission(#context,'eam-EMEN-Get')")
	@ApiOperation(value = "根据服务商物品获取DEFAULT", tags = {"能源" } ,notes = "根据服务商物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emen/fetchdefault")
	public ResponseEntity<List<EMENDTO>> fetchEMENDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMENSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEN> domains = emenService.searchDefault(context) ;
        List<EMENDTO> list = emenMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEN-searchDefault-all') and hasPermission(#context,'eam-EMEN-Get')")
	@ApiOperation(value = "根据服务商物品查询DEFAULT", tags = {"能源" } ,notes = "根据服务商物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emen/searchdefault")
	public ResponseEntity<Page<EMENDTO>> searchEMENDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMENSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEN> domains = emenService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emenMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendto),'eam-EMEN-Create')")
    @ApiOperation(value = "根据仓库库位物品建立能源", tags = {"能源" },  notes = "根据仓库库位物品建立能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen")
    public ResponseEntity<EMENDTO> createByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMENDTO emendto) {
        EMEN domain = emenMapping.toDomain(emendto);
        domain.setItemid(emitem_id);
		emenService.create(domain);
        EMENDTO dto = emenMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendtos),'eam-EMEN-Create')")
    @ApiOperation(value = "根据仓库库位物品批量建立能源", tags = {"能源" },  notes = "根据仓库库位物品批量建立能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/batch")
    public ResponseEntity<Boolean> createBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMENDTO> emendtos) {
        List<EMEN> domainlist=emenMapping.toDomain(emendtos);
        for(EMEN domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emenService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emen" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emenService.get(#emen_id),'eam-EMEN-Update')")
    @ApiOperation(value = "根据仓库库位物品更新能源", tags = {"能源" },  notes = "根据仓库库位物品更新能源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}")
    public ResponseEntity<EMENDTO> updateByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENDTO emendto) {
        EMEN domain = emenMapping.toDomain(emendto);
        domain.setItemid(emitem_id);
        domain.setEmenid(emen_id);
		emenService.update(domain);
        EMENDTO dto = emenMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenService.getEmenByEntities(this.emenMapping.toDomain(#emendtos)),'eam-EMEN-Update')")
    @ApiOperation(value = "根据仓库库位物品批量更新能源", tags = {"能源" },  notes = "根据仓库库位物品批量更新能源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/batch")
    public ResponseEntity<Boolean> updateBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMENDTO> emendtos) {
        List<EMEN> domainlist=emenMapping.toDomain(emendtos);
        for(EMEN domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emenService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emenService.get(#emen_id),'eam-EMEN-Remove')")
    @ApiOperation(value = "根据仓库库位物品删除能源", tags = {"能源" },  notes = "根据仓库库位物品删除能源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}")
    public ResponseEntity<Boolean> removeByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emenService.remove(emen_id));
    }

    @PreAuthorize("hasPermission(this.emenService.getEmenByIds(#ids),'eam-EMEN-Remove')")
    @ApiOperation(value = "根据仓库库位物品批量删除能源", tags = {"能源" },  notes = "根据仓库库位物品批量删除能源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/batch")
    public ResponseEntity<Boolean> removeBatchByEMStorePartEMItem(@RequestBody List<String> ids) {
        emenService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emenMapping.toDomain(returnObject.body),'eam-EMEN-Get')")
    @ApiOperation(value = "根据仓库库位物品获取能源", tags = {"能源" },  notes = "根据仓库库位物品获取能源")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}")
    public ResponseEntity<EMENDTO> getByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id) {
        EMEN domain = emenService.get(emen_id);
        EMENDTO dto = emenMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库库位物品获取能源草稿", tags = {"能源" },  notes = "根据仓库库位物品获取能源草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/getdraft")
    public ResponseEntity<EMENDTO> getDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id) {
        EMEN domain = new EMEN();
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenMapping.toDto(emenService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库库位物品检查能源", tags = {"能源" },  notes = "根据仓库库位物品检查能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMENDTO emendto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emenService.checkKey(emenMapping.toDomain(emendto)));
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendto),'eam-EMEN-Save')")
    @ApiOperation(value = "根据仓库库位物品保存能源", tags = {"能源" },  notes = "根据仓库库位物品保存能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/save")
    public ResponseEntity<Boolean> saveByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMENDTO emendto) {
        EMEN domain = emenMapping.toDomain(emendto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendtos),'eam-EMEN-Save')")
    @ApiOperation(value = "根据仓库库位物品批量保存能源", tags = {"能源" },  notes = "根据仓库库位物品批量保存能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMENDTO> emendtos) {
        List<EMEN> domainlist=emenMapping.toDomain(emendtos);
        for(EMEN domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emenService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEN-searchDefault-all') and hasPermission(#context,'eam-EMEN-Get')")
	@ApiOperation(value = "根据仓库库位物品获取DEFAULT", tags = {"能源" } ,notes = "根据仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/fetchdefault")
	public ResponseEntity<List<EMENDTO>> fetchEMENDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMENSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEN> domains = emenService.searchDefault(context) ;
        List<EMENDTO> list = emenMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEN-searchDefault-all') and hasPermission(#context,'eam-EMEN-Get')")
	@ApiOperation(value = "根据仓库库位物品查询DEFAULT", tags = {"能源" } ,notes = "根据仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/searchdefault")
	public ResponseEntity<Page<EMENDTO>> searchEMENDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMENSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEN> domains = emenService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emenMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendto),'eam-EMEN-Create')")
    @ApiOperation(value = "根据仓库物品建立能源", tags = {"能源" },  notes = "根据仓库物品建立能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen")
    public ResponseEntity<EMENDTO> createByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMENDTO emendto) {
        EMEN domain = emenMapping.toDomain(emendto);
        domain.setItemid(emitem_id);
		emenService.create(domain);
        EMENDTO dto = emenMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendtos),'eam-EMEN-Create')")
    @ApiOperation(value = "根据仓库物品批量建立能源", tags = {"能源" },  notes = "根据仓库物品批量建立能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMENDTO> emendtos) {
        List<EMEN> domainlist=emenMapping.toDomain(emendtos);
        for(EMEN domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emenService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emen" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emenService.get(#emen_id),'eam-EMEN-Update')")
    @ApiOperation(value = "根据仓库物品更新能源", tags = {"能源" },  notes = "根据仓库物品更新能源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}")
    public ResponseEntity<EMENDTO> updateByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENDTO emendto) {
        EMEN domain = emenMapping.toDomain(emendto);
        domain.setItemid(emitem_id);
        domain.setEmenid(emen_id);
		emenService.update(domain);
        EMENDTO dto = emenMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenService.getEmenByEntities(this.emenMapping.toDomain(#emendtos)),'eam-EMEN-Update')")
    @ApiOperation(value = "根据仓库物品批量更新能源", tags = {"能源" },  notes = "根据仓库物品批量更新能源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMENDTO> emendtos) {
        List<EMEN> domainlist=emenMapping.toDomain(emendtos);
        for(EMEN domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emenService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emenService.get(#emen_id),'eam-EMEN-Remove')")
    @ApiOperation(value = "根据仓库物品删除能源", tags = {"能源" },  notes = "根据仓库物品删除能源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emenService.remove(emen_id));
    }

    @PreAuthorize("hasPermission(this.emenService.getEmenByIds(#ids),'eam-EMEN-Remove')")
    @ApiOperation(value = "根据仓库物品批量删除能源", tags = {"能源" },  notes = "根据仓库物品批量删除能源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMItem(@RequestBody List<String> ids) {
        emenService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emenMapping.toDomain(returnObject.body),'eam-EMEN-Get')")
    @ApiOperation(value = "根据仓库物品获取能源", tags = {"能源" },  notes = "根据仓库物品获取能源")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}")
    public ResponseEntity<EMENDTO> getByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id) {
        EMEN domain = emenService.get(emen_id);
        EMENDTO dto = emenMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库物品获取能源草稿", tags = {"能源" },  notes = "根据仓库物品获取能源草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/getdraft")
    public ResponseEntity<EMENDTO> getDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id) {
        EMEN domain = new EMEN();
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenMapping.toDto(emenService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库物品检查能源", tags = {"能源" },  notes = "根据仓库物品检查能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMENDTO emendto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emenService.checkKey(emenMapping.toDomain(emendto)));
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendto),'eam-EMEN-Save')")
    @ApiOperation(value = "根据仓库物品保存能源", tags = {"能源" },  notes = "根据仓库物品保存能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/save")
    public ResponseEntity<Boolean> saveByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMENDTO emendto) {
        EMEN domain = emenMapping.toDomain(emendto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendtos),'eam-EMEN-Save')")
    @ApiOperation(value = "根据仓库物品批量保存能源", tags = {"能源" },  notes = "根据仓库物品批量保存能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMENDTO> emendtos) {
        List<EMEN> domainlist=emenMapping.toDomain(emendtos);
        for(EMEN domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emenService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEN-searchDefault-all') and hasPermission(#context,'eam-EMEN-Get')")
	@ApiOperation(value = "根据仓库物品获取DEFAULT", tags = {"能源" } ,notes = "根据仓库物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emen/fetchdefault")
	public ResponseEntity<List<EMENDTO>> fetchEMENDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMENSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEN> domains = emenService.searchDefault(context) ;
        List<EMENDTO> list = emenMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEN-searchDefault-all') and hasPermission(#context,'eam-EMEN-Get')")
	@ApiOperation(value = "根据仓库物品查询DEFAULT", tags = {"能源" } ,notes = "根据仓库物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emen/searchdefault")
	public ResponseEntity<Page<EMENDTO>> searchEMENDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMENSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEN> domains = emenService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emenMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendto),'eam-EMEN-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品建立能源", tags = {"能源" },  notes = "根据仓库仓库库位物品建立能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen")
    public ResponseEntity<EMENDTO> createByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMENDTO emendto) {
        EMEN domain = emenMapping.toDomain(emendto);
        domain.setItemid(emitem_id);
		emenService.create(domain);
        EMENDTO dto = emenMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendtos),'eam-EMEN-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品批量建立能源", tags = {"能源" },  notes = "根据仓库仓库库位物品批量建立能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMENDTO> emendtos) {
        List<EMEN> domainlist=emenMapping.toDomain(emendtos);
        for(EMEN domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emenService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emen" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emenService.get(#emen_id),'eam-EMEN-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品更新能源", tags = {"能源" },  notes = "根据仓库仓库库位物品更新能源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}")
    public ResponseEntity<EMENDTO> updateByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENDTO emendto) {
        EMEN domain = emenMapping.toDomain(emendto);
        domain.setItemid(emitem_id);
        domain.setEmenid(emen_id);
		emenService.update(domain);
        EMENDTO dto = emenMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenService.getEmenByEntities(this.emenMapping.toDomain(#emendtos)),'eam-EMEN-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品批量更新能源", tags = {"能源" },  notes = "根据仓库仓库库位物品批量更新能源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMENDTO> emendtos) {
        List<EMEN> domainlist=emenMapping.toDomain(emendtos);
        for(EMEN domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emenService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emenService.get(#emen_id),'eam-EMEN-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品删除能源", tags = {"能源" },  notes = "根据仓库仓库库位物品删除能源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emenService.remove(emen_id));
    }

    @PreAuthorize("hasPermission(this.emenService.getEmenByIds(#ids),'eam-EMEN-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品批量删除能源", tags = {"能源" },  notes = "根据仓库仓库库位物品批量删除能源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMStorePartEMItem(@RequestBody List<String> ids) {
        emenService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emenMapping.toDomain(returnObject.body),'eam-EMEN-Get')")
    @ApiOperation(value = "根据仓库仓库库位物品获取能源", tags = {"能源" },  notes = "根据仓库仓库库位物品获取能源")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}")
    public ResponseEntity<EMENDTO> getByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id) {
        EMEN domain = emenService.get(emen_id);
        EMENDTO dto = emenMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库仓库库位物品获取能源草稿", tags = {"能源" },  notes = "根据仓库仓库库位物品获取能源草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/getdraft")
    public ResponseEntity<EMENDTO> getDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id) {
        EMEN domain = new EMEN();
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenMapping.toDto(emenService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库仓库库位物品检查能源", tags = {"能源" },  notes = "根据仓库仓库库位物品检查能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMENDTO emendto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emenService.checkKey(emenMapping.toDomain(emendto)));
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendto),'eam-EMEN-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品保存能源", tags = {"能源" },  notes = "根据仓库仓库库位物品保存能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/save")
    public ResponseEntity<Boolean> saveByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMENDTO emendto) {
        EMEN domain = emenMapping.toDomain(emendto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emenMapping.toDomain(#emendtos),'eam-EMEN-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品批量保存能源", tags = {"能源" },  notes = "根据仓库仓库库位物品批量保存能源")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMENDTO> emendtos) {
        List<EMEN> domainlist=emenMapping.toDomain(emendtos);
        for(EMEN domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emenService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEN-searchDefault-all') and hasPermission(#context,'eam-EMEN-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取DEFAULT", tags = {"能源" } ,notes = "根据仓库仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/fetchdefault")
	public ResponseEntity<List<EMENDTO>> fetchEMENDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMENSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEN> domains = emenService.searchDefault(context) ;
        List<EMENDTO> list = emenMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEN-searchDefault-all') and hasPermission(#context,'eam-EMEN-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询DEFAULT", tags = {"能源" } ,notes = "根据仓库仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/searchdefault")
	public ResponseEntity<Page<EMENDTO>> searchEMENDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMENSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMEN> domains = emenService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emenMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

