package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQAH;
import cn.ibizlab.eam.webapi.dto.EMEQAHDTO;
import cn.ibizlab.eam.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="WebApiEMEQAHMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface EMEQAHMapping extends MappingBase<EMEQAHDTO, EMEQAH> {


}

