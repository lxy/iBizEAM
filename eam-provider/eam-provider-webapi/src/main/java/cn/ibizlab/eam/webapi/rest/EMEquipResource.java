package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEquip;
import cn.ibizlab.eam.core.eam_core.service.IEMEquipService;
import cn.ibizlab.eam.core.eam_core.filter.EMEquipSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"设备档案" })
@RestController("WebApi-emequip")
@RequestMapping("")
public class EMEquipResource {

    @Autowired
    public IEMEquipService emequipService;

    @Autowired
    @Lazy
    public EMEquipMapping emequipMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEquip-searchDefault-all') and hasPermission(#context,'eam-EMEquip-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"设备档案" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/fetchdefault")
	public ResponseEntity<List<EMEquipDTO>> fetchDefault(EMEquipSearchContext context) {
        Page<EMEquip> domains = emequipService.searchDefault(context) ;
        List<EMEquipDTO> list = emequipMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEquip-searchDefault-all') and hasPermission(#context,'eam-EMEquip-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"设备档案" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/searchdefault")
	public ResponseEntity<Page<EMEquipDTO>> searchDefault(@RequestBody EMEquipSearchContext context) {
        Page<EMEquip> domains = emequipService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emequipMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emequipMapping.toDomain(#emequipdto),'eam-EMEquip-Create')")
    @ApiOperation(value = "新建设备档案", tags = {"设备档案" },  notes = "新建设备档案")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips")
    public ResponseEntity<EMEquipDTO> create(@Validated @RequestBody EMEquipDTO emequipdto) {
        EMEquip domain = emequipMapping.toDomain(emequipdto);
		emequipService.create(domain);
        EMEquipDTO dto = emequipMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emequipMapping.toDomain(#emequipdtos),'eam-EMEquip-Create')")
    @ApiOperation(value = "批量新建设备档案", tags = {"设备档案" },  notes = "批量新建设备档案")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEquipDTO> emequipdtos) {
        emequipService.createBatch(emequipMapping.toDomain(emequipdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emequip" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emequipService.get(#emequip_id),'eam-EMEquip-Update')")
    @ApiOperation(value = "更新设备档案", tags = {"设备档案" },  notes = "更新设备档案")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}")
    public ResponseEntity<EMEquipDTO> update(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEquipDTO emequipdto) {
		EMEquip domain  = emequipMapping.toDomain(emequipdto);
        domain .setEmequipid(emequip_id);
		emequipService.update(domain );
		EMEquipDTO dto = emequipMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emequipService.getEmequipByEntities(this.emequipMapping.toDomain(#emequipdtos)),'eam-EMEquip-Update')")
    @ApiOperation(value = "批量更新设备档案", tags = {"设备档案" },  notes = "批量更新设备档案")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEquipDTO> emequipdtos) {
        emequipService.updateBatch(emequipMapping.toDomain(emequipdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emequipService.get(#emequip_id),'eam-EMEquip-Remove')")
    @ApiOperation(value = "删除设备档案", tags = {"设备档案" },  notes = "删除设备档案")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emequip_id") String emequip_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emequipService.remove(emequip_id));
    }

    @PreAuthorize("hasPermission(this.emequipService.getEmequipByIds(#ids),'eam-EMEquip-Remove')")
    @ApiOperation(value = "批量删除设备档案", tags = {"设备档案" },  notes = "批量删除设备档案")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emequipService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emequipMapping.toDomain(returnObject.body),'eam-EMEquip-Get')")
    @ApiOperation(value = "获取设备档案", tags = {"设备档案" },  notes = "获取设备档案")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}")
    public ResponseEntity<EMEquipDTO> get(@PathVariable("emequip_id") String emequip_id) {
        EMEquip domain = emequipService.get(emequip_id);
        EMEquipDTO dto = emequipMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取设备档案草稿", tags = {"设备档案" },  notes = "获取设备档案草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/getdraft")
    public ResponseEntity<EMEquipDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emequipMapping.toDto(emequipService.getDraft(new EMEquip())));
    }

    @ApiOperation(value = "检查设备档案", tags = {"设备档案" },  notes = "检查设备档案")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEquipDTO emequipdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emequipService.checkKey(emequipMapping.toDomain(emequipdto)));
    }

    @PreAuthorize("hasPermission(this.emequipMapping.toDomain(#emequipdto),'eam-EMEquip-Save')")
    @ApiOperation(value = "保存设备档案", tags = {"设备档案" },  notes = "保存设备档案")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEquipDTO emequipdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emequipService.save(emequipMapping.toDomain(emequipdto)));
    }

    @PreAuthorize("hasPermission(this.emequipMapping.toDomain(#emequipdtos),'eam-EMEquip-Save')")
    @ApiOperation(value = "批量保存设备档案", tags = {"设备档案" },  notes = "批量保存设备档案")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEquipDTO> emequipdtos) {
        emequipService.saveBatch(emequipMapping.toDomain(emequipdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEquip-searchEQTypeTree-all') and hasPermission(#context,'eam-EMEquip-Get')")
	@ApiOperation(value = "获取类型下设备", tags = {"设备档案" } ,notes = "获取类型下设备")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/fetcheqtypetree")
	public ResponseEntity<List<EMEquipDTO>> fetchEQTypeTree(EMEquipSearchContext context) {
        Page<EMEquip> domains = emequipService.searchEQTypeTree(context) ;
        List<EMEquipDTO> list = emequipMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEquip-searchEQTypeTree-all') and hasPermission(#context,'eam-EMEquip-Get')")
	@ApiOperation(value = "查询类型下设备", tags = {"设备档案" } ,notes = "查询类型下设备")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/searcheqtypetree")
	public ResponseEntity<Page<EMEquipDTO>> searchEQTypeTree(@RequestBody EMEquipSearchContext context) {
        Page<EMEquip> domains = emequipService.searchEQTypeTree(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emequipMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "获取各类型设备数量", tags = {"设备档案" } ,notes = "获取各类型设备数量")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/fetchtypeeqnum")
	public ResponseEntity<List<HashMap>> fetchTypeEQNum(EMEquipSearchContext context) {
        Page<HashMap> domains = emequipService.searchTypeEQNum(context) ;
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "查询各类型设备数量", tags = {"设备档案" } ,notes = "查询各类型设备数量")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/searchtypeeqnum")
	public ResponseEntity<Page<HashMap>> searchTypeEQNum(@RequestBody EMEquipSearchContext context) {
        Page<HashMap> domains = emequipService.searchTypeEQNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEquip-searchDefault-all') and hasPermission(#context,'eam-EMEquip-Get')")
	@ApiOperation(value = "根据班组获取DEFAULT", tags = {"设备档案" } ,notes = "根据班组获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/fetchdefault")
	public ResponseEntity<List<EMEquipDTO>> fetchEMEquipDefaultByPFTeam(@PathVariable("pfteam_id") String pfteam_id,EMEquipSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMEquip> domains = emequipService.searchDefault(context) ;
        List<EMEquipDTO> list = emequipMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEquip-searchDefault-all') and hasPermission(#context,'eam-EMEquip-Get')")
	@ApiOperation(value = "根据班组查询DEFAULT", tags = {"设备档案" } ,notes = "根据班组查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/searchdefault")
	public ResponseEntity<Page<EMEquipDTO>> searchEMEquipDefaultByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMEquipSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMEquip> domains = emequipService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emequipMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emequipMapping.toDomain(#emequipdto),'eam-EMEquip-Create')")
    @ApiOperation(value = "根据班组建立设备档案", tags = {"设备档案" },  notes = "根据班组建立设备档案")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips")
    public ResponseEntity<EMEquipDTO> createByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMEquipDTO emequipdto) {
        EMEquip domain = emequipMapping.toDomain(emequipdto);
        domain.setRteamid(pfteam_id);
		emequipService.create(domain);
        EMEquipDTO dto = emequipMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emequipMapping.toDomain(#emequipdtos),'eam-EMEquip-Create')")
    @ApiOperation(value = "根据班组批量建立设备档案", tags = {"设备档案" },  notes = "根据班组批量建立设备档案")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/batch")
    public ResponseEntity<Boolean> createBatchByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMEquipDTO> emequipdtos) {
        List<EMEquip> domainlist=emequipMapping.toDomain(emequipdtos);
        for(EMEquip domain:domainlist){
            domain.setRteamid(pfteam_id);
        }
        emequipService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emequip" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emequipService.get(#emequip_id),'eam-EMEquip-Update')")
    @ApiOperation(value = "根据班组更新设备档案", tags = {"设备档案" },  notes = "根据班组更新设备档案")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}")
    public ResponseEntity<EMEquipDTO> updateByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEquipDTO emequipdto) {
        EMEquip domain = emequipMapping.toDomain(emequipdto);
        domain.setRteamid(pfteam_id);
        domain.setEmequipid(emequip_id);
		emequipService.update(domain);
        EMEquipDTO dto = emequipMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emequipService.getEmequipByEntities(this.emequipMapping.toDomain(#emequipdtos)),'eam-EMEquip-Update')")
    @ApiOperation(value = "根据班组批量更新设备档案", tags = {"设备档案" },  notes = "根据班组批量更新设备档案")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMEquipDTO> emequipdtos) {
        List<EMEquip> domainlist=emequipMapping.toDomain(emequipdtos);
        for(EMEquip domain:domainlist){
            domain.setRteamid(pfteam_id);
        }
        emequipService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emequipService.get(#emequip_id),'eam-EMEquip-Remove')")
    @ApiOperation(value = "根据班组删除设备档案", tags = {"设备档案" },  notes = "根据班组删除设备档案")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}")
    public ResponseEntity<Boolean> removeByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emequipService.remove(emequip_id));
    }

    @PreAuthorize("hasPermission(this.emequipService.getEmequipByIds(#ids),'eam-EMEquip-Remove')")
    @ApiOperation(value = "根据班组批量删除设备档案", tags = {"设备档案" },  notes = "根据班组批量删除设备档案")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeam(@RequestBody List<String> ids) {
        emequipService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emequipMapping.toDomain(returnObject.body),'eam-EMEquip-Get')")
    @ApiOperation(value = "根据班组获取设备档案", tags = {"设备档案" },  notes = "根据班组获取设备档案")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}")
    public ResponseEntity<EMEquipDTO> getByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id) {
        EMEquip domain = emequipService.get(emequip_id);
        EMEquipDTO dto = emequipMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组获取设备档案草稿", tags = {"设备档案" },  notes = "根据班组获取设备档案草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/getdraft")
    public ResponseEntity<EMEquipDTO> getDraftByPFTeam(@PathVariable("pfteam_id") String pfteam_id) {
        EMEquip domain = new EMEquip();
        domain.setRteamid(pfteam_id);
        return ResponseEntity.status(HttpStatus.OK).body(emequipMapping.toDto(emequipService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组检查设备档案", tags = {"设备档案" },  notes = "根据班组检查设备档案")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMEquipDTO emequipdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emequipService.checkKey(emequipMapping.toDomain(emequipdto)));
    }

    @PreAuthorize("hasPermission(this.emequipMapping.toDomain(#emequipdto),'eam-EMEquip-Save')")
    @ApiOperation(value = "根据班组保存设备档案", tags = {"设备档案" },  notes = "根据班组保存设备档案")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/save")
    public ResponseEntity<Boolean> saveByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMEquipDTO emequipdto) {
        EMEquip domain = emequipMapping.toDomain(emequipdto);
        domain.setRteamid(pfteam_id);
        return ResponseEntity.status(HttpStatus.OK).body(emequipService.save(domain));
    }

    @PreAuthorize("hasPermission(this.emequipMapping.toDomain(#emequipdtos),'eam-EMEquip-Save')")
    @ApiOperation(value = "根据班组批量保存设备档案", tags = {"设备档案" },  notes = "根据班组批量保存设备档案")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMEquipDTO> emequipdtos) {
        List<EMEquip> domainlist=emequipMapping.toDomain(emequipdtos);
        for(EMEquip domain:domainlist){
             domain.setRteamid(pfteam_id);
        }
        emequipService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEquip-searchEQTypeTree-all') and hasPermission(#context,'eam-EMEquip-Get')")
	@ApiOperation(value = "根据班组获取类型下设备", tags = {"设备档案" } ,notes = "根据班组获取类型下设备")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/fetcheqtypetree")
	public ResponseEntity<List<EMEquipDTO>> fetchEMEquipEQTypeTreeByPFTeam(@PathVariable("pfteam_id") String pfteam_id,EMEquipSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMEquip> domains = emequipService.searchEQTypeTree(context) ;
        List<EMEquipDTO> list = emequipMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEquip-searchEQTypeTree-all') and hasPermission(#context,'eam-EMEquip-Get')")
	@ApiOperation(value = "根据班组查询类型下设备", tags = {"设备档案" } ,notes = "根据班组查询类型下设备")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/searcheqtypetree")
	public ResponseEntity<Page<EMEquipDTO>> searchEMEquipEQTypeTreeByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMEquipSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<EMEquip> domains = emequipService.searchEQTypeTree(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emequipMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据班组获取各类型设备数量", tags = {"设备档案" } ,notes = "根据班组获取各类型设备数量")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/fetchtypeeqnum")
	public ResponseEntity<List<HashMap>> fetchEMEquipTypeEQNumByPFTeam(@PathVariable("pfteam_id") String pfteam_id,EMEquipSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<HashMap> domains = emequipService.searchTypeEQNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据班组查询各类型设备数量", tags = {"设备档案" } ,notes = "根据班组查询各类型设备数量")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/searchtypeeqnum")
	public ResponseEntity<Page<HashMap>> searchEMEquipTypeEQNumByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMEquipSearchContext context) {
        context.setN_rteamid_eq(pfteam_id);
        Page<HashMap> domains = emequipService.searchTypeEQNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
}

