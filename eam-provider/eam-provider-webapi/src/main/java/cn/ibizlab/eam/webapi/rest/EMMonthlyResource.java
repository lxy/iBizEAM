package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMMonthly;
import cn.ibizlab.eam.core.eam_core.service.IEMMonthlyService;
import cn.ibizlab.eam.core.eam_core.filter.EMMonthlySearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"维修中心月度计划" })
@RestController("WebApi-emmonthly")
@RequestMapping("")
public class EMMonthlyResource {

    @Autowired
    public IEMMonthlyService emmonthlyService;

    @Autowired
    @Lazy
    public EMMonthlyMapping emmonthlyMapping;

    @PreAuthorize("hasPermission(this.emmonthlyMapping.toDomain(#emmonthlydto),'eam-EMMonthly-Create')")
    @ApiOperation(value = "新建维修中心月度计划", tags = {"维修中心月度计划" },  notes = "新建维修中心月度计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emmonthlies")
    public ResponseEntity<EMMonthlyDTO> create(@Validated @RequestBody EMMonthlyDTO emmonthlydto) {
        EMMonthly domain = emmonthlyMapping.toDomain(emmonthlydto);
		emmonthlyService.create(domain);
        EMMonthlyDTO dto = emmonthlyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emmonthlyMapping.toDomain(#emmonthlydtos),'eam-EMMonthly-Create')")
    @ApiOperation(value = "批量新建维修中心月度计划", tags = {"维修中心月度计划" },  notes = "批量新建维修中心月度计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emmonthlies/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMMonthlyDTO> emmonthlydtos) {
        emmonthlyService.createBatch(emmonthlyMapping.toDomain(emmonthlydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emmonthly" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emmonthlyService.get(#emmonthly_id),'eam-EMMonthly-Update')")
    @ApiOperation(value = "更新维修中心月度计划", tags = {"维修中心月度计划" },  notes = "更新维修中心月度计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emmonthlies/{emmonthly_id}")
    public ResponseEntity<EMMonthlyDTO> update(@PathVariable("emmonthly_id") String emmonthly_id, @RequestBody EMMonthlyDTO emmonthlydto) {
		EMMonthly domain  = emmonthlyMapping.toDomain(emmonthlydto);
        domain .setEmmonthlyid(emmonthly_id);
		emmonthlyService.update(domain );
		EMMonthlyDTO dto = emmonthlyMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emmonthlyService.getEmmonthlyByEntities(this.emmonthlyMapping.toDomain(#emmonthlydtos)),'eam-EMMonthly-Update')")
    @ApiOperation(value = "批量更新维修中心月度计划", tags = {"维修中心月度计划" },  notes = "批量更新维修中心月度计划")
	@RequestMapping(method = RequestMethod.PUT, value = "/emmonthlies/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMMonthlyDTO> emmonthlydtos) {
        emmonthlyService.updateBatch(emmonthlyMapping.toDomain(emmonthlydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emmonthlyService.get(#emmonthly_id),'eam-EMMonthly-Remove')")
    @ApiOperation(value = "删除维修中心月度计划", tags = {"维修中心月度计划" },  notes = "删除维修中心月度计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emmonthlies/{emmonthly_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emmonthly_id") String emmonthly_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emmonthlyService.remove(emmonthly_id));
    }

    @PreAuthorize("hasPermission(this.emmonthlyService.getEmmonthlyByIds(#ids),'eam-EMMonthly-Remove')")
    @ApiOperation(value = "批量删除维修中心月度计划", tags = {"维修中心月度计划" },  notes = "批量删除维修中心月度计划")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emmonthlies/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emmonthlyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emmonthlyMapping.toDomain(returnObject.body),'eam-EMMonthly-Get')")
    @ApiOperation(value = "获取维修中心月度计划", tags = {"维修中心月度计划" },  notes = "获取维修中心月度计划")
	@RequestMapping(method = RequestMethod.GET, value = "/emmonthlies/{emmonthly_id}")
    public ResponseEntity<EMMonthlyDTO> get(@PathVariable("emmonthly_id") String emmonthly_id) {
        EMMonthly domain = emmonthlyService.get(emmonthly_id);
        EMMonthlyDTO dto = emmonthlyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取维修中心月度计划草稿", tags = {"维修中心月度计划" },  notes = "获取维修中心月度计划草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emmonthlies/getdraft")
    public ResponseEntity<EMMonthlyDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emmonthlyMapping.toDto(emmonthlyService.getDraft(new EMMonthly())));
    }

    @ApiOperation(value = "检查维修中心月度计划", tags = {"维修中心月度计划" },  notes = "检查维修中心月度计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emmonthlies/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMMonthlyDTO emmonthlydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emmonthlyService.checkKey(emmonthlyMapping.toDomain(emmonthlydto)));
    }

    @PreAuthorize("hasPermission(this.emmonthlyMapping.toDomain(#emmonthlydto),'eam-EMMonthly-Save')")
    @ApiOperation(value = "保存维修中心月度计划", tags = {"维修中心月度计划" },  notes = "保存维修中心月度计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emmonthlies/save")
    public ResponseEntity<Boolean> save(@RequestBody EMMonthlyDTO emmonthlydto) {
        return ResponseEntity.status(HttpStatus.OK).body(emmonthlyService.save(emmonthlyMapping.toDomain(emmonthlydto)));
    }

    @PreAuthorize("hasPermission(this.emmonthlyMapping.toDomain(#emmonthlydtos),'eam-EMMonthly-Save')")
    @ApiOperation(value = "批量保存维修中心月度计划", tags = {"维修中心月度计划" },  notes = "批量保存维修中心月度计划")
	@RequestMapping(method = RequestMethod.POST, value = "/emmonthlies/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMMonthlyDTO> emmonthlydtos) {
        emmonthlyService.saveBatch(emmonthlyMapping.toDomain(emmonthlydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMMonthly-searchDefault-all') and hasPermission(#context,'eam-EMMonthly-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"维修中心月度计划" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emmonthlies/fetchdefault")
	public ResponseEntity<List<EMMonthlyDTO>> fetchDefault(EMMonthlySearchContext context) {
        Page<EMMonthly> domains = emmonthlyService.searchDefault(context) ;
        List<EMMonthlyDTO> list = emmonthlyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMMonthly-searchDefault-all') and hasPermission(#context,'eam-EMMonthly-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"维修中心月度计划" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emmonthlies/searchdefault")
	public ResponseEntity<Page<EMMonthlyDTO>> searchDefault(@RequestBody EMMonthlySearchContext context) {
        Page<EMMonthly> domains = emmonthlyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emmonthlyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

