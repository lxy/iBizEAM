package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEITIRes;
import cn.ibizlab.eam.core.eam_core.service.IEMEITIResService;
import cn.ibizlab.eam.core.eam_core.filter.EMEITIResSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"轮胎清单" })
@RestController("WebApi-emeitires")
@RequestMapping("")
public class EMEITIResResource {

    @Autowired
    public IEMEITIResService emeitiresService;

    @Autowired
    @Lazy
    public EMEITIResMapping emeitiresMapping;

    @PreAuthorize("hasPermission(this.emeitiresMapping.toDomain(#emeitiresdto),'eam-EMEITIRes-Create')")
    @ApiOperation(value = "新建轮胎清单", tags = {"轮胎清单" },  notes = "新建轮胎清单")
	@RequestMapping(method = RequestMethod.POST, value = "/emeitires")
    public ResponseEntity<EMEITIResDTO> create(@Validated @RequestBody EMEITIResDTO emeitiresdto) {
        EMEITIRes domain = emeitiresMapping.toDomain(emeitiresdto);
		emeitiresService.create(domain);
        EMEITIResDTO dto = emeitiresMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeitiresMapping.toDomain(#emeitiresdtos),'eam-EMEITIRes-Create')")
    @ApiOperation(value = "批量新建轮胎清单", tags = {"轮胎清单" },  notes = "批量新建轮胎清单")
	@RequestMapping(method = RequestMethod.POST, value = "/emeitires/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEITIResDTO> emeitiresdtos) {
        emeitiresService.createBatch(emeitiresMapping.toDomain(emeitiresdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeitires" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeitiresService.get(#emeitires_id),'eam-EMEITIRes-Update')")
    @ApiOperation(value = "更新轮胎清单", tags = {"轮胎清单" },  notes = "更新轮胎清单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeitires/{emeitires_id}")
    public ResponseEntity<EMEITIResDTO> update(@PathVariable("emeitires_id") String emeitires_id, @RequestBody EMEITIResDTO emeitiresdto) {
		EMEITIRes domain  = emeitiresMapping.toDomain(emeitiresdto);
        domain .setEmeitiresid(emeitires_id);
		emeitiresService.update(domain );
		EMEITIResDTO dto = emeitiresMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeitiresService.getEmeitiresByEntities(this.emeitiresMapping.toDomain(#emeitiresdtos)),'eam-EMEITIRes-Update')")
    @ApiOperation(value = "批量更新轮胎清单", tags = {"轮胎清单" },  notes = "批量更新轮胎清单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeitires/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEITIResDTO> emeitiresdtos) {
        emeitiresService.updateBatch(emeitiresMapping.toDomain(emeitiresdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeitiresService.get(#emeitires_id),'eam-EMEITIRes-Remove')")
    @ApiOperation(value = "删除轮胎清单", tags = {"轮胎清单" },  notes = "删除轮胎清单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeitires/{emeitires_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeitires_id") String emeitires_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeitiresService.remove(emeitires_id));
    }

    @PreAuthorize("hasPermission(this.emeitiresService.getEmeitiresByIds(#ids),'eam-EMEITIRes-Remove')")
    @ApiOperation(value = "批量删除轮胎清单", tags = {"轮胎清单" },  notes = "批量删除轮胎清单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeitires/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeitiresService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeitiresMapping.toDomain(returnObject.body),'eam-EMEITIRes-Get')")
    @ApiOperation(value = "获取轮胎清单", tags = {"轮胎清单" },  notes = "获取轮胎清单")
	@RequestMapping(method = RequestMethod.GET, value = "/emeitires/{emeitires_id}")
    public ResponseEntity<EMEITIResDTO> get(@PathVariable("emeitires_id") String emeitires_id) {
        EMEITIRes domain = emeitiresService.get(emeitires_id);
        EMEITIResDTO dto = emeitiresMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取轮胎清单草稿", tags = {"轮胎清单" },  notes = "获取轮胎清单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeitires/getdraft")
    public ResponseEntity<EMEITIResDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emeitiresMapping.toDto(emeitiresService.getDraft(new EMEITIRes())));
    }

    @ApiOperation(value = "检查轮胎清单", tags = {"轮胎清单" },  notes = "检查轮胎清单")
	@RequestMapping(method = RequestMethod.POST, value = "/emeitires/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEITIResDTO emeitiresdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeitiresService.checkKey(emeitiresMapping.toDomain(emeitiresdto)));
    }

    @PreAuthorize("hasPermission(this.emeitiresMapping.toDomain(#emeitiresdto),'eam-EMEITIRes-Save')")
    @ApiOperation(value = "保存轮胎清单", tags = {"轮胎清单" },  notes = "保存轮胎清单")
	@RequestMapping(method = RequestMethod.POST, value = "/emeitires/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEITIResDTO emeitiresdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emeitiresService.save(emeitiresMapping.toDomain(emeitiresdto)));
    }

    @PreAuthorize("hasPermission(this.emeitiresMapping.toDomain(#emeitiresdtos),'eam-EMEITIRes-Save')")
    @ApiOperation(value = "批量保存轮胎清单", tags = {"轮胎清单" },  notes = "批量保存轮胎清单")
	@RequestMapping(method = RequestMethod.POST, value = "/emeitires/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEITIResDTO> emeitiresdtos) {
        emeitiresService.saveBatch(emeitiresMapping.toDomain(emeitiresdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEITIRes-searchDefault-all') and hasPermission(#context,'eam-EMEITIRes-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"轮胎清单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeitires/fetchdefault")
	public ResponseEntity<List<EMEITIResDTO>> fetchDefault(EMEITIResSearchContext context) {
        Page<EMEITIRes> domains = emeitiresService.searchDefault(context) ;
        List<EMEITIResDTO> list = emeitiresMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEITIRes-searchDefault-all') and hasPermission(#context,'eam-EMEITIRes-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"轮胎清单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeitires/searchdefault")
	public ResponseEntity<Page<EMEITIResDTO>> searchDefault(@RequestBody EMEITIResSearchContext context) {
        Page<EMEITIRes> domains = emeitiresService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeitiresMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

