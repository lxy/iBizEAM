package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEICellSetup;
import cn.ibizlab.eam.core.eam_core.service.IEMEICellSetupService;
import cn.ibizlab.eam.core.eam_core.filter.EMEICellSetupSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"对讲机分发记录" })
@RestController("WebApi-emeicellsetup")
@RequestMapping("")
public class EMEICellSetupResource {

    @Autowired
    public IEMEICellSetupService emeicellsetupService;

    @Autowired
    @Lazy
    public EMEICellSetupMapping emeicellsetupMapping;

    @PreAuthorize("hasPermission(this.emeicellsetupMapping.toDomain(#emeicellsetupdto),'eam-EMEICellSetup-Create')")
    @ApiOperation(value = "新建对讲机分发记录", tags = {"对讲机分发记录" },  notes = "新建对讲机分发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicellsetups")
    public ResponseEntity<EMEICellSetupDTO> create(@Validated @RequestBody EMEICellSetupDTO emeicellsetupdto) {
        EMEICellSetup domain = emeicellsetupMapping.toDomain(emeicellsetupdto);
		emeicellsetupService.create(domain);
        EMEICellSetupDTO dto = emeicellsetupMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeicellsetupMapping.toDomain(#emeicellsetupdtos),'eam-EMEICellSetup-Create')")
    @ApiOperation(value = "批量新建对讲机分发记录", tags = {"对讲机分发记录" },  notes = "批量新建对讲机分发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicellsetups/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEICellSetupDTO> emeicellsetupdtos) {
        emeicellsetupService.createBatch(emeicellsetupMapping.toDomain(emeicellsetupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeicellsetup" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeicellsetupService.get(#emeicellsetup_id),'eam-EMEICellSetup-Update')")
    @ApiOperation(value = "更新对讲机分发记录", tags = {"对讲机分发记录" },  notes = "更新对讲机分发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeicellsetups/{emeicellsetup_id}")
    public ResponseEntity<EMEICellSetupDTO> update(@PathVariable("emeicellsetup_id") String emeicellsetup_id, @RequestBody EMEICellSetupDTO emeicellsetupdto) {
		EMEICellSetup domain  = emeicellsetupMapping.toDomain(emeicellsetupdto);
        domain .setEmeicellsetupid(emeicellsetup_id);
		emeicellsetupService.update(domain );
		EMEICellSetupDTO dto = emeicellsetupMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeicellsetupService.getEmeicellsetupByEntities(this.emeicellsetupMapping.toDomain(#emeicellsetupdtos)),'eam-EMEICellSetup-Update')")
    @ApiOperation(value = "批量更新对讲机分发记录", tags = {"对讲机分发记录" },  notes = "批量更新对讲机分发记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeicellsetups/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEICellSetupDTO> emeicellsetupdtos) {
        emeicellsetupService.updateBatch(emeicellsetupMapping.toDomain(emeicellsetupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeicellsetupService.get(#emeicellsetup_id),'eam-EMEICellSetup-Remove')")
    @ApiOperation(value = "删除对讲机分发记录", tags = {"对讲机分发记录" },  notes = "删除对讲机分发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeicellsetups/{emeicellsetup_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeicellsetup_id") String emeicellsetup_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeicellsetupService.remove(emeicellsetup_id));
    }

    @PreAuthorize("hasPermission(this.emeicellsetupService.getEmeicellsetupByIds(#ids),'eam-EMEICellSetup-Remove')")
    @ApiOperation(value = "批量删除对讲机分发记录", tags = {"对讲机分发记录" },  notes = "批量删除对讲机分发记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeicellsetups/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeicellsetupService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeicellsetupMapping.toDomain(returnObject.body),'eam-EMEICellSetup-Get')")
    @ApiOperation(value = "获取对讲机分发记录", tags = {"对讲机分发记录" },  notes = "获取对讲机分发记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emeicellsetups/{emeicellsetup_id}")
    public ResponseEntity<EMEICellSetupDTO> get(@PathVariable("emeicellsetup_id") String emeicellsetup_id) {
        EMEICellSetup domain = emeicellsetupService.get(emeicellsetup_id);
        EMEICellSetupDTO dto = emeicellsetupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取对讲机分发记录草稿", tags = {"对讲机分发记录" },  notes = "获取对讲机分发记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeicellsetups/getdraft")
    public ResponseEntity<EMEICellSetupDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emeicellsetupMapping.toDto(emeicellsetupService.getDraft(new EMEICellSetup())));
    }

    @ApiOperation(value = "检查对讲机分发记录", tags = {"对讲机分发记录" },  notes = "检查对讲机分发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicellsetups/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEICellSetupDTO emeicellsetupdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeicellsetupService.checkKey(emeicellsetupMapping.toDomain(emeicellsetupdto)));
    }

    @PreAuthorize("hasPermission(this.emeicellsetupMapping.toDomain(#emeicellsetupdto),'eam-EMEICellSetup-Save')")
    @ApiOperation(value = "保存对讲机分发记录", tags = {"对讲机分发记录" },  notes = "保存对讲机分发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicellsetups/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEICellSetupDTO emeicellsetupdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emeicellsetupService.save(emeicellsetupMapping.toDomain(emeicellsetupdto)));
    }

    @PreAuthorize("hasPermission(this.emeicellsetupMapping.toDomain(#emeicellsetupdtos),'eam-EMEICellSetup-Save')")
    @ApiOperation(value = "批量保存对讲机分发记录", tags = {"对讲机分发记录" },  notes = "批量保存对讲机分发记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicellsetups/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEICellSetupDTO> emeicellsetupdtos) {
        emeicellsetupService.saveBatch(emeicellsetupMapping.toDomain(emeicellsetupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEICellSetup-searchDefault-all') and hasPermission(#context,'eam-EMEICellSetup-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"对讲机分发记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeicellsetups/fetchdefault")
	public ResponseEntity<List<EMEICellSetupDTO>> fetchDefault(EMEICellSetupSearchContext context) {
        Page<EMEICellSetup> domains = emeicellsetupService.searchDefault(context) ;
        List<EMEICellSetupDTO> list = emeicellsetupMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEICellSetup-searchDefault-all') and hasPermission(#context,'eam-EMEICellSetup-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"对讲机分发记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeicellsetups/searchdefault")
	public ResponseEntity<Page<EMEICellSetupDTO>> searchDefault(@RequestBody EMEICellSetupSearchContext context) {
        Page<EMEICellSetup> domains = emeicellsetupService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeicellsetupMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

