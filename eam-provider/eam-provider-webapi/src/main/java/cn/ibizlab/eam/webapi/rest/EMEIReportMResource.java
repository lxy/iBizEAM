package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEIReportM;
import cn.ibizlab.eam.core.eam_core.service.IEMEIReportMService;
import cn.ibizlab.eam.core.eam_core.filter.EMEIReportMSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"月度工作报告" })
@RestController("WebApi-emeireportm")
@RequestMapping("")
public class EMEIReportMResource {

    @Autowired
    public IEMEIReportMService emeireportmService;

    @Autowired
    @Lazy
    public EMEIReportMMapping emeireportmMapping;

    @PreAuthorize("hasPermission(this.emeireportmMapping.toDomain(#emeireportmdto),'eam-EMEIReportM-Create')")
    @ApiOperation(value = "新建月度工作报告", tags = {"月度工作报告" },  notes = "新建月度工作报告")
	@RequestMapping(method = RequestMethod.POST, value = "/emeireportms")
    public ResponseEntity<EMEIReportMDTO> create(@Validated @RequestBody EMEIReportMDTO emeireportmdto) {
        EMEIReportM domain = emeireportmMapping.toDomain(emeireportmdto);
		emeireportmService.create(domain);
        EMEIReportMDTO dto = emeireportmMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeireportmMapping.toDomain(#emeireportmdtos),'eam-EMEIReportM-Create')")
    @ApiOperation(value = "批量新建月度工作报告", tags = {"月度工作报告" },  notes = "批量新建月度工作报告")
	@RequestMapping(method = RequestMethod.POST, value = "/emeireportms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEIReportMDTO> emeireportmdtos) {
        emeireportmService.createBatch(emeireportmMapping.toDomain(emeireportmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeireportm" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeireportmService.get(#emeireportm_id),'eam-EMEIReportM-Update')")
    @ApiOperation(value = "更新月度工作报告", tags = {"月度工作报告" },  notes = "更新月度工作报告")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeireportms/{emeireportm_id}")
    public ResponseEntity<EMEIReportMDTO> update(@PathVariable("emeireportm_id") String emeireportm_id, @RequestBody EMEIReportMDTO emeireportmdto) {
		EMEIReportM domain  = emeireportmMapping.toDomain(emeireportmdto);
        domain .setEmeireportmid(emeireportm_id);
		emeireportmService.update(domain );
		EMEIReportMDTO dto = emeireportmMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeireportmService.getEmeireportmByEntities(this.emeireportmMapping.toDomain(#emeireportmdtos)),'eam-EMEIReportM-Update')")
    @ApiOperation(value = "批量更新月度工作报告", tags = {"月度工作报告" },  notes = "批量更新月度工作报告")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeireportms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEIReportMDTO> emeireportmdtos) {
        emeireportmService.updateBatch(emeireportmMapping.toDomain(emeireportmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeireportmService.get(#emeireportm_id),'eam-EMEIReportM-Remove')")
    @ApiOperation(value = "删除月度工作报告", tags = {"月度工作报告" },  notes = "删除月度工作报告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeireportms/{emeireportm_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeireportm_id") String emeireportm_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeireportmService.remove(emeireportm_id));
    }

    @PreAuthorize("hasPermission(this.emeireportmService.getEmeireportmByIds(#ids),'eam-EMEIReportM-Remove')")
    @ApiOperation(value = "批量删除月度工作报告", tags = {"月度工作报告" },  notes = "批量删除月度工作报告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeireportms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeireportmService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeireportmMapping.toDomain(returnObject.body),'eam-EMEIReportM-Get')")
    @ApiOperation(value = "获取月度工作报告", tags = {"月度工作报告" },  notes = "获取月度工作报告")
	@RequestMapping(method = RequestMethod.GET, value = "/emeireportms/{emeireportm_id}")
    public ResponseEntity<EMEIReportMDTO> get(@PathVariable("emeireportm_id") String emeireportm_id) {
        EMEIReportM domain = emeireportmService.get(emeireportm_id);
        EMEIReportMDTO dto = emeireportmMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取月度工作报告草稿", tags = {"月度工作报告" },  notes = "获取月度工作报告草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeireportms/getdraft")
    public ResponseEntity<EMEIReportMDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emeireportmMapping.toDto(emeireportmService.getDraft(new EMEIReportM())));
    }

    @ApiOperation(value = "检查月度工作报告", tags = {"月度工作报告" },  notes = "检查月度工作报告")
	@RequestMapping(method = RequestMethod.POST, value = "/emeireportms/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEIReportMDTO emeireportmdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeireportmService.checkKey(emeireportmMapping.toDomain(emeireportmdto)));
    }

    @PreAuthorize("hasPermission(this.emeireportmMapping.toDomain(#emeireportmdto),'eam-EMEIReportM-Save')")
    @ApiOperation(value = "保存月度工作报告", tags = {"月度工作报告" },  notes = "保存月度工作报告")
	@RequestMapping(method = RequestMethod.POST, value = "/emeireportms/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEIReportMDTO emeireportmdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emeireportmService.save(emeireportmMapping.toDomain(emeireportmdto)));
    }

    @PreAuthorize("hasPermission(this.emeireportmMapping.toDomain(#emeireportmdtos),'eam-EMEIReportM-Save')")
    @ApiOperation(value = "批量保存月度工作报告", tags = {"月度工作报告" },  notes = "批量保存月度工作报告")
	@RequestMapping(method = RequestMethod.POST, value = "/emeireportms/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEIReportMDTO> emeireportmdtos) {
        emeireportmService.saveBatch(emeireportmMapping.toDomain(emeireportmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEIReportM-searchDefault-all') and hasPermission(#context,'eam-EMEIReportM-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"月度工作报告" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeireportms/fetchdefault")
	public ResponseEntity<List<EMEIReportMDTO>> fetchDefault(EMEIReportMSearchContext context) {
        Page<EMEIReportM> domains = emeireportmService.searchDefault(context) ;
        List<EMEIReportMDTO> list = emeireportmMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEIReportM-searchDefault-all') and hasPermission(#context,'eam-EMEIReportM-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"月度工作报告" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeireportms/searchdefault")
	public ResponseEntity<Page<EMEIReportMDTO>> searchDefault(@RequestBody EMEIReportMSearchContext context) {
        Page<EMEIReportM> domains = emeireportmService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeireportmMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

