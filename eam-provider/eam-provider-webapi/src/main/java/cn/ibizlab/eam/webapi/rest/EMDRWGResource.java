package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMDRWG;
import cn.ibizlab.eam.core.eam_core.service.IEMDRWGService;
import cn.ibizlab.eam.core.eam_core.filter.EMDRWGSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"文档" })
@RestController("WebApi-emdrwg")
@RequestMapping("")
public class EMDRWGResource {

    @Autowired
    public IEMDRWGService emdrwgService;

    @Autowired
    @Lazy
    public EMDRWGMapping emdrwgMapping;

    @PreAuthorize("hasPermission(this.emdrwgMapping.toDomain(#emdrwgdto),'eam-EMDRWG-Create')")
    @ApiOperation(value = "新建文档", tags = {"文档" },  notes = "新建文档")
	@RequestMapping(method = RequestMethod.POST, value = "/emdrwgs")
    public ResponseEntity<EMDRWGDTO> create(@Validated @RequestBody EMDRWGDTO emdrwgdto) {
        EMDRWG domain = emdrwgMapping.toDomain(emdrwgdto);
		emdrwgService.create(domain);
        EMDRWGDTO dto = emdrwgMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emdrwgMapping.toDomain(#emdrwgdtos),'eam-EMDRWG-Create')")
    @ApiOperation(value = "批量新建文档", tags = {"文档" },  notes = "批量新建文档")
	@RequestMapping(method = RequestMethod.POST, value = "/emdrwgs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMDRWGDTO> emdrwgdtos) {
        emdrwgService.createBatch(emdrwgMapping.toDomain(emdrwgdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emdrwg" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emdrwgService.get(#emdrwg_id),'eam-EMDRWG-Update')")
    @ApiOperation(value = "更新文档", tags = {"文档" },  notes = "更新文档")
	@RequestMapping(method = RequestMethod.PUT, value = "/emdrwgs/{emdrwg_id}")
    public ResponseEntity<EMDRWGDTO> update(@PathVariable("emdrwg_id") String emdrwg_id, @RequestBody EMDRWGDTO emdrwgdto) {
		EMDRWG domain  = emdrwgMapping.toDomain(emdrwgdto);
        domain .setEmdrwgid(emdrwg_id);
		emdrwgService.update(domain );
		EMDRWGDTO dto = emdrwgMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emdrwgService.getEmdrwgByEntities(this.emdrwgMapping.toDomain(#emdrwgdtos)),'eam-EMDRWG-Update')")
    @ApiOperation(value = "批量更新文档", tags = {"文档" },  notes = "批量更新文档")
	@RequestMapping(method = RequestMethod.PUT, value = "/emdrwgs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMDRWGDTO> emdrwgdtos) {
        emdrwgService.updateBatch(emdrwgMapping.toDomain(emdrwgdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emdrwgService.get(#emdrwg_id),'eam-EMDRWG-Remove')")
    @ApiOperation(value = "删除文档", tags = {"文档" },  notes = "删除文档")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emdrwgs/{emdrwg_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emdrwg_id") String emdrwg_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emdrwgService.remove(emdrwg_id));
    }

    @PreAuthorize("hasPermission(this.emdrwgService.getEmdrwgByIds(#ids),'eam-EMDRWG-Remove')")
    @ApiOperation(value = "批量删除文档", tags = {"文档" },  notes = "批量删除文档")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emdrwgs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emdrwgService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emdrwgMapping.toDomain(returnObject.body),'eam-EMDRWG-Get')")
    @ApiOperation(value = "获取文档", tags = {"文档" },  notes = "获取文档")
	@RequestMapping(method = RequestMethod.GET, value = "/emdrwgs/{emdrwg_id}")
    public ResponseEntity<EMDRWGDTO> get(@PathVariable("emdrwg_id") String emdrwg_id) {
        EMDRWG domain = emdrwgService.get(emdrwg_id);
        EMDRWGDTO dto = emdrwgMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取文档草稿", tags = {"文档" },  notes = "获取文档草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emdrwgs/getdraft")
    public ResponseEntity<EMDRWGDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emdrwgMapping.toDto(emdrwgService.getDraft(new EMDRWG())));
    }

    @ApiOperation(value = "检查文档", tags = {"文档" },  notes = "检查文档")
	@RequestMapping(method = RequestMethod.POST, value = "/emdrwgs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMDRWGDTO emdrwgdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emdrwgService.checkKey(emdrwgMapping.toDomain(emdrwgdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMDRWG-GenId-all')")
    @ApiOperation(value = "", tags = {"文档" },  notes = "")
	@RequestMapping(method = RequestMethod.POST, value = "/emdrwgs/{emdrwg_id}/genid")
    public ResponseEntity<EMDRWGDTO> genId(@PathVariable("emdrwg_id") String emdrwg_id, @RequestBody EMDRWGDTO emdrwgdto) {
        EMDRWG domain = emdrwgMapping.toDomain(emdrwgdto);
        domain.setEmdrwgid(emdrwg_id);
        domain = emdrwgService.genId(domain);
        emdrwgdto = emdrwgMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emdrwgdto);
    }

    @PreAuthorize("hasPermission(this.emdrwgMapping.toDomain(#emdrwgdto),'eam-EMDRWG-Save')")
    @ApiOperation(value = "保存文档", tags = {"文档" },  notes = "保存文档")
	@RequestMapping(method = RequestMethod.POST, value = "/emdrwgs/save")
    public ResponseEntity<Boolean> save(@RequestBody EMDRWGDTO emdrwgdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emdrwgService.save(emdrwgMapping.toDomain(emdrwgdto)));
    }

    @PreAuthorize("hasPermission(this.emdrwgMapping.toDomain(#emdrwgdtos),'eam-EMDRWG-Save')")
    @ApiOperation(value = "批量保存文档", tags = {"文档" },  notes = "批量保存文档")
	@RequestMapping(method = RequestMethod.POST, value = "/emdrwgs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMDRWGDTO> emdrwgdtos) {
        emdrwgService.saveBatch(emdrwgMapping.toDomain(emdrwgdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMDRWG-searchDefault-all') and hasPermission(#context,'eam-EMDRWG-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"文档" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emdrwgs/fetchdefault")
	public ResponseEntity<List<EMDRWGDTO>> fetchDefault(EMDRWGSearchContext context) {
        Page<EMDRWG> domains = emdrwgService.searchDefault(context) ;
        List<EMDRWGDTO> list = emdrwgMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMDRWG-searchDefault-all') and hasPermission(#context,'eam-EMDRWG-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"文档" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emdrwgs/searchdefault")
	public ResponseEntity<Page<EMDRWGDTO>> searchDefault(@RequestBody EMDRWGSearchContext context) {
        Page<EMDRWG> domains = emdrwgService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emdrwgMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

