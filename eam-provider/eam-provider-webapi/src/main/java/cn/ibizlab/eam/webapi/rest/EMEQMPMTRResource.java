package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQMPMTR;
import cn.ibizlab.eam.core.eam_core.service.IEMEQMPMTRService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQMPMTRSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"设备仪表读数" })
@RestController("WebApi-emeqmpmtr")
@RequestMapping("")
public class EMEQMPMTRResource {

    @Autowired
    public IEMEQMPMTRService emeqmpmtrService;

    @Autowired
    @Lazy
    public EMEQMPMTRMapping emeqmpmtrMapping;

    @PreAuthorize("hasPermission(this.emeqmpmtrMapping.toDomain(#emeqmpmtrdto),'eam-EMEQMPMTR-Create')")
    @ApiOperation(value = "新建设备仪表读数", tags = {"设备仪表读数" },  notes = "新建设备仪表读数")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmpmtrs")
    public ResponseEntity<EMEQMPMTRDTO> create(@Validated @RequestBody EMEQMPMTRDTO emeqmpmtrdto) {
        EMEQMPMTR domain = emeqmpmtrMapping.toDomain(emeqmpmtrdto);
		emeqmpmtrService.create(domain);
        EMEQMPMTRDTO dto = emeqmpmtrMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqmpmtrMapping.toDomain(#emeqmpmtrdtos),'eam-EMEQMPMTR-Create')")
    @ApiOperation(value = "批量新建设备仪表读数", tags = {"设备仪表读数" },  notes = "批量新建设备仪表读数")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmpmtrs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQMPMTRDTO> emeqmpmtrdtos) {
        emeqmpmtrService.createBatch(emeqmpmtrMapping.toDomain(emeqmpmtrdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqmpmtr" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqmpmtrService.get(#emeqmpmtr_id),'eam-EMEQMPMTR-Update')")
    @ApiOperation(value = "更新设备仪表读数", tags = {"设备仪表读数" },  notes = "更新设备仪表读数")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqmpmtrs/{emeqmpmtr_id}")
    public ResponseEntity<EMEQMPMTRDTO> update(@PathVariable("emeqmpmtr_id") String emeqmpmtr_id, @RequestBody EMEQMPMTRDTO emeqmpmtrdto) {
		EMEQMPMTR domain  = emeqmpmtrMapping.toDomain(emeqmpmtrdto);
        domain .setEmeqmpmtrid(emeqmpmtr_id);
		emeqmpmtrService.update(domain );
		EMEQMPMTRDTO dto = emeqmpmtrMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqmpmtrService.getEmeqmpmtrByEntities(this.emeqmpmtrMapping.toDomain(#emeqmpmtrdtos)),'eam-EMEQMPMTR-Update')")
    @ApiOperation(value = "批量更新设备仪表读数", tags = {"设备仪表读数" },  notes = "批量更新设备仪表读数")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqmpmtrs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQMPMTRDTO> emeqmpmtrdtos) {
        emeqmpmtrService.updateBatch(emeqmpmtrMapping.toDomain(emeqmpmtrdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqmpmtrService.get(#emeqmpmtr_id),'eam-EMEQMPMTR-Remove')")
    @ApiOperation(value = "删除设备仪表读数", tags = {"设备仪表读数" },  notes = "删除设备仪表读数")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqmpmtrs/{emeqmpmtr_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqmpmtr_id") String emeqmpmtr_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqmpmtrService.remove(emeqmpmtr_id));
    }

    @PreAuthorize("hasPermission(this.emeqmpmtrService.getEmeqmpmtrByIds(#ids),'eam-EMEQMPMTR-Remove')")
    @ApiOperation(value = "批量删除设备仪表读数", tags = {"设备仪表读数" },  notes = "批量删除设备仪表读数")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqmpmtrs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqmpmtrService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqmpmtrMapping.toDomain(returnObject.body),'eam-EMEQMPMTR-Get')")
    @ApiOperation(value = "获取设备仪表读数", tags = {"设备仪表读数" },  notes = "获取设备仪表读数")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqmpmtrs/{emeqmpmtr_id}")
    public ResponseEntity<EMEQMPMTRDTO> get(@PathVariable("emeqmpmtr_id") String emeqmpmtr_id) {
        EMEQMPMTR domain = emeqmpmtrService.get(emeqmpmtr_id);
        EMEQMPMTRDTO dto = emeqmpmtrMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取设备仪表读数草稿", tags = {"设备仪表读数" },  notes = "获取设备仪表读数草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqmpmtrs/getdraft")
    public ResponseEntity<EMEQMPMTRDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emeqmpmtrMapping.toDto(emeqmpmtrService.getDraft(new EMEQMPMTR())));
    }

    @ApiOperation(value = "检查设备仪表读数", tags = {"设备仪表读数" },  notes = "检查设备仪表读数")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmpmtrs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQMPMTRDTO emeqmpmtrdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqmpmtrService.checkKey(emeqmpmtrMapping.toDomain(emeqmpmtrdto)));
    }

    @PreAuthorize("hasPermission(this.emeqmpmtrMapping.toDomain(#emeqmpmtrdto),'eam-EMEQMPMTR-Save')")
    @ApiOperation(value = "保存设备仪表读数", tags = {"设备仪表读数" },  notes = "保存设备仪表读数")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmpmtrs/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEQMPMTRDTO emeqmpmtrdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emeqmpmtrService.save(emeqmpmtrMapping.toDomain(emeqmpmtrdto)));
    }

    @PreAuthorize("hasPermission(this.emeqmpmtrMapping.toDomain(#emeqmpmtrdtos),'eam-EMEQMPMTR-Save')")
    @ApiOperation(value = "批量保存设备仪表读数", tags = {"设备仪表读数" },  notes = "批量保存设备仪表读数")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmpmtrs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQMPMTRDTO> emeqmpmtrdtos) {
        emeqmpmtrService.saveBatch(emeqmpmtrMapping.toDomain(emeqmpmtrdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMPMTR-searchDefault-all') and hasPermission(#context,'eam-EMEQMPMTR-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"设备仪表读数" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqmpmtrs/fetchdefault")
	public ResponseEntity<List<EMEQMPMTRDTO>> fetchDefault(EMEQMPMTRSearchContext context) {
        Page<EMEQMPMTR> domains = emeqmpmtrService.searchDefault(context) ;
        List<EMEQMPMTRDTO> list = emeqmpmtrMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMPMTR-searchDefault-all') and hasPermission(#context,'eam-EMEQMPMTR-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"设备仪表读数" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqmpmtrs/searchdefault")
	public ResponseEntity<Page<EMEQMPMTRDTO>> searchDefault(@RequestBody EMEQMPMTRSearchContext context) {
        Page<EMEQMPMTR> domains = emeqmpmtrService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqmpmtrMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

