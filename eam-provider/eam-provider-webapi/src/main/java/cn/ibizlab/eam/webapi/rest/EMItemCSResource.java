package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItemCS;
import cn.ibizlab.eam.core.eam_core.service.IEMItemCSService;
import cn.ibizlab.eam.core.eam_core.filter.EMItemCSSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"库间调整单" })
@RestController("WebApi-emitemcs")
@RequestMapping("")
public class EMItemCSResource {

    @Autowired
    public IEMItemCSService emitemcsService;

    @Autowired
    @Lazy
    public EMItemCSMapping emitemcsMapping;

    @PreAuthorize("hasPermission(this.emitemcsMapping.toDomain(#emitemcsdto),'eam-EMItemCS-Create')")
    @ApiOperation(value = "新建库间调整单", tags = {"库间调整单" },  notes = "新建库间调整单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemcs")
    public ResponseEntity<EMItemCSDTO> create(@Validated @RequestBody EMItemCSDTO emitemcsdto) {
        EMItemCS domain = emitemcsMapping.toDomain(emitemcsdto);
		emitemcsService.create(domain);
        EMItemCSDTO dto = emitemcsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemcsMapping.toDomain(#emitemcsdtos),'eam-EMItemCS-Create')")
    @ApiOperation(value = "批量新建库间调整单", tags = {"库间调整单" },  notes = "批量新建库间调整单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemcs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMItemCSDTO> emitemcsdtos) {
        emitemcsService.createBatch(emitemcsMapping.toDomain(emitemcsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemcs" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemcsService.get(#emitemcs_id),'eam-EMItemCS-Update')")
    @ApiOperation(value = "更新库间调整单", tags = {"库间调整单" },  notes = "更新库间调整单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitemcs/{emitemcs_id}")
    public ResponseEntity<EMItemCSDTO> update(@PathVariable("emitemcs_id") String emitemcs_id, @RequestBody EMItemCSDTO emitemcsdto) {
		EMItemCS domain  = emitemcsMapping.toDomain(emitemcsdto);
        domain .setEmitemcsid(emitemcs_id);
		emitemcsService.update(domain );
		EMItemCSDTO dto = emitemcsMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemcsService.getEmitemcsByEntities(this.emitemcsMapping.toDomain(#emitemcsdtos)),'eam-EMItemCS-Update')")
    @ApiOperation(value = "批量更新库间调整单", tags = {"库间调整单" },  notes = "批量更新库间调整单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitemcs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMItemCSDTO> emitemcsdtos) {
        emitemcsService.updateBatch(emitemcsMapping.toDomain(emitemcsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemcsService.get(#emitemcs_id),'eam-EMItemCS-Remove')")
    @ApiOperation(value = "删除库间调整单", tags = {"库间调整单" },  notes = "删除库间调整单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitemcs/{emitemcs_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emitemcs_id") String emitemcs_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emitemcsService.remove(emitemcs_id));
    }

    @PreAuthorize("hasPermission(this.emitemcsService.getEmitemcsByIds(#ids),'eam-EMItemCS-Remove')")
    @ApiOperation(value = "批量删除库间调整单", tags = {"库间调整单" },  notes = "批量删除库间调整单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitemcs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emitemcsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemcsMapping.toDomain(returnObject.body),'eam-EMItemCS-Get')")
    @ApiOperation(value = "获取库间调整单", tags = {"库间调整单" },  notes = "获取库间调整单")
	@RequestMapping(method = RequestMethod.GET, value = "/emitemcs/{emitemcs_id}")
    public ResponseEntity<EMItemCSDTO> get(@PathVariable("emitemcs_id") String emitemcs_id) {
        EMItemCS domain = emitemcsService.get(emitemcs_id);
        EMItemCSDTO dto = emitemcsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取库间调整单草稿", tags = {"库间调整单" },  notes = "获取库间调整单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emitemcs/getdraft")
    public ResponseEntity<EMItemCSDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emitemcsMapping.toDto(emitemcsService.getDraft(new EMItemCS())));
    }

    @ApiOperation(value = "检查库间调整单", tags = {"库间调整单" },  notes = "检查库间调整单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemcs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMItemCSDTO emitemcsdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemcsService.checkKey(emitemcsMapping.toDomain(emitemcsdto)));
    }

    @PreAuthorize("hasPermission(this.emitemcsMapping.toDomain(#emitemcsdto),'eam-EMItemCS-Save')")
    @ApiOperation(value = "保存库间调整单", tags = {"库间调整单" },  notes = "保存库间调整单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemcs/save")
    public ResponseEntity<Boolean> save(@RequestBody EMItemCSDTO emitemcsdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emitemcsService.save(emitemcsMapping.toDomain(emitemcsdto)));
    }

    @PreAuthorize("hasPermission(this.emitemcsMapping.toDomain(#emitemcsdtos),'eam-EMItemCS-Save')")
    @ApiOperation(value = "批量保存库间调整单", tags = {"库间调整单" },  notes = "批量保存库间调整单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemcs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMItemCSDTO> emitemcsdtos) {
        emitemcsService.saveBatch(emitemcsMapping.toDomain(emitemcsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemCS-searchConfirmed-all') and hasPermission(#context,'eam-EMItemCS-Get')")
	@ApiOperation(value = "获取已确认", tags = {"库间调整单" } ,notes = "获取已确认")
    @RequestMapping(method= RequestMethod.GET , value="/emitemcs/fetchconfirmed")
	public ResponseEntity<List<EMItemCSDTO>> fetchConfirmed(EMItemCSSearchContext context) {
        Page<EMItemCS> domains = emitemcsService.searchConfirmed(context) ;
        List<EMItemCSDTO> list = emitemcsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemCS-searchConfirmed-all') and hasPermission(#context,'eam-EMItemCS-Get')")
	@ApiOperation(value = "查询已确认", tags = {"库间调整单" } ,notes = "查询已确认")
    @RequestMapping(method= RequestMethod.POST , value="/emitemcs/searchconfirmed")
	public ResponseEntity<Page<EMItemCSDTO>> searchConfirmed(@RequestBody EMItemCSSearchContext context) {
        Page<EMItemCS> domains = emitemcsService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemcsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemCS-searchDefault-all') and hasPermission(#context,'eam-EMItemCS-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"库间调整单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitemcs/fetchdefault")
	public ResponseEntity<List<EMItemCSDTO>> fetchDefault(EMItemCSSearchContext context) {
        Page<EMItemCS> domains = emitemcsService.searchDefault(context) ;
        List<EMItemCSDTO> list = emitemcsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemCS-searchDefault-all') and hasPermission(#context,'eam-EMItemCS-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"库间调整单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitemcs/searchdefault")
	public ResponseEntity<Page<EMItemCSDTO>> searchDefault(@RequestBody EMItemCSSearchContext context) {
        Page<EMItemCS> domains = emitemcsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemcsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemCS-searchDraft-all') and hasPermission(#context,'eam-EMItemCS-Get')")
	@ApiOperation(value = "获取草稿", tags = {"库间调整单" } ,notes = "获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emitemcs/fetchdraft")
	public ResponseEntity<List<EMItemCSDTO>> fetchDraft(EMItemCSSearchContext context) {
        Page<EMItemCS> domains = emitemcsService.searchDraft(context) ;
        List<EMItemCSDTO> list = emitemcsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemCS-searchDraft-all') and hasPermission(#context,'eam-EMItemCS-Get')")
	@ApiOperation(value = "查询草稿", tags = {"库间调整单" } ,notes = "查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emitemcs/searchdraft")
	public ResponseEntity<Page<EMItemCSDTO>> searchDraft(@RequestBody EMItemCSSearchContext context) {
        Page<EMItemCS> domains = emitemcsService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemcsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemCS-searchToConfirm-all') and hasPermission(#context,'eam-EMItemCS-Get')")
	@ApiOperation(value = "获取待确认", tags = {"库间调整单" } ,notes = "获取待确认")
    @RequestMapping(method= RequestMethod.GET , value="/emitemcs/fetchtoconfirm")
	public ResponseEntity<List<EMItemCSDTO>> fetchToConfirm(EMItemCSSearchContext context) {
        Page<EMItemCS> domains = emitemcsService.searchToConfirm(context) ;
        List<EMItemCSDTO> list = emitemcsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemCS-searchToConfirm-all') and hasPermission(#context,'eam-EMItemCS-Get')")
	@ApiOperation(value = "查询待确认", tags = {"库间调整单" } ,notes = "查询待确认")
    @RequestMapping(method= RequestMethod.POST , value="/emitemcs/searchtoconfirm")
	public ResponseEntity<Page<EMItemCSDTO>> searchToConfirm(@RequestBody EMItemCSSearchContext context) {
        Page<EMItemCS> domains = emitemcsService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemcsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

