package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_pf.domain.PFDept;
import cn.ibizlab.eam.core.eam_pf.service.IPFDeptService;
import cn.ibizlab.eam.core.eam_pf.filter.PFDeptSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"部门" })
@RestController("WebApi-pfdept")
@RequestMapping("")
public class PFDeptResource {

    @Autowired
    public IPFDeptService pfdeptService;

    @Autowired
    @Lazy
    public PFDeptMapping pfdeptMapping;

    @PreAuthorize("hasPermission(this.pfdeptMapping.toDomain(#pfdeptdto),'eam-PFDept-Create')")
    @ApiOperation(value = "新建部门", tags = {"部门" },  notes = "新建部门")
	@RequestMapping(method = RequestMethod.POST, value = "/pfdepts")
    public ResponseEntity<PFDeptDTO> create(@Validated @RequestBody PFDeptDTO pfdeptdto) {
        PFDept domain = pfdeptMapping.toDomain(pfdeptdto);
		pfdeptService.create(domain);
        PFDeptDTO dto = pfdeptMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pfdeptMapping.toDomain(#pfdeptdtos),'eam-PFDept-Create')")
    @ApiOperation(value = "批量新建部门", tags = {"部门" },  notes = "批量新建部门")
	@RequestMapping(method = RequestMethod.POST, value = "/pfdepts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<PFDeptDTO> pfdeptdtos) {
        pfdeptService.createBatch(pfdeptMapping.toDomain(pfdeptdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "pfdept" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.pfdeptService.get(#pfdept_id),'eam-PFDept-Update')")
    @ApiOperation(value = "更新部门", tags = {"部门" },  notes = "更新部门")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfdepts/{pfdept_id}")
    public ResponseEntity<PFDeptDTO> update(@PathVariable("pfdept_id") String pfdept_id, @RequestBody PFDeptDTO pfdeptdto) {
		PFDept domain  = pfdeptMapping.toDomain(pfdeptdto);
        domain .setPfdeptid(pfdept_id);
		pfdeptService.update(domain );
		PFDeptDTO dto = pfdeptMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pfdeptService.getPfdeptByEntities(this.pfdeptMapping.toDomain(#pfdeptdtos)),'eam-PFDept-Update')")
    @ApiOperation(value = "批量更新部门", tags = {"部门" },  notes = "批量更新部门")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfdepts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<PFDeptDTO> pfdeptdtos) {
        pfdeptService.updateBatch(pfdeptMapping.toDomain(pfdeptdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.pfdeptService.get(#pfdept_id),'eam-PFDept-Remove')")
    @ApiOperation(value = "删除部门", tags = {"部门" },  notes = "删除部门")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfdepts/{pfdept_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("pfdept_id") String pfdept_id) {
         return ResponseEntity.status(HttpStatus.OK).body(pfdeptService.remove(pfdept_id));
    }

    @PreAuthorize("hasPermission(this.pfdeptService.getPfdeptByIds(#ids),'eam-PFDept-Remove')")
    @ApiOperation(value = "批量删除部门", tags = {"部门" },  notes = "批量删除部门")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfdepts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        pfdeptService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.pfdeptMapping.toDomain(returnObject.body),'eam-PFDept-Get')")
    @ApiOperation(value = "获取部门", tags = {"部门" },  notes = "获取部门")
	@RequestMapping(method = RequestMethod.GET, value = "/pfdepts/{pfdept_id}")
    public ResponseEntity<PFDeptDTO> get(@PathVariable("pfdept_id") String pfdept_id) {
        PFDept domain = pfdeptService.get(pfdept_id);
        PFDeptDTO dto = pfdeptMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取部门草稿", tags = {"部门" },  notes = "获取部门草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/pfdepts/getdraft")
    public ResponseEntity<PFDeptDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(pfdeptMapping.toDto(pfdeptService.getDraft(new PFDept())));
    }

    @ApiOperation(value = "检查部门", tags = {"部门" },  notes = "检查部门")
	@RequestMapping(method = RequestMethod.POST, value = "/pfdepts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody PFDeptDTO pfdeptdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(pfdeptService.checkKey(pfdeptMapping.toDomain(pfdeptdto)));
    }

    @PreAuthorize("hasPermission(this.pfdeptMapping.toDomain(#pfdeptdto),'eam-PFDept-Save')")
    @ApiOperation(value = "保存部门", tags = {"部门" },  notes = "保存部门")
	@RequestMapping(method = RequestMethod.POST, value = "/pfdepts/save")
    public ResponseEntity<Boolean> save(@RequestBody PFDeptDTO pfdeptdto) {
        return ResponseEntity.status(HttpStatus.OK).body(pfdeptService.save(pfdeptMapping.toDomain(pfdeptdto)));
    }

    @PreAuthorize("hasPermission(this.pfdeptMapping.toDomain(#pfdeptdtos),'eam-PFDept-Save')")
    @ApiOperation(value = "批量保存部门", tags = {"部门" },  notes = "批量保存部门")
	@RequestMapping(method = RequestMethod.POST, value = "/pfdepts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<PFDeptDTO> pfdeptdtos) {
        pfdeptService.saveBatch(pfdeptMapping.toDomain(pfdeptdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFDept-searchDefault-all') and hasPermission(#context,'eam-PFDept-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"部门" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfdepts/fetchdefault")
	public ResponseEntity<List<PFDeptDTO>> fetchDefault(PFDeptSearchContext context) {
        Page<PFDept> domains = pfdeptService.searchDefault(context) ;
        List<PFDeptDTO> list = pfdeptMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFDept-searchDefault-all') and hasPermission(#context,'eam-PFDept-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"部门" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfdepts/searchdefault")
	public ResponseEntity<Page<PFDeptDTO>> searchDefault(@RequestBody PFDeptSearchContext context) {
        Page<PFDept> domains = pfdeptService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(pfdeptMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFDept-searchTopDept-all') and hasPermission(#context,'eam-PFDept-Get')")
	@ApiOperation(value = "获取顶级部门", tags = {"部门" } ,notes = "获取顶级部门")
    @RequestMapping(method= RequestMethod.GET , value="/pfdepts/fetchtopdept")
	public ResponseEntity<List<PFDeptDTO>> fetchTopDept(PFDeptSearchContext context) {
        Page<PFDept> domains = pfdeptService.searchTopDept(context) ;
        List<PFDeptDTO> list = pfdeptMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFDept-searchTopDept-all') and hasPermission(#context,'eam-PFDept-Get')")
	@ApiOperation(value = "查询顶级部门", tags = {"部门" } ,notes = "查询顶级部门")
    @RequestMapping(method= RequestMethod.POST , value="/pfdepts/searchtopdept")
	public ResponseEntity<Page<PFDeptDTO>> searchTopDept(@RequestBody PFDeptSearchContext context) {
        Page<PFDept> domains = pfdeptService.searchTopDept(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(pfdeptMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

