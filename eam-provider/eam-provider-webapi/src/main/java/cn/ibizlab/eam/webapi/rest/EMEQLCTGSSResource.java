package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTGSS;
import cn.ibizlab.eam.core.eam_core.service.IEMEQLCTGSSService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLCTGSSSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"钢丝绳位置" })
@RestController("WebApi-emeqlctgss")
@RequestMapping("")
public class EMEQLCTGSSResource {

    @Autowired
    public IEMEQLCTGSSService emeqlctgssService;

    @Autowired
    @Lazy
    public EMEQLCTGSSMapping emeqlctgssMapping;

    @PreAuthorize("hasPermission(this.emeqlctgssMapping.toDomain(#emeqlctgssdto),'eam-EMEQLCTGSS-Create')")
    @ApiOperation(value = "新建钢丝绳位置", tags = {"钢丝绳位置" },  notes = "新建钢丝绳位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctgsses")
    public ResponseEntity<EMEQLCTGSSDTO> create(@Validated @RequestBody EMEQLCTGSSDTO emeqlctgssdto) {
        EMEQLCTGSS domain = emeqlctgssMapping.toDomain(emeqlctgssdto);
		emeqlctgssService.create(domain);
        EMEQLCTGSSDTO dto = emeqlctgssMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqlctgssMapping.toDomain(#emeqlctgssdtos),'eam-EMEQLCTGSS-Create')")
    @ApiOperation(value = "批量新建钢丝绳位置", tags = {"钢丝绳位置" },  notes = "批量新建钢丝绳位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctgsses/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQLCTGSSDTO> emeqlctgssdtos) {
        emeqlctgssService.createBatch(emeqlctgssMapping.toDomain(emeqlctgssdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqlctgss" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqlctgssService.get(#emeqlctgss_id),'eam-EMEQLCTGSS-Update')")
    @ApiOperation(value = "更新钢丝绳位置", tags = {"钢丝绳位置" },  notes = "更新钢丝绳位置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqlctgsses/{emeqlctgss_id}")
    public ResponseEntity<EMEQLCTGSSDTO> update(@PathVariable("emeqlctgss_id") String emeqlctgss_id, @RequestBody EMEQLCTGSSDTO emeqlctgssdto) {
		EMEQLCTGSS domain  = emeqlctgssMapping.toDomain(emeqlctgssdto);
        domain .setEmeqlocationid(emeqlctgss_id);
		emeqlctgssService.update(domain );
		EMEQLCTGSSDTO dto = emeqlctgssMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqlctgssService.getEmeqlctgssByEntities(this.emeqlctgssMapping.toDomain(#emeqlctgssdtos)),'eam-EMEQLCTGSS-Update')")
    @ApiOperation(value = "批量更新钢丝绳位置", tags = {"钢丝绳位置" },  notes = "批量更新钢丝绳位置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqlctgsses/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQLCTGSSDTO> emeqlctgssdtos) {
        emeqlctgssService.updateBatch(emeqlctgssMapping.toDomain(emeqlctgssdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqlctgssService.get(#emeqlctgss_id),'eam-EMEQLCTGSS-Remove')")
    @ApiOperation(value = "删除钢丝绳位置", tags = {"钢丝绳位置" },  notes = "删除钢丝绳位置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqlctgsses/{emeqlctgss_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqlctgss_id") String emeqlctgss_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqlctgssService.remove(emeqlctgss_id));
    }

    @PreAuthorize("hasPermission(this.emeqlctgssService.getEmeqlctgssByIds(#ids),'eam-EMEQLCTGSS-Remove')")
    @ApiOperation(value = "批量删除钢丝绳位置", tags = {"钢丝绳位置" },  notes = "批量删除钢丝绳位置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqlctgsses/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqlctgssService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqlctgssMapping.toDomain(returnObject.body),'eam-EMEQLCTGSS-Get')")
    @ApiOperation(value = "获取钢丝绳位置", tags = {"钢丝绳位置" },  notes = "获取钢丝绳位置")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqlctgsses/{emeqlctgss_id}")
    public ResponseEntity<EMEQLCTGSSDTO> get(@PathVariable("emeqlctgss_id") String emeqlctgss_id) {
        EMEQLCTGSS domain = emeqlctgssService.get(emeqlctgss_id);
        EMEQLCTGSSDTO dto = emeqlctgssMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取钢丝绳位置草稿", tags = {"钢丝绳位置" },  notes = "获取钢丝绳位置草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqlctgsses/getdraft")
    public ResponseEntity<EMEQLCTGSSDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(emeqlctgssMapping.toDto(emeqlctgssService.getDraft(new EMEQLCTGSS())));
    }

    @ApiOperation(value = "检查钢丝绳位置", tags = {"钢丝绳位置" },  notes = "检查钢丝绳位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctgsses/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQLCTGSSDTO emeqlctgssdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqlctgssService.checkKey(emeqlctgssMapping.toDomain(emeqlctgssdto)));
    }

    @PreAuthorize("hasPermission(this.emeqlctgssMapping.toDomain(#emeqlctgssdto),'eam-EMEQLCTGSS-Save')")
    @ApiOperation(value = "保存钢丝绳位置", tags = {"钢丝绳位置" },  notes = "保存钢丝绳位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctgsses/save")
    public ResponseEntity<Boolean> save(@RequestBody EMEQLCTGSSDTO emeqlctgssdto) {
        return ResponseEntity.status(HttpStatus.OK).body(emeqlctgssService.save(emeqlctgssMapping.toDomain(emeqlctgssdto)));
    }

    @PreAuthorize("hasPermission(this.emeqlctgssMapping.toDomain(#emeqlctgssdtos),'eam-EMEQLCTGSS-Save')")
    @ApiOperation(value = "批量保存钢丝绳位置", tags = {"钢丝绳位置" },  notes = "批量保存钢丝绳位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlctgsses/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQLCTGSSDTO> emeqlctgssdtos) {
        emeqlctgssService.saveBatch(emeqlctgssMapping.toDomain(emeqlctgssdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTGSS-searchDefault-all') and hasPermission(#context,'eam-EMEQLCTGSS-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"钢丝绳位置" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqlctgsses/fetchdefault")
	public ResponseEntity<List<EMEQLCTGSSDTO>> fetchDefault(EMEQLCTGSSSearchContext context) {
        Page<EMEQLCTGSS> domains = emeqlctgssService.searchDefault(context) ;
        List<EMEQLCTGSSDTO> list = emeqlctgssMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTGSS-searchDefault-all') and hasPermission(#context,'eam-EMEQLCTGSS-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"钢丝绳位置" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqlctgsses/searchdefault")
	public ResponseEntity<Page<EMEQLCTGSSDTO>> searchDefault(@RequestBody EMEQLCTGSSSearchContext context) {
        Page<EMEQLCTGSS> domains = emeqlctgssService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqlctgssMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTGSS-searchMain3-all') and hasPermission(#context,'eam-EMEQLCTGSS-Get')")
	@ApiOperation(value = "获取钢丝绳超期预警查询模型", tags = {"钢丝绳位置" } ,notes = "获取钢丝绳超期预警查询模型")
    @RequestMapping(method= RequestMethod.GET , value="/emeqlctgsses/fetchmain3")
	public ResponseEntity<List<EMEQLCTGSSDTO>> fetchMain3(EMEQLCTGSSSearchContext context) {
        Page<EMEQLCTGSS> domains = emeqlctgssService.searchMain3(context) ;
        List<EMEQLCTGSSDTO> list = emeqlctgssMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTGSS-searchMain3-all') and hasPermission(#context,'eam-EMEQLCTGSS-Get')")
	@ApiOperation(value = "查询钢丝绳超期预警查询模型", tags = {"钢丝绳位置" } ,notes = "查询钢丝绳超期预警查询模型")
    @RequestMapping(method= RequestMethod.POST , value="/emeqlctgsses/searchmain3")
	public ResponseEntity<Page<EMEQLCTGSSDTO>> searchMain3(@RequestBody EMEQLCTGSSSearchContext context) {
        Page<EMEQLCTGSS> domains = emeqlctgssService.searchMain3(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqlctgssMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

