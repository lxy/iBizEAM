package cn.ibizlab.eam.util.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.ibizlab.eam.util.domain.IBZDataAudit;

public interface IBZDataAuditMapper extends BaseMapper<IBZDataAudit> {

}