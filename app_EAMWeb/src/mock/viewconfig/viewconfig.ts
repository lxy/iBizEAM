import { MockAdapter } from '../mock-adapter';
const mock = MockAdapter.getInstance();

import Mock from 'mockjs'

// 获取studio链接数据
mock.onGet('./assets/json/view-config.json').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status,{
                "emitemtypegridview": {
            "title": "物品类型",
            "caption": "物品类型",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeGridView",
            "viewtag": "00751c15423366b8738c166e2afd4e86"
        },
        "emassetpickupgridview": {
            "title": "资产选择表格视图",
            "caption": "资产",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMASSETPickupGridView",
            "viewtag": "0098781f1e1dd58fca268291027efbaf"
        },
        "emitempusedrafteditview9": {
            "title": "领料单",
            "caption": "领料单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPUseDraftEditView9",
            "viewtag": "00b423371c72b5170e6ab04cb7673a4b"
        },
        "emeqsparemapgridview": {
            "title": "备件包应用",
            "caption": "备件包应用",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREMAPGridView",
            "viewtag": "013bc8a5fb55d671563f83ac88cda7f7"
        },
        "emeqlctrhygridview": {
            "title": "润滑油位置",
            "caption": "润滑油位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTRHYGridView",
            "viewtag": "01433ecfad20e482c9228cabe2ee3de0"
        },
        "emstoreparteditview": {
            "title": "仓库库位",
            "caption": "仓库库位",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMSTOREPARTEditView",
            "viewtag": "0145b2180bbcd5c5addc5d077105961a"
        },
        "emrfodetabexpview": {
            "title": "现象",
            "caption": "现象",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODETabExpView",
            "viewtag": "0254c140083d7f8a22e12149d7dbad60"
        },
        "emeqsparelistexpview": {
            "title": "备件包列表导航视图",
            "caption": "备件包",
            "viewtype": "DELISTEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareListExpView",
            "viewtag": "025f0a0a8e56c86995325948bea99078"
        },
        "pfunitgridview": {
            "title": "计量单位",
            "caption": "计量单位",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFUNITGridView",
            "viewtag": "02c94bc50171ee375bd3d44f0b85fe14"
        },
        "emstockbystorepartgridview": {
            "title": "库存明细",
            "caption": "库存明细",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStockByStorePartGridView",
            "viewtag": "03388db28b40135472ea840d73818810"
        },
        "empogridview": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMPOGridView",
            "viewtag": "03675910b6c4640a13c0f4140201046e"
        },
        "emeqlocationtreeexpview": {
            "title": "位置树导航视图",
            "caption": "位置",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLocationTreeExpView",
            "viewtag": "0391631d2c7d270f47bbdbb7a1c39618"
        },
        "emeqkeepeditview9": {
            "title": "保养记录信息",
            "caption": "保养记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQKEEPEditView9",
            "viewtag": "045d66a45dfc2ac249d6c5d4981145a6"
        },
        "pfemppickupgridview": {
            "title": "职员选择表格视图",
            "caption": "职员",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFEmpPickupGridView",
            "viewtag": "04a9ace562bc2e5fa76c7931bd40f889"
        },
        "emplantempleditview": {
            "title": "计划模板",
            "caption": "计划模板",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANTEMPLEditView",
            "viewtag": "06a385b661866fe39c0a912d1944a8cf"
        },
        "emeqtypetreeexpview2": {
            "title": "设备类型",
            "caption": "设备档案",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQTypeTreeExpView2",
            "viewtag": "0749196d8b219c4909a64a917007faef"
        },
        "empodetaileditview9_editmode": {
            "title": "采购订单条目",
            "caption": "采购订单条目",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMPODETAILEditView9_EditMode",
            "viewtag": "07a90ae0be7cfe4493ec62bb6dd2bcd8"
        },
        "emeqlctgsstabexpview": {
            "title": "钢丝绳位置超期预警",
            "caption": "钢丝绳位置超期预警",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTGSSTabExpView",
            "viewtag": "07bd75896dc15dcd49ba5d57e105c4e6"
        },
        "emeqsparedetaileditview": {
            "title": "备件包明细",
            "caption": "备件包明细",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREDETAILEditView",
            "viewtag": "095b356064013d1fe71771b75ca6dbfe"
        },
        "emwplistcosteditview": {
            "title": "询价单编辑视图",
            "caption": "询价单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMWPLISTCOSTEditView",
            "viewtag": "09ad3a83b0e34aac0f60864d0730dda3"
        },
        "emproductgridview": {
            "title": "试用品",
            "caption": "试用品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMPRODUCTGridView",
            "viewtag": "0acaddf8473367f0800e7c37205eb063"
        },
        "emeqsetuppickupview": {
            "title": "更换安装数据选择视图",
            "caption": "更换安装",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQSETUPPickupView",
            "viewtag": "0b88e2e073970a899ed7e2e34d2a84b8"
        },
        "emwplisttreeexpview": {
            "title": "采购申请树导航视图",
            "caption": "采购申请",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListTreeExpView",
            "viewtag": "0cf7f1c1a00db80827f7b36601a03fb9"
        },
        "emitemcseditview9_editmode": {
            "title": "调整单",
            "caption": "调整单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMCSEditView9_EditMode",
            "viewtag": "0e97baefe424809a7afd48fdefa312f9"
        },
        "emeqsetupeditview9_editmode": {
            "title": "安装记录信息",
            "caption": "安装记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_act",
            "viewname": "EMEQSETUPEditView9_EditMode",
            "viewtag": "10a76f2f664ac4b9967df11ddc1d10a7"
        },
        "emitemtradegridview": {
            "title": "物品交易表格视图",
            "caption": "物品交易",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMTRADEGridView",
            "viewtag": "10d99b2c6e650a76eb8cd566376e000b"
        },
        "emeqlcttirespickupgridview": {
            "title": "轮胎位置选择表格视图",
            "caption": "轮胎位置",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTTIRESPickupGridView",
            "viewtag": "112c36fb37d93eeb5173212fe4f4f789"
        },
        "emeqmaintancecaleditview9": {
            "title": "维修记录信息",
            "caption": "维修记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQMaintanceCalEditView9",
            "viewtag": "1389f83d86a1c7f302679aea32f2e39b"
        },
        "emeqcheckeditview9": {
            "title": "检定记录信息",
            "caption": "检定记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQCHECKEditView9",
            "viewtag": "1450ec1c67898c375f9a6990466a5581"
        },
        "emwplistcostfillcostview": {
            "title": "询价单填报",
            "caption": "询价单填报",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListCostFillCostView",
            "viewtag": "14f3354cf669ecf9c5b8714dcf3c0c92"
        },
        "emeqkpkpeditview": {
            "title": "设备关键点编辑视图",
            "caption": "设备关键点",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPKPEditView",
            "viewtag": "15020835f44c628000d71881fdffb87c"
        },
        "emeqkprcdkprcdeditview": {
            "title": "关键点记录编辑视图",
            "caption": "关键点记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPRCDKPRcdEditView",
            "viewtag": "154aa11c89c7f4d37838c29647e965b9"
        },
        "emitemrinwaitingridview": {
            "title": "入库单",
            "caption": "入库单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemRInWaitInGridView",
            "viewtag": "15697f4a3e49536f4e63129008cf2d99"
        },
        "emitemsupplyinfoview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemSupplyInfoView",
            "viewtag": "1583578e985c62c06fa213785443f4db"
        },
        "emwo_dpeditview_editmode": {
            "title": "点检工单",
            "caption": "点检工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_DPEditView_EditMode",
            "viewtag": "165b334c10d4980ba8539260005c32be"
        },
        "emequipeditview_editmode": {
            "title": "基本信息",
            "caption": "基本信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEquipEditView_EditMode",
            "viewtag": "171898e6f4c7a247d735068d4d3cb1b8"
        },
        "emitempltabexpview": {
            "title": "损溢单分页导航视图",
            "caption": "损溢单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPLTabExpView",
            "viewtag": "17ea594695b8fbdb5f5c7c724de3fd59"
        },
        "emeqkppickupgridview": {
            "title": "设备关键点选择表格视图",
            "caption": "设备关键点",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPPickupGridView",
            "viewtag": "1899736ab44809eb0dfa5452b8893a2f"
        },
        "emdrwgeditview": {
            "title": "文档",
            "caption": "文档",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMDRWGEditView",
            "viewtag": "18bedf17550dca06530d8d1470b788e9"
        },
        "emrfodegridview": {
            "title": "现象",
            "caption": "现象",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFODEGridView",
            "viewtag": "193cf07f08f6448c6166184c8081c2aa"
        },
        "emitemrineditview": {
            "title": "入库单编辑视图",
            "caption": "入库单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMRINEditView",
            "viewtag": "1a4bf5fbe641c5ae04aabc16b4950ebb"
        },
        "emassetgridview_bf": {
            "title": "报废资产",
            "caption": "报废资产",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETGridView_BF",
            "viewtag": "1b788edc55f8d8c2d1c4b298adc182df"
        },
        "emeqsetupeditview9": {
            "title": "安装记录信息",
            "caption": "安装记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSETUPEditView9",
            "viewtag": "1b9e62f46991d036786cdceec5288d6e"
        },
        "emwplisteditview": {
            "title": "采购申请编辑视图",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMWPLISTEditView",
            "viewtag": "1bb337d8433c77da88f187b1eb0db265"
        },
        "emoutputeditview9": {
            "title": "能力信息",
            "caption": "能力信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMOUTPUTEditView9",
            "viewtag": "1c1a2a1eb45370f331f8d5355ca7f695"
        },
        "emitemprtneditview9_editmode": {
            "title": "还料单信息",
            "caption": "还料单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMPRTNEditView9_EditMode",
            "viewtag": "1c6a59a68c904d83c9b313a3ae66edb8"
        },
        "emitempltoconfirmgridview": {
            "title": "损溢单",
            "caption": "损溢单-待确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPLToConfirmGridView",
            "viewtag": "1ce1c362087be1919131bbef7c36d043"
        },
        "emeqkeepgridview": {
            "title": "保养记录",
            "caption": "保养记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQKeepGridView",
            "viewtag": "1d0eac0c4dfb6dd669b494b444701806"
        },
        "emrfomopickupgridview": {
            "title": "模式选择表格视图",
            "caption": "模式",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOMOPickupGridView",
            "viewtag": "1d107adacfe3f22a02aff712b12c1116"
        },
        "emservicepickupgridview": {
            "title": "服务商选择表格视图",
            "caption": "服务商",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSERVICEPickupGridView",
            "viewtag": "1d1e70a5b9543304e8ed1e67cd4359fc"
        },
        "empurplanpickupview": {
            "title": "计划修理数据选择视图",
            "caption": "计划修理",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPURPLANPickupView",
            "viewtag": "1d3dffff56f0f8225c55bd8652c9768f"
        },
        "emeqmpmpeditview": {
            "title": "设备仪表编辑视图",
            "caption": "设备仪表",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPMPEditView",
            "viewtag": "1d697999245ee1460339d24e5f4c4973"
        },
        "emplantempleditview_editmode": {
            "title": "计划模板",
            "caption": "计划模板",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPlanTemplEditView_EditMode",
            "viewtag": "1dccd45494894dd35174db1f415a7f06"
        },
        "embrandpickupview": {
            "title": "品牌数据选择视图",
            "caption": "品牌",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMBRANDPickupView",
            "viewtag": "1e26c5698500be5b232f96ceb6750271"
        },
        "emwplistgridview_ydh": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMWPLISTGridView_Ydh",
            "viewtag": "1e6ec592f18c98018c7195aafdd886c8"
        },
        "emitempuseissuedgridview": {
            "title": "领料单",
            "caption": "领料单-已发料",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPUseIssuedGridView",
            "viewtag": "1e861dbc3c43aba0fb68a77a5e9957c1"
        },
        "emwplistpoeditview9": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListPOEditView9",
            "viewtag": "1f0774bc3f303980c03efd08c1d7f0c3"
        },
        "emitemeditview_editmode": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMEditView_EditMode",
            "viewtag": "1fdb3aec31b15dc994c87c5647b7c97c"
        },
        "emwplistcostpickupview": {
            "title": "询价单数据选择视图",
            "caption": "询价单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPLISTCOSTPickupView",
            "viewtag": "1fde83a42aba69496ab0574941a04a08"
        },
        "pfunitpickupgridview": {
            "title": "计量单位选择表格视图",
            "caption": "计量单位",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFUNITPickupGridView",
            "viewtag": "2004a8e359ab90d68d2cc18dbbaea5e4"
        },
        "empodetailwaitbookgridview": {
            "title": "订单条目",
            "caption": "订单条目",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPODetailWaitBookGridView",
            "viewtag": "2061ffa9c76c7150c7462f7f7ea8060a"
        },
        "emwplisteditview9": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListEditView9",
            "viewtag": "206ec4a61e19c76ccdf230cb045cfa84"
        },
        "emeqsparepickupgridview": {
            "title": "备件包选择表格视图",
            "caption": "备件包",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREPickupGridView",
            "viewtag": "21669f5aa6d4c956282494807ef5ab59"
        },
        "emstockbystoregridview": {
            "title": "库存明细",
            "caption": "库存明细",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStockByStoreGridView",
            "viewtag": "21fd2def98999f86c9cef1b8f189e6b7"
        },
        "emservicefinanceview9": {
            "title": "服务商实体编辑视图（部件视图）<主信息>财务信息",
            "caption": "服务商",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceFinanceView9",
            "viewtag": "2212fa8a8eddba0c88dc546f7b060cd1"
        },
        "emrfoacpickupview": {
            "title": "方案数据选择视图",
            "caption": "方案",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOACPickupView",
            "viewtag": "22c4268fcb5bd9a5d544f48c6f4141aa"
        },
        "emwo_innereditview": {
            "title": "内部工单",
            "caption": "内部工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_INNEREditView",
            "viewtag": "2361099643586c627b770119262f2724"
        },
        "emwo_innerwovieweditview": {
            "title": "内部工单",
            "caption": "内部工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_INNERWOViewEditView",
            "viewtag": "23cf922005393e9ac7b94003dbcc8a7f"
        },
        "emeqmpmtrmpmtreditview": {
            "title": "设备仪表读数编辑视图",
            "caption": "设备仪表读数",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPMTRMpMtrEditView",
            "viewtag": "250d150693e3a13dad642f85b1ac4176"
        },
        "emeqkppickupview": {
            "title": "设备关键点数据选择视图",
            "caption": "设备关键点",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPPickupView",
            "viewtag": "25427d06dbcb7c87ca6df31b39eafc8b"
        },
        "emitemeditview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMEditView",
            "viewtag": "25964c045ead74bbe0c1e0a58ff7fb07"
        },
        "pfempeditview_editmode": {
            "title": "职员",
            "caption": "职员",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFEmpEditView_EditMode",
            "viewtag": "25a1dd82a2e45a928d12f9760c0b68cd"
        },
        "emwopickupgridview": {
            "title": "工单选择表格视图",
            "caption": "工单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOPickupGridView",
            "viewtag": "25a2d0b0d334a527ed021b59143f10b2"
        },
        "emitempickupview": {
            "title": "物品数据选择视图",
            "caption": "物品",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMPickupView",
            "viewtag": "25d5bba2ea6fd13e0b7b3135c298236a"
        },
        "emeqlocationalllocgridview": {
            "title": "位置",
            "caption": "位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLocationAllLocGridView",
            "viewtag": "289b2b9c811389501b8739486d66b3f5"
        },
        "emeqsetupeditview": {
            "title": "更换安装编辑视图",
            "caption": "更换安装",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQSETUPEditView",
            "viewtag": "2977252e22a822a52c615bdffe091afd"
        },
        "emeqkeepcaleditview9": {
            "title": "保养记录信息",
            "caption": "保养记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQKeepCalEditView9",
            "viewtag": "29a9e624a61d37f767ffea4ea02dde66"
        },
        "emengridview": {
            "title": "能源",
            "caption": "能源",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENGridView",
            "viewtag": "29eea99beaa446ca9e276435ff403187"
        },
        "appportalview": {
            "title": "应用门户视图",
            "caption": "",
            "viewtype": "APPPORTALVIEW",
            "viewmodule": "r7rt_dyna",
            "viewname": "AppPortalView",
            "viewtag": "2aa99fe33c29dc9f244dd4f64b35f4d1"
        },
        "emrfodetypegridview": {
            "title": "现象分类",
            "caption": "现象分类",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFODETYPEGridView",
            "viewtag": "2ae00e2f5556f24e9fdfc5c4388f7281"
        },
        "emeneditview": {
            "title": "能源编辑视图",
            "caption": "能源",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENEditView",
            "viewtag": "2bc1dbad21e69dd8bcabf478693434b6"
        },
        "emeqtypeeditview": {
            "title": "设备类型",
            "caption": "设备类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTYPEEditView",
            "viewtag": "2c43c508e829600fb3cc64c7d960527b"
        },
        "emwplistpickupgridview": {
            "title": "采购申请选择表格视图",
            "caption": "采购申请",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPLISTPickupGridView",
            "viewtag": "2d61d361e2270f72c10ad2bb12d142b9"
        },
        "emservicedashboardview": {
            "title": "综合评估",
            "caption": "综合评估",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMServiceDashboardView",
            "viewtag": "2e4a39364b4cef24bff0c8d221d38576"
        },
        "emitemcstoconfirmgridview": {
            "title": "调整单",
            "caption": "调整单-待确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemCSToConfirmGridView",
            "viewtag": "2f38db47e61061c30778365afee74beb"
        },
        "pfemptreeexpview": {
            "title": "职员树导航视图",
            "caption": "职员",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFEmpTreeExpView",
            "viewtag": "315930938f374fef1da7cc117f0bee37"
        },
        "emeqkeepeditview": {
            "title": "维护保养编辑视图",
            "caption": "维护保养",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQKEEPEditView",
            "viewtag": "31687d0a70ff5d370520e621fbfb7754"
        },
        "emwplistingridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListInGridView",
            "viewtag": "31b78ca14ac6073cd0050b73af50485b"
        },
        "emeqlctgsspickupgridview": {
            "title": "钢丝绳位置选择表格视图",
            "caption": "钢丝绳位置",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTGSSPickupGridView",
            "viewtag": "31d177948b54964c5d9399ef3e108d68"
        },
        "emitemrineditview9_editmode": {
            "title": "入库单信息",
            "caption": "入库单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMRINEditView9_EditMode",
            "viewtag": "322dea3adbbf873408e0e6e8a6b3882c"
        },
        "emeqlcttirespickupview": {
            "title": "轮胎位置数据选择视图",
            "caption": "轮胎位置",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTTIRESPickupView",
            "viewtag": "3332c5d0d6faef5d1782d61ac41eff0a"
        },
        "emeqmonitorgridview": {
            "title": "设备状态监控表格视图",
            "caption": "设备状态监控",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMonitorGridView",
            "viewtag": "34085fc7d06fab234e85c3f85822cc5a"
        },
        "emwogridview": {
            "title": "工单表格视图",
            "caption": "工单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOGridView",
            "viewtag": "341da6e362f62ba71efd268b27c65ad0"
        },
        "emwo_eneditview": {
            "title": "能耗登记工单",
            "caption": "能耗登记工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_ENEditView",
            "viewtag": "345ff519f8aa1ddb061490c2488fbf17"
        },
        "emeqsetupcaleditview9": {
            "title": "安装记录信息",
            "caption": "安装记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSetupCalEditView9",
            "viewtag": "34c5f59e0fff2aea89b766fbda3e890e"
        },
        "emequipdashboardview9": {
            "title": "设备主信息图表数据看板视图",
            "caption": "设备档案",
            "viewtype": "DEPORTALVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEquipDashboardView9",
            "viewtag": "34c8cbdf7eb525aa361d29c15e77141e"
        },
        "emeqcheckcaleditview9": {
            "title": "检定记录信息",
            "caption": "检定记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQCheckCalEditView9",
            "viewtag": "3609d6e8f168cabf89c3c7592c26b10e"
        },
        "embrandpickupgridview": {
            "title": "品牌选择表格视图",
            "caption": "品牌",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMBRANDPickupGridView",
            "viewtag": "365fba0fd1ce4d099fa4822479166f73"
        },
        "empopickupview": {
            "title": "订单数据选择视图",
            "caption": "订单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOPickupView",
            "viewtag": "3747513fcd029dfa942647e803f81d02"
        },
        "emrfodepickupgridview": {
            "title": "现象选择表格视图",
            "caption": "现象",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEPickupGridView",
            "viewtag": "37a73a57abb4994b6fcc0454cec89f2a"
        },
        "emrfoacgridview": {
            "title": "方案",
            "caption": "方案",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOACGridView",
            "viewtag": "37c0112937fdbe024269ecaf9f61a0dc"
        },
        "pfempdeptempgridview": {
            "title": "职员",
            "caption": "职员",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFEmpDeptEmpGridView",
            "viewtag": "38035a3dd892764c7966dd9fc97036d3"
        },
        "emoutputpickupview": {
            "title": "能力数据选择视图",
            "caption": "能力",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMOUTPUTPickupView",
            "viewtag": "3949f37a80cf105b50e28c948edcc0e1"
        },
        "empoeditview9_editmode": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMPOEditView9_EditMode",
            "viewtag": "39b5a686996f3c97f4b2ee163ec3aa10"
        },
        "emeqlctgssgridview": {
            "title": "钢丝绳位置",
            "caption": "钢丝绳位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTGSSGridView",
            "viewtag": "3aa5fb5800f3876034da0f420ef39255"
        },
        "emassetcleareditview_908": {
            "title": "资产清盘记录",
            "caption": "资产清盘记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLEAREditView_908",
            "viewtag": "3ab67d1643e912ed34f002875498e09e"
        },
        "emobjmaplocationtreeview": {
            "title": "位置信息",
            "caption": "位置信息",
            "viewtype": "DETREEVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMObjMapLocationTreeView",
            "viewtag": "3b9ed2292d3adb1fa915a4d69e587056"
        },
        "emeqcheckeditview": {
            "title": "维修记录编辑视图",
            "caption": "维修记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQCHECKEditView",
            "viewtag": "3bcba83274f4438a2b7caadc5e06b55a"
        },
        "emassetclasspickupgridview": {
            "title": "资产类别选择表格视图",
            "caption": "资产类别",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLASSPickupGridView",
            "viewtag": "3c335913b1a0ab75b99986012dfcfe78"
        },
        "empodetailwaitcheckgridview": {
            "title": "订单条目",
            "caption": "订单条目",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPODetailWaitCheckGridView",
            "viewtag": "3c70580f3678260cbc8eb41716097280"
        },
        "emeqmppickupview": {
            "title": "设备仪表数据选择视图",
            "caption": "设备仪表",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPPickupView",
            "viewtag": "3cab5e35dc9a82502376baaccc0e5c10"
        },
        "emitemtypeoptionview": {
            "title": "物品类型选项操作视图",
            "caption": "物品类型",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTypeOptionView",
            "viewtag": "3cb235f3a4e1d4c4e63be9d4294f0cde"
        },
        "emrfomoeditview": {
            "title": "模式信息",
            "caption": "模式信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOMOEditView",
            "viewtag": "3cca012f4fa60e57fa3cc611f5aee8a5"
        },
        "emeqkprcdgridview": {
            "title": "关键点记录表格视图",
            "caption": "关键点记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPRCDGridView",
            "viewtag": "3dbcfe8c1dcd6f132c8e3785e9122d05"
        },
        "emeqlocationmaininfo": {
            "title": "位置数据看板视图",
            "caption": "位置",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLocationMainInfo",
            "viewtag": "3e2068694e993df286aa445d3b6316b2"
        },
        "emwooripickupview": {
            "title": "工单来源数据选择视图",
            "caption": "工单来源",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOORIPickupView",
            "viewtag": "3ea6d46516d8218294d34a59d7371c0b"
        },
        "emstorepickupgridview": {
            "title": "仓库选择表格视图",
            "caption": "仓库",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSTOREPickupGridView",
            "viewtag": "3f7478a3c5e87b5fba682e59acecc9c7"
        },
        "emitemtypetreeexpview": {
            "title": "物品类型树导航视图",
            "caption": "物品类型",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTypeTreeExpView",
            "viewtag": "414713a2409444f96084360ecbb6c1ab"
        },
        "emeqmaintanceeditview": {
            "title": "抢修记录编辑视图",
            "caption": "抢修记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQMAINTANCEEditView",
            "viewtag": "414e6517d6331940d4e46365b25e3e98"
        },
        "emitemgridview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemGridView",
            "viewtag": "428e823cc84d522fb5493aefa31781f2"
        },
        "emrfodemapeditview": {
            "title": "现象引用编辑视图",
            "caption": "现象引用",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEMapEditView",
            "viewtag": "44248a93f9593eb9016d7460287f6529"
        },
        "emeqsetupgridview": {
            "title": "更换安装",
            "caption": "更换安装",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQSetupGridView",
            "viewtag": "464e420f9d987f4866ca077037ee01d8"
        },
        "emeneditview9_editmode": {
            "title": "能源信息",
            "caption": "能源信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_en",
            "viewname": "EMENEditView9_EditMode",
            "viewtag": "46616d674cb8df81b2126c71f513aaaa"
        },
        "emitempusedraftgridview": {
            "title": "领料单",
            "caption": "领料单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPUseDraftGridView",
            "viewtag": "4691b11405eeea706c57a40635ea9f78"
        },
        "emwplistgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMWPLISTGridView",
            "viewtag": "4750578f9f9466595f803ecac0acd619"
        },
        "emitempusepickupview": {
            "title": "领料单数据选择视图",
            "caption": "领料单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMPUSEPickupView",
            "viewtag": "47782801a8e838445601e03e13f887f5"
        },
        "emmachmodelpickupgridview": {
            "title": "机型选择表格视图",
            "caption": "机型",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMMACHMODELPickupGridView",
            "viewtag": "478b6337d8db293aefe322422df85c08"
        },
        "emobjectpickupgridview": {
            "title": "对象选择表格视图",
            "caption": "对象",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMOBJECTPickupGridView",
            "viewtag": "486a2b0216ea92dbfe1fe1b78baa73fe"
        },
        "emrfoaceditview": {
            "title": "方案信息",
            "caption": "方案信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOACEditView",
            "viewtag": "48f7dee358bd7941a353327768d5b680"
        },
        "emeqsparemaindashboardview9": {
            "title": "备件包信息",
            "caption": "备件包信息",
            "viewtype": "DEPORTALVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareMainDashboardView9",
            "viewtag": "4903396d56ac585de8cc7ef989d2f177"
        },
        "empoeditview": {
            "title": "订单编辑视图",
            "caption": "订单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMPOEditView",
            "viewtag": "49125cda017314ebbf2d8bea4226a17d"
        },
        "emasseteditview9": {
            "title": "资产信息",
            "caption": "资产信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMASSETEditView9",
            "viewtag": "49422a5e0d8d3c06e3b8b9a15b5807db"
        },
        "emrfocapickupview": {
            "title": "原因数据选择视图",
            "caption": "原因",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOCAPickupView",
            "viewtag": "49af69275d04283fa27d351646a8fa7e"
        },
        "emwo_engridview": {
            "title": "能耗登记工单",
            "caption": "能耗登记工单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_ENGridView",
            "viewtag": "4a0d09aa106d3dba14d1e3ceaf06165a"
        },
        "emitemtypeeditview_itemtype_editmode": {
            "title": "物品类型",
            "caption": "物品类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeEditView_itemtype_EditMode",
            "viewtag": "4a0f773543a61a3a5533704e368df3eb"
        },
        "emeqsparedetaileditview_editmode": {
            "title": "备件包明细",
            "caption": "备件包明细",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREDETAILEditView_EditMode",
            "viewtag": "4a8121c6fa1b47a736d87f0d41ac4085"
        },
        "emitempusewaitissueeditview9": {
            "title": "领料单",
            "caption": "领料单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMItemPUseWaitIssueEditView9",
            "viewtag": "4a97ec7fb3f8aa1dc65d9fe09b60b834"
        },
        "emwo_enwovieweditview": {
            "title": "能耗登记工单",
            "caption": "能耗登记工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_ENWOViewEditView",
            "viewtag": "4ae87b6c671bd22c86197d1ddf06a8f9"
        },
        "pfteamgridview": {
            "title": "班组",
            "caption": "班组",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "PFTEAMGridView",
            "viewtag": "4b08ade1e92a4d06e8f7c67323298a01"
        },
        "emeqlctmapgridview9": {
            "title": "位置关系表格视图",
            "caption": "位置关系",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTMapGridView9",
            "viewtag": "4b8faa22e39d1bb4e6119830aeb25133"
        },
        "empodetailpickupgridview": {
            "title": "订单条目选择表格视图",
            "caption": "订单条目",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPODETAILPickupGridView",
            "viewtag": "4cc4fce3fd52172ae7f756d1939e6c34"
        },
        "pfteameditview_editmode": {
            "title": "班组",
            "caption": "班组",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFTEAMEditView_EditMode",
            "viewtag": "4d91aca5ccf47ba93c881709705b34ac"
        },
        "emitemcsdraftgridview": {
            "title": "调整单",
            "caption": "调整单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemCSDraftGridView",
            "viewtag": "4e54bc67536bbae73e52b019ae2e7aff"
        },
        "emassettabexpview": {
            "title": "资产信息",
            "caption": "资产信息",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMASSETTabExpView",
            "viewtag": "4eb56a63c382258c0c1a2ccabae9389c"
        },
        "emrfomogridview": {
            "title": "模式",
            "caption": "模式",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOMOGridView",
            "viewtag": "4facc0cb6c8830328080ed421bb4b2eb"
        },
        "emeqtypepickupgridview": {
            "title": "设备类型选择表格视图",
            "caption": "设备类型",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQTYPEPickupGridView",
            "viewtag": "503b5eacba22c457c93a5d7f52160d26"
        },
        "emstoreoptionview": {
            "title": "仓库选项操作视图",
            "caption": "仓库",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStoreOptionView",
            "viewtag": "50582bdc9579edcd40202ec1cc41a388"
        },
        "emstorepartpickupview": {
            "title": "仓库库位数据选择视图",
            "caption": "仓库库位",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSTOREPARTPickupView",
            "viewtag": "50ac93347b4b197c99df4f6bc0c58123"
        },
        "emplancardview": {
            "title": "计划数据视图",
            "caption": "计划",
            "viewtype": "DEDATAVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPlanCardView",
            "viewtag": "50ad9e1827c3a8eb1ef0b5dfa0413bf6"
        },
        "emeqdebugeditview9": {
            "title": "调试记录信息",
            "caption": "调试记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQDEBUGEditView9",
            "viewtag": "51394fba2ae5c4f01f85eeaa0f74ec1f"
        },
        "emrfodeeditview": {
            "title": "现象信息",
            "caption": "现象信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFODEEditView",
            "viewtag": "51d8f08df268550d0721dc5c1c3cb723"
        },
        "emitemcsconfirmedgridview": {
            "title": "调整单",
            "caption": "调整单-已确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemCSConfirmedGridView",
            "viewtag": "53a58f1a6ef6438ff5994df2712ec509"
        },
        "emwo_oscgridview": {
            "title": "外委工单",
            "caption": "外委工单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_OSCGridView",
            "viewtag": "53a8f9336325b3cb34cf4d6e94b9ae82"
        },
        "emitemprtneditview": {
            "title": "还料单编辑视图",
            "caption": "还料单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMPRTNEditView",
            "viewtag": "54117096c2109748d2a8c151f12a5149"
        },
        "emstorepartoptionview": {
            "title": "仓库库位选项操作视图",
            "caption": "仓库库位",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStorePartOptionView",
            "viewtag": "550e982b6000fa6d53dd06fb914a15f1"
        },
        "emeqmaintancegridview": {
            "title": "抢修记录",
            "caption": "抢修记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQMaintanceGridView",
            "viewtag": "56f9100d6367b6a9e7e565e0c4f6939c"
        },
        "emstoreparteditview_9836": {
            "title": "仓库库位",
            "caption": "仓库库位",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMSTOREPARTEditView_9836",
            "viewtag": "571dd277c558698c2ca05150b9eb7841"
        },
        "emeqlocationpickupgridview": {
            "title": "位置选择表格视图",
            "caption": "位置",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLOCATIONPickupGridView",
            "viewtag": "571e487e337b60033d4001f09d2f1878"
        },
        "emwo_eneditview_editmode": {
            "title": "能耗登记工单",
            "caption": "能耗登记工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_ENEditView_EditMode",
            "viewtag": "587737198e4f46fa023e46417d9d5217"
        },
        "pfcontracteditview9": {
            "title": "合同信息",
            "caption": "合同信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_pf",
            "viewname": "PFCONTRACTEditView9",
            "viewtag": "58bf277c2cbee2a52a74106b41d778ae"
        },
        "emserviceeditview9": {
            "title": "PUR服务商信息",
            "caption": "PUR服务商信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceEditView9",
            "viewtag": "58c4f88b60a44f034297a49d8f864c97"
        },
        "emitempusewaitissuegridview": {
            "title": "领料单",
            "caption": "领料单-待发料",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPUseWaitIssueGridView",
            "viewtag": "592ae78a69a75929ce82a9638999c405"
        },
        "emitempleditview": {
            "title": "损溢单编辑视图",
            "caption": "损溢单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMPLEditView",
            "viewtag": "5982fb15294b4762ff8e00d11f12a279"
        },
        "pfdeptpickupgridview": {
            "title": "部门选择表格视图",
            "caption": "部门",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFDeptPickupGridView",
            "viewtag": "59a14eae6cf6369807fc76dec155879a"
        },
        "emstorepartpickupgridview": {
            "title": "仓库库位选择表格视图",
            "caption": "仓库库位",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSTOREPARTPickupGridView",
            "viewtag": "5a54185fd49f62506492f33fc27827ea"
        },
        "emwo_innergridview": {
            "title": "内部工单",
            "caption": "内部工单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_INNERGridView",
            "viewtag": "5a97d6251bdce275669771c24fb5b6e5"
        },
        "emequippickupgridview": {
            "title": "设备档案选择表格视图",
            "caption": "设备档案",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEquipPickupGridView",
            "viewtag": "5c0326aa9b310f037de5e6eb270857be"
        },
        "emitemrouttabexpview": {
            "title": "退货单分页导航视图",
            "caption": "退货单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemROutTabExpView",
            "viewtag": "5c5e4fa9e64c9921300966130d1ad5f9"
        },
        "emserviceinfoview9": {
            "title": "服务商实体编辑视图（部件视图）<主信息>基本信息",
            "caption": "服务商",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceInfoView9",
            "viewtag": "5da38941d0dfe5e783428c6bb83ce796"
        },
        "emitemtypepickupgridview": {
            "title": "物品类型选择表格视图",
            "caption": "物品类型",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypePickupGridView",
            "viewtag": "5e894d9e36080ddc4d7b4e5448bd7d24"
        },
        "emeqcheckgridview": {
            "title": "维修记录",
            "caption": "维修记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQCheckGridView",
            "viewtag": "5f19751cbdf4c149af0b96af3043e377"
        },
        "emdrwgmapeditview": {
            "title": "文档引用编辑视图",
            "caption": "文档引用",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMDRWGMapEditView",
            "viewtag": "5fc1bdb2af5ed457da1115bd91b98e19"
        },
        "emitemcstabexpview": {
            "title": "库间调整单分页导航视图",
            "caption": "库间调整单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemCSTabExpView",
            "viewtag": "5fc7b73f6070f9b56bfa8733537822ee"
        },
        "emequipruninfo": {
            "title": "设备档案编辑视图",
            "caption": "设备档案",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEquipRunInfo",
            "viewtag": "60665c8ec4c72abe4cdb2496075c907d"
        },
        "emenconsumeditview": {
            "title": "能耗编辑视图",
            "caption": "能耗",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENCONSUMEditView",
            "viewtag": "609f585350400bdf554915dca6837136"
        },
        "emequipallgridview": {
            "title": "设备档案",
            "caption": "设备档案",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEquipAllGridView",
            "viewtag": "60d9de1c821ff1290dd908ac80586f05"
        },
        "emeqsparemapeditview_editmode": {
            "title": "备件包引用",
            "caption": "备件包引用",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSpareMapEditView_EditMode",
            "viewtag": "60eeff9bc94af0f2429aa2206058b5d5"
        },
        "emstockpickupview": {
            "title": "库存数据选择视图",
            "caption": "库存",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSTOCKPickupView",
            "viewtag": "60f1d13bd2af148643eda5a6feb02fbb"
        },
        "emwo_dpeditview": {
            "title": "点检工单",
            "caption": "点检工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_DPEditView",
            "viewtag": "611985ee732078e174c62e53aa9f9c18"
        },
        "emitemplgridview": {
            "title": "损溢单",
            "caption": "损溢单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMPLGridView",
            "viewtag": "62330dd54219668920b9e397e8053178"
        },
        "emoutputpickupgridview": {
            "title": "能力选择表格视图",
            "caption": "能力",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMOUTPUTPickupGridView",
            "viewtag": "62950881fdc22843e8ff34af2d557e98"
        },
        "emwplistcosteditview9": {
            "title": "询价单",
            "caption": "询价单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListCostEditView9",
            "viewtag": "63c76d9d23f4953d0af77d6de9a80155"
        },
        "emwplistcostpickupgridview": {
            "title": "询价单选择表格视图",
            "caption": "询价单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPLISTCOSTPickupGridView",
            "viewtag": "64bed4452adbdc48b9f432a2561b0998"
        },
        "emitemstoreinfoview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemStoreInfoView",
            "viewtag": "64f4dddb0d705eddae701aefd70a858d"
        },
        "emstockgridview": {
            "title": "库存物品",
            "caption": "库存物品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStockGridView",
            "viewtag": "65b3d0c6e10a4f46a804e62d969a7a1d"
        },
        "emitemtypepickupview": {
            "title": "物品类型数据选择视图",
            "caption": "物品类型",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMTYPEPickupView",
            "viewtag": "67949bd6416cdb683c73643a5c6ad819"
        },
        "emeqmaintanceeditview9_editmode": {
            "title": "维修记录信息",
            "caption": "维修记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_act",
            "viewname": "EMEQMAINTANCEEditView9_EditMode",
            "viewtag": "67b3cfddcd792f6106aba52dc832bc10"
        },
        "emplandetailgridview": {
            "title": "计划步骤",
            "caption": "计划步骤",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANDETAILGridView",
            "viewtag": "6896e173bceeaed74a0ce72bac4f904d"
        },
        "emassetgridview": {
            "title": "固定资产台账",
            "caption": "固定资产台账",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETGridView",
            "viewtag": "6a10b8f5e87a12e8a24bdc67b437b47a"
        },
        "emeqlctrhyeditview": {
            "title": "润滑油位置",
            "caption": "润滑油位置",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTRHYEditView",
            "viewtag": "6a2a4551096d3828c346829603762b0b"
        },
        "emapplygridview": {
            "title": "外委申请",
            "caption": "外委申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMApplyGridView",
            "viewtag": "6ac8d5af7eb6ce3329e63fb26aa92235"
        },
        "emeqahgridview": {
            "title": "活动历史表格视图",
            "caption": "活动历史",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQAHGridView",
            "viewtag": "6b8d56d2a85071b7841d50dde9a4e378"
        },
        "pfunitpickupview": {
            "title": "计量单位数据选择视图",
            "caption": "计量单位",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFUNITPickupView",
            "viewtag": "6c210a328b465189e1ce8213e64f29d8"
        },
        "emstoreeditview": {
            "title": "仓库",
            "caption": "仓库",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMSTOREEditView",
            "viewtag": "6cb619376f72dccb8c09b0f68daaa909"
        },
        "empotabexpview": {
            "title": "订单",
            "caption": "订单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOTabExpView",
            "viewtag": "6dbfcb414a6ef9afdad952637a07efa3"
        },
        "pfteampickupgridview": {
            "title": "班组选择表格视图",
            "caption": "班组",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFTeamPickupGridView",
            "viewtag": "6e9902f95a9cbb189e5da96753da45da"
        },
        "emberthpickupview": {
            "title": "泊位数据选择视图",
            "caption": "泊位",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMBERTHPickupView",
            "viewtag": "6eb4f765c149eee0c495a2edaa2983cf"
        },
        "emeqmppickupgridview": {
            "title": "设备仪表选择表格视图",
            "caption": "设备仪表",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPPickupGridView",
            "viewtag": "6f7d72913ed0c1d4ed171df2f23abfec"
        },
        "emenconsumeditview9_editmode": {
            "title": "能耗信息",
            "caption": "能耗信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_en",
            "viewname": "EMENCONSUMEditView9_EditMode",
            "viewtag": "70422ddbe4aa1bb51c8426db89b3be7e"
        },
        "emserviceevleditview9_editmode": {
            "title": "服务商评估信息",
            "caption": "服务商评估信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMServiceEvlEditView9_EditMode",
            "viewtag": "70f71409257325745ff2e86c93876df7"
        },
        "emeneditview9": {
            "title": "能源信息",
            "caption": "能源信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMENEditView9",
            "viewtag": "71b6cc819239143a110e13eb520d3626"
        },
        "emdrwgmapequipgridview9": {
            "title": "文档引用表格视图",
            "caption": "文档引用",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMDRWGMapEquipGridView9",
            "viewtag": "72767a0e445dab6388099a833004e4b3"
        },
        "pfuniteditview": {
            "title": "计量单位",
            "caption": "计量单位",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFUNITEditView",
            "viewtag": "728d8cd0b4fabe71b80be85958e281d8"
        },
        "emeqlocationeditview": {
            "title": "位置",
            "caption": "位置",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLocationEditView",
            "viewtag": "73ef3caef86a2fd0cfd7454e4dd4d8cf"
        },
        "emwo_innereditview_editmode": {
            "title": "内部工单",
            "caption": "内部工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_INNEREditView_EditMode",
            "viewtag": "7499f8fe484f4182cff42890c9cd0c58"
        },
        "emwplisteditview9_editmode": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMWPListEditView9_EditMode",
            "viewtag": "762134bd0c3144814da5afa9ace061b3"
        },
        "emwplistconfirmcosteditview9": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListConfirmCostEditView9",
            "viewtag": "767b7b31bd9333d8b133b66d91a53c2d"
        },
        "pfcontractpickupview": {
            "title": "合同数据选择视图",
            "caption": "合同",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFCONTRACTPickupView",
            "viewtag": "774381e4b650bcdfdce939ad318f0fed"
        },
        "emwplistcostconfirmgridview9": {
            "title": "询价单表格视图",
            "caption": "询价单",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMWPListCostConfirmGridView9",
            "viewtag": "774c9fd778503dacdec1a0dcbe9cce55"
        },
        "emservicepickupview": {
            "title": "服务商数据选择视图",
            "caption": "服务商",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSERVICEPickupView",
            "viewtag": "7768e48f7cc440f8ef552f938853d026"
        },
        "emstockbycabgridview": {
            "title": "库存明细",
            "caption": "库存明细",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStockByCabGridView",
            "viewtag": "77ad1ef387dc56e1bc15ec1eab78dd8b"
        },
        "emequipoptionview": {
            "title": "设备档案选项操作视图",
            "caption": "设备档案",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEquipOptionView",
            "viewtag": "77c55c12b5d23974a9f44f901244c805"
        },
        "emeqtypegridview": {
            "title": "设备类型",
            "caption": "设备类型",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTypeGridView",
            "viewtag": "784c685c2487042fd1b7fa55bffb73bb"
        },
        "emassetclassgridview": {
            "title": "资产类别",
            "caption": "资产类别",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLASSGridView",
            "viewtag": "7854d9a7509d47aaf971c8b6dd2769c3"
        },
        "emwplistconfimcostgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListConfimCostGridView",
            "viewtag": "78b776caa59e35480f83bb7a46d1d687"
        },
        "emeqtypeoptionview": {
            "title": "设备类型",
            "caption": "设备类型",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTypeOptionView",
            "viewtag": "795b8ee7c6ff028148aa32586bc8cbee"
        },
        "emserviceevleditview9": {
            "title": "服务商评估信息",
            "caption": "服务商评估信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceEvlEditView9",
            "viewtag": "796bf3a44a5f4d9d27995d2ebb5f482a"
        },
        "emeqlctgsseditview": {
            "title": "钢丝绳位置",
            "caption": "钢丝绳位置",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTGSSEditView",
            "viewtag": "7a94d7f0edaeee15127dc6e681a6de4f"
        },
        "emitempuseeditview": {
            "title": "领料单编辑视图",
            "caption": "领料单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMPUSEEditView",
            "viewtag": "7a9bcdffd947fe8b8b9b88d63191291b"
        },
        "emrfocapickupgridview": {
            "title": "原因选择表格视图",
            "caption": "原因",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOCAPickupGridView",
            "viewtag": "7b1b112bf9e107a231ab748f634a2a23"
        },
        "empoplaceordergridview": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOPlaceOrderGridView",
            "viewtag": "7c656d6fcd4ed263b6a1bb54d92bdfe5"
        },
        "equipportalview": {
            "title": "设备模块看板视图",
            "caption": "",
            "viewtype": "APPPORTALVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EquipPortalView",
            "viewtag": "7CA66A5C-3001-4E00-890B-E837B1EF8BE5"
        },
        "emrfocaeditview": {
            "title": "原因信息",
            "caption": "原因信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOCAEditView",
            "viewtag": "7d70944c35d05b73a64d96692d9b7a11"
        },
        "emplantabexpview": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPlanTabExpView",
            "viewtag": "7f058dd8ad37ce1cb57ea0c71b3c6fcf"
        },
        "emrfoacpickupgridview": {
            "title": "方案选择表格视图",
            "caption": "方案",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOACPickupGridView",
            "viewtag": "7f8f5353cccd960a2fee86637c57e140"
        },
        "pfempeditview": {
            "title": "职员",
            "caption": "职员",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFEMPEditView",
            "viewtag": "7f9f1447aa8a509446c1214c3bd5a6a8"
        },
        "emeqspareeditview_editmode": {
            "title": "备件包信息",
            "caption": "备件包信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREEditView_EditMode",
            "viewtag": "7fbab3035ca47489c70f5aaa0321b7ca"
        },
        "emenconsumpickupview": {
            "title": "能耗数据选择视图",
            "caption": "能耗",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENCONSUMPickupView",
            "viewtag": "80a7a43341181c0103abee898a46ed53"
        },
        "empodetailpickupview": {
            "title": "订单条目数据选择视图",
            "caption": "订单条目",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPODETAILPickupView",
            "viewtag": "820753f3aeea3c78cb214230a40e7aa8"
        },
        "emwocalendarexpview": {
            "title": "工单日历导航视图",
            "caption": "工单",
            "viewtype": "DECALENDAREXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOCalendarExpView",
            "viewtag": "82786c251576f16615c5401c7bba7784"
        },
        "emeqmonitormonitoreditview": {
            "title": "设备状态监控编辑视图",
            "caption": "设备状态监控",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMonitorMonitorEditView",
            "viewtag": "83061e32b27f34cdc79beae44715f699"
        },
        "emitemdashboardview": {
            "title": "物品数据看板视图",
            "caption": "物品",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemDashboardView",
            "viewtag": "835ac463b38e9280e5e8a634a60b9ee8"
        },
        "emeqsparedetailgridview9": {
            "title": "备件物品",
            "caption": "备件物品",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareDetailGridView9",
            "viewtag": "84924f7fb797afe555106c38d97de991"
        },
        "emplancdtgridview": {
            "title": "计划条件",
            "caption": "计划条件",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANCDTGridView",
            "viewtag": "84add22fa08dcb5195f8009b07dfdecc"
        },
        "emrfocagridview": {
            "title": "原因",
            "caption": "原因",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFOCAGridView",
            "viewtag": "84c04af308d4fa82ac8465d31b236c8e"
        },
        "emeqlctfdjeditview": {
            "title": "发动机位置",
            "caption": "发动机位置",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTFDJEditView",
            "viewtag": "84d680d78fa5968f5a1aabd0570c554f"
        },
        "emapplyeditview_editmode": {
            "title": "外委申请",
            "caption": "外委申请",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMApplyEditView_EditMode",
            "viewtag": "855aa48e7c35c5070e82166fd59bf9e5"
        },
        "emeqtypepickupview": {
            "title": "设备类型数据选择视图",
            "caption": "设备类型",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQTYPEPickupView",
            "viewtag": "8699750ab7e52363ec30b0e681f58ed0"
        },
        "emoutputrcteditview": {
            "title": "产能信息",
            "caption": "产能信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMOUTPUTRCTEditView",
            "viewtag": "885c76f79072e981b9e9611d023e93c3"
        },
        "emequipgridview": {
            "title": "设备档案",
            "caption": "设备档案",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEquipGridView",
            "viewtag": "89830082f3410da731c0e787fbf4648a"
        },
        "empodetaileditview": {
            "title": "订单条目编辑视图",
            "caption": "订单条目",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMPODETAILEditView",
            "viewtag": "8a2a4d020f9bd33e9e1cea010aaff41d"
        },
        "emoutputrcteditview9": {
            "title": "产能信息",
            "caption": "产能信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMOUTPUTRCTEditView9",
            "viewtag": "8a3d1375852125c144f8ab45598d9884"
        },
        "emwooripickupgridview": {
            "title": "工单来源选择表格视图",
            "caption": "工单来源",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOORIPickupGridView",
            "viewtag": "8ab3aace431552e8ad15f24c6cf0c198"
        },
        "emwplistpickupview": {
            "title": "采购申请数据选择视图",
            "caption": "采购申请",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPLISTPickupView",
            "viewtag": "8b71081af178f5d3d04e4400acf700b5"
        },
        "emitemcseditview": {
            "title": "库间调整单编辑视图",
            "caption": "库间调整单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMCSEditView",
            "viewtag": "8bebbeff9bcc047392dd84a201ec8805"
        },
        "emitemtypeeditview": {
            "title": "物品类型",
            "caption": "物品类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMTYPEEditView",
            "viewtag": "8c4fa22482db266069dbfbf1aece48eb"
        },
        "empurplanpickupgridview": {
            "title": "计划修理选择表格视图",
            "caption": "计划修理",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPURPLANPickupGridView",
            "viewtag": "8cc8bb0e1da7824a272b13bad913dadd"
        },
        "emplandashboardview": {
            "title": "计划数据看板视图",
            "caption": "计划",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPlanDashboardView",
            "viewtag": "8e734893dc52033a06fa1a7eec14e1df"
        },
        "emplantemplpickupview": {
            "title": "计划模板数据选择视图",
            "caption": "计划模板",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANTEMPLPickupView",
            "viewtag": "8e8a9b547590ca9d94a65e7373bef39a"
        },
        "emplanpersoninfo": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPlanPersonInfo",
            "viewtag": "8ef30243c7d1137e1c3e5c23c849f6c5"
        },
        "emitemtypeitemtreeexpview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTypeItemTreeExpView",
            "viewtag": "8f76e5a86d3c0f946b27ac79f2cc4153"
        },
        "empoclosedordergridview": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOClosedOrderGridView",
            "viewtag": "8fe0ff60eec445839cfbdd825e4f0b54"
        },
        "emitempusepickupgridview": {
            "title": "领料单选择表格视图",
            "caption": "领料单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMPUSEPickupGridView",
            "viewtag": "90071f0679e8fdd324cea581331ceeb9"
        },
        "emwo_osceditview": {
            "title": "外委工单",
            "caption": "外委工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_OSCEditView",
            "viewtag": "9157061505c8c6f5cbe2cdee2cc4726b"
        },
        "emoutputeditview": {
            "title": "能力信息",
            "caption": "能力信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMOUTPUTEditView",
            "viewtag": "918d65d66e024998c81bbdb6a0a61913"
        },
        "emeqlctgsspickupview": {
            "title": "钢丝绳位置数据选择视图",
            "caption": "钢丝绳位置",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTGSSPickupView",
            "viewtag": "9341ba20f39986f10fe4715ca70fbfcd"
        },
        "emitemtypeeditview_itemtype": {
            "title": "物品类型",
            "caption": "物品类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeEditView_itemtype",
            "viewtag": "93ee67e91be4f661f92860285dfbbece"
        },
        "emeqahtreeexpview": {
            "title": "活动历史树导航视图",
            "caption": "活动历史",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQAHTreeExpView",
            "viewtag": "93f4c5800790b67affe864db5e0d6e6e"
        },
        "emservicecontactview9": {
            "title": "服务商实体编辑视图（部件视图）<主信息>联系信息",
            "caption": "服务商",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceContactView9",
            "viewtag": "94ceab7db69b7c0f89e4abfe5641ae93"
        },
        "emwplistcostgridview9": {
            "title": "询价单表格视图",
            "caption": "询价单",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListCostGridView9",
            "viewtag": "94ffdd2b1be694c6ec0e7511c815c6bc"
        },
        "emservicegridview": {
            "title": "服务商表格视图",
            "caption": "服务商",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMServiceGridView",
            "viewtag": "95e5d8e9491ac1c704391edba3df8dac"
        },
        "emeqkpgridview": {
            "title": "设备关键点表格视图",
            "caption": "设备关键点",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQKPGridView",
            "viewtag": "95f6cc645fb95b3a7d407fd47a481df5"
        },
        "emdrwggridview": {
            "title": "文档",
            "caption": "文档",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMDRWGGridView",
            "viewtag": "95fa93ae2764709bf48ca56295795929"
        },
        "emwplistdraftgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListDraftGridView",
            "viewtag": "969d6f824da8026cd7c51557e9d9a509"
        },
        "emeqcheckeditview9_editmode": {
            "title": "检定记录信息",
            "caption": "检定记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_act",
            "viewname": "EMEQCHECKEditView9_EditMode",
            "viewtag": "9753ceb117067ddcdbb28b5028c45368"
        },
        "emwplistcosteditview9_editmode": {
            "title": "询价单",
            "caption": "询价单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMWPLISTCOSTEditView9_EditMode",
            "viewtag": "97764057e5506bfc1e5774cdce25ffea"
        },
        "emitemtypegridexpview": {
            "title": "物品类型表格导航视图",
            "caption": "物品类型",
            "viewtype": "DEGRIDEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTypeGridExpView",
            "viewtag": "98dd6b90670dde4f6c715e219925c86e"
        },
        "pfcontractpickupgridview": {
            "title": "合同选择表格视图",
            "caption": "合同",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFCONTRACTPickupGridView",
            "viewtag": "98fc6acb2c4c6be29d2ce47dd0fccc5a"
        },
        "emenpickupview": {
            "title": "能源数据选择视图",
            "caption": "能源",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENPickupView",
            "viewtag": "98fd7570fa05a5721e90f8c2bc734e8b"
        },
        "emwplistcostgridview": {
            "title": "询价单",
            "caption": "询价单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMWPListCostGridView",
            "viewtag": "997ecabc7fe92a5ea975f669e5d2a0c2"
        },
        "emenconsumpickupgridview": {
            "title": "能耗选择表格视图",
            "caption": "能耗",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENCONSUMPickupGridView",
            "viewtag": "9b213ebe80d8a53a6756f9d306b8c596"
        },
        "emacclasspickupgridview": {
            "title": "总帐科目选择表格视图",
            "caption": "总帐科目",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMACCLASSPickupGridView",
            "viewtag": "9caa8a81f904195f8c89d52564766774"
        },
        "emequipeditview2": {
            "title": "设备档案编辑视图",
            "caption": "设备档案",
            "viewtype": "DEEDITVIEW2",
            "viewmodule": "eam_core",
            "viewname": "EMEquipEditView2",
            "viewtag": "9d546ecd716089e12f2c2602f5de0123"
        },
        "emitemtypeeditview_editmode": {
            "title": "物品类型",
            "caption": "物品类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeEditView_EditMode",
            "viewtag": "9e4ec3e0ede3f5634c7540f2c21673a6"
        },
        "emequippickupview": {
            "title": "设备档案数据选择视图",
            "caption": "设备档案",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQUIPPickupView",
            "viewtag": "9f1808ad0af0680daf887d4050376b41"
        },
        "emeqmpmtrgridview": {
            "title": "设备仪表读数表格视图",
            "caption": "设备仪表读数",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPMTRGridView",
            "viewtag": "9f559f8a15fa5cac3d9ce75e64664b26"
        },
        "emwo_dpgridview": {
            "title": "点检工单",
            "caption": "点检工单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_DPGridView",
            "viewtag": "9f9732def4c66d602c1c3e7d4576385a"
        },
        "emdrwgeditview_editmode": {
            "title": "文档",
            "caption": "文档",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMDRWGEditView_EditMode",
            "viewtag": "a043d07fdf452d7a89efe7e3b1b86795"
        },
        "emeqtypetreeexpview": {
            "title": "设备类型",
            "caption": "设备档案",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTypeTreeExpView",
            "viewtag": "a05c3a903b6c4c1657e1a2b01f5cb97b"
        },
        "emeqsparegridexpview": {
            "title": "备件包表格导航视图",
            "caption": "备件包",
            "viewtype": "DEGRIDEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareGridExpView",
            "viewtag": "a08baab304950ada849bf8d372da667d"
        },
        "emeqwleqgridview": {
            "title": "设备运行日志表格视图",
            "caption": "设备运行日志",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQWLEqGridView",
            "viewtag": "a13156d8910a0c58e5a6b64710e43c69"
        },
        "emwplistwpprocesstreeexpview": {
            "title": "采购申请树导航视图",
            "caption": "采购申请",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListWpProcessTreeExpView",
            "viewtag": "a22fd003d9da9123754f5f1b54592ac0"
        },
        "emstockpickupgridview": {
            "title": "库存选择表格视图",
            "caption": "库存",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStockPickupGridView",
            "viewtag": "a2d3b73797351b53b3c6c18c5521e588"
        },
        "emeqtypegridexpview": {
            "title": "设备类型表格导航视图",
            "caption": "设备类型",
            "viewtype": "DEGRIDEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTypeGridExpView",
            "viewtag": "a31a05a2d875246666373766e2283869"
        },
        "emrfomopickupview": {
            "title": "模式数据选择视图",
            "caption": "模式",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFOMOPickupView",
            "viewtag": "a45ddcd2be606b6ee6ba88b2f0e40374"
        },
        "emitemcsgridview": {
            "title": "调整单",
            "caption": "调整单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMCSGridView",
            "viewtag": "a4b03b50ee6214ced931a9a1139674f7"
        },
        "emeqlcttireseditview": {
            "title": "轮胎位置",
            "caption": "轮胎位置",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTTIRESEditView",
            "viewtag": "a59db44f90b2dc4168f2d6a15158dbb4"
        },
        "pfdepteditview": {
            "title": "部门",
            "caption": "部门",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFDEPTEditView",
            "viewtag": "a5d46f833cb95fbfaa23212d89364925"
        },
        "emeqtypepickuptreeview": {
            "title": "设备类型",
            "caption": "设备类型",
            "viewtype": "DEPICKUPTREEVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQTypePickupTreeView",
            "viewtag": "a6606bb729380753fc447984af7c29ed"
        },
        "emitemoptionview": {
            "title": "物品选项操作视图",
            "caption": "物品",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemOptionView",
            "viewtag": "a6c1475b17aa27870e7814ec31e1c9d3"
        },
        "emitemrinpickupgridview": {
            "title": "入库单选择表格视图",
            "caption": "入库单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMRINPickupGridView",
            "viewtag": "a797e59cfe10b1bb05a5e71d7ce45aca"
        },
        "emstoreparteditview_7215": {
            "title": "仓库库位",
            "caption": "仓库库位",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStorePartEditView_7215",
            "viewtag": "a7b26679d68f7d3407173c0934ea84bc"
        },
        "empodetaileditview9": {
            "title": "采购订单条目",
            "caption": "采购订单条目",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMPODETAILEditView9",
            "viewtag": "a92e856807e7904251f5e6e500ae588c"
        },
        "emstorepickupview": {
            "title": "仓库数据选择视图",
            "caption": "仓库",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMSTOREPickupView",
            "viewtag": "a93af11767f301deffbd9e48c085bf1b"
        },
        "emeqdebugcaleditview9": {
            "title": "调试记录信息",
            "caption": "调试记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQDebugCalEditView9",
            "viewtag": "aa36914094f91a706218650dec8af86e"
        },
        "emeqlcttiresgridview": {
            "title": "轮胎位置",
            "caption": "轮胎位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTTIRESGridView",
            "viewtag": "aa418c0dcfb1d823e87a66da6b08285e"
        },
        "emrfodegridexpview": {
            "title": "现象表格导航视图",
            "caption": "现象",
            "viewtype": "DEGRIDEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEGridExpView",
            "viewtag": "aaa3b6b0224d8923c814af67b703f0ca"
        },
        "emmachinecategorypickupview": {
            "title": "机种编号数据选择视图",
            "caption": "机种编号",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMMACHINECATEGORYPickupView",
            "viewtag": "aaaed92d2daf01a3dde53b4d0ac8784c"
        },
        "emeqtypeeditview_editmode": {
            "title": "设备类型",
            "caption": "设备类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQTypeEditView_EditMode",
            "viewtag": "aaf736274867f83f509b019c53bdfca9"
        },
        "emwplistwaitpogridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListWaitPoGridView",
            "viewtag": "ab4b3405ee3564694515edfd4edf17d1"
        },
        "pfemppickupview": {
            "title": "职员数据选择视图",
            "caption": "职员",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFEMPPickupView",
            "viewtag": "ab4f475b20564461a2834dc778f7db5e"
        },
        "emeqsparedetailgridview": {
            "title": "备件物品",
            "caption": "备件物品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREDETAILGridView",
            "viewtag": "ac2ec6e6d320e1912ce92d764ca1306d"
        },
        "empoonordergridview": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOOnOrderGridView",
            "viewtag": "ac540e9f4024d6ac9f6f1b6efbe1b28c"
        },
        "emeqlctmapeditview": {
            "title": "位置关系编辑视图",
            "caption": "位置关系",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTMapEditView",
            "viewtag": "adc4bd961edb468b6ab70b433f9ae735"
        },
        "emitemrouteditview9_editmode": {
            "title": "退货单信息",
            "caption": "退货单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMROUTEditView9_EditMode",
            "viewtag": "ae364d55428ad46f97db9670dc07d88b"
        },
        "emrfodepickupview": {
            "title": "现象数据选择视图",
            "caption": "现象",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEPickupView",
            "viewtag": "ae4d57ca65ee6e0dc185ae92383936c0"
        },
        "emeqlocationoptionview": {
            "title": "位置新建",
            "caption": "位置新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLocationOptionView",
            "viewtag": "ae55690d4fd01dab6647da03167af7f4"
        },
        "emeqaheqcalendarexpview": {
            "title": "设备活动日历导航视图",
            "caption": "活动历史",
            "viewtype": "DECALENDAREXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQAHEqCalendarExpView",
            "viewtag": "af462b2dc25c02bbb2147e2b870dcf21"
        },
        "emeqsparemapdataview9": {
            "title": "备件包引用数据视图",
            "caption": "备件包引用",
            "viewtype": "DEDATAVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareMapDataView9",
            "viewtag": "af58f06f6d8e218cbeb7e75f1cbf4286"
        },
        "emassetclasseditview": {
            "title": "ASSET资产类别信息",
            "caption": "ASSET资产类别信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLASSEditView",
            "viewtag": "af7d2de4c57a8912a459d4574fa48d9b"
        },
        "emproducteditview9": {
            "title": "试用品信息",
            "caption": "试用品信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMPRODUCTEditView9",
            "viewtag": "afaeddca7e510be3e3d487eedd81aa4d"
        },
        "emplangridview": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPlanGridView",
            "viewtag": "b0cdcd21454667bc48196e43dcea0042"
        },
        "emitemrinpickupview": {
            "title": "入库单数据选择视图",
            "caption": "入库单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMRINPickupView",
            "viewtag": "b0ff136b90c2901235ac196d3a053549"
        },
        "emenconsumgridview": {
            "title": "能耗信息",
            "caption": "能耗信息",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENConsumGridView",
            "viewtag": "b19c6d4c64f60d21f2087da573276c64"
        },
        "emwo_oscwovieweditview": {
            "title": "外委工单",
            "caption": "外委工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_OSCWOViewEditView",
            "viewtag": "b1aec6353412cebea7399b1a26c7b563"
        },
        "emitemprtnconfirmedgridview": {
            "title": "还料单",
            "caption": "还料单-已确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPRtnConfirmedGridView",
            "viewtag": "b1afaa8e991209bab100fc2828e56d71"
        },
        "emdrwgtreeexpview": {
            "title": "文档",
            "caption": "文档",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMDRWGTreeExpView",
            "viewtag": "b1fb9893ca839830dd6e3023769081d8"
        },
        "emitemprtngridview": {
            "title": "还料单",
            "caption": "还料单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMPRTNGridView",
            "viewtag": "b2ea4f0fcb22c7fee66340f6c991d946"
        },
        "emplancdteditview": {
            "title": "计划条件",
            "caption": "计划条件",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANCDTEditView",
            "viewtag": "b39fc5c6e7ece40a427f0eb097a4d985"
        },
        "emapplyeqgridview": {
            "title": "外委申请",
            "caption": "外委申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMApplyEqGridView",
            "viewtag": "b3ad1025e9dbaf2cc5c06bb168fc3d69"
        },
        "emwopickupview": {
            "title": "工单数据选择视图",
            "caption": "工单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOPickupView",
            "viewtag": "b48f506479fd66761ee52b3f531e1f68"
        },
        "pfcontractgridview": {
            "title": "合同",
            "caption": "合同",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "PFCONTRACTGridView",
            "viewtag": "b4908382917ec2721c640f7052e0c674"
        },
        "emitemroutdraftgridview": {
            "title": "退货单",
            "caption": "退货单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemROutDraftGridView",
            "viewtag": "b50baafba7078319ec97fac19fc24240"
        },
        "empoeditview9": {
            "title": "采购订单",
            "caption": "采购订单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMPOEditView9",
            "viewtag": "b55c3214a958e201031d16d01f043900"
        },
        "emeqdebugeditview": {
            "title": "事故记录编辑视图",
            "caption": "事故记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQDEBUGEditView",
            "viewtag": "b64fde81d6050c1c6970f6f45abf5e4c"
        },
        "emwocalendarview": {
            "title": "工单日历视图",
            "caption": "工单",
            "viewtype": "DECALENDARVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOCalendarView",
            "viewtag": "b6f4fd6dfffc79e0e18f8141f471803e"
        },
        "emitemallgridview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemAllGridView",
            "viewtag": "b7612baa72c389a1b2c3d136b0057eaf"
        },
        "emeqsparemapeditview": {
            "title": "备件包引用",
            "caption": "备件包引用",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREMAPEditView",
            "viewtag": "b872a2756b8f76de6e3952d936540661"
        },
        "emeqdebuggridview": {
            "title": "事故记录",
            "caption": "事故记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQDebugGridView",
            "viewtag": "b93057b3a295c324b6fe6863261ddaa9"
        },
        "appindex": {
            "title": "iBizEAM",
            "caption": "iBizEAM",
            "viewtype": "APPINDEXVIEW",
            "viewmodule": "eam_core",
            "viewname": "appIndex",
            "viewtag": "b9b3c2b39c97ba3485d12d9c376d7d33"
        },
        "emserviceevleditview": {
            "title": "服务商评估编辑视图",
            "caption": "服务商评估",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMServiceEvlEditView",
            "viewtag": "ba03c7160de1b430812272ff2aa94ea5"
        },
        "emberthpickupgridview": {
            "title": "泊位选择表格视图",
            "caption": "泊位",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMBERTHPickupGridView",
            "viewtag": "ba16ccec3253b3118e6bcdd92b6a5f51"
        },
        "empodetailgridview9": {
            "title": "订单条目",
            "caption": "订单条目",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMPODetailGridView9",
            "viewtag": "baf50834cf79de22220e45b98b24c41c"
        },
        "emenconsumeditview9": {
            "title": "能耗信息",
            "caption": "能耗信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMENCONSUMEditView9",
            "viewtag": "bb3bc0bd2796c3a6ae4d4f9a366a8249"
        },
        "emwplistpodgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListPodGridView",
            "viewtag": "bc2c30f6e51323b24935ac2b0d1806af"
        },
        "emitemtradetreeexpview": {
            "title": "物品交易树导航视图",
            "caption": "物品交易",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTradeTreeExpView",
            "viewtag": "bca931f90bf97b982d3935d161d4deb9"
        },
        "emwo_osceditview_editmode": {
            "title": "外委工单",
            "caption": "外委工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMWO_OSCEditView_EditMode",
            "viewtag": "bcd52da6e9a6f6a8926bc580ab14de07"
        },
        "emproducteditview9_editmode": {
            "title": "试用品信息",
            "caption": "试用品信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMPRODUCTEditView9_EditMode",
            "viewtag": "bd6e771add09e4cb9e4cd706831a6975"
        },
        "emwo_dpwovieweditview": {
            "title": "点检工单",
            "caption": "点检工单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWO_DPWOViewEditView",
            "viewtag": "be71c986633a114f2c752f1d712538a2"
        },
        "pfteampickupview": {
            "title": "班组数据选择视图",
            "caption": "班组",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFTEAMPickupView",
            "viewtag": "bef3ee446a9a19347eb0cb1e57878741"
        },
        "emitempleditview9": {
            "title": "损溢单",
            "caption": "损溢单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMITEMPLEditView9",
            "viewtag": "bf23ad24bfa1863e039772a1f1c00bb5"
        },
        "emeqsparemapdashboardview9": {
            "title": "备件包引用数据看板视图",
            "caption": "备件包引用",
            "viewtype": "DEPORTALVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareMapDashboardView9",
            "viewtag": "bfd25465a4ecf52ac58b74910577c033"
        },
        "empopickupgridview": {
            "title": "订单选择表格视图",
            "caption": "订单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPOPickupGridView",
            "viewtag": "bfef160d5a90d65b6d6b1cf7b5c04dfa"
        },
        "emassetcleareditview": {
            "title": "资产清盘记录",
            "caption": "资产清盘记录",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLEAREditView",
            "viewtag": "c04fd8c9c27dd0b85e796601359fae58"
        },
        "emasseteditview9_editmode": {
            "title": "资产信息",
            "caption": "资产信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_as",
            "viewname": "EMASSETEditView9_EditMode",
            "viewtag": "c0ea0882b97aeba72dd56b5314a4c191"
        },
        "emserviceeditview": {
            "title": "服务商编辑视图",
            "caption": "服务商",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMSERVICEEditView",
            "viewtag": "c125be1b61d36b5d49e1263a5992117b"
        },
        "emplaneditview": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPlanEditView",
            "viewtag": "c17d90d625c98847c2bb51cfed7fb546"
        },
        "emrfodedashboardview9": {
            "title": "现象数据看板视图",
            "caption": "现象",
            "viewtype": "DEPORTALVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEDashboardView9",
            "viewtag": "c207074659809cb328b32eeb9a06e68d"
        },
        "emstoreeditview_editmode": {
            "title": "仓库",
            "caption": "仓库",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStoreEditView_EditMode",
            "viewtag": "c27bfe990d149afe2ec27543630b7990"
        },
        "emservicefileview9": {
            "title": "服务商实体编辑视图（部件视图）<主信息>附件信息",
            "caption": "服务商",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceFileView9",
            "viewtag": "c368e5b951e06a9ed548bc2950fa0a40"
        },
        "pfdeptgridview": {
            "title": "部门",
            "caption": "部门",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFDeptGridView",
            "viewtag": "c3dc47de07e89c6ce14ee04951c5d91e"
        },
        "emitemrineditview9": {
            "title": "入库单信息",
            "caption": "入库单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMITEMRINEditView9",
            "viewtag": "c4534d77bfe66462a22661acb1783456"
        },
        "emeqlctgssgridview_cqyj": {
            "title": "钢丝绳位置",
            "caption": "钢丝绳位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_yj",
            "viewname": "EMEQLCTGSSGridView_CQYJ",
            "viewtag": "c505271bd9640daa2ea25255b24ca8b6"
        },
        "emassetclasseditview_4778": {
            "title": "ASSET资产类别信息",
            "caption": "ASSET资产类别信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLASSEditView_4778",
            "viewtag": "c5225b0c008cfb05c29468662954e875"
        },
        "emeqlocationeditview9": {
            "title": "位置",
            "caption": "位置",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLOCATIONEditView9",
            "viewtag": "c6098f1792e5751f61e4eddade272fe9"
        },
        "empodetailcloseddetailgridview": {
            "title": "订单条目",
            "caption": "订单条目",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPODetailClosedDetailGridView",
            "viewtag": "c68e9be69347618c36518133912eb7c0"
        },
        "emserviceeditview9_editmode": {
            "title": "PUR服务商信息",
            "caption": "PUR服务商信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "EMSERVICEEditView9_EditMode",
            "viewtag": "c758a28b4e386b736da420a5f66af404"
        },
        "emeqmaintanceeditview9": {
            "title": "维修记录信息",
            "caption": "维修记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQMAINTANCEEditView9",
            "viewtag": "c82c51213ba9d242dcf21890a74f5bcf"
        },
        "emitempickupgridview": {
            "title": "物品选择表格视图",
            "caption": "物品",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMITEMPickupGridView",
            "viewtag": "c85bed1ffbfeeccbdf2d115ca32aafe0"
        },
        "emitemringridview": {
            "title": "入库单",
            "caption": "入库单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemRInGridView",
            "viewtag": "c8bffb1278eddb48efc2625865b307f9"
        },
        "emplanpickupgridview": {
            "title": "计划选择表格视图",
            "caption": "计划",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPLANPickupGridView",
            "viewtag": "c98f0a1b66adbd986643d7d4d78b0232"
        },
        "emeqsetuppickupgridview": {
            "title": "更换安装选择表格视图",
            "caption": "更换安装",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_act",
            "viewname": "EMEQSETUPPickupGridView",
            "viewtag": "cabff129e300b0aacabf8563886a6f95"
        },
        "emitempuseeditview9_editmode": {
            "title": "领料单",
            "caption": "领料单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMITEMPUSEEditView9_EditMode",
            "viewtag": "caeb019319cbd24bdee9a439f1876835"
        },
        "emeitirespickupgridview": {
            "title": "轮胎清单选择表格视图",
            "caption": "轮胎清单",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEITIRESPickupGridView",
            "viewtag": "cb5018876b531cb8fe5414a428ae07d7"
        },
        "emwotreeexpview": {
            "title": "工单树导航视图",
            "caption": "工单",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWOTreeExpView",
            "viewtag": "cb5cf460ddd14ba04b8844c6ac0301ca"
        },
        "empodetailgridview": {
            "title": "订单条目",
            "caption": "订单条目",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMPODETAILGridView",
            "viewtag": "cbb15afbdfeb9c6267dbdd2ee4fcb0a8"
        },
        "emequipeditview9": {
            "title": "设备档案编辑视图",
            "caption": "设备档案",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEquipEditView9",
            "viewtag": "cc418211f125cd085825ddedb118f59e"
        },
        "emplaneditview_editmode": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPlanEditView_EditMode",
            "viewtag": "cc45a14a2a571f2d5eb916c0595962ed"
        },
        "emassetcleargridview_5564": {
            "title": "资产清盘记录",
            "caption": "资产清盘记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLEARGridView_5564",
            "viewtag": "ccc5721160cee3833b89319b9359c426"
        },
        "emitemrouttoconfirmgridview": {
            "title": "退货单",
            "caption": "退货单-待确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemROutToConfirmGridView",
            "viewtag": "cd0955e3cde129c4e3436a9750e4c394"
        },
        "emserviceevlgridview9": {
            "title": "服务商评估",
            "caption": "服务商评估",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMServiceEvlGridView9",
            "viewtag": "cd82fbd7b70649ee68efb4d43822c8e3"
        },
        "emplanoptionview": {
            "title": "计划选项操作视图",
            "caption": "计划",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPLANOptionView",
            "viewtag": "cda54777e21b6912299786271dc8b75f"
        },
        "emeqdebugeditview9_editmode": {
            "title": "调试记录信息",
            "caption": "调试记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_act",
            "viewname": "EMEQDEBUGEditView9_EditMode",
            "viewtag": "cdfd4f75cceaedb5f905374547581eae"
        },
        "pfdepteditview_editmode": {
            "title": "部门",
            "caption": "部门",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "PFDeptEditView_EditMode",
            "viewtag": "cf0ed1d76148abb0435c7fa5fd9556b1"
        },
        "emitemroutconfirmedgridview": {
            "title": "退货单",
            "caption": "退货单-已确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemROutConfirmedGridView",
            "viewtag": "cf6e6534d282f602b7c418bbe8d6eb31"
        },
        "emeqsparemapequipgridview9": {
            "title": "备件包应用",
            "caption": "备件包应用",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareMapEquipGridView9",
            "viewtag": "cf9bb5f4b9b036cc056707c5ecdcb7a1"
        },
        "emenpickupgridview": {
            "title": "能源选择表格视图",
            "caption": "能源",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMENPickupGridView",
            "viewtag": "cfcb33f4e84ddd4f2e21297d45a0e66c"
        },
        "emeqsparepickupview": {
            "title": "备件包数据选择视图",
            "caption": "备件包",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREPickupView",
            "viewtag": "d0eb65021d65496d58473308e744be7b"
        },
        "emdrwgoptionview": {
            "title": "文档选项操作视图",
            "caption": "文档",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMDRWGOptionView",
            "viewtag": "d1392668c0220ae17dd93eb04c41121b"
        },
        "emmachinecategorypickupgridview": {
            "title": "机种编号选择表格视图",
            "caption": "机种编号",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMMACHINECATEGORYPickupGridView",
            "viewtag": "d16ca3055fdcbeb1dd03272f872898bf"
        },
        "emrfodemapdataview": {
            "title": "现象引用数据视图",
            "caption": "现象引用",
            "viewtype": "DEDATAVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEMapDataView",
            "viewtag": "d16d98122c04f4d523d044b507d4bd8d"
        },
        "emeqlocationgridview": {
            "title": "位置",
            "caption": "位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLocationGridView",
            "viewtag": "d2da14cc2c2f2f9594a808f65c8609b0"
        },
        "emeqsparedetailtabexpview": {
            "title": "备件包明细分页导航视图",
            "caption": "备件包明细",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQSpareDetailTabExpView",
            "viewtag": "d4c8d6d04f054ee9b06a699563768688"
        },
        "emplantemplpickupgridview": {
            "title": "计划模板选择表格视图",
            "caption": "计划模板",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANTEMPLPickupGridView",
            "viewtag": "d69ba26d56a9ea91392719849724261e"
        },
        "emeqspareeditview": {
            "title": "备件包信息",
            "caption": "备件包信息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSpareEditView",
            "viewtag": "d6c472d7d44a4bd791fd33e86100cb18"
        },
        "emplaneqinfo": {
            "title": "计划",
            "caption": "计划",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPlanEQInfo",
            "viewtag": "d70c2491c2d1b9a2f2aa8414a28f3be2"
        },
        "emeqlctfdjgridview": {
            "title": "发动机位置",
            "caption": "发动机位置",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLCTFDJGridView",
            "viewtag": "d84963d959f370f98cfc86031dff753e"
        },
        "emeqsparegridview": {
            "title": "备件包",
            "caption": "备件包",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQSPAREGridView",
            "viewtag": "d8d0b0b0463c04d887f65581fea3079f"
        },
        "emitempusetabexpview": {
            "title": "领料单分页导航视图",
            "caption": "领料单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPUseTabExpView",
            "viewtag": "d967f62616c3b2b1aa01832376d2c871"
        },
        "emitempldraftgridview": {
            "title": "损溢单",
            "caption": "损溢单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPLDraftGridView",
            "viewtag": "da0b4c4a5c61c94ead8c5ffee4896be0"
        },
        "emeitirespickupview": {
            "title": "轮胎清单数据选择视图",
            "caption": "轮胎清单",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEITIRESPickupView",
            "viewtag": "da1ba460f87b2b7aeceb64c67aa370fd"
        },
        "emitemrouteditview9": {
            "title": "退货单信息",
            "caption": "退货单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMITEMROUTEditView9",
            "viewtag": "daafb3465e23223525f4c4d4da08fae3"
        },
        "emitemtabexpview": {
            "title": "物品分页导航视图",
            "caption": "物品",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTabExpView",
            "viewtag": "db76369286d8c4592646b38515118b7a"
        },
        "emoutputgridview": {
            "title": "能力",
            "caption": "能力",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMOUTPUTGridView",
            "viewtag": "dba2d5c1ab40d93af771607d88bf02d7"
        },
        "emeqlctgsstreeexpview": {
            "title": "钢丝绳位置树导航",
            "caption": "钢丝绳位置树导航",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQLCTGSSTreeExpView",
            "viewtag": "dcde22194e0b645b20bb462200eab5a8"
        },
        "emwplistcancelgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListCancelGridView",
            "viewtag": "dd3df1db97148f3f0c03bda90b88f993"
        },
        "emitempleditview9_editmode": {
            "title": "损溢单",
            "caption": "损溢单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMPLEditView9_EditMode",
            "viewtag": "dd6fd047f64a53d578abfd414add9c22"
        },
        "emitemtypepickuptreeview": {
            "title": "物品类型选择树视图",
            "caption": "物品类型",
            "viewtype": "DEPICKUPTREEVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemTypePickupTreeView",
            "viewtag": "dedfb1b28d503e46accd1cc438fe748a"
        },
        "emeqkeepeditview9_editmode": {
            "title": "保养记录信息",
            "caption": "保养记录信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_act",
            "viewname": "EMEQKEEPEditView9_EditMode",
            "viewtag": "dfa9ac688227ba6d2564d9c344d17c32"
        },
        "emitemtypetreepickupview": {
            "title": "物品类型数据选择视图",
            "caption": "物品类型",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeTreePickupView",
            "viewtag": "e09dd594c043992ccaa517655083aeb4"
        },
        "emoutputrctgridview": {
            "title": "产能",
            "caption": "产能",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_en",
            "viewname": "EMOutputRctGridView",
            "viewtag": "e0c4f65469feff3713fa23d7bf6f0bd5"
        },
        "emeqlocationpickupview": {
            "title": "位置数据选择视图",
            "caption": "位置",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQLOCATIONPickupView",
            "viewtag": "e2909898adfb9623415ed5e4b6017115"
        },
        "emplanequipgridview9": {
            "title": "计划表格视图",
            "caption": "计划",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMPlanEquipGridView9",
            "viewtag": "e3dfb7fd2c7145d1438de7ffa260befa"
        },
        "emitempusegridview": {
            "title": "领料单",
            "caption": "领料单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemPUseGridView",
            "viewtag": "e3ea9d4287f91ef15f3491f205f476ac"
        },
        "emrfodeeditview9": {
            "title": "现象信息",
            "caption": "现象信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMRFODEEditView9",
            "viewtag": "e5d5066723553d17862ae4faabf9b991"
        },
        "emeqsparemapgridview9": {
            "title": "备件包应用",
            "caption": "备件包应用",
            "viewtype": "DEGRIDVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMEQSPAREMAPGridView9",
            "viewtag": "e654b593fddf72a64b374ff937f17d63"
        },
        "pfempgridview": {
            "title": "职员",
            "caption": "职员",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "PFEmpGridView",
            "viewtag": "e8c58b31c30d9279e27709bcb278902b"
        },
        "emitembaseinfoview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemBaseInfoView",
            "viewtag": "e999eac16e6eb06c82ba55e20288ae26"
        },
        "emstoreeditview_9924": {
            "title": "仓库",
            "caption": "仓库",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStoreEditView_9924",
            "viewtag": "ea862e1cf91c975a230ee463122cf0bb"
        },
        "emwplistwaitcostgridview": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMWPListWaitCostGridView",
            "viewtag": "eafe822c90e88d38a1bae761a8bc2020"
        },
        "emitemrouteditview": {
            "title": "退货单编辑视图",
            "caption": "退货单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMROUTEditView",
            "viewtag": "ebbca2434d7f93a265abe76223f0be66"
        },
        "emobjectpickupview": {
            "title": "对象数据选择视图",
            "caption": "对象",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMOBJECTPickupView",
            "viewtag": "ec1dc7662e938aa1087ab591c9e91c0d"
        },
        "emplandetaileditview": {
            "title": "计划步骤",
            "caption": "计划步骤",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPLANDETAILEditView",
            "viewtag": "ec3414365aa6d8dbf2307cded48232db"
        },
        "pfteameditview": {
            "title": "班组",
            "caption": "班组",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "PFTEAMEditView",
            "viewtag": "ecd3905f3ae08142169d7861c44f82ef"
        },
        "emplantemplgridview": {
            "title": "计划模板",
            "caption": "计划模板",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_plan",
            "viewname": "EMPlanTemplGridView",
            "viewtag": "ed69711ada89a236e083dbf6e670592a"
        },
        "emstoretreeexpview": {
            "title": "仓库库位",
            "caption": "仓库库位",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMStoreTreeExpView",
            "viewtag": "ed77b0d2dfcda3182684e13be3bec9ee"
        },
        "pfcontracteditview9_editmode": {
            "title": "合同信息",
            "caption": "合同信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_wp",
            "viewname": "PFCONTRACTEditView9_EditMode",
            "viewtag": "ed8fe6b716f26b909943e18d050cd957"
        },
        "emitemprtntoconfirmgridview": {
            "title": "还料单",
            "caption": "还料单-待确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPRtnToConfirmGridView",
            "viewtag": "ee8cd635e561bbe73491e5b96602b585"
        },
        "emequiptabexpview": {
            "title": "设备档案分页导航视图",
            "caption": "设备档案",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEquipTabExpView",
            "viewtag": "ef1522e1c2cbe45896bd18c4f2a6774e"
        },
        "emeqtypetreepickupview": {
            "title": "设备类型数据选择视图",
            "caption": "设备类型",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQTypeTreePickupView",
            "viewtag": "efa1b7a3173659a86961ff91c5eba2c4"
        },
        "emitemprtneditview9": {
            "title": "还料单信息",
            "caption": "还料单信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMITEMPRTNEditView9",
            "viewtag": "f05aed62de0d686c8c52a8b8bfc5b96a"
        },
        "emeqwlwleditview": {
            "title": "设备运行日志编辑视图",
            "caption": "设备运行日志",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQWLWLEditView",
            "viewtag": "f0df843ba15b6e6632e0317b94f0f116"
        },
        "emeqmpgridview": {
            "title": "设备仪表表格视图",
            "caption": "设备仪表",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQMPGridView",
            "viewtag": "f19dbbe510c90444c2ec99780efcf24e"
        },
        "emitemprtntabexpview": {
            "title": "还料单分页导航视图",
            "caption": "还料单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPRtnTabExpView",
            "viewtag": "f3a6f6fe8d43df1e39f299745ca22a7e"
        },
        "emstorepartgridview": {
            "title": "仓库库位",
            "caption": "仓库库位",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMSTOREPARTGridView",
            "viewtag": "f493994398e4ecbc5974e4e026634da7"
        },
        "pfdeptpickupview": {
            "title": "部门数据选择视图",
            "caption": "部门",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_pf",
            "viewname": "PFDEPTPickupView",
            "viewtag": "f4dd610f8a72c9304f48d878d969547b"
        },
        "emeqahcalendarexpview": {
            "title": "活动历史日历导航视图",
            "caption": "活动历史",
            "viewtype": "DECALENDAREXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQAHCalendarExpView",
            "viewtag": "f5143ccda9228bfd3f3db9d7bdb59eaf"
        },
        "emitempuseeditview9": {
            "title": "领料单",
            "caption": "领料单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMITEMPUSEEditView9",
            "viewtag": "f51b89d6e74d1d393deb1e9b7985082e"
        },
        "emeqwlgridview": {
            "title": "设备运行日志表格视图",
            "caption": "设备运行日志",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMEQWLGridView",
            "viewtag": "f6b13c85a105b9aff4df73d0c81e28de"
        },
        "emapplyeditview": {
            "title": "外委申请",
            "caption": "外委申请",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_wo",
            "viewname": "EMAPPLYEditView",
            "viewtag": "f6be77a4355df423a03ecd98de66eec0"
        },
        "emequipeditview": {
            "title": "设备档案",
            "caption": "设备档案",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_eq",
            "viewname": "EMEQUIPEditView",
            "viewtag": "f7e24c4a7de19c8d74c1cc18bbebe5b4"
        },
        "emeqlctgsseditview9_editmode": {
            "title": "钢丝绳位置编辑视图",
            "caption": "钢丝绳位置",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_yj",
            "viewname": "EMEQLCTGSSEditView9_EditMode",
            "viewtag": "f9ce353eef595aa88a3abb7017017723"
        },
        "emassetclasspickupview": {
            "title": "资产类别数据选择视图",
            "caption": "资产类别",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLASSPickupView",
            "viewtag": "f9cf5c209e6d1447bf92d42bf4af4bba"
        },
        "emplanpickupview": {
            "title": "计划数据选择视图",
            "caption": "计划",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMPLANPickupView",
            "viewtag": "faa92111474981684349c243e9823fc7"
        },
        "emstoregridview_7848": {
            "title": "仓库",
            "caption": "仓库",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStoreGridView_7848",
            "viewtag": "fac546b9086999cdcc36f5204393007a"
        },
        "emrfodetypeeditview": {
            "title": "现象分类",
            "caption": "现象分类",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFODETYPEEditView",
            "viewtag": "fb58b86e0fa4171fd062a6cb2ff34bb9"
        },
        "emstockeditview": {
            "title": "库存明细",
            "caption": "库存明细",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStockEditView",
            "viewtag": "fbfe650e1269b71dab6462ffe67e57ee"
        },
        "emitemroutgridview": {
            "title": "退货单",
            "caption": "退货单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMITEMROUTGridView",
            "viewtag": "fc153775f37ee7a931d74c12bd96845e"
        },
        "emrfodeeditview9_editmode": {
            "title": "现象信息",
            "caption": "现象信息",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_rfo",
            "viewname": "EMRFODEEditView9_EditMode",
            "viewtag": "fc39fd346a1f9ddc6ec297200c57f93f"
        },
        "emitemprtndraftgridview": {
            "title": "还料单",
            "caption": "还料单-草稿",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPRtnDraftGridView",
            "viewtag": "fcbf2ca79580bc8e72de16e0e4c558c4"
        },
        "emmachmodelpickupview": {
            "title": "机型数据选择视图",
            "caption": "机型",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMMACHMODELPickupView",
            "viewtag": "fd2134118dc7f9c1db7086af8eb794bb"
        },
        "emitemplconfirmedgridview": {
            "title": "损溢单",
            "caption": "损溢单-已确认",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMItemPLConfirmedGridView",
            "viewtag": "fd336000a0b5da24b43cbf2e026dea87"
        },
        "emassetcleargridview": {
            "title": "资产清盘记录",
            "caption": "资产清盘记录",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_as",
            "viewname": "EMASSETCLEARGridView",
            "viewtag": "fd55e5e3b51b3c4b6f06063896c0b5db"
        },
        "emitemtypeinfotreeexpview": {
            "title": "物品",
            "caption": "物品",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMItemTypeInfoTreeExpView",
            "viewtag": "fdcfa3c586faed54a13d1e0938037f71"
        },
        "emserviceevlgridview": {
            "title": "服务商评估",
            "caption": "服务商评估",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_wp",
            "viewname": "EMServiceEvlGridView",
            "viewtag": "fdd707e083da5df48c8a4df45b885d44"
        },
        "emstoregridview": {
            "title": "仓库",
            "caption": "仓库",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "eam_mat",
            "viewname": "EMStoreGridView",
            "viewtag": "fdf497f9dbb3079b914b7e5999a06d8e"
        },
        "emservicetabexpview": {
            "title": "服务商分页导航视图",
            "caption": "服务商",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMServiceTabExpView",
            "viewtag": "fdffb1e68bdce0efe79820ffe6c56121"
        },
        "emitemcseditview9": {
            "title": "调整单",
            "caption": "调整单",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMITEMCSEditView9",
            "viewtag": "ff3339e0459fb79b19ddfa628b8b9dd0"
        },
        "emacclasspickupview": {
            "title": "总帐科目数据选择视图",
            "caption": "总帐科目",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMACCLASSPickupView",
            "viewtag": "ff79be163370f7ee32ebd0e6536689c4"
        },
        "emassetpickupview": {
            "title": "资产数据选择视图",
            "caption": "资产",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "eam_core",
            "viewname": "EMASSETPickupView",
            "viewtag": "ff7b91fcf517cd05e71d65349cbf042e"
        },
        "emwplistfillcosteditview9": {
            "title": "采购申请",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW9",
            "viewmodule": "eam_core",
            "viewname": "EMWPListFillCostEditView9",
            "viewtag": "fffc67dbee5765294d5c0337ae0055d9"
        }
    }];
});