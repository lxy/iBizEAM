/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'woid',
          prop: 'woid',
          dataType: 'PICKUP',
        },
        {
          name: 'nval',
          prop: 'nval',
          dataType: 'FLOAT',
        },
        {
          name: 'lastval',
          prop: 'lastval',
          dataType: 'FLOAT',
        },
        {
          name: 'enid',
          prop: 'enid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emenconsumname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emenconsumid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emenconsumid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'curval',
          prop: 'curval',
          dataType: 'FLOAT',
        },
        {
          name: 'amount',
          prop: 'amount',
          dataType: 'FLOAT',
        },
        {
          name: 'price',
          prop: 'price',
          dataType: 'CURRENCY',
        },
        {
          name: 'edate',
          prop: 'edate',
          dataType: 'DATETIME',
        },
        {
          name: 'description',
          prop: 'description',
          dataType: 'TEXT',
        },
        {
          name: 'vrate',
          prop: 'vrate',
          dataType: 'FLOAT',
        },
        {
          name: 'woname',
          prop: 'woname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'bdate',
          prop: 'bdate',
          dataType: 'DATETIME',
        },
        {
          name: 'objname',
          prop: 'objname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'enname',
          prop: 'enname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'emenconsum',
          prop: 'emenconsumid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}