/**
 * EqEnByYearLine 部件模型
 *
 * @export
 * @class EqEnByYearLineModel
 */
export default class EqEnByYearLineModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof EqEnByYearLineModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'price',
      },
      {
        name: 'deptid',
      },
      {
        name: 'emenconsum',
        prop: 'emenconsumid',
      },
      {
        name: 'pusetype',
      },
      {
        name: 'vrate',
      },
      {
        name: 'orgid',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'description',
      },
      {
        name: 'createdate',
      },
      {
        name: 'amount',
      },
      {
        name: 'createman',
      },
      {
        name: 'updateman',
      },
      {
        name: 'nval',
      },
      {
        name: 'lastval',
      },
      {
        name: 'deptname',
      },
      {
        name: 'enable',
      },
      {
        name: 'curval',
      },
      {
        name: 'emenconsumname',
      },
      {
        name: 'bdate',
      },
      {
        name: 'edate',
      },
      {
        name: 'enprice',
      },
      {
        name: 'objname',
      },
      {
        name: 'itemmtypename',
      },
      {
        name: 'itemname',
      },
      {
        name: 'woname',
      },
      {
        name: 'unitname',
      },
      {
        name: 'itemprice',
      },
      {
        name: 'equipcode',
      },
      {
        name: 'equipname',
      },
      {
        name: 'enname',
      },
      {
        name: 'sname',
      },
      {
        name: 'itemtypeid',
      },
      {
        name: 'itembtypeid',
      },
      {
        name: 'itembtypename',
      },
      {
        name: 'itemid',
      },
      {
        name: 'itemmtypeid',
      },
      {
        name: 'objid',
      },
      {
        name: 'equipid',
      },
      {
        name: 'woid',
      },
      {
        name: 'enid',
      },
      {
        name: 'energytypeid',
      },
    ]
  }


}
