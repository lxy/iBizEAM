/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'kpcode',
          prop: 'kpcode',
          dataType: 'TEXT',
        },
        {
          name: 'normalrefval',
          prop: 'normalrefval',
          dataType: 'TEXT',
        },
        {
          name: 'kptypeid',
          prop: 'kptypeid',
          dataType: 'SSCODELIST',
        },
        {
          name: 'kpscope',
          prop: 'kpscope',
          dataType: 'TEXT',
        },
        {
          name: 'emeqkpname',
          prop: 'emeqkpname',
          dataType: 'TEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'emeqkpname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emeqkpid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emeqkpid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emeqkp',
          prop: 'emeqkpid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}