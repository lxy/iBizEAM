/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'unitname',
          prop: 'unitname',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'empname',
          prop: 'empname',
          dataType: 'TEXT',
        },
        {
          name: 'avgtsfee',
          prop: 'avgtsfee',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'itemid',
          prop: 'itemid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emitemrinname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'emitemrinid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'emitemrinid',
          dataType: 'GUID',
        },
        {
          name: 'amount',
          prop: 'amount',
          dataType: 'CURRENCY',
        },
        {
          name: 'wfstate',
          prop: 'wfstate',
          dataType: 'WFSTATE',
        },
        {
          name: 'psum',
          prop: 'psum',
          dataType: 'FLOAT',
        },
        {
          name: 'itemname',
          prop: 'itemname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'shf',
          prop: 'shf',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'podetailid',
          prop: 'podetailid',
          dataType: 'PICKUP',
        },
        {
          name: 'labservicename',
          prop: 'labservicename',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'storepartid',
          prop: 'storepartid',
          dataType: 'PICKUP',
        },
        {
          name: 'storeid',
          prop: 'storeid',
          dataType: 'PICKUP',
        },
        {
          name: 'rinstate',
          prop: 'rinstate',
          dataType: 'NSCODELIST',
        },
        {
          name: 'storepartname',
          prop: 'storepartname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'emitemrinid',
          prop: 'emitemrinid',
          dataType: 'GUID',
        },
        {
          name: 'wfstep',
          prop: 'wfstep',
          dataType: 'SSCODELIST',
        },
        {
          name: 'podetailname',
          prop: 'podetailname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'price',
          prop: 'price',
          dataType: 'CURRENCY',
        },
        {
          name: 'emserviceid',
          prop: 'emserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'civo',
          prop: 'civo',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'porempname',
          prop: 'porempname',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'sumall',
          prop: 'sumall',
          dataType: 'FLOAT',
        },
        {
          name: 'storename',
          prop: 'storename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'batcode',
          prop: 'batcode',
          dataType: 'TEXT',
        },
        {
          name: 'sempname',
          prop: 'sempname',
          dataType: 'TEXT',
        },
        {
          name: 'sdate',
          prop: 'sdate',
          dataType: 'DATETIME',
        },
        {
          name: 'emitemrin',
          prop: 'emitemrinid',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}