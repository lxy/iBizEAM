/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'orgid',
          prop: 'orgid',
          dataType: 'SSCODELIST',
        },
        {
          name: 'teamcode',
          prop: 'teamcode',
          dataType: 'TEXT',
        },
        {
          name: 'description',
          prop: 'description',
          dataType: 'TEXT',
        },
        {
          name: 'teamtypeid',
          prop: 'teamtypeid',
          dataType: 'SSCODELIST',
        },
        {
          name: 'teamcapacity',
          prop: 'teamcapacity',
          dataType: 'INT',
        },
        {
          name: 'pfteamname',
          prop: 'pfteamname',
          dataType: 'TEXT',
        },
        {
          name: 'teamfn',
          prop: 'teamfn',
          dataType: 'TEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'pfteamname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'pfteamid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'pfteamid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'pfteam',
          prop: 'pfteamid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}