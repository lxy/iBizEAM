import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * ItemTreeExpViewtreeexpbar 部件服务对象
 *
 * @export
 * @class ItemTreeExpViewtreeexpbarService
 */
export default class ItemTreeExpViewtreeexpbarService extends ControlService {
}