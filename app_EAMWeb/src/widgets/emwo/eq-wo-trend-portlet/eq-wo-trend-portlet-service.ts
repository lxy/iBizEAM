import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * EqWoTrend 部件服务对象
 *
 * @export
 * @class EqWoTrendService
 */
export default class EqWoTrendService extends ControlService {
}
