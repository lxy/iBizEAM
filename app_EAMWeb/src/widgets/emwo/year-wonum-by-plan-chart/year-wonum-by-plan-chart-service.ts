import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMWOService from '@/service/emwo/emwo-service';
import YearWONumByPlanModel from './year-wonum-by-plan-chart-model';


/**
 * YearWONumByPlan 部件服务对象
 *
 * @export
 * @class YearWONumByPlanService
 */
export default class YearWONumByPlanService extends ControlService {

    /**
     * 工单服务对象
     *
     * @type {EMWOService}
     * @memberof YearWONumByPlanService
     */
    public appEntityService: EMWOService = new EMWOService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof YearWONumByPlanService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of YearWONumByPlanService.
     * 
     * @param {*} [opts={}]
     * @memberof YearWONumByPlanService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new YearWONumByPlanModel();
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof YearWONumByPlanService
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }
}