/**
 * Main6 部件模型
 *
 * @export
 * @class Main6Model
 */
export default class Main6Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main6GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main6GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'acclassid',
          prop: 'acclassid',
          dataType: 'PICKUP',
        },
        {
          name: 'val',
          prop: 'val',
          dataType: 'TEXT',
        },
        {
          name: 'rteamname',
          prop: 'rteamname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rfodeid',
          prop: 'rfodeid',
          dataType: 'PICKUP',
        },
        {
          name: 'emwoname',
          prop: 'emwoname',
          dataType: 'TEXT',
        },
        {
          name: 'rfomoid',
          prop: 'rfomoid',
          dataType: 'PICKUP',
        },
        {
          name: 'expiredate',
          prop: 'expiredate',
          dataType: 'DATETIME',
        },
        {
          name: 'rfocaid',
          prop: 'rfocaid',
          dataType: 'PICKUP',
        },
        {
          name: 'wopid',
          prop: 'wopid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emwoname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emwoid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emwoid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emwoid',
          prop: 'emwoid',
          dataType: 'GUID',
        },
        {
          name: 'wooriid',
          prop: 'wooriid',
          dataType: 'PICKUP',
        },
        {
          name: 'rdeptname',
          prop: 'rdeptname',
          dataType: 'TEXT',
        },
        {
          name: 'priority',
          prop: 'priority',
          dataType: 'NSCODELIST',
        },
        {
          name: 'rempname',
          prop: 'rempname',
          dataType: 'TEXT',
        },
        {
          name: 'worklength',
          prop: 'worklength',
          dataType: 'FLOAT',
        },
        {
          name: 'wodate',
          prop: 'wodate',
          dataType: 'DATETIME',
        },
        {
          name: 'wpersonname',
          prop: 'wpersonname',
          dataType: 'TEXT',
        },
        {
          name: 'dpid',
          prop: 'dpid',
          dataType: 'PICKUP',
        },
        {
          name: 'wopname',
          prop: 'wopname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'srfdatatype',
          prop: 'emwotype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'rteamid',
          prop: 'rteamid',
          dataType: 'PICKUP',
        },
        {
          name: 'emwotype',
          prop: 'emwotype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'rservicename',
          prop: 'rservicename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rserviceid',
          prop: 'rserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'regionbegindate',
          prop: 'regionbegindate',
          dataType: 'DATETIME',
        },
        {
          name: 'wresult',
          prop: 'wresult',
          dataType: 'TEXT',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'wooriname',
          prop: 'wooriname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'regionenddate',
          prop: 'regionenddate',
          dataType: 'DATETIME',
        },
        {
          name: 'wotype',
          prop: 'wotype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'rfoacid',
          prop: 'rfoacid',
          dataType: 'PICKUP',
        },
        {
          name: 'dpname',
          prop: 'dpname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'wogroup',
          prop: 'wogroup',
          dataType: 'SSCODELIST',
        },
        {
          name: 'objname',
          prop: 'objname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'emwo',
          prop: 'emwoid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}