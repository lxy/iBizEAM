import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMWOService from '@/service/emwo/emwo-service';
import CalendarExpViewcalendarexpbarModel from './calendar-exp-viewcalendarexpbar-calendarexpbar-model';


/**
 * CalendarExpViewcalendarexpbar 部件服务对象
 *
 * @export
 * @class CalendarExpViewcalendarexpbarService
 */
export default class CalendarExpViewcalendarexpbarService extends ControlService {

    /**
     * 工单服务对象
     *
     * @type {EMWOService}
     * @memberof CalendarExpViewcalendarexpbarService
     */
    public appEntityService: EMWOService = new EMWOService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof CalendarExpViewcalendarexpbarService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of CalendarExpViewcalendarexpbarService.
     * 
     * @param {*} [opts={}]
     * @memberof CalendarExpViewcalendarexpbarService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new CalendarExpViewcalendarexpbarModel();
    }

}