/**
 * WoCalendarExp 部件模型
 *
 * @export
 * @class WoCalendarExpModel
 */
export default class WoCalendarExpModel {

	/**
	 * 日历项类型
	 *
	 * @returns {any[]}
	 * @memberof WoCalendarExpCalendarexpbar_calendarMode
	 */
	public itemType: string = "";

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof WoCalendarExpCalendarexpbar_calendarMode
	 */
	public getDataItems(): any[] {
     let dataItems: any = [
          // 前端新增修改标识，新增为"0",修改为"1"或未设值
          {
            name: 'srffrontuf',
            prop: 'srffrontuf',
            dataType: 'TEXT',
          },
          {
            name: 'color',
          },
          {
            name: 'textColor',
          },
          {
            name: 'itemType',
          },
          {
            name: 'query',
            prop: 'query',
          },
      ];
      switch(this.itemType){
          case "EMWO_INNER":
              dataItems = [...dataItems,
                  {
                    name: 'emwo_inner',
                    prop: 'emwo_innerid'
                  },
                  {
                    name: 'title',
                    prop: 'emwo_innername'
                  },
                  {
                    name:'start',
                    prop:'regionbegindate'
                  },
                  {
                    name:'end',
                    prop:'regionenddate'
                  },
              ];
              break;
          case "EMWO_OSC":
              dataItems = [...dataItems,
                  {
                    name: 'emwo_osc',
                    prop: 'emwo_oscid'
                  },
                  {
                    name: 'title',
                    prop: 'emwo_oscname'
                  },
                  {
                    name:'start',
                    prop:'regionbegindate'
                  },
                  {
                    name:'end',
                    prop:'regionenddate'
                  },
              ];
              break;
          case "EMWO_DP":
              dataItems = [...dataItems,
                  {
                    name: 'emwo_dp',
                    prop: 'emwo_dpid'
                  },
                  {
                    name: 'title',
                    prop: 'emwo_dpname'
                  },
                  {
                    name:'start',
                    prop:'regionbegindate'
                  },
                  {
                    name:'end',
                    prop:'regionenddate'
                  },
              ];
              break;
          case "EMWO_EN":
              dataItems = [...dataItems,
                  {
                    name: 'emwo_en',
                    prop: 'emwo_enid'
                  },
                  {
                    name: 'title',
                    prop: 'emwo_enname'
                  },
                  {
                    name:'start',
                    prop:'regionbegindate'
                  },
                  {
                    name:'end',
                    prop:'regionenddate'
                  },
              ];
              break;
      }
      return dataItems;
	}

}