import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * EQInfo 部件服务对象
 *
 * @export
 * @class EQInfoService
 */
export default class EQInfoService extends ControlService {
}
