/**
 * QuickCreate 部件模型
 *
 * @export
 * @class QuickCreateModel
 */
export default class QuickCreateModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof QuickCreateModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emplanid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emplanname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emplanname',
        prop: 'emplanname',
        dataType: 'TEXT',
      },
      {
        name: 'plantype',
        prop: 'plantype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'planstate',
        prop: 'planstate',
        dataType: 'NSCODELIST',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'TEXT',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'TEXT',
      },
      {
        name: 'rdeptname',
        prop: 'rdeptname',
        dataType: 'TEXT',
      },
      {
        name: 'emplanid',
        prop: 'emplanid',
        dataType: 'GUID',
      },
      {
        name: 'emplan',
        prop: 'emplanid',
        dataType: 'FONTKEY',
      },
    ]
  }

}