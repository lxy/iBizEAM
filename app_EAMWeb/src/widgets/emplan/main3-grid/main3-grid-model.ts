/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main3GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main3GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'acclassid',
          prop: 'acclassid',
          dataType: 'PICKUP',
        },
        {
          name: 'rteamname',
          prop: 'rteamname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'mdate',
          prop: 'mdate',
          dataType: 'DATETIME',
        },
        {
          name: 'eqstoplength',
          prop: 'eqstoplength',
          dataType: 'FLOAT',
        },
        {
          name: 'emplanname',
          prop: 'emplanname',
          dataType: 'TEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'emplanname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'emplanid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'emplanid',
          dataType: 'GUID',
        },
        {
          name: 'plandesc',
          prop: 'plandesc',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'rdeptname',
          prop: 'rdeptname',
          dataType: 'TEXT',
        },
        {
          name: 'activelengths',
          prop: 'activelengths',
          dataType: 'FLOAT',
        },
        {
          name: 'rempname',
          prop: 'rempname',
          dataType: 'TEXT',
        },
        {
          name: 'mpersonname',
          prop: 'mpersonname',
          dataType: 'TEXT',
        },
        {
          name: 'dpid',
          prop: 'dpid',
          dataType: 'PICKUP',
        },
        {
          name: 'recvpersonname',
          prop: 'recvpersonname',
          dataType: 'TEXT',
        },
        {
          name: 'rteamid',
          prop: 'rteamid',
          dataType: 'PICKUP',
        },
        {
          name: 'emplanid',
          prop: 'emplanid',
          dataType: 'GUID',
        },
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'planstate',
          prop: 'planstate',
          dataType: 'NSCODELIST',
        },
        {
          name: 'plantype',
          prop: 'plantype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'rservicename',
          prop: 'rservicename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rserviceid',
          prop: 'rserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'prefee',
          prop: 'prefee',
          dataType: 'CURRENCY',
        },
        {
          name: 'dptype',
          prop: 'dptype',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'mtflag',
          prop: 'mtflag',
          dataType: 'YESNO',
        },
        {
          name: 'dpname',
          prop: 'dpname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'plantemplid',
          prop: 'plantemplid',
          dataType: 'PICKUP',
        },
        {
          name: 'archive',
          prop: 'archive',
          dataType: 'SMCODELIST',
        },
        {
          name: 'objname',
          prop: 'objname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'emplan',
          prop: 'emplanid',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}