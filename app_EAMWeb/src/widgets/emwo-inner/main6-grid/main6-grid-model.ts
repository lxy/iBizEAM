/**
 * Main6 部件模型
 *
 * @export
 * @class Main6Model
 */
export default class Main6Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main6GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main6GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'acclassid',
          prop: 'acclassid',
          dataType: 'PICKUP',
        },
        {
          name: 'rteamname',
          prop: 'rteamname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rfodeid',
          prop: 'rfodeid',
          dataType: 'PICKUP',
        },
        {
          name: 'eqstoplength',
          prop: 'eqstoplength',
          dataType: 'FLOAT',
        },
        {
          name: 'rfomoid',
          prop: 'rfomoid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfocaid',
          prop: 'rfocaid',
          dataType: 'PICKUP',
        },
        {
          name: 'woteam',
          prop: 'woteam',
          dataType: 'TEXT',
        },
        {
          name: 'wopid',
          prop: 'wopid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emwo_innername',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emwo_innerid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emwo_innerid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emeqlctgssid',
          prop: 'emeqlctgssid',
          dataType: 'PICKUP',
        },
        {
          name: 'wooriid',
          prop: 'wooriid',
          dataType: 'PICKUP',
        },
        {
          name: 'rdeptname',
          prop: 'rdeptname',
          dataType: 'TEXT',
        },
        {
          name: 'priority',
          prop: 'priority',
          dataType: 'NSCODELIST',
        },
        {
          name: 'rempname',
          prop: 'rempname',
          dataType: 'TEXT',
        },
        {
          name: 'worklength',
          prop: 'worklength',
          dataType: 'FLOAT',
        },
        {
          name: 'wodate',
          prop: 'wodate',
          dataType: 'DATETIME',
        },
        {
          name: 'wpersonname',
          prop: 'wpersonname',
          dataType: 'TEXT',
        },
        {
          name: 'dpid',
          prop: 'dpid',
          dataType: 'PICKUP',
        },
        {
          name: 'wopname',
          prop: 'wopname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'emwo_innername',
          prop: 'emwo_innername',
          dataType: 'TEXT',
        },
        {
          name: 'rteamid',
          prop: 'rteamid',
          dataType: 'PICKUP',
        },
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'emeitiresid',
          prop: 'emeitiresid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'cplanflag',
          prop: 'cplanflag',
          dataType: 'YESNO',
        },
        {
          name: 'rserviceid',
          prop: 'rserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'regionbegindate',
          prop: 'regionbegindate',
          dataType: 'DATETIME',
        },
        {
          name: 'waitbuy',
          prop: 'waitbuy',
          dataType: 'FLOAT',
        },
        {
          name: 'wresult',
          prop: 'wresult',
          dataType: 'TEXT',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'wfstep',
          prop: 'wfstep',
          dataType: 'SSCODELIST',
        },
        {
          name: 'wooriname',
          prop: 'wooriname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'regionenddate',
          prop: 'regionenddate',
          dataType: 'DATETIME',
        },
        {
          name: 'mfee',
          prop: 'mfee',
          dataType: 'CURRENCY',
        },
        {
          name: 'waitmodi',
          prop: 'waitmodi',
          dataType: 'FLOAT',
        },
        {
          name: 'wotype',
          prop: 'wotype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'rfoacid',
          prop: 'rfoacid',
          dataType: 'PICKUP',
        },
        {
          name: 'wogroup',
          prop: 'wogroup',
          dataType: 'SSCODELIST',
        },
        {
          name: 'emeqlcttiresid',
          prop: 'emeqlcttiresid',
          dataType: 'PICKUP',
        },
        {
          name: 'emwo_innerid',
          prop: 'emwo_innerid',
          dataType: 'GUID',
        },
        {
          name: 'objname',
          prop: 'objname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'emwo_inner',
          prop: 'emwo_innerid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}