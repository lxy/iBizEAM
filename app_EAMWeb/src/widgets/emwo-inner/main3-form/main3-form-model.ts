/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emwo_innerid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emwo_innername',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'emwo_innername',
        prop: 'emwo_innername',
        dataType: 'TEXT',
      },
      {
        name: 'emwo_innerid',
        prop: 'emwo_innerid',
        dataType: 'GUID',
      },
      {
        name: 'wogroup',
        prop: 'wogroup',
        dataType: 'SSCODELIST',
      },
      {
        name: 'activelengths',
        prop: 'activelengths',
        dataType: 'FLOAT',
      },
      {
        name: 'wotype',
        prop: 'wotype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'emeqlcttiresname',
        prop: 'emeqlcttiresname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'closeflag',
        prop: 'closeflag',
        dataType: 'YESNO',
      },
      {
        name: 'emeqlctgssname',
        prop: 'emeqlctgssname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'emeitiresname',
        prop: 'emeitiresname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'priority',
        prop: 'priority',
        dataType: 'NSCODELIST',
      },
      {
        name: 'yxcb',
        prop: 'yxcb',
        dataType: 'TEXT',
      },
      {
        name: 'archive',
        prop: 'archive',
        dataType: 'SMCODELIST',
      },
      {
        name: 'wodesc',
        prop: 'wodesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'wodate',
        prop: 'wodate',
        dataType: 'DATETIME',
      },
      {
        name: 'regionbegindate',
        prop: 'regionbegindate',
        dataType: 'DATETIME',
      },
      {
        name: 'regionenddate',
        prop: 'regionenddate',
        dataType: 'DATETIME',
      },
      {
        name: 'eqstoplength',
        prop: 'eqstoplength',
        dataType: 'FLOAT',
      },
      {
        name: 'waitmodi',
        prop: 'waitmodi',
        dataType: 'FLOAT',
      },
      {
        name: 'waitbuy',
        prop: 'waitbuy',
        dataType: 'FLOAT',
      },
      {
        name: 'worklength',
        prop: 'worklength',
        dataType: 'FLOAT',
      },
      {
        name: 'wpersonid',
        prop: 'wpersonid',
        dataType: 'TEXT',
      },
      {
        name: 'wpersonname',
        prop: 'wpersonname',
        dataType: 'TEXT',
      },
      {
        name: 'wresult',
        prop: 'wresult',
        dataType: 'TEXT',
      },
      {
        name: 'mfee',
        prop: 'mfee',
        dataType: 'CURRENCY',
      },
      {
        name: 'cplanflag',
        prop: 'cplanflag',
        dataType: 'YESNO',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'TEXT',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'TEXT',
      },
      {
        name: 'recvpersonid',
        prop: 'recvpersonid',
        dataType: 'TEXT',
      },
      {
        name: 'recvpersonname',
        prop: 'recvpersonname',
        dataType: 'TEXT',
      },
      {
        name: 'mpersonid',
        prop: 'mpersonid',
        dataType: 'TEXT',
      },
      {
        name: 'mpersonname',
        prop: 'mpersonname',
        dataType: 'TEXT',
      },
      {
        name: 'rdeptname',
        prop: 'rdeptname',
        dataType: 'TEXT',
      },
      {
        name: 'rteamname',
        prop: 'rteamname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rfodename',
        prop: 'rfodename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rfomoname',
        prop: 'rfomoname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rfocaname',
        prop: 'rfocaname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rfoacname',
        prop: 'rfoacname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'wooriname',
        prop: 'wooriname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'wooritype',
        prop: 'wooritype',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'wopname',
        prop: 'wopname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'prefee',
        prop: 'prefee',
        dataType: 'CURRENCY',
      },
      {
        name: 'mdate',
        prop: 'mdate',
        dataType: 'DATETIME',
      },
      {
        name: 'expiredate',
        prop: 'expiredate',
        dataType: 'DATETIME',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'emeqlctgssid',
        prop: 'emeqlctgssid',
        dataType: 'PICKUP',
      },
      {
        name: 'rfodeid',
        prop: 'rfodeid',
        dataType: 'PICKUP',
      },
      {
        name: 'wooriid',
        prop: 'wooriid',
        dataType: 'PICKUP',
      },
      {
        name: 'objid',
        prop: 'objid',
        dataType: 'PICKUP',
      },
      {
        name: 'emeitiresid',
        prop: 'emeitiresid',
        dataType: 'PICKUP',
      },
      {
        name: 'rfoacid',
        prop: 'rfoacid',
        dataType: 'PICKUP',
      },
      {
        name: 'rfomoid',
        prop: 'rfomoid',
        dataType: 'PICKUP',
      },
      {
        name: 'emeqlcttiresid',
        prop: 'emeqlcttiresid',
        dataType: 'PICKUP',
      },
      {
        name: 'rfocaid',
        prop: 'rfocaid',
        dataType: 'PICKUP',
      },
      {
        name: 'equipid',
        prop: 'equipid',
        dataType: 'PICKUP',
      },
      {
        name: 'wopid',
        prop: 'wopid',
        dataType: 'PICKUP',
      },
      {
        name: 'rteamid',
        prop: 'rteamid',
        dataType: 'PICKUP',
      },
      {
        name: 'emwo_inner',
        prop: 'emwo_innerid',
        dataType: 'FONTKEY',
      },
    ]
  }

}