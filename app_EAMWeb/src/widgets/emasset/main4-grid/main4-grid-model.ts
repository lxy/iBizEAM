/**
 * Main4 部件模型
 *
 * @export
 * @class Main4Model
 */
export default class Main4Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main4GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main4GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'acclassid',
          prop: 'acclassid',
          dataType: 'PICKUP',
        },
        {
          name: 'assetclassid',
          prop: 'assetclassid',
          dataType: 'PICKUP',
        },
        {
          name: 'eqmodelcode',
          prop: 'eqmodelcode',
          dataType: 'TEXT',
        },
        {
          name: 'disdesc',
          prop: 'disdesc',
          dataType: 'TEXT',
        },
        {
          name: 'unitname',
          prop: 'unitname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'ytzj',
          prop: 'ytzj',
          dataType: 'FLOAT',
        },
        {
          name: 'contractid',
          prop: 'contractid',
          dataType: 'PICKUP',
        },
        {
          name: 'empname',
          prop: 'empname',
          dataType: 'TEXT',
        },
        {
          name: 'eqserialcode',
          prop: 'eqserialcode',
          dataType: 'TEXT',
        },
        {
          name: 'blsystemdesc',
          prop: 'blsystemdesc',
          dataType: 'TEXT',
        },
        {
          name: 'labserviceid',
          prop: 'labserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'assetlct',
          prop: 'assetlct',
          dataType: 'TEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'emassetname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emassetid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emassetid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'unitid',
          prop: 'unitid',
          dataType: 'PICKUP',
        },
        {
          name: 'rempname',
          prop: 'rempname',
          dataType: 'TEXT',
        },
        {
          name: 'disdate',
          prop: 'disdate',
          dataType: 'DATE',
        },
        {
          name: 'replacerate',
          prop: 'replacerate',
          dataType: 'FLOAT',
        },
        {
          name: 'emassetname',
          prop: 'emassetname',
          dataType: 'TEXT',
        },
        {
          name: 'assetcode',
          prop: 'assetcode',
          dataType: 'TEXT',
        },
        {
          name: 'deptname',
          prop: 'deptname',
          dataType: 'TEXT',
        },
        {
          name: 'assettype',
          prop: 'assettype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'originalcost',
          prop: 'originalcost',
          dataType: 'CURRENCY',
        },
        {
          name: 'eqstartdate',
          prop: 'eqstartdate',
          dataType: 'DATE',
        },
        {
          name: 'purchdate',
          prop: 'purchdate',
          dataType: 'DATE',
        },
        {
          name: 'keyattparam',
          prop: 'keyattparam',
          dataType: 'TEXT',
        },
        {
          name: 'eqlife',
          prop: 'eqlife',
          dataType: 'FLOAT',
        },
        {
          name: 'rserviceid',
          prop: 'rserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'mserviceid',
          prop: 'mserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'num',
          prop: 'num',
          dataType: 'TEXT',
        },
        {
          name: 'eqlocationid',
          prop: 'eqlocationid',
          dataType: 'PICKUP',
        },
        {
          name: 'now',
          prop: 'now',
          dataType: 'FLOAT',
        },
        {
          name: 'assetclassname',
          prop: 'assetclassname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'mgrdeptname',
          prop: 'mgrdeptname',
          dataType: 'TEXT',
        },
        {
          name: 'discost',
          prop: 'discost',
          dataType: 'CURRENCY',
        },
        {
          name: 'emasset',
          prop: 'emassetid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}