/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'unitrate',
          prop: 'unitrate',
          dataType: 'FLOAT',
        },
        {
          name: 'empname',
          prop: 'empname',
          dataType: 'TEXT',
        },
        {
          name: 'avgtsfee',
          prop: 'avgtsfee',
          dataType: 'CURRENCY',
        },
        {
          name: 'itemid',
          prop: 'itemid',
          dataType: 'PICKUP',
        },
        {
          name: 'equips',
          prop: 'equips',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'useto',
          prop: 'useto',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'srfmajortext',
          prop: 'empodetailname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'empodetailid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'empodetailid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'rdate',
          prop: 'rdate',
          dataType: 'DATETIME',
        },
        {
          name: 'amount',
          prop: 'amount',
          dataType: 'CURRENCY',
        },
        {
          name: 'psum',
          prop: 'psum',
          dataType: 'FLOAT',
        },
        {
          name: 'unitid',
          prop: 'unitid',
          dataType: 'PICKUP',
        },
        {
          name: 'itemname',
          prop: 'itemname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'orderflag',
          prop: 'orderflag',
          dataType: 'INT',
        },
        {
          name: 'runitid',
          prop: 'runitid',
          dataType: 'PICKUP',
        },
        {
          name: 'description',
          prop: 'description',
          dataType: 'TEXT',
        },
        {
          name: 'poname',
          prop: 'poname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rempname',
          prop: 'rempname',
          dataType: 'TEXT',
        },
        {
          name: 'rsum',
          prop: 'rsum',
          dataType: 'FLOAT',
        },
        {
          name: 'shf',
          prop: 'shf',
          dataType: 'CURRENCY',
        },
        {
          name: 'avgtaxfee',
          prop: 'avgtaxfee',
          dataType: 'CURRENCY',
        },
        {
          name: 'totalprice',
          prop: 'totalprice',
          dataType: 'FLOAT',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'wplistid',
          prop: 'wplistid',
          dataType: 'PICKUP',
        },
        {
          name: 'rprice',
          prop: 'rprice',
          dataType: 'CURRENCY',
        },
        {
          name: 'attprice',
          prop: 'attprice',
          dataType: 'INT',
        },
        {
          name: 'price',
          prop: 'price',
          dataType: 'CURRENCY',
        },
        {
          name: 'civo',
          prop: 'civo',
          dataType: 'TEXT',
        },
        {
          name: 'runitname',
          prop: 'runitname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'poid',
          prop: 'poid',
          dataType: 'PICKUP',
        },
        {
          name: 'wplistname',
          prop: 'wplistname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'updatedate',
          prop: 'updatedate',
          dataType: 'DATETIME',
        },
        {
          name: 'objname',
          prop: 'objname',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'podetailstate',
          prop: 'podetailstate',
          dataType: 'NSCODELIST',
        },
        {
          name: 'empodetail',
          prop: 'empodetailid',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}