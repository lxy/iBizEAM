/**
 * PoDetailLine 部件模型
 *
 * @export
 * @class PoDetailLineModel
 */
export default class PoDetailLineModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof PoDetailLineDb_sysportlet2_chartMode
	 */
	public getDataItems(): any[] {
		return [
			{
			name:'size',
			prop:'size'
			},
			{
			name:'query',
			prop:'query'
			},
			{
			name:'page',
			prop:'page'
			},
			{
			name:'sort',
			prop:'sort'
			}
		]
	}

}