import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * CostByItem 部件服务对象
 *
 * @export
 * @class CostByItemService
 */
export default class CostByItemService extends ControlService {
}
