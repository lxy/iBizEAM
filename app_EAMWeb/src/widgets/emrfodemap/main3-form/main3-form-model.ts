/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emrfodemapid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emrfodemapname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'rfodename',
        prop: 'rfodename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'refobjname',
        prop: 'refobjname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rfodeid',
        prop: 'rfodeid',
        dataType: 'PICKUP',
      },
      {
        name: 'refobjid',
        prop: 'refobjid',
        dataType: 'PICKUP',
      },
      {
        name: 'emrfodemapid',
        prop: 'emrfodemapid',
        dataType: 'GUID',
      },
      {
        name: 'emrfodemap',
        prop: 'emrfodemapid',
        dataType: 'FONTKEY',
      },
    ]
  }

}