/**
 * Main4 部件模型
 *
 * @export
 * @class Main4Model
 */
export default class Main4Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main4Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emeqsetupid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emeqsetupname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'eitiresid',
        prop: 'eitiresid',
        dataType: 'PICKUP',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'activedate',
        prop: 'activedate',
        dataType: 'DATE',
      },
      {
        name: 'woname',
        prop: 'woname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'activedesc',
        prop: 'activedesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'activeadesc',
        prop: 'activeadesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'activebdesc',
        prop: 'activebdesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'regionbegindate',
        prop: 'regionbegindate',
        dataType: 'DATETIME',
      },
      {
        name: 'regionenddate',
        prop: 'regionenddate',
        dataType: 'DATETIME',
      },
      {
        name: 'activelengths',
        prop: 'activelengths',
        dataType: 'FLOAT',
      },
      {
        name: 'eqstoplength',
        prop: 'eqstoplength',
        dataType: 'FLOAT',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'TEXT',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'TEXT',
      },
      {
        name: 'rdeptname',
        prop: 'rdeptname',
        dataType: 'TEXT',
      },
      {
        name: 'rteamname',
        prop: 'rteamname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rservicename',
        prop: 'rservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'prefee',
        prop: 'prefee',
        dataType: 'CURRENCY',
      },
      {
        name: 'mfee',
        prop: 'mfee',
        dataType: 'CURRENCY',
      },
      {
        name: 'pfee',
        prop: 'pfee',
        dataType: 'CURRENCY',
      },
      {
        name: 'sfee',
        prop: 'sfee',
        dataType: 'CURRENCY',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'content',
        prop: 'content',
        dataType: 'HTMLTEXT',
      },
      {
        name: 'pic5',
        prop: 'pic5',
        dataType: 'TEXT',
      },
      {
        name: 'pic4',
        prop: 'pic4',
        dataType: 'TEXT',
      },
      {
        name: 'pic3',
        prop: 'pic3',
        dataType: 'TEXT',
      },
      {
        name: 'pic1',
        prop: 'pic1',
        dataType: 'TEXT',
      },
      {
        name: 'pic2',
        prop: 'pic2',
        dataType: 'TEXT',
      },
      {
        name: 'pic',
        prop: 'pic',
        dataType: 'TEXT',
      },
      {
        name: 'emeqsetupid',
        prop: 'emeqsetupid',
        dataType: 'GUID',
      },
      {
        name: 'objid',
        prop: 'objid',
        dataType: 'PICKUP',
      },
      {
        name: 'rserviceid',
        prop: 'rserviceid',
        dataType: 'PICKUP',
      },
      {
        name: 'woid',
        prop: 'woid',
        dataType: 'PICKUP',
      },
      {
        name: 'equipid',
        prop: 'equipid',
        dataType: 'PICKUP',
      },
      {
        name: 'rteamid',
        prop: 'rteamid',
        dataType: 'PICKUP',
      },
      {
        name: 'emeqsetup',
        prop: 'emeqsetupid',
        dataType: 'FONTKEY',
      },
    ]
  }

}