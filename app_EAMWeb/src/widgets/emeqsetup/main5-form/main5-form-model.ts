/**
 * Main5 部件模型
 *
 * @export
 * @class Main5Model
 */
export default class Main5Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main5Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emeqsetupid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emeqsetupname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'eitiresid',
        prop: 'eitiresid',
        dataType: 'PICKUP',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'activedate',
        prop: 'activedate',
        dataType: 'DATE',
      },
      {
        name: 'woname',
        prop: 'woname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'activedesc',
        prop: 'activedesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'activeadesc',
        prop: 'activeadesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'activebdesc',
        prop: 'activebdesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'regionbegindate',
        prop: 'regionbegindate',
        dataType: 'DATETIME',
      },
      {
        name: 'regionenddate',
        prop: 'regionenddate',
        dataType: 'DATETIME',
      },
      {
        name: 'activelengths',
        prop: 'activelengths',
        dataType: 'FLOAT',
      },
      {
        name: 'eqstoplength',
        prop: 'eqstoplength',
        dataType: 'FLOAT',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'TEXT',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'TEXT',
      },
      {
        name: 'rdeptname',
        prop: 'rdeptname',
        dataType: 'TEXT',
      },
      {
        name: 'rteamname',
        prop: 'rteamname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rservicename',
        prop: 'rservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'prefee',
        prop: 'prefee',
        dataType: 'CURRENCY',
      },
      {
        name: 'mfee',
        prop: 'mfee',
        dataType: 'CURRENCY',
      },
      {
        name: 'pfee',
        prop: 'pfee',
        dataType: 'CURRENCY',
      },
      {
        name: 'sfee',
        prop: 'sfee',
        dataType: 'CURRENCY',
      },
      {
        name: 'emeqsetupid',
        prop: 'emeqsetupid',
        dataType: 'GUID',
      },
      {
        name: 'emeqsetup',
        prop: 'emeqsetupid',
        dataType: 'FONTKEY',
      },
    ]
  }

}