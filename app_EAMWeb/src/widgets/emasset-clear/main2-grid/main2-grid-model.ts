/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'assetchecknum',
          prop: 'assetchecknum',
          dataType: 'INT',
        },
        {
          name: 'eqmodelcode',
          prop: 'eqmodelcode',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'purchdate',
          prop: 'purchdate',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'assetclearlct',
          prop: 'assetclearlct',
          dataType: 'TEXT',
        },
        {
          name: 'assetcheckprice',
          prop: 'assetcheckprice',
          dataType: 'CURRENCY',
        },
        {
          name: 'assetinprice',
          prop: 'assetinprice',
          dataType: 'CURRENCY',
        },
        {
          name: 'assetoutprice',
          prop: 'assetoutprice',
          dataType: 'CURRENCY',
        },
        {
          name: 'assetcleardate',
          prop: 'assetcleardate',
          dataType: 'DATE',
        },
        {
          name: 'srfmajortext',
          prop: 'emassetclearname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emassetclearid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emassetclearid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emassetclearname',
          prop: 'emassetclearname',
          dataType: 'TEXT',
        },
        {
          name: 'assetoutnum',
          prop: 'assetoutnum',
          dataType: 'INT',
        },
        {
          name: 'assetclearstate',
          prop: 'assetclearstate',
          dataType: 'TEXT',
        },
        {
          name: 'assetsort',
          prop: 'assetsort',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'assetclassname',
          prop: 'assetclassname',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'emassetid',
          prop: 'emassetid',
          dataType: 'PICKUP',
        },
        {
          name: 'assetinnum',
          prop: 'assetinnum',
          dataType: 'INT',
        },
        {
          name: 'emassetname',
          prop: 'emassetname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'assetcode',
          prop: 'assetcode',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'originalcost',
          prop: 'originalcost',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'emassetclear',
          prop: 'emassetclearid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}