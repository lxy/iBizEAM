/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'acclassid',
          prop: 'acclassid',
          dataType: 'PICKUP',
        },
        {
          name: 'storepartid',
          prop: 'storepartid',
          dataType: 'PICKUP',
        },
        {
          name: 'storeid',
          prop: 'storeid',
          dataType: 'PICKUP',
        },
        {
          name: 'unitname',
          prop: 'unitname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'labserviceid',
          prop: 'labserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'itemtypeid',
          prop: 'itemtypeid',
          dataType: 'PICKUP',
        },
        {
          name: 'itemcode',
          prop: 'itemcode',
          dataType: 'TEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'emitemname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'emitemid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'emitemid',
          dataType: 'GUID',
        },
        {
          name: 'amount',
          prop: 'amount',
          dataType: 'CURRENCY',
        },
        {
          name: 'mserviceid',
          prop: 'mserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'emcabid',
          prop: 'emcabid',
          dataType: 'PICKUP',
        },
        {
          name: 'stocksum',
          prop: 'stocksum',
          dataType: 'FLOAT',
        },
        {
          name: 'unitid',
          prop: 'unitid',
          dataType: 'PICKUP',
        },
        {
          name: 'itemtypename',
          prop: 'itemtypename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'emitemname',
          prop: 'emitemname',
          dataType: 'TEXT',
        },
        {
          name: 'emitem',
          prop: 'emitemid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}