/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'poinfo',
          prop: 'poinfo',
          dataType: 'TEXT',
        },
        {
          name: 'labserviceid',
          prop: 'labserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'updateman',
          prop: 'updateman',
          dataType: 'TEXT',
        },
        {
          name: 'updatedate',
          prop: 'updatedate',
          dataType: 'DATETIME',
        },
        {
          name: 'srfmajortext',
          prop: 'emponame',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'empoid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'empoid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'empo',
          prop: 'empoid',
        },
      {
        name: 'n_empoid_eq',
        prop: 'n_empoid_eq',
        dataType: 'GUID',
      },
      {
        name: 'n_postate_eq',
        prop: 'n_postate_eq',
        dataType: 'NSCODELIST',
      },
      {
        name: 'n_payway_eq',
        prop: 'n_payway_eq',
        dataType: 'SSCODELIST',
      },
      {
        name: 'empo',
        prop: 'empoid',
        dataType: 'FONTKEY',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}