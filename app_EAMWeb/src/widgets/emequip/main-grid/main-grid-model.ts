/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'acclassid',
          prop: 'acclassid',
          dataType: 'PICKUP',
        },
        {
          name: 'embrandid',
          prop: 'embrandid',
          dataType: 'PICKUP',
        },
        {
          name: 'emequipname',
          prop: 'emequipname',
          dataType: 'TEXT',
        },
        {
          name: 'contractid',
          prop: 'contractid',
          dataType: 'PICKUP',
        },
        {
          name: 'rserviceid',
          prop: 'rserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'labserviceid',
          prop: 'labserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'emmachinecategoryid',
          prop: 'emmachinecategoryid',
          dataType: 'PICKUP',
        },
        {
          name: 'eqstate',
          prop: 'eqstate',
          dataType: 'SSCODELIST',
        },
        {
          name: 'srfmajortext',
          prop: 'emequipname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'emequipid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'emequipid',
          dataType: 'GUID',
        },
        {
          name: 'emberthid',
          prop: 'emberthid',
          dataType: 'PICKUP',
        },
        {
          name: 'mserviceid',
          prop: 'mserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'eqlocationname',
          prop: 'eqlocationname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'eqlocationid',
          prop: 'eqlocationid',
          dataType: 'PICKUP',
        },
        {
          name: 'equippid',
          prop: 'equippid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipcode',
          prop: 'equipcode',
          dataType: 'TEXT',
        },
        {
          name: 'eqtypename',
          prop: 'eqtypename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'assetid',
          prop: 'assetid',
          dataType: 'PICKUP',
        },
        {
          name: 'emmachmodelid',
          prop: 'emmachmodelid',
          dataType: 'PICKUP',
        },
        {
          name: 'eqtypeid',
          prop: 'eqtypeid',
          dataType: 'PICKUP',
        },
        {
          name: 'rteamid',
          prop: 'rteamid',
          dataType: 'PICKUP',
        },
        {
          name: 'emequip',
          prop: 'emequipid',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}