/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'eqlocationname',
          prop: 'eqlocationname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'eqlocationid',
          prop: 'eqlocationid',
          dataType: 'PICKUP',
        },
        {
          name: 'eqlocationpname',
          prop: 'eqlocationpname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'description',
          prop: 'description',
          dataType: 'TEXT',
        },
        {
          name: 'eqlocationpid',
          prop: 'eqlocationpid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emeqlctmapname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emeqlctmapid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emeqlctmapid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emeqlctmap',
          prop: 'emeqlctmapid',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}