/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'rfodeid',
          prop: 'rfodeid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfomoid',
          prop: 'rfomoid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfocaid',
          prop: 'rfocaid',
          dataType: 'PICKUP',
        },
        {
          name: 'emapplyid',
          prop: 'emapplyid',
          dataType: 'GUID',
        },
        {
          name: 'srfmajortext',
          prop: 'emapplyname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'emapplyid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'emapplyid',
          dataType: 'GUID',
        },
        {
          name: 'closeempname',
          prop: 'closeempname',
          dataType: 'TEXT',
        },
        {
          name: 'priority',
          prop: 'priority',
          dataType: 'NSCODELIST',
        },
        {
          name: 'applyedate',
          prop: 'applyedate',
          dataType: 'DATETIME',
        },
        {
          name: 'rempname',
          prop: 'rempname',
          dataType: 'TEXT',
        },
        {
          name: 'mpersonname',
          prop: 'mpersonname',
          dataType: 'TEXT',
        },
        {
          name: 'sfee',
          prop: 'sfee',
          dataType: 'CURRENCY',
        },
        {
          name: 'applybdate',
          prop: 'applybdate',
          dataType: 'DATETIME',
        },
        {
          name: 'pfee',
          prop: 'pfee',
          dataType: 'CURRENCY',
        },
        {
          name: 'rteamid',
          prop: 'rteamid',
          dataType: 'PICKUP',
        },
        {
          name: 'fp',
          prop: 'fp',
          dataType: 'TEXT',
        },
        {
          name: 'prefee1',
          prop: 'prefee1',
          dataType: 'CURRENCY',
        },
        {
          name: 'applytype',
          prop: 'applytype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'applydate',
          prop: 'applydate',
          dataType: 'DATETIME',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rservicename',
          prop: 'rservicename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rserviceid',
          prop: 'rserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'emapplyname',
          prop: 'emapplyname',
          dataType: 'TEXT',
        },
        {
          name: 'closedate',
          prop: 'closedate',
          dataType: 'DATETIME',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'mfee',
          prop: 'mfee',
          dataType: 'CURRENCY',
        },
        {
          name: 'prefee',
          prop: 'prefee',
          dataType: 'CURRENCY',
        },
        {
          name: 'rfoacid',
          prop: 'rfoacid',
          dataType: 'PICKUP',
        },
        {
          name: 'applystate',
          prop: 'applystate',
          dataType: 'NSCODELIST',
        },
        {
          name: 'objname',
          prop: 'objname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'emapply',
          prop: 'emapplyid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}