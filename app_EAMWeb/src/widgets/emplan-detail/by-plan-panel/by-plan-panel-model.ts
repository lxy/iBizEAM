/**
 * ByPlan 部件模型
 *
 * @export
 * @class ByPlanModel
 */
export default class ByPlanModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ByPlanModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'orderflag',
        prop: 'orderflag'
      },
      {
        name: 'emplandetailname',
        prop: 'emplandetailname'
      },
      {
        name: 'equipname',
        prop: 'equipname'
      },
      {
        name: 'objname',
        prop: 'objname'
      },
      {
        name: 'recvpersonname',
        prop: 'recvpersonname'
      }
    ]
  }
}