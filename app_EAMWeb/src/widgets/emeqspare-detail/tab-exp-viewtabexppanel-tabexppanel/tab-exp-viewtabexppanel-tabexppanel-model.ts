/**
 * TabExpViewtabexppanel 部件模型
 *
 * @export
 * @class TabExpViewtabexppanelModel
 */
export default class TabExpViewtabexppanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabexppanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'updatedate',
      },
      {
        name: 'enable',
      },
      {
        name: 'orgid',
      },
      {
        name: 'updateman',
      },
      {
        name: 'createman',
      },
      {
        name: 'emeqsparedetail',
        prop: 'emeqsparedetailid',
      },
      {
        name: 'emeqsparedetailname',
      },
      {
        name: 'createdate',
      },
      {
        name: 'description',
      },
      {
        name: 'itemname',
      },
      {
        name: 'eqsparename',
      },
      {
        name: 'itemid',
      },
      {
        name: 'eqspareid',
      },
    ]
  }


}