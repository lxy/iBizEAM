/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emitemprtnid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emitemprtnname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emitemprtnid',
        prop: 'emitemprtnid',
        dataType: 'GUID',
      },
      {
        name: 'rname',
        prop: 'rname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'itemname',
        prop: 'itemname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'empid',
        prop: 'empid',
        dataType: 'TEXT',
      },
      {
        name: 'empname',
        prop: 'empname',
        dataType: 'TEXT',
      },
      {
        name: 'sdate',
        prop: 'sdate',
        dataType: 'DATETIME',
      },
      {
        name: 'storename',
        prop: 'storename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'storepartname',
        prop: 'storepartname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'psum',
        prop: 'psum',
        dataType: 'FLOAT',
      },
      {
        name: 'price',
        prop: 'price',
        dataType: 'CURRENCY',
      },
      {
        name: 'amount',
        prop: 'amount',
        dataType: 'CURRENCY',
      },
      {
        name: 'batcode',
        prop: 'batcode',
        dataType: 'TEXT',
      },
      {
        name: 'sempid',
        prop: 'sempid',
        dataType: 'TEXT',
      },
      {
        name: 'sempname',
        prop: 'sempname',
        dataType: 'TEXT',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'storepartid',
        prop: 'storepartid',
        dataType: 'PICKUP',
      },
      {
        name: 'storeid',
        prop: 'storeid',
        dataType: 'PICKUP',
      },
      {
        name: 'rid',
        prop: 'rid',
        dataType: 'PICKUP',
      },
      {
        name: 'itemid',
        prop: 'itemid',
        dataType: 'PICKUP',
      },
      {
        name: 'emitemprtn',
        prop: 'emitemprtnid',
        dataType: 'FONTKEY',
      },
    ]
  }

}