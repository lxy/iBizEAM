/**
 * TabExpViewtabviewpanel2 部件模型
 *
 * @export
 * @class TabExpViewtabviewpanel2Model
 */
export default class TabExpViewtabviewpanel2Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabviewpanel2Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'prtntype',
      },
      {
        name: 'sapreason',
      },
      {
        name: 'emitemprtnname',
      },
      {
        name: 'sap',
      },
      {
        name: 'createman',
      },
      {
        name: 'tradestate',
      },
      {
        name: 'itemprtninfo',
      },
      {
        name: 'psum',
      },
      {
        name: 'createdate',
      },
      {
        name: 'sempid',
      },
      {
        name: 'sapreason1',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'price',
      },
      {
        name: 'orgid',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'amount',
      },
      {
        name: 'empname',
      },
      {
        name: 'emitemprtn',
        prop: 'emitemprtnid',
      },
      {
        name: 'sapcontrol',
      },
      {
        name: 'description',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'empid',
      },
      {
        name: 'sempname',
      },
      {
        name: 'updateman',
      },
      {
        name: 'sdate',
      },
      {
        name: 'enable',
      },
      {
        name: 'batcode',
      },
      {
        name: 'deptid',
      },
      {
        name: 'storename',
      },
      {
        name: 'unitname',
      },
      {
        name: 'itemname',
      },
      {
        name: 'unitid',
      },
      {
        name: 'rname',
      },
      {
        name: 'pusetype',
      },
      {
        name: 'storepartname',
      },
      {
        name: 'avgprice',
      },
      {
        name: 'itemid',
      },
      {
        name: 'storepartid',
      },
      {
        name: 'storeid',
      },
      {
        name: 'rid',
      },
    ]
  }


}