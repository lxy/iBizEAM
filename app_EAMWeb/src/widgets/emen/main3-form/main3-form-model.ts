/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emenid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emenname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'energycode',
        prop: 'energycode',
        dataType: 'TEXT',
      },
      {
        name: 'emenname',
        prop: 'emenname',
        dataType: 'TEXT',
      },
      {
        name: 'energytypeid',
        prop: 'energytypeid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'vrate',
        prop: 'vrate',
        dataType: 'FLOAT',
      },
      {
        name: 'price',
        prop: 'price',
        dataType: 'CURRENCY',
      },
      {
        name: 'itemname',
        prop: 'itemname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'energydesc',
        prop: 'energydesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'emenid',
        prop: 'emenid',
        dataType: 'GUID',
      },
      {
        name: 'itemid',
        prop: 'itemid',
        dataType: 'PICKUP',
      },
      {
        name: 'emen',
        prop: 'emenid',
        dataType: 'FONTKEY',
      },
    ]
  }

}