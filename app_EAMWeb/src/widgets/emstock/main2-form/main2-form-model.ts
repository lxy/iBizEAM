/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main2Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emstockid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emstockname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'storename',
        prop: 'storename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'storepartname',
        prop: 'storepartname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'itemname',
        prop: 'itemname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'stockcnt',
        prop: 'stockcnt',
        dataType: 'FLOAT',
      },
      {
        name: 'price',
        prop: 'price',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'amount',
        prop: 'amount',
        dataType: 'TEXT',
      },
      {
        name: 'batcode',
        prop: 'batcode',
        dataType: 'TEXT',
      },
      {
        name: 'storepartid',
        prop: 'storepartid',
        dataType: 'PICKUP',
      },
      {
        name: 'storeid',
        prop: 'storeid',
        dataType: 'PICKUP',
      },
      {
        name: 'emstockid',
        prop: 'emstockid',
        dataType: 'GUID',
      },
      {
        name: 'itemid',
        prop: 'itemid',
        dataType: 'PICKUP',
      },
      {
        name: 'emstock',
        prop: 'emstockid',
        dataType: 'FONTKEY',
      },
    ]
  }

}