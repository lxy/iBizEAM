/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main2Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emitemroutid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emitemroutname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emitemroutid',
        prop: 'emitemroutid',
        dataType: 'GUID',
      },
      {
        name: 'rname',
        prop: 'rname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'itemname',
        prop: 'itemname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'sdate',
        prop: 'sdate',
        dataType: 'DATETIME',
      },
      {
        name: 'storename',
        prop: 'storename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'storepartname',
        prop: 'storepartname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'psum',
        prop: 'psum',
        dataType: 'FLOAT',
      },
      {
        name: 'price',
        prop: 'price',
        dataType: 'CURRENCY',
      },
      {
        name: 'amount',
        prop: 'amount',
        dataType: 'CURRENCY',
      },
      {
        name: 'shf',
        prop: 'shf',
        dataType: 'CURRENCY',
      },
      {
        name: 'batcode',
        prop: 'batcode',
        dataType: 'TEXT',
      },
      {
        name: 'sempid',
        prop: 'sempid',
        dataType: 'TEXT',
      },
      {
        name: 'sempname',
        prop: 'sempname',
        dataType: 'TEXT',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'emitemrout',
        prop: 'emitemroutid',
        dataType: 'FONTKEY',
      },
    ]
  }

}