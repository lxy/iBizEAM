/**
 * EvaluateTop5 部件模型
 *
 * @export
 * @class EvaluateTop5Model
 */
export default class EvaluateTop5Model {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof EvaluateTop5Portlet_EvaluateTop5_chartMode
	 */
	public getDataItems(): any[] {
		return [
			{
			name:'size',
			prop:'size'
			},
			{
			name:'query',
			prop:'query'
			},
			{
			name:'page',
			prop:'page'
			},
			{
			name:'sort',
			prop:'sort'
			}
		]
	}

}