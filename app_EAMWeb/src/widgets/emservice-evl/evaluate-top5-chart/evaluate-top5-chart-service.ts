import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMServiceEvlService from '@/service/emservice-evl/emservice-evl-service';
import EvaluateTop5Model from './evaluate-top5-chart-model';


/**
 * EvaluateTop5 部件服务对象
 *
 * @export
 * @class EvaluateTop5Service
 */
export default class EvaluateTop5Service extends ControlService {

    /**
     * 服务商评估服务对象
     *
     * @type {EMServiceEvlService}
     * @memberof EvaluateTop5Service
     */
    public appEntityService: EMServiceEvlService = new EMServiceEvlService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof EvaluateTop5Service
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of EvaluateTop5Service.
     * 
     * @param {*} [opts={}]
     * @memberof EvaluateTop5Service
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new EvaluateTop5Model();
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EvaluateTop5Service
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }
}