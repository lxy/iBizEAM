/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'evlresult2',
          prop: 'evlresult2',
          dataType: 'INT',
        },
        {
          name: 'evlresult3',
          prop: 'evlresult3',
          dataType: 'INT',
        },
        {
          name: 'evlresult1',
          prop: 'evlresult1',
          dataType: 'INT',
        },
        {
          name: 'evlresult6',
          prop: 'evlresult6',
          dataType: 'INT',
        },
        {
          name: 'evlresult7',
          prop: 'evlresult7',
          dataType: 'INT',
        },
        {
          name: 'evldate',
          prop: 'evldate',
          dataType: 'DATE',
        },
        {
          name: 'evlresult4',
          prop: 'evlresult4',
          dataType: 'INT',
        },
        {
          name: 'empname',
          prop: 'empname',
          dataType: 'TEXT',
        },
        {
          name: 'evlresult5',
          prop: 'evlresult5',
          dataType: 'INT',
        },
        {
          name: 'servicename',
          prop: 'servicename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'serviceevlstate',
          prop: 'serviceevlstate',
          dataType: 'SSCODELIST',
        },
        {
          name: 'evlmark',
          prop: 'evlmark',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'srfmajortext',
          prop: 'emserviceevlname',
          dataType: 'TEXT',
        },
        {
          name: 'serviceid',
          prop: 'serviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emserviceevlid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emserviceevlid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'evlresult8',
          prop: 'evlresult8',
          dataType: 'INT',
        },
        {
          name: 'evlresult9',
          prop: 'evlresult9',
          dataType: 'INT',
        },
        {
          name: 'description',
          prop: 'description',
          dataType: 'TEXT',
        },
        {
          name: 'evlresult',
          prop: 'evlresult',
          dataType: 'INT',
        },
        {
          name: 'evlregion',
          prop: 'evlregion',
          dataType: 'SSCODELIST',
        },
        {
          name: 'emserviceevl',
          prop: 'emserviceevlid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}