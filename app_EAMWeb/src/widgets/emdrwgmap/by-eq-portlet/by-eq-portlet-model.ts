/**
 * ByEQ 部件模型
 *
 * @export
 * @class ByEQModel
 */
export default class ByEQModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ByEQModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'updateman',
      },
      {
        name: 'createdate',
      },
      {
        name: 'emdrwgmapname',
      },
      {
        name: 'description',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'emdrwgmap',
        prop: 'emdrwgmapid',
      },
      {
        name: 'enable',
      },
      {
        name: 'orgid',
      },
      {
        name: 'createman',
      },
      {
        name: 'refobjname',
      },
      {
        name: 'drwgname',
      },
      {
        name: 'refobjid',
      },
      {
        name: 'drwgid',
      },
    ]
  }


}
