import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMItemTradeService from '@/service/emitem-trade/emitem-trade-service';
import YearNumByItemModel from './year-num-by-item-chart-model';


/**
 * YearNumByItem 部件服务对象
 *
 * @export
 * @class YearNumByItemService
 */
export default class YearNumByItemService extends ControlService {

    /**
     * 物品交易服务对象
     *
     * @type {EMItemTradeService}
     * @memberof YearNumByItemService
     */
    public appEntityService: EMItemTradeService = new EMItemTradeService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof YearNumByItemService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of YearNumByItemService.
     * 
     * @param {*} [opts={}]
     * @memberof YearNumByItemService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new YearNumByItemModel();
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof YearNumByItemService
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }
}