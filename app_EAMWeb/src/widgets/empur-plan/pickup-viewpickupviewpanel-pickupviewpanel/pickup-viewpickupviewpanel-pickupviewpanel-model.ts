/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'yearfrom',
      },
      {
        name: 'assessreport',
      },
      {
        name: 'empurplanname',
      },
      {
        name: 'updateman',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'years',
      },
      {
        name: 'msitemtype',
      },
      {
        name: 'empurplan',
        prop: 'empurplanid',
      },
      {
        name: 'createman',
      },
      {
        name: 'planstate',
      },
      {
        name: 'acceptancedate',
      },
      {
        name: 'cocnt',
      },
      {
        name: 'enable',
      },
      {
        name: 'yearto',
      },
      {
        name: 'nowamount',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'servicecode',
      },
      {
        name: 'acceptanceresult',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'puramount',
      },
      {
        name: 'trackrule',
      },
      {
        name: 'm3q',
      },
      {
        name: 'description',
      },
      {
        name: 'nowcnt',
      },
      {
        name: 'coamount',
      },
      {
        name: 'orgid',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'createdate',
      },
      {
        name: 'rempid',
      },
      {
        name: 'rempname',
      },
      {
        name: 'istrackok',
      },
      {
        name: 'contractscan',
      },
      {
        name: 'pursum',
      },
      {
        name: 'unitname',
      },
      {
        name: 'itemtypename',
      },
      {
        name: 'embidinquiryname',
      },
      {
        name: 'itemtypeid',
      },
      {
        name: 'embidinquiryid',
      },
      {
        name: 'unitid',
      },
    ]
  }


}