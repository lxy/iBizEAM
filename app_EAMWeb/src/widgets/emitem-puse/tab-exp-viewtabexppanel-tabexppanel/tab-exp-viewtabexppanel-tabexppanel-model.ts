/**
 * TabExpViewtabexppanel 部件模型
 *
 * @export
 * @class TabExpViewtabexppanelModel
 */
export default class TabExpViewtabexppanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabexppanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'remark',
      },
      {
        name: 'emitempusename',
      },
      {
        name: 'itempuseinfo',
      },
      {
        name: 'apprempname',
      },
      {
        name: 'deltype',
      },
      {
        name: 'empid',
      },
      {
        name: 'aempid',
      },
      {
        name: 'opinion',
      },
      {
        name: 'stocknum',
      },
      {
        name: 'approknum',
      },
      {
        name: 'equips',
      },
      {
        name: 'psum',
      },
      {
        name: 'orgid',
      },
      {
        name: 'aempname',
      },
      {
        name: 'price',
      },
      {
        name: 'batcode',
      },
      {
        name: 'apprempid',
      },
      {
        name: 'bz',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'sempname',
      },
      {
        name: 'empname',
      },
      {
        name: 'sempid',
      },
      {
        name: 'amount',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'sapreason1',
      },
      {
        name: 'pusestate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'apprdesc',
      },
      {
        name: 'sapcbzx',
      },
      {
        name: 'deptname',
      },
      {
        name: 'sapllyt',
      },
      {
        name: 'numdiff',
      },
      {
        name: 'asum',
      },
      {
        name: 'enable',
      },
      {
        name: 'sap',
      },
      {
        name: 'sdate',
      },
      {
        name: 'life2',
      },
      {
        name: 'deptid',
      },
      {
        name: 'emitempuse',
        prop: 'emitempuseid',
      },
      {
        name: 'stock2num',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'useto',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'pusetype',
      },
      {
        name: 'sapcontrol',
      },
      {
        name: 'adate',
      },
      {
        name: 'description',
      },
      {
        name: 'apprdate',
      },
      {
        name: 'createman',
      },
      {
        name: 'createdate',
      },
      {
        name: 'life',
      },
      {
        name: 'storename',
      },
      {
        name: 'unitname',
      },
      {
        name: 'objname',
      },
      {
        name: 'unitid',
      },
      {
        name: 'itembtypeid',
      },
      {
        name: 'teamname',
      },
      {
        name: 'itemgroup',
      },
      {
        name: 'labservicename',
      },
      {
        name: 'avgprice',
      },
      {
        name: 'itemname',
      },
      {
        name: 'mservicename',
      },
      {
        name: 'storepartname',
      },
      {
        name: 'equipname',
      },
      {
        name: 'purplanname',
      },
      {
        name: 'woname',
      },
      {
        name: 'storeid',
      },
      {
        name: 'itemid',
      },
      {
        name: 'teamid',
      },
      {
        name: 'equipid',
      },
      {
        name: 'labserviceid',
      },
      {
        name: 'woid',
      },
      {
        name: 'mserviceid',
      },
      {
        name: 'objid',
      },
      {
        name: 'purplanid',
      },
      {
        name: 'storepartid',
      },
    ]
  }


}