/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'mptypeid',
          prop: 'mptypeid',
          dataType: 'SSCODELIST',
        },
        {
          name: 'emeqmpname',
          prop: 'emeqmpname',
          dataType: 'TEXT',
        },
        {
          name: 'normalrefval',
          prop: 'normalrefval',
          dataType: 'TEXT',
        },
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'mpcode',
          prop: 'mpcode',
          dataType: 'TEXT',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'mpdesc',
          prop: 'mpdesc',
          dataType: 'TEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'emeqmpname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'emeqmpid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'emeqmpid',
          dataType: 'GUID',
        },
        {
          name: 'mpscope',
          prop: 'mpscope',
          dataType: 'TEXT',
        },
        {
          name: 'objname',
          prop: 'objname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'emeqmp',
          prop: 'emeqmpid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}