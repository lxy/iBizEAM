/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'wplistinfo',
          prop: 'wplistinfo',
          dataType: 'TEXT',
        },
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'teamid',
          prop: 'teamid',
          dataType: 'PICKUP',
        },
        {
          name: 'emserviceid',
          prop: 'emserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'updateman',
          prop: 'updateman',
          dataType: 'TEXT',
        },
        {
          name: 'itemid',
          prop: 'itemid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'wplistcostid',
          prop: 'wplistcostid',
          dataType: 'PICKUP',
        },
        {
          name: 'updatedate',
          prop: 'updatedate',
          dataType: 'DATETIME',
        },
        {
          name: 'srfmajortext',
          prop: 'emwplistname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emwplistid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emwplistid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emwplist',
          prop: 'emwplistid',
        },
      {
        name: 'n_adate_gtandeq',
        prop: 'n_adate_gtandeq',
        dataType: 'DATETIME',
      },
      {
        name: 'n_adate_ltandeq',
        prop: 'n_adate_ltandeq',
        dataType: 'DATETIME',
      },
      {
        name: 'n_emservicename_eq',
        prop: 'n_emservicename_eq',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_itemname_like',
        prop: 'n_itemname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_deptname_like',
        prop: 'n_deptname_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_emserviceid_eq',
        prop: 'n_emserviceid_eq',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}