import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMWPListService from '@/service/emwplist/emwplist-service';
import WpAmountFunnelModel from './wp-amount-funnel-chart-model';


/**
 * WpAmountFunnel 部件服务对象
 *
 * @export
 * @class WpAmountFunnelService
 */
export default class WpAmountFunnelService extends ControlService {

    /**
     * 采购申请服务对象
     *
     * @type {EMWPListService}
     * @memberof WpAmountFunnelService
     */
    public appEntityService: EMWPListService = new EMWPListService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof WpAmountFunnelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of WpAmountFunnelService.
     * 
     * @param {*} [opts={}]
     * @memberof WpAmountFunnelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new WpAmountFunnelModel();
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof WpAmountFunnelService
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }
}