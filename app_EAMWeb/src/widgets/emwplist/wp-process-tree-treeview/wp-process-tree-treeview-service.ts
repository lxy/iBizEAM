import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMWPListService from '@/service/emwplist/emwplist-service';
import WpProcessTreeModel from './wp-process-tree-treeview-model';
import CodeListService from '@service/app/codelist-service';
import i18n from '@/locale';


/**
 * WpProcessTree 部件服务对象
 *
 * @export
 * @class WpProcessTreeService
 */
export default class WpProcessTreeService extends ControlService {

    /**
     * 采购申请服务对象
     *
     * @type {EMWPListService}
     * @memberof WpProcessTreeService
     */
    public appEntityService: EMWPListService = new EMWPListService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof WpProcessTreeService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of WpProcessTreeService.
     * 
     * @param {*} [opts={}]
     * @memberof WpProcessTreeService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new WpProcessTreeModel();
    }


    /**
     * 代码表服务对象
     *
     * @type {CodeListService}
     * @memberof WpProcessTreeService
     */
    public codeListService:CodeListService = new CodeListService({ $store: this.getStore() });


    /**
     * 节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
    public TREENODE_SEPARATOR: string = ';';

    /**
     * 待入库节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_WAITIN: string = 'WaitIn';

    /**
     * 已入库节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_IN: string = 'In';

    /**
     * 全部入库单节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_ALLRIN: string = 'AllRin';

    /**
     * 待记账节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_WAITBOOK: string = 'WaitBook';

    /**
     * 全部采购申请节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_ALLWPLIST: string = 'AllWPList';

    /**
     * 默认根节点节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_ROOT: string = 'ROOT';

    /**
     * 已生成订单节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_POD: string = 'Pod';

    /**
     * 全部订单节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_ALLPO: string = 'AllPO';

    /**
     * 已关闭节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_CLOSEDDETAIL: string = 'ClosedDetail';

    /**
     * 已取消节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_CANCEL: string = 'Cancel';

    /**
     * 待验收节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_WAITCHECK: string = 'WaitCheck';

    /**
     * 已关闭节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_CLOSEDORDER: string = 'ClosedOrder';

    /**
     * 等待下单节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_PLACEORDER: string = 'PlaceOrder';

    /**
     * 在途节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_ONORDER: string = 'OnOrder';

    /**
     * 已入库节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_PUTIN: string = 'PutIn';

    /**
     * 待确认询价节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_CONFIMCOST: string = 'ConfimCost';

    /**
     * 草稿节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_DRAFT: string = 'Draft';

    /**
     * 待生成订单节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_WAITPO: string = 'WaitPo';

    /**
     * 待询价节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_WAITCOST: string = 'WaitCost';

    /**
     * 全部订单条目节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof WpProcessTreeService
     */
	public TREENODE_ALLDETAIL: string = 'AllDetail';

    /**
     * 获取节点数据
     *
     * @param {string} action
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async getNodes(context:any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let { srfparentkey, srfcat, srfnodeid, srfnodefilter, query }: { srfparentkey: string, srfcat: string, srfnodeid: string, srfnodefilter: string, query:string } = data;
        srfnodefilter = query ? query : srfnodefilter;
        let list: any[] = [];
        let filter: any = {};


        if (!srfnodeid || Object.is(srfnodeid, '#')) {
            srfnodeid = this.TREENODE_ROOT;
        }

        let strTreeNodeId: string = srfnodeid;
        let strRealNodeId: string = '';
        let bRootSelect: boolean = false;
        let strNodeType: string | null = null;
        let strRootSelectNode: string = '';

        if (Object.is(strTreeNodeId, this.TREENODE_ROOT)) {
            strNodeType = this.TREENODE_ROOT;
            if (srfparentkey) {
                strRealNodeId = srfparentkey;
            }
        } else {
            let nPos = strTreeNodeId.indexOf(this.TREENODE_SEPARATOR);
            if (nPos === -1) {
                return Promise.reject({ status: 500, data: { title: '失败', message: `树节点${strTreeNodeId}标识无效` } });
            }
            strNodeType = strTreeNodeId.substring(0, nPos);
			strRealNodeId = strTreeNodeId.substring(nPos + 1);
        }

        Object.assign(filter,
            {
                srfparentkey: srfparentkey,
                srfcat: srfcat,
                srfnodefilter: srfnodefilter,
                strRealNodeId: strRealNodeId,
                srfnodeid: srfnodeid,
                strNodeType: strNodeType,
                viewparams: JSON.parse(JSON.stringify(data)).viewparams
            }
        );

        // 分解节点标识
        let nodeid: string[] = strRealNodeId.split(this.TREENODE_SEPARATOR);
        for (let i = 0; i < nodeid.length; i++) {
            switch (i) {
                case 0:
                    Object.assign(filter, { nodeid: nodeid[0] });
                    break;
                case 1:
                    Object.assign(filter, { nodeid2: nodeid[1] });
                    break;
                case 2:
                    Object.assign(filter, { nodeid3: nodeid[2] });
                    break;
                case 3:
                    Object.assign(filter, { nodeid4: nodeid[3] });
                    break;
                default:
                    break;
            }
        }

        if (Object.is(strNodeType, this.TREENODE_WAITIN)) {
            await this.fillWaitinNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_IN)) {
            await this.fillInNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_ALLRIN)) {
            await this.fillAllrinNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_WAITBOOK)) {
            await this.fillWaitbookNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_ALLWPLIST)) {
            await this.fillAllwplistNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_ROOT)) {
            await this.fillRootNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_POD)) {
            await this.fillPodNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_ALLPO)) {
            await this.fillAllpoNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_CLOSEDDETAIL)) {
            await this.fillCloseddetailNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_CANCEL)) {
            await this.fillCancelNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_WAITCHECK)) {
            await this.fillWaitcheckNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_CLOSEDORDER)) {
            await this.fillClosedorderNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_PLACEORDER)) {
            await this.fillPlaceorderNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_ONORDER)) {
            await this.fillOnorderNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_PUTIN)) {
            await this.fillPutinNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_CONFIMCOST)) {
            await this.fillConfimcostNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_DRAFT)) {
            await this.fillDraftNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_WAITPO)) {
            await this.fillWaitpoNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_WAITCOST)) {
            await this.fillWaitcostNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        if (Object.is(strNodeType, this.TREENODE_ALLDETAIL)) {
            await this.fillAlldetailNodeChilds(context,filter, list);
            return Promise.resolve({ status: 200, data: list });
        }
        return Promise.resolve({ status: 500, data: { title: '失败', message: `树节点${strTreeNodeId}标识无效` } });
    }

    /**
     * 填充 树视图节点[待入库]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillWaitinNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.waitin' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'WaitIn';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: true });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[待入库]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillWaitinNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
		} else {
		}
	}

    /**
     * 填充 树视图节点[已入库]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillInNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.in' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'In';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: true });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[已入库]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillInNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
		} else {
		}
	}

    /**
     * 填充 树视图节点[全部入库单]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillAllrinNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.allrin' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'AllRin';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: false });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[全部入库单]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillAllrinNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
			// 填充待入库
            let WaitinRsNavContext:any = {};
            let WaitinRsNavParams:any = {};
            let WaitinRsParams:any = {};
			await this.fillWaitinNodes(context, filter, list ,WaitinRsNavContext,WaitinRsNavParams,WaitinRsParams);
			// 填充已入库
            let PutinRsNavContext:any = {};
            let PutinRsNavParams:any = {};
            let PutinRsParams:any = {};
			await this.fillPutinNodes(context, filter, list ,PutinRsNavContext,PutinRsNavParams,PutinRsParams);
		} else {
			// 填充待入库
            let WaitinRsNavContext:any = {};
            let WaitinRsNavParams:any = {};
            let WaitinRsParams:any = {};
			await this.fillWaitinNodes(context, filter, list ,WaitinRsNavContext,WaitinRsNavParams,WaitinRsParams);
			// 填充已入库
            let PutinRsNavContext:any = {};
            let PutinRsNavParams:any = {};
            let PutinRsParams:any = {};
			await this.fillPutinNodes(context, filter, list ,PutinRsNavContext,PutinRsNavParams,PutinRsParams);
		}
	}

    /**
     * 填充 树视图节点[待记账]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillWaitbookNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.waitbook' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'WaitBook';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: true });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[待记账]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillWaitbookNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
		} else {
		}
	}

    /**
     * 填充 树视图节点[全部采购申请]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillAllwplistNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.allwplist' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'AllWPList';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: false });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[全部采购申请]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillAllwplistNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
			// 填充草稿
            let DraftRsNavContext:any = {};
            let DraftRsNavParams:any = {};
            let DraftRsParams:any = {};
			await this.fillDraftNodes(context, filter, list ,DraftRsNavContext,DraftRsNavParams,DraftRsParams);
			// 填充待询价
            let WaitcostRsNavContext:any = {};
            let WaitcostRsNavParams:any = {};
            let WaitcostRsParams:any = {};
			await this.fillWaitcostNodes(context, filter, list ,WaitcostRsNavContext,WaitcostRsNavParams,WaitcostRsParams);
			// 填充待确认询价
            let ConfimcostRsNavContext:any = {};
            let ConfimcostRsNavParams:any = {};
            let ConfimcostRsParams:any = {};
			await this.fillConfimcostNodes(context, filter, list ,ConfimcostRsNavContext,ConfimcostRsNavParams,ConfimcostRsParams);
			// 填充待生成订单
            let WaitpoRsNavContext:any = {};
            let WaitpoRsNavParams:any = {};
            let WaitpoRsParams:any = {};
			await this.fillWaitpoNodes(context, filter, list ,WaitpoRsNavContext,WaitpoRsNavParams,WaitpoRsParams);
			// 填充已生成订单
            let PodRsNavContext:any = {};
            let PodRsNavParams:any = {};
            let PodRsParams:any = {};
			await this.fillPodNodes(context, filter, list ,PodRsNavContext,PodRsNavParams,PodRsParams);
			// 填充已入库
            let InRsNavContext:any = {};
            let InRsNavParams:any = {};
            let InRsParams:any = {};
			await this.fillInNodes(context, filter, list ,InRsNavContext,InRsNavParams,InRsParams);
			// 填充已取消
            let CancelRsNavContext:any = {};
            let CancelRsNavParams:any = {};
            let CancelRsParams:any = {};
			await this.fillCancelNodes(context, filter, list ,CancelRsNavContext,CancelRsNavParams,CancelRsParams);
		} else {
			// 填充草稿
            let DraftRsNavContext:any = {};
            let DraftRsNavParams:any = {};
            let DraftRsParams:any = {};
			await this.fillDraftNodes(context, filter, list ,DraftRsNavContext,DraftRsNavParams,DraftRsParams);
			// 填充待询价
            let WaitcostRsNavContext:any = {};
            let WaitcostRsNavParams:any = {};
            let WaitcostRsParams:any = {};
			await this.fillWaitcostNodes(context, filter, list ,WaitcostRsNavContext,WaitcostRsNavParams,WaitcostRsParams);
			// 填充待确认询价
            let ConfimcostRsNavContext:any = {};
            let ConfimcostRsNavParams:any = {};
            let ConfimcostRsParams:any = {};
			await this.fillConfimcostNodes(context, filter, list ,ConfimcostRsNavContext,ConfimcostRsNavParams,ConfimcostRsParams);
			// 填充待生成订单
            let WaitpoRsNavContext:any = {};
            let WaitpoRsNavParams:any = {};
            let WaitpoRsParams:any = {};
			await this.fillWaitpoNodes(context, filter, list ,WaitpoRsNavContext,WaitpoRsNavParams,WaitpoRsParams);
			// 填充已生成订单
            let PodRsNavContext:any = {};
            let PodRsNavParams:any = {};
            let PodRsParams:any = {};
			await this.fillPodNodes(context, filter, list ,PodRsNavContext,PodRsNavParams,PodRsParams);
			// 填充已入库
            let InRsNavContext:any = {};
            let InRsNavParams:any = {};
            let InRsParams:any = {};
			await this.fillInNodes(context, filter, list ,InRsNavContext,InRsNavParams,InRsParams);
			// 填充已取消
            let CancelRsNavContext:any = {};
            let CancelRsNavParams:any = {};
            let CancelRsParams:any = {};
			await this.fillCancelNodes(context, filter, list ,CancelRsNavContext,CancelRsNavParams,CancelRsParams);
		}
	}

    /**
     * 填充 树视图节点[默认根节点]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillRootNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.root' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'ROOT';

            Object.assign(treeNode, { srfkey: 'root' });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += 'root';

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: filter.isAutoexpand });
            Object.assign(treeNode, { leaf: false });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[默认根节点]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillRootNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
			// 填充全部采购申请
            let AllwplistRsNavContext:any = {};
            let AllwplistRsNavParams:any = {};
            let AllwplistRsParams:any = {};
			await this.fillAllwplistNodes(context, filter, list ,AllwplistRsNavContext,AllwplistRsNavParams,AllwplistRsParams);
			// 填充全部订单
            let AllpoRsNavContext:any = {};
            let AllpoRsNavParams:any = {};
            let AllpoRsParams:any = {};
			await this.fillAllpoNodes(context, filter, list ,AllpoRsNavContext,AllpoRsNavParams,AllpoRsParams);
			// 填充全部订单条目
            let AlldetailRsNavContext:any = {};
            let AlldetailRsNavParams:any = {};
            let AlldetailRsParams:any = {};
			await this.fillAlldetailNodes(context, filter, list ,AlldetailRsNavContext,AlldetailRsNavParams,AlldetailRsParams);
			// 填充全部入库单
            let AllrinRsNavContext:any = {};
            let AllrinRsNavParams:any = {};
            let AllrinRsParams:any = {};
			await this.fillAllrinNodes(context, filter, list ,AllrinRsNavContext,AllrinRsNavParams,AllrinRsParams);
		} else {
			// 填充全部采购申请
            let AllwplistRsNavContext:any = {};
            let AllwplistRsNavParams:any = {};
            let AllwplistRsParams:any = {};
			await this.fillAllwplistNodes(context, filter, list ,AllwplistRsNavContext,AllwplistRsNavParams,AllwplistRsParams);
			// 填充全部订单
            let AllpoRsNavContext:any = {};
            let AllpoRsNavParams:any = {};
            let AllpoRsParams:any = {};
			await this.fillAllpoNodes(context, filter, list ,AllpoRsNavContext,AllpoRsNavParams,AllpoRsParams);
			// 填充全部订单条目
            let AlldetailRsNavContext:any = {};
            let AlldetailRsNavParams:any = {};
            let AlldetailRsParams:any = {};
			await this.fillAlldetailNodes(context, filter, list ,AlldetailRsNavContext,AlldetailRsNavParams,AlldetailRsParams);
			// 填充全部入库单
            let AllrinRsNavContext:any = {};
            let AllrinRsNavParams:any = {};
            let AllrinRsParams:any = {};
			await this.fillAllrinNodes(context, filter, list ,AllrinRsNavContext,AllrinRsNavParams,AllrinRsParams);
		}
	}

    /**
     * 填充 树视图节点[已生成订单]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillPodNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.pod' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'Pod';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: true });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[已生成订单]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillPodNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
		} else {
		}
	}

    /**
     * 填充 树视图节点[全部订单]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillAllpoNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.allpo' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'AllPO';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: false });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[全部订单]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillAllpoNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
			// 填充等待下单
            let PlaceorderRsNavContext:any = {};
            let PlaceorderRsNavParams:any = {};
            let PlaceorderRsParams:any = {};
			await this.fillPlaceorderNodes(context, filter, list ,PlaceorderRsNavContext,PlaceorderRsNavParams,PlaceorderRsParams);
			// 填充在途
            let OnorderRsNavContext:any = {};
            let OnorderRsNavParams:any = {};
            let OnorderRsParams:any = {};
			await this.fillOnorderNodes(context, filter, list ,OnorderRsNavContext,OnorderRsNavParams,OnorderRsParams);
			// 填充已关闭
            let ClosedorderRsNavContext:any = {};
            let ClosedorderRsNavParams:any = {};
            let ClosedorderRsParams:any = {};
			await this.fillClosedorderNodes(context, filter, list ,ClosedorderRsNavContext,ClosedorderRsNavParams,ClosedorderRsParams);
		} else {
			// 填充等待下单
            let PlaceorderRsNavContext:any = {};
            let PlaceorderRsNavParams:any = {};
            let PlaceorderRsParams:any = {};
			await this.fillPlaceorderNodes(context, filter, list ,PlaceorderRsNavContext,PlaceorderRsNavParams,PlaceorderRsParams);
			// 填充在途
            let OnorderRsNavContext:any = {};
            let OnorderRsNavParams:any = {};
            let OnorderRsParams:any = {};
			await this.fillOnorderNodes(context, filter, list ,OnorderRsNavContext,OnorderRsNavParams,OnorderRsParams);
			// 填充已关闭
            let ClosedorderRsNavContext:any = {};
            let ClosedorderRsNavParams:any = {};
            let ClosedorderRsParams:any = {};
			await this.fillClosedorderNodes(context, filter, list ,ClosedorderRsNavContext,ClosedorderRsNavParams,ClosedorderRsParams);
		}
	}

    /**
     * 填充 树视图节点[已关闭]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillCloseddetailNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.closeddetail' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'ClosedDetail';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: true });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[已关闭]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillCloseddetailNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
		} else {
		}
	}

    /**
     * 填充 树视图节点[已取消]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillCancelNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.cancel' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'Cancel';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: true });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[已取消]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillCancelNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
		} else {
		}
	}

    /**
     * 填充 树视图节点[待验收]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillWaitcheckNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.waitcheck' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'WaitCheck';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: true });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[待验收]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillWaitcheckNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
		} else {
		}
	}

    /**
     * 填充 树视图节点[已关闭]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillClosedorderNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.closedorder' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'ClosedOrder';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: true });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[已关闭]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillClosedorderNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
		} else {
		}
	}

    /**
     * 填充 树视图节点[等待下单]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillPlaceorderNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.placeorder' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'PlaceOrder';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: true });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[等待下单]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillPlaceorderNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
		} else {
		}
	}

    /**
     * 填充 树视图节点[在途]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillOnorderNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.onorder' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'OnOrder';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: true });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[在途]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillOnorderNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
		} else {
		}
	}

    /**
     * 填充 树视图节点[已入库]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillPutinNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.putin' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'PutIn';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: true });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[已入库]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillPutinNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
		} else {
		}
	}

    /**
     * 填充 树视图节点[待确认询价]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillConfimcostNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.confimcost' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'ConfimCost';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: true });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[待确认询价]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillConfimcostNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
		} else {
		}
	}

    /**
     * 填充 树视图节点[草稿]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillDraftNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.draft' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'Draft';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: true });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[草稿]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillDraftNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
		} else {
		}
	}

    /**
     * 填充 树视图节点[待生成订单]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillWaitpoNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.waitpo' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'WaitPo';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: true });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[待生成订单]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillWaitpoNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
		} else {
		}
	}

    /**
     * 填充 树视图节点[待询价]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillWaitcostNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.waitcost' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'WaitCost';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: true });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[待询价]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillWaitcostNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
		} else {
		}
	}

    /**
     * 填充 树视图节点[全部订单条目]
     *
     * @public
     * @param {any{}} context     
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext   
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public fillAlldetailNodes(context:any={},filter: any, list: any[],rsNavContext?:any,rsNavParams?:any,rsParams?:any): Promise<any> {
        context = this.handleResNavContext(context,filter,rsNavContext);
        filter = this.handleResNavParams(context,filter,rsNavParams,rsParams);
        return new Promise((resolve:any,reject:any) =>{
            let treeNode: any = {};
            Object.assign(treeNode, { text: 'entities.emwplist.wpprocesstree_treeview.nodes.alldetail' });
            Object.assign(treeNode, { isUseLangRes: true });
            Object.assign(treeNode,{srfappctx:context});
            Object.assign(treeNode, { srfmajortext: treeNode.text });
            let strNodeId: string = 'AllDetail';

            // 没有指定节点值，直接使用父节点值
            Object.assign(treeNode, { srfkey: filter.strRealNodeId });
            strNodeId += this.TREENODE_SEPARATOR;
            strNodeId += filter.strRealNodeId;

            Object.assign(treeNode, { id: strNodeId });

            Object.assign(treeNode, { expanded: true });
            Object.assign(treeNode, { leaf: false });
            Object.assign(treeNode, { nodeid: treeNode.srfkey });
            Object.assign(treeNode, { nodeid2: filter.strRealNodeId });
            list.push(treeNode);
            resolve(list);
        });
	}

    /**
     * 填充 树视图节点[全部订单条目]子节点
     *
     * @public
     * @param {any{}} context         
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof WpProcessTreeService
     */
    @Errorlog
    public async fillAlldetailNodeChilds(context:any={}, filter: any, list: any[]): Promise<any> {
		if (filter.srfnodefilter && !Object.is(filter.srfnodefilter,"")) {
			// 填充待验收
            let WaitcheckRsNavContext:any = {};
            let WaitcheckRsNavParams:any = {};
            let WaitcheckRsParams:any = {};
			await this.fillWaitcheckNodes(context, filter, list ,WaitcheckRsNavContext,WaitcheckRsNavParams,WaitcheckRsParams);
			// 填充待记账
            let WaitbookRsNavContext:any = {};
            let WaitbookRsNavParams:any = {};
            let WaitbookRsParams:any = {};
			await this.fillWaitbookNodes(context, filter, list ,WaitbookRsNavContext,WaitbookRsNavParams,WaitbookRsParams);
			// 填充已关闭
            let CloseddetailRsNavContext:any = {};
            let CloseddetailRsNavParams:any = {};
            let CloseddetailRsParams:any = {};
			await this.fillCloseddetailNodes(context, filter, list ,CloseddetailRsNavContext,CloseddetailRsNavParams,CloseddetailRsParams);
		} else {
			// 填充待验收
            let WaitcheckRsNavContext:any = {};
            let WaitcheckRsNavParams:any = {};
            let WaitcheckRsParams:any = {};
			await this.fillWaitcheckNodes(context, filter, list ,WaitcheckRsNavContext,WaitcheckRsNavParams,WaitcheckRsParams);
			// 填充待记账
            let WaitbookRsNavContext:any = {};
            let WaitbookRsNavParams:any = {};
            let WaitbookRsParams:any = {};
			await this.fillWaitbookNodes(context, filter, list ,WaitbookRsNavContext,WaitbookRsNavParams,WaitbookRsParams);
			// 填充已关闭
            let CloseddetailRsNavContext:any = {};
            let CloseddetailRsNavParams:any = {};
            let CloseddetailRsParams:any = {};
			await this.fillCloseddetailNodes(context, filter, list ,CloseddetailRsNavContext,CloseddetailRsNavParams,CloseddetailRsParams);
		}
	}


    /**
     * 处理代码表返回数据(树状结构)
     * 
     * @param result 返回数组
     * @param context 应用上下文
     * @param callBack 回调
     * @memberof WpProcessTreeService
     */
    public handleDataSet(result:Array<any>,context:any,callBack:any){
        let list:Array<any> = [];
        if(result.length === 0){
            return list;
        }
        result.forEach((codeItem:any) =>{
            if(!codeItem.pvalue){
                let valueField:string = codeItem.value;
                this.setChildCodeItems(valueField,result,codeItem);
                list.push(codeItem);
            }
        })
        this.setNodeData(list,context,callBack);
        return list;
    }

    /**
     * 处理非根节点数据
     * 
     * @param result 返回数组
     * @param context 应用上下文
     * @param callBack 回调
     * @memberof WpProcessTreeService
     */
    public setChildCodeItems(pValue:string,result:Array<any>,codeItem:any){
        result.forEach((item:any) =>{
            if(item.pvalue == pValue){
                let valueField:string = item.value;
                this.setChildCodeItems(valueField,result,item);
                if(!codeItem.children){
                    codeItem.children = [];
                }
                codeItem.children.push(item);
            }
        })
    }

    /**
     * 设置节点UI数据
     * 
     * @param result 返回数组
     * @param context 应用上下文
     * @param callBack 回调
     * @memberof WpProcessTreeService
     */
    public setNodeData(result:Array<any>,context:any,callBack:any){
        result.forEach((item:any) =>{
            if(item.children){
                item.leaf = false;
                this.setNodeData(item.children,context,callBack);
            }else{
                item.leaf = true;
            }
            callBack(context,item);
        })
    }

    /**
     * 处理节点关系导航上下文
     *
     * @param context 应用上下文
     * @param filter 参数 
     * @param resNavContext 节点关系导航上下文
     *
     * @memberof WpProcessTreeService
     */
    public handleResNavContext(context:any,filter:any,resNavContext:any){
        if(resNavContext && Object.keys(resNavContext).length > 0){
            let tempContextData:any = JSON.parse(JSON.stringify(context));
            let tempViewParams:any = {};
            if(filter && filter.viewparams){
                tempViewParams = filter.viewparams;
            }
            Object.keys(resNavContext).forEach((item:any) =>{
                let curDataObj:any = resNavContext[item];
                this.handleCustomDataLogic(context,tempViewParams,curDataObj,tempContextData,item);
            })
            return tempContextData;
        }else{
            return context;
        }
    }

    /**
     * 处理关系导航参数
     *
     * @param context 应用上下文
     * @param filter 参数 
     * @param resNavParams 节点关系导航参数
     * @param resParams 节点关系参数
     *
     * @memberof WpProcessTreeService
     */
	public handleResNavParams(context:any,filter:any,resNavParams:any,resParams:any){
        if((resNavParams && Object.keys(resNavParams).length >0) || (resParams && Object.keys(resParams).length >0)){
            let tempViewParamData:any = {};
            let tempViewParams:any = {};
            if(filter && filter.viewparams){
                tempViewParams = filter.viewparams;
                tempViewParamData = JSON.parse(JSON.stringify(filter.viewparams));
            }
            if( Object.keys(resNavParams).length > 0){
                Object.keys(resNavParams).forEach((item:any) =>{
                    let curDataObj:any = resNavParams[item];
                    this.handleCustomDataLogic(context,tempViewParams,curDataObj,tempViewParamData,item);
                })
            }
            if( Object.keys(resParams).length > 0){
                Object.keys(resParams).forEach((item:any) =>{
                    let curDataObj:any = resParams[item];
                    tempViewParamData[item.toLowerCase()] = curDataObj.value;
                })
            }
            Object.assign(filter,{viewparams:tempViewParamData});
            return filter;
        }else{
            return filter;
        }
    }
    
    /**
     * 处理自定义节点关系导航数据
     * 
     * @param context 应用上下文
     * @param viewparams 参数 
     * @param curNavData 节点关系导航参数对象
     * @param tempData 返回数据
     * @param item 节点关系导航参数键值
     *
     * @memberof WpProcessTreeService
     */
	public handleCustomDataLogic(context:any,viewparams:any,curNavData:any,tempData:any,item:string){
		// 直接值直接赋值
		if(curNavData.isRawValue){
			if(Object.is(curNavData.value,"null") || Object.is(curNavData.value,"")){
                Object.defineProperty(tempData, item.toLowerCase(), {
                    value: null,
                    writable : true,
                    enumerable : true,
                    configurable : true
                });
            }else{
                Object.defineProperty(tempData, item.toLowerCase(), {
                    value: curNavData.value,
                    writable : true,
                    enumerable : true,
                    configurable : true
                });
            }
		}else{
			// 先从导航上下文取数，没有再从导航参数（URL）取数，如果导航上下文和导航参数都没有则为null
			if(context[(curNavData.value).toLowerCase()]){
				Object.defineProperty(tempData, item.toLowerCase(), {
					value: context[(curNavData.value).toLowerCase()],
					writable : true,
					enumerable : true,
					configurable : true
				});
			}else{
				if(viewparams[(curNavData.value).toLowerCase()]){
					Object.defineProperty(tempData, item.toLowerCase(), {
						value: viewparams[(curNavData.value).toLowerCase()],
						writable : true,
						enumerable : true,
						configurable : true
					});
				}else{
					Object.defineProperty(tempData, item.toLowerCase(), {
						value: null,
						writable : true,
						enumerable : true,
						configurable : true
					});
				}
			}
		}
	}

}