/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'wplistdp',
          prop: 'wplistdp',
          dataType: 'TEXT',
        },
        {
          name: 'need3q',
          prop: 'need3q',
          dataType: 'YESNO',
        },
        {
          name: 'unitname',
          prop: 'unitname',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'itemid',
          prop: 'itemid',
          dataType: 'PICKUP',
        },
        {
          name: 'itemcode',
          prop: 'itemcode',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'useto',
          prop: 'useto',
          dataType: 'SSCODELIST',
        },
        {
          name: 'srfmajortext',
          prop: 'emwplistname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emwplistid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emwplistid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'wplistcostname',
          prop: 'wplistcostname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'wpliststate',
          prop: 'wpliststate',
          dataType: 'NSCODELIST',
        },
        {
          name: 'itemname',
          prop: 'itemname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'apprempname',
          prop: 'apprempname',
          dataType: 'TEXT',
        },
        {
          name: 'rempname',
          prop: 'rempname',
          dataType: 'TEXT',
        },
        {
          name: 'wplisttype',
          prop: 'wplisttype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'lastdate',
          prop: 'lastdate',
          dataType: 'DATETIME',
        },
        {
          name: 'deptname',
          prop: 'deptname',
          dataType: 'TEXT',
        },
        {
          name: 'emwplistid',
          prop: 'emwplistid',
          dataType: 'GUID',
        },
        {
          name: 'itemdesc',
          prop: 'itemdesc',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'teamid',
          prop: 'teamid',
          dataType: 'PICKUP',
        },
        {
          name: 'apprdate',
          prop: 'apprdate',
          dataType: 'DATETIME',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'hdate',
          prop: 'hdate',
          dataType: 'DATETIME',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'wfstep',
          prop: 'wfstep',
          dataType: 'SSCODELIST',
        },
        {
          name: 'wplistcostid',
          prop: 'wplistcostid',
          dataType: 'PICKUP',
        },
        {
          name: 'aempname',
          prop: 'aempname',
          dataType: 'TEXT',
        },
        {
          name: 'adate',
          prop: 'adate',
          dataType: 'DATETIME',
        },
        {
          name: 'qcnt',
          prop: 'qcnt',
          dataType: 'INT',
        },
        {
          name: 'emserviceid',
          prop: 'emserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'asum',
          prop: 'asum',
          dataType: 'FLOAT',
        },
        {
          name: 'teamname',
          prop: 'teamname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'objname',
          prop: 'objname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'emwplist',
          prop: 'emwplistid',
        },
      {
        name: 'n_adate_gtandeq',
        prop: 'n_adate_gtandeq',
        dataType: 'DATETIME',
      },
      {
        name: 'n_adate_ltandeq',
        prop: 'n_adate_ltandeq',
        dataType: 'DATETIME',
      },
      {
        name: 'n_emservicename_eq',
        prop: 'n_emservicename_eq',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_itemname_like',
        prop: 'n_itemname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_deptname_like',
        prop: 'n_deptname_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_emserviceid_eq',
        prop: 'n_emserviceid_eq',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}