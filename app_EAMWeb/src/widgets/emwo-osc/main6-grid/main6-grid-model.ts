/**
 * Main6 部件模型
 *
 * @export
 * @class Main6Model
 */
export default class Main6Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main6GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main6GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'acclassid',
          prop: 'acclassid',
          dataType: 'PICKUP',
        },
        {
          name: 'arg5',
          prop: 'arg5',
          dataType: 'FLOAT',
        },
        {
          name: 'emwo_oscid',
          prop: 'emwo_oscid',
          dataType: 'GUID',
        },
        {
          name: 'arg4',
          prop: 'arg4',
          dataType: 'FLOAT',
        },
        {
          name: 'rteamname',
          prop: 'rteamname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rfodeid',
          prop: 'rfodeid',
          dataType: 'PICKUP',
        },
        {
          name: 'arg3',
          prop: 'arg3',
          dataType: 'FLOAT',
        },
        {
          name: 'arg2',
          prop: 'arg2',
          dataType: 'FLOAT',
        },
        {
          name: 'eqstoplength',
          prop: 'eqstoplength',
          dataType: 'FLOAT',
        },
        {
          name: 'arg1',
          prop: 'arg1',
          dataType: 'FLOAT',
        },
        {
          name: 'rfomoid',
          prop: 'rfomoid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfocaid',
          prop: 'rfocaid',
          dataType: 'PICKUP',
        },
        {
          name: 'woteam',
          prop: 'woteam',
          dataType: 'TEXT',
        },
        {
          name: 'wopid',
          prop: 'wopid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emwo_oscname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emwo_oscid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emwo_oscid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'wooriid',
          prop: 'wooriid',
          dataType: 'PICKUP',
        },
        {
          name: 'rdeptname',
          prop: 'rdeptname',
          dataType: 'TEXT',
        },
        {
          name: 'priority',
          prop: 'priority',
          dataType: 'NSCODELIST',
        },
        {
          name: 'worklength',
          prop: 'worklength',
          dataType: 'FLOAT',
        },
        {
          name: 'rempname',
          prop: 'rempname',
          dataType: 'TEXT',
        },
        {
          name: 'wodate',
          prop: 'wodate',
          dataType: 'DATETIME',
        },
        {
          name: 'wpersonname',
          prop: 'wpersonname',
          dataType: 'TEXT',
        },
        {
          name: 'dpid',
          prop: 'dpid',
          dataType: 'PICKUP',
        },
        {
          name: 'wopname',
          prop: 'wopname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rteamid',
          prop: 'rteamid',
          dataType: 'PICKUP',
        },
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'emwo_oscname',
          prop: 'emwo_oscname',
          dataType: 'TEXT',
        },
        {
          name: 'rservicename',
          prop: 'rservicename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'cplanflag',
          prop: 'cplanflag',
          dataType: 'YESNO',
        },
        {
          name: 'rserviceid',
          prop: 'rserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'regionbegindate',
          prop: 'regionbegindate',
          dataType: 'DATETIME',
        },
        {
          name: 'wresult',
          prop: 'wresult',
          dataType: 'TEXT',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'wooriname',
          prop: 'wooriname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'regionenddate',
          prop: 'regionenddate',
          dataType: 'DATETIME',
        },
        {
          name: 'mfee',
          prop: 'mfee',
          dataType: 'CURRENCY',
        },
        {
          name: 'wotype',
          prop: 'wotype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'rfoacid',
          prop: 'rfoacid',
          dataType: 'PICKUP',
        },
        {
          name: 'wogroup',
          prop: 'wogroup',
          dataType: 'SSCODELIST',
        },
        {
          name: 'objname',
          prop: 'objname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'emwo_osc',
          prop: 'emwo_oscid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}