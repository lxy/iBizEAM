/**
 * Nearest30DayByEQ 部件模型
 *
 * @export
 * @class Nearest30DayByEQModel
 */
export default class Nearest30DayByEQModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Nearest30DayByEQDashboard_sysportlet9_chartMode
	 */
	public getDataItems(): any[] {
		return [
			{
			name:'size',
			prop:'size'
			},
			{
			name:'query',
			prop:'query'
			},
			{
			name:'page',
			prop:'page'
			},
			{
			name:'sort',
			prop:'sort'
			}
		]
	}

}