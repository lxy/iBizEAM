/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'pfdeptid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'pfdeptname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'deptcode',
        prop: 'deptcode',
        dataType: 'TEXT',
      },
      {
        name: 'pfdeptname',
        prop: 'pfdeptname',
        dataType: 'TEXT',
      },
      {
        name: 'deptpname',
        prop: 'deptpname',
        dataType: 'TEXT',
      },
      {
        name: 'sdept',
        prop: 'sdept',
        dataType: 'NSCODELIST',
      },
      {
        name: 'deptfn',
        prop: 'deptfn',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'pfdeptid',
        prop: 'pfdeptid',
        dataType: 'GUID',
      },
      {
        name: 'pfdept',
        prop: 'pfdeptid',
        dataType: 'FONTKEY',
      },
    ]
  }

}