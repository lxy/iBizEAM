/**
 * TabExpViewtabviewpanel4 部件模型
 *
 * @export
 * @class TabExpViewtabviewpanel4Model
 */
export default class TabExpViewtabviewpanel4Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabviewpanel4Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'createman',
      },
      {
        name: 'orgid',
      },
      {
        name: 'description',
      },
      {
        name: 'rfodecode',
      },
      {
        name: 'objid',
      },
      {
        name: 'rfodeinfo',
      },
      {
        name: 'emrfode',
        prop: 'emrfodeid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'emrfodename',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'enable',
      },
      {
        name: 'updateman',
      },
    ]
  }


}