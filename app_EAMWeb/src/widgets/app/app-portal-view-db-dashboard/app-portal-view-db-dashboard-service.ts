import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * AppPortalView_db 部件服务对象
 *
 * @export
 * @class AppPortalView_dbService
 */
export default class AppPortalView_dbService extends ControlService {
}