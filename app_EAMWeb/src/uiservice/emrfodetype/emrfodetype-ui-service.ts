import EMRFODETypeUIServiceBase from './emrfodetype-ui-service-base';

/**
 * 现象分类UI服务对象
 *
 * @export
 * @class EMRFODETypeUIService
 */
export default class EMRFODETypeUIService extends EMRFODETypeUIServiceBase {

    /**
     * Creates an instance of  EMRFODETypeUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFODETypeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}