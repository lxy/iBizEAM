import EMItemROutUIServiceBase from './emitem-rout-ui-service-base';

/**
 * 退货单UI服务对象
 *
 * @export
 * @class EMItemROutUIService
 */
export default class EMItemROutUIService extends EMItemROutUIServiceBase {

    /**
     * Creates an instance of  EMItemROutUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemROutUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}