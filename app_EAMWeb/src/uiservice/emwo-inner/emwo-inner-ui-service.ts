import EMWO_INNERUIServiceBase from './emwo-inner-ui-service-base';

/**
 * 内部工单UI服务对象
 *
 * @export
 * @class EMWO_INNERUIService
 */
export default class EMWO_INNERUIService extends EMWO_INNERUIServiceBase {

    /**
     * Creates an instance of  EMWO_INNERUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWO_INNERUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}