import EMEQKPUIServiceBase from './emeqkp-ui-service-base';

/**
 * 设备关键点UI服务对象
 *
 * @export
 * @class EMEQKPUIService
 */
export default class EMEQKPUIService extends EMEQKPUIServiceBase {

    /**
     * Creates an instance of  EMEQKPUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQKPUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}