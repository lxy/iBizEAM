import EMItemCSUIServiceBase from './emitem-cs-ui-service-base';

/**
 * 库间调整单UI服务对象
 *
 * @export
 * @class EMItemCSUIService
 */
export default class EMItemCSUIService extends EMItemCSUIServiceBase {

    /**
     * Creates an instance of  EMItemCSUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemCSUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}