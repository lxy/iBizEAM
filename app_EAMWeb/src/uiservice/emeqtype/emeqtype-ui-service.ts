import EMEQTypeUIServiceBase from './emeqtype-ui-service-base';

/**
 * 设备类型UI服务对象
 *
 * @export
 * @class EMEQTypeUIService
 */
export default class EMEQTypeUIService extends EMEQTypeUIServiceBase {

    /**
     * Creates an instance of  EMEQTypeUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQTypeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}