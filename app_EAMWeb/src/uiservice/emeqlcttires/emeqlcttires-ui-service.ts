import EMEQLCTTIResUIServiceBase from './emeqlcttires-ui-service-base';

/**
 * 轮胎位置UI服务对象
 *
 * @export
 * @class EMEQLCTTIResUIService
 */
export default class EMEQLCTTIResUIService extends EMEQLCTTIResUIServiceBase {

    /**
     * Creates an instance of  EMEQLCTTIResUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLCTTIResUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}