import EMENUIServiceBase from './emen-ui-service-base';

/**
 * 能源UI服务对象
 *
 * @export
 * @class EMENUIService
 */
export default class EMENUIService extends EMENUIServiceBase {

    /**
     * Creates an instance of  EMENUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMENUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}