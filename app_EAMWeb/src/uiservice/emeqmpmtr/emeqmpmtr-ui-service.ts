import EMEQMPMTRUIServiceBase from './emeqmpmtr-ui-service-base';

/**
 * 设备仪表读数UI服务对象
 *
 * @export
 * @class EMEQMPMTRUIService
 */
export default class EMEQMPMTRUIService extends EMEQMPMTRUIServiceBase {

    /**
     * Creates an instance of  EMEQMPMTRUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQMPMTRUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}