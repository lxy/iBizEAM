import EMEQSpareDetailUIServiceBase from './emeqspare-detail-ui-service-base';

/**
 * 备件包明细UI服务对象
 *
 * @export
 * @class EMEQSpareDetailUIService
 */
export default class EMEQSpareDetailUIService extends EMEQSpareDetailUIServiceBase {

    /**
     * Creates an instance of  EMEQSpareDetailUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQSpareDetailUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}