import EMPODetailUIServiceBase from './empodetail-ui-service-base';

/**
 * 订单条目UI服务对象
 *
 * @export
 * @class EMPODetailUIService
 */
export default class EMPODetailUIService extends EMPODetailUIServiceBase {

    /**
     * Creates an instance of  EMPODetailUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPODetailUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}