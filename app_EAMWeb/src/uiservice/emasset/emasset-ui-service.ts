import EMAssetUIServiceBase from './emasset-ui-service-base';

/**
 * 资产UI服务对象
 *
 * @export
 * @class EMAssetUIService
 */
export default class EMAssetUIService extends EMAssetUIServiceBase {

    /**
     * Creates an instance of  EMAssetUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMAssetUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}