import EMEquipUIServiceBase from './emequip-ui-service-base';

/**
 * 设备档案UI服务对象
 *
 * @export
 * @class EMEquipUIService
 */
export default class EMEquipUIService extends EMEquipUIServiceBase {

    /**
     * Creates an instance of  EMEquipUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEquipUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}