import { Http,Util } from '@/utils';
import EMRFODEServiceBase from './emrfode-service-base';


/**
 * 现象服务对象
 *
 * @export
 * @class EMRFODEService
 * @extends {EMRFODEServiceBase}
 */
export default class EMRFODEService extends EMRFODEServiceBase {

    /**
     * Creates an instance of  EMRFODEService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFODEService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}