import { Http,Util } from '@/utils';
import EMENConsumServiceBase from './emenconsum-service-base';


/**
 * 能耗服务对象
 *
 * @export
 * @class EMENConsumService
 * @extends {EMENConsumServiceBase}
 */
export default class EMENConsumService extends EMENConsumServiceBase {

    /**
     * Creates an instance of  EMENConsumService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMENConsumService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}