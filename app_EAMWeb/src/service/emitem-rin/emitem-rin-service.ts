import { Http,Util } from '@/utils';
import EMItemRInServiceBase from './emitem-rin-service-base';


/**
 * 入库单服务对象
 *
 * @export
 * @class EMItemRInService
 * @extends {EMItemRInServiceBase}
 */
export default class EMItemRInService extends EMItemRInServiceBase {

    /**
     * Creates an instance of  EMItemRInService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemRInService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}