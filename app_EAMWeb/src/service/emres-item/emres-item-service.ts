import { Http,Util } from '@/utils';
import EMResItemServiceBase from './emres-item-service-base';


/**
 * 物品资源服务对象
 *
 * @export
 * @class EMResItemService
 * @extends {EMResItemServiceBase}
 */
export default class EMResItemService extends EMResItemServiceBase {

    /**
     * Creates an instance of  EMResItemService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMResItemService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}