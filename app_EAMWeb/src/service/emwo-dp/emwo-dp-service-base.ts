import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 点检工单服务对象基类
 *
 * @export
 * @class EMWO_DPServiceBase
 * @extends {EntityServie}
 */
export default class EMWO_DPServiceBase extends EntityService {

    /**
     * Creates an instance of  EMWO_DPServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWO_DPServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMWO_DPServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emwo_dp';
        this.APPDEKEY = 'emwo_dpid';
        this.APPDENAME = 'emwo_dps';
        this.APPDETEXT = 'emwo_dpname';
        this.APPNAME = 'eamweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_DPServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_dp){
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_dps/${context.emwo_dp}/select`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emwo_dp){
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emwo_dps/${context.emwo_dp}/select`,isloading);
            
            return res;
        }
        if(context.emequip && context.emwo_dp){
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emwo_dps/${context.emwo_dp}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emwo_dps/${context.emwo_dp}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_DPServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_dps`,data,isloading);
            
            return res;
        }
        if(context.pfteam && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emwo_dps`,data,isloading);
            
            return res;
        }
        if(context.emequip && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emwo_dps`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emwo_dps`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_DPServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_dp){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_dps/${context.emwo_dp}`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emwo_dp){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emwo_dps/${context.emwo_dp}`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emwo_dp){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emequips/${context.emequip}/emwo_dps/${context.emwo_dp}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emwo_dps/${context.emwo_dp}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_DPServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_dp){
            let res:any = Http.getInstance().delete(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_dps/${context.emwo_dp}`,isloading);
            return res;
        }
        if(context.pfteam && context.emwo_dp){
            let res:any = Http.getInstance().delete(`/pfteams/${context.pfteam}/emwo_dps/${context.emwo_dp}`,isloading);
            return res;
        }
        if(context.emequip && context.emwo_dp){
            let res:any = Http.getInstance().delete(`/emequips/${context.emequip}/emwo_dps/${context.emwo_dp}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emwo_dps/${context.emwo_dp}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_DPServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_dp){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_dps/${context.emwo_dp}`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emwo_dp){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emwo_dps/${context.emwo_dp}`,isloading);
            
            return res;
        }
        if(context.emequip && context.emwo_dp){
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emwo_dps/${context.emwo_dp}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emwo_dps/${context.emwo_dp}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_DPServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_dps/getdraft`,isloading);
            res.data.emwo_dp = data.emwo_dp;
            
            return res;
        }
        if(context.pfteam && true){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emwo_dps/getdraft`,isloading);
            res.data.emwo_dp = data.emwo_dp;
            
            return res;
        }
        if(context.emequip && true){
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emwo_dps/getdraft`,isloading);
            res.data.emwo_dp = data.emwo_dp;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/emwo_dps/getdraft`,isloading);
        res.data.emwo_dp = data.emwo_dp;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_DPServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_dp){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_dps/${context.emwo_dp}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emwo_dp){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emwo_dps/${context.emwo_dp}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emwo_dp){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emwo_dps/${context.emwo_dp}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emwo_dps/${context.emwo_dp}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_DPServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && context.emwo_dp){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_dps/${context.emwo_dp}/save`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emwo_dp){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emwo_dps/${context.emwo_dp}/save`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emwo_dp){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emwo_dps/${context.emwo_dp}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emwo_dps/${context.emwo_dp}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchCalendar接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_DPServiceBase
     */
    public async FetchCalendar(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_dps/fetchcalendar`,tempData,isloading);
            return res;
        }
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emwo_dps/fetchcalendar`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emwo_dps/fetchcalendar`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emwo_dps/fetchcalendar`,tempData,isloading);
        return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMWO_DPServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emwo_dps/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.pfteam && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/pfteams/${context.pfteam}/emwo_dps/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emequips/${context.emequip}/emwo_dps/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emwo_dps/fetchdefault`,tempData,isloading);
        return res;
    }
}