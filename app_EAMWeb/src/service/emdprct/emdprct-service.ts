import { Http,Util } from '@/utils';
import EMDPRCTServiceBase from './emdprct-service-base';


/**
 * 测点记录服务对象
 *
 * @export
 * @class EMDPRCTService
 * @extends {EMDPRCTServiceBase}
 */
export default class EMDPRCTService extends EMDPRCTServiceBase {

    /**
     * Creates an instance of  EMDPRCTService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMDPRCTService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}