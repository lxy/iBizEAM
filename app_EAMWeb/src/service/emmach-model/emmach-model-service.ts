import { Http,Util } from '@/utils';
import EMMachModelServiceBase from './emmach-model-service-base';


/**
 * 机型服务对象
 *
 * @export
 * @class EMMachModelService
 * @extends {EMMachModelServiceBase}
 */
export default class EMMachModelService extends EMMachModelServiceBase {

    /**
     * Creates an instance of  EMMachModelService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMMachModelService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}