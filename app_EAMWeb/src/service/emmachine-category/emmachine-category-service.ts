import { Http,Util } from '@/utils';
import EMMachineCategoryServiceBase from './emmachine-category-service-base';


/**
 * 机种编号服务对象
 *
 * @export
 * @class EMMachineCategoryService
 * @extends {EMMachineCategoryServiceBase}
 */
export default class EMMachineCategoryService extends EMMachineCategoryServiceBase {

    /**
     * Creates an instance of  EMMachineCategoryService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMMachineCategoryService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}