import { Http,Util } from '@/utils';
import EMEQLCTGSSServiceBase from './emeqlctgss-service-base';


/**
 * 钢丝绳位置服务对象
 *
 * @export
 * @class EMEQLCTGSSService
 * @extends {EMEQLCTGSSServiceBase}
 */
export default class EMEQLCTGSSService extends EMEQLCTGSSServiceBase {

    /**
     * Creates an instance of  EMEQLCTGSSService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLCTGSSService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}