import { Http,Util } from '@/utils';
import EMDRWGServiceBase from './emdrwg-service-base';


/**
 * 文档服务对象
 *
 * @export
 * @class EMDRWGService
 * @extends {EMDRWGServiceBase}
 */
export default class EMDRWGService extends EMDRWGServiceBase {

    /**
     * Creates an instance of  EMDRWGService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMDRWGService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}