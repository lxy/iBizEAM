import { Http,Util } from '@/utils';
import EMEQLCTFDJServiceBase from './emeqlctfdj-service-base';


/**
 * 发动机位置服务对象
 *
 * @export
 * @class EMEQLCTFDJService
 * @extends {EMEQLCTFDJServiceBase}
 */
export default class EMEQLCTFDJService extends EMEQLCTFDJServiceBase {

    /**
     * Creates an instance of  EMEQLCTFDJService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLCTFDJService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}