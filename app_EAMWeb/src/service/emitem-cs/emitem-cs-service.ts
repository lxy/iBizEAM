import { Http,Util } from '@/utils';
import EMItemCSServiceBase from './emitem-cs-service-base';


/**
 * 库间调整单服务对象
 *
 * @export
 * @class EMItemCSService
 * @extends {EMItemCSServiceBase}
 */
export default class EMItemCSService extends EMItemCSServiceBase {

    /**
     * Creates an instance of  EMItemCSService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemCSService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}