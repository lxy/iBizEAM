import { Http,Util } from '@/utils';
import PFEmpServiceBase from './pfemp-service-base';


/**
 * 职员服务对象
 *
 * @export
 * @class PFEmpService
 * @extends {PFEmpServiceBase}
 */
export default class PFEmpService extends PFEmpServiceBase {

    /**
     * Creates an instance of  PFEmpService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFEmpService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}