import { Http,Util } from '@/utils';
import EMEQSpareServiceBase from './emeqspare-service-base';


/**
 * 备件包服务对象
 *
 * @export
 * @class EMEQSpareService
 * @extends {EMEQSpareServiceBase}
 */
export default class EMEQSpareService extends EMEQSpareServiceBase {

    /**
     * Creates an instance of  EMEQSpareService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQSpareService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}