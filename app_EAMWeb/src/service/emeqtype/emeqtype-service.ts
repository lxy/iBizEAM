import { Http,Util } from '@/utils';
import EMEQTypeServiceBase from './emeqtype-service-base';


/**
 * 设备类型服务对象
 *
 * @export
 * @class EMEQTypeService
 * @extends {EMEQTypeServiceBase}
 */
export default class EMEQTypeService extends EMEQTypeServiceBase {

    /**
     * Creates an instance of  EMEQTypeService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQTypeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}