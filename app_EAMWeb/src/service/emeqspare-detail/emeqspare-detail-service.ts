import { Http,Util } from '@/utils';
import EMEQSpareDetailServiceBase from './emeqspare-detail-service-base';


/**
 * 备件包明细服务对象
 *
 * @export
 * @class EMEQSpareDetailService
 * @extends {EMEQSpareDetailServiceBase}
 */
export default class EMEQSpareDetailService extends EMEQSpareDetailServiceBase {

    /**
     * Creates an instance of  EMEQSpareDetailService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQSpareDetailService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}