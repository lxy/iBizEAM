import { Http,Util } from '@/utils';
import EMRFODEMapServiceBase from './emrfodemap-service-base';


/**
 * 现象引用服务对象
 *
 * @export
 * @class EMRFODEMapService
 * @extends {EMRFODEMapServiceBase}
 */
export default class EMRFODEMapService extends EMRFODEMapServiceBase {

    /**
     * Creates an instance of  EMRFODEMapService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFODEMapService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}