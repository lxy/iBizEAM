import { Http,Util } from '@/utils';
import PFUnitServiceBase from './pfunit-service-base';


/**
 * 计量单位服务对象
 *
 * @export
 * @class PFUnitService
 * @extends {PFUnitServiceBase}
 */
export default class PFUnitService extends PFUnitServiceBase {

    /**
     * Creates an instance of  PFUnitService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFUnitService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}