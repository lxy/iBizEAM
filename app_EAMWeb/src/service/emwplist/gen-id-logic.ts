import { Http,Util } from '@/utils';
import GenIdLogicBase from './gen-id-logic-base';

/**
 * 创建自动生产采购申请号
 *
 * @export
 * @class GenIdLogic
 */
export default class GenIdLogic extends GenIdLogicBase{

    /**
     * Creates an instance of  GenIdLogic
     * 
     * @param {*} [opts={}]
     * @memberof  GenIdLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}