import { Http,Util } from '@/utils';
import GetREMPLogicBase from './get-remp-logic-base';

/**
 * 根据物品获取采购员
 *
 * @export
 * @class GetREMPLogic
 */
export default class GetREMPLogic extends GetREMPLogicBase{

    /**
     * Creates an instance of  GetREMPLogic
     * 
     * @param {*} [opts={}]
     * @memberof  GetREMPLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}