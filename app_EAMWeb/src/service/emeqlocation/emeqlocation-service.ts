import { Http,Util } from '@/utils';
import EMEQLocationServiceBase from './emeqlocation-service-base';


/**
 * 位置服务对象
 *
 * @export
 * @class EMEQLocationService
 * @extends {EMEQLocationServiceBase}
 */
export default class EMEQLocationService extends EMEQLocationServiceBase {

    /**
     * Creates an instance of  EMEQLocationService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLocationService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}