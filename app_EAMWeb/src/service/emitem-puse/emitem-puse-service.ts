import { Http,Util } from '@/utils';
import EMItemPUseServiceBase from './emitem-puse-service-base';


/**
 * 领料单服务对象
 *
 * @export
 * @class EMItemPUseService
 * @extends {EMItemPUseServiceBase}
 */
export default class EMItemPUseService extends EMItemPUseServiceBase {

    /**
     * Creates an instance of  EMItemPUseService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemPUseService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}