import { Http,Util } from '@/utils';
import EMPODetailServiceBase from './empodetail-service-base';


/**
 * 订单条目服务对象
 *
 * @export
 * @class EMPODetailService
 * @extends {EMPODetailServiceBase}
 */
export default class EMPODetailService extends EMPODetailServiceBase {

    /**
     * Creates an instance of  EMPODetailService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPODetailService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}