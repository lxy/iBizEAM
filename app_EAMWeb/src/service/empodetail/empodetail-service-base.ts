import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 订单条目服务对象基类
 *
 * @export
 * @class EMPODetailServiceBase
 * @extends {EntityServie}
 */
export default class EMPODetailServiceBase extends EntityService {

    /**
     * Creates an instance of  EMPODetailServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPODetailServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMPODetailServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='empodetail';
        this.APPDEKEY = 'empodetailid';
        this.APPDENAME = 'empodetails';
        this.APPDETEXT = 'empodetailname';
        this.APPNAME = 'eamweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPODetailServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/select`,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && context.empodetail){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}/select`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && context.empodetail){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/select`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && context.empodetail){
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/select`,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplist && context.empodetail){
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/select`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.empodetail){
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/empodetails/${context.empodetail}/select`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.empodetail){
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.empodetail){
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/empodetails/${context.empodetail}/select`,isloading);
            
            return res;
        }
        if(context.emwplist && context.empodetail){
            let res:any = Http.getInstance().get(`/emwplists/${context.emwplist}/empodetails/${context.empodetail}/select`,isloading);
            
            return res;
        }
        if(context.empo && context.empodetail){
            let res:any = Http.getInstance().get(`/empos/${context.empo}/empodetails/${context.empodetail}/select`,isloading);
            
            return res;
        }
        if(context.emitem && context.empodetail){
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/empodetails/${context.empodetail}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/empodetails/${context.empodetail}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPODetailServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplist && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/empodetails`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/empodetails`,data,isloading);
            
            return res;
        }
        if(context.emwplist && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emwplists/${context.emwplist}/empodetails`,data,isloading);
            
            return res;
        }
        if(context.empo && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/empos/${context.empo}/empodetails`,data,isloading);
            
            return res;
        }
        if(context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/empodetails`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/empodetails`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPODetailServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emitems/${context.emitem}/empodetails/${context.empodetail}`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emitems/${context.emitem}/empodetails/${context.empodetail}`,data,isloading);
            
            return res;
        }
        if(context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,data,isloading);
            
            return res;
        }
        if(context.empo && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/empos/${context.empo}/empodetails/${context.empodetail}`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emitems/${context.emitem}/empodetails/${context.empodetail}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/empodetails/${context.empodetail}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPODetailServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,isloading);
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && context.empodetail){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}`,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && context.empodetail){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let res:any = Http.getInstance().delete(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && context.empodetail){
            let res:any = Http.getInstance().delete(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,isloading);
            return res;
        }
        if(context.emitem && context.emwplist && context.empodetail){
            let res:any = Http.getInstance().delete(`/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.empodetail){
            let res:any = Http.getInstance().delete(`/emstores/${context.emstore}/emitems/${context.emitem}/empodetails/${context.empodetail}`,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.empodetail){
            let res:any = Http.getInstance().delete(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}`,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.empodetail){
            let res:any = Http.getInstance().delete(`/emservices/${context.emservice}/emitems/${context.emitem}/empodetails/${context.empodetail}`,isloading);
            return res;
        }
        if(context.emwplist && context.empodetail){
            let res:any = Http.getInstance().delete(`/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,isloading);
            return res;
        }
        if(context.empo && context.empodetail){
            let res:any = Http.getInstance().delete(`/empos/${context.empo}/empodetails/${context.empodetail}`,isloading);
            return res;
        }
        if(context.emitem && context.empodetail){
            let res:any = Http.getInstance().delete(`/emitems/${context.emitem}/empodetails/${context.empodetail}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/empodetails/${context.empodetail}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPODetailServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && context.empodetail){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && context.empodetail){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && context.empodetail){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplist && context.empodetail){
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.empodetail){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/empodetails/${context.empodetail}`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.empodetail){
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.empodetail){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/empodetails/${context.empodetail}`,isloading);
            
            return res;
        }
        if(context.emwplist && context.empodetail){
            let res:any = await Http.getInstance().get(`/emwplists/${context.emwplist}/empodetails/${context.empodetail}`,isloading);
            
            return res;
        }
        if(context.empo && context.empodetail){
            let res:any = await Http.getInstance().get(`/empos/${context.empo}/empodetails/${context.empodetail}`,isloading);
            
            return res;
        }
        if(context.emitem && context.empodetail){
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/empodetails/${context.empodetail}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/empodetails/${context.empodetail}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPODetailServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && true){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/getdraft`,isloading);
            res.data.empodetail = data.empodetail;
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && true){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/getdraft`,isloading);
            res.data.empodetail = data.empodetail;
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && true){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/getdraft`,isloading);
            res.data.empodetail = data.empodetail;
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && true){
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/getdraft`,isloading);
            res.data.empodetail = data.empodetail;
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && true){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/getdraft`,isloading);
            res.data.empodetail = data.empodetail;
            
            return res;
        }
        if(context.emitem && context.emwplist && true){
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/getdraft`,isloading);
            res.data.empodetail = data.empodetail;
            
            return res;
        }
        if(context.emstore && context.emitem && true){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/empodetails/getdraft`,isloading);
            res.data.empodetail = data.empodetail;
            
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/getdraft`,isloading);
            res.data.empodetail = data.empodetail;
            
            return res;
        }
        if(context.emservice && context.emitem && true){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/empodetails/getdraft`,isloading);
            res.data.empodetail = data.empodetail;
            
            return res;
        }
        if(context.emwplist && true){
            let res:any = await Http.getInstance().get(`/emwplists/${context.emwplist}/empodetails/getdraft`,isloading);
            res.data.empodetail = data.empodetail;
            
            return res;
        }
        if(context.empo && true){
            let res:any = await Http.getInstance().get(`/empos/${context.empo}/empodetails/getdraft`,isloading);
            res.data.empodetail = data.empodetail;
            
            return res;
        }
        if(context.emitem && true){
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/empodetails/getdraft`,isloading);
            res.data.empodetail = data.empodetail;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/empodetails/getdraft`,isloading);
        res.data.empodetail = data.empodetail;
        
        return res;
    }

    /**
     * Check接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPODetailServiceBase
     */
    public async Check(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/check`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}/check`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/check`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/check`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/check`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/check`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/empodetails/${context.empodetail}/check`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}/check`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/empodetails/${context.empodetail}/check`,data,isloading);
            
            return res;
        }
        if(context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emwplists/${context.emwplist}/empodetails/${context.empodetail}/check`,data,isloading);
            
            return res;
        }
        if(context.empo && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/empos/${context.empo}/empodetails/${context.empodetail}/check`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/empodetails/${context.empodetail}/check`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/empodetails/${context.empodetail}/check`,data,isloading);
            return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPODetailServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/empodetails/${context.empodetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/empodetails/${context.empodetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emwplists/${context.emwplist}/empodetails/${context.empodetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.empo && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/empos/${context.empo}/empodetails/${context.empodetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/empodetails/${context.empodetail}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/empodetails/${context.empodetail}/checkkey`,data,isloading);
            return res;
    }

    /**
     * CreateRin接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPODetailServiceBase
     */
    public async CreateRin(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/createrin`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}/createrin`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/createrin`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/createrin`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/createrin`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/createrin`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/empodetails/${context.empodetail}/createrin`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}/createrin`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/empodetails/${context.empodetail}/createrin`,data,isloading);
            
            return res;
        }
        if(context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emwplists/${context.emwplist}/empodetails/${context.empodetail}/createrin`,data,isloading);
            
            return res;
        }
        if(context.empo && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/empos/${context.empo}/empodetails/${context.empodetail}/createrin`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/empodetails/${context.empodetail}/createrin`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/empodetails/${context.empodetail}/createrin`,data,isloading);
            return res;
    }

    /**
     * GenId接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPODetailServiceBase
     */
    public async GenId(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/genid`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}/genid`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/genid`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/genid`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/genid`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/genid`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/empodetails/${context.empodetail}/genid`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}/genid`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/empodetails/${context.empodetail}/genid`,data,isloading);
            
            return res;
        }
        if(context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emwplists/${context.emwplist}/empodetails/${context.empodetail}/genid`,data,isloading);
            
            return res;
        }
        if(context.empo && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/empos/${context.empo}/empodetails/${context.empodetail}/genid`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/empodetails/${context.empodetail}/genid`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/empodetails/${context.empodetail}/genid`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPODetailServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/${context.empodetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/empodetails/${context.empodetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/${context.empodetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/empodetails/${context.empodetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emwplist && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emwplists/${context.emwplist}/empodetails/${context.empodetail}/save`,data,isloading);
            
            return res;
        }
        if(context.empo && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/empos/${context.empo}/empodetails/${context.empodetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.empodetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/empodetails/${context.empodetail}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/empodetails/${context.empodetail}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchClosed接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPODetailServiceBase
     */
    public async FetchClosed(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchclosed`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/fetchclosed`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchclosed`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchclosed`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchclosed`,tempData,isloading);
            return res;
        }
        if(context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchclosed`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/empodetails/fetchclosed`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/fetchclosed`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/empodetails/fetchclosed`,tempData,isloading);
            return res;
        }
        if(context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emwplists/${context.emwplist}/empodetails/fetchclosed`,tempData,isloading);
            return res;
        }
        if(context.empo && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/empos/${context.empo}/empodetails/fetchclosed`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/empodetails/fetchclosed`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/empodetails/fetchclosed`,tempData,isloading);
        return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPODetailServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/empodetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/empodetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emwplists/${context.emwplist}/empodetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.empo && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/empos/${context.empo}/empodetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/empodetails/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/empodetails/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * FetchLaterYear接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPODetailServiceBase
     */
    public async FetchLaterYear(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchlateryear`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/fetchlateryear`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchlateryear`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchlateryear`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchlateryear`,tempData,isloading);
            return res;
        }
        if(context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchlateryear`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/empodetails/fetchlateryear`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/fetchlateryear`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/empodetails/fetchlateryear`,tempData,isloading);
            return res;
        }
        if(context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emwplists/${context.emwplist}/empodetails/fetchlateryear`,tempData,isloading);
            return res;
        }
        if(context.empo && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/empos/${context.empo}/empodetails/fetchlateryear`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/empodetails/fetchlateryear`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/empodetails/fetchlateryear`,tempData,isloading);
        return res;
    }

    /**
     * FetchWaitBook接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPODetailServiceBase
     */
    public async FetchWaitBook(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchwaitbook`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/fetchwaitbook`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchwaitbook`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchwaitbook`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchwaitbook`,tempData,isloading);
            return res;
        }
        if(context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchwaitbook`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/empodetails/fetchwaitbook`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/fetchwaitbook`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/empodetails/fetchwaitbook`,tempData,isloading);
            return res;
        }
        if(context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emwplists/${context.emwplist}/empodetails/fetchwaitbook`,tempData,isloading);
            return res;
        }
        if(context.empo && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/empos/${context.empo}/empodetails/fetchwaitbook`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/empodetails/fetchwaitbook`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/empodetails/fetchwaitbook`,tempData,isloading);
        return res;
    }

    /**
     * FetchWaitCheck接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPODetailServiceBase
     */
    public async FetchWaitCheck(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchwaitcheck`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/fetchwaitcheck`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchwaitcheck`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchwaitcheck`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchwaitcheck`,tempData,isloading);
            return res;
        }
        if(context.emitem && context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/emwplists/${context.emwplist}/empodetails/fetchwaitcheck`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/empodetails/fetchwaitcheck`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/empodetails/fetchwaitcheck`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/empodetails/fetchwaitcheck`,tempData,isloading);
            return res;
        }
        if(context.emwplist && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emwplists/${context.emwplist}/empodetails/fetchwaitcheck`,tempData,isloading);
            return res;
        }
        if(context.empo && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/empos/${context.empo}/empodetails/fetchwaitcheck`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emitems/${context.emitem}/empodetails/fetchwaitcheck`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/empodetails/fetchwaitcheck`,tempData,isloading);
        return res;
    }
}