import { Http,Util } from '@/utils';
import EMServiceEvlServiceBase from './emservice-evl-service-base';


/**
 * 服务商评估服务对象
 *
 * @export
 * @class EMServiceEvlService
 * @extends {EMServiceEvlServiceBase}
 */
export default class EMServiceEvlService extends EMServiceEvlServiceBase {

    /**
     * Creates an instance of  EMServiceEvlService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMServiceEvlService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}