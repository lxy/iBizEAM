import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 服务商评估服务对象基类
 *
 * @export
 * @class EMServiceEvlServiceBase
 * @extends {EntityServie}
 */
export default class EMServiceEvlServiceBase extends EntityService {

    /**
     * Creates an instance of  EMServiceEvlServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMServiceEvlServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMServiceEvlServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emserviceevl';
        this.APPDEKEY = 'emserviceevlid';
        this.APPDENAME = 'emserviceevls';
        this.APPDETEXT = 'emserviceevlname';
        this.APPNAME = 'eamweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMServiceEvlServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emservice && context.emserviceevl){
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emserviceevls/${context.emserviceevl}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/emserviceevls/${context.emserviceevl}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMServiceEvlServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emservice && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emserviceevls`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emserviceevls`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMServiceEvlServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emservice && context.emserviceevl){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emserviceevls/${context.emserviceevl}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emserviceevls/${context.emserviceevl}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMServiceEvlServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emservice && context.emserviceevl){
            let res:any = Http.getInstance().delete(`/emservices/${context.emservice}/emserviceevls/${context.emserviceevl}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/emserviceevls/${context.emserviceevl}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMServiceEvlServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emservice && context.emserviceevl){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emserviceevls/${context.emserviceevl}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emserviceevls/${context.emserviceevl}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMServiceEvlServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emservice && true){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emserviceevls/getdraft`,isloading);
            res.data.emserviceevl = data.emserviceevl;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/emserviceevls/getdraft`,isloading);
        res.data.emserviceevl = data.emserviceevl;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMServiceEvlServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emservice && context.emserviceevl){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emserviceevls/${context.emserviceevl}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/emserviceevls/${context.emserviceevl}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMServiceEvlServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emservice && context.emserviceevl){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emserviceevls/${context.emserviceevl}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emserviceevls/${context.emserviceevl}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMServiceEvlServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emserviceevls/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emserviceevls/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * FetchEvaluateTop5接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMServiceEvlServiceBase
     */
    public async FetchEvaluateTop5(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emserviceevls/fetchevaluatetop5`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emserviceevls/fetchevaluatetop5`,tempData,isloading);
        return res;
    }

    /**
     * FetchOverallEVL接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMServiceEvlServiceBase
     */
    public async FetchOverallEVL(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emservice && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/emservices/${context.emservice}/emserviceevls/fetchoverallevl`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/emserviceevls/fetchoverallevl`,tempData,isloading);
        return res;
    }
}