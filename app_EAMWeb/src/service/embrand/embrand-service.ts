import { Http,Util } from '@/utils';
import EMBrandServiceBase from './embrand-service-base';


/**
 * 品牌服务对象
 *
 * @export
 * @class EMBrandService
 * @extends {EMBrandServiceBase}
 */
export default class EMBrandService extends EMBrandServiceBase {

    /**
     * Creates an instance of  EMBrandService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMBrandService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}