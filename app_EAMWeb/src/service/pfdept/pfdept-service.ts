import { Http,Util } from '@/utils';
import PFDeptServiceBase from './pfdept-service-base';


/**
 * 部门服务对象
 *
 * @export
 * @class PFDeptService
 * @extends {PFDeptServiceBase}
 */
export default class PFDeptService extends PFDeptServiceBase {

    /**
     * Creates an instance of  PFDeptService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFDeptService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}