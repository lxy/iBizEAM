import { Http,Util } from '@/utils';
import EMACClassServiceBase from './emacclass-service-base';


/**
 * 总帐科目服务对象
 *
 * @export
 * @class EMACClassService
 * @extends {EMACClassServiceBase}
 */
export default class EMACClassService extends EMACClassServiceBase {

    /**
     * Creates an instance of  EMACClassService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMACClassService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}