import { Http,Util } from '@/utils';
import EMServiceServiceBase from './emservice-service-base';


/**
 * 服务商服务对象
 *
 * @export
 * @class EMServiceService
 * @extends {EMServiceServiceBase}
 */
export default class EMServiceService extends EMServiceServiceBase {

    /**
     * Creates an instance of  EMServiceService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMServiceService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}