import EMEQKPRCDAuthServiceBase from './emeqkprcd-auth-service-base';


/**
 * 关键点记录权限服务对象
 *
 * @export
 * @class EMEQKPRCDAuthService
 * @extends {EMEQKPRCDAuthServiceBase}
 */
export default class EMEQKPRCDAuthService extends EMEQKPRCDAuthServiceBase {

    /**
     * Creates an instance of  EMEQKPRCDAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQKPRCDAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}