import EMPlanCDTAuthServiceBase from './emplan-cdt-auth-service-base';


/**
 * 计划条件权限服务对象
 *
 * @export
 * @class EMPlanCDTAuthService
 * @extends {EMPlanCDTAuthServiceBase}
 */
export default class EMPlanCDTAuthService extends EMPlanCDTAuthServiceBase {

    /**
     * Creates an instance of  EMPlanCDTAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPlanCDTAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}