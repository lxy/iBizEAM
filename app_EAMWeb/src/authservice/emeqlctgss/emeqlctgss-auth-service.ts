import EMEQLCTGSSAuthServiceBase from './emeqlctgss-auth-service-base';


/**
 * 钢丝绳位置权限服务对象
 *
 * @export
 * @class EMEQLCTGSSAuthService
 * @extends {EMEQLCTGSSAuthServiceBase}
 */
export default class EMEQLCTGSSAuthService extends EMEQLCTGSSAuthServiceBase {

    /**
     * Creates an instance of  EMEQLCTGSSAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLCTGSSAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}