import AuthService from '../auth-service';

/**
 * 库间调整单权限服务对象基类
 *
 * @export
 * @class EMItemCSAuthServiceBase
 * @extends {AuthService}
 */
export default class EMItemCSAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMItemCSAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemCSAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMItemCSAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}