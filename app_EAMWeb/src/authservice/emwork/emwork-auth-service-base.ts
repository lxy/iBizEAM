import AuthService from '../auth-service';

/**
 * 加班工单权限服务对象基类
 *
 * @export
 * @class EMWorkAuthServiceBase
 * @extends {AuthService}
 */
export default class EMWorkAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMWorkAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWorkAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMWorkAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}