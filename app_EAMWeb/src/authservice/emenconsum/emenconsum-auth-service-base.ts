import AuthService from '../auth-service';

/**
 * 能耗权限服务对象基类
 *
 * @export
 * @class EMENConsumAuthServiceBase
 * @extends {AuthService}
 */
export default class EMENConsumAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMENConsumAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMENConsumAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMENConsumAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}