import EMENConsumAuthServiceBase from './emenconsum-auth-service-base';


/**
 * 能耗权限服务对象
 *
 * @export
 * @class EMENConsumAuthService
 * @extends {EMENConsumAuthServiceBase}
 */
export default class EMENConsumAuthService extends EMENConsumAuthServiceBase {

    /**
     * Creates an instance of  EMENConsumAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMENConsumAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}