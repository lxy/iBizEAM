import AuthService from '../auth-service';

/**
 * 设备类型权限服务对象基类
 *
 * @export
 * @class EMEQTypeAuthServiceBase
 * @extends {AuthService}
 */
export default class EMEQTypeAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMEQTypeAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQTypeAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMEQTypeAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}