import EMEQTypeAuthServiceBase from './emeqtype-auth-service-base';


/**
 * 设备类型权限服务对象
 *
 * @export
 * @class EMEQTypeAuthService
 * @extends {EMEQTypeAuthServiceBase}
 */
export default class EMEQTypeAuthService extends EMEQTypeAuthServiceBase {

    /**
     * Creates an instance of  EMEQTypeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQTypeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}