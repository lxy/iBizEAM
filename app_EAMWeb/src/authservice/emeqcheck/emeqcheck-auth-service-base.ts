import AuthService from '../auth-service';

/**
 * 维修记录权限服务对象基类
 *
 * @export
 * @class EMEQCheckAuthServiceBase
 * @extends {AuthService}
 */
export default class EMEQCheckAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMEQCheckAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQCheckAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMEQCheckAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}