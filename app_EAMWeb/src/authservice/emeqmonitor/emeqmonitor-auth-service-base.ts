import AuthService from '../auth-service';

/**
 * 设备状态监控权限服务对象基类
 *
 * @export
 * @class EMEQMonitorAuthServiceBase
 * @extends {AuthService}
 */
export default class EMEQMonitorAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMEQMonitorAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQMonitorAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMEQMonitorAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}