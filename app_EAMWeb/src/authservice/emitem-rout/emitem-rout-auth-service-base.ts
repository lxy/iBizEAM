import AuthService from '../auth-service';

/**
 * 退货单权限服务对象基类
 *
 * @export
 * @class EMItemROutAuthServiceBase
 * @extends {AuthService}
 */
export default class EMItemROutAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMItemROutAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemROutAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMItemROutAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}