import EMEQKeepAuthServiceBase from './emeqkeep-auth-service-base';


/**
 * 维护保养权限服务对象
 *
 * @export
 * @class EMEQKeepAuthService
 * @extends {EMEQKeepAuthServiceBase}
 */
export default class EMEQKeepAuthService extends EMEQKeepAuthServiceBase {

    /**
     * Creates an instance of  EMEQKeepAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQKeepAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}