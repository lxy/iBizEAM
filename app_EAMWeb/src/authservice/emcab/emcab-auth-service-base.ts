import AuthService from '../auth-service';

/**
 * 货架权限服务对象基类
 *
 * @export
 * @class EMCabAuthServiceBase
 * @extends {AuthService}
 */
export default class EMCabAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMCabAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMCabAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMCabAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}