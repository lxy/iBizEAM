import EMCabAuthServiceBase from './emcab-auth-service-base';


/**
 * 货架权限服务对象
 *
 * @export
 * @class EMCabAuthService
 * @extends {EMCabAuthServiceBase}
 */
export default class EMCabAuthService extends EMCabAuthServiceBase {

    /**
     * Creates an instance of  EMCabAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMCabAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}