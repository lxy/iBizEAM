import EMItemPUseAuthServiceBase from './emitem-puse-auth-service-base';


/**
 * 领料单权限服务对象
 *
 * @export
 * @class EMItemPUseAuthService
 * @extends {EMItemPUseAuthServiceBase}
 */
export default class EMItemPUseAuthService extends EMItemPUseAuthServiceBase {

    /**
     * Creates an instance of  EMItemPUseAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemPUseAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}