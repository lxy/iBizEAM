import EMServiceAuthServiceBase from './emservice-auth-service-base';


/**
 * 服务商权限服务对象
 *
 * @export
 * @class EMServiceAuthService
 * @extends {EMServiceAuthServiceBase}
 */
export default class EMServiceAuthService extends EMServiceAuthServiceBase {

    /**
     * Creates an instance of  EMServiceAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMServiceAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}