import AuthService from '../auth-service';

/**
 * 订单权限服务对象基类
 *
 * @export
 * @class EMPOAuthServiceBase
 * @extends {AuthService}
 */
export default class EMPOAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMPOAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPOAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMPOAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}