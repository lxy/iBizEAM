import AuthService from '../auth-service';

/**
 * 机种编号权限服务对象基类
 *
 * @export
 * @class EMMachineCategoryAuthServiceBase
 * @extends {AuthService}
 */
export default class EMMachineCategoryAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMMachineCategoryAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMMachineCategoryAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMMachineCategoryAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}