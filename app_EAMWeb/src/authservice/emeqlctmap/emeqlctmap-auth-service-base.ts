import AuthService from '../auth-service';

/**
 * 位置关系权限服务对象基类
 *
 * @export
 * @class EMEQLCTMapAuthServiceBase
 * @extends {AuthService}
 */
export default class EMEQLCTMapAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMEQLCTMapAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLCTMapAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMEQLCTMapAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}