import EMEQLCTMapAuthServiceBase from './emeqlctmap-auth-service-base';


/**
 * 位置关系权限服务对象
 *
 * @export
 * @class EMEQLCTMapAuthService
 * @extends {EMEQLCTMapAuthServiceBase}
 */
export default class EMEQLCTMapAuthService extends EMEQLCTMapAuthServiceBase {

    /**
     * Creates an instance of  EMEQLCTMapAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLCTMapAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}