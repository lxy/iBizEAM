import AuthService from '../auth-service';

/**
 * 机型权限服务对象基类
 *
 * @export
 * @class EMMachModelAuthServiceBase
 * @extends {AuthService}
 */
export default class EMMachModelAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMMachModelAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMMachModelAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMMachModelAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}