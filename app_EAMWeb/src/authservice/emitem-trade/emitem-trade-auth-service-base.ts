import AuthService from '../auth-service';

/**
 * 物品交易权限服务对象基类
 *
 * @export
 * @class EMItemTradeAuthServiceBase
 * @extends {AuthService}
 */
export default class EMItemTradeAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMItemTradeAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemTradeAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMItemTradeAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}