import AuthService from '../auth-service';

/**
 * 资产类别权限服务对象基类
 *
 * @export
 * @class EMAssetClassAuthServiceBase
 * @extends {AuthService}
 */
export default class EMAssetClassAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMAssetClassAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMAssetClassAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMAssetClassAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}