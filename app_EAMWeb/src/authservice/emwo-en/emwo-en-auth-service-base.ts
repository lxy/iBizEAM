import AuthService from '../auth-service';

/**
 * 能耗登记工单权限服务对象基类
 *
 * @export
 * @class EMWO_ENAuthServiceBase
 * @extends {AuthService}
 */
export default class EMWO_ENAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMWO_ENAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWO_ENAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMWO_ENAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}