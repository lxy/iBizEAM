import AuthService from '../auth-service';

/**
 * 计划步骤权限服务对象基类
 *
 * @export
 * @class EMPlanDetailAuthServiceBase
 * @extends {AuthService}
 */
export default class EMPlanDetailAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMPlanDetailAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPlanDetailAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMPlanDetailAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}