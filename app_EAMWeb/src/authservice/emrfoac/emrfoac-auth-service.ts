import EMRFOACAuthServiceBase from './emrfoac-auth-service-base';


/**
 * 方案权限服务对象
 *
 * @export
 * @class EMRFOACAuthService
 * @extends {EMRFOACAuthServiceBase}
 */
export default class EMRFOACAuthService extends EMRFOACAuthServiceBase {

    /**
     * Creates an instance of  EMRFOACAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFOACAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}