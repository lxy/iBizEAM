import AuthService from '../auth-service';

/**
 * 设备运行日志权限服务对象基类
 *
 * @export
 * @class EMEQWLAuthServiceBase
 * @extends {AuthService}
 */
export default class EMEQWLAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMEQWLAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQWLAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMEQWLAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}