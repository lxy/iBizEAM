import AuthService from '../auth-service';

/**
 * 外委保养工单权限服务对象基类
 *
 * @export
 * @class EMWO_OSCAuthServiceBase
 * @extends {AuthService}
 */
export default class EMWO_OSCAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMWO_OSCAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWO_OSCAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMWO_OSCAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}