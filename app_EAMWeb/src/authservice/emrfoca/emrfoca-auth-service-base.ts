import AuthService from '../auth-service';

/**
 * 原因权限服务对象基类
 *
 * @export
 * @class EMRFOCAAuthServiceBase
 * @extends {AuthService}
 */
export default class EMRFOCAAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMRFOCAAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFOCAAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMRFOCAAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}