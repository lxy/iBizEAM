import EMEITIResAuthServiceBase from './emeitires-auth-service-base';


/**
 * 轮胎清单权限服务对象
 *
 * @export
 * @class EMEITIResAuthService
 * @extends {EMEITIResAuthServiceBase}
 */
export default class EMEITIResAuthService extends EMEITIResAuthServiceBase {

    /**
     * Creates an instance of  EMEITIResAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEITIResAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}