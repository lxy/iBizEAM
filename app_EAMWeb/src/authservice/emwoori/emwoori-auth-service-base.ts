import AuthService from '../auth-service';

/**
 * 工单来源权限服务对象基类
 *
 * @export
 * @class EMWOORIAuthServiceBase
 * @extends {AuthService}
 */
export default class EMWOORIAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMWOORIAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWOORIAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMWOORIAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}