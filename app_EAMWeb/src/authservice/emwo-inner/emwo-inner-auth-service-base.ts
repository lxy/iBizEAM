import AuthService from '../auth-service';

/**
 * 内部工单权限服务对象基类
 *
 * @export
 * @class EMWO_INNERAuthServiceBase
 * @extends {AuthService}
 */
export default class EMWO_INNERAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMWO_INNERAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWO_INNERAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMWO_INNERAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}