import EMEQSpareAuthServiceBase from './emeqspare-auth-service-base';


/**
 * 备件包权限服务对象
 *
 * @export
 * @class EMEQSpareAuthService
 * @extends {EMEQSpareAuthServiceBase}
 */
export default class EMEQSpareAuthService extends EMEQSpareAuthServiceBase {

    /**
     * Creates an instance of  EMEQSpareAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQSpareAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}