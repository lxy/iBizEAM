import AuthService from '../auth-service';

/**
 * 位置权限服务对象基类
 *
 * @export
 * @class EMEQLocationAuthServiceBase
 * @extends {AuthService}
 */
export default class EMEQLocationAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMEQLocationAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLocationAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMEQLocationAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}