import AuthService from '../auth-service';

/**
 * 文档权限服务对象基类
 *
 * @export
 * @class EMDRWGAuthServiceBase
 * @extends {AuthService}
 */
export default class EMDRWGAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMDRWGAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMDRWGAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMDRWGAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}