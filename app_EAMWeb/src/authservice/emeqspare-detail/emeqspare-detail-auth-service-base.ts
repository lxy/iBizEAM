import AuthService from '../auth-service';

/**
 * 备件包明细权限服务对象基类
 *
 * @export
 * @class EMEQSpareDetailAuthServiceBase
 * @extends {AuthService}
 */
export default class EMEQSpareDetailAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMEQSpareDetailAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQSpareDetailAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMEQSpareDetailAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}