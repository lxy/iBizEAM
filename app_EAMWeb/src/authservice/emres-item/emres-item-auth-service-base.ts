import AuthService from '../auth-service';

/**
 * 物品资源权限服务对象基类
 *
 * @export
 * @class EMResItemAuthServiceBase
 * @extends {AuthService}
 */
export default class EMResItemAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMResItemAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMResItemAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMResItemAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}