import EMPODetailAuthServiceBase from './empodetail-auth-service-base';


/**
 * 订单条目权限服务对象
 *
 * @export
 * @class EMPODetailAuthService
 * @extends {EMPODetailAuthServiceBase}
 */
export default class EMPODetailAuthService extends EMPODetailAuthServiceBase {

    /**
     * Creates an instance of  EMPODetailAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPODetailAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}