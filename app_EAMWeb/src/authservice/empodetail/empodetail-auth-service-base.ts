import AuthService from '../auth-service';

/**
 * 订单条目权限服务对象基类
 *
 * @export
 * @class EMPODetailAuthServiceBase
 * @extends {AuthService}
 */
export default class EMPODetailAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMPODetailAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPODetailAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMPODetailAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}