import AuthService from '../auth-service';

/**
 * 职员权限服务对象基类
 *
 * @export
 * @class PFEmpAuthServiceBase
 * @extends {AuthService}
 */
export default class PFEmpAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  PFEmpAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  PFEmpAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof PFEmpAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}