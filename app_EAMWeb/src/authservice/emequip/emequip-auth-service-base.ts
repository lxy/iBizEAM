import AuthService from '../auth-service';

/**
 * 设备档案权限服务对象基类
 *
 * @export
 * @class EMEquipAuthServiceBase
 * @extends {AuthService}
 */
export default class EMEquipAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  EMEquipAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEquipAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof EMEquipAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        return curDefaultOPPrivs;
    }

}