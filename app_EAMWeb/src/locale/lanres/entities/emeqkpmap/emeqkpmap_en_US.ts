
export default {
  fields: {
    emeqkpmapname: "设备关键点关系",
    updateman: "更新人",
    description: "描述",
    orgid: "组织",
    createdate: "建立时间",
    updatedate: "更新时间",
    enable: "逻辑有效标志",
    emeqkpmapid: "设备关键点关系标识",
    createman: "建立人",
    refobjname: "设备关键点归属",
    kpname: "设备关键点",
    kpid: "设备关键点",
    refobjid: "设备关键点归属",
  },
};