
export default {
  fields: {
    emitemtypeid: "物品类型标识",
    description: "描述",
    itemtypecode: "物品类型代码",
    orgid: "组织",
    updatedate: "更新时间",
    enable: "逻辑有效标志",
    createman: "建立人",
    emitemtypename: "物品类型名称",
    updateman: "更新人",
    itemtypeinfo: "物品类型信息",
    createdate: "建立时间",
    itemtypepcode: "上级类型代码",
    itembtypename: "一级类",
    itemmtypename: "二级类",
    itemtypepname: "上级类型",
    itemtypepid: "上级类型",
    itemmtypeid: "二级类",
    itembtypeid: "一级类",
  },
	views: {
		gridview: {
			caption: "物品类型",
      		title: "物品类型",
		},
		optionview: {
			caption: "物品类型",
      		title: "物品类型选项操作视图",
		},
		treeexpview: {
			caption: "物品类型",
      		title: "物品类型树导航视图",
		},
		editview_itemtype_editmode: {
			caption: "物品类型",
      		title: "物品类型",
		},
		pickupgridview: {
			caption: "物品类型",
      		title: "物品类型选择表格视图",
		},
		pickupview: {
			caption: "物品类型",
      		title: "物品类型数据选择视图",
		},
		editview: {
			caption: "物品类型",
      		title: "物品类型",
		},
		itemtreeexpview: {
			caption: "物品",
      		title: "物品",
		},
		editview_itemtype: {
			caption: "物品类型",
      		title: "物品类型",
		},
		gridexpview: {
			caption: "物品类型",
      		title: "物品类型表格导航视图",
		},
		editview_editmode: {
			caption: "物品类型",
      		title: "物品类型",
		},
		pickuptreeview: {
			caption: "物品类型",
      		title: "物品类型选择树视图",
		},
		treepickupview: {
			caption: "物品类型",
      		title: "物品类型数据选择视图",
		},
		infotreeexpview: {
			caption: "物品",
      		title: "物品",
		},
	},
	main3_form: {
		details: {
			grouppanel2: "物品类型信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "物品类型标识", 
			srfmajortext: "物品类型名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			itemtypecode: "物品类型代码", 
			emitemtypename: "物品类型名称", 
			itemtypepname: "上级类型", 
			itembtypename: "一级类", 
			itemmtypename: "二级类", 
			itemmtypeid: "二级类", 
			itemtypepid: "上级类型", 
			emitemtypeid: "物品类型标识", 
			itembtypeid: "一级类", 
		},
		uiactions: {
		},
	},
	main2_form: {
		details: {
			grouppanel2: "物品类型信息", 
			grouppanel8: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "物品类型标识", 
			srfmajortext: "物品类型名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			itemtypecode: "物品类型代码", 
			emitemtypename: "物品类型名称", 
			itemtypepname: "上级类型", 
			itembtypename: "一级类", 
			itemmtypename: "二级类", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emitemtypeid: "物品类型标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			itemtypecode: "物品类型代码",
			emitemtypename: "物品类型名称",
			itemtypepcode: "上级类型代码",
			itemtypepname: "上级类型",
		},
		uiactions: {
		},
	},
	nav_grid: {
		columns: {
			itemtypecode: "物品类型代码",
			emitemtypename: "物品类型名称",
			itemtypepname: "上级类型",
		},
		uiactions: {
		},
	},
	pickup_grid: {
		columns: {
			itemtypecode: "物品类型代码",
			emitemtypename: "物品类型名称",
			itemtypepname: "上级类型",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editview_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "Save",
			tip: "Save",
		},
	},
	editview_itemtypetoolbar_toolbar: {
		deuiaction1: {
			caption: "编辑",
			tip: "编辑",
		},
	},
	editview_itemtype_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	editviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "编辑",
			tip: "编辑",
		},
	},
	info_treeview: {
		nodes: {
			itemtype: "全部物品类型",
			root: "默认根节点",
		},
		uiactions: {
        emitemtype_new: "添加类型",
		},
	},
	itemtypetree_treeview: {
		nodes: {
			subitem: "下属物品",
			root: "默认根节点",
			itemtype: "物品类型",
			subtype: "下属类型",
		},
		uiactions: {
		},
	},
	itemtype_treeview: {
		nodes: {
			root: "默认根节点",
			allitem: "全部物品",
		},
		uiactions: {
		},
	},
};