export default {
  fields: {
    createman: "建立人",
    machtypecode: "机种编码",
    emmachmodelid: "流水号",
    updateman: "更新人",
    createdate: "建立时间",
    remarks: "备注",
    enable: "逻辑有效标志",
    updatedate: "更新时间",
    emmachmodelname: "机型名称",
  },
	views: {
		pickupgridview: {
			caption: "机型",
      		title: "机型选择表格视图",
		},
		pickupview: {
			caption: "机型",
      		title: "机型数据选择视图",
		},
	},
	main_grid: {
		columns: {
			emmachmodelname: "机型名称",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};