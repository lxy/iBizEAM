
export default {
  fields: {
    createman: "建立人",
    tiresstate: "轮胎状态",
    description: "描述",
    par: "使用气压",
    amount: "价格",
    enable: "逻辑有效标志",
    picparams: "图形8*8=11-88",
    valve: "预警期限(天)",
    replacedate: "更换时间",
    createdate: "建立时间",
    eqmodelcode: "型号",
    orgid: "组织",
    newoldflag: "新旧标志",
    changp: "厂牌",
    systemparam: "材质层数",
    replacereason: "更换原因",
    lctdesc: "轮胎备注",
    updatedate: "更新时间",
    tirestype: "轮胎车型",
    lcttiresinfo: "轮胎信息",
    haveinner: "有内胎",
    updateman: "更新人",
    mccode: "出厂编号",
    labservicename: "供应商",
    mservicename: "制造商",
    eqlocationinfo: "位置信息",
    equipname: "设备",
    equipid: "设备",
    emeqlocationid: "位置标识",
    labserviceid: "供应商",
    mserviceid: "制造商",
  },
	views: {
		pickupgridview: {
			caption: "轮胎位置",
      		title: "轮胎位置选择表格视图",
		},
		pickupview: {
			caption: "轮胎位置",
      		title: "轮胎位置数据选择视图",
		},
		editview: {
			caption: "轮胎位置",
      		title: "轮胎位置",
		},
		gridview: {
			caption: "轮胎位置",
      		title: "轮胎位置",
		},
	},
	main_form: {
		details: {
			group1: "轮胎位置基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "位置标识", 
			srfmajortext: "位置信息", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			lcttiresinfo: "轮胎信息", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emeqlocationid: "位置标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			lcttiresinfo: "轮胎信息",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			lcttiresinfo: "轮胎信息",
			equipname: "设备",
			newoldflag: "新旧标志",
			tiresstate: "轮胎状态",
			haveinner: "有内胎",
			eqmodelcode: "型号",
			changp: "厂牌",
			systemparam: "材质层数",
			par: "使用气压",
			amount: "价格",
			mccode: "出厂编号",
			labservicename: "供应商",
			mservicename: "制造商",
			replacereason: "更换原因",
			replacedate: "更换时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
};