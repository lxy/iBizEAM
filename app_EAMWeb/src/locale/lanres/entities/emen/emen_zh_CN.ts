export default {
  fields: {
    createdate: "建立时间",
    energydesc: "能源备注",
    emenid: "能源标识",
    description: "描述",
    energycode: "能源代码",
    energytypeid: "能源类型",
    updateman: "更新人",
    createman: "建立人",
    vrate: "倍率",
    enable: "逻辑有效标志",
    orgid: "组织",
    updatedate: "更新时间",
    price: "能源单价",
    emenname: "能源名称",
    energyinfo: "能源信息",
    unitname: "单位",
    itemmtypeid: "物品二级类",
    itemname: "物品",
    itemprice: "物品库存单价",
    itemmtypename: "物品二级类",
    itemtypeid: "物品类型",
    itembtypeid: "物品大类",
    itembtypename: "物品大类",
    itemid: "物品",
  },
	views: {
		gridview: {
			caption: "能源",
      		title: "能源",
		},
		editview: {
			caption: "能源",
      		title: "能源编辑视图",
		},
		editview9_editmode: {
			caption: "能源信息",
      		title: "能源信息",
		},
		editview9: {
			caption: "能源信息",
      		title: "能源信息",
		},
		pickupview: {
			caption: "能源",
      		title: "能源数据选择视图",
		},
		pickupgridview: {
			caption: "能源",
      		title: "能源选择表格视图",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "能源信息", 
			grouppanel10: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "能源标识", 
			srfmajortext: "能源名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			energycode: "能源代码", 
			emenname: "能源名称", 
			energytypeid: "能源类型", 
			vrate: "倍率", 
			price: "能源单价", 
			itemname: "物品", 
			energydesc: "能源备注", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emenid: "能源标识", 
		},
		uiactions: {
		},
	},
	main3_form: {
		details: {
			grouppanel2: "能源信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "能源标识", 
			srfmajortext: "能源名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			energycode: "能源代码", 
			emenname: "能源名称", 
			energytypeid: "能源类型", 
			vrate: "倍率", 
			price: "能源单价", 
			itemname: "物品", 
			energydesc: "能源备注", 
			emenid: "能源标识", 
			itemid: "物品", 
		},
		uiactions: {
		},
	},
	main_form: {
		details: {
			group1: "能源基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "能源标识", 
			srfmajortext: "能源名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			energyinfo: "能源信息", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emenid: "能源标识", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			energycode: "能源代码",
			emenname: "能源名称",
			energytypeid: "能源类型",
			vrate: "倍率",
			energydesc: "能源备注",
			price: "能源单价",
			itemname: "物品",
			description: "描述",
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			energyinfo: "能源信息",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editview9toolbar_toolbar: {
		deuiaction1: {
			caption: "编辑",
			tip: "编辑",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editview9_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem23: {
			caption: "第一个记录",
			tip: "第一个记录",
		},
		tbitem24: {
			caption: "上一个记录",
			tip: "上一个记录",
		},
		tbitem25: {
			caption: "下一个记录",
			tip: "下一个记录",
		},
		tbitem26: {
			caption: "最后一个记录",
			tip: "最后一个记录",
		},
		tbitem21: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "帮助",
			tip: "帮助",
		},
	},
};