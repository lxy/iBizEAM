
export default {
  fields: {
    updateman: "更新人",
    emrfodetypeid: "现象分类标识",
    createdate: "建立时间",
    orgid: "组织",
    emrfodetypename: "现象分类名称",
    description: "描述",
    rfodetypecode: "现象分类代码",
    rfodetypeinfo: "现象分类信息",
    updatedate: "更新时间",
    createman: "建立人",
    enable: "逻辑有效标志",
  },
	views: {
		gridview: {
			caption: "现象分类",
      		title: "现象分类",
		},
		editview: {
			caption: "现象分类",
      		title: "现象分类",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "现象分类信息", 
			grouppanel5: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "现象分类标识", 
			srfmajortext: "现象分类名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			rfodetypecode: "现象分类代码", 
			emrfodetypename: "现象分类名称", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emrfodetypeid: "现象分类标识", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			rfodetypecode: "现象分类代码",
			emrfodetypename: "现象分类名称",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};