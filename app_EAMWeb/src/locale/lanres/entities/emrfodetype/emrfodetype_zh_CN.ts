export default {
  fields: {
    updateman: "更新人",
    emrfodetypeid: "现象分类标识",
    createdate: "建立时间",
    orgid: "组织",
    emrfodetypename: "现象分类名称",
    description: "描述",
    rfodetypecode: "现象分类代码",
    rfodetypeinfo: "现象分类信息",
    updatedate: "更新时间",
    createman: "建立人",
    enable: "逻辑有效标志",
  },
	views: {
		gridview: {
			caption: "现象分类",
      		title: "现象分类",
		},
		editview: {
			caption: "现象分类",
      		title: "现象分类",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "现象分类信息", 
			grouppanel5: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "现象分类标识", 
			srfmajortext: "现象分类名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			rfodetypecode: "现象分类代码", 
			emrfodetypename: "现象分类名称", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emrfodetypeid: "现象分类标识", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			rfodetypecode: "现象分类代码",
			emrfodetypename: "现象分类名称",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};