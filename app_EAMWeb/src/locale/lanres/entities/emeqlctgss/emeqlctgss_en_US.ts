
export default {
  fields: {
    description: "描述",
    gssxm: "项目",
    updatedate: "更新时间",
    eqmodelcode: "型号",
    updateman: "更新人",
    createdate: "建立时间",
    valve: "预警期限(天)",
    enable: "逻辑有效标志",
    createman: "建立人",
    len: "长度",
    zj: "直径",
    orgid: "组织",
    equipname: "设备",
    eqlocationinfo: "钢丝绳信息",
    emeqlocationid: "位置标识",
    equipid: "设备",
  },
	views: {
		tabexpview: {
			caption: "钢丝绳位置超期预警",
      		title: "钢丝绳位置超期预警",
		},
		pickupgridview: {
			caption: "钢丝绳位置",
      		title: "钢丝绳位置选择表格视图",
		},
		gridview: {
			caption: "钢丝绳位置",
      		title: "钢丝绳位置",
		},
		editview: {
			caption: "钢丝绳位置",
      		title: "钢丝绳位置",
		},
		pickupview: {
			caption: "钢丝绳位置",
      		title: "钢丝绳位置数据选择视图",
		},
		gridview_cqyj: {
			caption: "钢丝绳位置",
      		title: "钢丝绳位置",
		},
		treeexpview: {
			caption: "钢丝绳位置树导航",
      		title: "钢丝绳位置树导航",
		},
		editview9_editmode: {
			caption: "钢丝绳位置",
      		title: "钢丝绳位置编辑视图",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "位置信息", 
			grouppanel9: "预警信息", 
			grouppanel11: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "位置标识", 
			srfmajortext: "钢丝绳信息", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emeqlocationid: "位置标识", 
			equipname: "设备", 
			gssxm: "项目", 
			eqmodelcode: "型号", 
			zj: "直径", 
			len: "长度", 
			valve: "预警期限(天)", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
		},
		uiactions: {
		},
	},
	main3_form: {
		details: {
			grouppanel2: "位置信息", 
			grouppanel9: "预警信息", 
			grouppanel11: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "位置标识", 
			srfmajortext: "钢丝绳信息", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emeqlocationid: "位置标识", 
			equipname: "设备", 
			gssxm: "项目", 
			eqmodelcode: "型号", 
			zj: "直径", 
			len: "长度", 
			valve: "预警期限(天)", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			equipid: "设备", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			eqlocationinfo: "钢丝绳信息",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	main3_grid: {
		columns: {
			eqlocationinfo: "钢丝绳信息",
			equipname: "设备",
			eqmodelcode: "型号",
			len: "长度",
			zj: "直径",
			valve: "预警期限(天)",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridview_cqyjtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editview9_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	yjtree_treeview: {
		nodes: {
			yjnode: "钢丝绳位置",
			root: "默认根节点",
		},
		uiactions: {
		},
	},
};