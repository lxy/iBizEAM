export default {
  fields: {
    description: "描述",
    gssxm: "项目",
    updatedate: "更新时间",
    eqmodelcode: "型号",
    updateman: "更新人",
    createdate: "建立时间",
    valve: "预警期限(天)",
    enable: "逻辑有效标志",
    createman: "建立人",
    len: "长度",
    zj: "直径",
    orgid: "组织",
    equipname: "设备",
    eqlocationinfo: "钢丝绳信息",
    emeqlocationid: "位置标识",
    equipid: "设备",
  },
	views: {
		tabexpview: {
			caption: "钢丝绳位置超期预警",
      		title: "钢丝绳位置超期预警",
		},
		pickupgridview: {
			caption: "钢丝绳位置",
      		title: "钢丝绳位置选择表格视图",
		},
		gridview: {
			caption: "钢丝绳位置",
      		title: "钢丝绳位置",
		},
		editview: {
			caption: "钢丝绳位置",
      		title: "钢丝绳位置",
		},
		pickupview: {
			caption: "钢丝绳位置",
      		title: "钢丝绳位置数据选择视图",
		},
		gridview_cqyj: {
			caption: "钢丝绳位置",
      		title: "钢丝绳位置",
		},
		treeexpview: {
			caption: "钢丝绳位置树导航",
      		title: "钢丝绳位置树导航",
		},
		editview9_editmode: {
			caption: "钢丝绳位置",
      		title: "钢丝绳位置编辑视图",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "位置信息", 
			grouppanel9: "预警信息", 
			grouppanel11: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "位置标识", 
			srfmajortext: "钢丝绳信息", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emeqlocationid: "位置标识", 
			equipname: "设备", 
			gssxm: "项目", 
			eqmodelcode: "型号", 
			zj: "直径", 
			len: "长度", 
			valve: "预警期限(天)", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
		},
		uiactions: {
		},
	},
	main3_form: {
		details: {
			grouppanel2: "位置信息", 
			grouppanel9: "预警信息", 
			grouppanel11: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "位置标识", 
			srfmajortext: "钢丝绳信息", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emeqlocationid: "位置标识", 
			equipname: "设备", 
			gssxm: "项目", 
			eqmodelcode: "型号", 
			zj: "直径", 
			len: "长度", 
			valve: "预警期限(天)", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			equipid: "设备", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			eqlocationinfo: "钢丝绳信息",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	main3_grid: {
		columns: {
			eqlocationinfo: "钢丝绳信息",
			equipname: "设备",
			eqmodelcode: "型号",
			len: "长度",
			zj: "直径",
			valve: "预警期限(天)",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridview_cqyjtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editview9_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	yjtree_treeview: {
		nodes: {
			yjnode: "钢丝绳位置",
			root: "默认根节点",
		},
		uiactions: {
		},
	},
};