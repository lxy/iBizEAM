export default {
  fields: {
    updatedate: "更新时间",
    emoutputid: "能力标识",
    outputcode: "能力代码",
    description: "描述",
    orgid: "组织",
    createman: "建立人",
    emoutputname: "能力名称",
    updateman: "更新人",
    outputinfo: "能力信息",
    createdate: "建立时间",
    enable: "逻辑有效标志",
  },
	views: {
		editview9: {
			caption: "能力信息",
      		title: "能力信息",
		},
		pickupview: {
			caption: "能力",
      		title: "能力数据选择视图",
		},
		pickupgridview: {
			caption: "能力",
      		title: "能力选择表格视图",
		},
		editview: {
			caption: "能力信息",
      		title: "能力信息",
		},
		gridview: {
			caption: "能力",
      		title: "能力",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "能力信息", 
			grouppanel5: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "能力标识", 
			srfmajortext: "能力名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			outputcode: "能力代码", 
			emoutputname: "能力名称", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emoutputid: "能力标识", 
		},
		uiactions: {
		},
	},
	main3_form: {
		details: {
			grouppanel2: "能力信息", 
			grouppanel5: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "能力标识", 
			srfmajortext: "能力名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			outputcode: "能力代码", 
			emoutputname: "能力名称", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emoutputid: "能力标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			outputinfo: "能力信息",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			outputcode: "能力代码",
			emoutputname: "能力名称",
			description: "描述",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editview9toolbar_toolbar: {
		deuiaction1: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
};