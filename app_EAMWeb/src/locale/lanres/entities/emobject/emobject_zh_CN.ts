export default {
  fields: {
    updatedate: "更新时间",
    objectinfo: "对象信息",
    emobjecttype: "对象类型",
    updateman: "更新人",
    createman: "建立人",
    enable: "逻辑有效标志",
    emobjectid: "对象标识",
    orgid: "组织",
    objectcode: "对象代码",
    createdate: "建立时间",
    description: "描述",
    emobjectname: "对象名称",
    majorequipname: "主设备",
    majorequipid: "主设备",
  },
	views: {
		pickupgridview: {
			caption: "对象",
      		title: "对象选择表格视图",
		},
		pickupview: {
			caption: "对象",
      		title: "对象数据选择视图",
		},
	},
	main2_grid: {
		columns: {
			emobjectname: "对象名称",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};