export default {
  fields: {
    emstorepartid: "仓库库位标识",
    storepartid: "库位编号",
    emstorepartname: "仓库库位名称",
    enable: "逻辑有效标志",
    storepartinfo: "仓库库位信息",
    storepartcode: "仓库库位代码",
    updatedate: "更新时间",
    createman: "建立人",
    description: "描述",
    createdate: "建立时间",
    updateman: "更新人",
    orgid: "组织",
    storepartname: "仓库库位名称",
    storename: "仓库",
    storeid: "仓库",
  },
	views: {
		editview: {
			caption: "仓库库位",
      		title: "仓库库位",
		},
		pickupview: {
			caption: "仓库库位",
      		title: "仓库库位数据选择视图",
		},
		optionview: {
			caption: "仓库库位",
      		title: "仓库库位选项操作视图",
		},
		editview_9836: {
			caption: "仓库库位",
      		title: "仓库库位",
		},
		pickupgridview: {
			caption: "仓库库位",
      		title: "仓库库位选择表格视图",
		},
		editview_7215: {
			caption: "仓库库位",
      		title: "仓库库位",
		},
		gridview: {
			caption: "仓库库位",
      		title: "仓库库位",
		},
	},
	main3_form: {
		details: {
			grouppanel2: "仓库库位信息", 
			grouppanel6: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "仓库库位标识", 
			srfmajortext: "仓库库位名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			storepartcode: "仓库库位代码", 
			emstorepartname: "仓库库位名称", 
			storename: "仓库", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emstorepartid: "仓库库位标识", 
		},
		uiactions: {
		},
	},
	main2_form: {
		details: {
			grouppanel2: "仓库库位信息", 
			grouppanel6: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "仓库库位标识", 
			srfmajortext: "仓库库位名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			storepartcode: "仓库库位代码", 
			emstorepartname: "仓库库位名称", 
			storename: "仓库", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			storeid: "仓库", 
			emstorepartid: "仓库库位标识", 
		},
		uiactions: {
		},
	},
	main_form: {
		details: {
			group1: "仓库库位基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "仓库库位标识", 
			srfmajortext: "仓库库位名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emstorepartname: "仓库库位名称", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emstorepartid: "仓库库位标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			emstorepartname: "仓库库位名称",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			storepartcode: "仓库库位代码",
			emstorepartname: "仓库库位名称",
			storename: "仓库",
			description: "描述",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editview_9836toolbar_toolbar: {
		deuiaction1: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	editview_7215toolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
};