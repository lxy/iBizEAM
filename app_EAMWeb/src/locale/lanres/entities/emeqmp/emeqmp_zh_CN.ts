export default {
  fields: {
    updateman: "更新人",
    mpcode: "设备仪表代码",
    emeqmpname: "设备仪表名称",
    mpdesc: "仪表备注",
    createman: "建立人",
    mpinfo: "设备仪表信息",
    emeqmpid: "设备仪表标识",
    normalrefval: "正常参考值",
    mptypeid: "设备仪表类型",
    createdate: "建立时间",
    updatedate: "更新时间",
    mpscope: "仪表范围",
    orgid: "组织",
    description: "描述",
    enable: "逻辑有效标志",
    objname: "位置",
    equipname: "设备",
    objid: "位置",
    equipid: "设备",
  },
	views: {
		mpeditview: {
			caption: "设备仪表",
      		title: "设备仪表编辑视图",
		},
		pickupview: {
			caption: "设备仪表",
      		title: "设备仪表数据选择视图",
		},
		pickupgridview: {
			caption: "设备仪表",
      		title: "设备仪表选择表格视图",
		},
		gridview: {
			caption: "设备仪表",
      		title: "设备仪表表格视图",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "设备仪表信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "设备仪表标识", 
			srfmajortext: "设备仪表名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			mpcode: "设备仪表代码", 
			emeqmpname: "设备仪表名称", 
			equipname: "设备", 
			objname: "位置", 
			mptypeid: "设备仪表类型", 
			normalrefval: "正常参考值", 
			mpscope: "仪表范围", 
			mpdesc: "仪表备注", 
			orgid: "组织", 
			description: "描述", 
			objid: "位置", 
			equipid: "设备", 
			emeqmpid: "设备仪表标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			emeqmpname: "设备仪表名称",
			equipname: "设备",
			normalrefval: "正常参考值",
			mpdesc: "仪表备注",
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			mpcode: "设备仪表代码",
			emeqmpname: "设备仪表名称",
			mptypeid: "设备仪表类型",
			equipname: "设备",
			objname: "位置",
			mpdesc: "仪表备注",
			mpscope: "仪表范围",
			normalrefval: "正常参考值",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	mpeditviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
};