export default {
  fields: {
    updateman: "更新人",
    enable: "逻辑有效标志",
    updatedate: "更新时间",
    createdate: "建立时间",
    emrfodemapid: "现象引用标识",
    description: "描述",
    orgid: "组织",
    emrfodemapname: "现象引用名称",
    createman: "建立人",
    rfodename: "现象",
    refobjname: "现象引用",
    rfodeid: "现象",
    refobjid: "现象引用",
  },
	views: {
		editview: {
			caption: "现象引用",
      		title: "现象引用编辑视图",
		},
		dataview: {
			caption: "现象引用",
      		title: "现象引用数据视图",
		},
	},
	main3_form: {
		details: {
			grouppanel2: "现象引用信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "现象引用标识", 
			srfmajortext: "现象引用名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			rfodename: "现象", 
			refobjname: "现象引用", 
			rfodeid: "现象", 
			refobjid: "现象引用", 
			emrfodemapid: "现象引用标识", 
		},
		uiactions: {
		},
	},
	dataviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};