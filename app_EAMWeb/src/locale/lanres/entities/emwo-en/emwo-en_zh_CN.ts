export default {
  fields: {
    enable: "逻辑有效标志",
    woteam: "工单组",
    wfstate: "工作流状态",
    regionbegindate: "实际抄表时间",
    mpersonname: "制定人",
    rempname: "责任人",
    expiredate: "过期日期",
    wfstep: "流程步骤",
    prefee: "预算(￥)",
    updateman: "更新人",
    content: "详细内容",
    rempid: "责任人",
    priority: "优先级",
    mdate: "制定时间",
    emwo_enname: "工单名称",
    val: "值",
    regionenddate: "结束时间",
    wodate: "执行日期",
    createman: "建立人",
    description: "描述",
    recvpersonname: "指派抄表人",
    activelengths: "持续时间(H)",
    vrate: "倍率",
    mpersonid: "制定人",
    curval: "本次记录值",
    wostate: "工单状态",
    worklength: "实际工时(分)",
    wogroup: "工单分组",
    emwotype: "工单分组",
    wpersonname: "抄表人",
    emwo_enid: "工单编号",
    lastval: "上次记录值",
    recvpersonid: "指派抄表人",
    wfinstanceid: "工作流实例",
    woteam_show: "工单组",
    rdeptid: "责任部门",
    bdate: "上次采集时间",
    wpersonid: "抄表人",
    updatedate: "更新时间",
    createdate: "建立时间",
    wotype: "工单类型",
    eqstoplength: "停运时间(分)",
    wresult: "执行结果",
    rdeptname: "责任部门",
    nval: "能耗值",
    archive: "归档",
    wodesc: "工单内容",
    orgid: "组织",
    rfomoname: "模式",
    rfoacname: "方案",
    rteamname: "责任班组",
    rfocaname: "原因",
    rservicename: "服务商",
    dptype: "测点类型",
    objname: "位置",
    wopname_show: "上级工单",
    wopname: "上级工单",
    acclassname: "总帐科目",
    rfodename: "现象",
    dpname: "能源",
    wooriname: "工单来源",
    wooritype: "来源类型",
    equipname: "设备",
    dpid: "能源",
    objid: "位置",
    rserviceid: "服务商",
    equipid: "设备",
    wooriid: "工单来源",
    acclassid: "总帐科目",
    rfomoid: "模式",
    rteamid: "责任班组",
    rfocaid: "原因",
    rfodeid: "现象",
    wopid: "上级工单",
    rfoacid: "方案",
  },
	views: {
		editview: {
			caption: "能耗登记工单",
      		title: "能耗登记工单",
		},
		gridview: {
			caption: "能耗登记工单",
      		title: "能耗登记工单",
		},
		wovieweditview: {
			caption: "能耗登记工单",
      		title: "能耗登记工单",
		},
		editview_editmode: {
			caption: "能耗登记工单",
      		title: "能耗登记工单",
		},
	},
	main3_form: {
		details: {
			grouppanel2: "工单信息", 
			grouppanel9: "执行信息", 
			grouppanel18: "责任信息", 
			formpage1: "基本信息", 
			grouppanel24: "高级信息", 
			grouppanel29: "操作信息", 
			formpage23: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "工单编号", 
			srfmajortext: "工单名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emwo_enid: "工单编号(自动)", 
			emwo_enname: "工单名称", 
			equipname: "设备", 
			objname: "位置", 
			wodate: "安排日期", 
			activelengths: "持续时间(H)", 
			dpname: "能源", 
			wpersonid: "抄表人", 
			wpersonname: "抄表人", 
			bdate: "上次采集时间", 
			regionbegindate: "实际抄表时间", 
			lastval: "上次记录值", 
			curval: "本次记录值", 
			vrate: "倍率", 
			nval: "抄表值", 
			rempid: "责任人", 
			rempname: "责任人", 
			rdeptname: "责任部门", 
			recvpersonid: "指派抄表人", 
			recvpersonname: "指派抄表人", 
			mpersonid: "制定人", 
			mpersonname: "制定人", 
			wogroup: "工单分组", 
			wopname: "上级工单", 
			wooriname: "工单来源", 
			wooritype: "来源类型", 
			orgid: "组织", 
			archive: "归档", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			wooriid: "工单来源", 
			objid: "位置", 
			dpid: "能源", 
			equipid: "设备", 
			wopid: "上级工单", 
		},
		uiactions: {
		},
	},
	main2_form: {
		details: {
			grouppanel2: "工单信息", 
			grouppanel9: "执行信息", 
			grouppanel18: "责任信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "工单编号", 
			srfmajortext: "工单名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emwo_enid: "工单编号(自动)", 
			emwo_enname: "工单名称", 
			equipname: "设备", 
			objname: "位置", 
			wodate: "安排日期", 
			activelengths: "持续时间(H)", 
			dpname: "能源", 
			wpersonid: "抄表人", 
			wpersonname: "抄表人", 
			bdate: "上次采集时间", 
			regionbegindate: "实际抄表时间", 
			lastval: "上次记录值", 
			curval: "本次记录值", 
			vrate: "倍率", 
			nval: "抄表值", 
			rempid: "责任人", 
			rempname: "责任人", 
			rdeptname: "责任部门", 
			recvpersonid: "指派抄表人", 
			recvpersonname: "指派抄表人", 
			mpersonid: "制定人", 
			mpersonname: "制定人", 
		},
		uiactions: {
		},
	},
	main4_form: {
		details: {
			grouppanel2: "工单信息", 
			grouppanel9: "执行信息", 
			grouppanel18: "责任信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "工单编号", 
			srfmajortext: "工单名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emwo_enid: "工单编号(自动)", 
			emwo_enname: "工单名称", 
			equipname: "设备", 
			objname: "位置", 
			wodate: "安排日期", 
			activelengths: "持续时间(H)", 
			dpname: "能源", 
			wpersonid: "抄表人", 
			wpersonname: "抄表人", 
			bdate: "上次采集时间", 
			regionbegindate: "实际抄表时间", 
			lastval: "上次记录值", 
			curval: "本次记录值", 
			vrate: "倍率", 
			nval: "抄表值", 
			rempid: "责任人", 
			rempname: "责任人", 
			rdeptname: "责任部门", 
			recvpersonid: "指派抄表人", 
			recvpersonname: "指派抄表人", 
			mpersonid: "制定人", 
			mpersonname: "制定人", 
		},
		uiactions: {
		},
	},
	main4_grid: {
		columns: {
			emwo_enid: "工单编号",
			emwo_enname: "工单名称",
			equipname: "设备",
			objname: "位置",
			dpname: "能源",
			regionbegindate: "实际抄表时间",
			lastval: "上次记录值",
			curval: "本次记录值",
			nval: "能耗值",
			wpersonname: "抄表人",
			bdate: "上次采集时间",
			vrate: "倍率",
			woteam: "工单组",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editview_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	editviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "编辑",
			tip: "编辑",
		},
	},
};