export default {
  fields: {
    edate: "状态区间截至",
    updatedate: "更新时间",
    createman: "建立人",
    orgid: "组织",
    val: "运行状态",
    enable: "逻辑有效标志",
    emeqmonitorid: "设备状态监控标识",
    emeqmonitorname: "设备状态监控名称",
    createdate: "建立时间",
    description: "描述",
    updateman: "更新人",
    bdate: "状态区间起始",
    woname: "工单",
    equipname: "设备",
    woid: "工单",
    equipid: "设备",
  },
	views: {
		gridview: {
			caption: "设备状态监控",
      		title: "设备状态监控表格视图",
		},
		monitoreditview: {
			caption: "设备状态监控",
      		title: "设备状态监控编辑视图",
		},
	},
	main3_form: {
		details: {
			grouppanel2: "设备状态监控信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "设备状态监控标识", 
			srfmajortext: "设备状态监控名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			equipname: "设备", 
			val: "运行状态", 
			bdate: "状态区间起始", 
			edate: "状态区间截至", 
			woname: "工单", 
			emeqmonitorid: "设备状态监控标识", 
			woid: "工单", 
			equipid: "设备", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			equipname: "设备",
			val: "运行状态",
			bdate: "状态区间起始",
			edate: "状态区间截至",
			woname: "工单",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	monitoreditviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};