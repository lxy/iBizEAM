export default {
  fields: {
    updateman: "更新人",
    emeqspareid: "备件包标识",
    createdate: "建立时间",
    enable: "逻辑有效标志",
    emeqsparename: "备件包名称",
    createman: "建立人",
    orgid: "组织",
    eqsparecode: "备件包代码",
    eqspareinfo: "备件包信息",
    description: "描述",
    updatedate: "更新时间",
  },
	views: {
		listexpview: {
			caption: "备件包",
      		title: "备件包列表导航视图",
		},
		pickupgridview: {
			caption: "备件包",
      		title: "备件包选择表格视图",
		},
		maindashboardview9: {
			caption: "备件包信息",
      		title: "备件包信息",
		},
		editview_editmode: {
			caption: "备件包信息",
      		title: "备件包信息",
		},
		gridexpview: {
			caption: "备件包",
      		title: "备件包表格导航视图",
		},
		pickupview: {
			caption: "备件包",
      		title: "备件包数据选择视图",
		},
		editview: {
			caption: "备件包信息",
      		title: "备件包信息",
		},
		gridview: {
			caption: "备件包",
      		title: "备件包",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "备件包信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "备件包标识", 
			srfmajortext: "备件包名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			eqsparecode: "备件包代码", 
			emeqsparename: "备件包名称", 
			emeqspareid: "备件包标识", 
		},
		uiactions: {
		},
	},
	main3_form: {
		details: {
			grouppanel2: "备件包信息", 
			druipart1: "", 
			grouppanel1: "分组面板", 
			druipart2: "", 
			grouppanel3: "分组面板", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "备件包标识", 
			srfmajortext: "备件包名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			eqsparecode: "备件包代码", 
			emeqsparename: "备件包名称", 
			emeqspareid: "备件包标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			eqspareinfo: "备件包信息",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	main3_grid: {
		columns: {
			eqsparecode: "备件包代码",
			emeqsparename: "备件包名称",
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			eqsparecode: "备件包代码",
			emeqsparename: "备件包名称",
			description: "描述",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
			n_eqsparecode_like: "备件包代码(%)", 
			n_emeqsparename_like: "备件包名称(%)", 
		},
		uiactions: {
		},
	},
	editview_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
};