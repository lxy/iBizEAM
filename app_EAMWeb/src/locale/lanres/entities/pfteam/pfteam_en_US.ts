
export default {
  fields: {
    teamcode: "班组代码",
    enable: "逻辑有效标志",
    orgid: "组织",
    pfteamid: "班组标识",
    createman: "建立人",
    teamtypeid: "班组类型",
    updatedate: "更新时间",
    updateman: "更新人",
    createdate: "建立时间",
    description: "描述",
    teamfn: "职能",
    pfteamname: "班组名称",
    teaminfo: "班组信息",
    teamcapacity: "能力",
  },
	views: {
		gridview: {
			caption: "班组",
      		title: "班组",
		},
		editview_editmode: {
			caption: "班组",
      		title: "班组",
		},
		pickupgridview: {
			caption: "班组",
      		title: "班组选择表格视图",
		},
		pickupview: {
			caption: "班组",
      		title: "班组数据选择视图",
		},
		editview: {
			caption: "班组",
      		title: "班组",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "PF班组信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "班组标识", 
			srfmajortext: "班组名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			teamcode: "班组代码", 
			pfteamname: "班组名称", 
			teamtypeid: "班组类型", 
			orgid: "组织", 
			teamfn: "职能", 
			teamcapacity: "能力", 
			description: "描述", 
			pfteamid: "班组标识", 
		},
		uiactions: {
		},
	},
	main3_form: {
		details: {
			grouppanel2: "PF班组信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "班组标识", 
			srfmajortext: "班组名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			teamcode: "班组代码", 
			pfteamname: "班组名称", 
			teamtypeid: "班组类型", 
			orgid: "组织", 
			teamfn: "职能", 
			teamcapacity: "能力", 
			description: "描述", 
			pfteamid: "班组标识", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			teamcode: "班组代码",
			pfteamname: "班组名称",
			teamtypeid: "班组类型",
			orgid: "组织",
			teamcapacity: "能力",
			teamfn: "职能",
			description: "描述",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "编辑",
			tip: "编辑",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editview_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};