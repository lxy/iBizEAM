export default {
  fields: {
    orgid: "组织",
    createman: "建立人",
    eqlocationinfo: "位置信息",
    enable: "逻辑有效标志",
    emeqlocationname: "位置名称",
    updateman: "更新人",
    createdate: "建立时间",
    description: "描述",
    emeqlocationid: "位置标识",
    eqlocationcode: "位置代码",
    eqlocationtype: "位置类型",
    updatedate: "更新时间",
    majorequipname: "主设备",
    majorequipid: "主设备",
  },
	views: {
		treeexpview: {
			caption: "位置",
      		title: "位置树导航视图",
		},
		alllocgridview: {
			caption: "位置",
      		title: "位置",
		},
		maininfo: {
			caption: "位置",
      		title: "位置数据看板视图",
		},
		pickupgridview: {
			caption: "位置",
      		title: "位置选择表格视图",
		},
		editview: {
			caption: "位置",
      		title: "位置",
		},
		optionview: {
			caption: "位置新建",
      		title: "位置新建",
		},
		editview9: {
			caption: "位置",
      		title: "位置",
		},
		gridview: {
			caption: "位置",
      		title: "位置",
		},
		pickupview: {
			caption: "位置",
      		title: "位置数据选择视图",
		},
	},
	main3_form: {
		details: {
			grouppanel2: "位置信息", 
			druipart1: "", 
			grouppanel1: "位置关系", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "位置标识", 
			srfmajortext: "位置名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			eqlocationcode: "位置代码", 
			emeqlocationname: "位置名称", 
			eqlocationtype: "位置类型", 
			emeqlocationid: "位置标识", 
		},
		uiactions: {
		},
	},
	new_form: {
		details: {
			grouppanel2: "位置信息", 
			druipart1: "", 
			grouppanel1: "位置关系", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "位置标识", 
			srfmajortext: "位置名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			eqlocationcode: "位置代码", 
			emeqlocationname: "位置名称", 
			eqlocationtype: "位置类型", 
			emeqlocationid: "位置标识", 
		},
		uiactions: {
		},
	},
	main2_form: {
		details: {
			grouppanel2: "位置信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "位置标识", 
			srfmajortext: "位置名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			eqlocationcode: "位置代码", 
			emeqlocationname: "位置名称", 
			eqlocationtype: "位置类型", 
			emeqlocationid: "位置标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			eqlocationinfo: "位置信息",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			eqlocationcode: "位置代码",
			emeqlocationname: "位置名称",
			eqlocationtype: "位置类型",
			description: "描述",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
			n_eqlocationcode_like: "位置代码(%)", 
			n_emeqlocationname_like: "位置名称(%)", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	alllocgridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editview9toolbar_toolbar: {
		deuiaction1: {
			caption: "编辑",
			tip: "编辑",
		},
	},
	main_treeview: {
		nodes: {
			location: "全部位置",
			root: "默认根节点",
		},
		uiactions: {
		},
	},
};