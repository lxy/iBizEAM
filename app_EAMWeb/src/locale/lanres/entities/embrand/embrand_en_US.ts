
export default {
  fields: {
    embrandid: "流水号",
    createman: "建立人",
    enable: "逻辑有效标志",
    updateman: "更新人",
    updatedate: "更新时间",
    embrandname: "品牌名称",
    embrandcode: "品牌编码",
    createdate: "建立时间",
    machtypecode: "机种编码",
  },
	views: {
		pickupview: {
			caption: "品牌",
      		title: "品牌数据选择视图",
		},
		pickupgridview: {
			caption: "品牌",
      		title: "品牌选择表格视图",
		},
	},
	main_grid: {
		columns: {
			embrandname: "品牌名称",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};