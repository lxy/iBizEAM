
export default {
  fields: {
    batcode: "批次",
    emstockid: "库存标识",
    storepartgl: "库存货架管理",
    updatedate: "更新时间",
    stockinfo: "库存信息",
    createman: "建立人",
    orgid: "组织",
    enable: "逻辑有效标志",
    emstockname: "库存名称",
    createdate: "建立时间",
    amount: "库存金额",
    description: "描述",
    updateman: "更新人",
    stockcnt: "库存数量",
    storepartname: "库位",
    storename: "仓库",
    price: "平均价",
    itemname: "物品",
    itembtypename: "物品一级类型",
    storepartid: "库位",
    itemid: "物品",
    storeid: "仓库",
    itembtypeid: "物品一级类型",
  },
	views: {
		bystorepartgridview: {
			caption: "库存明细",
      		title: "库存明细",
		},
		bystoregridview: {
			caption: "库存明细",
      		title: "库存明细",
		},
		pickupview: {
			caption: "库存",
      		title: "库存数据选择视图",
		},
		gridview: {
			caption: "库存物品",
      		title: "库存物品",
		},
		bycabgridview: {
			caption: "库存明细",
      		title: "库存明细",
		},
		pickupgridview: {
			caption: "库存",
      		title: "库存选择表格视图",
		},
		editview: {
			caption: "库存明细",
      		title: "库存明细",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "库存信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "库存标识", 
			srfmajortext: "库存名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			storename: "仓库", 
			storepartname: "库位", 
			itemname: "物品", 
			stockcnt: "库存数量", 
			price: "平均价", 
			amount: "库存金额", 
			batcode: "批次", 
			storepartid: "库位", 
			storeid: "仓库", 
			emstockid: "库存标识", 
			itemid: "物品", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			itemname: "物品",
			storename: "仓库",
			storepartname: "库位",
			stockcnt: "库存数量",
			price: "平均价",
			amount: "库存金额",
			batcode: "批次",
			description: "描述",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	bystorepartgridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	bystoregridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	bycabgridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
};