
export default {
  fields: {
    emeqsparemapid: "备件包引用标识",
    createdate: "建立时间",
    orgid: "组织",
    enable: "逻辑有效标志",
    updatedate: "更新时间",
    description: "描述",
    createman: "建立人",
    updateman: "更新人",
    emeqsparemapname: "备件包引用",
    refobjname: "引用对象",
    eqsparename: "备件包",
    eqspareid: "备件包",
    refobjid: "对象标识",
  },
	views: {
		gridview: {
			caption: "备件包应用",
      		title: "备件包应用",
		},
		editview_editmode: {
			caption: "备件包引用",
      		title: "备件包引用",
		},
		dataview9: {
			caption: "备件包引用",
      		title: "备件包引用数据视图",
		},
		editview: {
			caption: "备件包引用",
      		title: "备件包引用",
		},
		dashboardview9: {
			caption: "备件包引用",
      		title: "备件包引用数据看板视图",
		},
		equipgridview9: {
			caption: "备件包应用",
      		title: "备件包应用",
		},
		gridview9: {
			caption: "备件包应用",
      		title: "备件包应用",
		},
	},
	main3_form: {
		details: {
			grouppanel2: "备件包引用信息", 
			grouppanel5: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "备件包引用标识", 
			srfmajortext: "备件包引用", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			eqsparename: "备件包", 
			refobjname: "引用对象", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emeqsparemapid: "备件包引用标识", 
			refobjid: "对象标识", 
			eqspareid: "备件包", 
		},
		uiactions: {
		},
	},
	main2_form: {
		details: {
			grouppanel2: "备件包引用信息", 
			grouppanel5: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "备件包引用标识", 
			srfmajortext: "备件包引用", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			eqsparename: "备件包", 
			refobjname: "引用对象", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emeqsparemapid: "备件包引用标识", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			refobjname: "引用对象",
			eqsparename: "备件包",
			description: "描述",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editview_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	editviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "编辑",
			tip: "编辑",
		},
	},
	dataview9toolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
	},
	gridview9toolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
	},
	equipgridview9toolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
	},
};