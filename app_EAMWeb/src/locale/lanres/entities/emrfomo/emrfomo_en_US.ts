
export default {
  fields: {
    emrfomoname: "模式名称",
    objid: "对象编号",
    createdate: "建立时间",
    rfomoinfo: "信息",
    rfomocode: "模式代码",
    createman: "建立人",
    enable: "逻辑有效标志",
    description: "描述",
    orgid: "组织",
    emrfomoid: "模式标识",
    updatedate: "更新时间",
    updateman: "更新人",
    rfodename: "现象",
    rfodeid: "现象",
  },
	views: {
		pickupgridview: {
			caption: "模式",
      		title: "模式选择表格视图",
		},
		editview: {
			caption: "模式信息",
      		title: "模式信息",
		},
		gridview: {
			caption: "模式",
      		title: "模式",
		},
		pickupview: {
			caption: "模式",
      		title: "模式数据选择视图",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "模式信息", 
			grouppanel6: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "模式标识", 
			srfmajortext: "模式名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			rfomocode: "模式代码", 
			emrfomoname: "模式名称", 
			rfodename: "现象", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			rfodeid: "现象", 
			emrfomoid: "模式标识", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			rfomocode: "模式代码",
			emrfomoname: "模式名称",
			rfodename: "现象",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};