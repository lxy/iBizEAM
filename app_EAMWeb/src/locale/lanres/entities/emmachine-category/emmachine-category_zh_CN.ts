export default {
  fields: {
    emmachinecategoryid: "流水号",
    enable: "逻辑有效标志",
    emmachinecategoryname: "机种名称",
    updateman: "更新人",
    createman: "建立人",
    remarks: "备注",
    jzinfo: "机种信息",
    createdate: "建立时间",
    machtypecode: "机种编码",
    updatedate: "更新时间",
  },
	views: {
		pickupview: {
			caption: "机种编号",
      		title: "机种编号数据选择视图",
		},
		pickupgridview: {
			caption: "机种编号",
      		title: "机种编号选择表格视图",
		},
	},
	main_grid: {
		columns: {
			jzinfo: "机种信息",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};