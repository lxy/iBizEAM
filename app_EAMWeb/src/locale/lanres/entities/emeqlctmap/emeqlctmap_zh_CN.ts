export default {
  fields: {
    updatedate: "更新时间",
    orgid: "组织",
    description: "描述",
    emeqlctmapid: "位置关系标识",
    createman: "建立人",
    updateman: "更新人",
    emeqlctmapname: "位置关系",
    createdate: "建立时间",
    enable: "逻辑有效标志",
    majorequipname: "主设备",
    eqlocationcode: "位置代码",
    eqlocationpname: "上级位置",
    eqlocationdesc: "位置描述",
    majorequipid: "主设备",
    eqlocationname: "位置",
    eqlocationid: "位置",
    eqlocationpid: "上级位置",
  },
	views: {
		gridview9: {
			caption: "位置关系",
      		title: "位置关系表格视图",
		},
		editview: {
			caption: "位置关系",
      		title: "位置关系编辑视图",
		},
	},
	main_form: {
		details: {
			grouppanel2: "位置关系信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "位置关系标识", 
			srfmajortext: "位置关系", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			eqlocationname: "位置", 
			eqlocationpname: "上级位置", 
			eqlocationid: "位置", 
			emeqlctmapid: "位置关系标识", 
			eqlocationpid: "上级位置", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			eqlocationpname: "上级位置",
			eqlocationname: "位置",
			description: "描述",
		},
		uiactions: {
		},
	},
	gridview9toolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem23: {
			caption: "第一个记录",
			tip: "第一个记录",
		},
		tbitem24: {
			caption: "上一个记录",
			tip: "上一个记录",
		},
		tbitem25: {
			caption: "下一个记录",
			tip: "下一个记录",
		},
		tbitem26: {
			caption: "最后一个记录",
			tip: "最后一个记录",
		},
		tbitem21: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "帮助",
			tip: "帮助",
		},
	},
};