
export default {
  fields: {
    description: "描述",
    enable: "逻辑有效标志",
    emrfocaid: "原因标识",
    updateman: "更新人",
    createdate: "建立时间",
    rfocacode: "原因代码",
    emrfocaname: "原因名称",
    createman: "建立人",
    orgid: "组织",
    rfocainfo: "信息",
    updatedate: "更新时间",
    objid: "对象编号",
    rfodename: "现象",
    rfomoname: "模式",
    rfodeid: "现象",
    rfomoid: "模式",
  },
	views: {
		pickupview: {
			caption: "原因",
      		title: "原因数据选择视图",
		},
		pickupgridview: {
			caption: "原因",
      		title: "原因选择表格视图",
		},
		editview: {
			caption: "原因信息",
      		title: "原因信息",
		},
		gridview: {
			caption: "原因",
      		title: "原因",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "原因信息", 
			grouppanel7: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "原因标识", 
			srfmajortext: "原因名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			rfocacode: "原因代码", 
			emrfocaname: "原因名称", 
			rfodename: "现象", 
			rfomoname: "模式", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			rfodeid: "现象", 
			emrfocaid: "原因标识", 
			rfomoid: "模式", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			rfocainfo: "信息",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			rfocacode: "原因代码",
			emrfocaname: "原因名称",
			rfodename: "现象",
			rfomoname: "模式",
			description: "描述",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};