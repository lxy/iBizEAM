
export default {
  fields: {
    mpersonname: "制定人",
    wostate: "工单状态",
    vrate: "倍率",
    mdate: "制定时间",
    wotype: "工单类型",
    mpersonid: "制定人",
    cplanflag: "转计划标志",
    waitbuy: "等待配件时(小时)",
    emwotype: "工单种类",
    description: "描述",
    rdeptname: "责任部门",
    wfstep: "流程步骤",
    prefee: "预算(￥)",
    activelengths: "持续时间(H)",
    waitmodi: "等待修理时(小时)",
    createdate: "建立时间",
    emwoid: "工单编号",
    mfee: "约合材料费(￥)",
    regionenddate: "结束时间",
    wogroup: "工单分组",
    worklength: "实际工时(分)",
    rdeptid: "责任部门",
    enable: "逻辑有效标志",
    content: "详细内容",
    woteam: "工单组",
    createman: "建立人",
    expiredate: "过期日期",
    yxcb: "影响船舶",
    recvpersonname: "接收人",
    priority: "优先级",
    woinfo: "工单信息",
    nval: "数值",
    updateman: "更新人",
    emwoname: "工单名称",
    eqstoplength: "停运时间(分)",
    wodate: "执行日期",
    wfinstanceid: "工作流实例",
    recvpersonid: "接收人",
    wresult: "执行结果",
    wpersonid: "执行人",
    rempname: "责任人",
    updatedate: "更新时间",
    wpersonname: "执行人",
    orgid: "组织",
    wodesc: "工单内容",
    wfstate: "工作流状态",
    rempid: "责任人",
    val: "值",
    archive: "归档",
    regionbegindate: "起始时间",
    rservicename: "服务商",
    wooriname: "工单来源",
    equipcode: "设备",
    rfoacname: "方案",
    rfomoname: "模式",
    acclassname: "总帐科目",
    equipname: "设备",
    wopname: "上级工单",
    rfodename: "现象",
    rteamname: "责任班组",
    stype: "统计归口类型分组",
    dptype: "测点类型",
    dpname: "测点",
    wooritype: "来源类型",
    sname: "统计归口类型",
    objname: "位置",
    rfocaname: "原因",
    equipid: "设备",
    rserviceid: "服务商",
    acclassid: "总帐科目",
    wopid: "上级工单",
    rfoacid: "方案",
    rfodeid: "现象",
    wooriid: "工单来源",
    rfomoid: "模式",
    rteamid: "责任班组",
    dpid: "测点",
    rfocaid: "原因",
    objid: "位置",
  },
	views: {
		pickupgridview: {
			caption: "工单",
      		title: "工单选择表格视图",
		},
		gridview: {
			caption: "工单",
      		title: "工单表格视图",
		},
		calendarexpview: {
			caption: "工单",
      		title: "工单日历导航视图",
		},
		pickupview: {
			caption: "工单",
      		title: "工单数据选择视图",
		},
		calendarview: {
			caption: "工单",
      		title: "工单日历视图",
		},
		treeexpview: {
			caption: "工单",
      		title: "工单树导航视图",
		},
	},
	main6_grid: {
		columns: {
			emwoid: "工单编号",
			emwoname: "工单名称",
			equipname: "设备",
			objname: "位置",
			wodate: "执行日期",
			rempname: "责任人",
			rdeptname: "责任部门",
			rteamname: "责任班组",
			rservicename: "服务商",
			wpersonname: "执行人",
			dpname: "测点",
			wopname: "上级工单",
			wooriname: "工单来源",
			regionbegindate: "起始时间",
			regionenddate: "结束时间",
			priority: "优先级",
			expiredate: "过期日期",
			emwotype: "工单种类",
			wogroup: "工单分组",
			wotype: "工单类型",
			worklength: "实际工时(分)",
			val: "值",
			wresult: "执行结果",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem16: {
			caption: "其它",
			tip: "其它",
		},
		tbitem21: {
			caption: "Export Data Model",
			tip: "导出数据模型",
		},
	},
	calendarviewcalendar_quicktoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
	},
	calendarviewcalendar_batchtoolbar_toolbar: {
		deuiaction2: {
			caption: "Remove",
			tip: "Remove {0}",
		},
	},
	wotree_treeview: {
		nodes: {
			all: "全部工单",
			en: "能耗工单",
			dp: "点检工单",
			inner: "内部工单",
			osc: "保养工单",
			root: "默认根节点",
		},
		uiactions: {
		},
	},
};