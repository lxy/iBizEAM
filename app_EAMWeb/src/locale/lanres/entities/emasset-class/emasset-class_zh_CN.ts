export default {
  fields: {
    assetclasscode: "资产科目代码",
    orgid: "组织",
    createdate: "建立时间",
    reserver: "RESERVER",
    updateman: "更新人",
    description: "描述",
    updatedate: "更新时间",
    reserver2: "RESERVER2",
    emassetclassname: "资产类别名称",
    assetclassgroup: "固定资产科目分类",
    createman: "建立人",
    emassetclassid: "资产类别标识",
    life: "折旧期",
    enable: "逻辑有效标志",
    assetclassinfo: "资产科目信息",
    assetclasspname: "上级科目",
    assetclasspcode: "上级科目代码",
    assetclasspid: "上级科目编号",
  },
	views: {
		pickupgridview: {
			caption: "资产类别",
      		title: "资产类别选择表格视图",
		},
		gridview: {
			caption: "资产类别",
      		title: "资产类别",
		},
		editview: {
			caption: "ASSET资产类别信息",
      		title: "ASSET资产类别信息",
		},
		editview_4778: {
			caption: "ASSET资产类别信息",
      		title: "ASSET资产类别信息",
		},
		pickupview: {
			caption: "资产类别",
      		title: "资产类别数据选择视图",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "ASSET资产类别信息", 
			grouppanel7: "附属信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "资产类别标识", 
			srfmajortext: "资产类别名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			assetclasscode: "资产科目代码", 
			emassetclassname: "资产类别名称", 
			assetclasspname: "上级科目", 
			assetclassgroup: "固定资产科目分类", 
			life: "折旧期", 
			emassetclassid: "资产类别标识", 
			assetclasspid: "上级科目编号", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			emassetclassname: "资产类别名称",
			assetclasscode: "资产科目代码",
			life: "折旧期",
			assetclasspname: "上级科目",
			assetclasspcode: "上级科目代码",
			assetclassgroup: "固定资产科目分类",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editview_4778toolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};