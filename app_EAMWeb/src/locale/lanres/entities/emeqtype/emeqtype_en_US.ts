
export default {
  fields: {
    eqtypegroup: "类型分组",
    createman: "建立人",
    eqstateinfo: "设备状态情况",
    updatedate: "更新时间",
    eqtypecode: "类型代码",
    stype: "统计归口分组",
    eqtypeinfo: "设备类型信息",
    arg: "辅助设施故障/维护",
    orgid: "组织",
    emeqtypeid: "设备类型标识",
    createdate: "建立时间",
    updateman: "更新人",
    sname: "统计归口名称",
    enable: "逻辑有效标志",
    emeqtypename: "设备类型名称",
    description: "描述",
    eqtypepcode: "上级设备类型代码",
    eqtypepname: "上级设备类型",
    eqtypepid: "上级设备类型",
  },
	views: {
		treeexpview2: {
			caption: "设备档案",
      		title: "设备类型",
		},
		editview: {
			caption: "设备类型",
      		title: "设备类型",
		},
		pickupgridview: {
			caption: "设备类型",
      		title: "设备类型选择表格视图",
		},
		gridview: {
			caption: "设备类型",
      		title: "设备类型",
		},
		optionview: {
			caption: "设备类型",
      		title: "设备类型",
		},
		pickupview: {
			caption: "设备类型",
      		title: "设备类型数据选择视图",
		},
		treeexpview: {
			caption: "设备档案",
      		title: "设备类型",
		},
		gridexpview: {
			caption: "设备类型",
      		title: "设备类型表格导航视图",
		},
		pickuptreeview: {
			caption: "设备类型",
      		title: "设备类型",
		},
		editview_editmode: {
			caption: "设备类型",
      		title: "设备类型",
		},
		treepickupview: {
			caption: "设备类型",
      		title: "设备类型数据选择视图",
		},
	},
	main3_form: {
		details: {
			grouppanel2: "设备类型信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "设备类型标识", 
			srfmajortext: "设备类型名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			eqtypecode: "类型代码", 
			emeqtypename: "设备类型名称", 
			eqtypepname: "上级设备类型", 
			eqtypegroup: "类型分组", 
			stype: "统计归口分组", 
			sname: "统计归口名称", 
			eqtypepid: "上级设备类型", 
			emeqtypeid: "设备类型标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			eqtypecode: "类型代码",
			emeqtypename: "设备类型名称",
			eqtypepname: "上级设备类型",
			eqtypegroup: "类型分组",
		},
		uiactions: {
		},
	},
	nav_grid: {
		columns: {
			eqtypecode: "类型代码",
			emeqtypename: "设备类型名称",
			eqtypepname: "上级设备类型",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
			n_eqtypecode_like: "类型代码(%)", 
			n_emeqtypename_like: "设备类型名称(%)", 
			n_eqtypepid_eq: "上级设备类型(=)", 
			n_eqtypegroup_eq: "类型分组(=)", 
		},
		uiactions: {
		},
	},
	editview_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "Save",
			tip: "Save",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem23: {
			caption: "第一个记录",
			tip: "第一个记录",
		},
		tbitem24: {
			caption: "上一个记录",
			tip: "上一个记录",
		},
		tbitem25: {
			caption: "下一个记录",
			tip: "下一个记录",
		},
		tbitem26: {
			caption: "最后一个记录",
			tip: "最后一个记录",
		},
		tbitem21: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
	eqtypetree_treeview: {
		nodes: {
			root: "根节点",
			equip: "全部设备",
		},
		uiactions: {
        emeqtype_addeqtype: "添加设备类型",
        emeqtype_editeqtype: "编辑",
        emeqtype_addequip: "添加设备",
		},
	},
	eqtype_treeview: {
		nodes: {
			root: "根节点",
			eqtype: "全部设备类型",
		},
		uiactions: {
        emeqtype_addeqtype: "添加设备类型",
        emeqtype_remove: "删除",
		},
	},
};