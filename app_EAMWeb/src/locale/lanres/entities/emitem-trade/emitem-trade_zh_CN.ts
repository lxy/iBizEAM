export default {
  fields: {
    sempname: "库管员",
    batcode: "批次",
    emitemtradename: "物品交易名称",
    civo: "发票号",
    price: "单价",
    createdate: "建立时间",
    createman: "建立人",
    aempname: "申请人",
    orgid: "组织",
    sdate: "出入日期",
    tradestate: "交易状态",
    pusetype: "领料分类",
    updatedate: "更新时间",
    aempid: "申请人",
    psum: "数量",
    amount: "总金额",
    inoutflag: "出入标志",
    deptname: "部门",
    updateman: "更新人",
    shf: "税费",
    deptid: "部门",
    emitemtradetype: "交易分组",
    sempid: "库管员",
    emitemtradeid: "物品交易标识",
    itemtypegroup: "物品类型分组",
    enable: "逻辑有效标志",
    description: "描述",
    itemmtypeid: "物品2级类",
    itembtypename: "物品大类",
    storename: "仓库",
    shfprice: "平均税费",
    itembtypeid: "物品大类",
    stockamount: "物品库存金额",
    rname: "关联单",
    itemcode: "物品代码",
    storepartname: "库位",
    itemname: "物品",
    teamname: "班组",
    itemmtypename: "物品二级类",
    labservicename: "供应商",
    itemtypeid: "物品类型",
    labserviceid: "供应商",
    teamid: "班组",
    rid: "关联单",
    itemid: "物品",
    storeid: "仓库",
    storepartid: "库位",
  },
	views: {
		gridview: {
			caption: "物品交易",
      		title: "物品交易表格视图",
		},
		treeexpview: {
			caption: "物品交易",
      		title: "物品交易树导航视图",
		},
	},
	main2_grid: {
		columns: {
			emitemtradeid: "物品交易标识",
			emitemtradetype: "交易分组",
			itemname: "物品",
			sdate: "出入日期",
			inoutflag: "出入标志",
			storename: "仓库",
			storepartname: "库位",
			psum: "数量",
			price: "单价",
			amount: "总金额",
			batcode: "批次",
			sempname: "库管员",
			rname: "关联单",
			tradestate: "交易状态",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem16: {
			caption: "其它",
			tip: "其它",
		},
		tbitem21: {
			caption: "导出数据模型",
			tip: "导出数据模型",
		},
	},
	tradetree_treeview: {
		nodes: {
			puse: "领料单",
			rout: "退货单",
			prtn: "还料单",
			rin: "入库单",
			trade: "全部记录",
			root: "默认根节点",
		},
		uiactions: {
		},
	},
};