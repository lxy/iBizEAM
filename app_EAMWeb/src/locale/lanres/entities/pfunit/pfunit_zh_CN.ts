export default {
  fields: {
    description: "描述",
    pfunitname: "计量单位名称",
    unitcode: "计量单位代码",
    unitinfo: "计量单位信息",
    orgid: "组织",
    createman: "建立人",
    createdate: "建立时间",
    pfunitid: "计量单位标识",
    updateman: "更新人",
    updatedate: "更新时间",
    enable: "逻辑有效标志",
  },
	views: {
		gridview: {
			caption: "计量单位",
      		title: "计量单位",
		},
		pickupgridview: {
			caption: "计量单位",
      		title: "计量单位选择表格视图",
		},
		pickupview: {
			caption: "计量单位",
      		title: "计量单位数据选择视图",
		},
		editview: {
			caption: "计量单位",
      		title: "计量单位",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "PF计量单位信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "计量单位标识", 
			srfmajortext: "计量单位名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			unitcode: "计量单位代码", 
			pfunitname: "计量单位名称", 
			description: "描述", 
			pfunitid: "计量单位标识", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			unitcode: "计量单位代码",
			pfunitname: "计量单位名称",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
};