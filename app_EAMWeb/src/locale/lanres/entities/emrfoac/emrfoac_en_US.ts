
export default {
  fields: {
    enable: "逻辑有效标志",
    objid: "对象编号",
    description: "描述",
    createman: "建立人",
    emrfoacname: "方案名称",
    rfoaccode: "方案代码",
    orgid: "组织",
    createdate: "建立时间",
    updatedate: "更新时间",
    updateman: "更新人",
    rfoacinfo: "信息",
    emrfoacid: "方案标识",
    rfodenane: "现象",
    rfomoname: "模式",
    rfodeid: "现象",
    rfomoid: "模式",
  },
	views: {
		pickupview: {
			caption: "方案",
      		title: "方案数据选择视图",
		},
		gridview: {
			caption: "方案",
      		title: "方案",
		},
		editview: {
			caption: "方案信息",
      		title: "方案信息",
		},
		pickupgridview: {
			caption: "方案",
      		title: "方案选择表格视图",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "方案信息", 
			grouppanel7: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "方案标识", 
			srfmajortext: "方案名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			rfoaccode: "方案代码", 
			emrfoacname: "方案名称", 
			rfodenane: "现象", 
			rfomoname: "模式", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			rfodeid: "现象", 
			emrfoacid: "方案标识", 
			rfomoid: "模式", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			rfoacinfo: "信息",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			rfoaccode: "方案代码",
			emrfoacname: "方案名称",
			rfodenane: "现象",
			rfomoname: "模式",
			description: "描述",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};