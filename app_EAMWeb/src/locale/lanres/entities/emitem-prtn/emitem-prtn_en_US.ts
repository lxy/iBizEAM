
export default {
  fields: {
    prtntype: "还料分类",
    sapreason: "sap传输错误原因",
    emitemprtnname: "还料单名称",
    sap: "sap传输状态",
    createman: "建立人",
    tradestate: "还料状态",
    itemprtninfo: "还料单信息",
    psum: "实还数",
    createdate: "建立时间",
    sempid: "收料人",
    sapreason1: "sap传输异常文本",
    wfinstanceid: "工作流实例",
    price: "单价",
    orgid: "组织",
    wfstep: "流程步骤",
    amount: "总金额",
    empname: "还料人",
    emitemprtnid: "还料单号",
    sapcontrol: "sap控制",
    description: "描述",
    wfstate: "工作流状态",
    updatedate: "更新时间",
    empid: "还料人",
    sempname: "收料人",
    updateman: "更新人",
    sdate: "还料日期",
    enable: "逻辑有效标志",
    batcode: "批次",
    deptid: "领料部门",
    storename: "仓库",
    unitname: "单位",
    itemname: "物品",
    unitid: "单位",
    rname: "领料单",
    pusetype: "领料分类",
    storepartname: "库位",
    avgprice: "物品均价",
    itemid: "物品",
    storepartid: "库位",
    storeid: "仓库",
    rid: "领料单",
  },
	views: {
		editview9_editmode: {
			caption: "还料单信息",
      		title: "还料单信息",
		},
		editview: {
			caption: "还料单",
      		title: "还料单编辑视图",
		},
		confirmedgridview: {
			caption: "还料单-已确认",
      		title: "还料单",
		},
		gridview: {
			caption: "还料单",
      		title: "还料单",
		},
		toconfirmgridview: {
			caption: "还料单-待确认",
      		title: "还料单",
		},
		editview9: {
			caption: "还料单信息",
      		title: "还料单信息",
		},
		tabexpview: {
			caption: "还料单",
      		title: "还料单分页导航视图",
		},
		draftgridview: {
			caption: "还料单-草稿",
      		title: "还料单",
		},
	},
	main3_form: {
		details: {
			grouppanel2: "还料单信息", 
			grouppanel8: "收料信息", 
			formpage1: "基本信息", 
			grouppanel17: "操作信息", 
			formpage16: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "还料单号", 
			srfmajortext: "还料单名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emitemprtnid: "还料单号(自动)", 
			rname: "领料单", 
			itemname: "物品", 
			empid: "还料人", 
			empname: "还料人", 
			sdate: "还料日期", 
			storename: "仓库", 
			storepartname: "库位", 
			psum: "实还数", 
			price: "单价", 
			amount: "总金额", 
			batcode: "批次", 
			sempid: "收料人", 
			sempname: "收料人", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			storepartid: "库位", 
			storeid: "仓库", 
			rid: "领料单", 
			itemid: "物品", 
		},
		uiactions: {
		},
	},
	main2_form: {
		details: {
			grouppanel2: "还料单信息", 
			grouppanel8: "收料信息", 
			formpage1: "基本信息", 
			grouppanel17: "操作信息", 
			formpage16: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "还料单号", 
			srfmajortext: "还料单名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emitemprtnid: "还料单号(自动)", 
			rname: "领料单", 
			itemname: "物品", 
			empid: "还料人", 
			empname: "还料人", 
			sdate: "还料日期", 
			storename: "仓库", 
			storepartname: "库位", 
			psum: "实还数", 
			price: "单价", 
			amount: "总金额", 
			batcode: "批次", 
			sempid: "收料人", 
			sempname: "收料人", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
		},
		uiactions: {
		},
	},
	main_form: {
		details: {
			group1: "还料单基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "还料单号", 
			srfmajortext: "还料单名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			itemprtninfo: "还料单信息", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emitemprtnid: "还料单号", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			emitemprtnid: "还料单号",
			itemname: "物品",
			psum: "实还数",
			unitname: "单位",
			price: "单价",
			amount: "总金额",
			sdate: "还料日期",
			storename: "仓库",
			storepartname: "库位",
			batcode: "批次",
			rname: "领料单",
			empname: "还料人",
			sempname: "收料人",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editview9_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	editview9toolbar_toolbar: {
		deuiaction1: {
			caption: "编辑",
			tip: "编辑",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem23: {
			caption: "第一个记录",
			tip: "第一个记录",
		},
		tbitem24: {
			caption: "上一个记录",
			tip: "上一个记录",
		},
		tbitem25: {
			caption: "下一个记录",
			tip: "下一个记录",
		},
		tbitem26: {
			caption: "最后一个记录",
			tip: "最后一个记录",
		},
		tbitem21: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
	confirmedgridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	draftgridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	toconfirmgridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
};