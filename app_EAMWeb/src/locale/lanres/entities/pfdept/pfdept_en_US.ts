
export default {
  fields: {
    deptcode: "部门代码",
    mgrempname: "主管",
    updatedate: "更新时间",
    orgid: "组织",
    createman: "建立人",
    deptinfo: "部门信息",
    deptfn: "职能",
    updateman: "更新人",
    maindeptcode: "主部门编码",
    enable: "逻辑有效标志",
    deptpid: "上级部门",
    pfdeptid: "部门标识",
    mgrempid: "主管",
    pfdeptname: "部门名称",
    sdept: "统计归口部门",
    createdate: "建立时间",
    description: "描述",
    deptpname: "上级部门",
  },
	views: {
		pickupgridview: {
			caption: "部门",
      		title: "部门选择表格视图",
		},
		editview: {
			caption: "部门",
      		title: "部门",
		},
		gridview: {
			caption: "部门",
      		title: "部门",
		},
		editview_editmode: {
			caption: "部门",
      		title: "部门",
		},
		pickupview: {
			caption: "部门",
      		title: "部门数据选择视图",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "部门信息", 
			grouppanel8: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "部门标识", 
			srfmajortext: "部门名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			deptcode: "部门代码", 
			pfdeptname: "部门名称", 
			deptpname: "上级部门", 
			sdept: "统计归口部门", 
			deptfn: "职能", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			pfdeptid: "部门标识", 
		},
		uiactions: {
		},
	},
	main3_form: {
		details: {
			grouppanel2: "部门信息", 
			grouppanel8: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "部门标识", 
			srfmajortext: "部门名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			deptcode: "部门代码", 
			pfdeptname: "部门名称", 
			deptpname: "上级部门", 
			sdept: "统计归口部门", 
			deptfn: "职能", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			pfdeptid: "部门标识", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			deptcode: "部门代码",
			pfdeptname: "部门名称",
			deptpname: "上级部门",
			sdept: "统计归口部门",
			orgid: "组织",
			deptfn: "职能",
			description: "描述",
			maindeptcode: "主部门编码",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "编辑",
			tip: "编辑",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editview_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	depttree_treeview: {
		nodes: {
			all: "全部职员",
			root: "默认根节点",
		},
		uiactions: {
		},
	},
};