export default {
  fields: {
    description: "描述",
    storecode: "仓库代码",
    createdate: "建立时间",
    storeinfo: "仓库信息",
    standpriceflag: "标准价标志",
    poweravgflag: "加权平均标志",
    createman: "建立人",
    updatedate: "更新时间",
    emstoreid: "仓库标识",
    newstoretypeid: "NEW仓库类型",
    costcenterid: "成本中心",
    mgrpersonid: "主管经理",
    orgid: "组织",
    ioalgo: "出入算法",
    empid: "库管员",
    storetypeid: "仓库类型",
    storeaddr: "地址",
    updateman: "更新人",
    empname: "库管员",
    emstorename: "仓库名称",
    enable: "逻辑有效标志",
    storetel: "联系电话",
    storefax: "传真",
  },
	views: {
		pickupgridview: {
			caption: "仓库",
      		title: "仓库选择表格视图",
		},
		optionview: {
			caption: "仓库",
      		title: "仓库选项操作视图",
		},
		editview: {
			caption: "仓库",
      		title: "仓库",
		},
		pickupview: {
			caption: "仓库",
      		title: "仓库数据选择视图",
		},
		editview_editmode: {
			caption: "仓库",
      		title: "仓库",
		},
		editview_9924: {
			caption: "仓库",
      		title: "仓库",
		},
		treeexpview: {
			caption: "仓库库位",
      		title: "仓库库位",
		},
		gridview_7848: {
			caption: "仓库",
      		title: "仓库",
		},
		gridview: {
			caption: "仓库",
      		title: "仓库",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "仓库信息", 
			grouppanel7: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "仓库标识", 
			srfmajortext: "仓库名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			storecode: "仓库代码", 
			emstorename: "仓库名称", 
			empid: "库管员", 
			empname: "库管员", 
			storetypeid: "仓库类型", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emstoreid: "仓库标识", 
		},
		uiactions: {
		},
	},
	main3_form: {
		details: {
			grouppanel2: "仓库信息", 
			grouppanel7: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "仓库标识", 
			srfmajortext: "仓库名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			storecode: "仓库代码", 
			emstorename: "仓库名称", 
			empid: "库管员", 
			empname: "库管员", 
			storetypeid: "仓库类型", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emstoreid: "仓库标识", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			storecode: "仓库代码",
			emstorename: "仓库名称",
			storetypeid: "仓库类型",
			empname: "库管员",
			description: "描述",
		},
		uiactions: {
		},
	},
	main2_1919_grid: {
		columns: {
			storecode: "仓库代码",
			emstorename: "仓库名称",
			storetypeid: "仓库类型",
			empname: "库管员",
			description: "描述",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "编辑",
			tip: "编辑",
		},
	},
	editview_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridview_7848toolbar_toolbar: {
		tbitem24: {
			caption: "行编辑",
			tip: "行编辑",
		},
		tbitem25: {
			caption: "新建行",
			tip: "新建行",
		},
		deuiaction1: {
			caption: "保存",
			tip: "保存",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem12: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editview_9924toolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	storetree_treeview: {
		nodes: {
			root: "默认根节点",
		},
		uiactions: {
			emstore_newstore: "添加仓库",
			emstore_editstore: "编辑",
			remove: "删除",
		},
	},
};