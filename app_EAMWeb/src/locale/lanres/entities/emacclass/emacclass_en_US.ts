
export default {
  fields: {
    emacclassid: "总帐科目标识",
    code: "代码",
    createdate: "建立时间",
    createman: "建立人",
    enable: "逻辑有效标志",
    description: "描述",
    emacclassname: "总帐科目名称",
    updateman: "更新人",
    updatedate: "更新时间",
    orgid: "组织",
  },
	views: {
		pickupgridview: {
			caption: "总帐科目",
      		title: "总帐科目选择表格视图",
		},
		pickupview: {
			caption: "总帐科目",
      		title: "总帐科目数据选择视图",
		},
	},
	main_grid: {
		columns: {
			emacclassname: "总帐科目名称",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};