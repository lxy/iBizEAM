
export default {
  fields: {
    emobjmapid: "对象关系标识",
    updatedate: "更新时间",
    updateman: "更新人",
    equipid: "设备编号",
    emobjmaptype: "对象关系类型",
    createdate: "建立时间",
    emobjmapname: "对象关系名称",
    createman: "建立人",
    description: "描述",
    enable: "逻辑有效标志",
    orgid: "组织",
    objptype: "上级对象类型",
    majorequipname: "主设备",
    majorequipid: "主设备",
    objpname: "上级对象",
    objtype: "对象类型",
    objname: "对象",
    objid: "对象",
    objpid: "上级对象",
  },
	views: {
		locationtreeview: {
			caption: "位置信息",
      		title: "位置信息",
		},
	},
	location_treeview: {
		nodes: {
			root: "默认根节点",
		},
		uiactions: {
		},
	},
};