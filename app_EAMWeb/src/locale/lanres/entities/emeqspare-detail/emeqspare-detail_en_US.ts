
export default {
  fields: {
    updatedate: "更新时间",
    enable: "逻辑有效标志",
    orgid: "组织",
    updateman: "更新人",
    createman: "建立人",
    emeqsparedetailid: "备件包明细标识",
    emeqsparedetailname: "备件包明细",
    createdate: "建立时间",
    description: "描述",
    itemname: "物品",
    eqsparename: "备件包",
    itemid: "物品",
    eqspareid: "备件包",
  },
	views: {
		editview: {
			caption: "备件包明细",
      		title: "备件包明细",
		},
		editview_editmode: {
			caption: "备件包明细",
      		title: "备件包明细",
		},
		gridview9: {
			caption: "备件物品",
      		title: "备件物品",
		},
		gridview: {
			caption: "备件物品",
      		title: "备件物品",
		},
		tabexpview: {
			caption: "备件包明细",
      		title: "备件包明细分页导航视图",
		},
	},
	main3_form: {
		details: {
			grouppanel2: "备件包明细信息", 
			grouppanel5: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "备件包明细标识", 
			srfmajortext: "备件包明细", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			eqsparename: "备件包", 
			itemname: "物品", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			itemid: "物品", 
			eqspareid: "备件包", 
			emeqsparedetailid: "备件包明细标识", 
		},
		uiactions: {
		},
	},
	main2_form: {
		details: {
			grouppanel2: "备件包明细信息", 
			grouppanel5: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "备件包明细标识", 
			srfmajortext: "备件包明细", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			eqsparename: "备件包", 
			itemname: "物品", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emeqsparedetailid: "备件包明细标识", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			itemname: "物品",
			eqsparename: "备件包",
			description: "描述",
		},
		uiactions: {
		},
	},
	searchform_searchform: {
		details: {
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridview9toolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editview_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	editviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "编辑",
			tip: "编辑",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
};