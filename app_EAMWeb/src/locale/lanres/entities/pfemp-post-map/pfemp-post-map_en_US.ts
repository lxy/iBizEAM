
export default {
  fields: {
    empname: "职员",
    createdate: "建立时间",
    updatedate: "更新时间",
    pfemppostmapid: "人事关系标识",
    deptid: "部门",
    createman: "建立人",
    description: "描述",
    deptname: "部门",
    empid: "职员",
    updateman: "更新人",
    orderflag: "排序值",
    pfemppostmapname: "人事关系名称",
    enable: "逻辑有效标志",
    orgid: "组织",
    teamname: "班组",
    postname: "岗位",
    teamid: "班组",
    postid: "岗位",
  },
};