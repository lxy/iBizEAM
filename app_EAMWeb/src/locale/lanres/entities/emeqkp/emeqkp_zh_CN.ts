export default {
  fields: {
    emeqkpid: "设备关键点标识",
    createdate: "建立时间",
    orgid: "组织",
    enable: "逻辑有效标志",
    kpscope: "关键点范围",
    kpdesc: "关键点备注",
    createman: "建立人",
    normalrefval: "正常参考值",
    kpinfo: "关键点信息",
    kptypeid: "关键点类型",
    updateman: "更新人",
    kpcode: "关键点代码",
    updatedate: "更新时间",
    description: "描述",
    emeqkpname: "关键点名称",
  },
	views: {
		kpeditview: {
			caption: "设备关键点",
      		title: "设备关键点编辑视图",
		},
		pickupgridview: {
			caption: "设备关键点",
      		title: "设备关键点选择表格视图",
		},
		pickupview: {
			caption: "设备关键点",
      		title: "设备关键点数据选择视图",
		},
		gridview: {
			caption: "设备关键点",
      		title: "设备关键点表格视图",
		},
	},
	main3_form: {
		details: {
			grouppanel2: "设备关键点信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "设备关键点标识", 
			srfmajortext: "关键点名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			kpcode: "关键点代码", 
			emeqkpname: "关键点名称", 
			kptypeid: "关键点类型", 
			normalrefval: "正常参考值", 
			kpscope: "关键点范围", 
			kpdesc: "关键点备注", 
			emeqkpid: "设备关键点标识", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			kpcode: "关键点代码",
			emeqkpname: "关键点名称",
			kptypeid: "关键点类型",
			kpscope: "关键点范围",
			normalrefval: "正常参考值",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	kpeditviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
};