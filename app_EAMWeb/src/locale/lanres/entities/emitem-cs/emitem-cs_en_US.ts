
export default {
  fields: {
    updatedate: "更新时间",
    itemroutinfo: "调整单信息",
    price: "单价",
    createdate: "建立时间",
    createman: "建立人",
    sdate: "调整日期",
    enable: "逻辑有效标志",
    wfstep: "流程步骤",
    emitemcsname: "调整单名称",
    emitemcsid: "调整单号",
    sempname: "调整人",
    batcode: "批次",
    wfinstanceid: "工作流实例",
    description: "描述",
    sempid: "调整人",
    amount: "总金额",
    tradestate: "调整状态",
    updateman: "更新人",
    wfstate: "工作流状态",
    psum: "数量",
    orgid: "组织",
    storepartname: "库位",
    rname: "入库单",
    storename: "仓库",
    itemname: "物品",
    stockname: "库存",
    storeid: "仓库",
    rid: "入库单",
    storepartid: "库位",
    stockid: "库存",
    itemid: "物品",
  },
	views: {
		editview9_editmode: {
			caption: "调整单",
      		title: "调整单",
		},
		toconfirmgridview: {
			caption: "调整单-待确认",
      		title: "调整单",
		},
		draftgridview: {
			caption: "调整单-草稿",
      		title: "调整单",
		},
		confirmedgridview: {
			caption: "调整单-已确认",
      		title: "调整单",
		},
		tabexpview: {
			caption: "库间调整单",
      		title: "库间调整单分页导航视图",
		},
		editview: {
			caption: "库间调整单",
      		title: "库间调整单编辑视图",
		},
		gridview: {
			caption: "调整单",
      		title: "调整单",
		},
		editview9: {
			caption: "调整单",
      		title: "调整单",
		},
	},
	main3_form: {
		details: {
			grouppanel2: "调整单信息", 
			formpage1: "基本信息", 
			grouppanel15: "操作信息", 
			formpage14: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "调整单号", 
			srfmajortext: "调整单名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emitemcsid: "调整单号(自动)", 
			itemname: "物品", 
			sdate: "调整日期", 
			sempid: "调整人", 
			sempname: "调整人", 
			stockname: "库存", 
			psum: "数量", 
			price: "单价", 
			amount: "总金额", 
			batcode: "批次", 
			storename: "仓库", 
			storepartname: "库位", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			storepartid: "库位", 
			storeid: "仓库", 
			stockid: "库存", 
			itemid: "物品", 
		},
		uiactions: {
		},
	},
	main2_form: {
		details: {
			grouppanel2: "调整单信息", 
			formpage1: "基本信息", 
			grouppanel15: "操作信息", 
			formpage14: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "调整单号", 
			srfmajortext: "调整单名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emitemcsid: "调整单号(自动)", 
			itemname: "物品", 
			sdate: "调整日期", 
			sempid: "调整人", 
			sempname: "调整人", 
			stockname: "库存", 
			psum: "数量", 
			price: "单价", 
			amount: "总金额", 
			batcode: "批次", 
			storename: "仓库", 
			storepartname: "库位", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
		},
		uiactions: {
		},
	},
	main_form: {
		details: {
			group1: "库间调整单基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "调整单号", 
			srfmajortext: "调整单名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			itemroutinfo: "调整单信息", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emitemcsid: "调整单号", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			emitemcsid: "调整单号",
			itemname: "物品",
			sdate: "调整日期",
			stockname: "库存",
			storename: "仓库",
			storepartname: "库位",
			psum: "数量",
			price: "单价",
			amount: "总金额",
			batcode: "批次",
			sempname: "调整人",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editview9_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	toconfirmgridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editview9toolbar_toolbar: {
		deuiaction1: {
			caption: "编辑",
			tip: "编辑",
		},
	},
	draftgridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	confirmedgridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem23: {
			caption: "第一个记录",
			tip: "第一个记录",
		},
		tbitem24: {
			caption: "上一个记录",
			tip: "上一个记录",
		},
		tbitem25: {
			caption: "下一个记录",
			tip: "下一个记录",
		},
		tbitem26: {
			caption: "最后一个记录",
			tip: "最后一个记录",
		},
		tbitem21: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
};