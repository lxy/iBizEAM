
export default {
  fields: {
    arg2: "ARG2",
    updatedate: "更新时间",
    storepartgl: "货架管理",
    arg1: "ARG1",
    enable: "逻辑有效标志",
    updateman: "更新人",
    arg0: "ARG0",
    emcabid: "货架标识",
    createdate: "建立时间",
    orgid: "组织",
    emcabname: "货架名称",
    createman: "建立人",
    graphparam: "GRAPHPARAM",
    emstorepartname: "库位",
    emstorename: "仓库",
    emstorepartid: "库位",
    emstoreid: "仓库",
  },
};