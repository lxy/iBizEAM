export default {
  fields: {
    createman: "建立人",
    orgid: "组织",
    description: "描述",
    rfodecode: "现象代码",
    objid: "对象编号",
    rfodeinfo: "现象信息",
    emrfodeid: "现象标识",
    createdate: "建立时间",
    emrfodename: "现象名称",
    updatedate: "更新时间",
    enable: "逻辑有效标志",
    updateman: "更新人",
  },
	views: {
		tabexpview: {
			caption: "现象",
      		title: "现象",
		},
		gridview: {
			caption: "现象",
      		title: "现象",
		},
		pickupgridview: {
			caption: "现象",
      		title: "现象选择表格视图",
		},
		editview: {
			caption: "现象信息",
      		title: "现象信息",
		},
		gridexpview: {
			caption: "现象",
      		title: "现象表格导航视图",
		},
		pickupview: {
			caption: "现象",
      		title: "现象数据选择视图",
		},
		dashboardview9: {
			caption: "现象",
      		title: "现象数据看板视图",
		},
		editview9: {
			caption: "现象信息",
      		title: "现象信息",
		},
		editview9_editmode: {
			caption: "现象信息",
      		title: "现象信息",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "现象信息", 
			grouppanel5: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "现象标识", 
			srfmajortext: "现象名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			rfodecode: "现象代码", 
			emrfodename: "现象名称", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emrfodeid: "现象标识", 
		},
		uiactions: {
		},
	},
	main3_form: {
		details: {
			grouppanel2: "现象信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "现象标识", 
			srfmajortext: "现象名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			rfodecode: "现象代码", 
			emrfodename: "现象名称", 
			emrfodeid: "现象标识", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			rfodeinfo: "现象信息",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			rfodecode: "现象代码",
			emrfodename: "现象名称",
			description: "描述",
		},
		uiactions: {
		},
	},
	main3_grid: {
		columns: {
			rfodecode: "现象代码",
			emrfodename: "现象名称",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editview9toolbar_toolbar: {
		deuiaction1: {
			caption: "编辑",
			tip: "编辑",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};