export default {
  fields: {
    description: "描述",
    enable: "逻辑有效标志",
    createdate: "建立时间",
    orgid: "组织",
    updatedate: "更新时间",
    emwooriname: "工单来源名称",
    wooriinfo: "工单来源信息",
    emwooriid: "工单来源标识",
    updateman: "更新人",
    emwooritype: "来源类型",
    createman: "建立人",
  },
	views: {
		pickupview: {
			caption: "工单来源",
      		title: "工单来源数据选择视图",
		},
		pickupgridview: {
			caption: "工单来源",
      		title: "工单来源选择表格视图",
		},
	},
	main_grid: {
		columns: {
			wooriinfo: "工单来源信息",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};