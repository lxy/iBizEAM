
export default {
  fields: {
    emplancdtid: "计划条件标识",
    createman: "建立人",
    createdate: "建立时间",
    enable: "逻辑有效标志",
    triggerval: "临界值",
    dpvaltype: "测点值类型",
    lastval: "上次触发值",
    nowval: "预警值",
    description: "描述",
    orgid: "组织",
    updatedate: "更新时间",
    updateman: "更新人",
    triggerdp: "触发操作",
    emplancdtname: "计划条件名称",
    dptype: "测点类型",
    objname: "位置",
    dpname: "测点",
    planname: "计划",
    equipname: "设备",
    dpid: "测点",
    planid: "计划",
    equipid: "设备",
    objid: "位置",
  },
	views: {
		gridview: {
			caption: "计划条件",
      		title: "计划条件",
		},
		editview: {
			caption: "计划条件",
      		title: "计划条件",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "计划条件信息", 
			grouppanel13: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "计划条件标识", 
			srfmajortext: "计划条件名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			equipname: "设备", 
			objname: "位置", 
			dpname: "测点", 
			dptype: "测点类型", 
			dpvaltype: "测点值类型", 
			triggerdp: "触发操作", 
			triggerval: "临界值", 
			nowval: "预警值", 
			lastval: "上次触发值", 
			planname: "计划", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			planid: "计划", 
			objid: "位置", 
			emplancdtid: "计划条件标识", 
			dpid: "测点", 
			equipid: "设备", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			equipname: "设备",
			objname: "位置",
			planname: "计划",
			dpname: "测点",
			dptype: "测点类型",
			dpvaltype: "测点值类型",
			triggerdp: "触发操作",
			triggerval: "临界值",
			nowval: "预警值",
			lastval: "上次触发值",
			description: "描述",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};