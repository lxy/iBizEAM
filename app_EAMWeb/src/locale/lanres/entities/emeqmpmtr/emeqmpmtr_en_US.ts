
export default {
  fields: {
    createdate: "建立时间",
    emeqmpmtrid: "设备仪表读数标识",
    emeqmpmtrname: "设备仪表读数名称",
    updatedate: "更新时间",
    enable: "逻辑有效标志",
    updateman: "更新人",
    edate: "采集时间",
    orgid: "组织",
    createman: "建立人",
    description: "描述",
    nval: "数值",
    bdate: "上次采集时间",
    val: "值",
    objname: "位置",
    woname: "工单",
    mpname: "仪表",
    equipname: "设备",
    woid: "工单",
    equipid: "设备",
    mpid: "仪表",
    objid: "位置",
  },
	views: {
		mpmtreditview: {
			caption: "设备仪表读数",
      		title: "设备仪表读数编辑视图",
		},
		gridview: {
			caption: "设备仪表读数",
      		title: "设备仪表读数表格视图",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "设备仪表读数信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "设备仪表读数标识", 
			srfmajortext: "设备仪表读数名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			mpname: "仪表", 
			woname: "工单", 
			equipname: "设备", 
			objname: "位置", 
			edate: "采集时间", 
			val: "值", 
			orgid: "组织", 
			description: "描述", 
			emeqmpmtrid: "设备仪表读数标识", 
			objid: "位置", 
			woid: "工单", 
			equipid: "设备", 
			mpid: "仪表", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			equipname: "设备",
			objname: "位置",
			mpname: "仪表",
			woname: "工单",
			edate: "采集时间",
			val: "值",
			description: "描述",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	mpmtreditviewtoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
};