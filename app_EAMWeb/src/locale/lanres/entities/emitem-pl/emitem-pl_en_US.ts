
export default {
  fields: {
    sdate: "损溢日期",
    price: "单价",
    wfstep: "流程步骤",
    enable: "逻辑有效标志",
    orgid: "组织",
    sempid: "损溢人",
    amount: "总金额",
    updateman: "更新人",
    batcode: "批次",
    wfstate: "工作流状态",
    psum: "数量",
    tradestate: "损溢状态",
    sap: "sap传输状态",
    wfinstanceid: "工作流实例",
    emitemplid: "损溢单号",
    itemroutinfo: "损溢单信息",
    emitemplname: "损溢单名称",
    createdate: "建立时间",
    sapreason1: "sap传输失败原因",
    inoutflag: "损益出入标志",
    description: "描述",
    updatedate: "更新时间",
    sempname: "损溢人",
    sapcontrol: "sap控制",
    createman: "建立人",
    storepartname: "库位",
    itemname: "物品",
    storename: "仓库",
    rname: "入库单",
    rid: "入库单",
    storeid: "仓库",
    itemid: "物品",
    storepartid: "库位",
  },
	views: {
		tabexpview: {
			caption: "损溢单",
      		title: "损溢单分页导航视图",
		},
		toconfirmgridview: {
			caption: "损溢单-待确认",
      		title: "损溢单",
		},
		editview: {
			caption: "损溢单",
      		title: "损溢单编辑视图",
		},
		gridview: {
			caption: "损溢单",
      		title: "损溢单",
		},
		editview9: {
			caption: "损溢单",
      		title: "损溢单",
		},
		draftgridview: {
			caption: "损溢单-草稿",
      		title: "损溢单",
		},
		editview9_editmode: {
			caption: "损溢单",
      		title: "损溢单",
		},
		confirmedgridview: {
			caption: "损溢单-已确认",
      		title: "损溢单",
		},
	},
	main3_form: {
		details: {
			grouppanel2: "损溢出单信息", 
			formpage1: "基本信息", 
			grouppanel15: "操作信息", 
			formpage14: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "损溢单号", 
			srfmajortext: "损溢单名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emitemplid: "损溢出单号(自动)", 
			inoutflag: "损益出入标志", 
			itemname: "物品", 
			sdate: "损溢日期", 
			storename: "仓库", 
			storepartname: "库位", 
			psum: "数量", 
			price: "单价", 
			amount: "总金额", 
			batcode: "批次", 
			sempid: "损溢人", 
			sempname: "损溢人", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			storepartid: "库位", 
			storeid: "仓库", 
			itemid: "物品", 
		},
		uiactions: {
		},
	},
	main2_form: {
		details: {
			grouppanel2: "损溢出单信息", 
			formpage1: "基本信息", 
			grouppanel15: "操作信息", 
			formpage14: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "损溢单号", 
			srfmajortext: "损溢单名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emitemplid: "损溢出单号(自动)", 
			inoutflag: "损益出入标志", 
			itemname: "物品", 
			sdate: "损溢日期", 
			storename: "仓库", 
			storepartname: "库位", 
			psum: "数量", 
			price: "单价", 
			amount: "总金额", 
			batcode: "批次", 
			sempid: "损溢人", 
			sempname: "损溢人", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
		},
		uiactions: {
		},
	},
	main_form: {
		details: {
			group1: "损溢单基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "损溢单号", 
			srfmajortext: "损溢单名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			itemroutinfo: "损溢单信息", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emitemplid: "损溢单号", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			emitemplid: "损溢单号",
			inoutflag: "损益出入标志",
			itemname: "物品",
			sdate: "损溢日期",
			storename: "仓库",
			storepartname: "库位",
			psum: "数量",
			price: "单价",
			amount: "总金额",
			batcode: "批次",
			sempname: "损溢人",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editview9_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	editview9toolbar_toolbar: {
		deuiaction1: {
			caption: "编辑",
			tip: "编辑",
		},
	},
	draftgridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	toconfirmgridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	confirmedgridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem23: {
			caption: "第一个记录",
			tip: "第一个记录",
		},
		tbitem24: {
			caption: "上一个记录",
			tip: "上一个记录",
		},
		tbitem25: {
			caption: "下一个记录",
			tip: "下一个记录",
		},
		tbitem26: {
			caption: "最后一个记录",
			tip: "最后一个记录",
		},
		tbitem21: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
};