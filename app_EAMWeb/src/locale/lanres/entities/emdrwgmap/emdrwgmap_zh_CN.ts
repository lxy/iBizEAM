export default {
  fields: {
    updateman: "更新人",
    createdate: "建立时间",
    emdrwgmapname: "文档引用",
    description: "描述",
    updatedate: "更新时间",
    emdrwgmapid: "文档引用标识",
    enable: "逻辑有效标志",
    orgid: "组织",
    createman: "建立人",
    refobjname: "引用对象",
    drwgname: "文档",
    refobjid: "引用对象",
    drwgid: "文档",
  },
	views: {
		editview: {
			caption: "文档引用",
      		title: "文档引用编辑视图",
		},
		equipgridview9: {
			caption: "文档引用",
      		title: "文档引用表格视图",
		},
	},
	main_form: {
		details: {
			group1: "文档引用基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "文档引用标识", 
			srfmajortext: "文档引用", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emdrwgmapname: "文档引用", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emdrwgmapid: "文档引用标识", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			refobjname: "引用对象",
			drwgname: "文档",
			description: "描述",
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem23: {
			caption: "第一个记录",
			tip: "第一个记录",
		},
		tbitem24: {
			caption: "上一个记录",
			tip: "上一个记录",
		},
		tbitem25: {
			caption: "下一个记录",
			tip: "下一个记录",
		},
		tbitem26: {
			caption: "最后一个记录",
			tip: "最后一个记录",
		},
		tbitem21: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "帮助",
			tip: "帮助",
		},
	},
	equipgridview9toolbar_toolbar: {
	},
};