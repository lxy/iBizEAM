
export default {
  fields: {
    updateman: "更新人",
    bdate: "运行区间起始",
    nval: "运行时间(H)",
    orgid: "组织",
    description: "描述",
    edate: "运行区间截至",
    emeqwlname: "设备运行日志名称",
    createdate: "建立时间",
    enable: "逻辑有效标志",
    updatedate: "更新时间",
    createman: "建立人",
    emeqwlid: "设备运行日志标识",
    woname: "工单",
    equipname: "设备",
    objname: "位置",
    equipid: "设备",
    woid: "工单",
    objid: "位置",
  },
	views: {
		eqgridview: {
			caption: "设备运行日志",
      		title: "设备运行日志表格视图",
		},
		wleditview: {
			caption: "设备运行日志",
      		title: "设备运行日志编辑视图",
		},
		gridview: {
			caption: "设备运行日志",
      		title: "设备运行日志表格视图",
		},
	},
	main3_form: {
		details: {
			grouppanel2: "设备运行日志信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "设备运行日志标识", 
			srfmajortext: "设备运行日志名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			equipname: "设备", 
			objname: "位置", 
			bdate: "运行区间起始", 
			edate: "运行区间截至", 
			nval: "运行时间(H)", 
			woname: "工单", 
			objid: "位置", 
			emeqwlid: "设备运行日志标识", 
			woid: "工单", 
			equipid: "设备", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			equipname: "设备",
			objname: "位置",
			nval: "运行时间(H)",
			bdate: "运行区间起始",
			edate: "运行区间截至",
			woname: "工单",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	wleditviewtoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	eqgridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
};