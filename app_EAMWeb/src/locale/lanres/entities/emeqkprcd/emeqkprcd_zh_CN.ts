export default {
  fields: {
    edate: "采集时间",
    bdate: "上次采集时间",
    orgid: "组织",
    emeqkprcdname: "关键点记录名称",
    updateman: "更新人",
    createdate: "建立时间",
    nval: "数值",
    updatedate: "更新时间",
    createman: "建立人",
    enable: "逻辑有效标志",
    val: "值",
    description: "描述",
    emeqkprcdid: "关键点记录标识",
    objname: "位置",
    kpname: "关键点",
    woname: "工单",
    equipname: "设备",
    objid: "位置",
    equipid: "设备",
    woid: "工单",
    kpid: "关键点",
  },
	views: {
		kprcdeditview: {
			caption: "关键点记录",
      		title: "关键点记录编辑视图",
		},
		gridview: {
			caption: "关键点记录",
      		title: "关键点记录表格视图",
		},
	},
	main3_form: {
		details: {
			grouppanel2: "关键点记录信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "关键点记录标识", 
			srfmajortext: "关键点记录名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			kpname: "关键点", 
			woname: "工单", 
			equipname: "设备", 
			objname: "位置", 
			edate: "采集时间", 
			val: "值", 
			objid: "位置", 
			woid: "工单", 
			equipid: "设备", 
			emeqkprcdid: "关键点记录标识", 
			kpid: "关键点", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			equipname: "设备",
			objname: "位置",
			kpname: "关键点",
			woname: "工单",
			edate: "采集时间",
			val: "值",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	kprcdeditviewtoolbar_toolbar: {
		tbitem1: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
	},
};