
export default {
  fields: {
    yl: "油量",
    eqmodelcode: "型号",
    rhytype: "润滑油类型",
    updatedate: "更新时间",
    enable: "逻辑有效标志",
    updateman: "更新人",
    orgid: "组织",
    description: "描述",
    createdate: "建立时间",
    createman: "建立人",
    eqlocationinfo: "润滑油信息",
    equipname: "设备",
    emeqlocationid: "位置标识",
    equipid: "设备",
  },
	views: {
		gridview: {
			caption: "润滑油位置",
      		title: "润滑油位置",
		},
		editview: {
			caption: "润滑油位置",
      		title: "润滑油位置",
		},
	},
	main2_form: {
		details: {
			grouppanel2: "钢丝绳位置信息", 
			grouppanel8: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "位置标识", 
			srfmajortext: "润滑油信息", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emeqlocationid: "位置标识", 
			equipname: "设备", 
			rhytype: "润滑油类型", 
			eqmodelcode: "型号", 
			yl: "油量", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			equipid: "设备", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			eqlocationinfo: "润滑油信息",
			equipname: "设备",
			rhytype: "润滑油类型",
			eqmodelcode: "型号",
			yl: "油量",
			description: "描述",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
};