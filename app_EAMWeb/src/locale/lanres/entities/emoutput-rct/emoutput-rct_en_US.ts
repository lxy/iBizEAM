
export default {
  fields: {
    updateman: "更新人",
    description: "描述",
    enable: "逻辑有效标志",
    orgid: "组织",
    edate: "产能区间起始",
    updatedate: "更新时间",
    bdate: "产能区间截至",
    emoutputrctname: "产能名称",
    createdate: "建立时间",
    createman: "建立人",
    nval: "产能值",
    emoutputrctid: "产能标识",
    equipname: "设备",
    outputname: "能力",
    objname: "位置",
    woname: "工单",
    woid: "工单",
    outputid: "能力",
    equipid: "设备",
    objid: "位置",
  },
	views: {
		editview: {
			caption: "产能信息",
      		title: "产能信息",
		},
		editview9: {
			caption: "产能信息",
      		title: "产能信息",
		},
		gridview: {
			caption: "产能",
      		title: "产能",
		},
	},
	main3_form: {
		details: {
			grouppanel2: "产能信息", 
			grouppanel10: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "产能标识", 
			srfmajortext: "产能名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			outputname: "能力", 
			woname: "工单", 
			equipname: "设备", 
			objname: "位置", 
			edate: "产能区间起始", 
			bdate: "产能区间截至", 
			nval: "产能值", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			outputid: "能力", 
			objid: "位置", 
			emoutputrctid: "产能标识", 
			woid: "工单", 
			equipid: "设备", 
		},
		uiactions: {
		},
	},
	main2_form: {
		details: {
			grouppanel2: "产能信息", 
			grouppanel10: "操作信息", 
			formpage1: "基本信息", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "产能标识", 
			srfmajortext: "产能名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			outputname: "能力", 
			woname: "工单", 
			equipname: "设备", 
			objname: "位置", 
			edate: "产能区间起始", 
			bdate: "产能区间截至", 
			nval: "产能值", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emoutputrctid: "产能标识", 
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			equipname: "设备",
			objname: "位置",
			outputname: "能力",
			woname: "工单",
			nval: "产能值",
			edate: "产能区间起始",
			bdate: "产能区间截至",
			description: "描述",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	editview9toolbar_toolbar: {
		deuiaction1: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
};