
export default {
  fields: {
    aempname: "申请人",
    deptid: "申请部门",
    apprempid: "批准人",
    apprdate: "批准日期",
    adate: "申请日期",
    wplistids: "采购申请",
    m3q: "经理指定询价数",
    aempid: "申请人",
    need3q: "需要3次询价",
    emwplistname: "采购申请名称",
    description: "描述",
    hdate: "希望到货日期",
    deptname: "申请部门",
    equips: "设备集合",
    qcnt: "询价记录数",
    createdate: "建立时间",
    orgid: "组织",
    rempid: "采购员",
    itemdesc: "物品备注",
    createman: "建立人",
    emwplistid: "采购申请号",
    enable: "逻辑有效标志",
    rempname: "采购员",
    wplisttype: "请购类型",
    itemanditemdesc: "物品和备注",
    costamount: "询价总金额",
    useto: "用途",
    apprempname: "批准人",
    wfstate: "工作流状态",
    iscancel: "是否为取消标志",
    wfinstanceid: "工作流实例",
    lastdate: "上次请购时间",
    apprdesc: "审核意见",
    wpliststate: "请购状态",
    wfstep: "流程步骤",
    wplistdp: "处理结果",
    asum: "请购数",
    wplistinfo: "采购申请信息",
    updateman: "更新人",
    updatedate: "更新时间",
    pamount: "预计总金额",
    deltype: "删除标识",
    labservicename: "产品供应商",
    no3q: "不足3家供应商",
    itemcode: "物品代码",
    wplistcostname: "询价结果",
    unitname: "单位",
    itemname: "物品",
    emservicename: "服务商",
    itembtypeid: "物品大类",
    itemname_show: "物品",
    avgprice: "物品均价",
    equipname: "设备",
    teamname: "申请班组",
    objname: "位置",
    labserviceid: "产品供应商",
    unitid: "单位",
    teamid: "申请班组",
    objid: "位置",
    emserviceid: "服务商",
    wplistcostid: "询价结果",
    equipid: "设备",
    itemid: "物品",
  },
	views: {
		treeexpview: {
			caption: "采购申请",
      		title: "采购申请树导航视图",
		},
		editview: {
			caption: "采购申请",
      		title: "采购申请编辑视图",
		},
		gridview_ydh: {
			caption: "采购申请",
      		title: "采购申请",
		},
		poeditview9: {
			caption: "采购申请",
      		title: "采购申请",
		},
		editview9: {
			caption: "采购申请",
      		title: "采购申请",
		},
		pickupgridview: {
			caption: "采购申请",
      		title: "采购申请选择表格视图",
		},
		ingridview: {
			caption: "采购申请",
      		title: "采购申请",
		},
		gridview: {
			caption: "采购申请",
      		title: "采购申请",
		},
		editview9_editmode: {
			caption: "采购申请",
      		title: "采购申请",
		},
		confirmcosteditview9: {
			caption: "采购申请",
      		title: "采购申请",
		},
		confimcostgridview: {
			caption: "采购申请",
      		title: "采购申请",
		},
		pickupview: {
			caption: "采购申请",
      		title: "采购申请数据选择视图",
		},
		draftgridview: {
			caption: "采购申请",
      		title: "采购申请",
		},
		wpprocesstreeexpview: {
			caption: "采购申请",
      		title: "采购申请树导航视图",
		},
		waitpogridview: {
			caption: "采购申请",
      		title: "采购申请",
		},
		podgridview: {
			caption: "采购申请",
      		title: "采购申请",
		},
		cancelgridview: {
			caption: "采购申请",
      		title: "采购申请",
		},
		waitcostgridview: {
			caption: "采购申请",
      		title: "采购申请",
		},
		fillcosteditview9: {
			caption: "采购申请",
      		title: "采购申请",
		},
	},
	main7_form: {
		details: {
			grouppanel1: "分组面板", 
			grouppanel2: "申请信息", 
			grouppanel18: "采购内容", 
			formpage1: "基本信息", 
			grouppanel27: "操作信息", 
			formpage26: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "采购申请号", 
			srfmajortext: "采购申请名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emwplistid: "采购申请号(自动)", 
			wplisttype: "请购类型", 
			adate: "申请日期", 
			useto: "用途", 
			equipname: "设备", 
			objname: "位置", 
			equips: "设备集合", 
			aempname: "申请人", 
			aempid: "申请人", 
			deptname: "申请部门", 
			teamname: "申请班组", 
			lastdate: "上次请购时间", 
			hdate: "希望到货日期", 
			need3q: "需要3次询价", 
			wpliststate: "请购状态", 
			itemname: "物品", 
			rempid: "采购员", 
			rempname: "采购员", 
			asum: "请购数", 
			itemdesc: "物品备注", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			teamid: "申请班组", 
			objid: "位置", 
			itemid: "物品", 
			equipid: "设备", 
		},
		uiactions: {
		},
	},
	main8_form: {
		details: {
			grouppanel1: "分组面板", 
			grouppanel2: "申请信息", 
			grouppanel18: "采购内容", 
			druipart1: "询价单", 
			grouppanel3: "询价单", 
			formpage1: "基本信息", 
			grouppanel27: "操作信息", 
			formpage26: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "采购申请号", 
			srfmajortext: "采购申请名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emwplistid: "采购申请号(自动)", 
			wplisttype: "请购类型", 
			adate: "申请日期", 
			useto: "用途", 
			equipname: "设备", 
			objname: "位置", 
			equips: "设备集合", 
			aempid: "申请人", 
			aempname: "申请人", 
			deptname: "申请部门", 
			teamname: "申请班组", 
			hdate: "希望到货日期", 
			wpliststate: "请购状态", 
			need3q: "需要3次询价", 
			lastdate: "上次请购时间", 
			itemname: "物品", 
			rempid: "采购员", 
			rempname: "采购员", 
			asum: "请购数", 
			itemdesc: "物品备注", 
			apprempid: "批准人", 
			apprempname: "批准人", 
			apprdate: "批准日期", 
			wplistcostname: "询价结果", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
		},
		uiactions: {
		},
	},
	main_form: {
		details: {
			group1: "采购申请基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "采购申请号", 
			srfmajortext: "采购申请名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			wplistinfo: "采购申请信息", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
			emwplistid: "采购申请号", 
		},
		uiactions: {
		},
	},
	main11_form: {
		details: {
			grouppanel1: "分组面板", 
			grouppanel2: "申请信息", 
			grouppanel18: "采购内容", 
			druipart1: "询价单", 
			grouppanel3: "订单条目", 
			formpage1: "基本信息", 
			grouppanel27: "操作信息", 
			formpage26: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "采购申请号", 
			srfmajortext: "采购申请名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emwplistid: "采购申请号(自动)", 
			wplisttype: "请购类型", 
			adate: "申请日期", 
			useto: "用途", 
			equipname: "设备", 
			objname: "位置", 
			equips: "设备集合", 
			aempid: "申请人", 
			aempname: "申请人", 
			deptname: "申请部门", 
			teamname: "申请班组", 
			hdate: "希望到货日期", 
			wpliststate: "请购状态", 
			need3q: "需要3次询价", 
			lastdate: "上次请购时间", 
			itemname: "物品", 
			rempid: "采购员", 
			rempname: "采购员", 
			asum: "请购数", 
			itemdesc: "物品备注", 
			apprempid: "批准人", 
			apprempname: "批准人", 
			apprdate: "批准日期", 
			wplistcostname: "询价结果", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
		},
		uiactions: {
		},
	},
	main10_form: {
		details: {
			grouppanel1: "分组面板", 
			grouppanel2: "申请信息", 
			grouppanel18: "采购内容", 
			druipart1: "询价单", 
			grouppanel3: "询价单", 
			formpage1: "基本信息", 
			grouppanel27: "操作信息", 
			formpage26: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "采购申请号", 
			srfmajortext: "采购申请名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emwplistid: "采购申请号(自动)", 
			wplisttype: "请购类型", 
			adate: "申请日期", 
			useto: "用途", 
			equipname: "设备", 
			objname: "位置", 
			equips: "设备集合", 
			aempid: "申请人", 
			aempname: "申请人", 
			deptname: "申请部门", 
			teamname: "申请班组", 
			hdate: "希望到货日期", 
			wpliststate: "请购状态", 
			need3q: "需要3次询价", 
			lastdate: "上次请购时间", 
			itemname: "物品", 
			rempid: "采购员", 
			rempname: "采购员", 
			asum: "请购数", 
			itemdesc: "物品备注", 
			apprempid: "批准人", 
			apprempname: "批准人", 
			apprdate: "批准日期", 
			wplistcostname: "询价结果", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
		},
		uiactions: {
		},
	},
	main9_form: {
		details: {
			grouppanel1: "分组面板", 
			grouppanel2: "申请信息", 
			grouppanel18: "采购内容", 
			druipart1: "询价单", 
			grouppanel3: "询价单", 
			formpage1: "基本信息", 
			grouppanel27: "操作信息", 
			formpage26: "其它", 
			srfupdatedate: "更新时间", 
			srforikey: "", 
			srfkey: "采购申请号", 
			srfmajortext: "采购申请名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			emwplistid: "采购申请号(自动)", 
			wplisttype: "请购类型", 
			adate: "申请日期", 
			useto: "用途", 
			equipname: "设备", 
			objname: "位置", 
			equips: "设备集合", 
			aempid: "申请人", 
			aempname: "申请人", 
			deptname: "申请部门", 
			teamname: "申请班组", 
			hdate: "希望到货日期", 
			wpliststate: "请购状态", 
			need3q: "需要3次询价", 
			lastdate: "上次请购时间", 
			itemname: "物品", 
			rempid: "采购员", 
			rempname: "采购员", 
			asum: "请购数", 
			itemdesc: "物品备注", 
			apprempid: "批准人", 
			apprempname: "批准人", 
			apprdate: "批准日期", 
			wplistcostname: "询价结果", 
			orgid: "组织", 
			description: "描述", 
			createman: "建立人", 
			createdate: "建立时间", 
			updateman: "更新人", 
			updatedate: "更新时间", 
		},
		uiactions: {
        emwplist_fillcost: "询价填报",
		},
	},
	main_grid: {
		columns: {
			wplistinfo: "采购申请信息",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			emwplistid: "采购申请号",
			itemname: "物品",
			itemcode: "物品代码",
			asum: "请购数",
			wplistdp: "处理结果",
			itemdesc: "物品备注",
			wfstep: "流程步骤",
			aempname: "申请人",
			adate: "申请日期",
			lastdate: "上次请购时间",
			unitname: "单位",
			wpliststate: "请购状态",
			objname: "位置",
			rempname: "采购员",
			wplistcostname: "询价结果",
			useto: "用途",
			equipname: "设备",
			deptname: "申请部门",
			teamname: "申请班组",
			apprempname: "批准人",
			apprdate: "批准日期",
			hdate: "希望到货日期",
			wplisttype: "请购类型",
			itemid: "物品",
			qcnt: "询价记录数",
			need3q: "需要3次询价",
		},
		uiactions: {
		},
	},
	main3_grid: {
		columns: {
			emwplistid: "采购申请号",
			itemname: "物品",
			asum: "请购数",
			wplistdp: "处理结果",
			itemdesc: "物品备注",
			wfstep: "流程步骤",
			aempname: "申请人",
			adate: "申请日期",
			lastdate: "上次请购时间",
			unitname: "单位",
			wpliststate: "请购状态",
			rempname: "采购员",
			wplistcostname: "询价结果",
			useto: "用途",
			equipname: "设备",
			objname: "位置",
			deptname: "申请部门",
			teamname: "申请班组",
			apprempname: "批准人",
			apprdate: "批准日期",
			hdate: "希望到货日期",
			wplisttype: "请购类型",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
			n_adate_gtandeq: "申请日期(>=)", 
			n_adate_ltandeq: "申请日期(<=)", 
			n_emservicename_eq: "服务商(等于(=))", 
			n_itemname_like: "物品(文本包含(%))", 
			n_deptname_like: "申请部门(%)", 
			n_emserviceid_eq: "", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	editview9_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "Save",
			tip: "Save",
		},
		deuiaction1_submit: {
			caption: "提交",
			tip: "提交",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	editview9toolbar_toolbar: {
		deuiaction1: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem23: {
			caption: "第一个记录",
			tip: "第一个记录",
		},
		tbitem24: {
			caption: "上一个记录",
			tip: "上一个记录",
		},
		tbitem25: {
			caption: "下一个记录",
			tip: "下一个记录",
		},
		tbitem26: {
			caption: "最后一个记录",
			tip: "最后一个记录",
		},
		tbitem21: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
	gridview_ydhtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	ingridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		deuiaction2: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	confirmcosteditview9toolbar_toolbar: {
		deuiaction1: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	confimcostgridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		deuiaction2: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	draftgridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	podgridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		deuiaction2: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	cancelgridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		deuiaction2: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	waitpogridviewtoolbar_toolbar: {
		tbitem1_genpo: {
			caption: "生产订单",
			tip: "生产订单",
		},
		deuiaction1: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		deuiaction2: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	waitcostgridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		deuiaction2: {
			caption: "Filter",
			tip: "Filter",
		},
	},
	fillcosteditview9toolbar_toolbar: {
		deuiaction1_fillcosted: {
			caption: "询价填报完成",
			tip: "询价填报完成",
		},
	},
	wptree_treeview: {
		nodes: {
			root: "默认根节点",
			podetail: "采购订单",
			wplist: "采购申请",
			cost: "询价单",
		},
		uiactions: {
		},
	},
	wpprocesstree_treeview: {
		nodes: {
			waitin: "待入库",
			in: "已入库",
			allrin: "全部入库单",
			waitbook: "待记账",
			allwplist: "全部采购申请",
			root: "默认根节点",
			pod: "已生成订单",
			allpo: "全部订单",
			closeddetail: "已关闭",
			cancel: "已取消",
			waitcheck: "待验收",
			closedorder: "已关闭",
			placeorder: "等待下单",
			onorder: "在途",
			putin: "已入库",
			confimcost: "待确认询价",
			draft: "草稿",
			waitpo: "待生成订单",
			waitcost: "待询价",
			alldetail: "全部订单条目",
		},
		uiactions: {
		},
	},
};