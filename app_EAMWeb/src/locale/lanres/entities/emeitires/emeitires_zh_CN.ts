export default {
  fields: {
    empid: "负责人",
    eicaminfo: "轮胎信息",
    createman: "建立人",
    description: "描述",
    macaddr: "地址",
    createdate: "建立时间",
    emeitiresname: "编码/出场号",
    eistate: "状态",
    updatedate: "更新时间",
    disdesc: "报废原因",
    empname: "负责人",
    updateman: "更新人",
    emeitiresid: "轮胎编号",
    disdate: "报废日期",
    orgid: "组织",
    eqmodelcode: "型号",
    enable: "逻辑有效标志",
    equipname: "设备",
    itempusename: "领料单",
    eqlocationname: "位置",
    eqlocationid: "位置",
    equipid: "设备",
    itempuseid: "领料单",
  },
	views: {
		pickupgridview: {
			caption: "轮胎清单",
      		title: "轮胎清单选择表格视图",
		},
		pickupview: {
			caption: "轮胎清单",
      		title: "轮胎清单数据选择视图",
		},
	},
	main_grid: {
		columns: {
			eicaminfo: "轮胎信息",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};