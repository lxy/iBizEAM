export default {
  fields: {
    createman: "建立人",
    updatedate: "更新时间",
    enable: "逻辑有效标志",
    updateman: "更新人",
    berthcode: "泊位编码",
    createdate: "建立时间",
    emberthname: "泊位名称",
    emberthid: "流水号",
  },
	views: {
		pickupview: {
			caption: "泊位",
      		title: "泊位数据选择视图",
		},
		pickupgridview: {
			caption: "泊位",
      		title: "泊位选择表格视图",
		},
	},
	main_grid: {
		columns: {
			emberthname: "泊位名称",
			updateman: "更新人",
			updatedate: "更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};