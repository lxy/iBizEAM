import emeqdebug_en_US from '@locale/lanres/entities/emeqdebug/emeqdebug_en_US';
import dynadashboard_en_US from '@locale/lanres/entities/dyna-dashboard/dyna-dashboard_en_US';
import emrfodetype_en_US from '@locale/lanres/entities/emrfodetype/emrfodetype_en_US';
import emequip_en_US from '@locale/lanres/entities/emequip/emequip_en_US';
import emplandetail_en_US from '@locale/lanres/entities/emplan-detail/emplan-detail_en_US';
import emobject_en_US from '@locale/lanres/entities/emobject/emobject_en_US';
import emeqtype_en_US from '@locale/lanres/entities/emeqtype/emeqtype_en_US';
import emeqmonitor_en_US from '@locale/lanres/entities/emeqmonitor/emeqmonitor_en_US';
import emeqsetup_en_US from '@locale/lanres/entities/emeqsetup/emeqsetup_en_US';
import emeqspare_en_US from '@locale/lanres/entities/emeqspare/emeqspare_en_US';
import pfteam_en_US from '@locale/lanres/entities/pfteam/pfteam_en_US';
import emserviceevl_en_US from '@locale/lanres/entities/emservice-evl/emservice-evl_en_US';
import emapply_en_US from '@locale/lanres/entities/emapply/emapply_en_US';
import empo_en_US from '@locale/lanres/entities/empo/empo_en_US';
import emwo_en_US from '@locale/lanres/entities/emwo/emwo_en_US';
import emitempl_en_US from '@locale/lanres/entities/emitem-pl/emitem-pl_en_US';
import emeqkpmap_en_US from '@locale/lanres/entities/emeqkpmap/emeqkpmap_en_US';
import emwork_en_US from '@locale/lanres/entities/emwork/emwork_en_US';
import emeqah_en_US from '@locale/lanres/entities/emeqah/emeqah_en_US';
import emeqkeep_en_US from '@locale/lanres/entities/emeqkeep/emeqkeep_en_US';
import emitemcs_en_US from '@locale/lanres/entities/emitem-cs/emitem-cs_en_US';
import emeqmp_en_US from '@locale/lanres/entities/emeqmp/emeqmp_en_US';
import emeqkp_en_US from '@locale/lanres/entities/emeqkp/emeqkp_en_US';
import emstock_en_US from '@locale/lanres/entities/emstock/emstock_en_US';
import emdprct_en_US from '@locale/lanres/entities/emdprct/emdprct_en_US';
import emwplistcost_en_US from '@locale/lanres/entities/emwplist-cost/emwplist-cost_en_US';
import emplantempl_en_US from '@locale/lanres/entities/emplan-templ/emplan-templ_en_US';
import emmachmodel_en_US from '@locale/lanres/entities/emmach-model/emmach-model_en_US';
import emresitem_en_US from '@locale/lanres/entities/emres-item/emres-item_en_US';
import emitemprtn_en_US from '@locale/lanres/entities/emitem-prtn/emitem-prtn_en_US';
import emservice_en_US from '@locale/lanres/entities/emservice/emservice_en_US';
import pfdept_en_US from '@locale/lanres/entities/pfdept/pfdept_en_US';
import emassetclear_en_US from '@locale/lanres/entities/emasset-clear/emasset-clear_en_US';
import emen_en_US from '@locale/lanres/entities/emen/emen_en_US';
import emstorepart_en_US from '@locale/lanres/entities/emstore-part/emstore-part_en_US';
import emrfoca_en_US from '@locale/lanres/entities/emrfoca/emrfoca_en_US';
import emeqcheck_en_US from '@locale/lanres/entities/emeqcheck/emeqcheck_en_US';
import pfcontract_en_US from '@locale/lanres/entities/pfcontract/pfcontract_en_US';
import emdrwg_en_US from '@locale/lanres/entities/emdrwg/emdrwg_en_US';
import pfemppostmap_en_US from '@locale/lanres/entities/pfemp-post-map/pfemp-post-map_en_US';
import emitemtype_en_US from '@locale/lanres/entities/emitem-type/emitem-type_en_US';
import emeqsparemap_en_US from '@locale/lanres/entities/emeqspare-map/emeqspare-map_en_US';
import emeqmaintance_en_US from '@locale/lanres/entities/emeqmaintance/emeqmaintance_en_US';
import emitemrout_en_US from '@locale/lanres/entities/emitem-rout/emitem-rout_en_US';
import emeqlocation_en_US from '@locale/lanres/entities/emeqlocation/emeqlocation_en_US';
import emrfoac_en_US from '@locale/lanres/entities/emrfoac/emrfoac_en_US';
import emstore_en_US from '@locale/lanres/entities/emstore/emstore_en_US';
import emenconsum_en_US from '@locale/lanres/entities/emenconsum/emenconsum_en_US';
import emwo_en_en_US from '@locale/lanres/entities/emwo-en/emwo-en_en_US';
import emitemrin_en_US from '@locale/lanres/entities/emitem-rin/emitem-rin_en_US';
import emitemtrade_en_US from '@locale/lanres/entities/emitem-trade/emitem-trade_en_US';
import emeqlctmap_en_US from '@locale/lanres/entities/emeqlctmap/emeqlctmap_en_US';
import emwoori_en_US from '@locale/lanres/entities/emwoori/emwoori_en_US';
import emeqsparedetail_en_US from '@locale/lanres/entities/emeqspare-detail/emeqspare-detail_en_US';
import emeqlctrhy_en_US from '@locale/lanres/entities/emeqlctrhy/emeqlctrhy_en_US';
import emobjmap_en_US from '@locale/lanres/entities/emobj-map/emobj-map_en_US';
import emoutput_en_US from '@locale/lanres/entities/emoutput/emoutput_en_US';
import emoutputrct_en_US from '@locale/lanres/entities/emoutput-rct/emoutput-rct_en_US';
import emwo_dp_en_US from '@locale/lanres/entities/emwo-dp/emwo-dp_en_US';
import emrfode_en_US from '@locale/lanres/entities/emrfode/emrfode_en_US';
import emwplist_en_US from '@locale/lanres/entities/emwplist/emwplist_en_US';
import empurplan_en_US from '@locale/lanres/entities/empur-plan/empur-plan_en_US';
import emrfomo_en_US from '@locale/lanres/entities/emrfomo/emrfomo_en_US';
import emeqlctfdj_en_US from '@locale/lanres/entities/emeqlctfdj/emeqlctfdj_en_US';
import emrfodemap_en_US from '@locale/lanres/entities/emrfodemap/emrfodemap_en_US';
import emeqlcttires_en_US from '@locale/lanres/entities/emeqlcttires/emeqlcttires_en_US';
import emitem_en_US from '@locale/lanres/entities/emitem/emitem_en_US';
import emdrwgmap_en_US from '@locale/lanres/entities/emdrwgmap/emdrwgmap_en_US';
import emberth_en_US from '@locale/lanres/entities/emberth/emberth_en_US';
import emeitires_en_US from '@locale/lanres/entities/emeitires/emeitires_en_US';
import emmachinecategory_en_US from '@locale/lanres/entities/emmachine-category/emmachine-category_en_US';
import emwo_osc_en_US from '@locale/lanres/entities/emwo-osc/emwo-osc_en_US';
import emeqwl_en_US from '@locale/lanres/entities/emeqwl/emeqwl_en_US';
import dynachart_en_US from '@locale/lanres/entities/dyna-chart/dyna-chart_en_US';
import pfemp_en_US from '@locale/lanres/entities/pfemp/pfemp_en_US';
import emcab_en_US from '@locale/lanres/entities/emcab/emcab_en_US';
import emassetclass_en_US from '@locale/lanres/entities/emasset-class/emasset-class_en_US';
import emeqkprcd_en_US from '@locale/lanres/entities/emeqkprcd/emeqkprcd_en_US';
import emitempuse_en_US from '@locale/lanres/entities/emitem-puse/emitem-puse_en_US';
import emplan_en_US from '@locale/lanres/entities/emplan/emplan_en_US';
import embrand_en_US from '@locale/lanres/entities/embrand/embrand_en_US';
import emacclass_en_US from '@locale/lanres/entities/emacclass/emacclass_en_US';
import emproduct_en_US from '@locale/lanres/entities/emproduct/emproduct_en_US';
import emwo_inner_en_US from '@locale/lanres/entities/emwo-inner/emwo-inner_en_US';
import emasset_en_US from '@locale/lanres/entities/emasset/emasset_en_US';
import pfunit_en_US from '@locale/lanres/entities/pfunit/pfunit_en_US';
import empodetail_en_US from '@locale/lanres/entities/empodetail/empodetail_en_US';
import emplancdt_en_US from '@locale/lanres/entities/emplan-cdt/emplan-cdt_en_US';
import emeqlctgss_en_US from '@locale/lanres/entities/emeqlctgss/emeqlctgss_en_US';
import emeqmpmtr_en_US from '@locale/lanres/entities/emeqmpmtr/emeqmpmtr_en_US';
import components_en_US from '@locale/lanres/components/components_en_US';
import codelist_en_US from '@locale/lanres/codelist/codelist_en_US';
import userCustom_en_US from '@locale/lanres/userCustom/userCustom_en_US';

export default {
    app: {
        commonWords:{
            error: "Error",
            success: "Success",
            ok: "OK",
            cancel: "Cancel",
            save: "Save",
            codeNotExist: 'Code list does not exist',
            reqException: "Request exception",
            sysException: "System abnormality",
            warning: "Warning",
            wrong: "Error",
            rulesException: "Abnormal value check rule",
            saveSuccess: "Saved successfully",
            saveFailed: "Save failed",
            deleteSuccess: "Successfully deleted!",
            deleteError: "Failed to delete",
            delDataFail: "Failed to delete data",
            noData: "No data",
        },
        local:{
            new: "New",
            add: "Add",
        },
        gridpage: {
            choicecolumns: "Choice columns",
            refresh: "refresh",
            show: "Show",
            records: "records",
            totle: "totle",
            noData: "No data",
            valueVail: "Value cannot be empty",
            notConfig: {
                fetchAction: "The view table fetchaction parameter is not configured",
                removeAction: "The view table removeaction parameter is not configured",
                createAction: "The view table createaction parameter is not configured",
                updateAction: "The view table updateaction parameter is not configured",
                loaddraftAction: "The view table loadtrafaction parameter is not configured",
            },
            data: "Data",
            delDataFail: "Failed to delete data",
            delSuccess: "Delete successfully!",
            confirmDel: "Are you sure you want to delete",
            notRecoverable: "delete will not be recoverable?",
            notBatch: "Batch addition not implemented",
            grid: "Grid",
            exportFail: "Data export failed",
            sum: "Total",
            formitemFailed: "Form item update failed",
        },
        list: {
            notConfig: {
                fetchAction: "View list fetchAction parameter is not configured",
                removeAction: "View table removeAction parameter is not configured",
                createAction: "View list createAction parameter is not configured",
                updateAction: "View list updateAction parameter is not configured",
            },
            confirmDel: "Are you sure you want to delete",
            notRecoverable: "delete will not be recoverable?",
        },
        listExpBar: {
            title: "List navigation bar",
        },
        wfExpBar: {
            title: "Process navigation bar",
        },
        calendarExpBar:{
            title: "Calendar navigation bar",
        },
        treeExpBar: {
            title: "Tree view navigation bar",
        },
        portlet: {
            noExtensions: "No extensions",
        },
        tabpage: {
            sureclosetip: {
                title: "Close warning",
                content: "Form data Changed, are sure close?",
            },
            closeall: "Close all",
            closeother: "Close other",
        },
        fileUpload: {
            caption: "Upload",
        },
        searchButton: {
            search: "Search",
            reset: "Reset",
        },
        calendar:{
          today: "today",
          month: "month",
          week: "week",
          day: "day",
          list: "list",
          dateSelectModalTitle: "select the time you wanted",
          gotoDate: "goto",
          from: "From",
          to: "To",
        },
        // 非实体视图
        views: {
            appportalview: {
                caption: "",
                title: "应用门户视图",
            },
            appindex: {
                caption: "iBizEAM",
                title: "iBizEAM",
            },
        },
        utilview:{
            importview:"Import Data",
            warning:"warning",
            info:"Please configure the data import item"    
        },
        menus: {
            emeqtypetreemenu: {
                menuitem1: "添加设备类型",
                seperator1: "菜单项",
                menuitem2: "添加设备",
            },
            indexview: {
                menuitem61: "工作台",
                menuitem3: "设备",
                menuitem1: "设备类型",
                menuitem8: "设备档案",
                menuitem5: "位置",
                menuitem6: "文档",
                menuitem7: "备件包",
                menuitem72: "运行",
                menuitem73: "运行日志",
                menuitem74: "运行监控",
                menuitem77: "仪表",
                menuitem78: "仪表读数",
                menuitem75: "关键点",
                menuitem76: "关键点记录",
                menuitem16: "计划",
                menuitem17: "计划",
                menuitem18: "计划模板",
                menuitem33: "能耗",
                menuitem50: "能源",
                menuitem55: "能耗",
                menuitem9: "工单",
                menuitem69: "工单日历",
                menuitem11: "内部工单",
                menuitem12: "外委工单",
                menuitem13: "能耗工单",
                menuitem14: "点检工单",
                menuitem15: "外委申请",
                menuitem32: "活动",
                menuitem70: "活动日历",
                menuitem56: "更换安装",
                menuitem57: "事故记录",
                menuitem58: "维修记录",
                menuitem59: "抢修记录",
                menuitem60: "保养记录",
                menuitem31: "故障",
                menuitem79: "故障知识库",
                menuitem48: "现象",
                menuitem47: "现象分类",
                menuitem49: "模式",
                menuitem51: "原因",
                menuitem52: "方案",
                menuitem30: "资产",
                menuitem43: "资产科目",
                menuitem44: "固定资产台账",
                menuitem45: "报废资产",
                menuitem46: "资产盘点记录",
                menuitem4: "材料",
                menuitem2: "物品类型",
                menuitem19: "物品",
                menuitem21: "库存管理",
                menuitem24: "损溢单",
                menuitem25: "调整单",
                menuitem27: "出库单",
                menuitem23: "领料单",
                menuitem28: "还料单",
                menuitem29: "采购",
                menuitem64: "采购流程",
                menuitem38: "服务商",
                menuitem39: "服务商评估",
                menuitem62: "预警",
                menuitem63: "钢丝绳位置超期预警",
            },
        },
        formpage:{
            error: "Error",
            desc1: "Operation failed, failed to find current form item",
            desc2: "Can't continue",
            notconfig: {
                loadaction: "View form loadAction parameter is not configured",
                loaddraftaction: "View form loaddraftAction parameter is not configured",
                actionname: "View form actionName parameter is not configured",
                removeaction: "View form removeAction parameter is not configured",
            },
            saveerror: "Error saving data",
            savecontent: "The data is inconsistent. The background data may have been modified. Do you want to reload the data?",
            valuecheckex: "Value rule check exception",
            savesuccess: "Saved successfully!",
            deletesuccess: "Successfully deleted!",  
            workflow: {
                starterror: "Workflow started successfully",
                startsuccess: "Workflow failed to start",
                submiterror: "Workflow submission failed",
                submitsuccess: "Workflow submitted successfully",
            },
            updateerror: "Form item update failed",       
        },
        gridBar: {
            title: "Table navigation bar",
        },
        multiEditView: {
            notConfig: {
                fetchAction: "View multi-edit view panel fetchAction parameter is not configured",
                loaddraftAction: "View multi-edit view panel loaddraftAction parameter is not configured",
            },
        },
        dataViewExpBar: {
            title: "Card view navigation bar",
        },
        kanban: {
            notConfig: {
                fetchAction: "View list fetchAction parameter is not configured",
                removeAction: "View table removeAction parameter is not configured",
            },
            delete1: "Confirm to delete ",
            delete2: "the delete operation will be unrecoverable!",
        },
        dashBoard: {
            handleClick: {
                title: "Panel design",
            },
        },
        dataView: {
            sum: "total",
            data: "data",
        },
        chart: {
            undefined: "Undefined",
            quarter: "Quarter",   
            year: "Year",
        },
        searchForm: {
            notConfig: {
                loadAction: "View search form loadAction parameter is not configured",
                loaddraftAction: "View search form loaddraftAction parameter is not configured",
            },
            custom: "Store custom queries",
            title: "Name",
        },
        wizardPanel: {
            back: "Back",
            next: "Next",
            complete: "Complete",
        },
        viewLayoutPanel: {
            appLogoutView: {
                prompt1: "Dear customer, you have successfully exited the system, after",
                prompt2: "seconds, we will jump to the",
                logingPage: "login page",
            },
            appWfstepTraceView: {
                title: "Application process processing record view",
            },
            appWfstepDataView: {
                title: "Application process tracking view",
            },
            appLoginView: {
                username: "Username",
                password: "Password",
                login: "Login",
            },
        },
    },
    entities: {
        emeqdebug: emeqdebug_en_US,
        dynadashboard: dynadashboard_en_US,
        emrfodetype: emrfodetype_en_US,
        emequip: emequip_en_US,
        emplandetail: emplandetail_en_US,
        emobject: emobject_en_US,
        emeqtype: emeqtype_en_US,
        emeqmonitor: emeqmonitor_en_US,
        emeqsetup: emeqsetup_en_US,
        emeqspare: emeqspare_en_US,
        pfteam: pfteam_en_US,
        emserviceevl: emserviceevl_en_US,
        emapply: emapply_en_US,
        empo: empo_en_US,
        emwo: emwo_en_US,
        emitempl: emitempl_en_US,
        emeqkpmap: emeqkpmap_en_US,
        emwork: emwork_en_US,
        emeqah: emeqah_en_US,
        emeqkeep: emeqkeep_en_US,
        emitemcs: emitemcs_en_US,
        emeqmp: emeqmp_en_US,
        emeqkp: emeqkp_en_US,
        emstock: emstock_en_US,
        emdprct: emdprct_en_US,
        emwplistcost: emwplistcost_en_US,
        emplantempl: emplantempl_en_US,
        emmachmodel: emmachmodel_en_US,
        emresitem: emresitem_en_US,
        emitemprtn: emitemprtn_en_US,
        emservice: emservice_en_US,
        pfdept: pfdept_en_US,
        emassetclear: emassetclear_en_US,
        emen: emen_en_US,
        emstorepart: emstorepart_en_US,
        emrfoca: emrfoca_en_US,
        emeqcheck: emeqcheck_en_US,
        pfcontract: pfcontract_en_US,
        emdrwg: emdrwg_en_US,
        pfemppostmap: pfemppostmap_en_US,
        emitemtype: emitemtype_en_US,
        emeqsparemap: emeqsparemap_en_US,
        emeqmaintance: emeqmaintance_en_US,
        emitemrout: emitemrout_en_US,
        emeqlocation: emeqlocation_en_US,
        emrfoac: emrfoac_en_US,
        emstore: emstore_en_US,
        emenconsum: emenconsum_en_US,
        emwo_en: emwo_en_en_US,
        emitemrin: emitemrin_en_US,
        emitemtrade: emitemtrade_en_US,
        emeqlctmap: emeqlctmap_en_US,
        emwoori: emwoori_en_US,
        emeqsparedetail: emeqsparedetail_en_US,
        emeqlctrhy: emeqlctrhy_en_US,
        emobjmap: emobjmap_en_US,
        emoutput: emoutput_en_US,
        emoutputrct: emoutputrct_en_US,
        emwo_dp: emwo_dp_en_US,
        emrfode: emrfode_en_US,
        emwplist: emwplist_en_US,
        empurplan: empurplan_en_US,
        emrfomo: emrfomo_en_US,
        emeqlctfdj: emeqlctfdj_en_US,
        emrfodemap: emrfodemap_en_US,
        emeqlcttires: emeqlcttires_en_US,
        emitem: emitem_en_US,
        emdrwgmap: emdrwgmap_en_US,
        emberth: emberth_en_US,
        emeitires: emeitires_en_US,
        emmachinecategory: emmachinecategory_en_US,
        emwo_osc: emwo_osc_en_US,
        emeqwl: emeqwl_en_US,
        dynachart: dynachart_en_US,
        pfemp: pfemp_en_US,
        emcab: emcab_en_US,
        emassetclass: emassetclass_en_US,
        emeqkprcd: emeqkprcd_en_US,
        emitempuse: emitempuse_en_US,
        emplan: emplan_en_US,
        embrand: embrand_en_US,
        emacclass: emacclass_en_US,
        emproduct: emproduct_en_US,
        emwo_inner: emwo_inner_en_US,
        emasset: emasset_en_US,
        pfunit: pfunit_en_US,
        empodetail: empodetail_en_US,
        emplancdt: emplancdt_en_US,
        emeqlctgss: emeqlctgss_en_US,
        emeqmpmtr: emeqmpmtr_en_US,
    },
    components: components_en_US,
    codelist: codelist_en_US,
    userCustom: userCustom_en_US,
};