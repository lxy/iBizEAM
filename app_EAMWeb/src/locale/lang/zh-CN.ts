import emeqdebug_zh_CN from '@locale/lanres/entities/emeqdebug/emeqdebug_zh_CN';
import dynadashboard_zh_CN from '@locale/lanres/entities/dyna-dashboard/dyna-dashboard_zh_CN';
import emrfodetype_zh_CN from '@locale/lanres/entities/emrfodetype/emrfodetype_zh_CN';
import emequip_zh_CN from '@locale/lanres/entities/emequip/emequip_zh_CN';
import emplandetail_zh_CN from '@locale/lanres/entities/emplan-detail/emplan-detail_zh_CN';
import emobject_zh_CN from '@locale/lanres/entities/emobject/emobject_zh_CN';
import emeqtype_zh_CN from '@locale/lanres/entities/emeqtype/emeqtype_zh_CN';
import emeqmonitor_zh_CN from '@locale/lanres/entities/emeqmonitor/emeqmonitor_zh_CN';
import emeqsetup_zh_CN from '@locale/lanres/entities/emeqsetup/emeqsetup_zh_CN';
import emeqspare_zh_CN from '@locale/lanres/entities/emeqspare/emeqspare_zh_CN';
import pfteam_zh_CN from '@locale/lanres/entities/pfteam/pfteam_zh_CN';
import emserviceevl_zh_CN from '@locale/lanres/entities/emservice-evl/emservice-evl_zh_CN';
import emapply_zh_CN from '@locale/lanres/entities/emapply/emapply_zh_CN';
import empo_zh_CN from '@locale/lanres/entities/empo/empo_zh_CN';
import emwo_zh_CN from '@locale/lanres/entities/emwo/emwo_zh_CN';
import emitempl_zh_CN from '@locale/lanres/entities/emitem-pl/emitem-pl_zh_CN';
import emeqkpmap_zh_CN from '@locale/lanres/entities/emeqkpmap/emeqkpmap_zh_CN';
import emwork_zh_CN from '@locale/lanres/entities/emwork/emwork_zh_CN';
import emeqah_zh_CN from '@locale/lanres/entities/emeqah/emeqah_zh_CN';
import emeqkeep_zh_CN from '@locale/lanres/entities/emeqkeep/emeqkeep_zh_CN';
import emitemcs_zh_CN from '@locale/lanres/entities/emitem-cs/emitem-cs_zh_CN';
import emeqmp_zh_CN from '@locale/lanres/entities/emeqmp/emeqmp_zh_CN';
import emeqkp_zh_CN from '@locale/lanres/entities/emeqkp/emeqkp_zh_CN';
import emstock_zh_CN from '@locale/lanres/entities/emstock/emstock_zh_CN';
import emdprct_zh_CN from '@locale/lanres/entities/emdprct/emdprct_zh_CN';
import emwplistcost_zh_CN from '@locale/lanres/entities/emwplist-cost/emwplist-cost_zh_CN';
import emplantempl_zh_CN from '@locale/lanres/entities/emplan-templ/emplan-templ_zh_CN';
import emmachmodel_zh_CN from '@locale/lanres/entities/emmach-model/emmach-model_zh_CN';
import emresitem_zh_CN from '@locale/lanres/entities/emres-item/emres-item_zh_CN';
import emitemprtn_zh_CN from '@locale/lanres/entities/emitem-prtn/emitem-prtn_zh_CN';
import emservice_zh_CN from '@locale/lanres/entities/emservice/emservice_zh_CN';
import pfdept_zh_CN from '@locale/lanres/entities/pfdept/pfdept_zh_CN';
import emassetclear_zh_CN from '@locale/lanres/entities/emasset-clear/emasset-clear_zh_CN';
import emen_zh_CN from '@locale/lanres/entities/emen/emen_zh_CN';
import emstorepart_zh_CN from '@locale/lanres/entities/emstore-part/emstore-part_zh_CN';
import emrfoca_zh_CN from '@locale/lanres/entities/emrfoca/emrfoca_zh_CN';
import emeqcheck_zh_CN from '@locale/lanres/entities/emeqcheck/emeqcheck_zh_CN';
import pfcontract_zh_CN from '@locale/lanres/entities/pfcontract/pfcontract_zh_CN';
import emdrwg_zh_CN from '@locale/lanres/entities/emdrwg/emdrwg_zh_CN';
import pfemppostmap_zh_CN from '@locale/lanres/entities/pfemp-post-map/pfemp-post-map_zh_CN';
import emitemtype_zh_CN from '@locale/lanres/entities/emitem-type/emitem-type_zh_CN';
import emeqsparemap_zh_CN from '@locale/lanres/entities/emeqspare-map/emeqspare-map_zh_CN';
import emeqmaintance_zh_CN from '@locale/lanres/entities/emeqmaintance/emeqmaintance_zh_CN';
import emitemrout_zh_CN from '@locale/lanres/entities/emitem-rout/emitem-rout_zh_CN';
import emeqlocation_zh_CN from '@locale/lanres/entities/emeqlocation/emeqlocation_zh_CN';
import emrfoac_zh_CN from '@locale/lanres/entities/emrfoac/emrfoac_zh_CN';
import emstore_zh_CN from '@locale/lanres/entities/emstore/emstore_zh_CN';
import emenconsum_zh_CN from '@locale/lanres/entities/emenconsum/emenconsum_zh_CN';
import emwo_en_zh_CN from '@locale/lanres/entities/emwo-en/emwo-en_zh_CN';
import emitemrin_zh_CN from '@locale/lanres/entities/emitem-rin/emitem-rin_zh_CN';
import emitemtrade_zh_CN from '@locale/lanres/entities/emitem-trade/emitem-trade_zh_CN';
import emeqlctmap_zh_CN from '@locale/lanres/entities/emeqlctmap/emeqlctmap_zh_CN';
import emwoori_zh_CN from '@locale/lanres/entities/emwoori/emwoori_zh_CN';
import emeqsparedetail_zh_CN from '@locale/lanres/entities/emeqspare-detail/emeqspare-detail_zh_CN';
import emeqlctrhy_zh_CN from '@locale/lanres/entities/emeqlctrhy/emeqlctrhy_zh_CN';
import emobjmap_zh_CN from '@locale/lanres/entities/emobj-map/emobj-map_zh_CN';
import emoutput_zh_CN from '@locale/lanres/entities/emoutput/emoutput_zh_CN';
import emoutputrct_zh_CN from '@locale/lanres/entities/emoutput-rct/emoutput-rct_zh_CN';
import emwo_dp_zh_CN from '@locale/lanres/entities/emwo-dp/emwo-dp_zh_CN';
import emrfode_zh_CN from '@locale/lanres/entities/emrfode/emrfode_zh_CN';
import emwplist_zh_CN from '@locale/lanres/entities/emwplist/emwplist_zh_CN';
import empurplan_zh_CN from '@locale/lanres/entities/empur-plan/empur-plan_zh_CN';
import emrfomo_zh_CN from '@locale/lanres/entities/emrfomo/emrfomo_zh_CN';
import emeqlctfdj_zh_CN from '@locale/lanres/entities/emeqlctfdj/emeqlctfdj_zh_CN';
import emrfodemap_zh_CN from '@locale/lanres/entities/emrfodemap/emrfodemap_zh_CN';
import emeqlcttires_zh_CN from '@locale/lanres/entities/emeqlcttires/emeqlcttires_zh_CN';
import emitem_zh_CN from '@locale/lanres/entities/emitem/emitem_zh_CN';
import emdrwgmap_zh_CN from '@locale/lanres/entities/emdrwgmap/emdrwgmap_zh_CN';
import emberth_zh_CN from '@locale/lanres/entities/emberth/emberth_zh_CN';
import emeitires_zh_CN from '@locale/lanres/entities/emeitires/emeitires_zh_CN';
import emmachinecategory_zh_CN from '@locale/lanres/entities/emmachine-category/emmachine-category_zh_CN';
import emwo_osc_zh_CN from '@locale/lanres/entities/emwo-osc/emwo-osc_zh_CN';
import emeqwl_zh_CN from '@locale/lanres/entities/emeqwl/emeqwl_zh_CN';
import dynachart_zh_CN from '@locale/lanres/entities/dyna-chart/dyna-chart_zh_CN';
import pfemp_zh_CN from '@locale/lanres/entities/pfemp/pfemp_zh_CN';
import emcab_zh_CN from '@locale/lanres/entities/emcab/emcab_zh_CN';
import emassetclass_zh_CN from '@locale/lanres/entities/emasset-class/emasset-class_zh_CN';
import emeqkprcd_zh_CN from '@locale/lanres/entities/emeqkprcd/emeqkprcd_zh_CN';
import emitempuse_zh_CN from '@locale/lanres/entities/emitem-puse/emitem-puse_zh_CN';
import emplan_zh_CN from '@locale/lanres/entities/emplan/emplan_zh_CN';
import embrand_zh_CN from '@locale/lanres/entities/embrand/embrand_zh_CN';
import emacclass_zh_CN from '@locale/lanres/entities/emacclass/emacclass_zh_CN';
import emproduct_zh_CN from '@locale/lanres/entities/emproduct/emproduct_zh_CN';
import emwo_inner_zh_CN from '@locale/lanres/entities/emwo-inner/emwo-inner_zh_CN';
import emasset_zh_CN from '@locale/lanres/entities/emasset/emasset_zh_CN';
import pfunit_zh_CN from '@locale/lanres/entities/pfunit/pfunit_zh_CN';
import empodetail_zh_CN from '@locale/lanres/entities/empodetail/empodetail_zh_CN';
import emplancdt_zh_CN from '@locale/lanres/entities/emplan-cdt/emplan-cdt_zh_CN';
import emeqlctgss_zh_CN from '@locale/lanres/entities/emeqlctgss/emeqlctgss_zh_CN';
import emeqmpmtr_zh_CN from '@locale/lanres/entities/emeqmpmtr/emeqmpmtr_zh_CN';
import components_zh_CN from '@locale/lanres/components/components_zh_CN';
import codelist_zh_CN from '@locale/lanres/codelist/codelist_zh_CN';
import userCustom_zh_CN from '@locale/lanres/userCustom/userCustom_zh_CN';

export default {
    app: {
        commonWords:{
            error: "失败",
            success: "成功",
            ok: "确认",
            cancel: "取消",
            save: "保存",
            codeNotExist: "代码表不存在",
            reqException: "请求异常",
            sysException: "系统异常",
            warning: "警告",
            wrong: "错误",
            rulesException: "值规则校验异常",
            saveSuccess: "保存成功",
            saveFailed: "保存失败",
            deleteSuccess: "删除成功！",
            deleteError: "删除失败！",
            delDataFail: "删除数据失败",
            noData: "暂无数据",
        },
        local:{
            new: "新建",
            add: "增加",
        },
        gridpage: {
            choicecolumns: "选择列",
            refresh: "刷新",
            show: "显示",
            records: "条",
            totle: "共",
            noData: "无数据",
            valueVail: "值不能为空",
            notConfig: {
                fetchAction: "视图表格fetchAction参数未配置",
                removeAction: "视图表格removeAction参数未配置",
                createAction: "视图表格createAction参数未配置",
                updateAction: "视图表格updateAction参数未配置",
                loaddraftAction: "视图表格loaddraftAction参数未配置",
            },
            data: "数据",
            delDataFail: "删除数据失败",
            delSuccess: "删除成功!",
            confirmDel: "确认要删除",
            notRecoverable: "删除操作将不可恢复？",
            notBatch: "批量添加未实现",
            grid: "表",
            exportFail: "数据导出失败",
            sum: "合计",
            formitemFailed: "表单项更新失败",
        },
        list: {
            notConfig: {
                fetchAction: "视图列表fetchAction参数未配置",
                removeAction: "视图表格removeAction参数未配置",
                createAction: "视图列表createAction参数未配置",
                updateAction: "视图列表updateAction参数未配置",
            },
            confirmDel: "确认要删除",
            notRecoverable: "删除操作将不可恢复？",
        },
        listExpBar: {
            title: "列表导航栏",
        },
        wfExpBar: {
            title: "流程导航栏",
        },
        calendarExpBar:{
            title: "日历导航栏",
        },
        treeExpBar: {
            title: "树视图导航栏",
        },
        portlet: {
            noExtensions: "无扩展插件",
        },
        tabpage: {
            sureclosetip: {
                title: "关闭提醒",
                content: "表单数据已经修改，确定要关闭？",
            },
            closeall: "关闭所有",
            closeother: "关闭其他",
        },
        fileUpload: {
            caption: "上传",
        },
        searchButton: {
            search: "搜索",
            reset: "重置",
        },
        calendar:{
          today: "今天",
          month: "月",
          week: "周",
          day: "天",
          list: "列",
          dateSelectModalTitle: "选择要跳转的时间",
          gotoDate: "跳转",
          from: "从",
          to: "至",
        },
        // 非实体视图
        views: {
            appportalview: {
                caption: "",
                title: "应用门户视图",
            },
            appindex: {
                caption: "iBizEAM",
                title: "iBizEAM",
            },
        },
        utilview:{
            importview:"导入数据",
            warning:"警告",
            info:"请配置数据导入项" 
        },
        menus: {
            emeqtypetreemenu: {
                menuitem1: "添加设备类型",
                seperator1: "菜单项",
                menuitem2: "添加设备",
            },
            indexview: {
                menuitem61: "工作台",
                menuitem3: "设备",
                menuitem1: "设备类型",
                menuitem8: "设备档案",
                menuitem5: "位置",
                menuitem6: "文档",
                menuitem7: "备件包",
                menuitem72: "运行",
                menuitem73: "运行日志",
                menuitem74: "运行监控",
                menuitem77: "仪表",
                menuitem78: "仪表读数",
                menuitem75: "关键点",
                menuitem76: "关键点记录",
                menuitem16: "计划",
                menuitem17: "计划",
                menuitem18: "计划模板",
                menuitem33: "能耗",
                menuitem50: "能源",
                menuitem55: "能耗",
                menuitem9: "工单",
                menuitem69: "工单日历",
                menuitem11: "内部工单",
                menuitem12: "外委工单",
                menuitem13: "能耗工单",
                menuitem14: "点检工单",
                menuitem15: "外委申请",
                menuitem32: "活动",
                menuitem70: "活动日历",
                menuitem56: "更换安装",
                menuitem57: "事故记录",
                menuitem58: "维修记录",
                menuitem59: "抢修记录",
                menuitem60: "保养记录",
                menuitem31: "故障",
                menuitem79: "故障知识库",
                menuitem48: "现象",
                menuitem47: "现象分类",
                menuitem49: "模式",
                menuitem51: "原因",
                menuitem52: "方案",
                menuitem30: "资产",
                menuitem43: "资产科目",
                menuitem44: "固定资产台账",
                menuitem45: "报废资产",
                menuitem46: "资产盘点记录",
                menuitem4: "材料",
                menuitem2: "物品类型",
                menuitem19: "物品",
                menuitem21: "库存管理",
                menuitem24: "损溢单",
                menuitem25: "调整单",
                menuitem27: "出库单",
                menuitem23: "领料单",
                menuitem28: "还料单",
                menuitem29: "采购",
                menuitem64: "采购流程",
                menuitem38: "服务商",
                menuitem39: "服务商评估",
                menuitem62: "预警",
                menuitem63: "钢丝绳位置超期预警",
            },
        },
        formpage:{
            desc1: "操作失败,未能找到当前表单项",
            desc2: "无法继续操作",
            notconfig: {
                loadaction: "视图表单loadAction参数未配置",
                loaddraftaction: "视图表单loaddraftAction参数未配置",
                actionname: "视图表单'+actionName+'参数未配置",
                removeaction: "视图表单removeAction参数未配置",
            },
            saveerror: "保存数据发生错误",
            savecontent: "数据不一致，可能后台数据已经被修改,是否要重新加载数据？",
            valuecheckex: "值规则校验异常",
            savesuccess: "保存成功！",
            deletesuccess: "删除成功！",  
            workflow: {
                starterror: "工作流启动失败",
                startsuccess: "工作流启动成功",
                submiterror: "工作流提交失败",
                submitsuccess: "工作流提交成功",
            },
            updateerror: "表单项更新失败",     
        },
        gridBar: {
            title: "表格导航栏",
        },
        multiEditView: {
            notConfig: {
                fetchAction: "视图多编辑视图面板fetchAction参数未配置",
                loaddraftAction: "视图多编辑视图面板loaddraftAction参数未配置",
            },
        },
        dataViewExpBar: {
            title: "卡片视图导航栏",
        },
        kanban: {
            notConfig: {
                fetchAction: "视图列表fetchAction参数未配置",
                removeAction: "视图表格removeAction参数未配置",
            },
            delete1: "确认要删除 ",
            delete2: "删除操作将不可恢复？",
        },
        dashBoard: {
            handleClick: {
                title: "面板设计",
            },
        },
        dataView: {
            sum: "共",
            data: "条数据",
        },
        chart: {
            undefined: "未定义",
            quarter: "季度",   
            year: "年",
        },
        searchForm: {
            notConfig: {
                loadAction: "视图搜索表单loadAction参数未配置",
                loaddraftAction: "视图搜索表单loaddraftAction参数未配置",
            },
            custom: "存储自定义查询",
            title: "名称",
        },
        wizardPanel: {
            back: "上一步",
            next: "下一步",
            complete: "完成",
        },
        viewLayoutPanel: {
            appLogoutView: {
                prompt1: "尊敬的客户您好，您已成功退出系统，将在",
                prompt2: "秒后跳转至",
                logingPage: "登录页",
            },
            appWfstepTraceView: {
                title: "应用流程处理记录视图",
            },
            appWfstepDataView: {
                title: "应用流程跟踪视图",
            },
            appLoginView: {
                username: "用户名",
                password: "密码",
                login: "登录",
            },
        },
    },
    entities: {
        emeqdebug: emeqdebug_zh_CN,
        dynadashboard: dynadashboard_zh_CN,
        emrfodetype: emrfodetype_zh_CN,
        emequip: emequip_zh_CN,
        emplandetail: emplandetail_zh_CN,
        emobject: emobject_zh_CN,
        emeqtype: emeqtype_zh_CN,
        emeqmonitor: emeqmonitor_zh_CN,
        emeqsetup: emeqsetup_zh_CN,
        emeqspare: emeqspare_zh_CN,
        pfteam: pfteam_zh_CN,
        emserviceevl: emserviceevl_zh_CN,
        emapply: emapply_zh_CN,
        empo: empo_zh_CN,
        emwo: emwo_zh_CN,
        emitempl: emitempl_zh_CN,
        emeqkpmap: emeqkpmap_zh_CN,
        emwork: emwork_zh_CN,
        emeqah: emeqah_zh_CN,
        emeqkeep: emeqkeep_zh_CN,
        emitemcs: emitemcs_zh_CN,
        emeqmp: emeqmp_zh_CN,
        emeqkp: emeqkp_zh_CN,
        emstock: emstock_zh_CN,
        emdprct: emdprct_zh_CN,
        emwplistcost: emwplistcost_zh_CN,
        emplantempl: emplantempl_zh_CN,
        emmachmodel: emmachmodel_zh_CN,
        emresitem: emresitem_zh_CN,
        emitemprtn: emitemprtn_zh_CN,
        emservice: emservice_zh_CN,
        pfdept: pfdept_zh_CN,
        emassetclear: emassetclear_zh_CN,
        emen: emen_zh_CN,
        emstorepart: emstorepart_zh_CN,
        emrfoca: emrfoca_zh_CN,
        emeqcheck: emeqcheck_zh_CN,
        pfcontract: pfcontract_zh_CN,
        emdrwg: emdrwg_zh_CN,
        pfemppostmap: pfemppostmap_zh_CN,
        emitemtype: emitemtype_zh_CN,
        emeqsparemap: emeqsparemap_zh_CN,
        emeqmaintance: emeqmaintance_zh_CN,
        emitemrout: emitemrout_zh_CN,
        emeqlocation: emeqlocation_zh_CN,
        emrfoac: emrfoac_zh_CN,
        emstore: emstore_zh_CN,
        emenconsum: emenconsum_zh_CN,
        emwo_en: emwo_en_zh_CN,
        emitemrin: emitemrin_zh_CN,
        emitemtrade: emitemtrade_zh_CN,
        emeqlctmap: emeqlctmap_zh_CN,
        emwoori: emwoori_zh_CN,
        emeqsparedetail: emeqsparedetail_zh_CN,
        emeqlctrhy: emeqlctrhy_zh_CN,
        emobjmap: emobjmap_zh_CN,
        emoutput: emoutput_zh_CN,
        emoutputrct: emoutputrct_zh_CN,
        emwo_dp: emwo_dp_zh_CN,
        emrfode: emrfode_zh_CN,
        emwplist: emwplist_zh_CN,
        empurplan: empurplan_zh_CN,
        emrfomo: emrfomo_zh_CN,
        emeqlctfdj: emeqlctfdj_zh_CN,
        emrfodemap: emrfodemap_zh_CN,
        emeqlcttires: emeqlcttires_zh_CN,
        emitem: emitem_zh_CN,
        emdrwgmap: emdrwgmap_zh_CN,
        emberth: emberth_zh_CN,
        emeitires: emeitires_zh_CN,
        emmachinecategory: emmachinecategory_zh_CN,
        emwo_osc: emwo_osc_zh_CN,
        emeqwl: emeqwl_zh_CN,
        dynachart: dynachart_zh_CN,
        pfemp: pfemp_zh_CN,
        emcab: emcab_zh_CN,
        emassetclass: emassetclass_zh_CN,
        emeqkprcd: emeqkprcd_zh_CN,
        emitempuse: emitempuse_zh_CN,
        emplan: emplan_zh_CN,
        embrand: embrand_zh_CN,
        emacclass: emacclass_zh_CN,
        emproduct: emproduct_zh_CN,
        emwo_inner: emwo_inner_zh_CN,
        emasset: emasset_zh_CN,
        pfunit: pfunit_zh_CN,
        empodetail: empodetail_zh_CN,
        emplancdt: emplancdt_zh_CN,
        emeqlctgss: emeqlctgss_zh_CN,
        emeqmpmtr: emeqmpmtr_zh_CN,
    },
    components: components_zh_CN,
    codelist: codelist_zh_CN,
    userCustom: userCustom_zh_CN,
};