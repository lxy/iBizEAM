import { ViewBase } from './ViewBase';

/**
 * 编辑视图基类
 *
 * @export
 * @class WFDynaEditViewBase
 * @extends {ViewBase}
 */
export class WFDynaEditViewBase extends ViewBase {

}