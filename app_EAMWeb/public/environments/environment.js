window.Environment = {
    // 是否为开发模式
    devMode: true,
    debugOpenMode:'mos',
    // 配置平台地址
    StudioUrl: "http://mos.ibizlab.cn/mos/",
    // 中心标识
    SlnId: "C7425DB4-CB88-4015-B3E7-5E5AEA2D45B5",
    // 系统标识
    SysId: "433D8BEB-6024-4E94-953C-3807DF994584",
    // 前端应用标识
    AppId: "113606170357cb61ec978e7a93742c7e",
    // 项目地址
    ProjectUrl: "https://gitee.com/ibizlab/iBizEAM"    
}
