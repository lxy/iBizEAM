package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMOutput;
import cn.ibizlab.eam.core.eam_core.filter.EMOutputSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMOutputService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMOutputMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[能力] 服务对象接口实现
 */
@Slf4j
@Service("EMOutputServiceImpl")
public class EMOutputServiceImpl extends ServiceImpl<EMOutputMapper, EMOutput> implements IEMOutputService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMOutputRctService emoutputrctService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMOutput et) {
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmoutputid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMOutput> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMOutput et) {
        emobjectService.update(emoutputInheritMapping.toEmobject(et));
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emoutputid",et.getEmoutputid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmoutputid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMOutput> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emobjectService.remove(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMOutput get(String key) {
        EMOutput et = getById(key);
        if(et==null){
            et=new EMOutput();
            et.setEmoutputid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMOutput getDraft(EMOutput et) {
        return et;
    }

    @Override
    public boolean checkKey(EMOutput et) {
        return (!ObjectUtils.isEmpty(et.getEmoutputid()))&&(!Objects.isNull(this.getById(et.getEmoutputid())));
    }
    @Override
    @Transactional
    public boolean save(EMOutput et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMOutput et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMOutput> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMOutput> list) {
        saveOrUpdateBatch(list,batchSize);
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMOutput> searchDefault(EMOutputSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMOutput> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMOutput>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }






    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMOutputInheritMapping emoutputInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMOutput et){
        if(ObjectUtils.isEmpty(et.getEmoutputid()))
            et.setEmoutputid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMObject emobject =emoutputInheritMapping.toEmobject(et);
        emobject.set("emobjecttype","OUTPUT");
        emobjectService.create(emobject);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMOutput> getEmoutputByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMOutput> getEmoutputByEntities(List<EMOutput> entities) {
        List ids =new ArrayList();
        for(EMOutput entity : entities){
            Serializable id=entity.getEmoutputid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



