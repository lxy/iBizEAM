package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMItem;
/**
 * 关系型数据实体[EMItem] 查询条件对象
 */
@Slf4j
@Data
public class EMItemSearchContext extends QueryWrapperContext<EMItem> {

	private String n_iteminfo_like;//[物品信息]
	public void setN_iteminfo_like(String n_iteminfo_like) {
        this.n_iteminfo_like = n_iteminfo_like;
        if(!ObjectUtils.isEmpty(this.n_iteminfo_like)){
            this.getSearchCond().like("iteminfo", n_iteminfo_like);
        }
    }
	private String n_abctype_eq;//[ABC分类]
	public void setN_abctype_eq(String n_abctype_eq) {
        this.n_abctype_eq = n_abctype_eq;
        if(!ObjectUtils.isEmpty(this.n_abctype_eq)){
            this.getSearchCond().eq("abctype", n_abctype_eq);
        }
    }
	private String n_isnew_eq;//[物品新旧标识]
	public void setN_isnew_eq(String n_isnew_eq) {
        this.n_isnew_eq = n_isnew_eq;
        if(!ObjectUtils.isEmpty(this.n_isnew_eq)){
            this.getSearchCond().eq("isnew", n_isnew_eq);
        }
    }
	private String n_lastaempname_eq;//[最新请购人]
	public void setN_lastaempname_eq(String n_lastaempname_eq) {
        this.n_lastaempname_eq = n_lastaempname_eq;
        if(!ObjectUtils.isEmpty(this.n_lastaempname_eq)){
            this.getSearchCond().eq("lastaempname", n_lastaempname_eq);
        }
    }
	private String n_lastaempname_like;//[最新请购人]
	public void setN_lastaempname_like(String n_lastaempname_like) {
        this.n_lastaempname_like = n_lastaempname_like;
        if(!ObjectUtils.isEmpty(this.n_lastaempname_like)){
            this.getSearchCond().like("lastaempname", n_lastaempname_like);
        }
    }
	private String n_empid_eq;//[最新采购员]
	public void setN_empid_eq(String n_empid_eq) {
        this.n_empid_eq = n_empid_eq;
        if(!ObjectUtils.isEmpty(this.n_empid_eq)){
            this.getSearchCond().eq("empid", n_empid_eq);
        }
    }
	private String n_sempid_eq;//[库管员]
	public void setN_sempid_eq(String n_sempid_eq) {
        this.n_sempid_eq = n_sempid_eq;
        if(!ObjectUtils.isEmpty(this.n_sempid_eq)){
            this.getSearchCond().eq("sempid", n_sempid_eq);
        }
    }
	private String n_emitemname_like;//[物品名称]
	public void setN_emitemname_like(String n_emitemname_like) {
        this.n_emitemname_like = n_emitemname_like;
        if(!ObjectUtils.isEmpty(this.n_emitemname_like)){
            this.getSearchCond().like("emitemname", n_emitemname_like);
        }
    }
	private String n_lastaempid_eq;//[最新请购人]
	public void setN_lastaempid_eq(String n_lastaempid_eq) {
        this.n_lastaempid_eq = n_lastaempid_eq;
        if(!ObjectUtils.isEmpty(this.n_lastaempid_eq)){
            this.getSearchCond().eq("lastaempid", n_lastaempid_eq);
        }
    }
	private String n_empname_eq;//[最新采购员]
	public void setN_empname_eq(String n_empname_eq) {
        this.n_empname_eq = n_empname_eq;
        if(!ObjectUtils.isEmpty(this.n_empname_eq)){
            this.getSearchCond().eq("empname", n_empname_eq);
        }
    }
	private String n_empname_like;//[最新采购员]
	public void setN_empname_like(String n_empname_like) {
        this.n_empname_like = n_empname_like;
        if(!ObjectUtils.isEmpty(this.n_empname_like)){
            this.getSearchCond().like("empname", n_empname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_sempname_eq;//[库管员]
	public void setN_sempname_eq(String n_sempname_eq) {
        this.n_sempname_eq = n_sempname_eq;
        if(!ObjectUtils.isEmpty(this.n_sempname_eq)){
            this.getSearchCond().eq("sempname", n_sempname_eq);
        }
    }
	private String n_sempname_like;//[库管员]
	public void setN_sempname_like(String n_sempname_like) {
        this.n_sempname_like = n_sempname_like;
        if(!ObjectUtils.isEmpty(this.n_sempname_like)){
            this.getSearchCond().like("sempname", n_sempname_like);
        }
    }
	private String n_storename_eq;//[最新存储仓库]
	public void setN_storename_eq(String n_storename_eq) {
        this.n_storename_eq = n_storename_eq;
        if(!ObjectUtils.isEmpty(this.n_storename_eq)){
            this.getSearchCond().eq("storename", n_storename_eq);
        }
    }
	private String n_storename_like;//[最新存储仓库]
	public void setN_storename_like(String n_storename_like) {
        this.n_storename_like = n_storename_like;
        if(!ObjectUtils.isEmpty(this.n_storename_like)){
            this.getSearchCond().like("storename", n_storename_like);
        }
    }
	private String n_unitname_eq;//[单位]
	public void setN_unitname_eq(String n_unitname_eq) {
        this.n_unitname_eq = n_unitname_eq;
        if(!ObjectUtils.isEmpty(this.n_unitname_eq)){
            this.getSearchCond().eq("unitname", n_unitname_eq);
        }
    }
	private String n_unitname_like;//[单位]
	public void setN_unitname_like(String n_unitname_like) {
        this.n_unitname_like = n_unitname_like;
        if(!ObjectUtils.isEmpty(this.n_unitname_like)){
            this.getSearchCond().like("unitname", n_unitname_like);
        }
    }
	private String n_emcabname_eq;//[货架]
	public void setN_emcabname_eq(String n_emcabname_eq) {
        this.n_emcabname_eq = n_emcabname_eq;
        if(!ObjectUtils.isEmpty(this.n_emcabname_eq)){
            this.getSearchCond().eq("emcabname", n_emcabname_eq);
        }
    }
	private String n_emcabname_like;//[货架]
	public void setN_emcabname_like(String n_emcabname_like) {
        this.n_emcabname_like = n_emcabname_like;
        if(!ObjectUtils.isEmpty(this.n_emcabname_like)){
            this.getSearchCond().like("emcabname", n_emcabname_like);
        }
    }
	private String n_storepartname_eq;//[最新存储库位]
	public void setN_storepartname_eq(String n_storepartname_eq) {
        this.n_storepartname_eq = n_storepartname_eq;
        if(!ObjectUtils.isEmpty(this.n_storepartname_eq)){
            this.getSearchCond().eq("storepartname", n_storepartname_eq);
        }
    }
	private String n_storepartname_like;//[最新存储库位]
	public void setN_storepartname_like(String n_storepartname_like) {
        this.n_storepartname_like = n_storepartname_like;
        if(!ObjectUtils.isEmpty(this.n_storepartname_like)){
            this.getSearchCond().like("storepartname", n_storepartname_like);
        }
    }
	private String n_labservicename_eq;//[建议供应商]
	public void setN_labservicename_eq(String n_labservicename_eq) {
        this.n_labservicename_eq = n_labservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_labservicename_eq)){
            this.getSearchCond().eq("labservicename", n_labservicename_eq);
        }
    }
	private String n_labservicename_like;//[建议供应商]
	public void setN_labservicename_like(String n_labservicename_like) {
        this.n_labservicename_like = n_labservicename_like;
        if(!ObjectUtils.isEmpty(this.n_labservicename_like)){
            this.getSearchCond().like("labservicename", n_labservicename_like);
        }
    }
	private String n_mservicename_eq;//[制造商]
	public void setN_mservicename_eq(String n_mservicename_eq) {
        this.n_mservicename_eq = n_mservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_mservicename_eq)){
            this.getSearchCond().eq("mservicename", n_mservicename_eq);
        }
    }
	private String n_mservicename_like;//[制造商]
	public void setN_mservicename_like(String n_mservicename_like) {
        this.n_mservicename_like = n_mservicename_like;
        if(!ObjectUtils.isEmpty(this.n_mservicename_like)){
            this.getSearchCond().like("mservicename", n_mservicename_like);
        }
    }
	private String n_acclassname_eq;//[总帐科目]
	public void setN_acclassname_eq(String n_acclassname_eq) {
        this.n_acclassname_eq = n_acclassname_eq;
        if(!ObjectUtils.isEmpty(this.n_acclassname_eq)){
            this.getSearchCond().eq("acclassname", n_acclassname_eq);
        }
    }
	private String n_acclassname_like;//[总帐科目]
	public void setN_acclassname_like(String n_acclassname_like) {
        this.n_acclassname_like = n_acclassname_like;
        if(!ObjectUtils.isEmpty(this.n_acclassname_like)){
            this.getSearchCond().like("acclassname", n_acclassname_like);
        }
    }
	private String n_itemtypename_eq;//[物品类型]
	public void setN_itemtypename_eq(String n_itemtypename_eq) {
        this.n_itemtypename_eq = n_itemtypename_eq;
        if(!ObjectUtils.isEmpty(this.n_itemtypename_eq)){
            this.getSearchCond().eq("itemtypename", n_itemtypename_eq);
        }
    }
	private String n_itemtypename_like;//[物品类型]
	public void setN_itemtypename_like(String n_itemtypename_like) {
        this.n_itemtypename_like = n_itemtypename_like;
        if(!ObjectUtils.isEmpty(this.n_itemtypename_like)){
            this.getSearchCond().like("itemtypename", n_itemtypename_like);
        }
    }
	private String n_emcabid_eq;//[货架]
	public void setN_emcabid_eq(String n_emcabid_eq) {
        this.n_emcabid_eq = n_emcabid_eq;
        if(!ObjectUtils.isEmpty(this.n_emcabid_eq)){
            this.getSearchCond().eq("emcabid", n_emcabid_eq);
        }
    }
	private String n_unitid_eq;//[单位]
	public void setN_unitid_eq(String n_unitid_eq) {
        this.n_unitid_eq = n_unitid_eq;
        if(!ObjectUtils.isEmpty(this.n_unitid_eq)){
            this.getSearchCond().eq("unitid", n_unitid_eq);
        }
    }
	private String n_storepartid_eq;//[最新存储库位]
	public void setN_storepartid_eq(String n_storepartid_eq) {
        this.n_storepartid_eq = n_storepartid_eq;
        if(!ObjectUtils.isEmpty(this.n_storepartid_eq)){
            this.getSearchCond().eq("storepartid", n_storepartid_eq);
        }
    }
	private String n_itemtypeid_eq;//[物品类型]
	public void setN_itemtypeid_eq(String n_itemtypeid_eq) {
        this.n_itemtypeid_eq = n_itemtypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_itemtypeid_eq)){
            this.getSearchCond().eq("itemtypeid", n_itemtypeid_eq);
        }
    }
	private String n_acclassid_eq;//[总帐科目]
	public void setN_acclassid_eq(String n_acclassid_eq) {
        this.n_acclassid_eq = n_acclassid_eq;
        if(!ObjectUtils.isEmpty(this.n_acclassid_eq)){
            this.getSearchCond().eq("acclassid", n_acclassid_eq);
        }
    }
	private String n_mserviceid_eq;//[制造商]
	public void setN_mserviceid_eq(String n_mserviceid_eq) {
        this.n_mserviceid_eq = n_mserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_mserviceid_eq)){
            this.getSearchCond().eq("mserviceid", n_mserviceid_eq);
        }
    }
	private String n_labserviceid_eq;//[建议供应商]
	public void setN_labserviceid_eq(String n_labserviceid_eq) {
        this.n_labserviceid_eq = n_labserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_labserviceid_eq)){
            this.getSearchCond().eq("labserviceid", n_labserviceid_eq);
        }
    }
	private String n_storeid_eq;//[最新存储仓库]
	public void setN_storeid_eq(String n_storeid_eq) {
        this.n_storeid_eq = n_storeid_eq;
        if(!ObjectUtils.isEmpty(this.n_storeid_eq)){
            this.getSearchCond().eq("storeid", n_storeid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emitemname", query)   
            );
		 }
	}
}



