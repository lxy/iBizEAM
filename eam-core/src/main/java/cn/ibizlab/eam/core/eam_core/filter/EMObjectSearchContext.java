package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMObject;
/**
 * 关系型数据实体[EMObject] 查询条件对象
 */
@Slf4j
@Data
public class EMObjectSearchContext extends QueryWrapperContext<EMObject> {

	private String n_objectinfo_like;//[对象信息]
	public void setN_objectinfo_like(String n_objectinfo_like) {
        this.n_objectinfo_like = n_objectinfo_like;
        if(!ObjectUtils.isEmpty(this.n_objectinfo_like)){
            this.getSearchCond().like("objectinfo", n_objectinfo_like);
        }
    }
	private String n_emobjecttype_eq;//[对象类型]
	public void setN_emobjecttype_eq(String n_emobjecttype_eq) {
        this.n_emobjecttype_eq = n_emobjecttype_eq;
        if(!ObjectUtils.isEmpty(this.n_emobjecttype_eq)){
            this.getSearchCond().eq("emobjecttype", n_emobjecttype_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emobjectname_like;//[对象名称]
	public void setN_emobjectname_like(String n_emobjectname_like) {
        this.n_emobjectname_like = n_emobjectname_like;
        if(!ObjectUtils.isEmpty(this.n_emobjectname_like)){
            this.getSearchCond().like("emobjectname", n_emobjectname_like);
        }
    }
	private String n_majorequipname_eq;//[主设备]
	public void setN_majorequipname_eq(String n_majorequipname_eq) {
        this.n_majorequipname_eq = n_majorequipname_eq;
        if(!ObjectUtils.isEmpty(this.n_majorequipname_eq)){
            this.getSearchCond().eq("majorequipname", n_majorequipname_eq);
        }
    }
	private String n_majorequipname_like;//[主设备]
	public void setN_majorequipname_like(String n_majorequipname_like) {
        this.n_majorequipname_like = n_majorequipname_like;
        if(!ObjectUtils.isEmpty(this.n_majorequipname_like)){
            this.getSearchCond().like("majorequipname", n_majorequipname_like);
        }
    }
	private String n_majorequipid_eq;//[主设备]
	public void setN_majorequipid_eq(String n_majorequipid_eq) {
        this.n_majorequipid_eq = n_majorequipid_eq;
        if(!ObjectUtils.isEmpty(this.n_majorequipid_eq)){
            this.getSearchCond().eq("majorequipid", n_majorequipid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emobjectname", query)   
            );
		 }
	}
}



