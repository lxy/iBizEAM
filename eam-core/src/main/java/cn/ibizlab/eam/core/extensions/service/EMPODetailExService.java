package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.service.impl.EMPODetailServiceImpl;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.eam.core.eam_core.domain.EMPODetail;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;
import java.util.*;

/**
 * 实体[订单条目] 自定义服务对象
 */
@Slf4j
@Primary
@Service("EMPODetailExService")
public class EMPODetailExService extends EMPODetailServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * 自定义行为[Check]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMPODetail check(EMPODetail et) {
        return super.check(et);
    }
    /**
     * 自定义行为[CreateRin]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMPODetail createRin(EMPODetail et) {
        return super.createRin(et);
    }
    /**
     * 自定义行为[GenId]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMPODetail genId(EMPODetail et) {
        return super.genId(et);
    }
}

