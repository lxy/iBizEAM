package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMItemPUse;
import cn.ibizlab.eam.core.eam_core.filter.EMItemPUseSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMItemPUse] 服务对象接口
 */
public interface IEMItemPUseService extends IService<EMItemPUse>{

    boolean create(EMItemPUse et) ;
    void createBatch(List<EMItemPUse> list) ;
    boolean update(EMItemPUse et) ;
    void updateBatch(List<EMItemPUse> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMItemPUse get(String key) ;
    EMItemPUse getDraft(EMItemPUse et) ;
    boolean checkKey(EMItemPUse et) ;
    EMItemPUse genId(EMItemPUse et) ;
    EMItemPUse issue(EMItemPUse et) ;
    boolean save(EMItemPUse et) ;
    void saveBatch(List<EMItemPUse> list) ;
    EMItemPUse submit(EMItemPUse et) ;
    Page<EMItemPUse> searchDefault(EMItemPUseSearchContext context) ;
    Page<EMItemPUse> searchDraft(EMItemPUseSearchContext context) ;
    Page<EMItemPUse> searchIssued(EMItemPUseSearchContext context) ;
    Page<EMItemPUse> searchWaitIssue(EMItemPUseSearchContext context) ;
    List<EMItemPUse> selectByEquipid(String emequipid) ;
    void removeByEquipid(String emequipid) ;
    List<EMItemPUse> selectByItemid(String emitemid) ;
    void removeByItemid(String emitemid) ;
    List<EMItemPUse> selectByObjid(String emobjectid) ;
    void removeByObjid(String emobjectid) ;
    List<EMItemPUse> selectByPurplanid(String empurplanid) ;
    void removeByPurplanid(String empurplanid) ;
    List<EMItemPUse> selectByLabserviceid(String emserviceid) ;
    void removeByLabserviceid(String emserviceid) ;
    List<EMItemPUse> selectByMserviceid(String emserviceid) ;
    void removeByMserviceid(String emserviceid) ;
    List<EMItemPUse> selectByStorepartid(String emstorepartid) ;
    void removeByStorepartid(String emstorepartid) ;
    List<EMItemPUse> selectByStoreid(String emstoreid) ;
    void removeByStoreid(String emstoreid) ;
    List<EMItemPUse> selectByWoid(String emwoid) ;
    void removeByWoid(String emwoid) ;
    List<EMItemPUse> selectByTeamid(String pfteamid) ;
    void removeByTeamid(String pfteamid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMItemPUse> getEmitempuseByIds(List<String> ids) ;
    List<EMItemPUse> getEmitempuseByEntities(List<EMItemPUse> entities) ;
}


