package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_INNER;
import cn.ibizlab.eam.core.eam_core.filter.EMWO_INNERSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMWO_INNERMapper extends BaseMapper<EMWO_INNER>{

    Page<EMWO_INNER> searchCalendar(IPage page, @Param("srf") EMWO_INNERSearchContext context, @Param("ew") Wrapper<EMWO_INNER> wrapper) ;
    Page<EMWO_INNER> searchDefault(IPage page, @Param("srf") EMWO_INNERSearchContext context, @Param("ew") Wrapper<EMWO_INNER> wrapper) ;
    @Override
    EMWO_INNER selectById(Serializable id);
    @Override
    int insert(EMWO_INNER entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMWO_INNER entity);
    @Override
    int update(@Param(Constants.ENTITY) EMWO_INNER entity, @Param("ew") Wrapper<EMWO_INNER> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMWO_INNER> selectByAcclassid(@Param("emacclassid") Serializable emacclassid) ;

    List<EMWO_INNER> selectByEmeitiresid(@Param("emeitiresid") Serializable emeitiresid) ;

    List<EMWO_INNER> selectByEmeqlctgssid(@Param("emeqlocationid") Serializable emeqlocationid) ;

    List<EMWO_INNER> selectByEmeqlcttiresid(@Param("emeqlocationid") Serializable emeqlocationid) ;

    List<EMWO_INNER> selectByEquipid(@Param("emequipid") Serializable emequipid) ;

    List<EMWO_INNER> selectByDpid(@Param("emobjectid") Serializable emobjectid) ;

    List<EMWO_INNER> selectByObjid(@Param("emobjectid") Serializable emobjectid) ;

    List<EMWO_INNER> selectByRfoacid(@Param("emrfoacid") Serializable emrfoacid) ;

    List<EMWO_INNER> selectByRfocaid(@Param("emrfocaid") Serializable emrfocaid) ;

    List<EMWO_INNER> selectByRfodeid(@Param("emrfodeid") Serializable emrfodeid) ;

    List<EMWO_INNER> selectByRfomoid(@Param("emrfomoid") Serializable emrfomoid) ;

    List<EMWO_INNER> selectByRserviceid(@Param("emserviceid") Serializable emserviceid) ;

    List<EMWO_INNER> selectByWooriid(@Param("emwooriid") Serializable emwooriid) ;

    List<EMWO_INNER> selectByWopid(@Param("emwoid") Serializable emwoid) ;

    List<EMWO_INNER> selectByRteamid(@Param("pfteamid") Serializable pfteamid) ;

}
