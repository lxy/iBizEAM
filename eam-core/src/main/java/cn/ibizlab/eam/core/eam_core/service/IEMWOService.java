package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMWO;
import cn.ibizlab.eam.core.eam_core.filter.EMWOSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMWO] 服务对象接口
 */
public interface IEMWOService extends IService<EMWO>{

    boolean create(EMWO et) ;
    void createBatch(List<EMWO> list) ;
    boolean update(EMWO et) ;
    void updateBatch(List<EMWO> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMWO get(String key) ;
    EMWO getDraft(EMWO et) ;
    boolean checkKey(EMWO et) ;
    boolean save(EMWO et) ;
    void saveBatch(List<EMWO> list) ;
    Page<EMWO> searchDefault(EMWOSearchContext context) ;
    Page<HashMap> searchEQYearWO(EMWOSearchContext context) ;
    Page<EMWO> searchIndexDER(EMWOSearchContext context) ;
    Page<HashMap> searchLaterThreeYear(EMWOSearchContext context) ;
    Page<HashMap> searchWoTypeNum(EMWOSearchContext context) ;
    Page<HashMap> searchYearWONumByPlan(EMWOSearchContext context) ;
    List<EMWO> selectByAcclassid(String emacclassid) ;
    void removeByAcclassid(String emacclassid) ;
    List<EMWO> selectByEquipid(String emequipid) ;
    void removeByEquipid(String emequipid) ;
    List<EMWO> selectByDpid(String emobjectid) ;
    void removeByDpid(String emobjectid) ;
    List<EMWO> selectByObjid(String emobjectid) ;
    void removeByObjid(String emobjectid) ;
    List<EMWO> selectByRfoacid(String emrfoacid) ;
    void removeByRfoacid(String emrfoacid) ;
    List<EMWO> selectByRfocaid(String emrfocaid) ;
    void removeByRfocaid(String emrfocaid) ;
    List<EMWO> selectByRfodeid(String emrfodeid) ;
    void removeByRfodeid(String emrfodeid) ;
    List<EMWO> selectByRfomoid(String emrfomoid) ;
    void removeByRfomoid(String emrfomoid) ;
    List<EMWO> selectByRserviceid(String emserviceid) ;
    void removeByRserviceid(String emserviceid) ;
    List<EMWO> selectByWooriid(String emwooriid) ;
    void removeByWooriid(String emwooriid) ;
    List<EMWO> selectByWopid(String emwoid) ;
    void removeByWopid(String emwoid) ;
    List<EMWO> selectByRteamid(String pfteamid) ;
    void removeByRteamid(String pfteamid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMWO> getEmwoByIds(List<String> ids) ;
    List<EMWO> getEmwoByEntities(List<EMWO> entities) ;
}


