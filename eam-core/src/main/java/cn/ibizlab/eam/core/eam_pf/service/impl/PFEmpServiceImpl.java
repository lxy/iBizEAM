package cn.ibizlab.eam.core.eam_pf.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_pf.domain.PFEmp;
import cn.ibizlab.eam.core.eam_pf.filter.PFEmpSearchContext;
import cn.ibizlab.eam.core.eam_pf.service.IPFEmpService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_pf.mapper.PFEmpMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[职员] 服务对象接口实现
 */
@Slf4j
@Service("PFEmpServiceImpl")
public class PFEmpServiceImpl extends ServiceImpl<PFEmpMapper, PFEmp> implements IPFEmpService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFTeamService pfteamService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(PFEmp et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getPfempid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<PFEmp> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(PFEmp et) {
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("pfempid",et.getPfempid())))
            return false;
        CachedBeanCopier.copy(get(et.getPfempid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<PFEmp> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public PFEmp get(String key) {
        PFEmp et = getById(key);
        if(et==null){
            et=new PFEmp();
            et.setPfempid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public PFEmp getDraft(PFEmp et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(PFEmp et) {
        return (!ObjectUtils.isEmpty(et.getPfempid()))&&(!Objects.isNull(this.getById(et.getPfempid())));
    }
    @Override
    @Transactional
    public boolean save(PFEmp et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(PFEmp et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<PFEmp> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<PFEmp> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<PFEmp> selectByMajorteamid(String pfteamid) {
        return baseMapper.selectByMajorteamid(pfteamid);
    }

    @Override
    public void removeByMajorteamid(String pfteamid) {
        this.remove(new QueryWrapper<PFEmp>().eq("majorteamid",pfteamid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<PFEmp> searchDefault(PFEmpSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<PFEmp> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<PFEmp>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 部门下职员
     */
    @Override
    public Page<PFEmp> searchDeptEmp(PFEmpSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<PFEmp> pages=baseMapper.searchDeptEmp(context.getPages(),context,context.getSelectCond());
        return new PageImpl<PFEmp>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(PFEmp et){
        //实体关系[DER1N_PFEMP_PFTEAM_MAJORTEAMID]
        if(!ObjectUtils.isEmpty(et.getMajorteamid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorteam=et.getMajorteam();
            if(ObjectUtils.isEmpty(majorteam)){
                cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorEntity=pfteamService.get(et.getMajorteamid());
                et.setMajorteam(majorEntity);
                majorteam=majorEntity;
            }
            et.setMajorteamname(majorteam.getPfteamname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<PFEmp> getPfempByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<PFEmp> getPfempByEntities(List<PFEmp> entities) {
        List ids =new ArrayList();
        for(PFEmp entity : entities){
            Serializable id=entity.getPfempid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



