package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMItemType;
import cn.ibizlab.eam.core.eam_core.filter.EMItemTypeSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMItemType] 服务对象接口
 */
public interface IEMItemTypeService extends IService<EMItemType>{

    boolean create(EMItemType et) ;
    void createBatch(List<EMItemType> list) ;
    boolean update(EMItemType et) ;
    void updateBatch(List<EMItemType> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMItemType get(String key) ;
    EMItemType getDraft(EMItemType et) ;
    boolean checkKey(EMItemType et) ;
    boolean save(EMItemType et) ;
    void saveBatch(List<EMItemType> list) ;
    Page<EMItemType> searchChildAll(EMItemTypeSearchContext context) ;
    Page<EMItemType> searchDefault(EMItemTypeSearchContext context) ;
    Page<EMItemType> searchRoot(EMItemTypeSearchContext context) ;
    List<EMItemType> selectByItembtypeid(String emitemtypeid) ;
    void removeByItembtypeid(String emitemtypeid) ;
    List<EMItemType> selectByItemmtypeid(String emitemtypeid) ;
    void removeByItemmtypeid(String emitemtypeid) ;
    List<EMItemType> selectByItemtypepid(String emitemtypeid) ;
    void removeByItemtypepid(String emitemtypeid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMItemType> getEmitemtypeByIds(List<String> ids) ;
    List<EMItemType> getEmitemtypeByEntities(List<EMItemType> entities) ;
}


