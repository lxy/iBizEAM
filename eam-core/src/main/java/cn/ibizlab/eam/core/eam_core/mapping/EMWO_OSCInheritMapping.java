

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_OSC;
import cn.ibizlab.eam.core.eam_core.domain.EMResRefObj;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMWO_OSCInheritMapping {

    @Mappings({
        @Mapping(source ="emwoOscid",target = "emresrefobjid"),
        @Mapping(source ="emwoOscname",target = "emresrefobjname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="wopid",target = "resrefobjpid"),
    })
    EMResRefObj toEmresrefobj(EMWO_OSC emwo_osc);

    @Mappings({
        @Mapping(source ="emresrefobjid" ,target = "emwoOscid"),
        @Mapping(source ="emresrefobjname" ,target = "emwoOscname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="resrefobjpid",target = "wopid"),
    })
    EMWO_OSC toEmwoOsc(EMResRefObj emresrefobj);

    List<EMResRefObj> toEmresrefobj(List<EMWO_OSC> emwo_osc);

    List<EMWO_OSC> toEmwoOsc(List<EMResRefObj> emresrefobj);

}


