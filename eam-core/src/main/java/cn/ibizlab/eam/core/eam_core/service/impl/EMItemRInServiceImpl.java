package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMItemRIn;
import cn.ibizlab.eam.core.eam_core.filter.EMItemRInSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMItemRInService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMItemRInMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[入库单] 服务对象接口实现
 */
@Slf4j
@Service("EMItemRInServiceImpl")
public class EMItemRInServiceImpl extends ServiceImpl<EMItemRInMapper, EMItemRIn> implements IEMItemRInService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemCSService emitemcsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPLService emitemplService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemROutService emitemroutService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPODetailService empodetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMServiceService emserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMStorePartService emstorepartService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMStoreService emstoreService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMItemRIn et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmitemrinid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMItemRIn> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMItemRIn et) {
        fillParentData(et);
        emitemtradeService.update(emitemrinInheritMapping.toEmitemtrade(et));
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emitemrinid",et.getEmitemrinid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmitemrinid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMItemRIn> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emitemtradeService.remove(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMItemRIn get(String key) {
        EMItemRIn et = getById(key);
        if(et==null){
            et=new EMItemRIn();
            et.setEmitemrinid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMItemRIn getDraft(EMItemRIn et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMItemRIn et) {
        return (!ObjectUtils.isEmpty(et.getEmitemrinid()))&&(!Objects.isNull(this.getById(et.getEmitemrinid())));
    }
    @Override
    @Transactional
    public EMItemRIn confirm(EMItemRIn et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public EMItemRIn genId(EMItemRIn et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean save(EMItemRIn et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMItemRIn et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMItemRIn> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMItemRIn> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMItemRIn> selectByItemid(String emitemid) {
        return baseMapper.selectByItemid(emitemid);
    }

    @Override
    public void removeByItemid(String emitemid) {
        this.remove(new QueryWrapper<EMItemRIn>().eq("itemid",emitemid));
    }

	@Override
    public List<EMItemRIn> selectByPodetailid(String empodetailid) {
        return baseMapper.selectByPodetailid(empodetailid);
    }

    @Override
    public void removeByPodetailid(String empodetailid) {
        this.remove(new QueryWrapper<EMItemRIn>().eq("podetailid",empodetailid));
    }

	@Override
    public List<EMItemRIn> selectByEmserviceid(String emserviceid) {
        return baseMapper.selectByEmserviceid(emserviceid);
    }

    @Override
    public void removeByEmserviceid(String emserviceid) {
        this.remove(new QueryWrapper<EMItemRIn>().eq("emserviceid",emserviceid));
    }

	@Override
    public List<EMItemRIn> selectByStorepartid(String emstorepartid) {
        return baseMapper.selectByStorepartid(emstorepartid);
    }

    @Override
    public void removeByStorepartid(String emstorepartid) {
        this.remove(new QueryWrapper<EMItemRIn>().eq("storepartid",emstorepartid));
    }

	@Override
    public List<EMItemRIn> selectByStoreid(String emstoreid) {
        return baseMapper.selectByStoreid(emstoreid);
    }

    @Override
    public void removeByStoreid(String emstoreid) {
        this.remove(new QueryWrapper<EMItemRIn>().eq("storeid",emstoreid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMItemRIn> searchDefault(EMItemRInSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemRIn> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemRIn>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 已入库
     */
    @Override
    public Page<EMItemRIn> searchPutIn(EMItemRInSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemRIn> pages=baseMapper.searchPutIn(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemRIn>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 待入库
     */
    @Override
    public Page<EMItemRIn> searchWaitIn(EMItemRInSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemRIn> pages=baseMapper.searchWaitIn(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemRIn>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMItemRIn et){
        //实体关系[DER1N_EMITEMRIN_EMITEM_ITEMID]
        if(!ObjectUtils.isEmpty(et.getItemid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItem item=et.getItem();
            if(ObjectUtils.isEmpty(item)){
                cn.ibizlab.eam.core.eam_core.domain.EMItem majorEntity=emitemService.get(et.getItemid());
                et.setItem(majorEntity);
                item=majorEntity;
            }
            et.setItemcode(item.getItemcode());
            et.setUnitid(item.getUnitid());
            et.setUnitname(item.getUnitname());
            et.setItemname(item.getEmitemname());
            et.setItemtypename(item.getIteminfo());
            et.setAvgprice(item.getPrice());
            et.setItembtypeid(item.getItembtypeid());
        }
        //实体关系[DER1N_EMITEMRIN_EMPODETAIL_PODETAILID]
        if(!ObjectUtils.isEmpty(et.getPodetailid())){
            cn.ibizlab.eam.core.eam_core.domain.EMPODetail podetail=et.getPodetail();
            if(ObjectUtils.isEmpty(podetail)){
                cn.ibizlab.eam.core.eam_core.domain.EMPODetail majorEntity=empodetailService.get(et.getPodetailid());
                et.setPodetail(majorEntity);
                podetail=majorEntity;
            }
            et.setTeamid(podetail.getTeamid());
            et.setLabservicename(podetail.getLabservicename());
            et.setCivo(podetail.getCivo());
            et.setPoid(podetail.getPoid());
            et.setAvgtsfee(podetail.getAvgtsfee());
            et.setWplistid(podetail.getWplistid());
            et.setPodetailname(podetail.getEmpodetailname());
            et.setDeptid(podetail.getDeptid());
            et.setAempid(podetail.getAempid());
            et.setPorempname(podetail.getPorempname());
            et.setShf(podetail.getShf());
            et.setLabserviceid(podetail.getLabserviceid());
        }
        //实体关系[DER1N_EMITEMRIN_EMSERVICE_EMSERVICEID]
        if(!ObjectUtils.isEmpty(et.getEmserviceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService emservice=et.getEmservice();
            if(ObjectUtils.isEmpty(emservice)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getEmserviceid());
                et.setEmservice(majorEntity);
                emservice=majorEntity;
            }
            et.setEmservicename(emservice.getEmservicename());
        }
        //实体关系[DER1N_EMITEMRIN_EMSTOREPART_STOREPARTID]
        if(!ObjectUtils.isEmpty(et.getStorepartid())){
            cn.ibizlab.eam.core.eam_core.domain.EMStorePart storepart=et.getStorepart();
            if(ObjectUtils.isEmpty(storepart)){
                cn.ibizlab.eam.core.eam_core.domain.EMStorePart majorEntity=emstorepartService.get(et.getStorepartid());
                et.setStorepart(majorEntity);
                storepart=majorEntity;
            }
            et.setStorepartname(storepart.getEmstorepartname());
        }
        //实体关系[DER1N_EMITEMRIN_EMSTORE_STOREID]
        if(!ObjectUtils.isEmpty(et.getStoreid())){
            cn.ibizlab.eam.core.eam_core.domain.EMStore store=et.getStore();
            if(ObjectUtils.isEmpty(store)){
                cn.ibizlab.eam.core.eam_core.domain.EMStore majorEntity=emstoreService.get(et.getStoreid());
                et.setStore(majorEntity);
                store=majorEntity;
            }
            et.setStorename(store.getEmstorename());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMItemRInInheritMapping emitemrinInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemTradeService emitemtradeService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMItemRIn et){
        if(ObjectUtils.isEmpty(et.getEmitemrinid()))
            et.setEmitemrinid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMItemTrade emitemtrade =emitemrinInheritMapping.toEmitemtrade(et);
        emitemtrade.set("emitemtradetype","RIN");
        emitemtradeService.create(emitemtrade);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMItemRIn> getEmitemrinByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMItemRIn> getEmitemrinByEntities(List<EMItemRIn> entities) {
        List ids =new ArrayList();
        for(EMItemRIn entity : entities){
            Serializable id=entity.getEmitemrinid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



