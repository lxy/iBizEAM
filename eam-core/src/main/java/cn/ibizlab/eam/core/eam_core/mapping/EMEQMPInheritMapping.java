

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQMP;
import cn.ibizlab.eam.core.eam_core.domain.EMObject;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQMPInheritMapping {

    @Mappings({
        @Mapping(source ="emeqmpid",target = "emobjectid"),
        @Mapping(source ="emeqmpname",target = "emobjectname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="mpcode",target = "objectcode"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObject toEmobject(EMEQMP emeqmp);

    @Mappings({
        @Mapping(source ="emobjectid" ,target = "emeqmpid"),
        @Mapping(source ="emobjectname" ,target = "emeqmpname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objectcode",target = "mpcode"),
    })
    EMEQMP toEmeqmp(EMObject emobject);

    List<EMObject> toEmobject(List<EMEQMP> emeqmp);

    List<EMEQMP> toEmeqmp(List<EMObject> emobject);

}


