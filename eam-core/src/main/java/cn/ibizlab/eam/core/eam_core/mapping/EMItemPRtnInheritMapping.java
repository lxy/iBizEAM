

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItemPRtn;
import cn.ibizlab.eam.core.eam_core.domain.EMItemTrade;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMItemPRtnInheritMapping {

    @Mappings({
        @Mapping(source ="emitemprtnid",target = "emitemtradeid"),
        @Mapping(source ="emitemprtnname",target = "emitemtradename"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMItemTrade toEmitemtrade(EMItemPRtn emitemprtn);

    @Mappings({
        @Mapping(source ="emitemtradeid" ,target = "emitemprtnid"),
        @Mapping(source ="emitemtradename" ,target = "emitemprtnname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMItemPRtn toEmitemprtn(EMItemTrade emitemtrade);

    List<EMItemTrade> toEmitemtrade(List<EMItemPRtn> emitemprtn);

    List<EMItemPRtn> toEmitemprtn(List<EMItemTrade> emitemtrade);

}


