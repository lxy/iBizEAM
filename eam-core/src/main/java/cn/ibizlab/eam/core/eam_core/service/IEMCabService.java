package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMCab;
import cn.ibizlab.eam.core.eam_core.filter.EMCabSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMCab] 服务对象接口
 */
public interface IEMCabService extends IService<EMCab>{

    boolean create(EMCab et) ;
    void createBatch(List<EMCab> list) ;
    boolean update(EMCab et) ;
    void updateBatch(List<EMCab> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMCab get(String key) ;
    EMCab getDraft(EMCab et) ;
    boolean checkKey(EMCab et) ;
    boolean save(EMCab et) ;
    void saveBatch(List<EMCab> list) ;
    Page<EMCab> searchDefault(EMCabSearchContext context) ;
    List<EMCab> selectByEmstorepartid(String emstorepartid) ;
    void removeByEmstorepartid(String emstorepartid) ;
    List<EMCab> selectByEmstoreid(String emstoreid) ;
    void removeByEmstoreid(String emstoreid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMCab> getEmcabByIds(List<String> ids) ;
    List<EMCab> getEmcabByEntities(List<EMCab> entities) ;
}


