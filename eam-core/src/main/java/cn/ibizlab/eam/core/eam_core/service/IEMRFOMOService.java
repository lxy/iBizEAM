package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMRFOMO;
import cn.ibizlab.eam.core.eam_core.filter.EMRFOMOSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMRFOMO] 服务对象接口
 */
public interface IEMRFOMOService extends IService<EMRFOMO>{

    boolean create(EMRFOMO et) ;
    void createBatch(List<EMRFOMO> list) ;
    boolean update(EMRFOMO et) ;
    void updateBatch(List<EMRFOMO> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMRFOMO get(String key) ;
    EMRFOMO getDraft(EMRFOMO et) ;
    boolean checkKey(EMRFOMO et) ;
    boolean save(EMRFOMO et) ;
    void saveBatch(List<EMRFOMO> list) ;
    Page<EMRFOMO> searchDefault(EMRFOMOSearchContext context) ;
    List<EMRFOMO> selectByRfodeid(String emrfodeid) ;
    void removeByRfodeid(String emrfodeid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMRFOMO> getEmrfomoByIds(List<String> ids) ;
    List<EMRFOMO> getEmrfomoByEntities(List<EMRFOMO> entities) ;
}


