package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMItemCS;
import cn.ibizlab.eam.core.eam_core.filter.EMItemCSSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMItemCS] 服务对象接口
 */
public interface IEMItemCSService extends IService<EMItemCS>{

    boolean create(EMItemCS et) ;
    void createBatch(List<EMItemCS> list) ;
    boolean update(EMItemCS et) ;
    void updateBatch(List<EMItemCS> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMItemCS get(String key) ;
    EMItemCS getDraft(EMItemCS et) ;
    boolean checkKey(EMItemCS et) ;
    boolean save(EMItemCS et) ;
    void saveBatch(List<EMItemCS> list) ;
    Page<EMItemCS> searchConfirmed(EMItemCSSearchContext context) ;
    Page<EMItemCS> searchDefault(EMItemCSSearchContext context) ;
    Page<EMItemCS> searchDraft(EMItemCSSearchContext context) ;
    Page<EMItemCS> searchToConfirm(EMItemCSSearchContext context) ;
    List<EMItemCS> selectByRid(String emitemrinid) ;
    void removeByRid(String emitemrinid) ;
    List<EMItemCS> selectByItemid(String emitemid) ;
    void removeByItemid(String emitemid) ;
    List<EMItemCS> selectByStockid(String emstockid) ;
    void removeByStockid(String emstockid) ;
    List<EMItemCS> selectByStorepartid(String emstorepartid) ;
    void removeByStorepartid(String emstorepartid) ;
    List<EMItemCS> selectByStoreid(String emstoreid) ;
    void removeByStoreid(String emstoreid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMItemCS> getEmitemcsByIds(List<String> ids) ;
    List<EMItemCS> getEmitemcsByEntities(List<EMItemCS> entities) ;
}


