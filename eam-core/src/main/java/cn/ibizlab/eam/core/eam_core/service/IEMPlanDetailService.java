package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMPlanDetail;
import cn.ibizlab.eam.core.eam_core.filter.EMPlanDetailSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMPlanDetail] 服务对象接口
 */
public interface IEMPlanDetailService extends IService<EMPlanDetail>{

    boolean create(EMPlanDetail et) ;
    void createBatch(List<EMPlanDetail> list) ;
    boolean update(EMPlanDetail et) ;
    void updateBatch(List<EMPlanDetail> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMPlanDetail get(String key) ;
    EMPlanDetail getDraft(EMPlanDetail et) ;
    boolean checkKey(EMPlanDetail et) ;
    boolean save(EMPlanDetail et) ;
    void saveBatch(List<EMPlanDetail> list) ;
    Page<EMPlanDetail> searchDefault(EMPlanDetailSearchContext context) ;
    List<EMPlanDetail> selectByEquipid(String emequipid) ;
    void removeByEquipid(String emequipid) ;
    List<EMPlanDetail> selectByDpid(String emobjectid) ;
    void removeByDpid(String emobjectid) ;
    List<EMPlanDetail> selectByObjid(String emobjectid) ;
    void removeByObjid(String emobjectid) ;
    List<EMPlanDetail> selectByPlanid(String emplanid) ;
    void removeByPlanid(String emplanid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMPlanDetail> getEmplandetailByIds(List<String> ids) ;
    List<EMPlanDetail> getEmplandetailByEntities(List<EMPlanDetail> entities) ;
}


