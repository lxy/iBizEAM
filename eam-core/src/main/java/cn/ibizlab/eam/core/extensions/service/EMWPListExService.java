package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.service.impl.EMWPListServiceImpl;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.eam.core.eam_core.domain.EMWPList;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;
import java.util.*;

/**
 * 实体[采购申请] 自定义服务对象
 */
@Slf4j
@Primary
@Service("EMWPListExService")
public class EMWPListExService extends EMWPListServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * 自定义行为[FillCosted]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMWPList fillCosted(EMWPList et) {
        return super.fillCosted(et);
    }
    /**
     * 自定义行为[GenId]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMWPList genId(EMWPList et) {
        return super.genId(et);
    }
    /**
     * 自定义行为[GenPO]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMWPList genPO(EMWPList et) {
        return super.genPO(et);
    }
    /**
     * 自定义行为[Submit]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMWPList submit(EMWPList et) {
        return super.submit(et);
    }
}

