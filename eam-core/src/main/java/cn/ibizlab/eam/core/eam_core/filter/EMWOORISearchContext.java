package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMWOORI;
/**
 * 关系型数据实体[EMWOORI] 查询条件对象
 */
@Slf4j
@Data
public class EMWOORISearchContext extends QueryWrapperContext<EMWOORI> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emwooriname_like;//[工单来源名称]
	public void setN_emwooriname_like(String n_emwooriname_like) {
        this.n_emwooriname_like = n_emwooriname_like;
        if(!ObjectUtils.isEmpty(this.n_emwooriname_like)){
            this.getSearchCond().like("emwooriname", n_emwooriname_like);
        }
    }
	private String n_wooriinfo_like;//[工单来源信息]
	public void setN_wooriinfo_like(String n_wooriinfo_like) {
        this.n_wooriinfo_like = n_wooriinfo_like;
        if(!ObjectUtils.isEmpty(this.n_wooriinfo_like)){
            this.getSearchCond().like("wooriinfo", n_wooriinfo_like);
        }
    }
	private String n_emwooritype_eq;//[来源类型]
	public void setN_emwooritype_eq(String n_emwooritype_eq) {
        this.n_emwooritype_eq = n_emwooritype_eq;
        if(!ObjectUtils.isEmpty(this.n_emwooritype_eq)){
            this.getSearchCond().eq("emwooritype", n_emwooritype_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emwooriname", query)   
            );
		 }
	}
}



