package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQKPMap;
import cn.ibizlab.eam.core.eam_core.filter.EMEQKPMapSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQKPMapService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQKPMapMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[设备关键点关系] 服务对象接口实现
 */
@Slf4j
@Service("EMEQKPMapServiceImpl")
public class EMEQKPMapServiceImpl extends ServiceImpl<EMEQKPMapMapper, EMEQKPMap> implements IEMEQKPMapService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQKPService emeqkpService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQKPMap et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmeqkpmapid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQKPMap> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQKPMap et) {
        fillParentData(et);
        emobjmapService.update(emeqkpmapInheritMapping.toEmobjmap(et));
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emeqkpmapid",et.getEmeqkpmapid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmeqkpmapid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQKPMap> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emobjmapService.remove(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQKPMap get(String key) {
        EMEQKPMap et = getById(key);
        if(et==null){
            et=new EMEQKPMap();
            et.setEmeqkpmapid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMEQKPMap getDraft(EMEQKPMap et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEQKPMap et) {
        return (!ObjectUtils.isEmpty(et.getEmeqkpmapid()))&&(!Objects.isNull(this.getById(et.getEmeqkpmapid())));
    }
    @Override
    @Transactional
    public boolean save(EMEQKPMap et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQKPMap et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQKPMap> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQKPMap> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMEQKPMap> selectByKpid(String emeqkpid) {
        return baseMapper.selectByKpid(emeqkpid);
    }

    @Override
    public void removeByKpid(String emeqkpid) {
        this.remove(new QueryWrapper<EMEQKPMap>().eq("kpid",emeqkpid));
    }

	@Override
    public List<EMEQKPMap> selectByRefobjid(String emobjectid) {
        return baseMapper.selectByRefobjid(emobjectid);
    }

    @Override
    public void removeByRefobjid(String emobjectid) {
        this.remove(new QueryWrapper<EMEQKPMap>().eq("refobjid",emobjectid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQKPMap> searchDefault(EMEQKPMapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQKPMap> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQKPMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEQKPMap et){
        //实体关系[DER1N_EMEQKPMAP_EMEQKP_KPID]
        if(!ObjectUtils.isEmpty(et.getKpid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEQKP kp=et.getKp();
            if(ObjectUtils.isEmpty(kp)){
                cn.ibizlab.eam.core.eam_core.domain.EMEQKP majorEntity=emeqkpService.get(et.getKpid());
                et.setKp(majorEntity);
                kp=majorEntity;
            }
            et.setKpname(kp.getEmeqkpname());
        }
        //实体关系[DER1N_EMEQKPMAP_EMOBJECT_REFOBJID]
        if(!ObjectUtils.isEmpty(et.getRefobjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject refobj=et.getRefobj();
            if(ObjectUtils.isEmpty(refobj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getRefobjid());
                et.setRefobj(majorEntity);
                refobj=majorEntity;
            }
            et.setRefobjname(refobj.getEmobjectname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMEQKPMapInheritMapping emeqkpmapInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjMapService emobjmapService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMEQKPMap et){
        if(ObjectUtils.isEmpty(et.getEmeqkpmapid()))
            et.setEmeqkpmapid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMObjMap emobjmap =emeqkpmapInheritMapping.toEmobjmap(et);
        emobjmap.set("emobjmaptype","EQKPMAP");
        emobjmapService.create(emobjmap);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQKPMap> getEmeqkpmapByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQKPMap> getEmeqkpmapByEntities(List<EMEQKPMap> entities) {
        List ids =new ArrayList();
        for(EMEQKPMap entity : entities){
            Serializable id=entity.getEmeqkpmapid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



