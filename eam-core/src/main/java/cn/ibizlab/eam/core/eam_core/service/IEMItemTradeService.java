package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMItemTrade;
import cn.ibizlab.eam.core.eam_core.filter.EMItemTradeSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMItemTrade] 服务对象接口
 */
public interface IEMItemTradeService extends IService<EMItemTrade>{

    boolean create(EMItemTrade et) ;
    void createBatch(List<EMItemTrade> list) ;
    boolean update(EMItemTrade et) ;
    void updateBatch(List<EMItemTrade> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMItemTrade get(String key) ;
    EMItemTrade getDraft(EMItemTrade et) ;
    boolean checkKey(EMItemTrade et) ;
    boolean save(EMItemTrade et) ;
    void saveBatch(List<EMItemTrade> list) ;
    Page<EMItemTrade> searchDefault(EMItemTradeSearchContext context) ;
    Page<EMItemTrade> searchIndexDER(EMItemTradeSearchContext context) ;
    Page<HashMap> searchYearNumByItem(EMItemTradeSearchContext context) ;
    List<EMItemTrade> selectByRid(String emitemtradeid) ;
    void removeByRid(String emitemtradeid) ;
    List<EMItemTrade> selectByItemid(String emitemid) ;
    void removeByItemid(String emitemid) ;
    List<EMItemTrade> selectByLabserviceid(String emserviceid) ;
    void removeByLabserviceid(String emserviceid) ;
    List<EMItemTrade> selectByStorepartid(String emstorepartid) ;
    void removeByStorepartid(String emstorepartid) ;
    List<EMItemTrade> selectByStoreid(String emstoreid) ;
    void removeByStoreid(String emstoreid) ;
    List<EMItemTrade> selectByTeamid(String pfteamid) ;
    void removeByTeamid(String pfteamid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMItemTrade> getEmitemtradeByIds(List<String> ids) ;
    List<EMItemTrade> getEmitemtradeByEntities(List<EMItemTrade> entities) ;
}


