package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMStore;
/**
 * 关系型数据实体[EMStore] 查询条件对象
 */
@Slf4j
@Data
public class EMStoreSearchContext extends QueryWrapperContext<EMStore> {

	private String n_storeinfo_like;//[仓库信息]
	public void setN_storeinfo_like(String n_storeinfo_like) {
        this.n_storeinfo_like = n_storeinfo_like;
        if(!ObjectUtils.isEmpty(this.n_storeinfo_like)){
            this.getSearchCond().like("storeinfo", n_storeinfo_like);
        }
    }
	private String n_newstoretypeid_eq;//[NEW仓库类型]
	public void setN_newstoretypeid_eq(String n_newstoretypeid_eq) {
        this.n_newstoretypeid_eq = n_newstoretypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_newstoretypeid_eq)){
            this.getSearchCond().eq("newstoretypeid", n_newstoretypeid_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_ioalgo_eq;//[出入算法]
	public void setN_ioalgo_eq(String n_ioalgo_eq) {
        this.n_ioalgo_eq = n_ioalgo_eq;
        if(!ObjectUtils.isEmpty(this.n_ioalgo_eq)){
            this.getSearchCond().eq("ioalgo", n_ioalgo_eq);
        }
    }
	private String n_empid_eq;//[库管员]
	public void setN_empid_eq(String n_empid_eq) {
        this.n_empid_eq = n_empid_eq;
        if(!ObjectUtils.isEmpty(this.n_empid_eq)){
            this.getSearchCond().eq("empid", n_empid_eq);
        }
    }
	private String n_storetypeid_eq;//[仓库类型]
	public void setN_storetypeid_eq(String n_storetypeid_eq) {
        this.n_storetypeid_eq = n_storetypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_storetypeid_eq)){
            this.getSearchCond().eq("storetypeid", n_storetypeid_eq);
        }
    }
	private String n_empname_eq;//[库管员]
	public void setN_empname_eq(String n_empname_eq) {
        this.n_empname_eq = n_empname_eq;
        if(!ObjectUtils.isEmpty(this.n_empname_eq)){
            this.getSearchCond().eq("empname", n_empname_eq);
        }
    }
	private String n_empname_like;//[库管员]
	public void setN_empname_like(String n_empname_like) {
        this.n_empname_like = n_empname_like;
        if(!ObjectUtils.isEmpty(this.n_empname_like)){
            this.getSearchCond().like("empname", n_empname_like);
        }
    }
	private String n_emstorename_like;//[仓库名称]
	public void setN_emstorename_like(String n_emstorename_like) {
        this.n_emstorename_like = n_emstorename_like;
        if(!ObjectUtils.isEmpty(this.n_emstorename_like)){
            this.getSearchCond().like("emstorename", n_emstorename_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emstorename", query)   
            );
		 }
	}
}



