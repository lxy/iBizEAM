package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMRigging;
/**
 * 关系型数据实体[EMRigging] 查询条件对象
 */
@Slf4j
@Data
public class EMRiggingSearchContext extends QueryWrapperContext<EMRigging> {

	private String n_organization_eq;//[组织]
	public void setN_organization_eq(String n_organization_eq) {
        this.n_organization_eq = n_organization_eq;
        if(!ObjectUtils.isEmpty(this.n_organization_eq)){
            this.getSearchCond().eq("organization", n_organization_eq);
        }
    }
	private String n_emriggingname_like;//[工索具名称]
	public void setN_emriggingname_like(String n_emriggingname_like) {
        this.n_emriggingname_like = n_emriggingname_like;
        if(!ObjectUtils.isEmpty(this.n_emriggingname_like)){
            this.getSearchCond().like("emriggingname", n_emriggingname_like);
        }
    }
	private String n_emitempusename_eq;//[领料单]
	public void setN_emitempusename_eq(String n_emitempusename_eq) {
        this.n_emitempusename_eq = n_emitempusename_eq;
        if(!ObjectUtils.isEmpty(this.n_emitempusename_eq)){
            this.getSearchCond().eq("emitempusename", n_emitempusename_eq);
        }
    }
	private String n_emitempusename_like;//[领料单]
	public void setN_emitempusename_like(String n_emitempusename_like) {
        this.n_emitempusename_like = n_emitempusename_like;
        if(!ObjectUtils.isEmpty(this.n_emitempusename_like)){
            this.getSearchCond().like("emitempusename", n_emitempusename_like);
        }
    }
	private String n_emitempuseid_eq;//[领料单]
	public void setN_emitempuseid_eq(String n_emitempuseid_eq) {
        this.n_emitempuseid_eq = n_emitempuseid_eq;
        if(!ObjectUtils.isEmpty(this.n_emitempuseid_eq)){
            this.getSearchCond().eq("emitempuseid", n_emitempuseid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emriggingname", query)   
            );
		 }
	}
}



