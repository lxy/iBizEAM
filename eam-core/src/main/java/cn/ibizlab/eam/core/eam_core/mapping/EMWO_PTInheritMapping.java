

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_PT;
import cn.ibizlab.eam.core.eam_core.domain.EMResRefObj;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMWO_PTInheritMapping {

    @Mappings({
        @Mapping(source ="emwoPtid",target = "emresrefobjid"),
        @Mapping(source ="emwoPtname",target = "emresrefobjname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="wopid",target = "resrefobjpid"),
    })
    EMResRefObj toEmresrefobj(EMWO_PT emwo_pt);

    @Mappings({
        @Mapping(source ="emresrefobjid" ,target = "emwoPtid"),
        @Mapping(source ="emresrefobjname" ,target = "emwoPtname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="resrefobjpid",target = "wopid"),
    })
    EMWO_PT toEmwoPt(EMResRefObj emresrefobj);

    List<EMResRefObj> toEmresrefobj(List<EMWO_PT> emwo_pt);

    List<EMWO_PT> toEmwoPt(List<EMResRefObj> emresrefobj);

}


