package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMRigging;
import cn.ibizlab.eam.core.eam_core.filter.EMRiggingSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMRigging] 服务对象接口
 */
public interface IEMRiggingService extends IService<EMRigging>{

    boolean create(EMRigging et) ;
    void createBatch(List<EMRigging> list) ;
    boolean update(EMRigging et) ;
    void updateBatch(List<EMRigging> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMRigging get(String key) ;
    EMRigging getDraft(EMRigging et) ;
    boolean checkKey(EMRigging et) ;
    boolean save(EMRigging et) ;
    void saveBatch(List<EMRigging> list) ;
    Page<EMRigging> searchDefault(EMRiggingSearchContext context) ;
    List<EMRigging> selectByEmitempuseid(String emitempuseid) ;
    void removeByEmitempuseid(String emitempuseid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMRigging> getEmriggingByIds(List<String> ids) ;
    List<EMRigging> getEmriggingByEntities(List<EMRigging> entities) ;
}


