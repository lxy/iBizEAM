package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMBidding;
import cn.ibizlab.eam.core.eam_core.filter.EMBiddingSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMBiddingService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMBiddingMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[项目招投标] 服务对象接口实现
 */
@Slf4j
@Service("EMBiddingServiceImpl")
public class EMBiddingServiceImpl extends ServiceImpl<EMBiddingMapper, EMBidding> implements IEMBiddingService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMServiceService emserviceService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMBidding et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmbiddingid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMBidding> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMBidding et) {
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("embiddingid",et.getEmbiddingid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmbiddingid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMBidding> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMBidding get(String key) {
        EMBidding et = getById(key);
        if(et==null){
            et=new EMBidding();
            et.setEmbiddingid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMBidding getDraft(EMBidding et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMBidding et) {
        return (!ObjectUtils.isEmpty(et.getEmbiddingid()))&&(!Objects.isNull(this.getById(et.getEmbiddingid())));
    }
    @Override
    @Transactional
    public boolean save(EMBidding et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMBidding et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMBidding> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMBidding> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMBidding> selectByEmserviceid(String emserviceid) {
        return baseMapper.selectByEmserviceid(emserviceid);
    }

    @Override
    public void removeByEmserviceid(String emserviceid) {
        this.remove(new QueryWrapper<EMBidding>().eq("emserviceid",emserviceid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMBidding> searchDefault(EMBiddingSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMBidding> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMBidding>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMBidding et){
        //实体关系[DER1N_EMBIDDING_EMSERVICE_EMSERVICEID]
        if(!ObjectUtils.isEmpty(et.getEmserviceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService emservice=et.getEmservice();
            if(ObjectUtils.isEmpty(emservice)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getEmserviceid());
                et.setEmservice(majorEntity);
                emservice=majorEntity;
            }
            et.setEmservicename(emservice.getEmservicename());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMBidding> getEmbiddingByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMBidding> getEmbiddingByEntities(List<EMBidding> entities) {
        List ids =new ArrayList();
        for(EMBidding entity : entities){
            Serializable id=entity.getEmbiddingid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



