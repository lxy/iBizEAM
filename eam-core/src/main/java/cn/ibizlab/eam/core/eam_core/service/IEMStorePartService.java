package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMStorePart;
import cn.ibizlab.eam.core.eam_core.filter.EMStorePartSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMStorePart] 服务对象接口
 */
public interface IEMStorePartService extends IService<EMStorePart>{

    boolean create(EMStorePart et) ;
    void createBatch(List<EMStorePart> list) ;
    boolean update(EMStorePart et) ;
    void updateBatch(List<EMStorePart> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMStorePart get(String key) ;
    EMStorePart getDraft(EMStorePart et) ;
    boolean checkKey(EMStorePart et) ;
    boolean save(EMStorePart et) ;
    void saveBatch(List<EMStorePart> list) ;
    Page<EMStorePart> searchDefault(EMStorePartSearchContext context) ;
    List<EMStorePart> selectByStoreid(String emstoreid) ;
    void removeByStoreid(String emstoreid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMStorePart> getEmstorepartByIds(List<String> ids) ;
    List<EMStorePart> getEmstorepartByEntities(List<EMStorePart> entities) ;
}


