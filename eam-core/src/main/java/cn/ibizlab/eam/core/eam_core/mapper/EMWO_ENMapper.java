package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_EN;
import cn.ibizlab.eam.core.eam_core.filter.EMWO_ENSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMWO_ENMapper extends BaseMapper<EMWO_EN>{

    Page<EMWO_EN> searchCalendar(IPage page, @Param("srf") EMWO_ENSearchContext context, @Param("ew") Wrapper<EMWO_EN> wrapper) ;
    Page<EMWO_EN> searchDefault(IPage page, @Param("srf") EMWO_ENSearchContext context, @Param("ew") Wrapper<EMWO_EN> wrapper) ;
    @Override
    EMWO_EN selectById(Serializable id);
    @Override
    int insert(EMWO_EN entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMWO_EN entity);
    @Override
    int update(@Param(Constants.ENTITY) EMWO_EN entity, @Param("ew") Wrapper<EMWO_EN> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMWO_EN> selectByAcclassid(@Param("emacclassid") Serializable emacclassid) ;

    List<EMWO_EN> selectByEquipid(@Param("emequipid") Serializable emequipid) ;

    List<EMWO_EN> selectByDpid(@Param("emobjectid") Serializable emobjectid) ;

    List<EMWO_EN> selectByObjid(@Param("emobjectid") Serializable emobjectid) ;

    List<EMWO_EN> selectByRfoacid(@Param("emrfoacid") Serializable emrfoacid) ;

    List<EMWO_EN> selectByRfocaid(@Param("emrfocaid") Serializable emrfocaid) ;

    List<EMWO_EN> selectByRfodeid(@Param("emrfodeid") Serializable emrfodeid) ;

    List<EMWO_EN> selectByRfomoid(@Param("emrfomoid") Serializable emrfomoid) ;

    List<EMWO_EN> selectByRserviceid(@Param("emserviceid") Serializable emserviceid) ;

    List<EMWO_EN> selectByWooriid(@Param("emwooriid") Serializable emwooriid) ;

    List<EMWO_EN> selectByWopid(@Param("emwoid") Serializable emwoid) ;

    List<EMWO_EN> selectByRteamid(@Param("pfteamid") Serializable pfteamid) ;

}
