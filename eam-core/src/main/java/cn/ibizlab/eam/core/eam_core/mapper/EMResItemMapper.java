package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMResItem;
import cn.ibizlab.eam.core.eam_core.filter.EMResItemSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMResItemMapper extends BaseMapper<EMResItem>{

    Page<HashMap> searchAmountByTypeByEQ(IPage page, @Param("srf") EMResItemSearchContext context, @Param("ew") Wrapper<EMResItem> wrapper) ;
    Page<EMResItem> searchDefault(IPage page, @Param("srf") EMResItemSearchContext context, @Param("ew") Wrapper<EMResItem> wrapper) ;
    Page<EMResItem> searchUsedByEQ(IPage page, @Param("srf") EMResItemSearchContext context, @Param("ew") Wrapper<EMResItem> wrapper) ;
    Page<EMResItem> searchUsedByItem(IPage page, @Param("srf") EMResItemSearchContext context, @Param("ew") Wrapper<EMResItem> wrapper) ;
    @Override
    EMResItem selectById(Serializable id);
    @Override
    int insert(EMResItem entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMResItem entity);
    @Override
    int update(@Param(Constants.ENTITY) EMResItem entity, @Param("ew") Wrapper<EMResItem> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMResItem> selectByEquipid(@Param("emequipid") Serializable emequipid) ;

    List<EMResItem> selectByResid(@Param("emitemid") Serializable emitemid) ;

    List<EMResItem> selectByResrefobjid(@Param("emresrefobjid") Serializable emresrefobjid) ;

}
