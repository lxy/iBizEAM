

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMENConsum;
import cn.ibizlab.eam.core.eam_core.domain.EMDPRCT;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMENConsumInheritMapping {

    @Mappings({
        @Mapping(source ="emenconsumid",target = "emdprctid"),
        @Mapping(source ="emenconsumname",target = "emdprctname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="enname",target = "dpname"),
        @Mapping(source ="enid",target = "dpid"),
    })
    EMDPRCT toEmdprct(EMENConsum emenconsum);

    @Mappings({
        @Mapping(source ="emdprctid" ,target = "emenconsumid"),
        @Mapping(source ="emdprctname" ,target = "emenconsumname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="dpname",target = "enname"),
        @Mapping(source ="dpid",target = "enid"),
    })
    EMENConsum toEmenconsum(EMDPRCT emdprct);

    List<EMDPRCT> toEmdprct(List<EMENConsum> emenconsum);

    List<EMENConsum> toEmenconsum(List<EMDPRCT> emdprct);

}


