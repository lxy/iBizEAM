

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSpareDetail;
import cn.ibizlab.eam.core.eam_core.domain.EMObjMap;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQSpareDetailInheritMapping {

    @Mappings({
        @Mapping(source ="emeqsparedetailid",target = "emobjmapid"),
        @Mapping(source ="emeqsparedetailname",target = "emobjmapname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="itemid",target = "objid"),
        @Mapping(source ="eqspareid",target = "objpid"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObjMap toEmobjmap(EMEQSpareDetail emeqsparedetail);

    @Mappings({
        @Mapping(source ="emobjmapid" ,target = "emeqsparedetailid"),
        @Mapping(source ="emobjmapname" ,target = "emeqsparedetailname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objid",target = "itemid"),
        @Mapping(source ="objpid",target = "eqspareid"),
    })
    EMEQSpareDetail toEmeqsparedetail(EMObjMap emobjmap);

    List<EMObjMap> toEmobjmap(List<EMEQSpareDetail> emeqsparedetail);

    List<EMEQSpareDetail> toEmeqsparedetail(List<EMObjMap> emobjmap);

}


