package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTRHY;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLCTRHYSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQLCTRHYService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQLCTRHYMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[润滑油位置] 服务对象接口实现
 */
@Slf4j
@Service("EMEQLCTRHYServiceImpl")
public class EMEQLCTRHYServiceImpl extends ServiceImpl<EMEQLCTRHYMapper, EMEQLCTRHY> implements IEMEQLCTRHYService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQLCTRHY et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmeqlocationid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQLCTRHY> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQLCTRHY et) {
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emeqlocationid",et.getEmeqlocationid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmeqlocationid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQLCTRHY> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQLCTRHY get(String key) {
        EMEQLCTRHY et = getById(key);
        if(et==null){
            et=new EMEQLCTRHY();
            et.setEmeqlocationid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMEQLCTRHY getDraft(EMEQLCTRHY et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEQLCTRHY et) {
        return (!ObjectUtils.isEmpty(et.getEmeqlocationid()))&&(!Objects.isNull(this.getById(et.getEmeqlocationid())));
    }
    @Override
    @Transactional
    public boolean save(EMEQLCTRHY et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQLCTRHY et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQLCTRHY> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQLCTRHY> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMEQLCTRHY> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }

    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMEQLCTRHY>().eq("equipid",emequipid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQLCTRHY> searchDefault(EMEQLCTRHYSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQLCTRHY> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQLCTRHY>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEQLCTRHY et){
        //实体关系[DER1N_EMEQLCTRHY_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQLCTRHY> getEmeqlctrhyByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQLCTRHY> getEmeqlctrhyByEntities(List<EMEQLCTRHY> entities) {
        List ids =new ArrayList();
        for(EMEQLCTRHY entity : entities){
            Serializable id=entity.getEmeqlocationid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



