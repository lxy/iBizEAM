package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSpareDetail;
/**
 * 关系型数据实体[EMEQSpareDetail] 查询条件对象
 */
@Slf4j
@Data
public class EMEQSpareDetailSearchContext extends QueryWrapperContext<EMEQSpareDetail> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emeqsparedetailname_like;//[备件包明细]
	public void setN_emeqsparedetailname_like(String n_emeqsparedetailname_like) {
        this.n_emeqsparedetailname_like = n_emeqsparedetailname_like;
        if(!ObjectUtils.isEmpty(this.n_emeqsparedetailname_like)){
            this.getSearchCond().like("emeqsparedetailname", n_emeqsparedetailname_like);
        }
    }
	private String n_itemname_eq;//[物品]
	public void setN_itemname_eq(String n_itemname_eq) {
        this.n_itemname_eq = n_itemname_eq;
        if(!ObjectUtils.isEmpty(this.n_itemname_eq)){
            this.getSearchCond().eq("itemname", n_itemname_eq);
        }
    }
	private String n_itemname_like;//[物品]
	public void setN_itemname_like(String n_itemname_like) {
        this.n_itemname_like = n_itemname_like;
        if(!ObjectUtils.isEmpty(this.n_itemname_like)){
            this.getSearchCond().like("itemname", n_itemname_like);
        }
    }
	private String n_eqsparename_eq;//[备件包]
	public void setN_eqsparename_eq(String n_eqsparename_eq) {
        this.n_eqsparename_eq = n_eqsparename_eq;
        if(!ObjectUtils.isEmpty(this.n_eqsparename_eq)){
            this.getSearchCond().eq("eqsparename", n_eqsparename_eq);
        }
    }
	private String n_eqsparename_like;//[备件包]
	public void setN_eqsparename_like(String n_eqsparename_like) {
        this.n_eqsparename_like = n_eqsparename_like;
        if(!ObjectUtils.isEmpty(this.n_eqsparename_like)){
            this.getSearchCond().like("eqsparename", n_eqsparename_like);
        }
    }
	private String n_itemid_eq;//[物品]
	public void setN_itemid_eq(String n_itemid_eq) {
        this.n_itemid_eq = n_itemid_eq;
        if(!ObjectUtils.isEmpty(this.n_itemid_eq)){
            this.getSearchCond().eq("itemid", n_itemid_eq);
        }
    }
	private String n_eqspareid_eq;//[备件包]
	public void setN_eqspareid_eq(String n_eqspareid_eq) {
        this.n_eqspareid_eq = n_eqspareid_eq;
        if(!ObjectUtils.isEmpty(this.n_eqspareid_eq)){
            this.getSearchCond().eq("eqspareid", n_eqspareid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emeqsparedetailname", query)   
            );
		 }
	}
}



