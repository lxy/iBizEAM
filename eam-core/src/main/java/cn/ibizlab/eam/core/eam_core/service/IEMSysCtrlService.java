package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMSysCtrl;
import cn.ibizlab.eam.core.eam_core.filter.EMSysCtrlSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMSysCtrl] 服务对象接口
 */
public interface IEMSysCtrlService extends IService<EMSysCtrl>{

    boolean create(EMSysCtrl et) ;
    void createBatch(List<EMSysCtrl> list) ;
    boolean update(EMSysCtrl et) ;
    void updateBatch(List<EMSysCtrl> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMSysCtrl get(String key) ;
    EMSysCtrl getDraft(EMSysCtrl et) ;
    boolean checkKey(EMSysCtrl et) ;
    boolean save(EMSysCtrl et) ;
    void saveBatch(List<EMSysCtrl> list) ;
    Page<EMSysCtrl> searchDefault(EMSysCtrlSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMSysCtrl> getEmsysctrlByIds(List<String> ids) ;
    List<EMSysCtrl> getEmsysctrlByEntities(List<EMSysCtrl> entities) ;
}


