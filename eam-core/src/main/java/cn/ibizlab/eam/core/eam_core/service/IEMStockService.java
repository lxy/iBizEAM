package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMStock;
import cn.ibizlab.eam.core.eam_core.filter.EMStockSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMStock] 服务对象接口
 */
public interface IEMStockService extends IService<EMStock>{

    boolean create(EMStock et) ;
    void createBatch(List<EMStock> list) ;
    boolean update(EMStock et) ;
    void updateBatch(List<EMStock> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMStock get(String key) ;
    EMStock getDraft(EMStock et) ;
    boolean checkKey(EMStock et) ;
    boolean save(EMStock et) ;
    void saveBatch(List<EMStock> list) ;
    Page<EMStock> searchDefault(EMStockSearchContext context) ;
    Page<HashMap> searchTypeStock(EMStockSearchContext context) ;
    List<EMStock> selectByItemid(String emitemid) ;
    void removeByItemid(String emitemid) ;
    List<EMStock> selectByStorepartid(String emstorepartid) ;
    void removeByStorepartid(String emstorepartid) ;
    List<EMStock> selectByStoreid(String emstoreid) ;
    void removeByStoreid(String emstoreid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMStock> getEmstockByIds(List<String> ids) ;
    List<EMStock> getEmstockByEntities(List<EMStock> entities) ;
}


