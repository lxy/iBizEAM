package cn.ibizlab.eam.core.eam_core.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.eam.core.eam_core.service.logic.IEMDRWGGenIdLogic;
import cn.ibizlab.eam.core.eam_core.domain.EMDRWG;

/**
 * 关系型数据实体[GenId] 对象
 */
@Slf4j
@Service
public class EMDRWGGenIdLogicImpl implements IEMDRWGGenIdLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMDRWGService emdrwgservice;

    public cn.ibizlab.eam.core.eam_core.service.IEMDRWGService getEmdrwgService() {
        return this.emdrwgservice;
    }


    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMDRWGService iBzSysDefaultService;

    public cn.ibizlab.eam.core.eam_core.service.IEMDRWGService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(EMDRWG et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("emdrwggeniddefault",et);
           kieSession.setGlobal("emdrwgservice",emdrwgservice);
           kieSession.setGlobal("iBzSysEmdrwgDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.eam.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.eam.core.eam_core.service.logic.emdrwggenid");

        }catch(Exception e){
            throw new RuntimeException("执行[GenId]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}
