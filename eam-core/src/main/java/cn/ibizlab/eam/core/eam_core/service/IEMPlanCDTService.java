package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMPlanCDT;
import cn.ibizlab.eam.core.eam_core.filter.EMPlanCDTSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMPlanCDT] 服务对象接口
 */
public interface IEMPlanCDTService extends IService<EMPlanCDT>{

    boolean create(EMPlanCDT et) ;
    void createBatch(List<EMPlanCDT> list) ;
    boolean update(EMPlanCDT et) ;
    void updateBatch(List<EMPlanCDT> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMPlanCDT get(String key) ;
    EMPlanCDT getDraft(EMPlanCDT et) ;
    boolean checkKey(EMPlanCDT et) ;
    boolean save(EMPlanCDT et) ;
    void saveBatch(List<EMPlanCDT> list) ;
    Page<EMPlanCDT> searchDefault(EMPlanCDTSearchContext context) ;
    List<EMPlanCDT> selectByEquipid(String emequipid) ;
    void removeByEquipid(String emequipid) ;
    List<EMPlanCDT> selectByDpid(String emobjectid) ;
    void removeByDpid(String emobjectid) ;
    List<EMPlanCDT> selectByObjid(String emobjectid) ;
    void removeByObjid(String emobjectid) ;
    List<EMPlanCDT> selectByPlanid(String emplanid) ;
    void removeByPlanid(String emplanid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMPlanCDT> getEmplancdtByIds(List<String> ids) ;
    List<EMPlanCDT> getEmplancdtByEntities(List<EMPlanCDT> entities) ;
}


