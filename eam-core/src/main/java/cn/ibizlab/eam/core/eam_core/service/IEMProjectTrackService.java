package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMProjectTrack;
import cn.ibizlab.eam.core.eam_core.filter.EMProjectTrackSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMProjectTrack] 服务对象接口
 */
public interface IEMProjectTrackService extends IService<EMProjectTrack>{

    boolean create(EMProjectTrack et) ;
    void createBatch(List<EMProjectTrack> list) ;
    boolean update(EMProjectTrack et) ;
    void updateBatch(List<EMProjectTrack> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMProjectTrack get(String key) ;
    EMProjectTrack getDraft(EMProjectTrack et) ;
    boolean checkKey(EMProjectTrack et) ;
    boolean save(EMProjectTrack et) ;
    void saveBatch(List<EMProjectTrack> list) ;
    Page<EMProjectTrack> searchDefault(EMProjectTrackSearchContext context) ;
    List<EMProjectTrack> selectByEmpurplanid(String empurplanid) ;
    void removeByEmpurplanid(String empurplanid) ;
    List<EMProjectTrack> selectByEmserviceid(String emserviceid) ;
    void removeByEmserviceid(String emserviceid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMProjectTrack> getEmprojecttrackByIds(List<String> ids) ;
    List<EMProjectTrack> getEmprojecttrackByEntities(List<EMProjectTrack> entities) ;
}


