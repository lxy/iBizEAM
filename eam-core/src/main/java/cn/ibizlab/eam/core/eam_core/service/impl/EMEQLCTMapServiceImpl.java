package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTMap;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLCTMapSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQLCTMapService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQLCTMapMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[位置关系] 服务对象接口实现
 */
@Slf4j
@Service("EMEQLCTMapServiceImpl")
public class EMEQLCTMapServiceImpl extends ServiceImpl<EMEQLCTMapMapper, EMEQLCTMap> implements IEMEQLCTMapService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQLocationService emeqlocationService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQLCTMap et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmeqlctmapid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQLCTMap> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQLCTMap et) {
        fillParentData(et);
        emobjmapService.update(emeqlctmapInheritMapping.toEmobjmap(et));
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emeqlctmapid",et.getEmeqlctmapid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmeqlctmapid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQLCTMap> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emobjmapService.remove(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQLCTMap get(String key) {
        EMEQLCTMap et = getById(key);
        if(et==null){
            et=new EMEQLCTMap();
            et.setEmeqlctmapid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMEQLCTMap getDraft(EMEQLCTMap et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEQLCTMap et) {
        return (!ObjectUtils.isEmpty(et.getEmeqlctmapid()))&&(!Objects.isNull(this.getById(et.getEmeqlctmapid())));
    }
    @Override
    @Transactional
    public boolean save(EMEQLCTMap et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQLCTMap et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQLCTMap> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQLCTMap> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMEQLCTMap> selectByEqlocationid(String emeqlocationid) {
        return baseMapper.selectByEqlocationid(emeqlocationid);
    }

    @Override
    public void removeByEqlocationid(String emeqlocationid) {
        this.remove(new QueryWrapper<EMEQLCTMap>().eq("eqlocationid",emeqlocationid));
    }

	@Override
    public List<EMEQLCTMap> selectByEqlocationpid(String emeqlocationid) {
        return baseMapper.selectByEqlocationpid(emeqlocationid);
    }

    @Override
    public void removeByEqlocationpid(String emeqlocationid) {
        this.remove(new QueryWrapper<EMEQLCTMap>().eq("eqlocationpid",emeqlocationid));
    }


    /**
     * 查询集合 ByLocation
     */
    @Override
    public Page<EMEQLCTMap> searchByLocation(EMEQLCTMapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQLCTMap> pages=baseMapper.searchByLocation(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQLCTMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 ChildLocation
     */
    @Override
    public Page<EMEQLCTMap> searchChildLocation(EMEQLCTMapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQLCTMap> pages=baseMapper.searchChildLocation(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQLCTMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQLCTMap> searchDefault(EMEQLCTMapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQLCTMap> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQLCTMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 RootLocation
     */
    @Override
    public Page<EMEQLCTMap> searchRootLocation(EMEQLCTMapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQLCTMap> pages=baseMapper.searchRootLocation(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQLCTMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEQLCTMap et){
        //实体关系[DER1N_EMEQLCTMAP_EMEQLOCATION_EQLOCATIONID]
        if(!ObjectUtils.isEmpty(et.getEqlocationid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEQLocation eqlocation=et.getEqlocation();
            if(ObjectUtils.isEmpty(eqlocation)){
                cn.ibizlab.eam.core.eam_core.domain.EMEQLocation majorEntity=emeqlocationService.get(et.getEqlocationid());
                et.setEqlocation(majorEntity);
                eqlocation=majorEntity;
            }
            et.setMajorequipname(eqlocation.getMajorequipname());
            et.setEqlocationcode(eqlocation.getEqlocationcode());
            et.setEqlocationdesc(eqlocation.getDescription());
            et.setMajorequipid(eqlocation.getMajorequipid());
            et.setEqlocationname(eqlocation.getEqlocationinfo());
        }
        //实体关系[DER1N_EMEQLCTMAP_EMEQLOCATION_EQLOCATIONPID]
        if(!ObjectUtils.isEmpty(et.getEqlocationpid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEQLocation eqlocationp=et.getEqlocationp();
            if(ObjectUtils.isEmpty(eqlocationp)){
                cn.ibizlab.eam.core.eam_core.domain.EMEQLocation majorEntity=emeqlocationService.get(et.getEqlocationpid());
                et.setEqlocationp(majorEntity);
                eqlocationp=majorEntity;
            }
            et.setEqlocationpname(eqlocationp.getEmeqlocationname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMEQLCTMapInheritMapping emeqlctmapInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjMapService emobjmapService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMEQLCTMap et){
        if(ObjectUtils.isEmpty(et.getEmeqlctmapid()))
            et.setEmeqlctmapid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMObjMap emobjmap =emeqlctmapInheritMapping.toEmobjmap(et);
        emobjmap.set("emobjmaptype","EQLOCATION");
        emobjmapService.create(emobjmap);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQLCTMap> getEmeqlctmapByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQLCTMap> getEmeqlctmapByEntities(List<EMEQLCTMap> entities) {
        List ids =new ArrayList();
        for(EMEQLCTMap entity : entities){
            Serializable id=entity.getEmeqlctmapid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



