package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMResEmp;
import cn.ibizlab.eam.core.eam_core.filter.EMResEmpSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMResEmp] 服务对象接口
 */
public interface IEMResEmpService extends IService<EMResEmp>{

    boolean create(EMResEmp et) ;
    void createBatch(List<EMResEmp> list) ;
    boolean update(EMResEmp et) ;
    void updateBatch(List<EMResEmp> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMResEmp get(String key) ;
    EMResEmp getDraft(EMResEmp et) ;
    boolean checkKey(EMResEmp et) ;
    boolean save(EMResEmp et) ;
    void saveBatch(List<EMResEmp> list) ;
    Page<EMResEmp> searchDefault(EMResEmpSearchContext context) ;
    List<EMResEmp> selectByEquipid(String emequipid) ;
    void removeByEquipid(String emequipid) ;
    List<EMResEmp> selectByResrefobjid(String emresrefobjid) ;
    void removeByResrefobjid(String emresrefobjid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMResEmp> getEmresempByIds(List<String> ids) ;
    List<EMResEmp> getEmresempByEntities(List<EMResEmp> entities) ;
}


