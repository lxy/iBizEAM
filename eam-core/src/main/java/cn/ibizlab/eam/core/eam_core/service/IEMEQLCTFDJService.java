package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTFDJ;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLCTFDJSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQLCTFDJ] 服务对象接口
 */
public interface IEMEQLCTFDJService extends IService<EMEQLCTFDJ>{

    boolean create(EMEQLCTFDJ et) ;
    void createBatch(List<EMEQLCTFDJ> list) ;
    boolean update(EMEQLCTFDJ et) ;
    void updateBatch(List<EMEQLCTFDJ> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMEQLCTFDJ get(String key) ;
    EMEQLCTFDJ getDraft(EMEQLCTFDJ et) ;
    boolean checkKey(EMEQLCTFDJ et) ;
    boolean save(EMEQLCTFDJ et) ;
    void saveBatch(List<EMEQLCTFDJ> list) ;
    Page<EMEQLCTFDJ> searchDefault(EMEQLCTFDJSearchContext context) ;
    List<EMEQLCTFDJ> selectByEquipid(String emequipid) ;
    void removeByEquipid(String emequipid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQLCTFDJ> getEmeqlctfdjByIds(List<String> ids) ;
    List<EMEQLCTFDJ> getEmeqlctfdjByEntities(List<EMEQLCTFDJ> entities) ;
}


