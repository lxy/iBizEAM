package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.service.impl.EMPOServiceImpl;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.eam.core.eam_core.domain.EMPO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;
import java.util.*;

/**
 * 实体[订单] 自定义服务对象
 */
@Slf4j
@Primary
@Service("EMPOExService")
public class EMPOExService extends EMPOServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * 自定义行为[Arrival]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMPO arrival(EMPO et) {
        return super.arrival(et);
    }
    /**
     * 自定义行为[GenId]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMPO genId(EMPO et) {
        return super.genId(et);
    }
    /**
     * 自定义行为[PlaceOrder]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMPO placeOrder(EMPO et) {
        return super.placeOrder(et);
    }
}

