package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMPlanTempl;
/**
 * 关系型数据实体[EMPlanTempl] 查询条件对象
 */
@Slf4j
@Data
public class EMPlanTemplSearchContext extends QueryWrapperContext<EMPlanTempl> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_plantemplinfo_like;//[计划模板信息]
	public void setN_plantemplinfo_like(String n_plantemplinfo_like) {
        this.n_plantemplinfo_like = n_plantemplinfo_like;
        if(!ObjectUtils.isEmpty(this.n_plantemplinfo_like)){
            this.getSearchCond().like("plantemplinfo", n_plantemplinfo_like);
        }
    }
	private String n_mpersonname_eq;//[制定人]
	public void setN_mpersonname_eq(String n_mpersonname_eq) {
        this.n_mpersonname_eq = n_mpersonname_eq;
        if(!ObjectUtils.isEmpty(this.n_mpersonname_eq)){
            this.getSearchCond().eq("mpersonname", n_mpersonname_eq);
        }
    }
	private String n_mpersonname_like;//[制定人]
	public void setN_mpersonname_like(String n_mpersonname_like) {
        this.n_mpersonname_like = n_mpersonname_like;
        if(!ObjectUtils.isEmpty(this.n_mpersonname_like)){
            this.getSearchCond().like("mpersonname", n_mpersonname_like);
        }
    }
	private String n_emplantemplname_like;//[计划模板名称]
	public void setN_emplantemplname_like(String n_emplantemplname_like) {
        this.n_emplantemplname_like = n_emplantemplname_like;
        if(!ObjectUtils.isEmpty(this.n_emplantemplname_like)){
            this.getSearchCond().like("emplantemplname", n_emplantemplname_like);
        }
    }
	private String n_rempname_eq;//[责任人]
	public void setN_rempname_eq(String n_rempname_eq) {
        this.n_rempname_eq = n_rempname_eq;
        if(!ObjectUtils.isEmpty(this.n_rempname_eq)){
            this.getSearchCond().eq("rempname", n_rempname_eq);
        }
    }
	private String n_rempname_like;//[责任人]
	public void setN_rempname_like(String n_rempname_like) {
        this.n_rempname_like = n_rempname_like;
        if(!ObjectUtils.isEmpty(this.n_rempname_like)){
            this.getSearchCond().like("rempname", n_rempname_like);
        }
    }
	private String n_mpersonid_eq;//[制定人]
	public void setN_mpersonid_eq(String n_mpersonid_eq) {
        this.n_mpersonid_eq = n_mpersonid_eq;
        if(!ObjectUtils.isEmpty(this.n_mpersonid_eq)){
            this.getSearchCond().eq("mpersonid", n_mpersonid_eq);
        }
    }
	private String n_plantype_eq;//[计划类型]
	public void setN_plantype_eq(String n_plantype_eq) {
        this.n_plantype_eq = n_plantype_eq;
        if(!ObjectUtils.isEmpty(this.n_plantype_eq)){
            this.getSearchCond().eq("plantype", n_plantype_eq);
        }
    }
	private String n_recvpersonid_eq;//[接收人]
	public void setN_recvpersonid_eq(String n_recvpersonid_eq) {
        this.n_recvpersonid_eq = n_recvpersonid_eq;
        if(!ObjectUtils.isEmpty(this.n_recvpersonid_eq)){
            this.getSearchCond().eq("recvpersonid", n_recvpersonid_eq);
        }
    }
	private String n_emwotype_eq;//[生成工单种类]
	public void setN_emwotype_eq(String n_emwotype_eq) {
        this.n_emwotype_eq = n_emwotype_eq;
        if(!ObjectUtils.isEmpty(this.n_emwotype_eq)){
            this.getSearchCond().eq("emwotype", n_emwotype_eq);
        }
    }
	private String n_rdeptname_eq;//[责任部门]
	public void setN_rdeptname_eq(String n_rdeptname_eq) {
        this.n_rdeptname_eq = n_rdeptname_eq;
        if(!ObjectUtils.isEmpty(this.n_rdeptname_eq)){
            this.getSearchCond().eq("rdeptname", n_rdeptname_eq);
        }
    }
	private String n_rdeptname_like;//[责任部门]
	public void setN_rdeptname_like(String n_rdeptname_like) {
        this.n_rdeptname_like = n_rdeptname_like;
        if(!ObjectUtils.isEmpty(this.n_rdeptname_like)){
            this.getSearchCond().like("rdeptname", n_rdeptname_like);
        }
    }
	private String n_rempid_eq;//[责任人]
	public void setN_rempid_eq(String n_rempid_eq) {
        this.n_rempid_eq = n_rempid_eq;
        if(!ObjectUtils.isEmpty(this.n_rempid_eq)){
            this.getSearchCond().eq("rempid", n_rempid_eq);
        }
    }
	private String n_rdeptid_eq;//[责任部门]
	public void setN_rdeptid_eq(String n_rdeptid_eq) {
        this.n_rdeptid_eq = n_rdeptid_eq;
        if(!ObjectUtils.isEmpty(this.n_rdeptid_eq)){
            this.getSearchCond().eq("rdeptid", n_rdeptid_eq);
        }
    }
	private String n_recvpersonname_eq;//[接收人]
	public void setN_recvpersonname_eq(String n_recvpersonname_eq) {
        this.n_recvpersonname_eq = n_recvpersonname_eq;
        if(!ObjectUtils.isEmpty(this.n_recvpersonname_eq)){
            this.getSearchCond().eq("recvpersonname", n_recvpersonname_eq);
        }
    }
	private String n_recvpersonname_like;//[接收人]
	public void setN_recvpersonname_like(String n_recvpersonname_like) {
        this.n_recvpersonname_like = n_recvpersonname_like;
        if(!ObjectUtils.isEmpty(this.n_recvpersonname_like)){
            this.getSearchCond().like("recvpersonname", n_recvpersonname_like);
        }
    }
	private Integer n_planstate_eq;//[计划状态]
	public void setN_planstate_eq(Integer n_planstate_eq) {
        this.n_planstate_eq = n_planstate_eq;
        if(!ObjectUtils.isEmpty(this.n_planstate_eq)){
            this.getSearchCond().eq("planstate", n_planstate_eq);
        }
    }
	private String n_rservicename_eq;//[服务商]
	public void setN_rservicename_eq(String n_rservicename_eq) {
        this.n_rservicename_eq = n_rservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_rservicename_eq)){
            this.getSearchCond().eq("rservicename", n_rservicename_eq);
        }
    }
	private String n_rservicename_like;//[服务商]
	public void setN_rservicename_like(String n_rservicename_like) {
        this.n_rservicename_like = n_rservicename_like;
        if(!ObjectUtils.isEmpty(this.n_rservicename_like)){
            this.getSearchCond().like("rservicename", n_rservicename_like);
        }
    }
	private String n_rteamname_eq;//[责任班组]
	public void setN_rteamname_eq(String n_rteamname_eq) {
        this.n_rteamname_eq = n_rteamname_eq;
        if(!ObjectUtils.isEmpty(this.n_rteamname_eq)){
            this.getSearchCond().eq("rteamname", n_rteamname_eq);
        }
    }
	private String n_rteamname_like;//[责任班组]
	public void setN_rteamname_like(String n_rteamname_like) {
        this.n_rteamname_like = n_rteamname_like;
        if(!ObjectUtils.isEmpty(this.n_rteamname_like)){
            this.getSearchCond().like("rteamname", n_rteamname_like);
        }
    }
	private String n_acclassname_eq;//[总帐科目]
	public void setN_acclassname_eq(String n_acclassname_eq) {
        this.n_acclassname_eq = n_acclassname_eq;
        if(!ObjectUtils.isEmpty(this.n_acclassname_eq)){
            this.getSearchCond().eq("acclassname", n_acclassname_eq);
        }
    }
	private String n_acclassname_like;//[总帐科目]
	public void setN_acclassname_like(String n_acclassname_like) {
        this.n_acclassname_like = n_acclassname_like;
        if(!ObjectUtils.isEmpty(this.n_acclassname_like)){
            this.getSearchCond().like("acclassname", n_acclassname_like);
        }
    }
	private String n_acclassid_eq;//[总帐科目]
	public void setN_acclassid_eq(String n_acclassid_eq) {
        this.n_acclassid_eq = n_acclassid_eq;
        if(!ObjectUtils.isEmpty(this.n_acclassid_eq)){
            this.getSearchCond().eq("acclassid", n_acclassid_eq);
        }
    }
	private String n_rserviceid_eq;//[服务商]
	public void setN_rserviceid_eq(String n_rserviceid_eq) {
        this.n_rserviceid_eq = n_rserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_rserviceid_eq)){
            this.getSearchCond().eq("rserviceid", n_rserviceid_eq);
        }
    }
	private String n_rteamid_eq;//[责任班组]
	public void setN_rteamid_eq(String n_rteamid_eq) {
        this.n_rteamid_eq = n_rteamid_eq;
        if(!ObjectUtils.isEmpty(this.n_rteamid_eq)){
            this.getSearchCond().eq("rteamid", n_rteamid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emplantemplname", query)   
            );
		 }
	}
}



