package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMBerth;
import cn.ibizlab.eam.core.eam_core.filter.EMBerthSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMBerthService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMBerthMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[泊位] 服务对象接口实现
 */
@Slf4j
@Service("EMBerthServiceImpl")
public class EMBerthServiceImpl extends ServiceImpl<EMBerthMapper, EMBerth> implements IEMBerthService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMBerth et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmberthid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMBerth> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMBerth et) {
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emberthid",et.getEmberthid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmberthid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMBerth> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMBerth get(String key) {
        EMBerth et = getById(key);
        if(et==null){
            et=new EMBerth();
            et.setEmberthid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMBerth getDraft(EMBerth et) {
        return et;
    }

    @Override
    public boolean checkKey(EMBerth et) {
        return (!ObjectUtils.isEmpty(et.getEmberthid()))&&(!Objects.isNull(this.getById(et.getEmberthid())));
    }
    @Override
    @Transactional
    public boolean save(EMBerth et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMBerth et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMBerth> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMBerth> list) {
        saveOrUpdateBatch(list,batchSize);
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMBerth> searchDefault(EMBerthSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMBerth> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMBerth>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMBerth> getEmberthByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMBerth> getEmberthByEntities(List<EMBerth> entities) {
        List ids =new ArrayList();
        for(EMBerth entity : entities){
            Serializable id=entity.getEmberthid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



