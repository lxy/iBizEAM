package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMObject;
import cn.ibizlab.eam.core.eam_core.filter.EMObjectSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMObject] 服务对象接口
 */
public interface IEMObjectService extends IService<EMObject>{

    boolean create(EMObject et) ;
    void createBatch(List<EMObject> list) ;
    boolean update(EMObject et) ;
    void updateBatch(List<EMObject> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMObject get(String key) ;
    EMObject getDraft(EMObject et) ;
    boolean checkKey(EMObject et) ;
    boolean save(EMObject et) ;
    void saveBatch(List<EMObject> list) ;
    Page<EMObject> searchDefault(EMObjectSearchContext context) ;
    Page<EMObject> searchIndexDER(EMObjectSearchContext context) ;
    List<EMObject> selectByMajorequipid(String emequipid) ;
    void removeByMajorequipid(String emequipid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMObject> getEmobjectByIds(List<String> ids) ;
    List<EMObject> getEmobjectByEntities(List<EMObject> entities) ;
}


