package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMWPList;
import cn.ibizlab.eam.core.eam_core.filter.EMWPListSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMWPListMapper extends BaseMapper<EMWPList>{

    Page<EMWPList> searchCancel(IPage page, @Param("srf") EMWPListSearchContext context, @Param("ew") Wrapper<EMWPList> wrapper) ;
    Page<EMWPList> searchConfimCost(IPage page, @Param("srf") EMWPListSearchContext context, @Param("ew") Wrapper<EMWPList> wrapper) ;
    Page<EMWPList> searchDefault(IPage page, @Param("srf") EMWPListSearchContext context, @Param("ew") Wrapper<EMWPList> wrapper) ;
    Page<EMWPList> searchDraft(IPage page, @Param("srf") EMWPListSearchContext context, @Param("ew") Wrapper<EMWPList> wrapper) ;
    Page<EMWPList> searchIn(IPage page, @Param("srf") EMWPListSearchContext context, @Param("ew") Wrapper<EMWPList> wrapper) ;
    Page<EMWPList> searchMain6(IPage page, @Param("srf") EMWPListSearchContext context, @Param("ew") Wrapper<EMWPList> wrapper) ;
    Page<EMWPList> searchMain6_8692(IPage page, @Param("srf") EMWPListSearchContext context, @Param("ew") Wrapper<EMWPList> wrapper) ;
    Page<EMWPList> searchWaitCost(IPage page, @Param("srf") EMWPListSearchContext context, @Param("ew") Wrapper<EMWPList> wrapper) ;
    Page<EMWPList> searchWaitPo(IPage page, @Param("srf") EMWPListSearchContext context, @Param("ew") Wrapper<EMWPList> wrapper) ;
    Page<HashMap> searchWpStateNum(IPage page, @Param("srf") EMWPListSearchContext context, @Param("ew") Wrapper<EMWPList> wrapper) ;
    @Override
    EMWPList selectById(Serializable id);
    @Override
    int insert(EMWPList entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMWPList entity);
    @Override
    int update(@Param(Constants.ENTITY) EMWPList entity, @Param("ew") Wrapper<EMWPList> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMWPList> selectByEquipid(@Param("emequipid") Serializable emequipid) ;

    List<EMWPList> selectByItemid(@Param("emitemid") Serializable emitemid) ;

    List<EMWPList> selectByObjid(@Param("emobjectid") Serializable emobjectid) ;

    List<EMWPList> selectByEmserviceid(@Param("emserviceid") Serializable emserviceid) ;

    List<EMWPList> selectByWplistcostid(@Param("emwplistcostid") Serializable emwplistcostid) ;

    List<EMWPList> selectByTeamid(@Param("pfteamid") Serializable pfteamid) ;

}
