package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQSpareMap;
import cn.ibizlab.eam.core.eam_core.filter.EMEQSpareMapSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQSpareMap] 服务对象接口
 */
public interface IEMEQSpareMapService extends IService<EMEQSpareMap>{

    boolean create(EMEQSpareMap et) ;
    void createBatch(List<EMEQSpareMap> list) ;
    boolean update(EMEQSpareMap et) ;
    void updateBatch(List<EMEQSpareMap> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMEQSpareMap get(String key) ;
    EMEQSpareMap getDraft(EMEQSpareMap et) ;
    boolean checkKey(EMEQSpareMap et) ;
    boolean save(EMEQSpareMap et) ;
    void saveBatch(List<EMEQSpareMap> list) ;
    Page<EMEQSpareMap> searchByEQ(EMEQSpareMapSearchContext context) ;
    Page<EMEQSpareMap> searchDefault(EMEQSpareMapSearchContext context) ;
    List<EMEQSpareMap> selectByEqspareid(String emeqspareid) ;
    void removeByEqspareid(String emeqspareid) ;
    List<EMEQSpareMap> selectByRefobjid(String emobjectid) ;
    void removeByRefobjid(String emobjectid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQSpareMap> getEmeqsparemapByIds(List<String> ids) ;
    List<EMEQSpareMap> getEmeqsparemapByEntities(List<EMEQSpareMap> entities) ;
}


