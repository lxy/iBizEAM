package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMDRWG1;
import cn.ibizlab.eam.core.eam_core.filter.EMDRWG1SearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMDRWG1Service;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMDRWG1Mapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[特种设备标准化管理] 服务对象接口实现
 */
@Slf4j
@Service("EMDRWG1ServiceImpl")
public class EMDRWG1ServiceImpl extends ServiceImpl<EMDRWG1Mapper, EMDRWG1> implements IEMDRWG1Service {


    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMDRWG1 et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmdrwg1id()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMDRWG1> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMDRWG1 et) {
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emdrwg1id",et.getEmdrwg1id())))
            return false;
        CachedBeanCopier.copy(get(et.getEmdrwg1id()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMDRWG1> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMDRWG1 get(String key) {
        EMDRWG1 et = getById(key);
        if(et==null){
            et=new EMDRWG1();
            et.setEmdrwg1id(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMDRWG1 getDraft(EMDRWG1 et) {
        return et;
    }

    @Override
    public boolean checkKey(EMDRWG1 et) {
        return (!ObjectUtils.isEmpty(et.getEmdrwg1id()))&&(!Objects.isNull(this.getById(et.getEmdrwg1id())));
    }
    @Override
    @Transactional
    public boolean save(EMDRWG1 et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMDRWG1 et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMDRWG1> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMDRWG1> list) {
        saveOrUpdateBatch(list,batchSize);
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMDRWG1> searchDefault(EMDRWG1SearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMDRWG1> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMDRWG1>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMDRWG1> getEmdrwg1ByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMDRWG1> getEmdrwg1ByEntities(List<EMDRWG1> entities) {
        List ids =new ArrayList();
        for(EMDRWG1 entity : entities){
            Serializable id=entity.getEmdrwg1id();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



