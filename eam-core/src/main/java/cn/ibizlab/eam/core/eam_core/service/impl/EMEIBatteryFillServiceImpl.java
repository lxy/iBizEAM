package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEIBatteryFill;
import cn.ibizlab.eam.core.eam_core.filter.EMEIBatteryFillSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEIBatteryFillService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEIBatteryFillMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[电瓶充电记录] 服务对象接口实现
 */
@Slf4j
@Service("EMEIBatteryFillServiceImpl")
public class EMEIBatteryFillServiceImpl extends ServiceImpl<EMEIBatteryFillMapper, EMEIBatteryFill> implements IEMEIBatteryFillService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEIBatteryService emeibatteryService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEIBatteryFill et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmeibatteryfillid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEIBatteryFill> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEIBatteryFill et) {
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emeibatteryfillid",et.getEmeibatteryfillid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmeibatteryfillid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEIBatteryFill> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEIBatteryFill get(String key) {
        EMEIBatteryFill et = getById(key);
        if(et==null){
            et=new EMEIBatteryFill();
            et.setEmeibatteryfillid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMEIBatteryFill getDraft(EMEIBatteryFill et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEIBatteryFill et) {
        return (!ObjectUtils.isEmpty(et.getEmeibatteryfillid()))&&(!Objects.isNull(this.getById(et.getEmeibatteryfillid())));
    }
    @Override
    @Transactional
    public boolean save(EMEIBatteryFill et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEIBatteryFill et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEIBatteryFill> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEIBatteryFill> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMEIBatteryFill> selectByEiobjid(String emeibatteryid) {
        return baseMapper.selectByEiobjid(emeibatteryid);
    }

    @Override
    public void removeByEiobjid(String emeibatteryid) {
        this.remove(new QueryWrapper<EMEIBatteryFill>().eq("eiobjid",emeibatteryid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEIBatteryFill> searchDefault(EMEIBatteryFillSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEIBatteryFill> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEIBatteryFill>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEIBatteryFill et){
        //实体关系[DER1N_EMEIBATTERYFILL_EMEIBATTERY_EIOBJID]
        if(!ObjectUtils.isEmpty(et.getEiobjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEIBattery eiobj=et.getEiobj();
            if(ObjectUtils.isEmpty(eiobj)){
                cn.ibizlab.eam.core.eam_core.domain.EMEIBattery majorEntity=emeibatteryService.get(et.getEiobjid());
                et.setEiobj(majorEntity);
                eiobj=majorEntity;
            }
            et.setEiobjname(eiobj.getEmeibatteryname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEIBatteryFill> getEmeibatteryfillByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEIBatteryFill> getEmeibatteryfillByEntities(List<EMEIBatteryFill> entities) {
        List ids =new ArrayList();
        for(EMEIBatteryFill entity : entities){
            Serializable id=entity.getEmeibatteryfillid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



