package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMItemPL;
import cn.ibizlab.eam.core.eam_core.filter.EMItemPLSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMItemPLService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMItemPLMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[损溢单] 服务对象接口实现
 */
@Slf4j
@Service("EMItemPLServiceImpl")
public class EMItemPLServiceImpl extends ServiceImpl<EMItemPLMapper, EMItemPL> implements IEMItemPLService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemRInService emitemrinService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMStorePartService emstorepartService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMStoreService emstoreService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMItemPL et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmitemplid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMItemPL> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMItemPL et) {
        fillParentData(et);
        emitemtradeService.update(emitemplInheritMapping.toEmitemtrade(et));
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emitemplid",et.getEmitemplid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmitemplid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMItemPL> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emitemtradeService.remove(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMItemPL get(String key) {
        EMItemPL et = getById(key);
        if(et==null){
            et=new EMItemPL();
            et.setEmitemplid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMItemPL getDraft(EMItemPL et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMItemPL et) {
        return (!ObjectUtils.isEmpty(et.getEmitemplid()))&&(!Objects.isNull(this.getById(et.getEmitemplid())));
    }
    @Override
    @Transactional
    public boolean save(EMItemPL et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMItemPL et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMItemPL> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMItemPL> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMItemPL> selectByRid(String emitemrinid) {
        return baseMapper.selectByRid(emitemrinid);
    }

    @Override
    public void removeByRid(String emitemrinid) {
        this.remove(new QueryWrapper<EMItemPL>().eq("rid",emitemrinid));
    }

	@Override
    public List<EMItemPL> selectByItemid(String emitemid) {
        return baseMapper.selectByItemid(emitemid);
    }

    @Override
    public void removeByItemid(String emitemid) {
        this.remove(new QueryWrapper<EMItemPL>().eq("itemid",emitemid));
    }

	@Override
    public List<EMItemPL> selectByStorepartid(String emstorepartid) {
        return baseMapper.selectByStorepartid(emstorepartid);
    }

    @Override
    public void removeByStorepartid(String emstorepartid) {
        this.remove(new QueryWrapper<EMItemPL>().eq("storepartid",emstorepartid));
    }

	@Override
    public List<EMItemPL> selectByStoreid(String emstoreid) {
        return baseMapper.selectByStoreid(emstoreid);
    }

    @Override
    public void removeByStoreid(String emstoreid) {
        this.remove(new QueryWrapper<EMItemPL>().eq("storeid",emstoreid));
    }


    /**
     * 查询集合 已确认
     */
    @Override
    public Page<EMItemPL> searchConfirmed(EMItemPLSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemPL> pages=baseMapper.searchConfirmed(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemPL>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMItemPL> searchDefault(EMItemPLSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemPL> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemPL>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 草稿
     */
    @Override
    public Page<EMItemPL> searchDraft(EMItemPLSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemPL> pages=baseMapper.searchDraft(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemPL>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 待确认
     */
    @Override
    public Page<EMItemPL> searchToConfirm(EMItemPLSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemPL> pages=baseMapper.searchToConfirm(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemPL>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMItemPL et){
        //实体关系[DER1N_EMITEMPL_EMITEMRIN_RID]
        if(!ObjectUtils.isEmpty(et.getRid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItemRIn r=et.getR();
            if(ObjectUtils.isEmpty(r)){
                cn.ibizlab.eam.core.eam_core.domain.EMItemRIn majorEntity=emitemrinService.get(et.getRid());
                et.setR(majorEntity);
                r=majorEntity;
            }
            et.setRname(r.getEmitemrinname());
        }
        //实体关系[DER1N_EMITEMPL_EMITEM_ITEMID]
        if(!ObjectUtils.isEmpty(et.getItemid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItem item=et.getItem();
            if(ObjectUtils.isEmpty(item)){
                cn.ibizlab.eam.core.eam_core.domain.EMItem majorEntity=emitemService.get(et.getItemid());
                et.setItem(majorEntity);
                item=majorEntity;
            }
            et.setItemname(item.getEmitemname());
        }
        //实体关系[DER1N_EMITEMPL_EMSTOREPART_STOREPARTID]
        if(!ObjectUtils.isEmpty(et.getStorepartid())){
            cn.ibizlab.eam.core.eam_core.domain.EMStorePart storepart=et.getStorepart();
            if(ObjectUtils.isEmpty(storepart)){
                cn.ibizlab.eam.core.eam_core.domain.EMStorePart majorEntity=emstorepartService.get(et.getStorepartid());
                et.setStorepart(majorEntity);
                storepart=majorEntity;
            }
            et.setStorepartname(storepart.getEmstorepartname());
        }
        //实体关系[DER1N_EMITEMPL_EMSTORE_STOREID]
        if(!ObjectUtils.isEmpty(et.getStoreid())){
            cn.ibizlab.eam.core.eam_core.domain.EMStore store=et.getStore();
            if(ObjectUtils.isEmpty(store)){
                cn.ibizlab.eam.core.eam_core.domain.EMStore majorEntity=emstoreService.get(et.getStoreid());
                et.setStore(majorEntity);
                store=majorEntity;
            }
            et.setStorename(store.getEmstorename());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMItemPLInheritMapping emitemplInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemTradeService emitemtradeService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMItemPL et){
        if(ObjectUtils.isEmpty(et.getEmitemplid()))
            et.setEmitemplid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMItemTrade emitemtrade =emitemplInheritMapping.toEmitemtrade(et);
        emitemtrade.set("emitemtradetype","PL");
        emitemtradeService.create(emitemtrade);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMItemPL> getEmitemplByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMItemPL> getEmitemplByEntities(List<EMItemPL> entities) {
        List ids =new ArrayList();
        for(EMItemPL entity : entities){
            Serializable id=entity.getEmitemplid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



