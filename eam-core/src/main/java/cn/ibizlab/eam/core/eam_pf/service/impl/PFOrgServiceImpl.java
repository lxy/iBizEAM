package cn.ibizlab.eam.core.eam_pf.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_pf.domain.PFOrg;
import cn.ibizlab.eam.core.eam_pf.filter.PFOrgSearchContext;
import cn.ibizlab.eam.core.eam_pf.service.IPFOrgService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_pf.mapper.PFOrgMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[组织] 服务对象接口实现
 */
@Slf4j
@Service("PFOrgServiceImpl")
public class PFOrgServiceImpl extends ServiceImpl<PFOrgMapper, PFOrg> implements IPFOrgService {


    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(PFOrg et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getPforgid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<PFOrg> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(PFOrg et) {
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("pforgid",et.getPforgid())))
            return false;
        CachedBeanCopier.copy(get(et.getPforgid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<PFOrg> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public PFOrg get(String key) {
        PFOrg et = getById(key);
        if(et==null){
            et=new PFOrg();
            et.setPforgid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public PFOrg getDraft(PFOrg et) {
        return et;
    }

    @Override
    public boolean checkKey(PFOrg et) {
        return (!ObjectUtils.isEmpty(et.getPforgid()))&&(!Objects.isNull(this.getById(et.getPforgid())));
    }
    @Override
    @Transactional
    public boolean save(PFOrg et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(PFOrg et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<PFOrg> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<PFOrg> list) {
        saveOrUpdateBatch(list,batchSize);
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<PFOrg> searchDefault(PFOrgSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<PFOrg> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<PFOrg>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<PFOrg> getPforgByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<PFOrg> getPforgByEntities(List<PFOrg> entities) {
        List ids =new ArrayList();
        for(PFOrg entity : entities){
            Serializable id=entity.getPforgid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



