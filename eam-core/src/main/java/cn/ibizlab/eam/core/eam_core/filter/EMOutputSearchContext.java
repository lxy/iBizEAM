package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMOutput;
/**
 * 关系型数据实体[EMOutput] 查询条件对象
 */
@Slf4j
@Data
public class EMOutputSearchContext extends QueryWrapperContext<EMOutput> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emoutputname_like;//[能力名称]
	public void setN_emoutputname_like(String n_emoutputname_like) {
        this.n_emoutputname_like = n_emoutputname_like;
        if(!ObjectUtils.isEmpty(this.n_emoutputname_like)){
            this.getSearchCond().like("emoutputname", n_emoutputname_like);
        }
    }
	private String n_outputinfo_like;//[能力信息]
	public void setN_outputinfo_like(String n_outputinfo_like) {
        this.n_outputinfo_like = n_outputinfo_like;
        if(!ObjectUtils.isEmpty(this.n_outputinfo_like)){
            this.getSearchCond().like("outputinfo", n_outputinfo_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emoutputname", query)   
            );
		 }
	}
}



