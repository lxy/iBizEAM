package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEN;
import cn.ibizlab.eam.core.eam_core.filter.EMENSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEN] 服务对象接口
 */
public interface IEMENService extends IService<EMEN>{

    boolean create(EMEN et) ;
    void createBatch(List<EMEN> list) ;
    boolean update(EMEN et) ;
    void updateBatch(List<EMEN> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMEN get(String key) ;
    EMEN getDraft(EMEN et) ;
    boolean checkKey(EMEN et) ;
    boolean save(EMEN et) ;
    void saveBatch(List<EMEN> list) ;
    Page<EMEN> searchDefault(EMENSearchContext context) ;
    List<EMEN> selectByItemid(String emitemid) ;
    void removeByItemid(String emitemid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEN> getEmenByIds(List<String> ids) ;
    List<EMEN> getEmenByEntities(List<EMEN> entities) ;
}


