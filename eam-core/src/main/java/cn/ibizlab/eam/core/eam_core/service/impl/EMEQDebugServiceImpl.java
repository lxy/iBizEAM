package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQDebug;
import cn.ibizlab.eam.core.eam_core.filter.EMEQDebugSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQDebugService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQDebugMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[事故记录] 服务对象接口实现
 */
@Slf4j
@Service("EMEQDebugServiceImpl")
public class EMEQDebugServiceImpl extends ServiceImpl<EMEQDebugMapper, EMEQDebug> implements IEMEQDebugService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMACClassService emacclassService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFOACService emrfoacService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFOCAService emrfocaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFODEService emrfodeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFOMOService emrfomoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMServiceService emserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOService emwoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFTeamService pfteamService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQDebug et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmeqdebugid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQDebug> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQDebug et) {
        fillParentData(et);
        emeqahService.update(emeqdebugInheritMapping.toEmeqah(et));
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emeqdebugid",et.getEmeqdebugid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmeqdebugid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQDebug> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emeqahService.remove(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQDebug get(String key) {
        EMEQDebug et = getById(key);
        if(et==null){
            et=new EMEQDebug();
            et.setEmeqdebugid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMEQDebug getDraft(EMEQDebug et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEQDebug et) {
        return (!ObjectUtils.isEmpty(et.getEmeqdebugid()))&&(!Objects.isNull(this.getById(et.getEmeqdebugid())));
    }
    @Override
    @Transactional
    public boolean save(EMEQDebug et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQDebug et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQDebug> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQDebug> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMEQDebug> selectByAcclassid(String emacclassid) {
        return baseMapper.selectByAcclassid(emacclassid);
    }

    @Override
    public void removeByAcclassid(String emacclassid) {
        this.remove(new QueryWrapper<EMEQDebug>().eq("acclassid",emacclassid));
    }

	@Override
    public List<EMEQDebug> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }

    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMEQDebug>().eq("equipid",emequipid));
    }

	@Override
    public List<EMEQDebug> selectByObjid(String emobjectid) {
        return baseMapper.selectByObjid(emobjectid);
    }

    @Override
    public void removeByObjid(String emobjectid) {
        this.remove(new QueryWrapper<EMEQDebug>().eq("objid",emobjectid));
    }

	@Override
    public List<EMEQDebug> selectByRfoacid(String emrfoacid) {
        return baseMapper.selectByRfoacid(emrfoacid);
    }

    @Override
    public void removeByRfoacid(String emrfoacid) {
        this.remove(new QueryWrapper<EMEQDebug>().eq("rfoacid",emrfoacid));
    }

	@Override
    public List<EMEQDebug> selectByRfocaid(String emrfocaid) {
        return baseMapper.selectByRfocaid(emrfocaid);
    }

    @Override
    public void removeByRfocaid(String emrfocaid) {
        this.remove(new QueryWrapper<EMEQDebug>().eq("rfocaid",emrfocaid));
    }

	@Override
    public List<EMEQDebug> selectByRfodeid(String emrfodeid) {
        return baseMapper.selectByRfodeid(emrfodeid);
    }

    @Override
    public void removeByRfodeid(String emrfodeid) {
        this.remove(new QueryWrapper<EMEQDebug>().eq("rfodeid",emrfodeid));
    }

	@Override
    public List<EMEQDebug> selectByRfomoid(String emrfomoid) {
        return baseMapper.selectByRfomoid(emrfomoid);
    }

    @Override
    public void removeByRfomoid(String emrfomoid) {
        this.remove(new QueryWrapper<EMEQDebug>().eq("rfomoid",emrfomoid));
    }

	@Override
    public List<EMEQDebug> selectByRserviceid(String emserviceid) {
        return baseMapper.selectByRserviceid(emserviceid);
    }

    @Override
    public void removeByRserviceid(String emserviceid) {
        this.remove(new QueryWrapper<EMEQDebug>().eq("rserviceid",emserviceid));
    }

	@Override
    public List<EMEQDebug> selectByWoid(String emwoid) {
        return baseMapper.selectByWoid(emwoid);
    }

    @Override
    public void removeByWoid(String emwoid) {
        this.remove(new QueryWrapper<EMEQDebug>().eq("woid",emwoid));
    }

	@Override
    public List<EMEQDebug> selectByRteamid(String pfteamid) {
        return baseMapper.selectByRteamid(pfteamid);
    }

    @Override
    public void removeByRteamid(String pfteamid) {
        this.remove(new QueryWrapper<EMEQDebug>().eq("rteamid",pfteamid));
    }


    /**
     * 查询集合 日历查询
     */
    @Override
    public Page<EMEQDebug> searchCalendar(EMEQDebugSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQDebug> pages=baseMapper.searchCalendar(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQDebug>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQDebug> searchDefault(EMEQDebugSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQDebug> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQDebug>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEQDebug et){
        //实体关系[DER1N_EMEQDEBUG_EMACCLASS_ACCLASSID]
        if(!ObjectUtils.isEmpty(et.getAcclassid())){
            cn.ibizlab.eam.core.eam_core.domain.EMACClass acclass=et.getAcclass();
            if(ObjectUtils.isEmpty(acclass)){
                cn.ibizlab.eam.core.eam_core.domain.EMACClass majorEntity=emacclassService.get(et.getAcclassid());
                et.setAcclass(majorEntity);
                acclass=majorEntity;
            }
            et.setAcclassname(acclass.getEmacclassname());
        }
        //实体关系[DER1N_EMEQDEBUG_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMEQDEBUG_EMOBJECT_OBJID]
        if(!ObjectUtils.isEmpty(et.getObjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject obj=et.getObj();
            if(ObjectUtils.isEmpty(obj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getObjid());
                et.setObj(majorEntity);
                obj=majorEntity;
            }
            et.setObjname(obj.getEmobjectname());
        }
        //实体关系[DER1N_EMEQDEBUG_EMRFOAC_RFOACID]
        if(!ObjectUtils.isEmpty(et.getRfoacid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFOAC rfoac=et.getRfoac();
            if(ObjectUtils.isEmpty(rfoac)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFOAC majorEntity=emrfoacService.get(et.getRfoacid());
                et.setRfoac(majorEntity);
                rfoac=majorEntity;
            }
            et.setRfoacname(rfoac.getEmrfoacname());
        }
        //实体关系[DER1N_EMEQDEBUG_EMRFOCA_RFOCAID]
        if(!ObjectUtils.isEmpty(et.getRfocaid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFOCA rfoca=et.getRfoca();
            if(ObjectUtils.isEmpty(rfoca)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFOCA majorEntity=emrfocaService.get(et.getRfocaid());
                et.setRfoca(majorEntity);
                rfoca=majorEntity;
            }
            et.setRfocaname(rfoca.getEmrfocaname());
        }
        //实体关系[DER1N_EMEQDEBUG_EMRFODE_RFODEID]
        if(!ObjectUtils.isEmpty(et.getRfodeid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFODE rfode=et.getRfode();
            if(ObjectUtils.isEmpty(rfode)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFODE majorEntity=emrfodeService.get(et.getRfodeid());
                et.setRfode(majorEntity);
                rfode=majorEntity;
            }
            et.setRfodename(rfode.getEmrfodename());
        }
        //实体关系[DER1N_EMEQDEBUG_EMRFOMO_RFOMOID]
        if(!ObjectUtils.isEmpty(et.getRfomoid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFOMO rfomo=et.getRfomo();
            if(ObjectUtils.isEmpty(rfomo)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFOMO majorEntity=emrfomoService.get(et.getRfomoid());
                et.setRfomo(majorEntity);
                rfomo=majorEntity;
            }
            et.setRfomoname(rfomo.getEmrfomoname());
        }
        //实体关系[DER1N_EMEQDEBUG_EMSERVICE_RSERVICEID]
        if(!ObjectUtils.isEmpty(et.getRserviceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService rservice=et.getRservice();
            if(ObjectUtils.isEmpty(rservice)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getRserviceid());
                et.setRservice(majorEntity);
                rservice=majorEntity;
            }
            et.setRservicename(rservice.getEmservicename());
        }
        //实体关系[DER1N_EMEQDEBUG_EMWO_WOID]
        if(!ObjectUtils.isEmpty(et.getWoid())){
            cn.ibizlab.eam.core.eam_core.domain.EMWO wo=et.getWo();
            if(ObjectUtils.isEmpty(wo)){
                cn.ibizlab.eam.core.eam_core.domain.EMWO majorEntity=emwoService.get(et.getWoid());
                et.setWo(majorEntity);
                wo=majorEntity;
            }
            et.setWoname(wo.getEmwoname());
        }
        //实体关系[DER1N_EMEQDEBUG_PFTEAM_RTEAMID]
        if(!ObjectUtils.isEmpty(et.getRteamid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFTeam rteam=et.getRteam();
            if(ObjectUtils.isEmpty(rteam)){
                cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorEntity=pfteamService.get(et.getRteamid());
                et.setRteam(majorEntity);
                rteam=majorEntity;
            }
            et.setRteamname(rteam.getPfteamname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMEQDebugInheritMapping emeqdebugInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQAHService emeqahService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMEQDebug et){
        if(ObjectUtils.isEmpty(et.getEmeqdebugid()))
            et.setEmeqdebugid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMEQAH emeqah =emeqdebugInheritMapping.toEmeqah(et);
        emeqah.set("emeqahtype","DEBUG");
        emeqahService.create(emeqah);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQDebug> getEmeqdebugByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQDebug> getEmeqdebugByEntities(List<EMEQDebug> entities) {
        List ids =new ArrayList();
        for(EMEQDebug entity : entities){
            Serializable id=entity.getEmeqdebugid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



