package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMItemPL;
import cn.ibizlab.eam.core.eam_core.filter.EMItemPLSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMItemPL] 服务对象接口
 */
public interface IEMItemPLService extends IService<EMItemPL>{

    boolean create(EMItemPL et) ;
    void createBatch(List<EMItemPL> list) ;
    boolean update(EMItemPL et) ;
    void updateBatch(List<EMItemPL> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMItemPL get(String key) ;
    EMItemPL getDraft(EMItemPL et) ;
    boolean checkKey(EMItemPL et) ;
    boolean save(EMItemPL et) ;
    void saveBatch(List<EMItemPL> list) ;
    Page<EMItemPL> searchConfirmed(EMItemPLSearchContext context) ;
    Page<EMItemPL> searchDefault(EMItemPLSearchContext context) ;
    Page<EMItemPL> searchDraft(EMItemPLSearchContext context) ;
    Page<EMItemPL> searchToConfirm(EMItemPLSearchContext context) ;
    List<EMItemPL> selectByRid(String emitemrinid) ;
    void removeByRid(String emitemrinid) ;
    List<EMItemPL> selectByItemid(String emitemid) ;
    void removeByItemid(String emitemid) ;
    List<EMItemPL> selectByStorepartid(String emstorepartid) ;
    void removeByStorepartid(String emstorepartid) ;
    List<EMItemPL> selectByStoreid(String emstoreid) ;
    void removeByStoreid(String emstoreid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMItemPL> getEmitemplByIds(List<String> ids) ;
    List<EMItemPL> getEmitemplByEntities(List<EMItemPL> entities) ;
}


