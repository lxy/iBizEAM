package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMRigging;
import cn.ibizlab.eam.core.eam_core.filter.EMRiggingSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMRiggingService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMRiggingMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[工具库管理] 服务对象接口实现
 */
@Slf4j
@Service("EMRiggingServiceImpl")
public class EMRiggingServiceImpl extends ServiceImpl<EMRiggingMapper, EMRigging> implements IEMRiggingService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRiggingHService emrigginghService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService emitempuseService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMRigging et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmriggingid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMRigging> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMRigging et) {
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emriggingid",et.getEmriggingid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmriggingid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMRigging> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMRigging get(String key) {
        EMRigging et = getById(key);
        if(et==null){
            et=new EMRigging();
            et.setEmriggingid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMRigging getDraft(EMRigging et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMRigging et) {
        return (!ObjectUtils.isEmpty(et.getEmriggingid()))&&(!Objects.isNull(this.getById(et.getEmriggingid())));
    }
    @Override
    @Transactional
    public boolean save(EMRigging et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMRigging et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMRigging> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMRigging> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMRigging> selectByEmitempuseid(String emitempuseid) {
        return baseMapper.selectByEmitempuseid(emitempuseid);
    }

    @Override
    public void removeByEmitempuseid(String emitempuseid) {
        this.remove(new QueryWrapper<EMRigging>().eq("emitempuseid",emitempuseid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMRigging> searchDefault(EMRiggingSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMRigging> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMRigging>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMRigging et){
        //实体关系[DER1N_EMRIGGING_EMITEMPUSE_EMITEMPUSEID]
        if(!ObjectUtils.isEmpty(et.getEmitempuseid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItemPUse emitempuse=et.getEmitempuse();
            if(ObjectUtils.isEmpty(emitempuse)){
                cn.ibizlab.eam.core.eam_core.domain.EMItemPUse majorEntity=emitempuseService.get(et.getEmitempuseid());
                et.setEmitempuse(majorEntity);
                emitempuse=majorEntity;
            }
            et.setEmitempusename(emitempuse.getEmitempusename());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMRigging> getEmriggingByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMRigging> getEmriggingByEntities(List<EMRigging> entities) {
        List ids =new ArrayList();
        for(EMRigging entity : entities){
            Serializable id=entity.getEmriggingid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



