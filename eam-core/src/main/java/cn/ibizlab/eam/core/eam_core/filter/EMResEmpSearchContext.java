package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMResEmp;
/**
 * 关系型数据实体[EMResEmp] 查询条件对象
 */
@Slf4j
@Data
public class EMResEmpSearchContext extends QueryWrapperContext<EMResEmp> {

	private String n_resid_eq;//[员工]
	public void setN_resid_eq(String n_resid_eq) {
        this.n_resid_eq = n_resid_eq;
        if(!ObjectUtils.isEmpty(this.n_resid_eq)){
            this.getSearchCond().eq("resid", n_resid_eq);
        }
    }
	private String n_resname_eq;//[员工]
	public void setN_resname_eq(String n_resname_eq) {
        this.n_resname_eq = n_resname_eq;
        if(!ObjectUtils.isEmpty(this.n_resname_eq)){
            this.getSearchCond().eq("resname", n_resname_eq);
        }
    }
	private String n_resname_like;//[员工]
	public void setN_resname_like(String n_resname_like) {
        this.n_resname_like = n_resname_like;
        if(!ObjectUtils.isEmpty(this.n_resname_like)){
            this.getSearchCond().like("resname", n_resname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emresempname_like;//[员工资源名称]
	public void setN_emresempname_like(String n_emresempname_like) {
        this.n_emresempname_like = n_emresempname_like;
        if(!ObjectUtils.isEmpty(this.n_emresempname_like)){
            this.getSearchCond().like("emresempname", n_emresempname_like);
        }
    }
	private String n_equipname_eq;//[设备]
	public void setN_equipname_eq(String n_equipname_eq) {
        this.n_equipname_eq = n_equipname_eq;
        if(!ObjectUtils.isEmpty(this.n_equipname_eq)){
            this.getSearchCond().eq("equipname", n_equipname_eq);
        }
    }
	private String n_equipname_like;//[设备]
	public void setN_equipname_like(String n_equipname_like) {
        this.n_equipname_like = n_equipname_like;
        if(!ObjectUtils.isEmpty(this.n_equipname_like)){
            this.getSearchCond().like("equipname", n_equipname_like);
        }
    }
	private String n_resrefobjname_eq;//[引用对象]
	public void setN_resrefobjname_eq(String n_resrefobjname_eq) {
        this.n_resrefobjname_eq = n_resrefobjname_eq;
        if(!ObjectUtils.isEmpty(this.n_resrefobjname_eq)){
            this.getSearchCond().eq("resrefobjname", n_resrefobjname_eq);
        }
    }
	private String n_resrefobjname_like;//[引用对象]
	public void setN_resrefobjname_like(String n_resrefobjname_like) {
        this.n_resrefobjname_like = n_resrefobjname_like;
        if(!ObjectUtils.isEmpty(this.n_resrefobjname_like)){
            this.getSearchCond().like("resrefobjname", n_resrefobjname_like);
        }
    }
	private String n_equipid_eq;//[设备]
	public void setN_equipid_eq(String n_equipid_eq) {
        this.n_equipid_eq = n_equipid_eq;
        if(!ObjectUtils.isEmpty(this.n_equipid_eq)){
            this.getSearchCond().eq("equipid", n_equipid_eq);
        }
    }
	private String n_resrefobjid_eq;//[引用对象]
	public void setN_resrefobjid_eq(String n_resrefobjid_eq) {
        this.n_resrefobjid_eq = n_resrefobjid_eq;
        if(!ObjectUtils.isEmpty(this.n_resrefobjid_eq)){
            this.getSearchCond().eq("resrefobjid", n_resrefobjid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emresempname", query)   
            );
		 }
	}
}



