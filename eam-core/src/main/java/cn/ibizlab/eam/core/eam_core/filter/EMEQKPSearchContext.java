package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEQKP;
/**
 * 关系型数据实体[EMEQKP] 查询条件对象
 */
@Slf4j
@Data
public class EMEQKPSearchContext extends QueryWrapperContext<EMEQKP> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_kpinfo_like;//[关键点信息]
	public void setN_kpinfo_like(String n_kpinfo_like) {
        this.n_kpinfo_like = n_kpinfo_like;
        if(!ObjectUtils.isEmpty(this.n_kpinfo_like)){
            this.getSearchCond().like("kpinfo", n_kpinfo_like);
        }
    }
	private String n_kptypeid_eq;//[关键点类型]
	public void setN_kptypeid_eq(String n_kptypeid_eq) {
        this.n_kptypeid_eq = n_kptypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_kptypeid_eq)){
            this.getSearchCond().eq("kptypeid", n_kptypeid_eq);
        }
    }
	private String n_emeqkpname_like;//[关键点名称]
	public void setN_emeqkpname_like(String n_emeqkpname_like) {
        this.n_emeqkpname_like = n_emeqkpname_like;
        if(!ObjectUtils.isEmpty(this.n_emeqkpname_like)){
            this.getSearchCond().like("emeqkpname", n_emeqkpname_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emeqkpname", query)   
            );
		 }
	}
}



