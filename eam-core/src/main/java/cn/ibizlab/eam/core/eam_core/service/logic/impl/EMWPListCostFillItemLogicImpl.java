package cn.ibizlab.eam.core.eam_core.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.eam.core.eam_core.service.logic.IEMWPListCostFillItemLogic;
import cn.ibizlab.eam.core.eam_core.domain.EMWPListCost;

/**
 * 关系型数据实体[FillItem] 对象
 */
@Slf4j
@Service
public class EMWPListCostFillItemLogicImpl implements IEMWPListCostFillItemLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMWPListService emwplistservice;

    public cn.ibizlab.eam.core.eam_core.service.IEMWPListService getEmwplistService() {
        return this.emwplistservice;
    }


    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMWPListCostService iBzSysDefaultService;

    public cn.ibizlab.eam.core.eam_core.service.IEMWPListCostService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(EMWPListCost et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           cn.ibizlab.eam.core.eam_core.domain.EMWPList  emwplistcostfillitemwplist =new cn.ibizlab.eam.core.eam_core.domain.EMWPList();
           kieSession.insert(emwplistcostfillitemwplist); 
           kieSession.setGlobal("emwplistcostfillitemwplist",emwplistcostfillitemwplist);
           kieSession.insert(et); 
           kieSession.setGlobal("emwplistcostfillitemdefault",et);
           kieSession.setGlobal("emwplistservice",emwplistservice);
           kieSession.setGlobal("iBzSysEmwplistcostDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.eam.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.eam.core.eam_core.service.logic.emwplistcostfillitem");

        }catch(Exception e){
            throw new RuntimeException("执行[FillItem]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}
