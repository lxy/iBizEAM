

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMDRWG;
import cn.ibizlab.eam.core.eam_core.domain.EMObject;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMDRWGInheritMapping {

    @Mappings({
        @Mapping(source ="emdrwgid",target = "emobjectid"),
        @Mapping(source ="emdrwgname",target = "emobjectname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="drwgcode",target = "objectcode"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObject toEmobject(EMDRWG emdrwg);

    @Mappings({
        @Mapping(source ="emobjectid" ,target = "emdrwgid"),
        @Mapping(source ="emobjectname" ,target = "emdrwgname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objectcode",target = "drwgcode"),
    })
    EMDRWG toEmdrwg(EMObject emobject);

    List<EMObject> toEmobject(List<EMDRWG> emdrwg);

    List<EMDRWG> toEmdrwg(List<EMObject> emobject);

}


