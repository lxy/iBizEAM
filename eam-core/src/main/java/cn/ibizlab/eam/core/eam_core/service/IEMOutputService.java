package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMOutput;
import cn.ibizlab.eam.core.eam_core.filter.EMOutputSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMOutput] 服务对象接口
 */
public interface IEMOutputService extends IService<EMOutput>{

    boolean create(EMOutput et) ;
    void createBatch(List<EMOutput> list) ;
    boolean update(EMOutput et) ;
    void updateBatch(List<EMOutput> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMOutput get(String key) ;
    EMOutput getDraft(EMOutput et) ;
    boolean checkKey(EMOutput et) ;
    boolean save(EMOutput et) ;
    void saveBatch(List<EMOutput> list) ;
    Page<EMOutput> searchDefault(EMOutputSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMOutput> getEmoutputByIds(List<String> ids) ;
    List<EMOutput> getEmoutputByEntities(List<EMOutput> entities) ;
}


