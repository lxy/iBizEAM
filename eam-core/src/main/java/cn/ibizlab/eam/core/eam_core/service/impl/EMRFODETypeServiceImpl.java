package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMRFODEType;
import cn.ibizlab.eam.core.eam_core.filter.EMRFODETypeSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMRFODETypeService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMRFODETypeMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[现象分类] 服务对象接口实现
 */
@Slf4j
@Service("EMRFODETypeServiceImpl")
public class EMRFODETypeServiceImpl extends ServiceImpl<EMRFODETypeMapper, EMRFODEType> implements IEMRFODETypeService {


    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMRFODEType et) {
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmrfodetypeid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMRFODEType> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMRFODEType et) {
        emobjectService.update(emrfodetypeInheritMapping.toEmobject(et));
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emrfodetypeid",et.getEmrfodetypeid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmrfodetypeid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMRFODEType> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emobjectService.remove(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMRFODEType get(String key) {
        EMRFODEType et = getById(key);
        if(et==null){
            et=new EMRFODEType();
            et.setEmrfodetypeid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMRFODEType getDraft(EMRFODEType et) {
        return et;
    }

    @Override
    public boolean checkKey(EMRFODEType et) {
        return (!ObjectUtils.isEmpty(et.getEmrfodetypeid()))&&(!Objects.isNull(this.getById(et.getEmrfodetypeid())));
    }
    @Override
    @Transactional
    public boolean save(EMRFODEType et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMRFODEType et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMRFODEType> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMRFODEType> list) {
        saveOrUpdateBatch(list,batchSize);
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMRFODEType> searchDefault(EMRFODETypeSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMRFODEType> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMRFODEType>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }






    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMRFODETypeInheritMapping emrfodetypeInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMRFODEType et){
        if(ObjectUtils.isEmpty(et.getEmrfodetypeid()))
            et.setEmrfodetypeid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMObject emobject =emrfodetypeInheritMapping.toEmobject(et);
        emobject.set("emobjecttype","RFODETYPE");
        emobjectService.create(emobject);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMRFODEType> getEmrfodetypeByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMRFODEType> getEmrfodetypeByEntities(List<EMRFODEType> entities) {
        List ids =new ArrayList();
        for(EMRFODEType entity : entities){
            Serializable id=entity.getEmrfodetypeid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



