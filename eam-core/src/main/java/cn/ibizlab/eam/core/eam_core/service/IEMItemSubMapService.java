package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMItemSubMap;
import cn.ibizlab.eam.core.eam_core.filter.EMItemSubMapSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMItemSubMap] 服务对象接口
 */
public interface IEMItemSubMapService extends IService<EMItemSubMap>{

    boolean create(EMItemSubMap et) ;
    void createBatch(List<EMItemSubMap> list) ;
    boolean update(EMItemSubMap et) ;
    void updateBatch(List<EMItemSubMap> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMItemSubMap get(String key) ;
    EMItemSubMap getDraft(EMItemSubMap et) ;
    boolean checkKey(EMItemSubMap et) ;
    boolean save(EMItemSubMap et) ;
    void saveBatch(List<EMItemSubMap> list) ;
    Page<EMItemSubMap> searchDefault(EMItemSubMapSearchContext context) ;
    List<EMItemSubMap> selectByItemid(String emitemid) ;
    void removeByItemid(String emitemid) ;
    List<EMItemSubMap> selectBySubitemid(String emitemid) ;
    void removeBySubitemid(String emitemid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMItemSubMap> getEmitemsubmapByIds(List<String> ids) ;
    List<EMItemSubMap> getEmitemsubmapByEntities(List<EMItemSubMap> entities) ;
}


