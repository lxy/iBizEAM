

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQMPMTR;
import cn.ibizlab.eam.core.eam_core.domain.EMDPRCT;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQMPMTRInheritMapping {

    @Mappings({
        @Mapping(source ="emeqmpmtrid",target = "emdprctid"),
        @Mapping(source ="emeqmpmtrname",target = "emdprctname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="mpname",target = "dpname"),
        @Mapping(source ="mpid",target = "dpid"),
    })
    EMDPRCT toEmdprct(EMEQMPMTR emeqmpmtr);

    @Mappings({
        @Mapping(source ="emdprctid" ,target = "emeqmpmtrid"),
        @Mapping(source ="emdprctname" ,target = "emeqmpmtrname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="dpname",target = "mpname"),
        @Mapping(source ="dpid",target = "mpid"),
    })
    EMEQMPMTR toEmeqmpmtr(EMDPRCT emdprct);

    List<EMDPRCT> toEmdprct(List<EMEQMPMTR> emeqmpmtr);

    List<EMEQMPMTR> toEmeqmpmtr(List<EMDPRCT> emdprct);

}


