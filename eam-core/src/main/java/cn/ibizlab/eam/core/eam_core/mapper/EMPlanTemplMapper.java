package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMPlanTempl;
import cn.ibizlab.eam.core.eam_core.filter.EMPlanTemplSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMPlanTemplMapper extends BaseMapper<EMPlanTempl>{

    Page<EMPlanTempl> searchDefault(IPage page, @Param("srf") EMPlanTemplSearchContext context, @Param("ew") Wrapper<EMPlanTempl> wrapper) ;
    @Override
    EMPlanTempl selectById(Serializable id);
    @Override
    int insert(EMPlanTempl entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMPlanTempl entity);
    @Override
    int update(@Param(Constants.ENTITY) EMPlanTempl entity, @Param("ew") Wrapper<EMPlanTempl> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMPlanTempl> selectByAcclassid(@Param("emacclassid") Serializable emacclassid) ;

    List<EMPlanTempl> selectByRserviceid(@Param("emserviceid") Serializable emserviceid) ;

    List<EMPlanTempl> selectByRteamid(@Param("pfteamid") Serializable pfteamid) ;

}
