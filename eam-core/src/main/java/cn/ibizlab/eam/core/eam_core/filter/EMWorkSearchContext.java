package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMWork;
/**
 * 关系型数据实体[EMWork] 查询条件对象
 */
@Slf4j
@Data
public class EMWorkSearchContext extends QueryWrapperContext<EMWork> {

	private String n_empname_eq;//[员工]
	public void setN_empname_eq(String n_empname_eq) {
        this.n_empname_eq = n_empname_eq;
        if(!ObjectUtils.isEmpty(this.n_empname_eq)){
            this.getSearchCond().eq("empname", n_empname_eq);
        }
    }
	private String n_empname_like;//[员工]
	public void setN_empname_like(String n_empname_like) {
        this.n_empname_like = n_empname_like;
        if(!ObjectUtils.isEmpty(this.n_empname_like)){
            this.getSearchCond().like("empname", n_empname_like);
        }
    }
	private String n_workstate_eq;//[加班区分]
	public void setN_workstate_eq(String n_workstate_eq) {
        this.n_workstate_eq = n_workstate_eq;
        if(!ObjectUtils.isEmpty(this.n_workstate_eq)){
            this.getSearchCond().eq("workstate", n_workstate_eq);
        }
    }
	private String n_empid_eq;//[员工]
	public void setN_empid_eq(String n_empid_eq) {
        this.n_empid_eq = n_empid_eq;
        if(!ObjectUtils.isEmpty(this.n_empid_eq)){
            this.getSearchCond().eq("empid", n_empid_eq);
        }
    }
	private String n_emworkname_like;//[加班工单名称]
	public void setN_emworkname_like(String n_emworkname_like) {
        this.n_emworkname_like = n_emworkname_like;
        if(!ObjectUtils.isEmpty(this.n_emworkname_like)){
            this.getSearchCond().like("emworkname", n_emworkname_like);
        }
    }
	private String n_wfstep_eq;//[流程步骤]
	public void setN_wfstep_eq(String n_wfstep_eq) {
        this.n_wfstep_eq = n_wfstep_eq;
        if(!ObjectUtils.isEmpty(this.n_wfstep_eq)){
            this.getSearchCond().eq("wfstep", n_wfstep_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emworkname", query)   
            );
		 }
	}
}



