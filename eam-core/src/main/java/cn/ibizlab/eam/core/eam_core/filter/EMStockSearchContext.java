package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMStock;
/**
 * 关系型数据实体[EMStock] 查询条件对象
 */
@Slf4j
@Data
public class EMStockSearchContext extends QueryWrapperContext<EMStock> {

	private String n_stockinfo_like;//[库存信息]
	public void setN_stockinfo_like(String n_stockinfo_like) {
        this.n_stockinfo_like = n_stockinfo_like;
        if(!ObjectUtils.isEmpty(this.n_stockinfo_like)){
            this.getSearchCond().like("stockinfo", n_stockinfo_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emstockname_like;//[库存名称]
	public void setN_emstockname_like(String n_emstockname_like) {
        this.n_emstockname_like = n_emstockname_like;
        if(!ObjectUtils.isEmpty(this.n_emstockname_like)){
            this.getSearchCond().like("emstockname", n_emstockname_like);
        }
    }
	private String n_storepartname_eq;//[库位]
	public void setN_storepartname_eq(String n_storepartname_eq) {
        this.n_storepartname_eq = n_storepartname_eq;
        if(!ObjectUtils.isEmpty(this.n_storepartname_eq)){
            this.getSearchCond().eq("storepartname", n_storepartname_eq);
        }
    }
	private String n_storepartname_like;//[库位]
	public void setN_storepartname_like(String n_storepartname_like) {
        this.n_storepartname_like = n_storepartname_like;
        if(!ObjectUtils.isEmpty(this.n_storepartname_like)){
            this.getSearchCond().like("storepartname", n_storepartname_like);
        }
    }
	private String n_storename_eq;//[仓库]
	public void setN_storename_eq(String n_storename_eq) {
        this.n_storename_eq = n_storename_eq;
        if(!ObjectUtils.isEmpty(this.n_storename_eq)){
            this.getSearchCond().eq("storename", n_storename_eq);
        }
    }
	private String n_storename_like;//[仓库]
	public void setN_storename_like(String n_storename_like) {
        this.n_storename_like = n_storename_like;
        if(!ObjectUtils.isEmpty(this.n_storename_like)){
            this.getSearchCond().like("storename", n_storename_like);
        }
    }
	private String n_itemname_eq;//[物品]
	public void setN_itemname_eq(String n_itemname_eq) {
        this.n_itemname_eq = n_itemname_eq;
        if(!ObjectUtils.isEmpty(this.n_itemname_eq)){
            this.getSearchCond().eq("itemname", n_itemname_eq);
        }
    }
	private String n_itemname_like;//[物品]
	public void setN_itemname_like(String n_itemname_like) {
        this.n_itemname_like = n_itemname_like;
        if(!ObjectUtils.isEmpty(this.n_itemname_like)){
            this.getSearchCond().like("itemname", n_itemname_like);
        }
    }
	private String n_storepartid_eq;//[库位]
	public void setN_storepartid_eq(String n_storepartid_eq) {
        this.n_storepartid_eq = n_storepartid_eq;
        if(!ObjectUtils.isEmpty(this.n_storepartid_eq)){
            this.getSearchCond().eq("storepartid", n_storepartid_eq);
        }
    }
	private String n_itemid_eq;//[物品]
	public void setN_itemid_eq(String n_itemid_eq) {
        this.n_itemid_eq = n_itemid_eq;
        if(!ObjectUtils.isEmpty(this.n_itemid_eq)){
            this.getSearchCond().eq("itemid", n_itemid_eq);
        }
    }
	private String n_storeid_eq;//[仓库]
	public void setN_storeid_eq(String n_storeid_eq) {
        this.n_storeid_eq = n_storeid_eq;
        if(!ObjectUtils.isEmpty(this.n_storeid_eq)){
            this.getSearchCond().eq("storeid", n_storeid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emstockname", query)   
            );
		 }
	}
}



