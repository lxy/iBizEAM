package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMOutputRct;
import cn.ibizlab.eam.core.eam_core.filter.EMOutputRctSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMOutputRctService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMOutputRctMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[产能] 服务对象接口实现
 */
@Slf4j
@Service("EMOutputRctServiceImpl")
public class EMOutputRctServiceImpl extends ServiceImpl<EMOutputRctMapper, EMOutputRct> implements IEMOutputRctService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMOutputService emoutputService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOService emwoService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMOutputRct et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmoutputrctid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMOutputRct> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMOutputRct et) {
        fillParentData(et);
        emdprctService.update(emoutputrctInheritMapping.toEmdprct(et));
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emoutputrctid",et.getEmoutputrctid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmoutputrctid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMOutputRct> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emdprctService.remove(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMOutputRct get(String key) {
        EMOutputRct et = getById(key);
        if(et==null){
            et=new EMOutputRct();
            et.setEmoutputrctid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMOutputRct getDraft(EMOutputRct et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMOutputRct et) {
        return (!ObjectUtils.isEmpty(et.getEmoutputrctid()))&&(!Objects.isNull(this.getById(et.getEmoutputrctid())));
    }
    @Override
    @Transactional
    public boolean save(EMOutputRct et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMOutputRct et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMOutputRct> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMOutputRct> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMOutputRct> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }

    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMOutputRct>().eq("equipid",emequipid));
    }

	@Override
    public List<EMOutputRct> selectByObjid(String emobjectid) {
        return baseMapper.selectByObjid(emobjectid);
    }

    @Override
    public void removeByObjid(String emobjectid) {
        this.remove(new QueryWrapper<EMOutputRct>().eq("objid",emobjectid));
    }

	@Override
    public List<EMOutputRct> selectByOutputid(String emoutputid) {
        return baseMapper.selectByOutputid(emoutputid);
    }

    @Override
    public void removeByOutputid(String emoutputid) {
        this.remove(new QueryWrapper<EMOutputRct>().eq("outputid",emoutputid));
    }

	@Override
    public List<EMOutputRct> selectByWoid(String emwoid) {
        return baseMapper.selectByWoid(emwoid);
    }

    @Override
    public void removeByWoid(String emwoid) {
        this.remove(new QueryWrapper<EMOutputRct>().eq("woid",emwoid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMOutputRct> searchDefault(EMOutputRctSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMOutputRct> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMOutputRct>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMOutputRct et){
        //实体关系[DER1N_EMOUTPUTRCT_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMOUTPUTRCT_EMOBJECT_OBJID]
        if(!ObjectUtils.isEmpty(et.getObjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject obj=et.getObj();
            if(ObjectUtils.isEmpty(obj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getObjid());
                et.setObj(majorEntity);
                obj=majorEntity;
            }
            et.setObjname(obj.getEmobjectname());
        }
        //实体关系[DER1N_EMOUTPUTRCT_EMOUTPUT_OUTPUTID]
        if(!ObjectUtils.isEmpty(et.getOutputid())){
            cn.ibizlab.eam.core.eam_core.domain.EMOutput output=et.getOutput();
            if(ObjectUtils.isEmpty(output)){
                cn.ibizlab.eam.core.eam_core.domain.EMOutput majorEntity=emoutputService.get(et.getOutputid());
                et.setOutput(majorEntity);
                output=majorEntity;
            }
            et.setOutputname(output.getEmoutputname());
        }
        //实体关系[DER1N_EMOUTPUTRCT_EMWO_WOID]
        if(!ObjectUtils.isEmpty(et.getWoid())){
            cn.ibizlab.eam.core.eam_core.domain.EMWO wo=et.getWo();
            if(ObjectUtils.isEmpty(wo)){
                cn.ibizlab.eam.core.eam_core.domain.EMWO majorEntity=emwoService.get(et.getWoid());
                et.setWo(majorEntity);
                wo=majorEntity;
            }
            et.setWoname(wo.getEmwoname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMOutputRctInheritMapping emoutputrctInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMDPRCTService emdprctService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMOutputRct et){
        if(ObjectUtils.isEmpty(et.getEmoutputrctid()))
            et.setEmoutputrctid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMDPRCT emdprct =emoutputrctInheritMapping.toEmdprct(et);
        emdprct.set("emdprcttype","OUTPUTRCT");
        emdprctService.create(emdprct);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMOutputRct> getEmoutputrctByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMOutputRct> getEmoutputrctByEntities(List<EMOutputRct> entities) {
        List ids =new ArrayList();
        for(EMOutputRct entity : entities){
            Serializable id=entity.getEmoutputrctid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



