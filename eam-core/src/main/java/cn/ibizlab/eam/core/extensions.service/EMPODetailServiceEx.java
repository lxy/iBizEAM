package cn.ibizlab.eam.core.extensions.service;

/**
 * 扩展目录已变更，请到[cn.ibizlab.eam.core.extensions.service.xxExService]中来进行扩展
 * 若您之前有在当前目录下扩展过其它的service对象，请将扩展的代码移到新的扩展类中，并注释掉老的扩展类，防止Bean重复
 */
@Deprecated
public class EMPODetailServiceEx{

}


