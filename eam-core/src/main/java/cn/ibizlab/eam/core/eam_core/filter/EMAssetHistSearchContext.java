package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMAssetHist;
/**
 * 关系型数据实体[EMAssetHist] 查询条件对象
 */
@Slf4j
@Data
public class EMAssetHistSearchContext extends QueryWrapperContext<EMAssetHist> {

	private String n_empid_eq;//[责任人]
	public void setN_empid_eq(String n_empid_eq) {
        this.n_empid_eq = n_empid_eq;
        if(!ObjectUtils.isEmpty(this.n_empid_eq)){
            this.getSearchCond().eq("empid", n_empid_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_empname_eq;//[责任人]
	public void setN_empname_eq(String n_empname_eq) {
        this.n_empname_eq = n_empname_eq;
        if(!ObjectUtils.isEmpty(this.n_empname_eq)){
            this.getSearchCond().eq("empname", n_empname_eq);
        }
    }
	private String n_empname_like;//[责任人]
	public void setN_empname_like(String n_empname_like) {
        this.n_empname_like = n_empname_like;
        if(!ObjectUtils.isEmpty(this.n_empname_like)){
            this.getSearchCond().like("empname", n_empname_like);
        }
    }
	private String n_emassethistname_like;//[记录内容]
	public void setN_emassethistname_like(String n_emassethistname_like) {
        this.n_emassethistname_like = n_emassethistname_like;
        if(!ObjectUtils.isEmpty(this.n_emassethistname_like)){
            this.getSearchCond().like("emassethistname", n_emassethistname_like);
        }
    }
	private String n_assetname_eq;//[资产]
	public void setN_assetname_eq(String n_assetname_eq) {
        this.n_assetname_eq = n_assetname_eq;
        if(!ObjectUtils.isEmpty(this.n_assetname_eq)){
            this.getSearchCond().eq("assetname", n_assetname_eq);
        }
    }
	private String n_assetname_like;//[资产]
	public void setN_assetname_like(String n_assetname_like) {
        this.n_assetname_like = n_assetname_like;
        if(!ObjectUtils.isEmpty(this.n_assetname_like)){
            this.getSearchCond().like("assetname", n_assetname_like);
        }
    }
	private String n_assetid_eq;//[资产]
	public void setN_assetid_eq(String n_assetid_eq) {
        this.n_assetid_eq = n_assetid_eq;
        if(!ObjectUtils.isEmpty(this.n_assetid_eq)){
            this.getSearchCond().eq("assetid", n_assetid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emassethistname", query)   
            );
		 }
	}
}



