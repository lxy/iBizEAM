package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMAssetClear;
import cn.ibizlab.eam.core.eam_core.filter.EMAssetClearSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMAssetClearService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMAssetClearMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[资产清盘记录] 服务对象接口实现
 */
@Slf4j
@Service("EMAssetClearServiceImpl")
public class EMAssetClearServiceImpl extends ServiceImpl<EMAssetClearMapper, EMAssetClear> implements IEMAssetClearService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMAssetService emassetService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMAssetClear et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmassetclearid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMAssetClear> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMAssetClear et) {
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emassetclearid",et.getEmassetclearid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmassetclearid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMAssetClear> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMAssetClear get(String key) {
        EMAssetClear et = getById(key);
        if(et==null){
            et=new EMAssetClear();
            et.setEmassetclearid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMAssetClear getDraft(EMAssetClear et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMAssetClear et) {
        return (!ObjectUtils.isEmpty(et.getEmassetclearid()))&&(!Objects.isNull(this.getById(et.getEmassetclearid())));
    }
    @Override
    @Transactional
    public boolean save(EMAssetClear et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMAssetClear et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMAssetClear> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMAssetClear> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMAssetClear> selectByEmassetid(String emassetid) {
        return baseMapper.selectByEmassetid(emassetid);
    }

    @Override
    public void removeByEmassetid(String emassetid) {
        this.remove(new QueryWrapper<EMAssetClear>().eq("emassetid",emassetid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMAssetClear> searchDefault(EMAssetClearSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMAssetClear> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMAssetClear>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMAssetClear et){
        //实体关系[DER1N_EMASSETCLEAR_EMASSET_EMASSETID]
        if(!ObjectUtils.isEmpty(et.getEmassetid())){
            cn.ibizlab.eam.core.eam_core.domain.EMAsset emasset=et.getEmasset();
            if(ObjectUtils.isEmpty(emasset)){
                cn.ibizlab.eam.core.eam_core.domain.EMAsset majorEntity=emassetService.get(et.getEmassetid());
                et.setEmasset(majorEntity);
                emasset=majorEntity;
            }
            et.setAssetsort(emasset.getAssetsort());
            et.setAssetcode(emasset.getAssetcode());
            et.setAssetclassname(emasset.getAssettype());
            et.setEqmodelcode(emasset.getEqmodelcode());
            et.setPurchdate(emasset.getPurchdate());
            et.setOriginalcost(emasset.getOriginalcost());
            et.setAssetclassid(emasset.getAssetclassid());
            et.setEmassetname(emasset.getEmassetname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMAssetClear> getEmassetclearByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMAssetClear> getEmassetclearByEntities(List<EMAssetClear> entities) {
        List ids =new ArrayList();
        for(EMAssetClear entity : entities){
            Serializable id=entity.getEmassetclearid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



