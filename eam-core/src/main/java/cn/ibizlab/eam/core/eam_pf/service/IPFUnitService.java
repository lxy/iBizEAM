package cn.ibizlab.eam.core.eam_pf.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_pf.domain.PFUnit;
import cn.ibizlab.eam.core.eam_pf.filter.PFUnitSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[PFUnit] 服务对象接口
 */
public interface IPFUnitService extends IService<PFUnit>{

    boolean create(PFUnit et) ;
    void createBatch(List<PFUnit> list) ;
    boolean update(PFUnit et) ;
    void updateBatch(List<PFUnit> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    PFUnit get(String key) ;
    PFUnit getDraft(PFUnit et) ;
    boolean checkKey(PFUnit et) ;
    boolean save(PFUnit et) ;
    void saveBatch(List<PFUnit> list) ;
    Page<PFUnit> searchDefault(PFUnitSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<PFUnit> getPfunitByIds(List<String> ids) ;
    List<PFUnit> getPfunitByEntities(List<PFUnit> entities) ;
}


