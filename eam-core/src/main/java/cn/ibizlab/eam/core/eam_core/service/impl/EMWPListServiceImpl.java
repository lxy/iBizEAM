package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMWPList;
import cn.ibizlab.eam.core.eam_core.filter.EMWPListSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMWPListService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMWPListMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[采购申请] 服务对象接口实现
 */
@Slf4j
@Service("EMWPListServiceImpl")
public class EMWPListServiceImpl extends ServiceImpl<EMWPListMapper, EMWPList> implements IEMWPListService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPODetailService empodetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWPListCostService emwplistcostService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMServiceService emserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFTeamService pfteamService;

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.logic.IEMWPListGenIdLogic genidLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.logic.IEMWPListGetREMPLogic getrempLogic;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMWPList et) {
        fillParentData(et);
        genidLogic.execute(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmwplistid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMWPList> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMWPList et) {
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emwplistid",et.getEmwplistid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmwplistid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMWPList> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMWPList get(String key) {
        EMWPList et = getById(key);
        if(et==null){
            et=new EMWPList();
            et.setEmwplistid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMWPList getDraft(EMWPList et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMWPList et) {
        return (!ObjectUtils.isEmpty(et.getEmwplistid()))&&(!Objects.isNull(this.getById(et.getEmwplistid())));
    }
        @Override
    @Transactional
    public EMWPList fillCosted(EMWPList et) {
        et.setWfstep("50");
        this.update(et);
        return et;
    }
        @Override
    @Transactional
    public EMWPList genId(EMWPList et) {
        String strId = "S" + System.currentTimeMillis() ;
        et.setEmwplistid(strId);
        return et;
    }
    
    @Autowired
    cn.ibizlab.eam.core.eam_core.service.IEMPOService empoService ;

    @Override
    @Transactional
    public EMWPList genPO(EMWPList et) {
        String[] listids  = et.getEmwplistid().split(",") ;
        Map<String,EMWPList> listMap = new HashMap<>() ;
        Map<String,ArrayList<cn.ibizlab.eam.core.eam_core.domain.EMWPListCost>> labserviceMap = new HashMap<>() ;
        for(int i = 0 ; i < listids.length ; i++ ){
            EMWPList list = this.get(listids[i]) ;
            listMap.put(list.getEmwplistid(),list) ;
            cn.ibizlab.eam.core.eam_core.domain.EMWPListCost cost = list.getWplistcost() ;
            if(!labserviceMap.containsKey(cost.getLabserviceid())){
                labserviceMap.put(cost.getLabserviceid(),new ArrayList<>()) ;
            }
            labserviceMap.get(cost.getLabserviceid()).add(cost) ;
        }
        for(Map.Entry<String, ArrayList<cn.ibizlab.eam.core.eam_core.domain.EMWPListCost>> entry : labserviceMap.entrySet()){
            String labserviceid = entry.getKey();
            ArrayList<cn.ibizlab.eam.core.eam_core.domain.EMWPListCost> costs = entry.getValue();
            cn.ibizlab.eam.core.eam_core.domain.EMPO po = new cn.ibizlab.eam.core.eam_core.domain.EMPO() ;
            po.setLabserviceid(labserviceid);

            empoService.create(po) ;

            for (cn.ibizlab.eam.core.eam_core.domain.EMWPListCost cost : costs ){
                cn.ibizlab.eam.core.eam_core.domain.EMPODetail detail = new cn.ibizlab.eam.core.eam_core.domain.EMPODetail() ;
                detail.setPoid(po.getEmpoid());
                detail.setItemid(cost.getItemid());
                detail.setItemdesc(cost.getItemdesc());
                detail.setDiscnt(cost.getDiscnt());
                detail.setListprice(cost.getListprice()!=null?cost.getListprice():cost.getPrice());
                detail.setTaxrate(cost.getTaxrate());
                detail.setUnitrate(cost.getUnitrate());
//                if(cost.getIntunitflag()==1){
                detail.setPsum(listMap.get(cost.getWplistid()).getAsum()/cost.getUnitrate());
//                }
//                if(cost.getIntunitflag()==1){
                detail.setRsum(listMap.get(cost.getWplistid()).getAsum()/cost.getUnitrate());
//                }
                detail.setRprice(cost.getPrice());
                detail.setAvgtsfee("0");
                detail.setAvgtaxfee("0");
                detail.setUnitid(cost.getUnitid());
                detail.setRunitid(cost.getUnitid());
                detail.setRempid(cost.getRempid());
                detail.setPodetailstate(0);
                detail.setWplistid(cost.getWplistid());
                empodetailService.create(detail) ;
            }
            
            
            
        }

        et.setWpliststate(20);
        this.update(et);
        return et;
    }
    @Override
    @Transactional
    public EMWPList getREMP(EMWPList et) {
        getrempLogic.execute(et);
         return et ;
    }

    @Override
    @Transactional
    public boolean save(EMWPList et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMWPList et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMWPList> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMWPList> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }

        @Override
    @Transactional
    public EMWPList submit(EMWPList et) {
        et.setWpliststate(10);
        et.setWfstep("40");
        this.update(et);
        return et;
    }

	@Override
    public List<EMWPList> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }

    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMWPList>().eq("equipid",emequipid));
    }

	@Override
    public List<EMWPList> selectByItemid(String emitemid) {
        return baseMapper.selectByItemid(emitemid);
    }

    @Override
    public void removeByItemid(String emitemid) {
        this.remove(new QueryWrapper<EMWPList>().eq("itemid",emitemid));
    }

	@Override
    public List<EMWPList> selectByObjid(String emobjectid) {
        return baseMapper.selectByObjid(emobjectid);
    }

    @Override
    public void removeByObjid(String emobjectid) {
        this.remove(new QueryWrapper<EMWPList>().eq("objid",emobjectid));
    }

	@Override
    public List<EMWPList> selectByEmserviceid(String emserviceid) {
        return baseMapper.selectByEmserviceid(emserviceid);
    }

    @Override
    public void removeByEmserviceid(String emserviceid) {
        this.remove(new QueryWrapper<EMWPList>().eq("emserviceid",emserviceid));
    }

	@Override
    public List<EMWPList> selectByWplistcostid(String emwplistcostid) {
        return baseMapper.selectByWplistcostid(emwplistcostid);
    }

    @Override
    public void removeByWplistcostid(String emwplistcostid) {
        this.remove(new QueryWrapper<EMWPList>().eq("wplistcostid",emwplistcostid));
    }

	@Override
    public List<EMWPList> selectByTeamid(String pfteamid) {
        return baseMapper.selectByTeamid(pfteamid);
    }

    @Override
    public void removeByTeamid(String pfteamid) {
        this.remove(new QueryWrapper<EMWPList>().eq("teamid",pfteamid));
    }


    /**
     * 查询集合 已取消申请
     */
    @Override
    public Page<EMWPList> searchCancel(EMWPListSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWPList> pages=baseMapper.searchCancel(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWPList>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 待确认询价
     */
    @Override
    public Page<EMWPList> searchConfimCost(EMWPListSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWPList> pages=baseMapper.searchConfimCost(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWPList>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMWPList> searchDefault(EMWPListSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWPList> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWPList>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 草稿
     */
    @Override
    public Page<EMWPList> searchDraft(EMWPListSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWPList> pages=baseMapper.searchDraft(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWPList>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 已入库申请
     */
    @Override
    public Page<EMWPList> searchIn(EMWPListSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWPList> pages=baseMapper.searchIn(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWPList>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 已生成订单采购申请
     */
    @Override
    public Page<EMWPList> searchMain6(EMWPListSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWPList> pages=baseMapper.searchMain6(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWPList>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 已到货采购申请
     */
    @Override
    public Page<EMWPList> searchMain6_8692(EMWPListSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWPList> pages=baseMapper.searchMain6_8692(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWPList>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 待询价
     */
    @Override
    public Page<EMWPList> searchWaitCost(EMWPListSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWPList> pages=baseMapper.searchWaitCost(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWPList>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 待生成订单
     */
    @Override
    public Page<EMWPList> searchWaitPo(EMWPListSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWPList> pages=baseMapper.searchWaitPo(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWPList>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 采购情况统计
     */
    @Override
    public Page<HashMap> searchWpStateNum(EMWPListSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<HashMap> pages=baseMapper.searchWpStateNum(context.getPages(),context,context.getSelectCond());
        return new PageImpl<HashMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMWPList et){
        //实体关系[DER1N_EMWPLIST_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMWPLIST_EMITEM_ITEMID]
        if(!ObjectUtils.isEmpty(et.getItemid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItem item=et.getItem();
            if(ObjectUtils.isEmpty(item)){
                cn.ibizlab.eam.core.eam_core.domain.EMItem majorEntity=emitemService.get(et.getItemid());
                et.setItem(majorEntity);
                item=majorEntity;
            }
            et.setNo3q(item.getNo3q());
            et.setItemcode(item.getItemcode());
            et.setUnitname(item.getUnitname());
            et.setItemname(item.getEmitemname());
            et.setItembtypeid(item.getItembtypeid());
            et.setItemnameShow(item.getEmitemname());
            et.setAvgprice(item.getPrice());
            et.setUnitid(item.getUnitid());
        }
        //实体关系[DER1N_EMWPLIST_EMOBJECT_OBJID]
        if(!ObjectUtils.isEmpty(et.getObjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject obj=et.getObj();
            if(ObjectUtils.isEmpty(obj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getObjid());
                et.setObj(majorEntity);
                obj=majorEntity;
            }
            et.setObjname(obj.getEmobjectname());
        }
        //实体关系[DER1N_EMWPLIST_EMSERVICE_EMSERVICEID]
        if(!ObjectUtils.isEmpty(et.getEmserviceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService emservice=et.getEmservice();
            if(ObjectUtils.isEmpty(emservice)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getEmserviceid());
                et.setEmservice(majorEntity);
                emservice=majorEntity;
            }
            et.setEmservicename(emservice.getEmservicename());
        }
        //实体关系[DER1N_EMWPLIST_EMWPLISTCOST_WPLISTCOSTID]
        if(!ObjectUtils.isEmpty(et.getWplistcostid())){
            cn.ibizlab.eam.core.eam_core.domain.EMWPListCost wplistcost=et.getWplistcost();
            if(ObjectUtils.isEmpty(wplistcost)){
                cn.ibizlab.eam.core.eam_core.domain.EMWPListCost majorEntity=emwplistcostService.get(et.getWplistcostid());
                et.setWplistcost(majorEntity);
                wplistcost=majorEntity;
            }
            et.setLabservicename(wplistcost.getLabservicename());
            et.setWplistcostname(wplistcost.getEmwplistcostname());
            et.setLabserviceid(wplistcost.getLabserviceid());
        }
        //实体关系[DER1N_EMWPLIST_PFTEAM_TEAMID]
        if(!ObjectUtils.isEmpty(et.getTeamid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFTeam team=et.getTeam();
            if(ObjectUtils.isEmpty(team)){
                cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorEntity=pfteamService.get(et.getTeamid());
                et.setTeam(majorEntity);
                team=majorEntity;
            }
            et.setTeamname(team.getPfteamname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMWPList> getEmwplistByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMWPList> getEmwplistByEntities(List<EMWPList> entities) {
        List ids =new ArrayList();
        for(EMWPList entity : entities){
            Serializable id=entity.getEmwplistid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



