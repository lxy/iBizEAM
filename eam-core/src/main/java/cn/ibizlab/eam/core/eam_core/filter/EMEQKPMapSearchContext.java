package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEQKPMap;
/**
 * 关系型数据实体[EMEQKPMap] 查询条件对象
 */
@Slf4j
@Data
public class EMEQKPMapSearchContext extends QueryWrapperContext<EMEQKPMap> {

	private String n_emeqkpmapname_like;//[设备关键点关系]
	public void setN_emeqkpmapname_like(String n_emeqkpmapname_like) {
        this.n_emeqkpmapname_like = n_emeqkpmapname_like;
        if(!ObjectUtils.isEmpty(this.n_emeqkpmapname_like)){
            this.getSearchCond().like("emeqkpmapname", n_emeqkpmapname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_refobjname_eq;//[设备关键点归属]
	public void setN_refobjname_eq(String n_refobjname_eq) {
        this.n_refobjname_eq = n_refobjname_eq;
        if(!ObjectUtils.isEmpty(this.n_refobjname_eq)){
            this.getSearchCond().eq("refobjname", n_refobjname_eq);
        }
    }
	private String n_refobjname_like;//[设备关键点归属]
	public void setN_refobjname_like(String n_refobjname_like) {
        this.n_refobjname_like = n_refobjname_like;
        if(!ObjectUtils.isEmpty(this.n_refobjname_like)){
            this.getSearchCond().like("refobjname", n_refobjname_like);
        }
    }
	private String n_kpname_eq;//[设备关键点]
	public void setN_kpname_eq(String n_kpname_eq) {
        this.n_kpname_eq = n_kpname_eq;
        if(!ObjectUtils.isEmpty(this.n_kpname_eq)){
            this.getSearchCond().eq("kpname", n_kpname_eq);
        }
    }
	private String n_kpname_like;//[设备关键点]
	public void setN_kpname_like(String n_kpname_like) {
        this.n_kpname_like = n_kpname_like;
        if(!ObjectUtils.isEmpty(this.n_kpname_like)){
            this.getSearchCond().like("kpname", n_kpname_like);
        }
    }
	private String n_kpid_eq;//[设备关键点]
	public void setN_kpid_eq(String n_kpid_eq) {
        this.n_kpid_eq = n_kpid_eq;
        if(!ObjectUtils.isEmpty(this.n_kpid_eq)){
            this.getSearchCond().eq("kpid", n_kpid_eq);
        }
    }
	private String n_refobjid_eq;//[设备关键点归属]
	public void setN_refobjid_eq(String n_refobjid_eq) {
        this.n_refobjid_eq = n_refobjid_eq;
        if(!ObjectUtils.isEmpty(this.n_refobjid_eq)){
            this.getSearchCond().eq("refobjid", n_refobjid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emeqkpmapname", query)   
            );
		 }
	}
}



