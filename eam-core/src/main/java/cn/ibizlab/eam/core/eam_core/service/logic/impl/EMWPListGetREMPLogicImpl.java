package cn.ibizlab.eam.core.eam_core.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.eam.core.eam_core.service.logic.IEMWPListGetREMPLogic;
import cn.ibizlab.eam.core.eam_core.domain.EMWPList;

/**
 * 关系型数据实体[GetREMP] 对象
 */
@Slf4j
@Service
public class EMWPListGetREMPLogicImpl implements IEMWPListGetREMPLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemservice;

    public cn.ibizlab.eam.core.eam_core.service.IEMItemService getEmitemService() {
        return this.emitemservice;
    }


    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMWPListService iBzSysDefaultService;

    public cn.ibizlab.eam.core.eam_core.service.IEMWPListService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(EMWPList et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("emwplistgetrempdefault",et);
           cn.ibizlab.eam.core.eam_core.domain.EMItem  emwplistgetrempemitem =new cn.ibizlab.eam.core.eam_core.domain.EMItem();
           kieSession.insert(emwplistgetrempemitem); 
           kieSession.setGlobal("emwplistgetrempemitem",emwplistgetrempemitem);
           kieSession.setGlobal("emitemservice",emitemservice);
           kieSession.setGlobal("iBzSysEmwplistDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.eam.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.eam.core.eam_core.service.logic.emwplistgetremp");

        }catch(Exception e){
            throw new RuntimeException("执行[根据物品获取采购员]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}
