package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMProduct;
/**
 * 关系型数据实体[EMProduct] 查询条件对象
 */
@Slf4j
@Data
public class EMProductSearchContext extends QueryWrapperContext<EMProduct> {

	private String n_deptname_eq;//[部门]
	public void setN_deptname_eq(String n_deptname_eq) {
        this.n_deptname_eq = n_deptname_eq;
        if(!ObjectUtils.isEmpty(this.n_deptname_eq)){
            this.getSearchCond().eq("deptname", n_deptname_eq);
        }
    }
	private String n_deptname_like;//[部门]
	public void setN_deptname_like(String n_deptname_like) {
        this.n_deptname_like = n_deptname_like;
        if(!ObjectUtils.isEmpty(this.n_deptname_like)){
            this.getSearchCond().like("deptname", n_deptname_like);
        }
    }
	private String n_emproductname_like;//[试用品名称]
	public void setN_emproductname_like(String n_emproductname_like) {
        this.n_emproductname_like = n_emproductname_like;
        if(!ObjectUtils.isEmpty(this.n_emproductname_like)){
            this.getSearchCond().like("emproductname", n_emproductname_like);
        }
    }
	private String n_empname_eq;//[用料人]
	public void setN_empname_eq(String n_empname_eq) {
        this.n_empname_eq = n_empname_eq;
        if(!ObjectUtils.isEmpty(this.n_empname_eq)){
            this.getSearchCond().eq("empname", n_empname_eq);
        }
    }
	private String n_empname_like;//[用料人]
	public void setN_empname_like(String n_empname_like) {
        this.n_empname_like = n_empname_like;
        if(!ObjectUtils.isEmpty(this.n_empname_like)){
            this.getSearchCond().like("empname", n_empname_like);
        }
    }
	private Integer n_productstate_eq;//[试用品状态]
	public void setN_productstate_eq(Integer n_productstate_eq) {
        this.n_productstate_eq = n_productstate_eq;
        if(!ObjectUtils.isEmpty(this.n_productstate_eq)){
            this.getSearchCond().eq("productstate", n_productstate_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_aempname_eq;//[采购员]
	public void setN_aempname_eq(String n_aempname_eq) {
        this.n_aempname_eq = n_aempname_eq;
        if(!ObjectUtils.isEmpty(this.n_aempname_eq)){
            this.getSearchCond().eq("aempname", n_aempname_eq);
        }
    }
	private String n_aempname_like;//[采购员]
	public void setN_aempname_like(String n_aempname_like) {
        this.n_aempname_like = n_aempname_like;
        if(!ObjectUtils.isEmpty(this.n_aempname_like)){
            this.getSearchCond().like("aempname", n_aempname_like);
        }
    }
	private String n_useto_eq;//[用途]
	public void setN_useto_eq(String n_useto_eq) {
        this.n_useto_eq = n_useto_eq;
        if(!ObjectUtils.isEmpty(this.n_useto_eq)){
            this.getSearchCond().eq("useto", n_useto_eq);
        }
    }
	private String n_aempid_eq;//[采购员]
	public void setN_aempid_eq(String n_aempid_eq) {
        this.n_aempid_eq = n_aempid_eq;
        if(!ObjectUtils.isEmpty(this.n_aempid_eq)){
            this.getSearchCond().eq("aempid", n_aempid_eq);
        }
    }
	private String n_empid_eq;//[用料人]
	public void setN_empid_eq(String n_empid_eq) {
        this.n_empid_eq = n_empid_eq;
        if(!ObjectUtils.isEmpty(this.n_empid_eq)){
            this.getSearchCond().eq("empid", n_empid_eq);
        }
    }
	private String n_deptid_eq;//[部门]
	public void setN_deptid_eq(String n_deptid_eq) {
        this.n_deptid_eq = n_deptid_eq;
        if(!ObjectUtils.isEmpty(this.n_deptid_eq)){
            this.getSearchCond().eq("deptid", n_deptid_eq);
        }
    }
	private String n_wfstep_eq;//[流程步骤]
	public void setN_wfstep_eq(String n_wfstep_eq) {
        this.n_wfstep_eq = n_wfstep_eq;
        if(!ObjectUtils.isEmpty(this.n_wfstep_eq)){
            this.getSearchCond().eq("wfstep", n_wfstep_eq);
        }
    }
	private String n_equipname_eq;//[设备]
	public void setN_equipname_eq(String n_equipname_eq) {
        this.n_equipname_eq = n_equipname_eq;
        if(!ObjectUtils.isEmpty(this.n_equipname_eq)){
            this.getSearchCond().eq("equipname", n_equipname_eq);
        }
    }
	private String n_equipname_like;//[设备]
	public void setN_equipname_like(String n_equipname_like) {
        this.n_equipname_like = n_equipname_like;
        if(!ObjectUtils.isEmpty(this.n_equipname_like)){
            this.getSearchCond().like("equipname", n_equipname_like);
        }
    }
	private String n_itemname_eq;//[物品]
	public void setN_itemname_eq(String n_itemname_eq) {
        this.n_itemname_eq = n_itemname_eq;
        if(!ObjectUtils.isEmpty(this.n_itemname_eq)){
            this.getSearchCond().eq("itemname", n_itemname_eq);
        }
    }
	private String n_itemname_like;//[物品]
	public void setN_itemname_like(String n_itemname_like) {
        this.n_itemname_like = n_itemname_like;
        if(!ObjectUtils.isEmpty(this.n_itemname_like)){
            this.getSearchCond().like("itemname", n_itemname_like);
        }
    }
	private String n_objname_eq;//[位置]
	public void setN_objname_eq(String n_objname_eq) {
        this.n_objname_eq = n_objname_eq;
        if(!ObjectUtils.isEmpty(this.n_objname_eq)){
            this.getSearchCond().eq("objname", n_objname_eq);
        }
    }
	private String n_objname_like;//[位置]
	public void setN_objname_like(String n_objname_like) {
        this.n_objname_like = n_objname_like;
        if(!ObjectUtils.isEmpty(this.n_objname_like)){
            this.getSearchCond().like("objname", n_objname_like);
        }
    }
	private String n_teamname_eq;//[班组]
	public void setN_teamname_eq(String n_teamname_eq) {
        this.n_teamname_eq = n_teamname_eq;
        if(!ObjectUtils.isEmpty(this.n_teamname_eq)){
            this.getSearchCond().eq("teamname", n_teamname_eq);
        }
    }
	private String n_teamname_like;//[班组]
	public void setN_teamname_like(String n_teamname_like) {
        this.n_teamname_like = n_teamname_like;
        if(!ObjectUtils.isEmpty(this.n_teamname_like)){
            this.getSearchCond().like("teamname", n_teamname_like);
        }
    }
	private String n_servicename_eq;//[供应商]
	public void setN_servicename_eq(String n_servicename_eq) {
        this.n_servicename_eq = n_servicename_eq;
        if(!ObjectUtils.isEmpty(this.n_servicename_eq)){
            this.getSearchCond().eq("servicename", n_servicename_eq);
        }
    }
	private String n_servicename_like;//[供应商]
	public void setN_servicename_like(String n_servicename_like) {
        this.n_servicename_like = n_servicename_like;
        if(!ObjectUtils.isEmpty(this.n_servicename_like)){
            this.getSearchCond().like("servicename", n_servicename_like);
        }
    }
	private String n_serviceid_eq;//[供应商]
	public void setN_serviceid_eq(String n_serviceid_eq) {
        this.n_serviceid_eq = n_serviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_serviceid_eq)){
            this.getSearchCond().eq("serviceid", n_serviceid_eq);
        }
    }
	private String n_itemid_eq;//[物品]
	public void setN_itemid_eq(String n_itemid_eq) {
        this.n_itemid_eq = n_itemid_eq;
        if(!ObjectUtils.isEmpty(this.n_itemid_eq)){
            this.getSearchCond().eq("itemid", n_itemid_eq);
        }
    }
	private String n_equipid_eq;//[设备]
	public void setN_equipid_eq(String n_equipid_eq) {
        this.n_equipid_eq = n_equipid_eq;
        if(!ObjectUtils.isEmpty(this.n_equipid_eq)){
            this.getSearchCond().eq("equipid", n_equipid_eq);
        }
    }
	private String n_objid_eq;//[位置]
	public void setN_objid_eq(String n_objid_eq) {
        this.n_objid_eq = n_objid_eq;
        if(!ObjectUtils.isEmpty(this.n_objid_eq)){
            this.getSearchCond().eq("objid", n_objid_eq);
        }
    }
	private String n_teamid_eq;//[班组]
	public void setN_teamid_eq(String n_teamid_eq) {
        this.n_teamid_eq = n_teamid_eq;
        if(!ObjectUtils.isEmpty(this.n_teamid_eq)){
            this.getSearchCond().eq("teamid", n_teamid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emproductname", query)   
            );
		 }
	}
}



