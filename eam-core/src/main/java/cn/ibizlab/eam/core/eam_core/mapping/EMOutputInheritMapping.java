

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMOutput;
import cn.ibizlab.eam.core.eam_core.domain.EMObject;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMOutputInheritMapping {

    @Mappings({
        @Mapping(source ="emoutputid",target = "emobjectid"),
        @Mapping(source ="emoutputname",target = "emobjectname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="outputcode",target = "objectcode"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObject toEmobject(EMOutput emoutput);

    @Mappings({
        @Mapping(source ="emobjectid" ,target = "emoutputid"),
        @Mapping(source ="emobjectname" ,target = "emoutputname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objectcode",target = "outputcode"),
    })
    EMOutput toEmoutput(EMObject emobject);

    List<EMObject> toEmobject(List<EMOutput> emoutput);

    List<EMOutput> toEmoutput(List<EMObject> emobject);

}


