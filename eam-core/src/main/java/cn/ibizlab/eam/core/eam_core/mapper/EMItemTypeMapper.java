package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMItemType;
import cn.ibizlab.eam.core.eam_core.filter.EMItemTypeSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMItemTypeMapper extends BaseMapper<EMItemType>{

    Page<EMItemType> searchChildAll(IPage page, @Param("srf") EMItemTypeSearchContext context, @Param("ew") Wrapper<EMItemType> wrapper) ;
    Page<EMItemType> searchDefault(IPage page, @Param("srf") EMItemTypeSearchContext context, @Param("ew") Wrapper<EMItemType> wrapper) ;
    Page<EMItemType> searchRoot(IPage page, @Param("srf") EMItemTypeSearchContext context, @Param("ew") Wrapper<EMItemType> wrapper) ;
    @Override
    EMItemType selectById(Serializable id);
    @Override
    int insert(EMItemType entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMItemType entity);
    @Override
    int update(@Param(Constants.ENTITY) EMItemType entity, @Param("ew") Wrapper<EMItemType> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMItemType> selectByItembtypeid(@Param("emitemtypeid") Serializable emitemtypeid) ;

    List<EMItemType> selectByItemmtypeid(@Param("emitemtypeid") Serializable emitemtypeid) ;

    List<EMItemType> selectByItemtypepid(@Param("emitemtypeid") Serializable emitemtypeid) ;

}
