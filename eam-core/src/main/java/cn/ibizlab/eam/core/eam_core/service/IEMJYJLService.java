package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMJYJL;
import cn.ibizlab.eam.core.eam_core.filter.EMJYJLSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMJYJL] 服务对象接口
 */
public interface IEMJYJLService extends IService<EMJYJL>{

    boolean create(EMJYJL et) ;
    void createBatch(List<EMJYJL> list) ;
    boolean update(EMJYJL et) ;
    void updateBatch(List<EMJYJL> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMJYJL get(String key) ;
    EMJYJL getDraft(EMJYJL et) ;
    boolean checkKey(EMJYJL et) ;
    boolean save(EMJYJL et) ;
    void saveBatch(List<EMJYJL> list) ;
    Page<EMJYJL> searchDefault(EMJYJLSearchContext context) ;
    List<EMJYJL> selectByEquipid(String emequipid) ;
    void removeByEquipid(String emequipid) ;
    List<EMJYJL> selectByItempuseid(String emitempuseid) ;
    void removeByItempuseid(String emitempuseid) ;
    List<EMJYJL> selectByItemid(String emitemid) ;
    void removeByItemid(String emitemid) ;
    List<EMJYJL> selectByTeamid(String pfteamid) ;
    void removeByTeamid(String pfteamid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMJYJL> getEmjyjlByIds(List<String> ids) ;
    List<EMJYJL> getEmjyjlByEntities(List<EMJYJL> entities) ;
}


