package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.service.impl.EMItemPUseServiceImpl;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.eam.core.eam_core.domain.EMItemPUse;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;
import java.util.*;

/**
 * 实体[领料单] 自定义服务对象
 */
@Slf4j
@Primary
@Service("EMItemPUseExService")
public class EMItemPUseExService extends EMItemPUseServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * 自定义行为[GenId]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMItemPUse genId(EMItemPUse et) {
        return super.genId(et);
    }
    /**
     * 自定义行为[Issue]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMItemPUse issue(EMItemPUse et) {
        return super.issue(et);
    }
    /**
     * 自定义行为[Submit]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMItemPUse submit(EMItemPUse et) {
        return super.submit(et);
    }
}

