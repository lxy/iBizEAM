package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMPO;
import cn.ibizlab.eam.core.eam_core.filter.EMPOSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMPOService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMPOMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[订单] 服务对象接口实现
 */
@Slf4j
@Service("EMPOServiceImpl")
public class EMPOServiceImpl extends ServiceImpl<EMPOMapper, EMPO> implements IEMPOService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPODetailService empodetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMServiceService emserviceService;

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.logic.IEMPOGenIdLogic genidLogic;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMPO et) {
        fillParentData(et);
        genidLogic.execute(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmpoid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMPO> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMPO et) {
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("empoid",et.getEmpoid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmpoid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMPO> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMPO get(String key) {
        EMPO et = getById(key);
        if(et==null){
            et=new EMPO();
            et.setEmpoid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMPO getDraft(EMPO et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public EMPO arrival(EMPO et) {
        //自定义代码
        return et;
    }

    @Override
    public boolean checkKey(EMPO et) {
        return (!ObjectUtils.isEmpty(et.getEmpoid()))&&(!Objects.isNull(this.getById(et.getEmpoid())));
    }
    @Override
    @Transactional
    public EMPO genId(EMPO et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public EMPO placeOrder(EMPO et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean save(EMPO et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMPO et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMPO> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMPO> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMPO> selectByLabserviceid(String emserviceid) {
        return baseMapper.selectByLabserviceid(emserviceid);
    }

    @Override
    public void removeByLabserviceid(String emserviceid) {
        this.remove(new QueryWrapper<EMPO>().eq("labserviceid",emserviceid));
    }


    /**
     * 查询集合 已关闭订单
     */
    @Override
    public Page<EMPO> searchClosedOrder(EMPOSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMPO> pages=baseMapper.searchClosedOrder(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMPO>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMPO> searchDefault(EMPOSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMPO> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMPO>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 在途
     */
    @Override
    public Page<EMPO> searchOnOrder(EMPOSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMPO> pages=baseMapper.searchOnOrder(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMPO>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 等待下单
     */
    @Override
    public Page<EMPO> searchPlaceOrder(EMPOSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMPO> pages=baseMapper.searchPlaceOrder(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMPO>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMPO et){
        //实体关系[DER1N_EMPO_EMSERVICE_LABSERVICEID]
        if(!ObjectUtils.isEmpty(et.getLabserviceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService labservice=et.getLabservice();
            if(ObjectUtils.isEmpty(labservice)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getLabserviceid());
                et.setLabservice(majorEntity);
                labservice=majorEntity;
            }
            et.setLabservicetypeid(labservice.getLabservicetypeid());
            et.setLabservicename(labservice.getEmservicename());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMPO> getEmpoByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMPO> getEmpoByEntities(List<EMPO> entities) {
        List ids =new ArrayList();
        for(EMPO entity : entities){
            Serializable id=entity.getEmpoid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



