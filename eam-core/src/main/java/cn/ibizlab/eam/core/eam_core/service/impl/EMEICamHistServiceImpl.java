package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEICamHist;
import cn.ibizlab.eam.core.eam_core.filter.EMEICamHistSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEICamHistService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEICamHistMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[探头检修记录] 服务对象接口实现
 */
@Slf4j
@Service("EMEICamHistServiceImpl")
public class EMEICamHistServiceImpl extends ServiceImpl<EMEICamHistMapper, EMEICamHist> implements IEMEICamHistService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEICamService emeicamService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEICamHist et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmeicamhistid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEICamHist> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEICamHist et) {
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emeicamhistid",et.getEmeicamhistid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmeicamhistid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEICamHist> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEICamHist get(String key) {
        EMEICamHist et = getById(key);
        if(et==null){
            et=new EMEICamHist();
            et.setEmeicamhistid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMEICamHist getDraft(EMEICamHist et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEICamHist et) {
        return (!ObjectUtils.isEmpty(et.getEmeicamhistid()))&&(!Objects.isNull(this.getById(et.getEmeicamhistid())));
    }
    @Override
    @Transactional
    public boolean save(EMEICamHist et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEICamHist et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEICamHist> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEICamHist> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMEICamHist> selectByEiobjid(String emeicamid) {
        return baseMapper.selectByEiobjid(emeicamid);
    }

    @Override
    public void removeByEiobjid(String emeicamid) {
        this.remove(new QueryWrapper<EMEICamHist>().eq("eiobjid",emeicamid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEICamHist> searchDefault(EMEICamHistSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEICamHist> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEICamHist>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEICamHist et){
        //实体关系[DER1N_EMEICAMHIST_EMEICAM_EIOBJID]
        if(!ObjectUtils.isEmpty(et.getEiobjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEICam eiobj=et.getEiobj();
            if(ObjectUtils.isEmpty(eiobj)){
                cn.ibizlab.eam.core.eam_core.domain.EMEICam majorEntity=emeicamService.get(et.getEiobjid());
                et.setEiobj(majorEntity);
                eiobj=majorEntity;
            }
            et.setEiobjname(eiobj.getEmeicamname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEICamHist> getEmeicamhistByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEICamHist> getEmeicamhistByEntities(List<EMEICamHist> entities) {
        List ids =new ArrayList();
        for(EMEICamHist entity : entities){
            Serializable id=entity.getEmeicamhistid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



