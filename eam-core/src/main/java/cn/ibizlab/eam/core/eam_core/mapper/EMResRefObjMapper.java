package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMResRefObj;
import cn.ibizlab.eam.core.eam_core.filter.EMResRefObjSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMResRefObjMapper extends BaseMapper<EMResRefObj>{

    Page<EMResRefObj> searchDefault(IPage page, @Param("srf") EMResRefObjSearchContext context, @Param("ew") Wrapper<EMResRefObj> wrapper) ;
    Page<EMResRefObj> searchIndexDER(IPage page, @Param("srf") EMResRefObjSearchContext context, @Param("ew") Wrapper<EMResRefObj> wrapper) ;
    @Override
    EMResRefObj selectById(Serializable id);
    @Override
    int insert(EMResRefObj entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMResRefObj entity);
    @Override
    int update(@Param(Constants.ENTITY) EMResRefObj entity, @Param("ew") Wrapper<EMResRefObj> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMResRefObj> selectByEquipid(@Param("emequipid") Serializable emequipid) ;

    List<EMResRefObj> selectByResrefobjpid(@Param("emresrefobjid") Serializable emresrefobjid) ;

}
