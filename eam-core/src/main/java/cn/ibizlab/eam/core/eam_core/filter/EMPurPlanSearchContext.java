package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMPurPlan;
/**
 * 关系型数据实体[EMPurPlan] 查询条件对象
 */
@Slf4j
@Data
public class EMPurPlanSearchContext extends QueryWrapperContext<EMPurPlan> {

	private String n_empurplanname_like;//[采购计划项目]
	public void setN_empurplanname_like(String n_empurplanname_like) {
        this.n_empurplanname_like = n_empurplanname_like;
        if(!ObjectUtils.isEmpty(this.n_empurplanname_like)){
            this.getSearchCond().like("empurplanname", n_empurplanname_like);
        }
    }
	private String n_years_eq;//[采购年度]
	public void setN_years_eq(String n_years_eq) {
        this.n_years_eq = n_years_eq;
        if(!ObjectUtils.isEmpty(this.n_years_eq)){
            this.getSearchCond().eq("years", n_years_eq);
        }
    }
	private Integer n_planstate_eq;//[计划修理状态]
	public void setN_planstate_eq(Integer n_planstate_eq) {
        this.n_planstate_eq = n_planstate_eq;
        if(!ObjectUtils.isEmpty(this.n_planstate_eq)){
            this.getSearchCond().eq("planstate", n_planstate_eq);
        }
    }
	private Integer n_m3q_eq;//[经理指定询价数]
	public void setN_m3q_eq(Integer n_m3q_eq) {
        this.n_m3q_eq = n_m3q_eq;
        if(!ObjectUtils.isEmpty(this.n_m3q_eq)){
            this.getSearchCond().eq("m3q", n_m3q_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_wfstep_eq;//[流程步骤]
	public void setN_wfstep_eq(String n_wfstep_eq) {
        this.n_wfstep_eq = n_wfstep_eq;
        if(!ObjectUtils.isEmpty(this.n_wfstep_eq)){
            this.getSearchCond().eq("wfstep", n_wfstep_eq);
        }
    }
	private String n_rempid_eq;//[负责人]
	public void setN_rempid_eq(String n_rempid_eq) {
        this.n_rempid_eq = n_rempid_eq;
        if(!ObjectUtils.isEmpty(this.n_rempid_eq)){
            this.getSearchCond().eq("rempid", n_rempid_eq);
        }
    }
	private String n_rempname_eq;//[负责人]
	public void setN_rempname_eq(String n_rempname_eq) {
        this.n_rempname_eq = n_rempname_eq;
        if(!ObjectUtils.isEmpty(this.n_rempname_eq)){
            this.getSearchCond().eq("rempname", n_rempname_eq);
        }
    }
	private String n_rempname_like;//[负责人]
	public void setN_rempname_like(String n_rempname_like) {
        this.n_rempname_like = n_rempname_like;
        if(!ObjectUtils.isEmpty(this.n_rempname_like)){
            this.getSearchCond().like("rempname", n_rempname_like);
        }
    }
	private String n_unitname_eq;//[单位]
	public void setN_unitname_eq(String n_unitname_eq) {
        this.n_unitname_eq = n_unitname_eq;
        if(!ObjectUtils.isEmpty(this.n_unitname_eq)){
            this.getSearchCond().eq("unitname", n_unitname_eq);
        }
    }
	private String n_unitname_like;//[单位]
	public void setN_unitname_like(String n_unitname_like) {
        this.n_unitname_like = n_unitname_like;
        if(!ObjectUtils.isEmpty(this.n_unitname_like)){
            this.getSearchCond().like("unitname", n_unitname_like);
        }
    }
	private String n_itemtypename_eq;//[物品类型]
	public void setN_itemtypename_eq(String n_itemtypename_eq) {
        this.n_itemtypename_eq = n_itemtypename_eq;
        if(!ObjectUtils.isEmpty(this.n_itemtypename_eq)){
            this.getSearchCond().eq("itemtypename", n_itemtypename_eq);
        }
    }
	private String n_itemtypename_like;//[物品类型]
	public void setN_itemtypename_like(String n_itemtypename_like) {
        this.n_itemtypename_like = n_itemtypename_like;
        if(!ObjectUtils.isEmpty(this.n_itemtypename_like)){
            this.getSearchCond().like("itemtypename", n_itemtypename_like);
        }
    }
	private String n_embidinquiryname_eq;//[计划修理]
	public void setN_embidinquiryname_eq(String n_embidinquiryname_eq) {
        this.n_embidinquiryname_eq = n_embidinquiryname_eq;
        if(!ObjectUtils.isEmpty(this.n_embidinquiryname_eq)){
            this.getSearchCond().eq("embidinquiryname", n_embidinquiryname_eq);
        }
    }
	private String n_embidinquiryname_like;//[计划修理]
	public void setN_embidinquiryname_like(String n_embidinquiryname_like) {
        this.n_embidinquiryname_like = n_embidinquiryname_like;
        if(!ObjectUtils.isEmpty(this.n_embidinquiryname_like)){
            this.getSearchCond().like("embidinquiryname", n_embidinquiryname_like);
        }
    }
	private String n_itemtypeid_eq;//[物品类型]
	public void setN_itemtypeid_eq(String n_itemtypeid_eq) {
        this.n_itemtypeid_eq = n_itemtypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_itemtypeid_eq)){
            this.getSearchCond().eq("itemtypeid", n_itemtypeid_eq);
        }
    }
	private String n_embidinquiryid_eq;//[计划修理]
	public void setN_embidinquiryid_eq(String n_embidinquiryid_eq) {
        this.n_embidinquiryid_eq = n_embidinquiryid_eq;
        if(!ObjectUtils.isEmpty(this.n_embidinquiryid_eq)){
            this.getSearchCond().eq("embidinquiryid", n_embidinquiryid_eq);
        }
    }
	private String n_unitid_eq;//[单位]
	public void setN_unitid_eq(String n_unitid_eq) {
        this.n_unitid_eq = n_unitid_eq;
        if(!ObjectUtils.isEmpty(this.n_unitid_eq)){
            this.getSearchCond().eq("unitid", n_unitid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("empurplanname", query)   
            );
		 }
	}
}



