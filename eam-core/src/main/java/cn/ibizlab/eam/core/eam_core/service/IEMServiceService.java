package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMService;
import cn.ibizlab.eam.core.eam_core.filter.EMServiceSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMService] 服务对象接口
 */
public interface IEMServiceService extends IService<EMService>{

    boolean create(EMService et) ;
    void createBatch(List<EMService> list) ;
    boolean update(EMService et) ;
    void updateBatch(List<EMService> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMService get(String key) ;
    EMService getDraft(EMService et) ;
    boolean checkKey(EMService et) ;
    boolean save(EMService et) ;
    void saveBatch(List<EMService> list) ;
    Page<EMService> searchDefault(EMServiceSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMService> getEmserviceByIds(List<String> ids) ;
    List<EMService> getEmserviceByEntities(List<EMService> entities) ;
}


