package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMDPRCT;
import cn.ibizlab.eam.core.eam_core.filter.EMDPRCTSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMDPRCT] 服务对象接口
 */
public interface IEMDPRCTService extends IService<EMDPRCT>{

    boolean create(EMDPRCT et) ;
    void createBatch(List<EMDPRCT> list) ;
    boolean update(EMDPRCT et) ;
    void updateBatch(List<EMDPRCT> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMDPRCT get(String key) ;
    EMDPRCT getDraft(EMDPRCT et) ;
    boolean checkKey(EMDPRCT et) ;
    boolean save(EMDPRCT et) ;
    void saveBatch(List<EMDPRCT> list) ;
    Page<EMDPRCT> searchDefault(EMDPRCTSearchContext context) ;
    Page<EMDPRCT> searchIndexDER(EMDPRCTSearchContext context) ;
    List<EMDPRCT> selectByEquipid(String emequipid) ;
    void removeByEquipid(String emequipid) ;
    List<EMDPRCT> selectByDpid(String emobjectid) ;
    void removeByDpid(String emobjectid) ;
    List<EMDPRCT> selectByObjid(String emobjectid) ;
    void removeByObjid(String emobjectid) ;
    List<EMDPRCT> selectByWoid(String emwoid) ;
    void removeByWoid(String emwoid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMDPRCT> getEmdprctByIds(List<String> ids) ;
    List<EMDPRCT> getEmdprctByEntities(List<EMDPRCT> entities) ;
}


