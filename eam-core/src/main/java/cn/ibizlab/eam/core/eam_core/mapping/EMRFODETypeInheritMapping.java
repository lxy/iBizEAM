

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMRFODEType;
import cn.ibizlab.eam.core.eam_core.domain.EMObject;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMRFODETypeInheritMapping {

    @Mappings({
        @Mapping(source ="emrfodetypeid",target = "emobjectid"),
        @Mapping(source ="emrfodetypename",target = "emobjectname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="rfodetypecode",target = "objectcode"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObject toEmobject(EMRFODEType emrfodetype);

    @Mappings({
        @Mapping(source ="emobjectid" ,target = "emrfodetypeid"),
        @Mapping(source ="emobjectname" ,target = "emrfodetypename"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objectcode",target = "rfodetypecode"),
    })
    EMRFODEType toEmrfodetype(EMObject emobject);

    List<EMObject> toEmobject(List<EMRFODEType> emrfodetype);

    List<EMRFODEType> toEmrfodetype(List<EMObject> emobject);

}


