package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMRFODEMap;
import cn.ibizlab.eam.core.eam_core.filter.EMRFODEMapSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMRFODEMap] 服务对象接口
 */
public interface IEMRFODEMapService extends IService<EMRFODEMap>{

    boolean create(EMRFODEMap et) ;
    void createBatch(List<EMRFODEMap> list) ;
    boolean update(EMRFODEMap et) ;
    void updateBatch(List<EMRFODEMap> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMRFODEMap get(String key) ;
    EMRFODEMap getDraft(EMRFODEMap et) ;
    boolean checkKey(EMRFODEMap et) ;
    boolean save(EMRFODEMap et) ;
    void saveBatch(List<EMRFODEMap> list) ;
    Page<EMRFODEMap> searchDefault(EMRFODEMapSearchContext context) ;
    List<EMRFODEMap> selectByRefobjid(String emobjectid) ;
    void removeByRefobjid(String emobjectid) ;
    List<EMRFODEMap> selectByRfodeid(String emrfodeid) ;
    void removeByRfodeid(String emrfodeid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMRFODEMap> getEmrfodemapByIds(List<String> ids) ;
    List<EMRFODEMap> getEmrfodemapByEntities(List<EMRFODEMap> entities) ;
}


