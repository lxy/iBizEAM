package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMBidinquiry;
/**
 * 关系型数据实体[EMBidinquiry] 查询条件对象
 */
@Slf4j
@Data
public class EMBidinquirySearchContext extends QueryWrapperContext<EMBidinquiry> {

	private String n_rempid_eq;//[录入人]
	public void setN_rempid_eq(String n_rempid_eq) {
        this.n_rempid_eq = n_rempid_eq;
        if(!ObjectUtils.isEmpty(this.n_rempid_eq)){
            this.getSearchCond().eq("rempid", n_rempid_eq);
        }
    }
	private String n_embidinquiryname_like;//[招投标信息名称]
	public void setN_embidinquiryname_like(String n_embidinquiryname_like) {
        this.n_embidinquiryname_like = n_embidinquiryname_like;
        if(!ObjectUtils.isEmpty(this.n_embidinquiryname_like)){
            this.getSearchCond().like("embidinquiryname", n_embidinquiryname_like);
        }
    }
	private String n_rempname_eq;//[录入人]
	public void setN_rempname_eq(String n_rempname_eq) {
        this.n_rempname_eq = n_rempname_eq;
        if(!ObjectUtils.isEmpty(this.n_rempname_eq)){
            this.getSearchCond().eq("rempname", n_rempname_eq);
        }
    }
	private String n_rempname_like;//[录入人]
	public void setN_rempname_like(String n_rempname_like) {
        this.n_rempname_like = n_rempname_like;
        if(!ObjectUtils.isEmpty(this.n_rempname_like)){
            this.getSearchCond().like("rempname", n_rempname_like);
        }
    }
	private String n_emservicename_eq;//[服务商]
	public void setN_emservicename_eq(String n_emservicename_eq) {
        this.n_emservicename_eq = n_emservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_emservicename_eq)){
            this.getSearchCond().eq("emservicename", n_emservicename_eq);
        }
    }
	private String n_emservicename_like;//[服务商]
	public void setN_emservicename_like(String n_emservicename_like) {
        this.n_emservicename_like = n_emservicename_like;
        if(!ObjectUtils.isEmpty(this.n_emservicename_like)){
            this.getSearchCond().like("emservicename", n_emservicename_like);
        }
    }
	private String n_empurplanname_eq;//[计划修理]
	public void setN_empurplanname_eq(String n_empurplanname_eq) {
        this.n_empurplanname_eq = n_empurplanname_eq;
        if(!ObjectUtils.isEmpty(this.n_empurplanname_eq)){
            this.getSearchCond().eq("empurplanname", n_empurplanname_eq);
        }
    }
	private String n_empurplanname_like;//[计划修理]
	public void setN_empurplanname_like(String n_empurplanname_like) {
        this.n_empurplanname_like = n_empurplanname_like;
        if(!ObjectUtils.isEmpty(this.n_empurplanname_like)){
            this.getSearchCond().like("empurplanname", n_empurplanname_like);
        }
    }
	private String n_emserviceid_eq;//[服务商]
	public void setN_emserviceid_eq(String n_emserviceid_eq) {
        this.n_emserviceid_eq = n_emserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_emserviceid_eq)){
            this.getSearchCond().eq("emserviceid", n_emserviceid_eq);
        }
    }
	private String n_empurplanid_eq;//[计划修理]
	public void setN_empurplanid_eq(String n_empurplanid_eq) {
        this.n_empurplanid_eq = n_empurplanid_eq;
        if(!ObjectUtils.isEmpty(this.n_empurplanid_eq)){
            this.getSearchCond().eq("empurplanid", n_empurplanid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("embidinquiryname", query)   
            );
		 }
	}
}



