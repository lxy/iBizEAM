package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMJYJL;
import cn.ibizlab.eam.core.eam_core.filter.EMJYJLSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMJYJLService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMJYJLMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[加油记录] 服务对象接口实现
 */
@Slf4j
@Service("EMJYJLServiceImpl")
public class EMJYJLServiceImpl extends ServiceImpl<EMJYJLMapper, EMJYJL> implements IEMJYJLService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService emitempuseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFTeamService pfteamService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMJYJL et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmjyjlid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMJYJL> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMJYJL et) {
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emjyjlid",et.getEmjyjlid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmjyjlid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMJYJL> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMJYJL get(String key) {
        EMJYJL et = getById(key);
        if(et==null){
            et=new EMJYJL();
            et.setEmjyjlid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMJYJL getDraft(EMJYJL et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMJYJL et) {
        return (!ObjectUtils.isEmpty(et.getEmjyjlid()))&&(!Objects.isNull(this.getById(et.getEmjyjlid())));
    }
    @Override
    @Transactional
    public boolean save(EMJYJL et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMJYJL et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMJYJL> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMJYJL> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMJYJL> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }

    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMJYJL>().eq("equipid",emequipid));
    }

	@Override
    public List<EMJYJL> selectByItempuseid(String emitempuseid) {
        return baseMapper.selectByItempuseid(emitempuseid);
    }

    @Override
    public void removeByItempuseid(String emitempuseid) {
        this.remove(new QueryWrapper<EMJYJL>().eq("itempuseid",emitempuseid));
    }

	@Override
    public List<EMJYJL> selectByItemid(String emitemid) {
        return baseMapper.selectByItemid(emitemid);
    }

    @Override
    public void removeByItemid(String emitemid) {
        this.remove(new QueryWrapper<EMJYJL>().eq("itemid",emitemid));
    }

	@Override
    public List<EMJYJL> selectByTeamid(String pfteamid) {
        return baseMapper.selectByTeamid(pfteamid);
    }

    @Override
    public void removeByTeamid(String pfteamid) {
        this.remove(new QueryWrapper<EMJYJL>().eq("teamid",pfteamid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMJYJL> searchDefault(EMJYJLSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMJYJL> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMJYJL>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMJYJL et){
        //实体关系[DER1N_EMJYJL_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEqorgid(equip.getOrgid());
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMJYJL_EMITEMPUSE_ITEMPUSEID]
        if(!ObjectUtils.isEmpty(et.getItempuseid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItemPUse itempuse=et.getItempuse();
            if(ObjectUtils.isEmpty(itempuse)){
                cn.ibizlab.eam.core.eam_core.domain.EMItemPUse majorEntity=emitempuseService.get(et.getItempuseid());
                et.setItempuse(majorEntity);
                itempuse=majorEntity;
            }
            et.setItempusename(itempuse.getEmitempusename());
        }
        //实体关系[DER1N_EMJYJL_EMITEM_ITEMID]
        if(!ObjectUtils.isEmpty(et.getItemid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItem item=et.getItem();
            if(ObjectUtils.isEmpty(item)){
                cn.ibizlab.eam.core.eam_core.domain.EMItem majorEntity=emitemService.get(et.getItemid());
                et.setItem(majorEntity);
                item=majorEntity;
            }
            et.setItemdens(item.getDens());
            et.setItemname(item.getEmitemname());
        }
        //实体关系[DER1N_EMJYJL_PFTEAM_TEAMID]
        if(!ObjectUtils.isEmpty(et.getTeamid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFTeam team=et.getTeam();
            if(ObjectUtils.isEmpty(team)){
                cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorEntity=pfteamService.get(et.getTeamid());
                et.setTeam(majorEntity);
                team=majorEntity;
            }
            et.setTeamname(team.getPfteamname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMJYJL> getEmjyjlByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMJYJL> getEmjyjlByEntities(List<EMJYJL> entities) {
        List ids =new ArrayList();
        for(EMJYJL entity : entities){
            Serializable id=entity.getEmjyjlid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



