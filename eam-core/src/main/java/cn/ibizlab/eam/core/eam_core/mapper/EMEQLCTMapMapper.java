package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTMap;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLCTMapSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMEQLCTMapMapper extends BaseMapper<EMEQLCTMap>{

    Page<EMEQLCTMap> searchByLocation(IPage page, @Param("srf") EMEQLCTMapSearchContext context, @Param("ew") Wrapper<EMEQLCTMap> wrapper) ;
    Page<EMEQLCTMap> searchChildLocation(IPage page, @Param("srf") EMEQLCTMapSearchContext context, @Param("ew") Wrapper<EMEQLCTMap> wrapper) ;
    Page<EMEQLCTMap> searchDefault(IPage page, @Param("srf") EMEQLCTMapSearchContext context, @Param("ew") Wrapper<EMEQLCTMap> wrapper) ;
    Page<EMEQLCTMap> searchRootLocation(IPage page, @Param("srf") EMEQLCTMapSearchContext context, @Param("ew") Wrapper<EMEQLCTMap> wrapper) ;
    @Override
    EMEQLCTMap selectById(Serializable id);
    @Override
    int insert(EMEQLCTMap entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMEQLCTMap entity);
    @Override
    int update(@Param(Constants.ENTITY) EMEQLCTMap entity, @Param("ew") Wrapper<EMEQLCTMap> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMEQLCTMap> selectByEqlocationid(@Param("emeqlocationid") Serializable emeqlocationid) ;

    List<EMEQLCTMap> selectByEqlocationpid(@Param("emeqlocationid") Serializable emeqlocationid) ;

}
