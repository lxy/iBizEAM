package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEITool;
import cn.ibizlab.eam.core.eam_core.filter.EMEIToolSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEITool] 服务对象接口
 */
public interface IEMEIToolService extends IService<EMEITool>{

    boolean create(EMEITool et) ;
    void createBatch(List<EMEITool> list) ;
    boolean update(EMEITool et) ;
    void updateBatch(List<EMEITool> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMEITool get(String key) ;
    EMEITool getDraft(EMEITool et) ;
    boolean checkKey(EMEITool et) ;
    boolean save(EMEITool et) ;
    void saveBatch(List<EMEITool> list) ;
    Page<EMEITool> searchDefault(EMEIToolSearchContext context) ;
    List<EMEITool> selectByEqlocationid(String emeqlocationid) ;
    void removeByEqlocationid(String emeqlocationid) ;
    List<EMEITool> selectByEquipid(String emequipid) ;
    void removeByEquipid(String emequipid) ;
    List<EMEITool> selectByItempuseid(String emitempuseid) ;
    void removeByItempuseid(String emitempuseid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEITool> getEmeitoolByIds(List<String> ids) ;
    List<EMEITool> getEmeitoolByEntities(List<EMEITool> entities) ;
}


