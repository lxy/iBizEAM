package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEN;
/**
 * 关系型数据实体[EMEN] 查询条件对象
 */
@Slf4j
@Data
public class EMENSearchContext extends QueryWrapperContext<EMEN> {

	private String n_energytypeid_eq;//[能源类型]
	public void setN_energytypeid_eq(String n_energytypeid_eq) {
        this.n_energytypeid_eq = n_energytypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_energytypeid_eq)){
            this.getSearchCond().eq("energytypeid", n_energytypeid_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emenname_like;//[能源名称]
	public void setN_emenname_like(String n_emenname_like) {
        this.n_emenname_like = n_emenname_like;
        if(!ObjectUtils.isEmpty(this.n_emenname_like)){
            this.getSearchCond().like("emenname", n_emenname_like);
        }
    }
	private String n_energyinfo_like;//[能源信息]
	public void setN_energyinfo_like(String n_energyinfo_like) {
        this.n_energyinfo_like = n_energyinfo_like;
        if(!ObjectUtils.isEmpty(this.n_energyinfo_like)){
            this.getSearchCond().like("energyinfo", n_energyinfo_like);
        }
    }
	private String n_itemname_eq;//[物品]
	public void setN_itemname_eq(String n_itemname_eq) {
        this.n_itemname_eq = n_itemname_eq;
        if(!ObjectUtils.isEmpty(this.n_itemname_eq)){
            this.getSearchCond().eq("itemname", n_itemname_eq);
        }
    }
	private String n_itemname_like;//[物品]
	public void setN_itemname_like(String n_itemname_like) {
        this.n_itemname_like = n_itemname_like;
        if(!ObjectUtils.isEmpty(this.n_itemname_like)){
            this.getSearchCond().like("itemname", n_itemname_like);
        }
    }
	private String n_itemid_eq;//[物品]
	public void setN_itemid_eq(String n_itemid_eq) {
        this.n_itemid_eq = n_itemid_eq;
        if(!ObjectUtils.isEmpty(this.n_itemid_eq)){
            this.getSearchCond().eq("itemid", n_itemid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emenname", query)   
            );
		 }
	}
}



