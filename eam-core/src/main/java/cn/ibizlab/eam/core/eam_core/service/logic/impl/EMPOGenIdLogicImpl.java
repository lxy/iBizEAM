package cn.ibizlab.eam.core.eam_core.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.eam.core.eam_core.service.logic.IEMPOGenIdLogic;
import cn.ibizlab.eam.core.eam_core.domain.EMPO;

/**
 * 关系型数据实体[GenId] 对象
 */
@Slf4j
@Service
public class EMPOGenIdLogicImpl implements IEMPOGenIdLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMPOService emposervice;

    public cn.ibizlab.eam.core.eam_core.service.IEMPOService getEmpoService() {
        return this.emposervice;
    }


    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMPOService iBzSysDefaultService;

    public cn.ibizlab.eam.core.eam_core.service.IEMPOService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(EMPO et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("empogeniddefault",et);
           kieSession.setGlobal("emposervice",emposervice);
           kieSession.setGlobal("iBzSysEmpoDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.eam.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.eam.core.eam_core.service.logic.empogenid");

        }catch(Exception e){
            throw new RuntimeException("执行[生成订单编号]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}
