package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMPlanCDT;
import cn.ibizlab.eam.core.eam_core.filter.EMPlanCDTSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMPlanCDTService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMPlanCDTMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[计划条件] 服务对象接口实现
 */
@Slf4j
@Service("EMPlanCDTServiceImpl")
public class EMPlanCDTServiceImpl extends ServiceImpl<EMPlanCDTMapper, EMPlanCDT> implements IEMPlanCDTService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPlanService emplanService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMPlanCDT et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmplancdtid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMPlanCDT> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMPlanCDT et) {
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emplancdtid",et.getEmplancdtid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmplancdtid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMPlanCDT> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMPlanCDT get(String key) {
        EMPlanCDT et = getById(key);
        if(et==null){
            et=new EMPlanCDT();
            et.setEmplancdtid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMPlanCDT getDraft(EMPlanCDT et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMPlanCDT et) {
        return (!ObjectUtils.isEmpty(et.getEmplancdtid()))&&(!Objects.isNull(this.getById(et.getEmplancdtid())));
    }
    @Override
    @Transactional
    public boolean save(EMPlanCDT et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMPlanCDT et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMPlanCDT> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMPlanCDT> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMPlanCDT> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }

    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMPlanCDT>().eq("equipid",emequipid));
    }

	@Override
    public List<EMPlanCDT> selectByDpid(String emobjectid) {
        return baseMapper.selectByDpid(emobjectid);
    }

    @Override
    public void removeByDpid(String emobjectid) {
        this.remove(new QueryWrapper<EMPlanCDT>().eq("dpid",emobjectid));
    }

	@Override
    public List<EMPlanCDT> selectByObjid(String emobjectid) {
        return baseMapper.selectByObjid(emobjectid);
    }

    @Override
    public void removeByObjid(String emobjectid) {
        this.remove(new QueryWrapper<EMPlanCDT>().eq("objid",emobjectid));
    }

	@Override
    public List<EMPlanCDT> selectByPlanid(String emplanid) {
        return baseMapper.selectByPlanid(emplanid);
    }

    @Override
    public void removeByPlanid(String emplanid) {
        this.remove(new QueryWrapper<EMPlanCDT>().eq("planid",emplanid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMPlanCDT> searchDefault(EMPlanCDTSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMPlanCDT> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMPlanCDT>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMPlanCDT et){
        //实体关系[DER1N_EMPLANCDT_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMPLANCDT_EMOBJECT_DPID]
        if(!ObjectUtils.isEmpty(et.getDpid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject dp=et.getDp();
            if(ObjectUtils.isEmpty(dp)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getDpid());
                et.setDp(majorEntity);
                dp=majorEntity;
            }
            et.setDptype(dp.getEmobjecttype());
            et.setDpname(dp.getEmobjectname());
        }
        //实体关系[DER1N_EMPLANCDT_EMOBJECT_OBJID]
        if(!ObjectUtils.isEmpty(et.getObjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject obj=et.getObj();
            if(ObjectUtils.isEmpty(obj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getObjid());
                et.setObj(majorEntity);
                obj=majorEntity;
            }
            et.setObjname(obj.getEmobjectname());
        }
        //实体关系[DER1N_EMPLANCDT_EMPLAN_PLANID]
        if(!ObjectUtils.isEmpty(et.getPlanid())){
            cn.ibizlab.eam.core.eam_core.domain.EMPlan plan=et.getPlan();
            if(ObjectUtils.isEmpty(plan)){
                cn.ibizlab.eam.core.eam_core.domain.EMPlan majorEntity=emplanService.get(et.getPlanid());
                et.setPlan(majorEntity);
                plan=majorEntity;
            }
            et.setPlanname(plan.getEmplanname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMPlanCDT> getEmplancdtByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMPlanCDT> getEmplancdtByEntities(List<EMPlanCDT> entities) {
        List ids =new ArrayList();
        for(EMPlanCDT entity : entities){
            Serializable id=entity.getEmplancdtid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



