package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMPODetail;
/**
 * 关系型数据实体[EMPODetail] 查询条件对象
 */
@Slf4j
@Data
public class EMPODetailSearchContext extends QueryWrapperContext<EMPODetail> {

	private String n_rempname_eq;//[收货人]
	public void setN_rempname_eq(String n_rempname_eq) {
        this.n_rempname_eq = n_rempname_eq;
        if(!ObjectUtils.isEmpty(this.n_rempname_eq)){
            this.getSearchCond().eq("rempname", n_rempname_eq);
        }
    }
	private String n_rempname_like;//[收货人]
	public void setN_rempname_like(String n_rempname_like) {
        this.n_rempname_like = n_rempname_like;
        if(!ObjectUtils.isEmpty(this.n_rempname_like)){
            this.getSearchCond().like("rempname", n_rempname_like);
        }
    }
	private String n_empid_eq;//[记账人]
	public void setN_empid_eq(String n_empid_eq) {
        this.n_empid_eq = n_empid_eq;
        if(!ObjectUtils.isEmpty(this.n_empid_eq)){
            this.getSearchCond().eq("empid", n_empid_eq);
        }
    }
	private Integer n_podetailstate_eq;//[条目状态]
	public void setN_podetailstate_eq(Integer n_podetailstate_eq) {
        this.n_podetailstate_eq = n_podetailstate_eq;
        if(!ObjectUtils.isEmpty(this.n_podetailstate_eq)){
            this.getSearchCond().eq("podetailstate", n_podetailstate_eq);
        }
    }
	private String n_podetailinfo_like;//[订单条目信息]
	public void setN_podetailinfo_like(String n_podetailinfo_like) {
        this.n_podetailinfo_like = n_podetailinfo_like;
        if(!ObjectUtils.isEmpty(this.n_podetailinfo_like)){
            this.getSearchCond().like("podetailinfo", n_podetailinfo_like);
        }
    }
	private String n_empname_eq;//[记账人]
	public void setN_empname_eq(String n_empname_eq) {
        this.n_empname_eq = n_empname_eq;
        if(!ObjectUtils.isEmpty(this.n_empname_eq)){
            this.getSearchCond().eq("empname", n_empname_eq);
        }
    }
	private String n_empname_like;//[记账人]
	public void setN_empname_like(String n_empname_like) {
        this.n_empname_like = n_empname_like;
        if(!ObjectUtils.isEmpty(this.n_empname_like)){
            this.getSearchCond().like("empname", n_empname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_sapsl_eq;//[sap税率]
	public void setN_sapsl_eq(String n_sapsl_eq) {
        this.n_sapsl_eq = n_sapsl_eq;
        if(!ObjectUtils.isEmpty(this.n_sapsl_eq)){
            this.getSearchCond().eq("sapsl", n_sapsl_eq);
        }
    }
	private String n_wfstep_eq;//[流程步骤]
	public void setN_wfstep_eq(String n_wfstep_eq) {
        this.n_wfstep_eq = n_wfstep_eq;
        if(!ObjectUtils.isEmpty(this.n_wfstep_eq)){
            this.getSearchCond().eq("wfstep", n_wfstep_eq);
        }
    }
	private String n_empodetailname_like;//[订单条目名称]
	public void setN_empodetailname_like(String n_empodetailname_like) {
        this.n_empodetailname_like = n_empodetailname_like;
        if(!ObjectUtils.isEmpty(this.n_empodetailname_like)){
            this.getSearchCond().like("empodetailname", n_empodetailname_like);
        }
    }
	private String n_rempid_eq;//[收货人]
	public void setN_rempid_eq(String n_rempid_eq) {
        this.n_rempid_eq = n_rempid_eq;
        if(!ObjectUtils.isEmpty(this.n_rempid_eq)){
            this.getSearchCond().eq("rempid", n_rempid_eq);
        }
    }
	private String n_itemname_eq;//[物品]
	public void setN_itemname_eq(String n_itemname_eq) {
        this.n_itemname_eq = n_itemname_eq;
        if(!ObjectUtils.isEmpty(this.n_itemname_eq)){
            this.getSearchCond().eq("itemname", n_itemname_eq);
        }
    }
	private String n_itemname_like;//[物品]
	public void setN_itemname_like(String n_itemname_like) {
        this.n_itemname_like = n_itemname_like;
        if(!ObjectUtils.isEmpty(this.n_itemname_like)){
            this.getSearchCond().like("itemname", n_itemname_like);
        }
    }
	private String n_runitname_eq;//[收货单位]
	public void setN_runitname_eq(String n_runitname_eq) {
        this.n_runitname_eq = n_runitname_eq;
        if(!ObjectUtils.isEmpty(this.n_runitname_eq)){
            this.getSearchCond().eq("runitname", n_runitname_eq);
        }
    }
	private String n_runitname_like;//[收货单位]
	public void setN_runitname_like(String n_runitname_like) {
        this.n_runitname_like = n_runitname_like;
        if(!ObjectUtils.isEmpty(this.n_runitname_like)){
            this.getSearchCond().like("runitname", n_runitname_like);
        }
    }
	private String n_wplistname_eq;//[采购申请]
	public void setN_wplistname_eq(String n_wplistname_eq) {
        this.n_wplistname_eq = n_wplistname_eq;
        if(!ObjectUtils.isEmpty(this.n_wplistname_eq)){
            this.getSearchCond().eq("wplistname", n_wplistname_eq);
        }
    }
	private String n_wplistname_like;//[采购申请]
	public void setN_wplistname_like(String n_wplistname_like) {
        this.n_wplistname_like = n_wplistname_like;
        if(!ObjectUtils.isEmpty(this.n_wplistname_like)){
            this.getSearchCond().like("wplistname", n_wplistname_like);
        }
    }
	private String n_poname_eq;//[订单]
	public void setN_poname_eq(String n_poname_eq) {
        this.n_poname_eq = n_poname_eq;
        if(!ObjectUtils.isEmpty(this.n_poname_eq)){
            this.getSearchCond().eq("poname", n_poname_eq);
        }
    }
	private String n_poname_like;//[订单]
	public void setN_poname_like(String n_poname_like) {
        this.n_poname_like = n_poname_like;
        if(!ObjectUtils.isEmpty(this.n_poname_like)){
            this.getSearchCond().like("poname", n_poname_like);
        }
    }
	private String n_unitname_eq;//[订货单位]
	public void setN_unitname_eq(String n_unitname_eq) {
        this.n_unitname_eq = n_unitname_eq;
        if(!ObjectUtils.isEmpty(this.n_unitname_eq)){
            this.getSearchCond().eq("unitname", n_unitname_eq);
        }
    }
	private String n_unitname_like;//[订货单位]
	public void setN_unitname_like(String n_unitname_like) {
        this.n_unitname_like = n_unitname_like;
        if(!ObjectUtils.isEmpty(this.n_unitname_like)){
            this.getSearchCond().like("unitname", n_unitname_like);
        }
    }
	private String n_itemid_eq;//[物品]
	public void setN_itemid_eq(String n_itemid_eq) {
        this.n_itemid_eq = n_itemid_eq;
        if(!ObjectUtils.isEmpty(this.n_itemid_eq)){
            this.getSearchCond().eq("itemid", n_itemid_eq);
        }
    }
	private String n_unitid_eq;//[订货单位]
	public void setN_unitid_eq(String n_unitid_eq) {
        this.n_unitid_eq = n_unitid_eq;
        if(!ObjectUtils.isEmpty(this.n_unitid_eq)){
            this.getSearchCond().eq("unitid", n_unitid_eq);
        }
    }
	private String n_poid_eq;//[订单]
	public void setN_poid_eq(String n_poid_eq) {
        this.n_poid_eq = n_poid_eq;
        if(!ObjectUtils.isEmpty(this.n_poid_eq)){
            this.getSearchCond().eq("poid", n_poid_eq);
        }
    }
	private String n_runitid_eq;//[收货单位]
	public void setN_runitid_eq(String n_runitid_eq) {
        this.n_runitid_eq = n_runitid_eq;
        if(!ObjectUtils.isEmpty(this.n_runitid_eq)){
            this.getSearchCond().eq("runitid", n_runitid_eq);
        }
    }
	private String n_wplistid_eq;//[采购申请]
	public void setN_wplistid_eq(String n_wplistid_eq) {
        this.n_wplistid_eq = n_wplistid_eq;
        if(!ObjectUtils.isEmpty(this.n_wplistid_eq)){
            this.getSearchCond().eq("wplistid", n_wplistid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("empodetailname", query)   
            );
		 }
	}
}



