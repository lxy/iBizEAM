package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTGSS;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLCTGSSSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQLCTGSS] 服务对象接口
 */
public interface IEMEQLCTGSSService extends IService<EMEQLCTGSS>{

    boolean create(EMEQLCTGSS et) ;
    void createBatch(List<EMEQLCTGSS> list) ;
    boolean update(EMEQLCTGSS et) ;
    void updateBatch(List<EMEQLCTGSS> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMEQLCTGSS get(String key) ;
    EMEQLCTGSS getDraft(EMEQLCTGSS et) ;
    boolean checkKey(EMEQLCTGSS et) ;
    boolean save(EMEQLCTGSS et) ;
    void saveBatch(List<EMEQLCTGSS> list) ;
    Page<EMEQLCTGSS> searchDefault(EMEQLCTGSSSearchContext context) ;
    Page<EMEQLCTGSS> searchMain3(EMEQLCTGSSSearchContext context) ;
    List<EMEQLCTGSS> selectByEquipid(String emequipid) ;
    void removeByEquipid(String emequipid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQLCTGSS> getEmeqlctgssByIds(List<String> ids) ;
    List<EMEQLCTGSS> getEmeqlctgssByEntities(List<EMEQLCTGSS> entities) ;
}


