package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEICamSetup;
import cn.ibizlab.eam.core.eam_core.filter.EMEICamSetupSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEICamSetup] 服务对象接口
 */
public interface IEMEICamSetupService extends IService<EMEICamSetup>{

    boolean create(EMEICamSetup et) ;
    void createBatch(List<EMEICamSetup> list) ;
    boolean update(EMEICamSetup et) ;
    void updateBatch(List<EMEICamSetup> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMEICamSetup get(String key) ;
    EMEICamSetup getDraft(EMEICamSetup et) ;
    boolean checkKey(EMEICamSetup et) ;
    boolean save(EMEICamSetup et) ;
    void saveBatch(List<EMEICamSetup> list) ;
    Page<EMEICamSetup> searchDefault(EMEICamSetupSearchContext context) ;
    List<EMEICamSetup> selectByEiobjid(String emeicamid) ;
    void removeByEiobjid(String emeicamid) ;
    List<EMEICamSetup> selectByEqlocationid(String emeqlocationid) ;
    void removeByEqlocationid(String emeqlocationid) ;
    List<EMEICamSetup> selectByEquipid(String emequipid) ;
    void removeByEquipid(String emequipid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEICamSetup> getEmeicamsetupByIds(List<String> ids) ;
    List<EMEICamSetup> getEmeicamsetupByEntities(List<EMEICamSetup> entities) ;
}


