package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQSTop;
import cn.ibizlab.eam.core.eam_core.filter.EMEQSTopSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQSTop] 服务对象接口
 */
public interface IEMEQSTopService extends IService<EMEQSTop>{

    boolean create(EMEQSTop et) ;
    void createBatch(List<EMEQSTop> list) ;
    boolean update(EMEQSTop et) ;
    void updateBatch(List<EMEQSTop> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMEQSTop get(String key) ;
    EMEQSTop getDraft(EMEQSTop et) ;
    boolean checkKey(EMEQSTop et) ;
    boolean save(EMEQSTop et) ;
    void saveBatch(List<EMEQSTop> list) ;
    Page<EMEQSTop> searchDefault(EMEQSTopSearchContext context) ;
    List<EMEQSTop> selectByEmeqtypeid(String emeqtypeid) ;
    void removeByEmeqtypeid(String emeqtypeid) ;
    List<EMEQSTop> selectByEmequipid(String emequipid) ;
    void removeByEmequipid(String emequipid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQSTop> getEmeqstopByIds(List<String> ids) ;
    List<EMEQSTop> getEmeqstopByEntities(List<EMEQSTop> entities) ;
}


