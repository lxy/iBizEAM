package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMMonthly;
import cn.ibizlab.eam.core.eam_core.filter.EMMonthlySearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMMonthlyService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMMonthlyMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[维修中心月度计划] 服务对象接口实现
 */
@Slf4j
@Service("EMMonthlyServiceImpl")
public class EMMonthlyServiceImpl extends ServiceImpl<EMMonthlyMapper, EMMonthly> implements IEMMonthlyService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMMonthlyDetailService emmonthlydetailService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMMonthly et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmmonthlyid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMMonthly> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMMonthly et) {
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emmonthlyid",et.getEmmonthlyid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmmonthlyid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMMonthly> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMMonthly get(String key) {
        EMMonthly et = getById(key);
        if(et==null){
            et=new EMMonthly();
            et.setEmmonthlyid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMMonthly getDraft(EMMonthly et) {
        return et;
    }

    @Override
    public boolean checkKey(EMMonthly et) {
        return (!ObjectUtils.isEmpty(et.getEmmonthlyid()))&&(!Objects.isNull(this.getById(et.getEmmonthlyid())));
    }
    @Override
    @Transactional
    public boolean save(EMMonthly et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMMonthly et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMMonthly> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMMonthly> list) {
        saveOrUpdateBatch(list,batchSize);
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMMonthly> searchDefault(EMMonthlySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMMonthly> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMMonthly>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMMonthly> getEmmonthlyByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMMonthly> getEmmonthlyByEntities(List<EMMonthly> entities) {
        List ids =new ArrayList();
        for(EMMonthly entity : entities){
            Serializable id=entity.getEmmonthlyid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



