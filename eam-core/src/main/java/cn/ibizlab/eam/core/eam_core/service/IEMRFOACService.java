package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMRFOAC;
import cn.ibizlab.eam.core.eam_core.filter.EMRFOACSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMRFOAC] 服务对象接口
 */
public interface IEMRFOACService extends IService<EMRFOAC>{

    boolean create(EMRFOAC et) ;
    void createBatch(List<EMRFOAC> list) ;
    boolean update(EMRFOAC et) ;
    void updateBatch(List<EMRFOAC> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    EMRFOAC get(String key) ;
    EMRFOAC getDraft(EMRFOAC et) ;
    boolean checkKey(EMRFOAC et) ;
    boolean save(EMRFOAC et) ;
    void saveBatch(List<EMRFOAC> list) ;
    Page<EMRFOAC> searchDefault(EMRFOACSearchContext context) ;
    List<EMRFOAC> selectByRfodeid(String emrfodeid) ;
    void removeByRfodeid(String emrfodeid) ;
    List<EMRFOAC> selectByRfomoid(String emrfomoid) ;
    void removeByRfomoid(String emrfomoid) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMRFOAC> getEmrfoacByIds(List<String> ids) ;
    List<EMRFOAC> getEmrfoacByEntities(List<EMRFOAC> entities) ;
}


