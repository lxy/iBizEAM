package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSTopMoni;
import cn.ibizlab.eam.core.eam_core.filter.EMEQSTopMoniSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQSTopMoniService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQSTopMoniMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[设备停机监控明细] 服务对象接口实现
 */
@Slf4j
@Service("EMEQSTopMoniServiceImpl")
public class EMEQSTopMoniServiceImpl extends ServiceImpl<EMEQSTopMoniMapper, EMEQSTopMoni> implements IEMEQSTopMoniService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQSTopService emeqstopService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQTypeService emeqtypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQSTopMoni et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmeqstopmoniid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQSTopMoni> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQSTopMoni et) {
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emeqstopmoniid",et.getEmeqstopmoniid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmeqstopmoniid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQSTopMoni> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQSTopMoni get(String key) {
        EMEQSTopMoni et = getById(key);
        if(et==null){
            et=new EMEQSTopMoni();
            et.setEmeqstopmoniid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMEQSTopMoni getDraft(EMEQSTopMoni et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEQSTopMoni et) {
        return (!ObjectUtils.isEmpty(et.getEmeqstopmoniid()))&&(!Objects.isNull(this.getById(et.getEmeqstopmoniid())));
    }
    @Override
    @Transactional
    public boolean save(EMEQSTopMoni et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQSTopMoni et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQSTopMoni> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQSTopMoni> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMEQSTopMoni> selectByEmeqstopid(String emeqstopid) {
        return baseMapper.selectByEmeqstopid(emeqstopid);
    }

    @Override
    public void removeByEmeqstopid(String emeqstopid) {
        this.remove(new QueryWrapper<EMEQSTopMoni>().eq("emeqstopid",emeqstopid));
    }

	@Override
    public List<EMEQSTopMoni> selectByEmeqtypeid(String emeqtypeid) {
        return baseMapper.selectByEmeqtypeid(emeqtypeid);
    }

    @Override
    public void removeByEmeqtypeid(String emeqtypeid) {
        this.remove(new QueryWrapper<EMEQSTopMoni>().eq("emeqtypeid",emeqtypeid));
    }

	@Override
    public List<EMEQSTopMoni> selectByEmequipid(String emequipid) {
        return baseMapper.selectByEmequipid(emequipid);
    }

    @Override
    public void removeByEmequipid(String emequipid) {
        this.remove(new QueryWrapper<EMEQSTopMoni>().eq("emequipid",emequipid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQSTopMoni> searchDefault(EMEQSTopMoniSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQSTopMoni> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQSTopMoni>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEQSTopMoni et){
        //实体关系[DER1N_EMEQSTOPMONI_EMEQSTOP_EMEQSTOPID]
        if(!ObjectUtils.isEmpty(et.getEmeqstopid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEQSTop emeqstop=et.getEmeqstop();
            if(ObjectUtils.isEmpty(emeqstop)){
                cn.ibizlab.eam.core.eam_core.domain.EMEQSTop majorEntity=emeqstopService.get(et.getEmeqstopid());
                et.setEmeqstop(majorEntity);
                emeqstop=majorEntity;
            }
            et.setEmeqstopname(emeqstop.getEmeqstopname());
        }
        //实体关系[DER1N_EMEQSTOPMONI_EMEQTYPE_EMEQTYPEID]
        if(!ObjectUtils.isEmpty(et.getEmeqtypeid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEQType emeqtype=et.getEmeqtype();
            if(ObjectUtils.isEmpty(emeqtype)){
                cn.ibizlab.eam.core.eam_core.domain.EMEQType majorEntity=emeqtypeService.get(et.getEmeqtypeid());
                et.setEmeqtype(majorEntity);
                emeqtype=majorEntity;
            }
            et.setEmeqtypename(emeqtype.getEqtypeinfo());
        }
        //实体关系[DER1N_EMEQSTOPMONI_EMEQUIP_EMEQUIPID]
        if(!ObjectUtils.isEmpty(et.getEmequipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip emequip=et.getEmequip();
            if(ObjectUtils.isEmpty(emequip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEmequipid());
                et.setEmequip(majorEntity);
                emequip=majorEntity;
            }
            et.setEmequipname(emequip.getEquipinfo());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQSTopMoni> getEmeqstopmoniByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQSTopMoni> getEmeqstopmoniByEntities(List<EMEQSTopMoni> entities) {
        List ids =new ArrayList();
        for(EMEQSTopMoni entity : entities){
            Serializable id=entity.getEmeqstopmoniid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



