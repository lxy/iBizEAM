

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItemPL;
import cn.ibizlab.eam.core.eam_core.domain.EMItemTrade;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMItemPLInheritMapping {

    @Mappings({
        @Mapping(source ="emitemplid",target = "emitemtradeid"),
        @Mapping(source ="emitemplname",target = "emitemtradename"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMItemTrade toEmitemtrade(EMItemPL emitempl);

    @Mappings({
        @Mapping(source ="emitemtradeid" ,target = "emitemplid"),
        @Mapping(source ="emitemtradename" ,target = "emitemplname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMItemPL toEmitempl(EMItemTrade emitemtrade);

    List<EMItemTrade> toEmitemtrade(List<EMItemPL> emitempl);

    List<EMItemPL> toEmitempl(List<EMItemTrade> emitemtrade);

}


