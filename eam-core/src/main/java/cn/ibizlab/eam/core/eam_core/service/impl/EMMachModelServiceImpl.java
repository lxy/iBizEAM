package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMMachModel;
import cn.ibizlab.eam.core.eam_core.filter.EMMachModelSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMMachModelService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMMachModelMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[机型] 服务对象接口实现
 */
@Slf4j
@Service("EMMachModelServiceImpl")
public class EMMachModelServiceImpl extends ServiceImpl<EMMachModelMapper, EMMachModel> implements IEMMachModelService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMMachModel et) {
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmmachmodelid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMMachModel> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMMachModel et) {
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emmachmodelid",et.getEmmachmodelid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmmachmodelid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMMachModel> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMMachModel get(String key) {
        EMMachModel et = getById(key);
        if(et==null){
            et=new EMMachModel();
            et.setEmmachmodelid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMMachModel getDraft(EMMachModel et) {
        return et;
    }

    @Override
    public boolean checkKey(EMMachModel et) {
        return (!ObjectUtils.isEmpty(et.getEmmachmodelid()))&&(!Objects.isNull(this.getById(et.getEmmachmodelid())));
    }
    @Override
    @Transactional
    public boolean save(EMMachModel et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMMachModel et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMMachModel> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMMachModel> list) {
        saveOrUpdateBatch(list,batchSize);
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMMachModel> searchDefault(EMMachModelSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMMachModel> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMMachModel>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMMachModel> getEmmachmodelByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMMachModel> getEmmachmodelByEntities(List<EMMachModel> entities) {
        List ids =new ArrayList();
        for(EMMachModel entity : entities){
            Serializable id=entity.getEmmachmodelid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



