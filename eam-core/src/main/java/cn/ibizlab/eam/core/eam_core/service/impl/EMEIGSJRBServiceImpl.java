package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEIGSJRB;
import cn.ibizlab.eam.core.eam_core.filter.EMEIGSJRBSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEIGSJRBService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEIGSJRBMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[工索具日报] 服务对象接口实现
 */
@Slf4j
@Service("EMEIGSJRBServiceImpl")
public class EMEIGSJRBServiceImpl extends ServiceImpl<EMEIGSJRBMapper, EMEIGSJRB> implements IEMEIGSJRBService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEIGSJRB et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmeigsjrbid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEIGSJRB> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEIGSJRB et) {
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emeigsjrbid",et.getEmeigsjrbid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmeigsjrbid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEIGSJRB> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEIGSJRB get(String key) {
        EMEIGSJRB et = getById(key);
        if(et==null){
            et=new EMEIGSJRB();
            et.setEmeigsjrbid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMEIGSJRB getDraft(EMEIGSJRB et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEIGSJRB et) {
        return (!ObjectUtils.isEmpty(et.getEmeigsjrbid()))&&(!Objects.isNull(this.getById(et.getEmeigsjrbid())));
    }
    @Override
    @Transactional
    public boolean save(EMEIGSJRB et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEIGSJRB et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEIGSJRB> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEIGSJRB> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMEIGSJRB> selectByItemid(String emitemid) {
        return baseMapper.selectByItemid(emitemid);
    }

    @Override
    public void removeByItemid(String emitemid) {
        this.remove(new QueryWrapper<EMEIGSJRB>().eq("itemid",emitemid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEIGSJRB> searchDefault(EMEIGSJRBSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEIGSJRB> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEIGSJRB>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEIGSJRB et){
        //实体关系[DER1N_EMEIGSJRB_EMITEM_ITEMID]
        if(!ObjectUtils.isEmpty(et.getItemid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItem item=et.getItem();
            if(ObjectUtils.isEmpty(item)){
                cn.ibizlab.eam.core.eam_core.domain.EMItem majorEntity=emitemService.get(et.getItemid());
                et.setItem(majorEntity);
                item=majorEntity;
            }
            et.setItemname(item.getEmitemname());
            et.setModelcode(item.getItemmodelcode());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEIGSJRB> getEmeigsjrbByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEIGSJRB> getEmeigsjrbByEntities(List<EMEIGSJRB> entities) {
        List ids =new ArrayList();
        for(EMEIGSJRB entity : entities){
            Serializable id=entity.getEmeigsjrbid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



