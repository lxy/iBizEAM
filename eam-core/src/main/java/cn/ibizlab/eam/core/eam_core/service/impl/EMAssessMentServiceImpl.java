package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMAssessMent;
import cn.ibizlab.eam.core.eam_core.filter.EMAssessMentSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMAssessMentService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMAssessMentMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[计划及项目进程考核] 服务对象接口实现
 */
@Slf4j
@Service("EMAssessMentServiceImpl")
public class EMAssessMentServiceImpl extends ServiceImpl<EMAssessMentMapper, EMAssessMent> implements IEMAssessMentService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMAssessMentMXService emassessmentmxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFTeamService pfteamService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMAssessMent et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmassessmentid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMAssessMent> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMAssessMent et) {
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emassessmentid",et.getEmassessmentid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmassessmentid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMAssessMent> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMAssessMent get(String key) {
        EMAssessMent et = getById(key);
        if(et==null){
            et=new EMAssessMent();
            et.setEmassessmentid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMAssessMent getDraft(EMAssessMent et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMAssessMent et) {
        return (!ObjectUtils.isEmpty(et.getEmassessmentid()))&&(!Objects.isNull(this.getById(et.getEmassessmentid())));
    }
    @Override
    @Transactional
    public boolean save(EMAssessMent et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMAssessMent et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMAssessMent> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMAssessMent> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMAssessMent> selectByPfteamid(String pfteamid) {
        return baseMapper.selectByPfteamid(pfteamid);
    }

    @Override
    public void removeByPfteamid(String pfteamid) {
        this.remove(new QueryWrapper<EMAssessMent>().eq("pfteamid",pfteamid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMAssessMent> searchDefault(EMAssessMentSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMAssessMent> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMAssessMent>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMAssessMent et){
        //实体关系[DER1N_EMASSESSMENT_PFTEAM_PFTEAMID]
        if(!ObjectUtils.isEmpty(et.getPfteamid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFTeam pfteam=et.getPfteam();
            if(ObjectUtils.isEmpty(pfteam)){
                cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorEntity=pfteamService.get(et.getPfteamid());
                et.setPfteam(majorEntity);
                pfteam=majorEntity;
            }
            et.setPfteamname(pfteam.getPfteamname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMAssessMent> getEmassessmentByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMAssessMent> getEmassessmentByEntities(List<EMAssessMent> entities) {
        List ids =new ArrayList();
        for(EMAssessMent entity : entities){
            Serializable id=entity.getEmassessmentid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



