

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItemPUse;
import cn.ibizlab.eam.core.eam_core.domain.EMItemTrade;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMItemPUseInheritMapping {

    @Mappings({
        @Mapping(source ="emitempuseid",target = "emitemtradeid"),
        @Mapping(source ="emitempusename",target = "emitemtradename"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="pusestate",target = "tradestate"),
    })
    EMItemTrade toEmitemtrade(EMItemPUse emitempuse);

    @Mappings({
        @Mapping(source ="emitemtradeid" ,target = "emitempuseid"),
        @Mapping(source ="emitemtradename" ,target = "emitempusename"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="tradestate",target = "pusestate"),
    })
    EMItemPUse toEmitempuse(EMItemTrade emitemtrade);

    List<EMItemTrade> toEmitemtrade(List<EMItemPUse> emitempuse);

    List<EMItemPUse> toEmitempuse(List<EMItemTrade> emitemtrade);

}


