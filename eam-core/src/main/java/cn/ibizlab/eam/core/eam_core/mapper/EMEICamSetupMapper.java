package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMEICamSetup;
import cn.ibizlab.eam.core.eam_core.filter.EMEICamSetupSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMEICamSetupMapper extends BaseMapper<EMEICamSetup>{

    Page<EMEICamSetup> searchDefault(IPage page, @Param("srf") EMEICamSetupSearchContext context, @Param("ew") Wrapper<EMEICamSetup> wrapper) ;
    @Override
    EMEICamSetup selectById(Serializable id);
    @Override
    int insert(EMEICamSetup entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMEICamSetup entity);
    @Override
    int update(@Param(Constants.ENTITY) EMEICamSetup entity, @Param("ew") Wrapper<EMEICamSetup> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMEICamSetup> selectByEiobjid(@Param("emeicamid") Serializable emeicamid) ;

    List<EMEICamSetup> selectByEqlocationid(@Param("emeqlocationid") Serializable emeqlocationid) ;

    List<EMEICamSetup> selectByEquipid(@Param("emequipid") Serializable emequipid) ;

}
