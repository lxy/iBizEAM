package cn.ibizlab.eam.core.eam_core.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.eam.core.eam_core.service.logic.IEMWPListGenIdLogic;
import cn.ibizlab.eam.core.eam_core.domain.EMWPList;

/**
 * 关系型数据实体[GenId] 对象
 */
@Slf4j
@Service
public class EMWPListGenIdLogicImpl implements IEMWPListGenIdLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMWPListService emwplistservice;

    public cn.ibizlab.eam.core.eam_core.service.IEMWPListService getEmwplistService() {
        return this.emwplistservice;
    }


    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMWPListService iBzSysDefaultService;

    public cn.ibizlab.eam.core.eam_core.service.IEMWPListService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(EMWPList et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("emwplistgeniddefault",et);
           kieSession.setGlobal("emwplistservice",emwplistservice);
           kieSession.setGlobal("iBzSysEmwplistDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.eam.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.eam.core.eam_core.service.logic.emwplistgenid");

        }catch(Exception e){
            throw new RuntimeException("执行[创建自动生产采购申请号]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}
