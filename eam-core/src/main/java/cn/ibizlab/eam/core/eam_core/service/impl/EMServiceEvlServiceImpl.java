package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMServiceEvl;
import cn.ibizlab.eam.core.eam_core.filter.EMServiceEvlSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMServiceEvlService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMServiceEvlMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[服务商评估] 服务对象接口实现
 */
@Slf4j
@Service("EMServiceEvlServiceImpl")
public class EMServiceEvlServiceImpl extends ServiceImpl<EMServiceEvlMapper, EMServiceEvl> implements IEMServiceEvlService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMServiceService emserviceService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMServiceEvl et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmserviceevlid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMServiceEvl> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMServiceEvl et) {
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emserviceevlid",et.getEmserviceevlid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmserviceevlid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMServiceEvl> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMServiceEvl get(String key) {
        EMServiceEvl et = getById(key);
        if(et==null){
            et=new EMServiceEvl();
            et.setEmserviceevlid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMServiceEvl getDraft(EMServiceEvl et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMServiceEvl et) {
        return (!ObjectUtils.isEmpty(et.getEmserviceevlid()))&&(!Objects.isNull(this.getById(et.getEmserviceevlid())));
    }
    @Override
    @Transactional
    public boolean save(EMServiceEvl et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMServiceEvl et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMServiceEvl> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMServiceEvl> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMServiceEvl> selectByServiceid(String emserviceid) {
        return baseMapper.selectByServiceid(emserviceid);
    }

    @Override
    public void removeByServiceid(String emserviceid) {
        this.remove(new QueryWrapper<EMServiceEvl>().eq("serviceid",emserviceid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMServiceEvl> searchDefault(EMServiceEvlSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMServiceEvl> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMServiceEvl>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 评估前五
     */
    @Override
    public Page<HashMap> searchEvaluateTop5(EMServiceEvlSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<HashMap> pages=baseMapper.searchEvaluateTop5(context.getPages(),context,context.getSelectCond());
        return new PageImpl<HashMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 OverallEVL
     */
    @Override
    public Page<HashMap> searchOverallEVL(EMServiceEvlSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<HashMap> pages=baseMapper.searchOverallEVL(context.getPages(),context,context.getSelectCond());
        return new PageImpl<HashMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMServiceEvl et){
        //实体关系[DER1N_EMSERVICEEVL_EMSERVICE_SERVICEID]
        if(!ObjectUtils.isEmpty(et.getServiceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService service=et.getService();
            if(ObjectUtils.isEmpty(service)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getServiceid());
                et.setService(majorEntity);
                service=majorEntity;
            }
            et.setServicename(service.getEmservicename());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMServiceEvl> getEmserviceevlByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMServiceEvl> getEmserviceevlByEntities(List<EMServiceEvl> entities) {
        List ids =new ArrayList();
        for(EMServiceEvl entity : entities){
            Serializable id=entity.getEmserviceevlid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



