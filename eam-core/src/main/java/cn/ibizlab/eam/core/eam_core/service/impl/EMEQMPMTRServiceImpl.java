package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQMPMTR;
import cn.ibizlab.eam.core.eam_core.filter.EMEQMPMTRSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQMPMTRService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQMPMTRMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[设备仪表读数] 服务对象接口实现
 */
@Slf4j
@Service("EMEQMPMTRServiceImpl")
public class EMEQMPMTRServiceImpl extends ServiceImpl<EMEQMPMTRMapper, EMEQMPMTR> implements IEMEQMPMTRService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQMPService emeqmpService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOService emwoService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQMPMTR et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy(get(et.getEmeqmpmtrid()),et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQMPMTR> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQMPMTR et) {
        fillParentData(et);
        emdprctService.update(emeqmpmtrInheritMapping.toEmdprct(et));
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("emeqmpmtrid",et.getEmeqmpmtrid())))
            return false;
        CachedBeanCopier.copy(get(et.getEmeqmpmtrid()),et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQMPMTR> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emdprctService.remove(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQMPMTR get(String key) {
        EMEQMPMTR et = getById(key);
        if(et==null){
            et=new EMEQMPMTR();
            et.setEmeqmpmtrid(key);
        }
        else{
        }
        return et;
    }

    @Override
    public EMEQMPMTR getDraft(EMEQMPMTR et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEQMPMTR et) {
        return (!ObjectUtils.isEmpty(et.getEmeqmpmtrid()))&&(!Objects.isNull(this.getById(et.getEmeqmpmtrid())));
    }
    @Override
    @Transactional
    public boolean save(EMEQMPMTR et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQMPMTR et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQMPMTR> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQMPMTR> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<EMEQMPMTR> selectByMpid(String emeqmpid) {
        return baseMapper.selectByMpid(emeqmpid);
    }

    @Override
    public void removeByMpid(String emeqmpid) {
        this.remove(new QueryWrapper<EMEQMPMTR>().eq("mpid",emeqmpid));
    }

	@Override
    public List<EMEQMPMTR> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }

    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMEQMPMTR>().eq("equipid",emequipid));
    }

	@Override
    public List<EMEQMPMTR> selectByObjid(String emobjectid) {
        return baseMapper.selectByObjid(emobjectid);
    }

    @Override
    public void removeByObjid(String emobjectid) {
        this.remove(new QueryWrapper<EMEQMPMTR>().eq("objid",emobjectid));
    }

	@Override
    public List<EMEQMPMTR> selectByWoid(String emwoid) {
        return baseMapper.selectByWoid(emwoid);
    }

    @Override
    public void removeByWoid(String emwoid) {
        this.remove(new QueryWrapper<EMEQMPMTR>().eq("woid",emwoid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQMPMTR> searchDefault(EMEQMPMTRSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQMPMTR> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQMPMTR>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEQMPMTR et){
        //实体关系[DER1N_EMEQMPMTR_EMEQMP_MPID]
        if(!ObjectUtils.isEmpty(et.getMpid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEQMP mp=et.getMp();
            if(ObjectUtils.isEmpty(mp)){
                cn.ibizlab.eam.core.eam_core.domain.EMEQMP majorEntity=emeqmpService.get(et.getMpid());
                et.setMp(majorEntity);
                mp=majorEntity;
            }
            et.setMpname(mp.getEmeqmpname());
        }
        //实体关系[DER1N_EMEQMPMTR_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMEQMPMTR_EMOBJECT_OBJID]
        if(!ObjectUtils.isEmpty(et.getObjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject obj=et.getObj();
            if(ObjectUtils.isEmpty(obj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getObjid());
                et.setObj(majorEntity);
                obj=majorEntity;
            }
            et.setObjname(obj.getEmobjectname());
        }
        //实体关系[DER1N_EMEQMPMTR_EMWO_WOID]
        if(!ObjectUtils.isEmpty(et.getWoid())){
            cn.ibizlab.eam.core.eam_core.domain.EMWO wo=et.getWo();
            if(ObjectUtils.isEmpty(wo)){
                cn.ibizlab.eam.core.eam_core.domain.EMWO majorEntity=emwoService.get(et.getWoid());
                et.setWo(majorEntity);
                wo=majorEntity;
            }
            et.setWoname(wo.getEmwoname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMEQMPMTRInheritMapping emeqmpmtrInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMDPRCTService emdprctService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMEQMPMTR et){
        if(ObjectUtils.isEmpty(et.getEmeqmpmtrid()))
            et.setEmeqmpmtrid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMDPRCT emdprct =emeqmpmtrInheritMapping.toEmdprct(et);
        emdprct.set("emdprcttype","EQMPMTR");
        emdprctService.create(emdprct);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQMPMTR> getEmeqmpmtrByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQMPMTR> getEmeqmpmtrByEntities(List<EMEQMPMTR> entities) {
        List ids =new ArrayList();
        for(EMEQMPMTR entity : entities){
            Serializable id=entity.getEmeqmpmtrid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }


}



